"use strict";

let jwt                             = require('jsonwebtoken');
let redis                           = require('redis');
let redisAdapter                    = require('socket.io-redis');
let { REDIS_SEPERATOR }             = require('../config/cf_redis');

// tạo GlobalStore -> lưu trữ danh sách user đang online (nhận được socketIO)
let usersConnectedInstance          = require('../config/cf_globalscope').GlobalStore;
let usersConnected                  = usersConnectedInstance.usersOnline;
let ObjectID                        = require('mongoose').Types.ObjectId;

let {
    CSS_TEST,
    SSC_TEST,
} = require('./constants');

/**
 * UTILS
 */
let { replaceExist, decrypt }   			= require('../utils/utils');
let { mapSocketIDAndData, sendToClient, getUserNotConnected }    = require('../utils/socket_utils');
let { sendMessageMobile }    = require('../fcm/utils');
let { SCREEN_KEY }           = require("../config/cf_constants")

/**
 * MODELS, COLLECTIONS
 */
let { USER_MODEL }            = require('../packages/users');
// let CUSTOMER_MODEL            = require('../packages/customer/models/customer').MODEL;


module.exports = function (io) {
    let pub = redis.createClient({
        // detect_buffers: true,
        // return_buffers: true,

        host: REDIS_SEPERATOR.HOST,
        port: REDIS_SEPERATOR.PORT,
        auth_pass: REDIS_SEPERATOR.PWD
    });

    let sub = redis.createClient({
        // detect_buffers: true,
        // return_buffers: true,

        host: REDIS_SEPERATOR.HOST,
        port: REDIS_SEPERATOR.PORT,
        auth_pass: REDIS_SEPERATOR.PWD
    });
    // let sub = CLIENT_REDIS;
    // let pub = CLIENT_REDIS;

    io.adapter(redisAdapter({
        pubClient: pub,
        subClient: sub
    }));
    io
    .set('transports', ['websocket']) //config
    .use(async (socket, next) => {
        socket.isAuth    = false;
        
        if (socket.handshake.query && socket.handshake.query.token) {
            // console.log({ ["socket.handshake.query.token"]: socket.handshake.query.token });
            let signalVerifyToken = await USER_MODEL.checkAuth({ token: socket.handshake.query.token });
            if (signalVerifyToken.error) {
                console.log({ [`signalVerifyToken.error`]: signalVerifyToken.error })
                return next(new Error('Authentication error'));
            }
            let { data: infoUser } = signalVerifyToken;

            socket.decoded   = infoUser;
            socket.isAuth    = true;
            socket.tokenFake = false;
            /**
             * ADD USER INTO usersConnected
             * usersConnected
             */
            let { id: socketID } = socket;
            let { _id: userID, username }  = infoUser;
            socket.userID = userID;
            
            let usersConnectedGlobal = usersConnectedInstance.usersOnline;
            // console.log({
            //     [`danh sách TRƯỚC KHI user connected vào socket`]: usersConnectedGlobal
            // })

            // console.log({
            //     [`thông tin trước khi connect`]: usersConnectedGlobal,
            //     [`số lượng`]: usersConnectedGlobal.length
            // })

            usersConnected = replaceExist(usersConnectedGlobal, userID, socketID, username);

            // console.log({
            //     [`danh sách SAU KHI user connected vào socket`]: usersConnected
            // })
            // console.log({
            //     [`thông tin trước khi connect`]: usersConnected,
            //     [`số lượng`]: usersConnected.length,
            //     [`Truy cập mới`]: {
            //         userID, socketID, username
            //     }
            // })
            
            // console.log({
            //     [`danh sách SAU KHI user not connected vào socket`]: listUserNotConnected
            // });

            usersConnectedInstance.setUsersOnline(usersConnected);
            next();
        }
        if(socket.handshake.query && socket.handshake.query._token){ // customer_fake, customer auth
            try {
                let decoded = await jwt.verify(socket.handshake.query._token, process.env.JWT_SECRET_KEY);
                if (decoded && decoded.hashed) { // đang dùng token FAKE (user)
                    // console.log({ decoded })
                    let content = decrypt(decoded.hashed);
                    if(content !== process.env.ENCRYPT_SECRET_KEY) { 
                        console.log({ 
                            content,
                            [`process.env.ENCRYPT_SECRET_KEY`]: process.env.ENCRYPT_SECRET_KEY
                        })
                        next(new Error('Authentication error, catch workspace'));
                    } else {
                        console.log(`---------------2-----------------`)

                        socket.decoded      = decoded;
                        socket.tokenFake    = true;
                        socket.isAuth       = true;
                    }
                } else { // dùng token user (mobile có login)
                    // let signalVerifyToken = await CUSTOMER_MODEL.checkAuth({ token: socket.handshake.query._token });
                    // if (signalVerifyToken.error) {
                    //     console.log({ [`signalVerifyToken.error`]: signalVerifyToken.error })
                    //     return next(new Error('Authentication error'));
                    // }
                    // let { data: infoUser  } = signalVerifyToken;
                    // socket.decoded   = infoUser;
                    // socket.isAuth    = true;
                    // socket.tokenFake = false;
                    // /**
                    //  * ADD USER INTO usersConnected
                    //  * usersConnected
                    //  */
                    // let { id: socketID } = socket;
                    // let { _id: userID, phone }  = infoUser;
                    
                    // let usersConnectedGlobal = usersConnectedInstance.usersOnline
                    // // console.log({
                    // //     [`danh sách TRƯỚC KHI user connected vào socket`]: usersConnectedGlobal
                    // // })

                    // // console.log({
                    // //     [`thông tin trước khi connect`]: usersConnectedGlobal,
                    // //     [`số lượng`]:  usersConnectedGlobal && usersConnectedGlobal.length
                    // // })
        
                    // // usersConnected = replaceExist(usersConnectedGlobal, userID, socketID, phone); //TODO cần bổ sung thêm trường hợp gửi cho ENDUSER
        
                    // // console.log({
                    // //     [`danh sách SAU KHI user connected vào socket`]: usersConnected
                    // // })
                    // console.log({
                    //     [`thông tin trước khi connect`]: usersConnectedGlobal,
                    //     [`số lượng`]: usersConnectedGlobal && usersConnectedGlobal.length,
                    //     [`Truy cập mới`]: {
                    //         userID, socketID, phone
                    //     }
                    // })
        
                    // // console.log({
                    // //     [`danh sách SAU KHI user connected vào socket`]: usersConnected
                    // // })
        
                    // usersConnectedInstance.setUsersOnline(usersConnected);
                }

                return next();
            } catch (error) {
                console.log({ error })
                next(new Error('Authentication error, catch workspace'));
            }
        }
    })
    .on('connection', function (socket) {
        // console.log(`------------------connection------------`)
        // const { id: socketID, decoded, isAuth } = socket;
        // const { id: socketID, userID: currentUserID } = socket;
        const { id: socketID, decoded, isAuth, tokenFake } = socket;
        // console.log({ decoded, tokenFake });
        
        if (socket.isAuth) {
            socket.on(CSS_TEST, async data => {
                let { message } = data;

                console.log({ message });
                // mapSocketIDAndData(listUserSendNotifi, SSC_TEST, {
                //     message
                // }, usersConnected, io);

                /**
                 * Lấy danh sách user not connected
                 */
                //  let listUserNotConnected = getUserNotConnected(usersConnected, listUserSendNotifi);
                    
                //  let body = {
                //      screen_key: 'Setting',
                //      sender: userID,
                //      transactionID: transactionID
                //  };
              
                // sendMessageMobile({ title, description: content, arrReceiverID: listUserNotConnected, senderID: userID, body });
            })
        } // END AUTH

        socket.on('disconnect', function () {
            // console.log(`Disconnected..., Socket ID: ${socket.id}`);
            console.log(`Disconnected..., Socket ID: ${socket.id}`);
            /**
            * ADD USER INTO usersConnected
            * usersConnected
            */
           let { id: socketIDDisconnect, decoded } = socket;
           if (!decoded || !decoded._id) return;

           let { _id: userIDDisconnect } = decoded;
           
           let usersConnectedGlobal = usersConnectedInstance.usersOnline;

           let itemDisconnect          = usersConnectedGlobal.find(itemSocket => itemSocket.userID == userIDDisconnect);

           console.log({
               [`thông tin trước khi disconnect`]: itemDisconnect,
               [`số lượng socketID trước khi disconnect`]: itemDisconnect && itemDisconnect.socketID && itemDisconnect.socketID.length
           })

           let listItemStillConnect    = usersConnectedGlobal.filter(itemSocket => itemSocket.userID != userIDDisconnect);
           let listSocketIDOfItemDisconnectAfterRemoveSocketDisconnect = itemDisconnect && itemDisconnect.socketID.filter(socketID => socketID != socketIDDisconnect);

           let itemDisconnectAfterRemoveSocketID = {
               ...itemDisconnect, socketID: listSocketIDOfItemDisconnectAfterRemoveSocketDisconnect
           }
           let listUserConnectAfterRemoveSocketDisconnect = [
               ...listItemStillConnect, itemDisconnectAfterRemoveSocketID
           ]

           console.log({
               [`thông tin sau khi disconnect`]: itemDisconnectAfterRemoveSocketID,
               [`số lượng socketID sau khi disconnect`]: itemDisconnectAfterRemoveSocketID && itemDisconnectAfterRemoveSocketID.socketID && itemDisconnectAfterRemoveSocketID.socketID.length
           })

           usersConnectedInstance.setUsersOnline(listUserConnectAfterRemoveSocketDisconnect);
        })
    });
};
