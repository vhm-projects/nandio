// KIỂU TÀI KHOẢN
exports.ADMIN_LEVEL = [
    { value: 0, text: 'Editor' },
    { value: 1, text: 'Admin' },
]

// TRẠNG THÁI
exports.ADMIN_STATUS = [
    { value: 0, text: 'Khóa' },
    { value: 1, text: 'Hoạt động' },
]

// KIỂU BÀI VIẾT
exports.POST_TYPE = [
    { value: 0, text: 'Product' },
    { value: 1, text: 'News' },
]

/**
 * MIME Types image
 */
exports.MIME_TYPES_IMAGE = [ 
	'image/jpeg', 
	'image/pjpeg', 
	'image/png', 
	'image/svg+xml'
];

// GIỚI TÍNH
exports.GENDER_TYPE = [
    { value: 0, text: 'Nữ' },
    { value: 1, text: 'Nam' },
    { value: 2, text: 'Khác' },
]

// LOẠI TÀI KHOẢN
exports.CUSTOMER_ACCOUNT_TYPE = [
    { value: 0, text: 'Bình thường' },
    { value: 1, text: 'Google' },
    { value: 2, text: 'Facebook' },
    { value: 3, text: 'Apple' },
]


/**
 * SCREEN KEY
 */
exports.SCREEN_KEY = {
    ImageDealScreen: {
        value: 1, text: 'ImageDealScreen'
    },
    NotificationScreen: {
        value: 2, text: 'NotificationScreen'
    }
}

/**
 * Định nghĩa file collection
 * BIG SALE
 */
 exports.DESCRIPTION_BIG_SALE_COLL = {
    name: "Tên khuyến mãi BigSale",
    description: "Mô tả khuyến mãi",
    image: "Hình ảnh khuyến mãi",
    typeDiscount: "Loại giảm giá [1, 2] => ['Giảm theo tiền', 'Giảm theo %']",
    code: "Mã code khuyến mãi",
    maxUsage: "Số lượng mã được sử dụng trong chương trình khuyến mãi này",
    products: "Mảng sản phẩm được sử dụng khuyến mãi",
    amountDiscountByMoney: "Nếu typeDiscount = 1 thì sử dụng trường này",
    amountDiscountByPercent: "Nếu typeDiscount = 2 thì sử dụng trường này",
    status: "[0, 1] => [hết hạn, còn hạng]",
    linkDiscounts: "Là một mảng link được link tới những trang thương mại điện tử khác",
    userCreate: "User tạo khuyến mãi",
    userUpdate: "User update khuyến mãi",
    createAt: "Ngày tạo khuyến mãi"
};