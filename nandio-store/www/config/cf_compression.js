const sharp             = require('sharp');
const fs                = require('fs');
const uuidv4            = require('uuid').v4;
const { getExtension }  = require('../utils/utils');

module.exports.compress = function (files){
    const request = require('request').defaults({ encoding: null });
    const uri = 'https://i.pinimg.com/originals/eb/bb/9c/ebbb9c6067fd1f30c0c1b5261833e051.jpg';

    request.get(uri, async function (err, _, body) {
        if(err) return res.json(err);

        fs.access("./uploads", (error) => {
            if (error) {
                fs.mkdirSync("./uploads");
            }
        });

        const extension = getExtension(uri);
        const fileName = `${uuidv4()}.${extension}`;

        const metadata = await sharp(body).metadata();
        const compress = await sharp(body)
            .webp({ quality: 80 })
            .toFile(`./uploads/${fileName}`);

        console.log({ metadata, compress });
    });
}