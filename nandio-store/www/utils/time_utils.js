"use strict";

let moment = require('moment-timezone');
let timeConf = require('../config/cf_time');
let ISOdate = require('isodate');

function setTimeZone(date) {
    return moment.tz(date, timeConf._default_tme_zone);
}
exports.getCurrentTime = function () {
    return ISOdate(new Date());
    // return setTimeZone(new Date()).utcOffset(7).format('YYYY-MM-DDTHH:mm:ss');
    // .format('Y-MM-DD H:m:sZ');
};

exports.parseTimeUTC = (datetime = new Date(), format = false) => {
    if(format){
        return setTimeZone(datetime).utcOffset(7).format('YYYY-MM-DDTHH:mm:ss');
    }

    return setTimeZone(datetime).utcOffset(7);
};

exports.parseFormat1 = function (oldTimeFormat) {
    return setTimeZone(oldTimeFormat).format('Y-MM-DD | H:m');
};

exports.parseFormat2 = function (oldTimeFormat) {
    return setTimeZone(oldTimeFormat).format('H:mm DD-MM-Y');
};

exports.parseFormat3 = function (oldTimeFormat) {
    return setTimeZone(oldTimeFormat).format('MM-DD-Y');
};

exports.parseTimeFormat4 = function (oldTimeFormat) {
    return setTimeZone(oldTimeFormat).format('HH:mm DD/MM/Y');
};

exports.parseTimeFormat5 = function (oldTimeFormat) {
    return setTimeZone(oldTimeFormat).format('DD/MM/Y');
};

exports.parseTimeFormatOption = function (oldTimeFormat, format) {
    return setTimeZone(oldTimeFormat).format(format);
};

exports.isValidDate = (d) => {
    return d instanceof Date && !isNaN(d);
}


/**
 * compare time
 * if time1 > time2: return 1
 * if time1 < time2: return 2
 * if time1 = time2: return 0
 * @param time1
 * @param time2
 * @returns {number}
 */
exports.compareTwoTime = function (time1, time2) {
    let a = (new Date(time1)).getTime();
    let b = (new Date(time2)).getTime();
    if (a > b) {
        return 1;
    } else if (b > a) {
        return 2;
    } else {
        return 0;
    }
};

exports.getTimeBetween = function (time1, time2) {
    let a = (new Date(time1)).getTime();
    let b = (new Date(time2)).getTime();
    return (a - b) / (1000);
};

exports.addMinutesToDate = function (dateAdded, minutes) {
    return new Date((new Date(dateAdded)).getTime() + minutes * 60000);
};

exports.subMinutesToDate = function (subAdded, minutes) {
    return new Date((new Date(subAdded)).getTime() - minutes * 60000);
};

exports.addMinuteToDate = function (dateAdded, minute) {
    return new Date((new Date(dateAdded)).getTime() + minute * 60000);
};

exports.betweenTwoDateResultMinute = (today, endDate) => {
    // ref: https://www.codegrepper.com/code-examples/javascript/get+minutes+between+two+dates+in+javascript
    const minutes = parseInt(Math.abs(endDate.getTime() - today.getTime()) / (1000 * 60) % 60);
    return minutes;
}

exports.betweenTwoDateResultSeconds = (today, endDate) => {
    // ref: https://www.codegrepper.com/code-examples/javascript/get+minutes+between+two+dates+in+javascript
    const seconds = parseInt(Math.abs(endDate.getTime() - today.getTime()) / (1000) % 60); 
    return seconds;
}

exports.addHoursToDate = function (dateAdded, hours) {
    return new Date((new Date(dateAdded)).getTime() + (hours * 60 * 60000));
};

exports.setHours = (date, h = 0, m = 0, s = 0) => {
    date = new Date(date);
    date.setHours(h,m,s); // Set hours, minutes and seconds
    return date;
 }

exports.calculateExpire = (currentTimestamp, expireTimestamp, type = 'minutes') => {
    const currentDate = new Date(currentTimestamp);
    const expireDate  = new Date(expireTimestamp);

    // Get Seconds
    const secondsCurrent = currentDate.getSeconds();
    const secondsExpired = expireDate.getSeconds();

    // Get Minutes
    const minutesCurrent = currentDate.getMinutes();
    const minutesExpired = expireDate.getMinutes();

    // Get Hours
    const hoursCurrent = currentDate.getHours();
    const hoursExpired = expireDate.getHours();

    // Get date
    const dateCurrent = currentDate.getDate();
    const dateExpired = expireDate.getDate();

    // Get month
    const monthCurrent = currentDate.getMonth();
    const monthExpired = expireDate.getMonth();

    // Get year
    const yearCurrent = currentDate.getFullYear();
    const yearExpired = expireDate.getFullYear();

    const current = moment([yearCurrent, monthCurrent, dateCurrent, hoursCurrent, minutesCurrent, secondsCurrent]);
    const expire = moment([yearExpired, monthExpired, dateExpired, hoursExpired, minutesExpired, secondsExpired]);

    return expire.diff(current, type);
}

exports.calculateExpireTime = (currentTimestamp, expireTimestamp, type = 'minutes') => {
    const currentDate = new Date(currentTimestamp);
    const expireDate  = new Date(expireTimestamp);

    // Get Hours
    const minutesCurrent = currentDate.getMinutes();
    const minutesExpire = expireDate.getMinutes();

    // Get Hours
    const hoursCurrent = currentDate.getHours();
    const hoursExpire = expireDate.getHours();

    // Get date
    const dateCurrent = currentDate.getDate();

    // Get month
    const monthCurrent = currentDate.getMonth();

    // Get year
    const yearCurrent = currentDate.getFullYear();

    const current = moment([yearCurrent, monthCurrent, dateCurrent, hoursCurrent, minutesCurrent]);
    const expire = moment([yearCurrent, monthCurrent, dateCurrent, hoursExpire, minutesExpire]);

    return expire.diff(current, type);
}

// Hàm trừ ngày
exports.subDate = function (dates) {
    let d = new Date();
    d.setDate(d.getDate() - dates);
    return d;
};

// Hàm cộng ngày
exports.addDate = function (dates) {
    let d = new Date();
    d.setDate(d.getDate() + dates);
    return d;
};

exports.setFormatDDMMYYYYtoMMDDYYYY = (date, separator = '/') => {
    const [day, month, year] = date.split('/');
    return month + separator + day + separator + year;
};

exports.checkDateValid = (d) => {
    if (Object.prototype.toString.call(d) === "[object Date]") {
        // it is a date
        if (isNaN(d.getTime())) {  // d.valueOf() could also work
          // date is not valid
          return false;
        } else {
          // date is valid
          return true;
        }
      } else {
        // not a date
        return false;
      }
}

exports.getTimeBeforeMinutes = function (timeBefore, minute) { 
    let timeAfterMinusMinute = new Date((new Date(timeBefore)).getTime() - minute * 60000);
    var datestringFormat = timeAfterMinusMinute.getFullYear() + '-' + ("0"+(timeAfterMinusMinute.getMonth()+1)).slice(-2) + '-' + ("0" + timeAfterMinusMinute.getDate()).slice(-2) + " " + ("0" + timeAfterMinusMinute.getHours()).slice(-2) + ":" + ("0" + timeAfterMinusMinute.getMinutes()).slice(-2);

    return datestringFormat;
};
