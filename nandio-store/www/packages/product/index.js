const PRODUCT_COLL = require('./databases/product-coll');
const PRODUCT_MODEL = require('./models/product').MODEL;
const PRODUCT_ROUTES = require('./apis/product');
// MARK REQUIRE

module.exports = {
    PRODUCT_COLL,
    PRODUCT_MODEL,
    PRODUCT_ROUTES,
    // MARK EXPORT
}