"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const beautifyer = require('js-beautify').js_beautify;
const fs = require('fs');
const moment = require('moment');
const logger = require('../../../config/logger/winston.config');
const chalk = require('chalk');
const log = console.log;
const request = require('request');
const TOKEN_STORE = process.env.TOKEN_STORE;
const NANDIO_SHOP_DOMAIN = process.env.NANDIO_SHOP_DOMAIN || 'http://localhost:5001';

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    ISSELLING_PRODUCT_TYPE,
    ISGIFT_PRODUCT_TYPE,
    ISTRIALPROGRAM_PRODUCT_TYPE,
    STATUS_PRODUCT_TYPE,
} = require('../constants/product');
const {
    CF_ROUTINGS_PRODUCT
} = require('../constants/product/product.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const PRODUCT_MODEL = require('../models/product').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */
const CRITERIA_COLL         = require('../../campaign_criteria/databases/criteria-coll');
const PRODUCT_CRITERIA_COLL = require('../../campaign_criteria/databases/products_criteria-coll');

const {
    BRAND_COLL
} = require('../../brand');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ PRODUCT  ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: Insert Product (API, VIEW)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.ADD_PRODUCT]: {
                config: {
                    scopes: ['create:product'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Product',
                    code: CF_ROUTINGS_PRODUCT.ADD_PRODUCT,
                    inc: path.resolve(__dirname, '../views/product/add_product.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listBrands = await BRAND_COLL.find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1
                            }).lean()

                        ChildRouter.renderToView(req, res, {
                            listBrands,
                            CF_ROUTINGS_PRODUCT
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            productCode,
                            name,
                            SKU,
                            price,
                            brand,
                            isSelling,
                            isGift,
                            isTrialProgram,
                        } = req.body;


                        let infoAfterInsert = await PRODUCT_MODEL.insert({
                            productCode,
                            name,
                            SKU,
                            price,
                            brand,
                            isSelling,
                            isGift,
                            isTrialProgram,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Product By Id (API, VIEW)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.UPDATE_PRODUCT_BY_ID]: {
                config: {
                    scopes: ['update:product'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Product',
                    code: CF_ROUTINGS_PRODUCT.UPDATE_PRODUCT_BY_ID,
                    inc: path.resolve(__dirname, '../views/product/update_product.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            productID
                        } = req.query;

                        let infoProduct = await PRODUCT_MODEL.getInfoById(productID);
                        if (infoProduct.error) {
                            return res.redirect('/something-went-wrong');
                        }

                        let listBrands = await BRAND_COLL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();
                        
                        let listCriterias = await CRITERIA_COLL.find({  
                            state: 1,
                            status: 1 
                        });
                        
                        let variants = [
                            { value: "Y5%", text: "Y5%" },
                            { value: "Y6%", text: "Y6%" },
                            { value: "Y8%", text: "Y8%" },
                            { value: "Y9%", text: "Y9%" },
                            { value: "Y15%", text: "Y15%" },
                            { value: "Y21%", text: "Y21%" },
                            { value: "DIAMOND", text: "Diamond" },
                            { value: "GOLD", text: "Gold" },
                            { value: "PLATINUM", text: "Platinum" },
                            { value: "TRAI_CAY_VI_LUU", text: "Trái cây vị lựu" },
                            { value: "TRAI_CAY_VIET_QUAT", text: "Trái cây việt quất" }
                        ]
                    
                        let specifications = [
                            { value: "HP", text: "HP" },
                            { value: "LC", text: "LC" },
                            { value: "LO", text: "LO" },
                            { value: "TUI", text: "TUI"},
                        ]
                        // Lấy danh sách tiêu chí đã được áp dụng cho sản phẩm
                        let listProductCriterias = await PRODUCT_CRITERIA_COLL.find({ product: productID }, { criteria: 1, _id: 0 });
                        listProductCriterias = listProductCriterias.map(item => item.criteria &&  item.criteria.toString());
                        console.log({ listProductCriterias });
                        ChildRouter.renderToView(req, res, {
                            infoProduct: infoProduct.data || {},
                            listCriterias,
                            listBrands,
                            listProductCriterias,
                            CF_ROUTINGS_PRODUCT,
                            variants,
                            specifications
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            productID,
                            productCode,
                            name,
                            SKU,
                            price,
                            brand,
                            isSelling,
                            isGift,
                            isTrialProgram,
                            status,
                            criterias,
                            specifications,
                            variants
                        } = req.body;

                        const infoAfterUpdate = await PRODUCT_MODEL.update({
                            productID,
                            productCode,
                            name,
                            SKU,
                            price,
                            brand,
                            isSelling,
                            isGift,
                            isTrialProgram,
                            status,
                            criterias,
                            userUpdate,
                            specifications,
                            variants
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Product By Id (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.UPDATE_PRODUCT_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:product'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            productID,
                            productCode,
                            name,
                            SKU,
                            price,
                            brand,
                            isSelling,
                            isGift,
                            isTrialProgram,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await PRODUCT_MODEL.updateNotRequire({
                            productID,
                            productCode,
                            name,
                            SKU,
                            price,
                            brand,
                            isSelling,
                            isGift,
                            isTrialProgram,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Product By Id (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.DELETE_PRODUCT_BY_ID]: {
                config: {
                    scopes: ['delete:product'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            productID
                        } = req.params;

                        const infoAfterDelete = await PRODUCT_MODEL.deleteById(productID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Product By List Id (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.DELETE_PRODUCT_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:product'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            productID
                        } = req.body;

                        const infoAfterDelete = await PRODUCT_MODEL.deleteByListId(productID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Product By Id (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.GET_INFO_PRODUCT_BY_ID]: {
                config: {
                    scopes: ['read:info_product'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            productID
                        } = req.params;

                        const infoProductById = await PRODUCT_MODEL.getInfoById(productID);
                        res.json(infoProductById);
                    }]
                },
            },

            /**
             * Function: Get List Product (API, VIEW)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.GET_LIST_PRODUCT]: {
                config: {
                    scopes: ['read:list_product'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách sản phẩm',
                    code: CF_ROUTINGS_PRODUCT.GET_LIST_PRODUCT,
                    inc: path.resolve(__dirname, '../views/product/list_products.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            priceFromNumber,
                            priceToNumber,
                            isSelling,
                            isGift,
                            isTrialProgram,
                            status,
                            typeGetList
                        } = req.query;

                        let listProducts = [];
                        if (typeGetList === 'FILTER') {
                            listProducts = await PRODUCT_MODEL.getListByFilter({
                                keyword,
                                priceFromNumber,
                                priceToNumber,
                                isSelling,
                                isGift,
                                isTrialProgram,
                                status,
                            });
                        } else {
                            listProducts = await PRODUCT_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listProducts: listProducts.data || [],
                            ISSELLING_PRODUCT_TYPE,
                            ISGIFT_PRODUCT_TYPE,
                            ISTRIALPROGRAM_PRODUCT_TYPE,
                            STATUS_PRODUCT_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Product By Field (API, VIEW)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.GET_LIST_PRODUCT_BY_FIELD]: {
                config: {
                    scopes: ['read:list_product'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Product by field isStatus',
                    code: CF_ROUTINGS_PRODUCT.GET_LIST_PRODUCT_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/product/list_products.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            priceFromNumber,
                            priceToNumber,
                            isSelling,
                            isGift,
                            isTrialProgram,
                            status,
                            type
                        } = req.query;

                        let listProducts = await PRODUCT_MODEL.getListByFilter({
                            keyword,
                            priceFromNumber,
                            priceToNumber,
                            isSelling,
                            isGift,
                            isTrialProgram,
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listProducts: listProducts.data || [],
                            ISSELLING_PRODUCT_TYPE,
                            ISGIFT_PRODUCT_TYPE,
                            ISTRIALPROGRAM_PRODUCT_TYPE,
                            STATUS_PRODUCT_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Product Server Side (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.GET_LIST_PRODUCT_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_product'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listProductServerSide = await PRODUCT_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listProductServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Product Import (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.GET_LIST_PRODUCT_IMPORT]: {
                config: {
                    scopes: ['read:list_product'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listProductImport = await PRODUCT_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listProductImport);
                    }]
                },
            },

            /**
             * Function: Get List Product Excel Server Side (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.GET_LIST_PRODUCT_EXCEL]: {
                config: {
                    scopes: ['read:list_product'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = PRODUCT_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Product Excel Export (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.DOWNLOAD_LIST_PRODUCT_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_product'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'limit_kpi_config'
                        });

                        let conditionObj = PRODUCT_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listProduct = await PRODUCT_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listProduct)
                    }]
                },
            },

            /**
             * Function: Setting Product Excel Import (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.SETTING_FILE_PRODUCT_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_product'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = PRODUCT_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Product Excel Import (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.DOWNLOAD_FILE_PRODUCT_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_product'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'product'
                        });

                        let listProductImport = await PRODUCT_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listProductImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listProductImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Product Excel Import (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.CREATE_PRODUCT_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:product'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'product'
                        });

                        let infoProductAfterImport = await PRODUCT_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'product',
                        });

                        res.json(infoProductAfterImport);
                    }]
                },
            },

            /**
             * Function: API Get info Product
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.API_GET_INFO_PRODUCT]: {
                config: {
                    scopes: ['read:info_product'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            productID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoProduct = await PRODUCT_MODEL.getInfoProduct({
                            productID,
                            select,
                            filter,
                            explain
                        });
                        res.json(infoProduct);
                    }]
                },
            },

            /**
             * Function: API Get info Product
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.API_GET_LIST_PRODUCTS]: {
                config: {
                    scopes: ['read:list_product'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listProducts = await PRODUCT_MODEL.getListProducts({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listProducts);
                    }]
                },
            },

            /**
             * Function: TÍCH ĐIỂM QR => GỌI SERVER NANDIO SHOP
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.API_ACCUMULATE_PRODUCT_QR]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const { customerID, code } = req.query;
                        
                        var options = {
                            'method': 'GET',
                            'url': `${NANDIO_SHOP_DOMAIN}/api/product-qr/info-product-qr/${code}?TOKEN=${TOKEN_STORE}&customerID=${customerID}`,
                            'headers': {
                                'user-agent': 'PostmanRuntime/7.28.4',
                                'postman_test_sandbox': 'ldk_postman'
                            },
                            form: {
                                
                            }
                        };
                        request(options, function (error, response) {
                            if (error) throw new Error(error);
                            return res.json(JSON.parse(response.body));
                        });

                    }]
                },
            },

            /**
             * Function: LẤY THÔNG TIN TRUY XUẤT NGUỒN GỐC QR => GỌI SERVER NANDIO SHOP
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.INFO_PRODUCT_QR]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const { customerID, code } = req.query;

                        var options = {
                            'method': 'GET',
                            'url': `${NANDIO_SHOP_DOMAIN}/api/product-qr/detail-product-qr/${code}?TOKEN=${TOKEN_STORE}&customerID=${customerID}`,
                            'headers': {
                                'user-agent': 'PostmanRuntime/7.28.4',
                                'postman_test_sandbox': 'ldk_postman'
                            },
                            form: {
                            }
                        };
                        request(options, function (error, response) {
                            if (error) throw new Error(error);
                            return res.json(JSON.parse(response.body));
                        });

                    }]
                },
            },

            /**
             * Function: CHECK VARIANT AND SPECIFICATION
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCT.CHECK_VARIANT_AND_SPECIFICATION]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const { variants, specification } = req.body;
                        const data = await PRODUCT_MODEL.checkVariantAndSpecification({
                            variants, specification
                        });
                        res.json(data);
                    }]
                },
            },
        }
    }
};