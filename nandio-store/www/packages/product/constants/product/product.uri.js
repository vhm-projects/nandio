const BASE_ROUTE = '/product';
const API_BASE_ROUTE = '/api/product';

const CF_ROUTINGS_PRODUCT = {
    ADD_PRODUCT: `${BASE_ROUTE}/add-product`,
    UPDATE_PRODUCT_BY_ID: `${BASE_ROUTE}/update-product-by-id`,
    DELETE_PRODUCT_BY_ID: `${BASE_ROUTE}/delete/:productID`,

    GET_INFO_PRODUCT_BY_ID: `${BASE_ROUTE}/info/:productID`,
    GET_LIST_PRODUCT: `${BASE_ROUTE}/list-product`,
    GET_LIST_PRODUCT_BY_FIELD: `${BASE_ROUTE}/list-product/:field/:value`,
    GET_LIST_PRODUCT_SERVER_SIDE: `${BASE_ROUTE}/list-product-server-side`,

    UPDATE_PRODUCT_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-product-by-id-v2`,
    DELETE_PRODUCT_BY_LIST_ID: `${BASE_ROUTE}/delete-product-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_PRODUCT_EXCEL: `${BASE_ROUTE}/list-product-excel`,
    DOWNLOAD_LIST_PRODUCT_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-product-excel-export`,

    // IMPORT EXCEL
    GET_LIST_PRODUCT_IMPORT: `${BASE_ROUTE}/list-product-import`,
    SETTING_FILE_PRODUCT_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-product-import-setting`,
    DOWNLOAD_FILE_PRODUCT_EXCEL_IMPORT: `${BASE_ROUTE}/list-product-import-dowload`,
    CREATE_PRODUCT_IMPORT_EXCEL: `${BASE_ROUTE}/create-product-import-excel`,

    API_GET_INFO_PRODUCT: `${API_BASE_ROUTE}/info-product/:productID`,
    API_GET_LIST_PRODUCTS: `${API_BASE_ROUTE}/list-products`,

    // QUÉT QR || TÍCH ĐIỂM
    API_ACCUMULATE_PRODUCT_QR: `${API_BASE_ROUTE}/accumlate-product-qr`,
    INFO_PRODUCT_QR:           `${API_BASE_ROUTE}/info-product-qr`,
    CHECK_VARIANT_AND_SPECIFICATION: `${API_BASE_ROUTE}/check-variant-and-specification`,

    ORIGIN_APP: BASE_ROUTE,

}

exports.CF_ROUTINGS_PRODUCT = CF_ROUTINGS_PRODUCT;