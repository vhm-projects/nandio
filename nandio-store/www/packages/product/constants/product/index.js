/**
 * Hàng Bán 
 * 1: Có
 * 2: Không
 */
exports.ISSELLING_PRODUCT_TYPE = {

    "1": {
        value: "Có",
        color: "#0b51b7"
    },

    "2": {
        value: "Không",
        color: "#d63031"
    },

};

/**
 * Hàng Tặng 
 * 1: Có
 * 2: Không
 */
exports.ISGIFT_PRODUCT_TYPE = {

    "1": {
        value: "Có",
        color: "#0b51b7"
    },

    "2": {
        value: "Không",
        color: "#d63031"
    },

};

/**
 * Hàng dùng thử 
 * 1: Có
 * 2: Không
 */
exports.ISTRIALPROGRAM_PRODUCT_TYPE = {

    "1": {
        value: "Có",
        color: "#0b51b7"
    },

    "2": {
        value: "Không",
        color: "#d63031"
    },

};

/**
 * Trạng thái 
 * 1: Hoạt động
 * 2: Không hoạt động
 */
exports.STATUS_PRODUCT_TYPE = {

    "1": {
        value: "Hoạt động",
        color: "#0b51b7"
    },

    "2": {
        value: "Không hoạt động",
        color: "#d63031"
    },

};