"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('product', {
    
    /**
     * Mã sản phẩm
     */
    productCode: {
        type: String,
        unique: true,
        required: true,
    },
    /**
     * Tên sản phẩm
     */
    name: {
        type: String,
        required: true,
    },
    /**
     * SKU
     */
    SKU: {
        type: String,
        required: true,
    },
    /**
     * Đơn giá
     */
    price: {
        type: Number,
        required: true,
    },
    /**
	 * Giá sản phẩm QSTORE (giá này là p0 của DMS = giá sản phẩm * 8% VAT)
	 */
	priceOfQStore: {
		type: Number,
	},
    /**
     * Thương hiệu
     */
    brand: {
        type: Schema.Types.ObjectId,
        ref: 'brand',
    },
    /**
     * Hàng Bán 
     * 1: Có,
     * 2: Không
     */
    isSelling: {
        type: Number,
        enum: [1, 2],
    },
    /**
     * Hàng Tặng 
     * 1: Có,
     * 2: Không
     */
    isGift: {
        type: Number,
        enum: [1, 2],
    },
    /**
     * Hàng dùng thử 
     * 1: Có,
     * 2: Không
     */
    isTrialProgram: {
        type: Number,
        enum: [1, 2],
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },

    //=================================================================================
    //============================== DỮ LIỆU NANDIO SHOP ==============================
    //=================================================================================

    /**
	 * Danh mục sản phẩm
	 */
	category: {
		type: Schema.Types.ObjectId,
		ref: 'category',
	},

    sku_2: {
		type: Schema.Types.ObjectId,
		ref: 'sku_2',
	},

    description: {
		type: String,
	},

    /**
	 * Trạng thái hoạt động.
	 * 1. Quét
	 * 2. Không quét
	 */
	show_qr: {
		type: Number,
		default: 2
	},

    /**
	 * Hình ảnh đại diện
	 */
	avatar: {
		type: Schema.Types.ObjectId,
		ref: 'image',
	},

    /**
	 * Hình ảnh 
	 */
	images: [{
		type: Schema.Types.ObjectId,
		ref: 'image',
	}],

    /**
	 * Thông tin website
	 */
	website: String,
	/**
	 * Nguồn nguyên liệu
	 */
	sourceMaterial: String,
	/**
	 * Sản xuất bởi
	 */
	madeBy: String,
	/**
	 * Nhập khẩu bởi
	 */
	importedBy: String,
	/**
	 * Phân phối bởi
	 */
	distributor: String,
	/**
	 * Ngày sản xuất
	 */
	manufactureDate: Date,
	/**
	* Ngày hết hạn
	*/
	expiredTime: Date,
	/**
	 * Hình ảnh chứng nhận
	 */
	certifiedImages: [
		{
			type: Schema.Types.ObjectId,
			ref: 'image',
		}
	],

    // Dữ liệu bổ sung 9/5/2022
    // Quy cách
    /**
     *  HP
     *  LC
     *  LO
     *  TUI
     */
    specification: String,

    /**
     * Y5%
     * Y6%
     * Y8%
     * Y9%
     * Y15%
     * Y20%
     * Diamond
     * Gold
     * Platinum
     * Trái cây vị lựu
     * Trái cây việt quất
     */
    variants: [String],
});