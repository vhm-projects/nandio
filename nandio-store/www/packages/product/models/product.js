"use strict";

/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const path = require('path');
const fs = require('fs');
const {
    hash,
    hashSync,
    compare
} = require('bcryptjs');
const XlsxPopulate = require('xlsx-populate');
const mongodb = require("mongodb");
const {
    MongoClient
} = mongodb;
const formatCurrency = require('number-format.js');

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    renderOptionFilter,
    colName,
    checkNumberIsValidWithRange
} = require('../../../utils/utils');
const {
    isTrue
} = require('../../../tools/module/check');
const {
    randomStringFixLength
} = require('../../../utils/string_utils');

const {
    ISSELLING_PRODUCT_TYPE,
    ISGIFT_PRODUCT_TYPE,
    ISTRIALPROGRAM_PRODUCT_TYPE,
    STATUS_PRODUCT_TYPE,
} = require('../constants/product');


/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const URL_DATABASE = process.env.URL_DATABASE || 'mongodb://localhost:27017';
const NAME_DATABASE = process.env.NAME_DATABASE || 'ldk_tools_op';

/**
 * COLLECTIONS
 */
const PRODUCT_COLL          = require('../databases/product-coll');
const PRODUCT_CRITERIA_COLL = require('../../campaign_criteria/databases/products_criteria-coll');

const {
    BRAND_COLL
} = require('../../brand');
const { response } = require('express');

/**
 * MODELS
 */
const BIG_SALE_MODEL                = require('../../promotion/models/big_sale').MODEL;
const FLASH_SALE_MODEL              = require('../../promotion/models/flash_sale').MODEL;
const PRODUCT_CRITERIA_MODEL        = require('../../campaign_criteria/models/products_criteria').MODEL;

class Model extends BaseModel {
    constructor() {
        super(PRODUCT_COLL);
    }

    /**
         * Tạo mới product
		* @param {string} productCode
		* @param {string} name
		* @param {string} SKU
		* @param {number} price
		* @param {object} brand
		* @param {number} isSelling
		* @param {number} isGift
		* @param {number} isTrialProgram

         * @param {objectId} userCreate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        productCode,
        name,
        SKU,
        price,
        brand,
        isSelling,
        isGift,
        isTrialProgram,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (productCode.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mã sản phẩm không được lớn hơn 125 ký tự'
                    });
                }

                if (!productCode) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mã sản phẩm'
                    });
                }

                if (name.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tên sản phẩm không được lớn hơn 125 ký tự'
                    });
                }

                if (!name) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tên sản phẩm'
                    });
                }

                if (SKU.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài sku không được lớn hơn 125 ký tự'
                    });
                }

                if (!SKU) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập sku'
                    });
                }

                if (!price) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập đơn giá'
                    });
                }


                if (isSelling && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isSelling
                    })) {
                    return resolve({
                        error: true,
                        message: 'hàng bán không hợp lệ'
                    });
                }

                if (isGift && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isGift
                    })) {
                    return resolve({
                        error: true,
                        message: 'hàng tặng không hợp lệ'
                    });
                }

                if (isTrialProgram && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isTrialProgram
                    })) {
                    return resolve({
                        error: true,
                        message: 'hàng dùng thử không hợp lệ'
                    });
                }




                if (!checkObjectIDs([brand])) {
                    return resolve({
                        error: true,
                        message: 'thương hiệu không hợp lệ'
                    });
                }


                const checkProductCodeExits = await PRODUCT_COLL.findOne({
                    productCode
                });
                if (checkProductCodeExits) {
                    return resolve({
                        error: true,
                        message: 'Mã SảN PhẩM đã tồn tại'
                    });
                }

                let dataInsert = {
                    productCode,
                    name,
                    SKU,
                    price,
                    brand,
                    isSelling,
                    isGift,
                    isTrialProgram,
                    userCreate
                };


                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo sản phẩm thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật product 
         * @param {objectId} productID
		* @param {string} productCode
		* @param {string} name
		* @param {string} SKU
		* @param {number} price
		* @param {object} brand
		* @param {number} isSelling
		* @param {number} isGift
		* @param {number} isTrialProgram
		* @param {number} status

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        productID,
        productCode,
        name,
        SKU,
        price,
        brand,
        isSelling,
        isGift,
        isTrialProgram,
        status,
        criterias,
        userUpdate,
        specifications,
        variants
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([productID])) {
                    return resolve({
                        error: true,
                        message: 'productID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (productCode.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mã sản phẩm không được lớn hơn 125 ký tự'
                    });
                }

                if (!productCode) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mã sản phẩm cho product'
                    });
                }

                if (name.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tên sản phẩm không được lớn hơn 125 ký tự'
                    });
                }

                if (!name) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tên sản phẩm cho product'
                    });
                }

                if (SKU.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài sku không được lớn hơn 125 ký tự'
                    });
                }

                if (!SKU) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập sku cho product'
                    });
                }

                if (!price) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập đơn giá cho product'
                    });
                }


                if (isSelling && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isSelling
                    })) {
                    return resolve({
                        error: true,
                        message: 'hàng bán không hợp lệ'
                    });
                }

                if (isGift && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isGift
                    })) {
                    return resolve({
                        error: true,
                        message: 'hàng tặng không hợp lệ'
                    });
                }

                if (isTrialProgram && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isTrialProgram
                    })) {
                    return resolve({
                        error: true,
                        message: 'hàng dùng thử không hợp lệ'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trạng thái không hợp lệ'
                    });
                }


                if (!checkObjectIDs([brand])) {
                    return resolve({
                        error: true,
                        message: 'thương hiệu không hợp lệ'
                    });
                }

                const checkExists = await PRODUCT_COLL.findById(productID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'sản phẩm không tồn tại'
                    });
                }

                const checkProductCodeExits = await PRODUCT_COLL.findOne({
                    productCode
                });

                if (checkProductCodeExits && checkExists.productCode !== productCode) {
                    return resolve({
                        error: true,
                        message: 'mã sản phẩm đã tồn tại'
                    });
                }


                let dataUpdate = {
                    userUpdate
                };
                productCode && (dataUpdate.productCode = productCode);
                name && (dataUpdate.name = name);
                SKU && (dataUpdate.SKU = SKU);
                price && (dataUpdate.price = price);
                brand && (dataUpdate.brand = brand);
                dataUpdate.isSelling = isSelling;
                dataUpdate.isGift = isGift;
                dataUpdate.isTrialProgram = isTrialProgram;
                dataUpdate.status = status;

                if(specifications){
                    dataUpdate.specification = specifications;
                }

                if(variants){
                    dataUpdate.variants = variants;
                }

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: productID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }
                
                await PRODUCT_CRITERIA_COLL.findOneAndDelete({ product: productID });
                if(criterias){
                    for (const criteria of criterias) {
                        let infoCriteriaProduct = await PRODUCT_CRITERIA_MODEL.insert({
                            product: productID, criteria, status: 1
                        });
                    }
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật product (không bắt buộc)
         * @param {objectId} productID
		* @param {string} productCode
		* @param {string} name
		* @param {string} SKU
		* @param {number} price
		* @param {object} brand
		* @param {number} isSelling
		* @param {number} isGift
		* @param {number} isTrialProgram
		* @param {number} status

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        productID,
        productCode,
        name,
        SKU,
        price,
        brand,
        isSelling,
        isGift,
        isTrialProgram,
        status,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([productID])) {
                    return resolve({
                        error: true,
                        message: 'productID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (brand && !checkObjectIDs([brand])) {
                    return resolve({
                        error: true,
                        message: 'thương hiệu không hợp lệ'
                    });
                }

                if (isSelling && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isSelling
                    })) {
                    return resolve({
                        error: true,
                        message: 'hàng bán không hợp lệ'
                    });
                }

                if (isGift && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isGift
                    })) {
                    return resolve({
                        error: true,
                        message: 'hàng tặng không hợp lệ'
                    });
                }

                if (isTrialProgram && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isTrialProgram
                    })) {
                    return resolve({
                        error: true,
                        message: 'hàng dùng thử không hợp lệ'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trạng thái không hợp lệ'
                    });
                }

                const checkExists = await PRODUCT_COLL.findById(productID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'sản phẩm không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                productCode && (dataUpdate.productCode = productCode);
                name && (dataUpdate.name = name);
                SKU && (dataUpdate.SKU = SKU);
                price && (dataUpdate.price = price);
                brand && (dataUpdate.brand = brand);
                isSelling && (dataUpdate.isSelling = isSelling);
                isGift && (dataUpdate.isGift = isGift);
                isTrialProgram && (dataUpdate.isTrialProgram = isTrialProgram);
                status && (dataUpdate.status = status);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: productID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa product 
     * @param {objectId} productID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(productID) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 2
                };
                if (!checkObjectIDs([productID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị productID không hợp lệ'
                    });
                }


                const infoAfterDelete = await this.updateById(productID, conditionObj);

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa product 
     * @param {array} productID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(productID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(productID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị productID không hợp lệ'
                    });
                }

                const infoAfterDelete = await PRODUCT_COLL.deleteMany({
                    _id: {
                        $in: productID
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin product 
     * @param {objectId} productID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(productID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([productID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị productID không hợp lệ'
                    });
                }

                const infoProduct = await PRODUCT_COLL.findById(productID)
                    .populate('brand')

                if (!infoProduct) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin sản phẩm'
                    });
                }

                return resolve({
                    error: false,
                    data: infoProduct
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách product 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listProduct = await PRODUCT_COLL
                    .find({
                        state: 1
                    }).populate('brand')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listProduct) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách sản phẩm'
                    });
                }

                return resolve({
                    error: false,
                    data: listProduct
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách product theo bộ lọc
		* @param {string} keyword
		* @param {number} priceFromNumber
		* @param {number} priceToNumber
		* @enum {number} isSelling
		* @enum {number} isGift
		* @enum {number} isTrialProgram
		* @enum {number} status

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        keyword,
        priceFromNumber,
        priceToNumber,
        isSelling,
        isGift,
        isTrialProgram,
        status,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        productCode: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        name: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        SKU: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                if (priceFromNumber && priceToNumber) {
                    conditionObj.price = {
                        $gte: priceFromNumber,
                        $lt: priceToNumber,
                    };
                }

                isSelling && (conditionObj.isSelling = isSelling);

                isGift && (conditionObj.isGift = isGift);

                isTrialProgram && (conditionObj.isTrialProgram = isTrialProgram);

                status && (conditionObj.status = status);


                const listProductByFilter = await PRODUCT_COLL
                    .find(conditionObj).populate('brand')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listProductByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách sản phẩm"
                    });
                }

                return resolve({
                    error: false,
                    data: listProductByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách product theo bộ lọc (server side)
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        productCode: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        name: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        SKU: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }



                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.isSelling) {
                        if (![1, 2].includes(Number(objFilterStatic.isSelling)) || Number.isNaN(Number(objFilterStatic.isSelling))) {
                            return resolve({
                                error: true,
                                message: "hàng bán không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            isSelling: Number(objFilterStatic.isSelling)
                        }
                    }

                    if (objFilterStatic.isGift) {
                        if (![1, 2].includes(Number(objFilterStatic.isGift)) || Number.isNaN(Number(objFilterStatic.isGift))) {
                            return resolve({
                                error: true,
                                message: "hàng tặng không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            isGift: Number(objFilterStatic.isGift)
                        }
                    }

                    if (objFilterStatic.isTrialProgram) {
                        if (![1, 2].includes(Number(objFilterStatic.isTrialProgram)) || Number.isNaN(Number(objFilterStatic.isTrialProgram))) {
                            return resolve({
                                error: true,
                                message: "hàng dùng thử không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            isTrialProgram: Number(objFilterStatic.isTrialProgram)
                        }
                    }

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trạng thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }


                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalProduct = await PRODUCT_COLL.countDocuments(conditionObj);

                const listProductByFilter = await PRODUCT_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'brands',
                            localField: 'brand',
                            foreignField: '_id',
                            as: 'brand'
                        }
                    },
                    {
                        $unwind: {
                            path: '$brand',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listProductByFilter) {
                    return resolve({
                        recordsTotal: totalProduct,
                        recordsFiltered: totalProduct,
                        data: []
                    });
                }

                const listProductDataTable = listProductByFilter.map((product, index) => {

                    let isSelling = '';
                    if (product.isSelling == 1) {
                        isSelling = 'checked';
                    }

                    let isGift = '';
                    if (product.isGift == 1) {
                        isGift = 'checked';
                    }

                    let isTrialProgram = '';
                    if (product.isTrialProgram == 1) {
                        isTrialProgram = 'checked';
                    }

                    let status = '';
                    if (product.status == 1) {
                        status = 'checked';
                    }

                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${product._id}" type="checkbox" class="check-record check-record-${product._id}" _index ="${index + 1}"><label for="${product._id}"></label></div></td>`,
                        indexSTT: skip + index + 1,
                        productCode: `<a href="/product/update-product-by-id?productID=${product._id}">${product.productCode && product.productCode.length > 50 ? product.productCode.substr(0,50) + "..." : product.productCode} </a>`,
                        name: `${product.name && product.name.length > 50 ? product.name.substr(0,50) + "..." : product.name}`,
                        price: formatCurrency('###,###.', product.price || 0),
                        brand: product.brand ? product.brand.name : '',
                        isSelling: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-isSelling" _productID="${product._id}" type="checkbox" id="${product._id}" ${isSelling} style="width: 40px;height: 20px;"></div></td>`,
                        isGift: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-isGift" _productID="${product._id}" type="checkbox" id="${product._id}" ${isGift} style="width: 40px;height: 20px;"></div></td>`,
                        isTrialProgram: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-isTrialProgram" _productID="${product._id}" type="checkbox" id="${product._id}" ${isTrialProgram} style="width: 40px;height: 20px;"></div></td>`,
                        status: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-status" _productID="${product._id}" type="checkbox" id="${product._id}" ${status} style="width: 40px;height: 20px;"></div></td>`,
                        createAt: moment(product.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalProduct,
                    recordsFiltered: totalProduct,
                    data: listProductDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách product theo bộ lọc import
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterImport({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        productCode: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        name: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        SKU: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }


                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.isSelling) {
                        if (![1, 2].includes(Number(objFilterStatic.isSelling)) || Number.isNaN(Number(objFilterStatic.isSelling))) {
                            return resolve({
                                error: true,
                                message: "hàng bán không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            isSelling: Number(objFilterStatic.isSelling)
                        }
                    }

                    if (objFilterStatic.isGift) {
                        if (![1, 2].includes(Number(objFilterStatic.isGift)) || Number.isNaN(Number(objFilterStatic.isGift))) {
                            return resolve({
                                error: true,
                                message: "hàng tặng không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            isGift: Number(objFilterStatic.isGift)
                        }
                    }

                    if (objFilterStatic.isTrialProgram) {
                        if (![1, 2].includes(Number(objFilterStatic.isTrialProgram)) || Number.isNaN(Number(objFilterStatic.isTrialProgram))) {
                            return resolve({
                                error: true,
                                message: "hàng dùng thử không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            isTrialProgram: Number(objFilterStatic.isTrialProgram)
                        }
                    }

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trạng thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }


                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalProduct = await PRODUCT_COLL.countDocuments(conditionObj);

                const listProductByFilter = await PRODUCT_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'brands',
                            localField: 'brand',
                            foreignField: '_id',
                            as: 'brand'
                        }
                    },
                    {
                        $unwind: {
                            path: '$brand',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listProductByFilter) {
                    return resolve({
                        recordsTotal: totalProduct,
                        recordsFiltered: totalProduct,
                        data: []
                    });
                }

                return resolve({
                    error: false,
                    recordsTotal: totalProduct,
                    recordsFiltered: totalProduct,
                    data: listProductByFilter || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc product
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc product
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listProductByFilter = await PRODUCT_COLL.aggregate(arrayFilter)

                if (!listProductByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listProductByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listProductByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('HH:mm DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Tải file import excel mẫu product
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    fileImportExcelPreview({
        arrayItemCustomerChoice
    }) {
        return new Promise(async resolve => {
            try {
                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition; // BỘ LỌC && LOẠI IMPORT
                let {
                    listFieldPrimaryKey
                } = condition; // DANH SÁCH PRIMARY KEY

                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        let index = 0;
                        arrayItemCustomerChoice.map((elem) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            if (elem.dataDynamic && elem.dataDynamic.length) {
                                listFieldPrimaryKey = listFieldPrimaryKey.filter(key => key != elem.nameFieldRef); // LỌC PRIMARY KEY MÀ KHÔNG PHẢI REF

                                elem.dataDynamic.map(item => {
                                    workbook.sheet("Report").row(1).cell(index + 1).value(item);
                                    index++;
                                })
                            } else {
                                workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                                index++;
                            }
                        });

                        if (isTrue(condition.checkDownloadDataOld)) { // KIỂM TRA CÓ ĐÍNH ĐÈM DỮ LIỆU CŨ THEO ĐIỀU KIỆN
                            let listItemImport = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].listItemImport; // LIST FIELD ĐÃ ĐƯỢC CẤU HÌNH
                            let {
                                arrayFilter
                            } = this.getConditionArrayFilterExcel(listItemImport, condition.conditionDeleteImport.filter, condition.conditionDeleteImport.condition); // LẤY RA ARRAY ARREGATE

                            let groupByProduct = {};
                            listFieldPrimaryKey.map(key => {
                                groupByProduct = {
                                    ...groupByProduct,
                                    [key]: '$' + key
                                }
                            });

                            arrayFilter = [
                                ...arrayFilter,
                                {
                                    $group: {
                                        _id: {
                                            groupByProduct
                                        },
                                        listData: {
                                            $addToSet: "$$CURRENT"
                                        },
                                    }
                                }
                            ];

                            const listProductByFilter = await PRODUCT_COLL.aggregate(arrayFilter);

                            listProductByFilter && listProductByFilter.length && listProductByFilter.map((item, indexProduct) => {
                                let indexValue = 0;
                                arrayItemCustomerChoice.map((elem, indexChoice) => {
                                    let variable = elem.name.split('.');

                                    if (elem.dataDynamic && elem.dataDynamic.length) { // KIỂM TRA FIELD CÓ CHỌN DYNAMIC
                                        item.listData && item.listData.length && item.listData.map(value => { // ARRAY DATA DYNAMIC
                                            if (item.listData.length > elem.dataDynamic.length) { // KIỂM TRA ĐỘ DÀI CỦA DATA SO VỚI SỐ CỘT
                                                // TODO: XỬ LÝ NHIỀU DYNAMIC

                                            } else {
                                                elem.dataDynamic.map(dynamic => {
                                                    let valueOfField;
                                                    if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                        let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                        if (objDataOfVariable) {
                                                            valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                        }
                                                    } else {
                                                        valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                                    }

                                                    if (valueOfField == dynamic) { // CHECK NẾU VALUE === CỘT
                                                        let valueImportDynamic = value[elem.variableChoice] ? value[elem.variableChoice] : '';

                                                        // INSERT DỮ LIỆU VÀO BẢNG VỚI FIELD ĐƯỢC CHỌN THEO DẠNG DYNAMIC
                                                        workbook.sheet("Report").row(indexProduct + 2).cell(indexValue + 1).value(valueImportDynamic);
                                                        indexValue++;
                                                    } else {
                                                        if (elem.dataDynamic && item.listData && elem.dataDynamic.length > item.listData.length) {
                                                            indexValue++;
                                                        }
                                                    }
                                                })
                                            }
                                        })
                                    } else { // DẠNG STATIC
                                        let valueProduct;
                                        if (variable.length > 1) {
                                            let objDataOfVariable = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                            if (objDataOfVariable) {
                                                valueProduct = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                            }
                                        } else {
                                            valueProduct = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                        }

                                        workbook.sheet("Report").row(indexProduct + 2).cell(indexValue + 1).value(valueProduct);
                                        indexValue++;
                                    }
                                });
                            });
                        }

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    pathWriteFile,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Upload File Excel Import Lưu Dữ Liệu product
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    importExcel({
        nameCollParent,
        arrayItemCustomerChoice,
        file,
    }) {
        return new Promise(async resolve => {
            try {

                XlsxPopulate.fromFileAsync(file.path)
                    .then(async workbook => {
                        let listData = [];
                        let index = 2;
                        let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition;

                        const client = await MongoClient.connect(URL_DATABASE);
                        const db = client.db(NAME_DATABASE)

                        for (; true;) {
                            if (arrayItemCustomerChoice && arrayItemCustomerChoice.length) {
                                let conditionObj = {};

                                let totalLength = 0;
                                arrayItemCustomerChoice.map((item, index) => {
                                    if (item.dataDynamic && item.dataDynamic.length) {
                                        totalLength += item.dataDynamic.length;
                                    } else {
                                        totalLength++;
                                    }
                                });

                                let indexOfListField = 0;
                                let checkIsRequire = false;
                                let arrayConditionObjDynamic = [];

                                for (let i = 0; i < totalLength; i++) {
                                    if (arrayItemCustomerChoice[indexOfListField].dataDynamic && arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                        let indexOfDynamic = 1;
                                        for (let valueDynamic of arrayItemCustomerChoice[indexOfListField].dataDynamic) {
                                            let letter = colName(i);
                                            let indexOfCeil = letter.toUpperCase() + index;

                                            let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                            if (variable) {

                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: valueDynamic.trim()
                                                });

                                                let conditionOfOneValueDynamic = {
                                                    [arrayItemCustomerChoice[indexOfListField].variableChoice]: variable
                                                }
                                                if (docs) {
                                                    conditionOfOneValueDynamic = {
                                                        ...conditionOfOneValueDynamic,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    }
                                                }

                                                arrayConditionObjDynamic = [
                                                    ...arrayConditionObjDynamic,
                                                    conditionOfOneValueDynamic
                                                ];
                                            }

                                            if (indexOfDynamic < arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                                i++;
                                            }
                                            indexOfDynamic++;
                                        }
                                    } else {
                                        let letter = colName(i);
                                        let indexOfCeil = letter.toUpperCase() + index;
                                        let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                        if (arrayItemCustomerChoice[indexOfListField].isRequire && !variable) {
                                            checkIsRequire = true;
                                            break;
                                        }
                                        if (arrayItemCustomerChoice[indexOfListField].ref && arrayItemCustomerChoice[indexOfListField].ref != nameCollParent) {
                                            if (arrayItemCustomerChoice[indexOfListField].isRequire) {
                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: variable.trim()
                                                })
                                                // .sort({
                                                //     _id: -1
                                                // })
                                                // .limit(100)
                                                // .toArray();
                                                if (docs) {
                                                    conditionObj = {
                                                        ...conditionObj,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                    };

                                                    if (arrayItemCustomerChoice[indexOfListField].mappingRef && arrayItemCustomerChoice[indexOfListField].mappingRef.length) {
                                                        arrayItemCustomerChoice[indexOfListField].mappingRef.map(mapping => {
                                                            conditionObj = {
                                                                ...conditionObj,
                                                                [mapping]: docs[mapping]
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                [arrayItemCustomerChoice[indexOfListField].name]: variable
                                            };
                                        }
                                    }

                                    indexOfListField++;
                                }
                                if (checkIsRequire) {
                                    break;
                                }

                                conditionObj = {
                                    ...conditionObj,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                }

                                let arrayCondditionObj = []
                                if (arrayConditionObjDynamic && arrayConditionObjDynamic.length) {
                                    arrayConditionObjDynamic.map(item => {
                                        arrayCondditionObj = [
                                            ...arrayCondditionObj,
                                            {
                                                ...conditionObj,
                                                ...item
                                            }
                                        ]
                                    });
                                } else {
                                    arrayCondditionObj = [
                                        ...arrayCondditionObj,
                                        conditionObj
                                    ]
                                }
                                listData = [
                                    ...listData,
                                    ...arrayCondditionObj
                                ];

                                index++;
                            }
                        }

                        await fs.unlinkSync(file.path);

                        if (listData.length) {
                            await this.changeDataImport({
                                condition,
                                listProduct: listData
                            });
                        } else {
                            return resolve({
                                error: true,
                                message: 'Import thất bại'
                            });
                        }

                        return resolve({
                            error: false,
                            message: 'Import thành công'
                        });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lưu Dữ Liệu Theo Lựa Chọn product
     * @param {object} listProduct
     * @param {object} condition
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    changeDataImport({
        condition,
        listProduct
    }) {
        return new Promise(async resolve => {
            try {
                if (isTrue(condition.delete)) { // XÓA DATA CŨ
                    if (isTrue(condition.deleteAll)) { // XÓA TẤT CẢ DỮ LIỆU
                        console.log("====================XÓA TẤT CẢ DỮ LIỆU====================");
                        await PRODUCT_COLL.deleteMany({});
                        let listDataAfterInsert = await PRODUCT_COLL.insertMany(listProduct);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    } else { // XÓA VỚI ĐIỀU KIỆN
                        console.log("====================XÓA VỚI ĐIỀU KIỆN====================");

                        /**
                         * ===========================================================================
                         * =========================XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===========================================================================
                         */
                        let {
                            filter,
                            condition: conditionMultiple,
                        } = condition.conditionDeleteImport;

                        if (!filter || !filter.length) {
                            return resolve({
                                error: true,
                                message: 'Filter do not exist'
                            });
                        }

                        let conditionObj = {
                            state: 1,
                            $or: []
                        };

                        if (filter && filter.length) {
                            if (filter.length > 1) {

                                filter.map(filterObj => {
                                    if (filterObj.type === 'ref') {
                                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionFieldRef);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionFieldRef
                                            };
                                        }
                                    } else {
                                        const conditionByFilter = this.getConditionObj(filterObj);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionByFilter);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionByFilter
                                            };
                                        }
                                    }
                                });

                            } else {
                                let {
                                    type,
                                    ref,
                                    fieldRefName
                                } = filter[0];

                                if (type === 'ref') {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(ref, fieldRefName)
                                    };
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(filter[0])
                                    };
                                }
                            }
                        }

                        if (conditionObj.$or && !conditionObj.$or.length) {
                            delete conditionObj.$or;
                        }

                        let listAfterDelete = await PRODUCT_COLL.deleteMany({
                            ...conditionObj
                        });
                        /**
                         * ===============================================================================
                         * =========================END XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===============================================================================
                         */

                        if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                            for (let item of listProduct) {
                                let listConditionFindOneUpdate = {};
                                let {
                                    listFieldPrimaryKey
                                } = condition;
                                listFieldPrimaryKey.map(elem => {
                                    listConditionFindOneUpdate = {
                                        ...listConditionFindOneUpdate,
                                        [elem]: item[elem]
                                    }
                                });

                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    state: 1
                                }

                                let checkExist = await PRODUCT_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                    $set: item
                                }, {
                                    upsert: true
                                });
                            }
                            return resolve({
                                error: false,
                                message: 'Insert success'
                            });

                        } else { // INSERT CÁI MỚI
                            console.log("====================INSERT CÁI MỚI 2====================");
                            let listDataAfterInsert = await PRODUCT_COLL.insertMany(listProduct);
                            return resolve({
                                error: false,
                                message: 'Insert success',
                                data: listDataAfterInsert
                            });
                        }
                    }
                } else { // KHÔNG XÓA DATA CŨ
                    if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                        console.log("====================KIỂM TRA TỒN TẠI VÀ UPDATE====================");
                        for (let item of listProduct) {
                            let listConditionFindOneUpdate = {};
                            let {
                                listFieldPrimaryKey
                            } = condition;
                            listFieldPrimaryKey.map(elem => {
                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    [elem]: item[elem]
                                }
                            });

                            listConditionFindOneUpdate = {
                                ...listConditionFindOneUpdate,
                                state: 1
                            }

                            let checkExist = await PRODUCT_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                $set: item
                            }, {
                                upsert: true
                            });
                        }
                        return resolve({
                            error: false,
                            message: 'Insert success'
                        });
                    } else { // INSERT CÁI MỚI
                        console.log("====================INSERT CÁI MỚI====================");
                        let listDataAfterInsert = await PRODUCT_COLL.insertMany(listProduct);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    }
                }

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc product
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
            $or: []
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            let mappingRef = [];
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                    mappingRef = element.mappingRef;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            fieldID: item.fieldID,
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            ref: item.coll,
                            variable: item.name,
                            nameFieldRef,
                            dataEnum: item.dataEnum,
                            isRequire: item.isRequire,
                            dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                            variableChoice: item.variableChoice,
                            nameCollChoice: item.nameCollChoice,
                            mappingRef: mappingRef,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                fieldID: item.fieldID,
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                ref: item.coll,
                                variable: item.name,
                                nameFieldRef,
                                dataEnum: item.dataEnum,
                                isRequire: item.isRequire,
                                dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                variableChoice: item.variableChoice,
                                nameCollChoice: item.nameCollChoice,
                                mappingRef: mappingRef,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    dataEnum: item.dataEnum,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {
            if (filter.length > 1) {

                filter.map(filterObj => {
                    if (filterObj.type === 'ref') {
                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionFieldRef);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionFieldRef
                            };
                        }
                    } else {
                        const conditionByFilter = this.getConditionObj(filterObj);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionByFilter);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionByFilter
                            };
                        }
                    }
                });

            } else {
                let {
                    type,
                    ref,
                    fieldRefName
                } = filter[0];

                if (type === 'ref') {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(ref, fieldRefName)
                    };
                } else {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(filter[0])
                    };
                }
            }
        }

        if (conditionObj.$or && !conditionObj.$or.length) {
            delete conditionObj.$or;
        }


        if (!isEmptyObject(objFilterStatic)) {

            if (objFilterStatic.isSelling) {
                if (![1, 2].includes(Number(objFilterStatic.isSelling)) || Number.isNaN(Number(objFilterStatic.isSelling))) {
                    return resolve({
                        error: true,
                        message: "hàng bán không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    isSelling: Number(objFilterStatic.isSelling)
                }
            }

            if (objFilterStatic.isGift) {
                if (![1, 2].includes(Number(objFilterStatic.isGift)) || Number.isNaN(Number(objFilterStatic.isGift))) {
                    return resolve({
                        error: true,
                        message: "hàng tặng không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    isGift: Number(objFilterStatic.isGift)
                }
            }

            if (objFilterStatic.isTrialProgram) {
                if (![1, 2].includes(Number(objFilterStatic.isTrialProgram)) || Number.isNaN(Number(objFilterStatic.isTrialProgram))) {
                    return resolve({
                        error: true,
                        message: "hàng dùng thử không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    isTrialProgram: Number(objFilterStatic.isTrialProgram)
                }
            }

            if (objFilterStatic.status) {
                if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                    return resolve({
                        error: true,
                        message: "trạng thái không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    status: Number(objFilterStatic.status)
                }
            }

        }


        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let collName = pluralize.plural(item.coll);
                let checkPluralColl = collName[collName.length - 1];

                if (checkPluralColl.toLowerCase() != 's') {
                    collName += 's';
                }

                let lookup = [{
                        $lookup: {
                            from: collName,
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        let collNamePopulate = pluralize.plural(populate.coll);
                        let checkPluralColl = collNamePopulate[collNamePopulate.length - 1];

                        if (checkPluralColl.toLowerCase() != 's') {
                            collNamePopulate += 's';
                        }

                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: collNamePopulate,
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Lấy thông tin product
     * @param {objectId} productID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoProduct({
        productID,
        select,
        filter = {},
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];
                let conditionObj = {
                    state: 1
                };

                if (!checkObjectIDs([productID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị productID không hợp lệ',
                        status: 400
                    });
                }

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                Object.keys(filter).map(key => {
                    if (![].includes(key)) {
                        delete filter[key];
                    }
                });

                let {} = filter;


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['brand'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoProduct = await PRODUCT_COLL
                    .findOne({
                        _id: productID,
                        ...conditionObj
                    })
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoProduct = await PRODUCT_COLL.populate(infoProduct, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoProduct = await PRODUCT_COLL.populate(infoProduct, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoProduct) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin product',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoProduct,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách product
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListProducts({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                } else {

                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['priceFromNumber', 'priceToNumber', 'brand', 'isSelling', 'isGift', 'isTrialProgram', 'status'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    priceFromNumber,
                    priceToNumber,
                    brand,
                    isSelling,
                    isGift,
                    isTrialProgram,
                    status,
                } = filter;

                if (priceFromNumber && priceToNumber) {
                    conditionObj.price = {
                        $gte: priceFromNumber,
                        $lte: priceToNumber,
                    };
                }

                brand && (conditionObj.brand = brand);

                isSelling && (conditionObj.isSelling = isSelling);

                isGift && (conditionObj.isGift = isGift);

                isTrialProgram && (conditionObj.isTrialProgram = isTrialProgram);

                status && (conditionObj.status = status);


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['brand'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listProducts = await PRODUCT_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listProducts = await PRODUCT_COLL.populate(listProducts, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listProducts = await PRODUCT_COLL.populate(listProducts, `${ref}.${field}`);
                        }
                    }
                }

                if (!listProducts) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách product',
                        status: 400
                    });
                }

                let totalRecord = await PRODUCT_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listProducts,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

   /**
     * Lấy danh sách product
     * @extends {BaseModel}
     * @returns {{ 
    *   error: boolean, 
    *   data?: {
    *      object,
    *   },
    *   message?: string,
    * }}
    */
     getListBigSaleAndInfoFlashSale() {
        return new Promise(async resolve => {
            try {
                 /**
                 * ===========================DANH SÁCH BIGSALE================================
                 */
                let listBigSale = await BIG_SALE_MODEL.getListBigSaleActive({ });
            
                /**
                 * ===========================DANH SÁCH FLASH SALE================================
                 */
                let listFlashSale = await FLASH_SALE_MODEL.getListFlashSaleActive({ });
                let data = {
                    listBigSale: listBigSale.data,
                    infoFlashSale: listFlashSale.data,
                }
                return resolve({ error: false, data });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * THÔNG TIN SẢN PHẨM CỦA SKU 2
     * @param {objectId} sku_2
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getInfoProductOfSku2({ sku_2 }) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(sku_2))
                    return resolve({ error: true, message: 'id_invalid' });
                
                const SHOW_QR = 1;
                let infoProduct = await PRODUCT_COLL.findOne({
                    sku_2,
                    show_qr: SHOW_QR
                })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    .populate({
                        path: "images",
                        select: "_id path"
                    })
                    .populate({
                        path: "certifiedImages",
                        select: "_id path"
                    })
                    .populate({
                        path: 'brand',
                        select: 'name linkWebsite image',
                        populate: {
                            path: 'image',
                            select: 'path'
                        }
                    })
                    .populate({
                        path: 'category',
                        select: 'title'
                    });

                if (!infoProduct)
                    return resolve({ error: true, message: "cannot_get_info" });

                return resolve({ error: false, data: infoProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * THÔNG TIN SẢN PHẨM CỦA SKU 1
     * @param {objectId} sku_1
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getInfoProductOfSku1({ sku_1 }) {
        return new Promise(async resolve => {
            try {
                if(!sku_1)
                    return resolve({ error: true, message: 'Mã SKU không tồn tại' });
                
                let infoProduct = await PRODUCT_COLL.findOne({
                    SKU: sku_1,
                })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    .populate({
                        path: "images",
                        select: "_id path"
                    })
                    .populate({
                        path: "certifiedImages",
                        select: "_id path"
                    })
                    .populate({
                        path: 'brand',
                        select: 'name linkWebsite image',
                        populate: {
                            path: 'image',
                            select: 'path'
                        }
                    })
                    .populate({
                        path: 'category',
                        select: 'title'
                    });

                if (!infoProduct)
                    return resolve({ error: true, message: "cannot_get_info" });

                return resolve({ error: false, data: infoProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkVariantAndSpecification({
        variants, specification
    }) {
        return new Promise(async resolve => {
            try {

                let conditionAndOperation = [];
                
                if (variants && variants.length)
                    conditionAndOperation = [
                        ...conditionAndOperation, 
                        { variants: { $all: variants } }
                    ]
                if (specification)  
                    conditionAndOperation = [
                        ...conditionAndOperation,
                        { specification: specification },
                    ]
                    

                let conditionSearch = {
                    $and: conditionAndOperation
                }
                
                let listData = await PRODUCT_COLL.find(conditionSearch);
                return resolve({ error: false, data: listData })
               
            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }
}

exports.MODEL = new Model;