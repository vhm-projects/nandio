const EMPLOYEE_STORE_COLL = require('./databases/employee_store-coll');
const EMPLOYEE_STORE_MODEL = require('./models/employee_store').MODEL;
const EMPLOYEE_STORE_ROUTES = require('./apis/employee_store');
// MARK REQUIRE

module.exports = {
    EMPLOYEE_STORE_COLL,
    EMPLOYEE_STORE_MODEL,
    EMPLOYEE_STORE_ROUTES,
    // MARK EXPORT
}