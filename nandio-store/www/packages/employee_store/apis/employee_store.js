"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const beautifyer = require('js-beautify').js_beautify;
const fs = require('fs');
const moment = require('moment');
const logger = require('../../../config/logger/winston.config');
const chalk = require('chalk');
const log = console.log;

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {} = require('../constants/employee_store');
const {
    CF_ROUTINGS_EMPLOYEE_STORE
} = require('../constants/employee_store/employee_store.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const EMPLOYEE_STORE_MODEL = require('../models/employee_store').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const {
    STORE_COLL
} = require('../../store');

const {
    EMPLOYEE_COLL
} = require('../../employee');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ EMPLOYEE_STORE  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Employee_store (API, VIEW)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.ADD_EMPLOYEE_STORE]: {
                config: {
                    scopes: ['create:employee_store'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Employee_store',
                    code: CF_ROUTINGS_EMPLOYEE_STORE.ADD_EMPLOYEE_STORE,
                    inc: path.resolve(__dirname, '../views/employee_store/add_employee_store.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listStores = await STORE_COLL.find({
                            state: 1,
                            status: 1
                        })

                        let listEmployees = await EMPLOYEE_COLL.find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1
                            }).lean()

                        ChildRouter.renderToView(req, res, {
                            listStores,
                            listEmployees,
                            CF_ROUTINGS_EMPLOYEE_STORE
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            store,
                            employee,
                        } = req.body;


                        let infoAfterInsert = await EMPLOYEE_STORE_MODEL.insert({
                            store,
                            employee,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Employee_store By Id (API, VIEW)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.UPDATE_EMPLOYEE_STORE_BY_ID]: {
                config: {
                    scopes: ['update:employee_store'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Employee_store',
                    code: CF_ROUTINGS_EMPLOYEE_STORE.UPDATE_EMPLOYEE_STORE_BY_ID,
                    inc: path.resolve(__dirname, '../views/employee_store/update_employee_store.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            employee_storeID
                        } = req.query;

                        let infoEmployee_store = await EMPLOYEE_STORE_MODEL.getInfoById(employee_storeID);
                        if (infoEmployee_store.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let listStores = await STORE_COLL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();

                        let listEmployees = await EMPLOYEE_COLL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoEmployee_store: infoEmployee_store.data || {},

                            listStores,
                            listEmployees,
                            CF_ROUTINGS_EMPLOYEE_STORE
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            employee_storeID,
                            store,
                            employee,
                        } = req.body;


                        const infoAfterUpdate = await EMPLOYEE_STORE_MODEL.update({
                            employee_storeID,
                            store,
                            employee,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Employee_store By Id (API)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.UPDATE_EMPLOYEE_STORE_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:employee_store'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            employee_storeID,
                            store,
                            employee,
                        } = req.body;


                        const infoAfterUpdate = await EMPLOYEE_STORE_MODEL.updateNotRequire({
                            employee_storeID,
                            store,
                            employee,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Employee_store By Id (API)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.DELETE_EMPLOYEE_STORE_BY_ID]: {
                config: {
                    scopes: ['delete:employee_store'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            employee_storeID
                        } = req.params;

                        const infoAfterDelete = await EMPLOYEE_STORE_MODEL.deleteById(employee_storeID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Employee_store By List Id (API)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.DELETE_EMPLOYEE_STORE_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:employee_store'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            employee_storeID
                        } = req.body;

                        const infoAfterDelete = await EMPLOYEE_STORE_MODEL.deleteByListId(employee_storeID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Employee_store By Id (API)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.GET_INFO_EMPLOYEE_STORE_BY_ID]: {
                config: {
                    scopes: ['read:info_employee_store'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            employee_storeID
                        } = req.params;

                        const infoEmployee_storeById = await EMPLOYEE_STORE_MODEL.getInfoById(employee_storeID);
                        res.json(infoEmployee_storeById);
                    }]
                },
            },

            /**
             * Function: Get List Employee_store (API, VIEW)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.GET_LIST_EMPLOYEE_STORE]: {
                config: {
                    scopes: ['read:list_employee_store'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Employee_store',
                    code: CF_ROUTINGS_EMPLOYEE_STORE.GET_LIST_EMPLOYEE_STORE,
                    inc: path.resolve(__dirname, '../views/employee_store/list_employee_stores.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            typeGetList
                        } = req.query;

                        let listEmployee_stores = [];
                        if (typeGetList === 'FILTER') {
                            listEmployee_stores = await EMPLOYEE_STORE_MODEL.getListByFilter({

                            });
                        } else {
                            listEmployee_stores = await EMPLOYEE_STORE_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listEmployee_stores: listEmployee_stores.data || [],

                        });
                    }]
                },
            },

            /**
             * Function: Get List Employee_store By Field (API, VIEW)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.GET_LIST_EMPLOYEE_STORE_BY_FIELD]: {
                config: {
                    scopes: ['read:list_employee_store'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Employee_store by field isStatus',
                    code: CF_ROUTINGS_EMPLOYEE_STORE.GET_LIST_EMPLOYEE_STORE_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/employee_store/list_employee_stores.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            type
                        } = req.query;

                        let listEmployee_stores = await EMPLOYEE_STORE_MODEL.getListByFilter({
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listEmployee_stores: listEmployee_stores.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Employee_store Server Side (API)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.GET_LIST_EMPLOYEE_STORE_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_employee_store'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listEmployee_storeServerSide = await EMPLOYEE_STORE_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listEmployee_storeServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Employee_store Import (API)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.GET_LIST_EMPLOYEE_STORE_IMPORT]: {
                config: {
                    scopes: ['read:list_employee_store'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listEmployee_storeImport = await EMPLOYEE_STORE_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listEmployee_storeImport);
                    }]
                },
            },

            /**
             * Function: Get List Employee_store Excel Server Side (API)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.GET_LIST_EMPLOYEE_STORE_EXCEL]: {
                config: {
                    scopes: ['read:list_employee_store'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = EMPLOYEE_STORE_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Employee_store Excel Export (API)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.DOWNLOAD_LIST_EMPLOYEE_STORE_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_employee_store'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'employee_store'
                        });

                        let conditionObj = EMPLOYEE_STORE_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listEmployee_store = await EMPLOYEE_STORE_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listEmployee_store)
                    }]
                },
            },

            /**
             * Function: Setting Employee_store Excel Import (API)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.SETTING_FILE_EMPLOYEE_STORE_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_employee_store'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = EMPLOYEE_STORE_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Employee_store Excel Import (API)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.DOWNLOAD_FILE_EMPLOYEE_STORE_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_employee_store'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'employee_store'
                        });

                        let listEmployee_storeImport = await EMPLOYEE_STORE_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listEmployee_storeImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listEmployee_storeImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Employee_store Excel Import (API)
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.CREATE_EMPLOYEE_STORE_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:employee_store'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'employee_store'
                        });

                        let infoEmployee_storeAfterImport = await EMPLOYEE_STORE_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'employee_store',
                        });

                        res.json(infoEmployee_storeAfterImport);
                    }]
                },
            },

            /**
             * Function: API Get list Employee_store
             * Date: 28/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_STORE.API_GET_LIST_EMPLOYEE_STORES]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listEmployee_stores = await EMPLOYEE_STORE_MODEL.getListEmployee_stores({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listEmployee_stores);
                    }]
                },
            },

        }
    }
};