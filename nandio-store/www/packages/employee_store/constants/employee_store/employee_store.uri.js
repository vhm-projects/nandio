const BASE_ROUTE = '/employee_store';
const API_BASE_ROUTE = '/api/employee_store';

const CF_ROUTINGS_EMPLOYEE_STORE = {
    ADD_EMPLOYEE_STORE: `${BASE_ROUTE}/add-employee_store`,
    UPDATE_EMPLOYEE_STORE_BY_ID: `${BASE_ROUTE}/update-employee_store-by-id`,
    DELETE_EMPLOYEE_STORE_BY_ID: `${BASE_ROUTE}/delete/:employee_storeID`,

    GET_INFO_EMPLOYEE_STORE_BY_ID: `${BASE_ROUTE}/info/:employee_storeID`,
    GET_LIST_EMPLOYEE_STORE: `${BASE_ROUTE}/list-employee_store`,
    GET_LIST_EMPLOYEE_STORE_BY_FIELD: `${BASE_ROUTE}/list-employee_store/:field/:value`,
    GET_LIST_EMPLOYEE_STORE_SERVER_SIDE: `${BASE_ROUTE}/list-employee_store-server-side`,

    UPDATE_EMPLOYEE_STORE_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-employee_store-by-id-v2`,
    DELETE_EMPLOYEE_STORE_BY_LIST_ID: `${BASE_ROUTE}/delete-employee_store-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_EMPLOYEE_STORE_EXCEL: `${BASE_ROUTE}/list-employee_store-excel`,
    DOWNLOAD_LIST_EMPLOYEE_STORE_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-employee_store-excel-export`,

    // IMPORT EXCEL
    GET_LIST_EMPLOYEE_STORE_IMPORT: `${BASE_ROUTE}/list-employee_store-import`,
    SETTING_FILE_EMPLOYEE_STORE_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-employee_store-import-setting`,
    DOWNLOAD_FILE_EMPLOYEE_STORE_EXCEL_IMPORT: `${BASE_ROUTE}/list-employee_store-import-dowload`,
    CREATE_EMPLOYEE_STORE_IMPORT_EXCEL: `${BASE_ROUTE}/create-employee_store-import-excel`,

    API_GET_LIST_EMPLOYEE_STORES: `${API_BASE_ROUTE}/list-employee_stores`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_EMPLOYEE_STORE = CF_ROUTINGS_EMPLOYEE_STORE;