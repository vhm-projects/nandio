const LIMIT_KPI_CONFIG_COLL = require('./databases/limit_kpi_config-coll');
const LIMIT_KPI_CONFIG_MODEL = require('./models/limit_kpi_config').MODEL;
const LIMIT_KPI_CONFIG_ROUTES = require('./apis/limit_kpi_config');

const LIMIT_GIFT_CONFIG_COLL  = require('./databases/limit_gift_config-coll');
const LIMIT_GIFT_CONFIG_MODEL  = require('./models/limit_gift_config').MODEL;
const LIMIT_GIFT_CONFIG_ROUTES  = require('./apis/limit_gift_config');
    

const LIMIT_SAMPLING_CONFIG_COLL  = require('./databases/limit_sampling_config-coll');
const LIMIT_SAMPLING_CONFIG_MODEL  = require('./models/limit_sampling_config').MODEL;
const LIMIT_SAMPLING_CONFIG_ROUTES  = require('./apis/limit_sampling_config');
    

const SYSTEM_CONFIG_COLL  = require('./databases/system_config-coll');
const SYSTEM_CONFIG_MODEL  = require('./models/system_config').MODEL;
const SYSTEM_CONFIG_ROUTES  = require('./apis/system_config');
    

const CRITERIA_CAMPAIGN_CONFIG_COLL  = require('./databases/criteria_campaign_config-coll');
const CRITERIA_CAMPAIGN_CONFIG_MODEL  = require('./models/criteria_campaign_config').MODEL;
const CRITERIA_CAMPAIGN_CONFIG_ROUTES  = require('./apis/criteria_campaign_config');
    
// MARK REQUIRE

module.exports = {
    LIMIT_KPI_CONFIG_COLL,
    LIMIT_KPI_CONFIG_MODEL,
    LIMIT_KPI_CONFIG_ROUTES,

    LIMIT_GIFT_CONFIG_COLL,
    LIMIT_GIFT_CONFIG_MODEL,
    LIMIT_GIFT_CONFIG_ROUTES,
    

    LIMIT_SAMPLING_CONFIG_COLL,
    LIMIT_SAMPLING_CONFIG_MODEL,
    LIMIT_SAMPLING_CONFIG_ROUTES,
    

    SYSTEM_CONFIG_COLL,
    SYSTEM_CONFIG_MODEL,
    SYSTEM_CONFIG_ROUTES,
    

    CRITERIA_CAMPAIGN_CONFIG_COLL,
    CRITERIA_CAMPAIGN_CONFIG_MODEL,
    CRITERIA_CAMPAIGN_CONFIG_ROUTES,
    
    // MARK EXPORT
}