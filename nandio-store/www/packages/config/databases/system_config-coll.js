"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('system_config', {

    /**
     * Thời gian trước khi checkin
     */
    timeBeforeCheckin: {
        type: Number,
        required: true,
    },
    /**
     * Thời gian sau khi checkout
     */
    timeAfterCheckout: {
        type: Number,
        required: true,
    },
    /**
     * Giới hạn đổi quà/ngày
     */
    limitExchangeGift: {
        type: Number,
        required: true,
    },
    /**
     * Giới hạn hàng dùng thử/ngày
     */
    limitExchangeSampling: {
        type: String,
        required: true,
    },
});