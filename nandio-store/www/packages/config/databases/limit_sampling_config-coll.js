"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('limit_sampling_config', {

    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Vùng
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * Nhà phân phối
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    /**
     * Nhân viên
     */
    employee: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },
    /**
     * SKU
     */
    sku: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },
    /**
     * số lượng
     */
    amount: {
        type: Number,
        required: true,
    },
    /**
     * Tháng
     */
    month: {
        type: Number,
        required: true,
    },
    /**
     * Năm
     */
    year: {
        type: Number,
        required: true,
    },
});