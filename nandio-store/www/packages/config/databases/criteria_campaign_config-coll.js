"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('criteria_campaign_config', {

    /**
     * Tiêu chí
     */
    criteria: {
        type: Schema.Types.ObjectId,
        ref: 'criteria',
    },
    /**
     * Chiến dịch
     */
    campaign: {
        type: Schema.Types.ObjectId,
        ref: 'campaign',
    },
    /**
     * Phần trăm
     */
    percent: {
        type: Number,
        required: true,
    },
    /**
     * Max Phần trăm
     */
    maxPercent: {
        type: Number,
    },
});