"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const beautifyer = require('js-beautify').js_beautify;
const fs = require('fs');
const moment = require('moment');
const logger = require('../../../config/logger/winston.config');
const chalk = require('chalk');
const log = console.log;

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {} = require('../constants/system_config');
const {
    CF_ROUTINGS_SYSTEM_CONFIG
} = require('../constants/system_config/system_config.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const SYSTEM_CONFIG_MODEL = require('../models/system_config').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ SYSTEM_CONFIG  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert System_config (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.ADD_SYSTEM_CONFIG]: {
                config: {
                    scopes: ['create:system_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm System_config',
                    code: CF_ROUTINGS_SYSTEM_CONFIG.ADD_SYSTEM_CONFIG,
                    inc: path.resolve(__dirname, '../views/system_config/add_system_config.ejs')
                },
                methods: {
                    get: [async function(req, res) {


                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_SYSTEM_CONFIG
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            timeBeforeCheckin,
                            timeAfterCheckout,
                            limitExchangeGift,
                            limitExchangeSampling,
                        } = req.body;


                        let infoAfterInsert = await SYSTEM_CONFIG_MODEL.insert({
                            timeBeforeCheckin,
                            timeAfterCheckout,
                            limitExchangeGift,
                            limitExchangeSampling,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update System_config By Id (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.UPDATE_SYSTEM_CONFIG_BY_ID]: {
                config: {
                    scopes: ['update:system_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật System_config',
                    code: CF_ROUTINGS_SYSTEM_CONFIG.UPDATE_SYSTEM_CONFIG_BY_ID,
                    inc: path.resolve(__dirname, '../views/system_config/update_system_config.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            system_configID
                        } = req.query;

                        let infoSystem_config = await SYSTEM_CONFIG_MODEL.getInfoById(system_configID);
                        if (infoSystem_config.error) {
                            return res.redirect('/something-went-wrong');
                        }



                        ChildRouter.renderToView(req, res, {
                            infoSystem_config: infoSystem_config.data || {},


                            CF_ROUTINGS_SYSTEM_CONFIG
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            system_configID,
                            timeBeforeCheckin,
                            timeAfterCheckout,
                            limitExchangeGift,
                            limitExchangeSampling,
                        } = req.body;


                        const infoAfterUpdate = await SYSTEM_CONFIG_MODEL.update({
                            system_configID,
                            timeBeforeCheckin,
                            timeAfterCheckout,
                            limitExchangeGift,
                            limitExchangeSampling,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require System_config By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.UPDATE_SYSTEM_CONFIG_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:system_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            system_configID,
                            timeBeforeCheckin,
                            timeAfterCheckout,
                            limitExchangeGift,
                            limitExchangeSampling,
                        } = req.body;


                        const infoAfterUpdate = await SYSTEM_CONFIG_MODEL.updateNotRequire({
                            system_configID,
                            timeBeforeCheckin,
                            timeAfterCheckout,
                            limitExchangeGift,
                            limitExchangeSampling,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete System_config By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.DELETE_SYSTEM_CONFIG_BY_ID]: {
                config: {
                    scopes: ['delete:system_config'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            system_configID
                        } = req.params;

                        const infoAfterDelete = await SYSTEM_CONFIG_MODEL.deleteById(system_configID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete System_config By List Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.DELETE_SYSTEM_CONFIG_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:system_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            system_configID
                        } = req.body;

                        const infoAfterDelete = await SYSTEM_CONFIG_MODEL.deleteByListId(system_configID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info System_config By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.GET_INFO_SYSTEM_CONFIG_BY_ID]: {
                config: {
                    scopes: ['read:info_system_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            system_configID
                        } = req.params;

                        const infoSystem_configById = await SYSTEM_CONFIG_MODEL.getInfoById(system_configID);
                        res.json(infoSystem_configById);
                    }]
                },
            },

            /**
             * Function: Get List System_config (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.GET_LIST_SYSTEM_CONFIG]: {
                config: {
                    scopes: ['read:list_system_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách cấu hình hệ thống',
                    code: CF_ROUTINGS_SYSTEM_CONFIG.GET_LIST_SYSTEM_CONFIG,
                    inc: path.resolve(__dirname, '../views/system_config/list_system_configs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            typeGetList
                        } = req.query;

                        let listSystem_configs = [];
                        if (typeGetList === 'FILTER') {
                            listSystem_configs = await SYSTEM_CONFIG_MODEL.getListByFilter({
                                keyword,
                            });
                        } else {
                            listSystem_configs = await SYSTEM_CONFIG_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listSystem_configs: listSystem_configs.data || [],

                        });
                    }]
                },
            },

            /**
             * Function: Get List System_config By Field (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.GET_LIST_SYSTEM_CONFIG_BY_FIELD]: {
                config: {
                    scopes: ['read:list_system_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách System_config by field isStatus',
                    code: CF_ROUTINGS_SYSTEM_CONFIG.GET_LIST_SYSTEM_CONFIG_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/system_config/list_system_configs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            type
                        } = req.query;

                        let listSystem_configs = await SYSTEM_CONFIG_MODEL.getListByFilter({
                            keyword,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listSystem_configs: listSystem_configs.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List System_config Server Side (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.GET_LIST_SYSTEM_CONFIG_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_system_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listSystem_configServerSide = await SYSTEM_CONFIG_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listSystem_configServerSide);
                    }]
                },
            },

            /**
             * Function: Get List System_config Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.GET_LIST_SYSTEM_CONFIG_IMPORT]: {
                config: {
                    scopes: ['read:list_system_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listSystem_configImport = await SYSTEM_CONFIG_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listSystem_configImport);
                    }]
                },
            },

            /**
             * Function: Get List System_config Excel Server Side (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.GET_LIST_SYSTEM_CONFIG_EXCEL]: {
                config: {
                    scopes: ['read:list_system_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = SYSTEM_CONFIG_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download System_config Excel Export (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.DOWNLOAD_LIST_SYSTEM_CONFIG_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_system_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'system_config'
                        });

                        let conditionObj = SYSTEM_CONFIG_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listSystem_config = await SYSTEM_CONFIG_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listSystem_config)
                    }]
                },
            },

            /**
             * Function: Setting System_config Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.SETTING_FILE_SYSTEM_CONFIG_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_system_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = SYSTEM_CONFIG_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download System_config Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.DOWNLOAD_FILE_SYSTEM_CONFIG_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_system_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'system_config'
                        });

                        let listSystem_configImport = await SYSTEM_CONFIG_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listSystem_configImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listSystem_configImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload System_config Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SYSTEM_CONFIG.CREATE_SYSTEM_CONFIG_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:system_config'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'system_config'
                        });

                        let infoSystem_configAfterImport = await SYSTEM_CONFIG_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'system_config',
                        });

                        res.json(infoSystem_configAfterImport);
                    }]
                },
            },

        }
    }
};