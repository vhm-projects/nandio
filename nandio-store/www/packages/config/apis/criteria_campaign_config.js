"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG
} = require('../constants/criteria_campaign_config/criteria_campaign_config.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const CRITERIA_CAMPAIGN_CONFIG_MODEL = require('../models/criteria_campaign_config').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */
const {
    CRITERIA_COLL
} = require('../../campaign_criteria');
const {
    CAMPAIGN_COLL
} = require('../../campaign_criteria');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ CRITERIA_CAMPAIGN_CONFIG  ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: Insert Criteria_campaign_config (API, VIEW)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.ADD_CRITERIA_CAMPAIGN_CONFIG]: {
                config: {
                    scopes: ['create:criteria_campaign_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Criteria_campaign_config',
                    code: CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.ADD_CRITERIA_CAMPAIGN_CONFIG,
                    inc: path.resolve(__dirname, '../views/criteria_campaign_config/add_criteria_campaign_config.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listCriterias = await CRITERIA_COLL.find({
                            state: 1,
                            status: 1
                        })

                        let listCampaigns = await CAMPAIGN_COLL.find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                percent: 1,
                                modifyAt: -1
                            }).lean()

                        ChildRouter.renderToView(req, res, {
                            listCriterias,
                            listCampaigns,
                            CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            criteria,
                            campaign,
                            percent,
                            maxPercent,
                        } = req.body;

                        let infoAfterInsert = await CRITERIA_CAMPAIGN_CONFIG_MODEL.insert({
                            criteria,
                            campaign,
                            percent,
                            maxPercent,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Criteria_campaign_config By Id (API, VIEW)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.UPDATE_CRITERIA_CAMPAIGN_CONFIG_BY_ID]: {
                config: {
                    scopes: ['update:criteria_campaign_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Criteria_campaign_config',
                    code: CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.UPDATE_CRITERIA_CAMPAIGN_CONFIG_BY_ID,
                    inc: path.resolve(__dirname, '../views/criteria_campaign_config/update_criteria_campaign_config.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            criteria_campaign_configID
                        } = req.query;

                        let infoCriteria_campaign_config = await CRITERIA_CAMPAIGN_CONFIG_MODEL.getInfoById(criteria_campaign_configID);
                        if (infoCriteria_campaign_config.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let listCriterias = await CRITERIA_COLL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();

                        let listCampaigns = await CAMPAIGN_COLL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoCriteria_campaign_config: infoCriteria_campaign_config.data || {},
                            listCriterias,
                            listCampaigns,
                            CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            criteria_campaign_configID,
                            criteria,
                            campaign,
                            percent,
                            maxPercent,
                        } = req.body;

                        const infoAfterUpdate = await CRITERIA_CAMPAIGN_CONFIG_MODEL.update({
                            criteria_campaign_configID,
                            criteria,
                            campaign,
                            percent,
                            maxPercent,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Criteria_campaign_config By Id (API)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.UPDATE_CRITERIA_CAMPAIGN_CONFIG_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            criteria_campaign_configID,
                            criteria,
                            campaign,
                            percent,
                        } = req.body;


                        const infoAfterUpdate = await CRITERIA_CAMPAIGN_CONFIG_MODEL.updateNotRequire({
                            criteria_campaign_configID,
                            criteria,
                            campaign,
                            percent,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Criteria_campaign_config By Id (API)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.DELETE_CRITERIA_CAMPAIGN_CONFIG_BY_ID]: {
                config: {
                    scopes: ['delete:criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            criteria_campaign_configID
                        } = req.params;

                        const infoAfterDelete = await CRITERIA_CAMPAIGN_CONFIG_MODEL.deleteById(criteria_campaign_configID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Criteria_campaign_config By List Id (API)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.DELETE_CRITERIA_CAMPAIGN_CONFIG_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            criteria_campaign_configID
                        } = req.body;

                        const infoAfterDelete = await CRITERIA_CAMPAIGN_CONFIG_MODEL.deleteByListId(criteria_campaign_configID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Criteria_campaign_config By Id (API)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.GET_INFO_CRITERIA_CAMPAIGN_CONFIG_BY_ID]: {
                config: {
                    scopes: ['read:info_criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            criteria_campaign_configID
                        } = req.params;

                        const infoCriteria_campaign_configById = await CRITERIA_CAMPAIGN_CONFIG_MODEL.getInfoById(criteria_campaign_configID);
                        res.json(infoCriteria_campaign_configById);
                    }]
                },
            },

            /**
             * Function: Get List Criteria_campaign_config (API, VIEW)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.GET_LIST_CRITERIA_CAMPAIGN_CONFIG]: {
                config: {
                    scopes: ['read:list_criteria_campaign_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách tiêu chí chiến dịch',
                    code: CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.GET_LIST_CRITERIA_CAMPAIGN_CONFIG,
                    inc: path.resolve(__dirname, '../views/criteria_campaign_config/list_criteria_campaign_configs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            typeGetList
                        } = req.query;

                        let listCriteria_campaign_configs = [];
                        if (typeGetList === 'FILTER') {
                            listCriteria_campaign_configs = await CRITERIA_CAMPAIGN_CONFIG_MODEL.getListByFilter({

                            });
                        } else {
                            listCriteria_campaign_configs = await CRITERIA_CAMPAIGN_CONFIG_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listCriteria_campaign_configs: listCriteria_campaign_configs.data || [],

                        });
                    }]
                },
            },

            /**
             * Function: Get List Criteria_campaign_config By Field (API, VIEW)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.GET_LIST_CRITERIA_CAMPAIGN_CONFIG_BY_FIELD]: {
                config: {
                    scopes: ['read:list_criteria_campaign_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Criteria_campaign_config by field isStatus',
                    code: CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.GET_LIST_CRITERIA_CAMPAIGN_CONFIG_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/criteria_campaign_config/list_criteria_campaign_configs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            type
                        } = req.query;

                        let listCriteria_campaign_configs = await CRITERIA_CAMPAIGN_CONFIG_MODEL.getListByFilter({
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listCriteria_campaign_configs: listCriteria_campaign_configs.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Criteria_campaign_config Server Side (API)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.GET_LIST_CRITERIA_CAMPAIGN_CONFIG_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listCriteria_campaign_configServerSide = await CRITERIA_CAMPAIGN_CONFIG_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listCriteria_campaign_configServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Criteria_campaign_config Import (API)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.GET_LIST_CRITERIA_CAMPAIGN_CONFIG_IMPORT]: {
                config: {
                    scopes: ['read:list_criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listCriteria_campaign_configImport = await CRITERIA_CAMPAIGN_CONFIG_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listCriteria_campaign_configImport);
                    }]
                },
            },

            /**
             * Function: Get List Criteria_campaign_config Excel Server Side (API)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.GET_LIST_CRITERIA_CAMPAIGN_CONFIG_EXCEL]: {
                config: {
                    scopes: ['read:list_criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = CRITERIA_CAMPAIGN_CONFIG_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Criteria_campaign_config Excel Export (API)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.DOWNLOAD_LIST_CRITERIA_CAMPAIGN_CONFIG_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'criteria_campaign_config'
                        });

                        let conditionObj = CRITERIA_CAMPAIGN_CONFIG_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listCriteria_campaign_config = await CRITERIA_CAMPAIGN_CONFIG_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listCriteria_campaign_config)
                    }]
                },
            },

            /**
             * Function: Setting Criteria_campaign_config Excel Import (API)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.SETTING_FILE_CRITERIA_CAMPAIGN_CONFIG_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = CRITERIA_CAMPAIGN_CONFIG_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Criteria_campaign_config Excel Import (API)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.DOWNLOAD_FILE_CRITERIA_CAMPAIGN_CONFIG_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'criteria_campaign_config'
                        });

                        let listCriteria_campaign_configImport = await CRITERIA_CAMPAIGN_CONFIG_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listCriteria_campaign_configImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listCriteria_campaign_configImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Criteria_campaign_config Excel Import (API)
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.CREATE_CRITERIA_CAMPAIGN_CONFIG_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'criteria_campaign_config'
                        });

                        let infoCriteria_campaign_configAfterImport = await CRITERIA_CAMPAIGN_CONFIG_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'criteria_campaign_config',
                        });

                        res.json(infoCriteria_campaign_configAfterImport);
                    }]
                },
            },

            /**
             * Function: API Get info Criteria_campaign_config
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.API_GET_INFO_CRITERIA_CAMPAIGN_CONFIG_OF_ME]: {
                config: {
                    scopes: ['read:info_criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {
                            criteria_campaign_configID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoCriteria_campaign_config = await CRITERIA_CAMPAIGN_CONFIG_MODEL.getInfoCriteria_campaign_config({
                            criteria_campaign_configID,
                            select,
                            filter,
                            explain,
                            authorID
                        });
                        res.json(infoCriteria_campaign_config);
                    }]
                },
            },

            /**
             * Function: API Get list Criteria_campaign_config
             * Date: 19/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG.API_GET_LIST_CRITERIA_CAMPAIGN_CONFIGS]: {
                config: {
                    scopes: ['read:list_criteria_campaign_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listCriteria_campaign_configs = await CRITERIA_CAMPAIGN_CONFIG_MODEL.getListCriteria_campaign_configs({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                        });
                        res.json(listCriteria_campaign_configs);
                    }]
                },
            },

        }
    }
};