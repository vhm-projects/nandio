"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const { CF_ROUTINGS_LIMIT_KPI_CONFIG } = require('../constants/limit_kpi_config/limit_kpi_config.uri');
const { uploadSingle } = require('../../../config/cf_helpers_multer');
const { checkObjectIDs } = require('../../../utils/utils');
const { TYPE_CRITERIA } = require('../constants/limit_kpi_config');
const XlsxPopulate = require('xlsx-populate');

const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain    = HOST_PROD + ":" + PORT_PROD;

/**
 * MODELS
 */
const LIMIT_KPI_CONFIG_MODEL = require('../models/limit_kpi_config').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */
const {
    STORE_COLL
} = require('../../store');

const {
    EMPLOYEE_COLL, EMPLOYEE_HR_COLL
} = require('../../employee');

const {
    CRITERIA_COLL
} = require('../../campaign_criteria');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ LIMIT_KPI_CONFIG  ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: Insert Limit_kpi_config (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.ADD_LIMIT_KPI_CONFIG]: {
                config: {
                    scopes: ['create:limit_kpi_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Limit_kpi_config',
                    code: CF_ROUTINGS_LIMIT_KPI_CONFIG.ADD_LIMIT_KPI_CONFIG,
                    inc: path.resolve(__dirname, '../views/limit_kpi_config/add_limit_kpi_config.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listCriterias = await CRITERIA_COLL.find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1
                            }).lean()

                        ChildRouter.renderToView(req, res, {
                            listCriterias,
                            CF_ROUTINGS_LIMIT_KPI_CONFIG
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            store,
                            employee,
                            criteria,
                            amount,
                            month,
                            year,
                        } = req.body;


                        let infoAfterInsert = await LIMIT_KPI_CONFIG_MODEL.insert({
                            store,
                            employee,
                            criteria,
                            amount,
                            month,
                            year,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Limit_kpi_config By Id (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.UPDATE_LIMIT_KPI_CONFIG_BY_ID]: {
                config: {
                    scopes: ['update:limit_kpi_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Limit_kpi_config',
                    code: CF_ROUTINGS_LIMIT_KPI_CONFIG.UPDATE_LIMIT_KPI_CONFIG_BY_ID,
                    inc: path.resolve(__dirname, '../views/limit_kpi_config/update_limit_kpi_config.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            limit_kpi_configID
                        } = req.query;

                        let infoLimit_kpi_config = await LIMIT_KPI_CONFIG_MODEL.getInfoById(limit_kpi_configID);
                        if (infoLimit_kpi_config.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionStore = {
                            state: 1,
                            status: 1
                        };
                        if (infoLimit_kpi_config.data.store) {

                            conditionStore._id = infoLimit_kpi_config.data.store._id;

                        }

                        let listStores = await STORE_COLL
                            .find(conditionStore)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionEmployee = {
                            state: 1,
                            status: 1
                        };
                        if (infoLimit_kpi_config.data.employee) {

                            conditionEmployee._id = infoLimit_kpi_config.data.employee._id;

                        }

                        let listEmployees = await EMPLOYEE_COLL
                            .find(conditionEmployee)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let listCriterias = await CRITERIA_COLL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoLimit_kpi_config: infoLimit_kpi_config.data || {},

                            listStores,
                            listEmployees,
                            listCriterias,
                            CF_ROUTINGS_LIMIT_KPI_CONFIG
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            limit_kpi_configID,
                            store,
                            employee,
                            criteria,
                            amount,
                            month,
                            year,
                        } = req.body;


                        const infoAfterUpdate = await LIMIT_KPI_CONFIG_MODEL.update({
                            limit_kpi_configID,
                            store,
                            employee,
                            criteria,
                            amount,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Limit_kpi_config By Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.UPDATE_LIMIT_KPI_CONFIG_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            limit_kpi_configID,
                            store,
                            employee,
                            criteria,
                            amount,
                            month,
                            year,
                        } = req.body;


                        const infoAfterUpdate = await LIMIT_KPI_CONFIG_MODEL.updateNotRequire({
                            limit_kpi_configID,
                            store,
                            employee,
                            criteria,
                            amount,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Limit_kpi_config By Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.DELETE_LIMIT_KPI_CONFIG_BY_ID]: {
                config: {
                    scopes: ['delete:limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            limit_kpi_configID
                        } = req.params;

                        const infoAfterDelete = await LIMIT_KPI_CONFIG_MODEL.deleteById(limit_kpi_configID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Limit_kpi_config By List Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.DELETE_LIMIT_KPI_CONFIG_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            limit_kpi_configID
                        } = req.body;

                        const infoAfterDelete = await LIMIT_KPI_CONFIG_MODEL.deleteByListId(limit_kpi_configID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Limit_kpi_config By Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.GET_INFO_LIMIT_KPI_CONFIG_BY_ID]: {
                config: {
                    scopes: ['read:info_limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            limit_kpi_configID
                        } = req.params;

                        const infoLimit_kpi_configById = await LIMIT_KPI_CONFIG_MODEL.getInfoById(limit_kpi_configID);
                        res.json(infoLimit_kpi_configById);
                    }]
                },
            },

            /**
             * Function: Get List Limit_kpi_config (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.GET_LIST_LIMIT_KPI_CONFIG]: {
                config: {
                    scopes: ['read:list_limit_kpi_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách cấu hình kpi',
                    code: CF_ROUTINGS_LIMIT_KPI_CONFIG.GET_LIST_LIMIT_KPI_CONFIG,
                    inc: path.resolve(__dirname, '../views/limit_kpi_config/list_limit_kpi_configs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            amountFromNumber,
                            amountToNumber,
                            typeGetList
                        } = req.query;

                        let listLimit_kpi_configs = [];
                        if (typeGetList === 'FILTER') {
                            listLimit_kpi_configs = await LIMIT_KPI_CONFIG_MODEL.getListByFilter({
                                amountFromNumber,
                                amountToNumber,
                            });
                        } else {
                            listLimit_kpi_configs = await LIMIT_KPI_CONFIG_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listLimit_kpi_configs: listLimit_kpi_configs.data || [],

                        });
                    }]
                },
            },

            /**
             * Function: Get List Limit_kpi_config By Field (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.GET_LIST_LIMIT_KPI_CONFIG_BY_FIELD]: {
                config: {
                    scopes: ['read:list_limit_kpi_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Limit_kpi_config by field isStatus',
                    code: CF_ROUTINGS_LIMIT_KPI_CONFIG.GET_LIST_LIMIT_KPI_CONFIG_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/limit_kpi_config/list_limit_kpi_configs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            amountFromNumber,
                            amountToNumber,
                            type
                        } = req.query;

                        let listLimit_kpi_configs = await LIMIT_KPI_CONFIG_MODEL.getListByFilter({
                            amountFromNumber,
                            amountToNumber,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listLimit_kpi_configs: listLimit_kpi_configs.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Limit_kpi_config Server Side (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.GET_LIST_LIMIT_KPI_CONFIG_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listLimit_kpi_configServerSide = await LIMIT_KPI_CONFIG_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(listLimit_kpi_configServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Limit_kpi_config Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.GET_LIST_LIMIT_KPI_CONFIG_IMPORT]: {
                config: {
                    scopes: ['read:list_limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listLimit_kpi_configImport = await LIMIT_KPI_CONFIG_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listLimit_kpi_configImport);
                    }]
                },
            },

            /**
             * Function: Get List Limit_kpi_config Excel Server Side (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.GET_LIST_LIMIT_KPI_CONFIG_EXCEL]: {
                config: {
                    scopes: ['read:list_limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = LIMIT_KPI_CONFIG_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Limit_kpi_config Excel Export (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.DOWNLOAD_LIST_LIMIT_KPI_CONFIG_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'limit_kpi_config'
                        });

                        let conditionObj = LIMIT_KPI_CONFIG_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listLimit_kpi_config = await LIMIT_KPI_CONFIG_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listLimit_kpi_config)
                    }]
                },
            },

            /**
             * Function: Setting Limit_kpi_config Excel Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.SETTING_FILE_LIMIT_KPI_CONFIG_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = LIMIT_KPI_CONFIG_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Limit_kpi_config Excel Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.DOWNLOAD_FILE_LIMIT_KPI_CONFIG_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { opts }  = req.query;
                        let condition = JSON.parse(opts);

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'limit_kpi_config'
                        });

                        let listLimit_kpi_configImport = await LIMIT_KPI_CONFIG_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            opts: condition
                        });
                        console.log({ listLimit_kpi_configImport })

                        res.download(listLimit_kpi_configImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listLimit_kpi_configImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Limit_kpi_config Excel Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.CREATE_LIMIT_KPI_CONFIG_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'limit_kpi_config'
                        });

                        let infoLimit_kpi_configAfterImport = await LIMIT_KPI_CONFIG_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'limit_kpi_config',
                        });

                        res.json(infoLimit_kpi_configAfterImport);
                    }]
                },
            },

            /**
             * Function: API Get info Limit_kpi_config
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.API_GET_INFO_LIMIT_KPI_CONFIG]: {
                config: {
                    scopes: ['read:info_limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            limit_kpi_configID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoLimit_kpi_config = await LIMIT_KPI_CONFIG_MODEL.getInfoLimit_kpi_config({
                            limit_kpi_configID,
                            select,
                            filter,
                            explain
                        });
                        res.json(infoLimit_kpi_config);
                    }]
                },
            },

            /**
             * Function: API Get info Limit_kpi_config
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.API_GET_LIST_LIMIT_KPI_CONFIGS]: {
                config: {
                    scopes: ['read:list_limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listLimit_kpi_configs = await LIMIT_KPI_CONFIG_MODEL.getListLimit_kpi_configs({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listLimit_kpi_configs);
                    }]
                },
            },

            /**
             * Function: API Lấy danh sách KPI theo bộ lọc
             * Date: 17/01/2022
             * Dev: MinhVH
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.API_GET_LIST_KPI_BY_FILTER]: {
                config: {
                    scopes: ['read:list_limit_kpi_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const { employeeID, date, type } = req.query;
                        let listKPI = { status: 500 };

                        if (!checkObjectIDs([employeeID])) {
                            return res.json({
                                error: true,
                                status: 400,
                                message: 'ID Nhân viên không hợp lệ'
                            });
                        }

                        // TODO: can cap nhat lai -> tieu chi (_id) (updated - 18/01/2022)
                        switch (type) {
                            case TYPE_CRITERIA.SALEOUT:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPISaleOut({ employeeID, date });
                                break;
                            case TYPE_CRITERIA.EXCHANGE_GIFT:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPIExchangeGift({ employeeID, date });
                                break;
                            case TYPE_CRITERIA.EXCHANGE_SAMPLING:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPIExchangeSampling({ employeeID, date });
                                break;
                            case TYPE_CRITERIA.INIT_CUSTOMER:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPIInitCustomer({ employeeID, date });
                                break;
                            case TYPE_CRITERIA.SCAN_QR_SALEOUT:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPIInitCustomer({ employeeID, date });
                                break;
                            default:
                                break;
                        }

                        res.status(listKPI.status).json(listKPI);
                    }]
                },
            },
            
            /**
             * =======================KPI VERSION MỚI (TỪ NGÀY 23/02/2022)====================
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.API_GET_LIST_KPI_REQUIRE_NEW]: {
                config: {
                    scopes: ['read:list_kpi_salein_require'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const { employeeID, date, type } = req.query;
                        let listKPI = { status: 500 };

                        if (!checkObjectIDs([employeeID])) {
                            return res.json({
                                error: true,
                                status: 400,
                                message: 'ID Nhân viên không hợp lệ'
                            });
                        }

                        // TODO: can cap nhat lai -> tieu chi (_id) (updated - 18/01/2022)
                        switch (type) {
                            case TYPE_CRITERIA.SALEIN:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPISaleinRequire({ employeeID, date });
                                break;
                            case TYPE_CRITERIA.PRESENT:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPIPresentRequire({ employeeID, date });
                                break;
                            case TYPE_CRITERIA.SBD:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPISBDRequire({ date });
                                break;
                            case TYPE_CRITERIA.MHTT:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPIMHTTRequire({ date });
                                break;
                            default:
                                break;
                        }

                        res.status(listKPI.status).json(listKPI);
                    }]
                },
            },

            [CF_ROUTINGS_LIMIT_KPI_CONFIG.API_GET_LIST_KPI_ACTUALYY_NEW]: {
                config: {
                    scopes: ['read:list_kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const { employeeID, date, type } = req.query;
                        let listKPI = { status: 500 };

                        if (!checkObjectIDs([employeeID])) {
                            return res.json({
                                error: true,
                                status: 400,
                                message: 'ID Nhân viên không hợp lệ'
                            });
                        }

                        // TODO: can cap nhat lai -> tieu chi (_id) (updated - 18/01/2022)
                        switch (type) {
                            case TYPE_CRITERIA.SALEIN:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPISaleinActually({ employeeID, date });
                                break;
                            case TYPE_CRITERIA.PRESENT:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPIPresentActully({ employeeID, date });
                                break;
                            case TYPE_CRITERIA.SBD:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPISBDActually({ employeeID, date });
                                break;
                            case TYPE_CRITERIA.MHTT:
                                listKPI = await LIMIT_KPI_CONFIG_MODEL.getListKPISaleinActually({ employeeID, date, typeMHTT: true });
                                break;
                            default:
                                break;
                        }

                        res.status(listKPI.status).json(listKPI);
                    }]
                },
            },

            [CF_ROUTINGS_LIMIT_KPI_CONFIG.API_INVENTORY_WITH_SKU]: {
                config: {
                    scopes: ['read:list_kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let employeeID = req.user && req.user._id;
                        const { date } = req.query;
                        let listSKUWithInventory = await LIMIT_KPI_CONFIG_MODEL.getListSKUWithInventory({ employeeID, date });
                        res.json(listSKUWithInventory);
                    }]
                },
            },

            /**
             * Function: Get List Limit_kpi_config By Field (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_KPI_CONFIG.REPORT_KPI]: {
                config: {
                    scopes: ['read:list_kpi_salein_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Report KPI',
                    code: CF_ROUTINGS_LIMIT_KPI_CONFIG.REPORT_KPI,
                    inc: path.resolve(__dirname, '../views/limit_kpi_config/dowload_report_kpi.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        ChildRouter.renderToView(req, res, {
                        });
                    }]
                },
            },

            [CF_ROUTINGS_LIMIT_KPI_CONFIG.REPORT_KPI_NEW]: {
                config: {
                    scopes: ['read:list_kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { region }       = req.user;
                        let { monthAndYear } = req.query;
                        let listReportKPI = await LIMIT_KPI_CONFIG_MODEL.reportKPI({ region, monthAndYear });
                        XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Template_kpi_update_mhtt.xlsx')))
                        .then(async workbook => {
                            let {
                                listEmployees,
                                listSaleinRequire,
                                listSaleinActually,
                                listPresentRequire,
                                listPresentActually,
                                listSBDRequire,
                                listSBDActually,
                                listMHTTRequire,
                                listSaleinActually__MHTT,
                                listCriteriasCampaign 
                            } = listReportKPI;
                            let rowBegin = 9;
                            let index = 0;
                            for(const employee of listEmployees ) {
                                if(employee.distributor && employee.state == 1 && employee.status == 1){
                                    workbook.sheet("KPI").row(rowBegin).cell(1).value(index + 1);
                                    workbook.sheet("KPI").row(rowBegin).cell(2).value(employee.area.code);
                                    workbook.sheet("KPI").row(rowBegin).cell(3).value(employee.region.code);
                                    workbook.sheet("KPI").row(rowBegin).cell(4).value(employee.distributor && employee.distributor.code);
                                    workbook.sheet("KPI").row(rowBegin).cell(5).value(employee.distributor && employee.distributor.name);
                                    workbook.sheet("KPI").row(rowBegin).cell(6).value(employee.code);
                                    workbook.sheet("KPI").row(rowBegin).cell(7).value(employee.fullname);
                                    workbook.sheet("KPI").row(rowBegin).cell(8).value(employee.detail_type_employee && employee.detail_type_employee.name);

                                    // Tìm trong bảng nhân viên HR
                                    const info_hr = await EMPLOYEE_HR_COLL.findOne({ employee: employee._id });
                                    workbook.sheet("KPI").row(rowBegin).cell(9).value(info_hr?.code);
                                    workbook.sheet("KPI").row(rowBegin).cell(10).value(info_hr?.fullname);

                                    let amountSaleinRequire  = 0;
                                    let amountSaleinActually = 0
                                    let amountPresentRequire = 0;
                                    let amountPresentActually= 0
                                    let amountSBDRequire     = 0
                                    let amountSBDActually    = 0
                                    let amountMHTTRequire     = 0
                                    let amountMHTTActually    = 0

                                    let percentSaleInActually = 0;
                                    let percentPresentActually = 0;
                                    let percentSPDActually = 0;
                                    let percentMHTTActually = 0;

                                    // chỉ tiêu sellin
                                    listSaleinRequire.forEach((saleinRequire )=> {
                                        if(saleinRequire.employee && saleinRequire.employee.toString() == employee._id.toString()){
                                            workbook.sheet("KPI").row(rowBegin).cell(11).value(saleinRequire.amount);
                                            amountSaleinRequire = saleinRequire.amount;
                                        }
                                    })

                                    // chỉ tiêu sellin thự đạt
                                    listSaleinActually.forEach((saleinActually )=> {
                                        if(saleinActually._id.employee && saleinActually._id.employee.toString() == employee._id.toString()){
                                            workbook.sheet("KPI").row(rowBegin).cell(12).value(saleinActually.total);
                                            amountSaleinActually = saleinActually.total
                                        }
                                    })

                                    if(amountSaleinRequire && amountSaleinActually){
                                        let percentSaleInCurrent = amountSaleinActually/amountSaleinRequire*100;
                                        let maxPercentSaleIn     = listCriteriasCampaign[2].maxPercent;
                                        let percentSaleIn        = listCriteriasCampaign[2].percent;
                                        let percentSaleInAfer    = percentSaleInCurrent > maxPercentSaleIn ? maxPercentSaleIn : percentSaleInCurrent

                                        percentSaleInActually = percentSaleInAfer * percentSaleIn / 100;
                                        workbook.sheet("KPI").row(rowBegin).cell(13).value(percentSaleInAfer);
                                    }

                                    // chỉ tiêu trưng bày
                                    listPresentRequire.forEach((presentRequire )=> {
                                        if(presentRequire.employee && presentRequire.employee.toString() == employee._id.toString()){
                                            workbook.sheet("KPI").row(rowBegin).cell(14).value(presentRequire.amount);
                                            amountPresentRequire = presentRequire.amount;
                                        }
                                    })


                                    // chỉ tiêu trưng bày thự đạt
                                    listPresentActually.forEach((presentActually )=> {
                                        if(presentActually.employee && presentActually.employee.toString() == employee._id.toString()){
                                            workbook.sheet("KPI").row(rowBegin).cell(15).value(presentActually.amount);
                                            amountPresentActually = presentActually.amount
                                        }
                                    })

                                    if(amountPresentRequire && amountPresentActually){
                                        let percentPresentCurrent = amountPresentActually/amountPresentRequire*100;
                                        let maxPercentPresent     = listCriteriasCampaign[1].maxPercent;
                                        let percentPersent        = listCriteriasCampaign[1].percent;
                                        let percentPersentAfer     = percentPresentCurrent > maxPercentPresent ? maxPercentPresent : percentPresentCurrent

                                        percentPresentActually = percentPersentAfer * percentPersent / 100;
                                        workbook.sheet("KPI").row(rowBegin).cell(16).value(percentPersentAfer);
                                    }

                                    /**
                                     * ============================MHTT(salein)=======================
                                     */
                                    // chỉ tiêu mhtt(salein)
                                    listMHTTRequire.forEach((mhttRequireItem )=> {
                                        if(mhttRequireItem.employee && mhttRequireItem.employee.toString() == employee._id.toString()){
                                            workbook.sheet("KPI").row(rowBegin).cell(17).value(mhttRequireItem.amount);
                                            amountMHTTRequire = mhttRequireItem.amount;
                                        }
                                    })
                                      // MHTT(sellin) thực đạt
                                    listSaleinActually__MHTT.forEach((mhttSaleinActually )=> {
                                        if(mhttSaleinActually._id.employee && mhttSaleinActually._id.employee.toString() == employee._id.toString()){
                                            workbook.sheet("KPI").row(rowBegin).cell(18).value(mhttSaleinActually.total);
                                            amountMHTTActually = mhttSaleinActually.total
                                        }
                                    })

                                    if(amountMHTTRequire && amountMHTTActually){
                                        let percentSaleInCurrent = amountMHTTActually/amountMHTTRequire*100;
                                        let maxPercentSaleIn     = listCriteriasCampaign[0].maxPercent;
                                        let percentSaleIn        = listCriteriasCampaign[0].percent;
                                        let percentSaleInAfer    = percentSaleInCurrent > maxPercentSaleIn ? maxPercentSaleIn : percentSaleInCurrent

                                        percentMHTTActually = percentSaleInAfer * percentSaleIn / 100;
                                        workbook.sheet("KPI").row(rowBegin).cell(19).value(percentSaleInAfer);
                                    }
                                    /**
                                     * ==========================(end)MHTT(salein) =======================
                                     */

                                    // ===========================================================//
                                    // ========================(start)SBD workspace===============//
                                    // ===========================================================//

                                    // workbook.sheet("KPI").row(rowBegin).cell(17).value(listSBDRequire.length);
                                    // amountSBDRequire = listSBDRequire.length

                                    // listSBDActually.forEach((SBDActually)=> {
                                    //     if(SBDActually?._id?.toString() == employee?._id?.toString()){
                                    //         workbook.sheet("KPI").row(rowBegin).cell(18).value(SBDActually.products.length);
                                    //         amountSBDActually = SBDActually.products.length;
                                    //     }
                                    // })
                                    
                                    // if(amountSBDRequire && amountSBDActually){
                                    //     workbook.sheet("KPI").row(rowBegin).cell(19).value(amountSBDActually/amountSBDRequire*100);
                                    //     let percentSBDCurrent = amountPresentActually/amountPresentRequire*100;
                                    //     let maxSBDPresent     = listCriteriasCampaign[0].maxPercent;
                                    //     let percentSBD        = listCriteriasCampaign[0].percent;
                                    //     let percentPersentAfer     = percentSBDCurrent > maxSBDPresent ? maxSBDPresent : percentSBDCurrent

                                    //     percentSPDActually = percentPersentAfer * percentSBD / 100
                                    //     workbook.sheet("KPI").row(rowBegin).cell(16).value(percentPersentAfer);
                                    // }
                                    // ========================(end)SBD workspace===============//


                                    workbook.sheet("KPI").row(rowBegin).cell(20).value(percentSaleInActually + percentPresentActually + percentMHTTActually);
                                    rowBegin++;
                                    index ++;
                                }
                            };

                            let random = new Date().getTime();
                            let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/',`Report_kpi_${random}.xlsx`);
                            workbook.toFileAsync(pathWriteFile)
                                .then(_ => {
                                    // Download file from server
                                    res.download(pathWriteFile, function (err) {

                                        if (err) {
                                            console.log(err);
                                        } else {
                                            // Remove file on server
                                            fs.unlinkSync(pathWriteFile);
                                        }    
                                    });
                                });      
                        });
                    }]
                },
            },
        }
    }
};