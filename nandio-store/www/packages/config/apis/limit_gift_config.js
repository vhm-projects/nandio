"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    CF_ROUTINGS_LIMIT_GIFT_CONFIG
} = require('../constants/limit_gift_config/limit_gift_config.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const LIMIT_GIFT_CONFIG_MODEL = require('../models/limit_gift_config').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */
const {
    EMPLOYEE_COLL
} = require('../../employee');

const {
    PRODUCT_COLL
} = require('../../product');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ LIMIT_GIFT_CONFIG  ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: Insert Limit_gift_config (API, VIEW)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.ADD_LIMIT_GIFT_CONFIG]: {
                config: {
                    scopes: ['create:limit_gift_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Limit_gift_config',
                    code: CF_ROUTINGS_LIMIT_GIFT_CONFIG.ADD_LIMIT_GIFT_CONFIG,
                    inc: path.resolve(__dirname, '../views/limit_gift_config/add_limit_gift_config.ejs')
                },
                methods: {
                    get: [async function(req, res) {


                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_LIMIT_GIFT_CONFIG
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            employee,
                            sku,
                            amount,
                            month,
                            year,
                        } = req.body;


                        let infoAfterInsert = await LIMIT_GIFT_CONFIG_MODEL.insert({
                            employee,
                            sku,
                            amount,
                            month,
                            year,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Limit_gift_config By Id (API, VIEW)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.UPDATE_LIMIT_GIFT_CONFIG_BY_ID]: {
                config: {
                    scopes: ['update:limit_gift_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Limit_gift_config',
                    code: CF_ROUTINGS_LIMIT_GIFT_CONFIG.UPDATE_LIMIT_GIFT_CONFIG_BY_ID,
                    inc: path.resolve(__dirname, '../views/limit_gift_config/update_limit_gift_config.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            limit_gift_configID
                        } = req.query;

                        let infoLimit_gift_config = await LIMIT_GIFT_CONFIG_MODEL.getInfoById(limit_gift_configID);
                        if (infoLimit_gift_config.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionEmployee = {
                            state: 1,
                            status: 1
                        };
                        if (infoLimit_gift_config.data.employee) {

                            conditionEmployee._id = infoLimit_gift_config.data.employee._id;

                        }

                        let listEmployees = await EMPLOYEE_COLL
                            .find(conditionEmployee)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionSku = {
                            state: 1,
                            status: 1
                        };
                        if (infoLimit_gift_config.data.sku) {

                            conditionSku._id = infoLimit_gift_config.data.sku._id;

                        }

                        let listSkus = await PRODUCT_COLL
                            .find(conditionSku)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoLimit_gift_config: infoLimit_gift_config.data || {},

                            listEmployees,
                            listSkus,
                            CF_ROUTINGS_LIMIT_GIFT_CONFIG
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            limit_gift_configID,
                            employee,
                            sku,
                            amount,
                            month,
                            year,
                        } = req.body;


                        const infoAfterUpdate = await LIMIT_GIFT_CONFIG_MODEL.update({
                            limit_gift_configID,
                            employee,
                            sku,
                            amount,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Limit_gift_config By Id (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.UPDATE_LIMIT_GIFT_CONFIG_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            limit_gift_configID,
                            employee,
                            sku,
                            amount,
                            month,
                            year,
                        } = req.body;


                        const infoAfterUpdate = await LIMIT_GIFT_CONFIG_MODEL.updateNotRequire({
                            limit_gift_configID,
                            employee,
                            sku,
                            amount,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Limit_gift_config By Id (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.DELETE_LIMIT_GIFT_CONFIG_BY_ID]: {
                config: {
                    scopes: ['delete:limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            limit_gift_configID
                        } = req.params;

                        const infoAfterDelete = await LIMIT_GIFT_CONFIG_MODEL.deleteById(limit_gift_configID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Limit_gift_config By List Id (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.DELETE_LIMIT_GIFT_CONFIG_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            limit_gift_configID
                        } = req.body;

                        const infoAfterDelete = await LIMIT_GIFT_CONFIG_MODEL.deleteByListId(limit_gift_configID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Limit_gift_config By Id (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.GET_INFO_LIMIT_GIFT_CONFIG_BY_ID]: {
                config: {
                    scopes: ['read:info_limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            limit_gift_configID
                        } = req.params;

                        const infoLimit_gift_configById = await LIMIT_GIFT_CONFIG_MODEL.getInfoById(limit_gift_configID);
                        res.json(infoLimit_gift_configById);
                    }]
                },
            },

            /**
             * Function: Get List Limit_gift_config (API, VIEW)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.GET_LIST_LIMIT_GIFT_CONFIG]: {
                config: {
                    scopes: ['read:list_limit_gift_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách giới hạn quà tặng',
                    code: CF_ROUTINGS_LIMIT_GIFT_CONFIG.GET_LIST_LIMIT_GIFT_CONFIG,
                    inc: path.resolve(__dirname, '../views/limit_gift_config/list_limit_gift_configs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            amountFromNumber,
                            amountToNumber,
                            typeGetList
                        } = req.query;

                        let listLimit_gift_configs = [];
                        if (typeGetList === 'FILTER') {
                            listLimit_gift_configs = await LIMIT_GIFT_CONFIG_MODEL.getListByFilter({
                                amountFromNumber,
                                amountToNumber,
                            });
                        } else {
                            listLimit_gift_configs = await LIMIT_GIFT_CONFIG_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listLimit_gift_configs: listLimit_gift_configs.data || [],

                        });
                    }]
                },
            },

            /**
             * Function: Get List Limit_gift_config By Field (API, VIEW)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.GET_LIST_LIMIT_GIFT_CONFIG_BY_FIELD]: {
                config: {
                    scopes: ['read:list_limit_gift_config'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Limit_gift_config by field isStatus',
                    code: CF_ROUTINGS_LIMIT_GIFT_CONFIG.GET_LIST_LIMIT_GIFT_CONFIG_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/limit_gift_config/list_limit_gift_configs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            amountFromNumber,
                            amountToNumber,
                            type
                        } = req.query;

                        let listLimit_gift_configs = await LIMIT_GIFT_CONFIG_MODEL.getListByFilter({
                            amountFromNumber,
                            amountToNumber,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listLimit_gift_configs: listLimit_gift_configs.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Limit_gift_config Server Side (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.GET_LIST_LIMIT_GIFT_CONFIG_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listLimit_gift_configServerSide = await LIMIT_GIFT_CONFIG_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(listLimit_gift_configServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Limit_gift_config Import (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.GET_LIST_LIMIT_GIFT_CONFIG_IMPORT]: {
                config: {
                    scopes: ['read:list_limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listLimit_gift_configImport = await LIMIT_GIFT_CONFIG_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listLimit_gift_configImport);
                    }]
                },
            },

            /**
             * Function: Get List Limit_gift_config Excel Server Side (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.GET_LIST_LIMIT_GIFT_CONFIG_EXCEL]: {
                config: {
                    scopes: ['read:list_limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = LIMIT_GIFT_CONFIG_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Limit_gift_config Excel Export (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.DOWNLOAD_LIST_LIMIT_GIFT_CONFIG_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'limit_gift_config'
                        });

                        let conditionObj = LIMIT_GIFT_CONFIG_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listLimit_gift_config = await LIMIT_GIFT_CONFIG_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listLimit_gift_config)
                    }]
                },
            },

            /**
             * Function: Setting Limit_gift_config Excel Import (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.SETTING_FILE_LIMIT_GIFT_CONFIG_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = LIMIT_GIFT_CONFIG_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Limit_gift_config Excel Import (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.DOWNLOAD_FILE_LIMIT_GIFT_CONFIG_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { opts } = req.query;
                        let condition = JSON.parse(opts);

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'limit_gift_config'
                        });
                       
                        let listLimit_gift_configImport = await LIMIT_GIFT_CONFIG_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            opts: condition
                        });

                        res.download(listLimit_gift_configImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listLimit_gift_configImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Limit_gift_config Excel Import (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.CREATE_LIMIT_GIFT_CONFIG_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {
                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'limit_gift_config'
                        });
                                            
                        let infoLimit_gift_configAfterImport = await LIMIT_GIFT_CONFIG_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'limit_gift_config',
                        });

                        res.json(infoLimit_gift_configAfterImport);
                    }]
                },
            },

            /**
             * Function: API Delete Limit_gift_config
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.API_DELETE_LIMIT_GIFT_CONFIG]: {
                config: {
                    scopes: ['delete:limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            limit_gift_configID
                        } = req.params;

                        const infoAfterDelete = await LIMIT_GIFT_CONFIG_MODEL.deleteLimit_gift_config(limit_gift_configID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: API Get info Limit_gift_config
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.API_GET_INFO_LIMIT_GIFT_CONFIG]: {
                config: {
                    scopes: ['read:info_limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            limit_gift_configID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoLimit_gift_config = await LIMIT_GIFT_CONFIG_MODEL.getInfoLimit_gift_config({
                            limit_gift_configID,
                            select,
                            filter,
                            explain
                        });
                        res.json(infoLimit_gift_config);
                    }]
                },
            },

            /**
             * Function: API Get info Limit_gift_config
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.API_GET_LIST_LIMIT_GIFT_CONFIGS]: {
                config: {
                    scopes: ['read:list_limit_gift_config'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listLimit_gift_configs = await LIMIT_GIFT_CONFIG_MODEL.getListLimit_gift_configs({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listLimit_gift_configs);
                    }]
                },
            },

            [CF_ROUTINGS_LIMIT_GIFT_CONFIG.DOWNLOAD_LIMIT_GIFT_CONFIG_EXCEL]: {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { opt } = req.query;
                        opt = JSON.parse(opt);
                        
                        let response = await LIMIT_GIFT_CONFIG_MODEL.downloadLimit_gift_configsExcel({ 
                           ...opt
                        });

                        res.json(response);
                    }]
                },
            },

        }
    }
};