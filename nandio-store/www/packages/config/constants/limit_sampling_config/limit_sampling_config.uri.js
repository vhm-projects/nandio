const BASE_ROUTE = '/limit_sampling_config';
const API_BASE_ROUTE = '/api/limit_sampling_config';

const CF_ROUTINGS_LIMIT_SAMPLING_CONFIG = {
    ADD_LIMIT_SAMPLING_CONFIG: `${BASE_ROUTE}/add-limit_sampling_config`,
    UPDATE_LIMIT_SAMPLING_CONFIG_BY_ID: `${BASE_ROUTE}/update-limit_sampling_config-by-id`,
    DELETE_LIMIT_SAMPLING_CONFIG_BY_ID: `${BASE_ROUTE}/delete/:limit_sampling_configID`,

    GET_INFO_LIMIT_SAMPLING_CONFIG_BY_ID: `${BASE_ROUTE}/info/:limit_sampling_configID`,
    GET_LIST_LIMIT_SAMPLING_CONFIG: `${BASE_ROUTE}/list-limit_sampling_config`,
    GET_LIST_LIMIT_SAMPLING_CONFIG_BY_FIELD: `${BASE_ROUTE}/list-limit_sampling_config/:field/:value`,
    GET_LIST_LIMIT_SAMPLING_CONFIG_SERVER_SIDE: `${BASE_ROUTE}/list-limit_sampling_config-server-side`,

    UPDATE_LIMIT_SAMPLING_CONFIG_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-limit_sampling_config-by-id-v2`,
    DELETE_LIMIT_SAMPLING_CONFIG_BY_LIST_ID: `${BASE_ROUTE}/delete-limit_sampling_config-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_LIMIT_SAMPLING_CONFIG_EXCEL: `${BASE_ROUTE}/list-limit_sampling_config-excel`,
    DOWNLOAD_LIST_LIMIT_SAMPLING_CONFIG_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-limit_sampling_config-excel-export`,
    DOWNLOAD_LIMIT_SAMPLING_CONFIG_EXCEL: `${BASE_ROUTE}/dowload-limit_sampling_config-excel`,

    // IMPORT EXCEL
    GET_LIST_LIMIT_SAMPLING_CONFIG_IMPORT: `${BASE_ROUTE}/list-limit_sampling_config-import`,
    SETTING_FILE_LIMIT_SAMPLING_CONFIG_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-limit_sampling_config-import-setting`,
    DOWNLOAD_FILE_LIMIT_SAMPLING_CONFIG_EXCEL_IMPORT: `${BASE_ROUTE}/list-limit_sampling_config-import-dowload`,
    CREATE_LIMIT_SAMPLING_CONFIG_IMPORT_EXCEL: `${BASE_ROUTE}/create-limit_sampling_config-import-excel`,

    API_GET_INFO_LIMIT_SAMPLING_CONFIG: `${API_BASE_ROUTE}/info-limit_sampling_config/:limit_sampling_configID`,
    API_GET_LIST_LIMIT_SAMPLING_CONFIGS: `${API_BASE_ROUTE}/list-limit_sampling_configs`,
    API_DELETE_LIMIT_SAMPLING_CONFIG: `${API_BASE_ROUTE}/delete-limit_sampling_config/:limit_sampling_configID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_LIMIT_SAMPLING_CONFIG = CF_ROUTINGS_LIMIT_SAMPLING_CONFIG;