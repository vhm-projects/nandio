const BASE_ROUTE = '/limit_gift_config';
const API_BASE_ROUTE = '/api/limit_gift_config';

const CF_ROUTINGS_LIMIT_GIFT_CONFIG = {
    ADD_LIMIT_GIFT_CONFIG: `${BASE_ROUTE}/add-limit_gift_config`,
    UPDATE_LIMIT_GIFT_CONFIG_BY_ID: `${BASE_ROUTE}/update-limit_gift_config-by-id`,
    DELETE_LIMIT_GIFT_CONFIG_BY_ID: `${BASE_ROUTE}/delete/:limit_gift_configID`,

    GET_INFO_LIMIT_GIFT_CONFIG_BY_ID: `${BASE_ROUTE}/info/:limit_gift_configID`,
    GET_LIST_LIMIT_GIFT_CONFIG: `${BASE_ROUTE}/list-limit_gift_config`,
    GET_LIST_LIMIT_GIFT_CONFIG_BY_FIELD: `${BASE_ROUTE}/list-limit_gift_config/:field/:value`,
    GET_LIST_LIMIT_GIFT_CONFIG_SERVER_SIDE: `${BASE_ROUTE}/list-limit_gift_config-server-side`,

    UPDATE_LIMIT_GIFT_CONFIG_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-limit_gift_config-by-id-v2`,
    DELETE_LIMIT_GIFT_CONFIG_BY_LIST_ID: `${BASE_ROUTE}/delete-limit_gift_config-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_LIMIT_GIFT_CONFIG_EXCEL: `${BASE_ROUTE}/list-limit_gift_config-excel`,
    DOWNLOAD_LIST_LIMIT_GIFT_CONFIG_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-limit_gift_config-excel-export`,
    DOWNLOAD_LIMIT_GIFT_CONFIG_EXCEL: `${BASE_ROUTE}/dowload-limit_gift_config-excel`,

    // IMPORT EXCEL
    GET_LIST_LIMIT_GIFT_CONFIG_IMPORT: `${BASE_ROUTE}/list-limit_gift_config-import`,
    SETTING_FILE_LIMIT_GIFT_CONFIG_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-limit_gift_config-import-setting`,
    DOWNLOAD_FILE_LIMIT_GIFT_CONFIG_EXCEL_IMPORT: `${BASE_ROUTE}/list-limit_gift_config-import-dowload`,
    CREATE_LIMIT_GIFT_CONFIG_IMPORT_EXCEL: `${BASE_ROUTE}/create-limit_gift_config-import-excel`,

    API_DELETE_LIMIT_GIFT_CONFIG: `${API_BASE_ROUTE}/delete-limit_gift_config/:limit_gift_configID`,
    API_GET_INFO_LIMIT_GIFT_CONFIG: `${API_BASE_ROUTE}/info-limit_gift_config/:limit_gift_configID`,
    API_GET_LIST_LIMIT_GIFT_CONFIGS: `${API_BASE_ROUTE}/list-limit_gift_configs`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_LIMIT_GIFT_CONFIG = CF_ROUTINGS_LIMIT_GIFT_CONFIG;