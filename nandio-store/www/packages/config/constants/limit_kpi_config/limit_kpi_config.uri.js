const BASE_ROUTE = '/limit_kpi_config';
const API_BASE_ROUTE = '/api/limit_kpi_config';

const CF_ROUTINGS_LIMIT_KPI_CONFIG = {
    ADD_LIMIT_KPI_CONFIG: `${BASE_ROUTE}/add-limit_kpi_config`,
    UPDATE_LIMIT_KPI_CONFIG_BY_ID: `${BASE_ROUTE}/update-limit_kpi_config-by-id`,
    DELETE_LIMIT_KPI_CONFIG_BY_ID: `${BASE_ROUTE}/delete/:limit_kpi_configID`,

    GET_INFO_LIMIT_KPI_CONFIG_BY_ID: `${BASE_ROUTE}/info/:limit_kpi_configID`,
    GET_LIST_LIMIT_KPI_CONFIG: `${BASE_ROUTE}/list-limit_kpi_config`,
    GET_LIST_LIMIT_KPI_CONFIG_BY_FIELD: `${BASE_ROUTE}/list-limit_kpi_config/:field/:value`,
    GET_LIST_LIMIT_KPI_CONFIG_SERVER_SIDE: `${BASE_ROUTE}/list-limit_kpi_config-server-side`,

    UPDATE_LIMIT_KPI_CONFIG_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-limit_kpi_config-by-id-v2`,
    DELETE_LIMIT_KPI_CONFIG_BY_LIST_ID: `${BASE_ROUTE}/delete-limit_kpi_config-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_LIMIT_KPI_CONFIG_EXCEL: `${BASE_ROUTE}/list-limit_kpi_config-excel`,
    DOWNLOAD_LIST_LIMIT_KPI_CONFIG_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-limit_kpi_config-excel-export`,

    // IMPORT EXCEL
    GET_LIST_LIMIT_KPI_CONFIG_IMPORT: `${BASE_ROUTE}/list-limit_kpi_config-import`,
    SETTING_FILE_LIMIT_KPI_CONFIG_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-limit_kpi_config-import-setting`,
    DOWNLOAD_FILE_LIMIT_KPI_CONFIG_EXCEL_IMPORT: `${BASE_ROUTE}/list-limit_kpi_config-import-dowload`,
    CREATE_LIMIT_KPI_CONFIG_IMPORT_EXCEL: `${BASE_ROUTE}/create-limit_kpi_config-import-excel`,
    
    API_GET_INFO_LIMIT_KPI_CONFIG: `${API_BASE_ROUTE}/info-limit_kpi_config/:limit_kpi_configID`,
    API_GET_LIST_LIMIT_KPI_CONFIGS: `${API_BASE_ROUTE}/list-limit_kpi_config`,

    API_GET_LIST_KPI_BY_FILTER: `${API_BASE_ROUTE}/list-kpi-by-filter`,

    /**
     * API này tương tự 'API_GET_LIST_KPI_BY_FILTER', do lần thay đổi của A Tuấn (SALEIN, TRƯNG  BÀY, SBD), nên sẽ dùng KPI mới 'API_GET_LIST_KPI_NEW'
     */
    API_GET_LIST_KPI_REQUIRE_NEW: `${API_BASE_ROUTE}/list-kpi-require-new`,
    API_GET_LIST_KPI_ACTUALYY_NEW: `${API_BASE_ROUTE}/list-kpi-actually-new`,
    REPORT_KPI_NEW: `${API_BASE_ROUTE}/report-kpi-new`,
    REPORT_KPI: `${API_BASE_ROUTE}/report-kpi`,

    /**
     * API(API_INVENTORY_WITH_SKU) này sẽ lấy được danh sách SKU + tồn kho theo từng SKU
     *  -> hiển thị tại 2 màn hình  
     *      1/ Quản lý Sampling
     *      2/ Quản lý Quà Tặng
     */
    API_INVENTORY_WITH_SKU: `${API_BASE_ROUTE}/inventory-with-sku`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_LIMIT_KPI_CONFIG = CF_ROUTINGS_LIMIT_KPI_CONFIG;