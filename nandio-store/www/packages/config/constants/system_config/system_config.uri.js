const BASE_ROUTE = '/system_config';
const API_BASE_ROUTE = '/api/system_config';

const CF_ROUTINGS_SYSTEM_CONFIG = {
    ADD_SYSTEM_CONFIG: `${BASE_ROUTE}/add-system_config`,
    UPDATE_SYSTEM_CONFIG_BY_ID: `${BASE_ROUTE}/update-system_config-by-id`,
    DELETE_SYSTEM_CONFIG_BY_ID: `${BASE_ROUTE}/delete/:system_configID`,

    GET_INFO_SYSTEM_CONFIG_BY_ID: `${BASE_ROUTE}/info/:system_configID`,
    GET_LIST_SYSTEM_CONFIG: `${BASE_ROUTE}/list-system_config`,
    GET_LIST_SYSTEM_CONFIG_BY_FIELD: `${BASE_ROUTE}/list-system_config/:field/:value`,
    GET_LIST_SYSTEM_CONFIG_SERVER_SIDE: `${BASE_ROUTE}/list-system_config-server-side`,

    UPDATE_SYSTEM_CONFIG_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-system_config-by-id-v2`,
    DELETE_SYSTEM_CONFIG_BY_LIST_ID: `${BASE_ROUTE}/delete-system_config-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_SYSTEM_CONFIG_EXCEL: `${BASE_ROUTE}/list-system_config-excel`,
    DOWNLOAD_LIST_SYSTEM_CONFIG_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-system_config-excel-export`,

    // IMPORT EXCEL
    GET_LIST_SYSTEM_CONFIG_IMPORT: `${BASE_ROUTE}/list-system_config-import`,
    SETTING_FILE_SYSTEM_CONFIG_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-system_config-import-setting`,
    DOWNLOAD_FILE_SYSTEM_CONFIG_EXCEL_IMPORT: `${BASE_ROUTE}/list-system_config-import-dowload`,
    CREATE_SYSTEM_CONFIG_IMPORT_EXCEL: `${BASE_ROUTE}/create-system_config-import-excel`,



    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_SYSTEM_CONFIG = CF_ROUTINGS_SYSTEM_CONFIG;