const BASE_ROUTE = '/criteria_campaign_config';
const API_BASE_ROUTE = '/api/criteria_campaign_config';

const CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG = {
    ADD_CRITERIA_CAMPAIGN_CONFIG: `${BASE_ROUTE}/add-criteria_campaign_config`,
    UPDATE_CRITERIA_CAMPAIGN_CONFIG_BY_ID: `${BASE_ROUTE}/update-criteria_campaign_config-by-id`,
    DELETE_CRITERIA_CAMPAIGN_CONFIG_BY_ID: `${BASE_ROUTE}/delete/:criteria_campaign_configID`,

    GET_INFO_CRITERIA_CAMPAIGN_CONFIG_BY_ID: `${BASE_ROUTE}/info/:criteria_campaign_configID`,
    GET_LIST_CRITERIA_CAMPAIGN_CONFIG: `${BASE_ROUTE}/list-criteria_campaign_config`,
    GET_LIST_CRITERIA_CAMPAIGN_CONFIG_BY_FIELD: `${BASE_ROUTE}/list-criteria_campaign_config/:field/:value`,
    GET_LIST_CRITERIA_CAMPAIGN_CONFIG_SERVER_SIDE: `${BASE_ROUTE}/list-criteria_campaign_config-server-side`,

    UPDATE_CRITERIA_CAMPAIGN_CONFIG_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-criteria_campaign_config-by-id-v2`,
    DELETE_CRITERIA_CAMPAIGN_CONFIG_BY_LIST_ID: `${BASE_ROUTE}/delete-criteria_campaign_config-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_CRITERIA_CAMPAIGN_CONFIG_EXCEL: `${BASE_ROUTE}/list-criteria_campaign_config-excel`,
    DOWNLOAD_LIST_CRITERIA_CAMPAIGN_CONFIG_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-criteria_campaign_config-excel-export`,

    // IMPORT EXCEL
    GET_LIST_CRITERIA_CAMPAIGN_CONFIG_IMPORT: `${BASE_ROUTE}/list-criteria_campaign_config-import`,
    SETTING_FILE_CRITERIA_CAMPAIGN_CONFIG_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-criteria_campaign_config-import-setting`,
    DOWNLOAD_FILE_CRITERIA_CAMPAIGN_CONFIG_EXCEL_IMPORT: `${BASE_ROUTE}/list-criteria_campaign_config-import-dowload`,
    CREATE_CRITERIA_CAMPAIGN_CONFIG_IMPORT_EXCEL: `${BASE_ROUTE}/create-criteria_campaign_config-import-excel`,

    API_GET_INFO_CRITERIA_CAMPAIGN_CONFIG_OF_ME: `${API_BASE_ROUTE}/info-criteria_campaign_config-of-me/:criteria_campaign_configID`,
    API_GET_LIST_CRITERIA_CAMPAIGN_CONFIGS: `${API_BASE_ROUTE}/list-criteria_campaign_configs`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG = CF_ROUTINGS_CRITERIA_CAMPAIGN_CONFIG;