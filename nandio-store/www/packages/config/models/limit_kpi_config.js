/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const path = require('path');
const fs = require('fs');
const XlsxPopulate = require('xlsx-populate');
const ObjectID = require("mongoose").Types.ObjectId;
const ISOdate = require("isodate");
const mongodb = require("mongodb");
const { MongoClient } = mongodb;
const formatCurrency = require('number-format.js')

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    colName,
} = require('../../../utils/utils');
const {
    isTrue
} = require('../../../tools/module/check');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const URL_DATABASE = process.env.URL_DATABASE || 'mongodb://localhost:27017';
const NAME_DATABASE = process.env.NAME_DATABASE || 'nandio_staging2';
const { TYPE_CRITERIA } = require('../constants/limit_kpi_config');

/**
 * COLLECTIONS
 */
const LIMIT_KPI_CONFIG_COLL             = require('../databases/limit_kpi_config-coll');
const PRODUCT_CRITERIA_COLL             = require('../../campaign_criteria/databases/products_criteria-coll');
const SALEOUT_COLL                      = require('../../history/databases/saleout-coll');
const HISTORY_EXCHANGE_GIFT_COLL        = require('../../history/databases/history_exchange_gift-coll');
const HISTORY_EXCHANGE_SAMPLING_COLL    = require('../../history/databases/history_exchange_sampling-coll');
const HISTORY_INIT_CUSTOMER_COLL        = require('../../history/databases/history_init_customer-coll');
const { EMPLOYEE_COLL }                 = require('../../employee');

const KPI_SALEIN_REQUIRE_COLL                   = require('../../kpi/databases/kpi_salein_require-coll');
const KPI_PRESENT_REQUIRE_COLL                  = require('../../kpi/databases/kpi_present_require-coll');
const KPI_SBD_REQUIRE_COLL                      = require('../../kpi/databases/kpi_sbd_require-coll');
const KPI_MHTT_REQUIRE_COLL                     = require('../../kpi/databases/kpi_mhtt_require-coll');
const KPI_MHTT_CONFIG_PRODUCTS_COLL             = require('../../kpi/databases/kpi_mhtt_config_products-coll');

const KPI_SALEIN_ACTUALLY_COLL                  = require('../../kpi/databases/kpi_salein_actually-coll');
const KPI_PRESENT_ACTUALLY_COLL                 = require('../../kpi/databases/kpi_present_actually-coll');
const KPI_SBD_ACTUALLY_COLL                     = require('../../kpi/databases/kpi_sbd_actually-coll');
const CRITERIA_CAMPAIGN_CONFIG_MODEL            = require('../../config/models/criteria_campaign_config').MODEL;

const LIMIT_GIFT_CONFIG_COLL                  = require('../../config/databases/limit_gift_config-coll');
const LIMIT_SAMPLING_CONFIG_COLL              = require('../../config/databases/limit_sampling_config-coll');
const INVENTORY_BEGIN_MONTH_COLL              = require('../../inventory_begin_month/databases/inventory_begin_month-coll');
const PRODUCT_COLL                            = require('../../product/databases/product-coll');

class Model extends BaseModel {
    constructor() {
        super(LIMIT_KPI_CONFIG_COLL);
    }

    /**
         * Tạo mới limit_kpi_config
		* @param {object} store
		* @param {object} employee
		* @param {object} criteria
		* @param {number} amount
		* @param {number} month
		* @param {number} year

         * @param {objectId} userCreate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        store,
        employee,
        criteria,
        amount = 0,
        month,
        year,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (!amount) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập chỉ tiêu'
                    });
                }

                if (!month) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tháng'
                    });
                }

                if (!year) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập năm'
                    });
                }





                if (!checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (!checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                if (!checkObjectIDs([criteria])) {
                    return resolve({
                        error: true,
                        message: 'tiêu chí không hợp lệ'
                    });
                }



                let dataInsert = {
                    store,
                    employee,
                    criteria,
                    amount,
                    month,
                    year,
                    userCreate
                };


                if (employee) {
                    const employeeInfo = await EMPLOYEE_COLL.findById(employee);
                    dataInsert.region = employeeInfo.region;
                    dataInsert.area = employeeInfo.area;
                    dataInsert.distributor = employeeInfo.distributor;
                }

                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo cấu hình kpi thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật limit_kpi_config 
         * @param {objectId} limit_kpi_configID
		* @param {object} store
		* @param {object} employee
		* @param {object} criteria
		* @param {number} amount
		* @param {number} month
		* @param {number} year

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        limit_kpi_configID,
        store,
        employee,
        criteria,
        amount,
        month,
        year,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([limit_kpi_configID])) {
                    return resolve({
                        error: true,
                        message: 'limit_kpi_configID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (!amount) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập chỉ tiêu cho limit_kpi_config'
                    });
                }

                if (!month) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tháng cho limit_kpi_config'
                    });
                }

                if (!year) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập năm cho limit_kpi_config'
                    });
                }





                if (!checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (!checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                if (!checkObjectIDs([criteria])) {
                    return resolve({
                        error: true,
                        message: 'tiêu chí không hợp lệ'
                    });
                }

                const checkExists = await LIMIT_KPI_CONFIG_COLL.findById(limit_kpi_configID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'cấu hình kpi không tồn tại'
                    });
                }


                let dataUpdate = {
                    userUpdate
                };
                store && (dataUpdate.store = store);
                employee && (dataUpdate.employee = employee);
                criteria && (dataUpdate.criteria = criteria);
                amount && (dataUpdate.amount = amount);
                month && (dataUpdate.month = month);
                year && (dataUpdate.year = year);

                if (employee) {
                    const employeeInfo = await EMPLOYEE_COLL.findById(employee);
                    dataUpdate.region = employeeInfo.region;
                    dataUpdate.area = employeeInfo.area;
                    dataUpdate.distributor = employeeInfo.distributor;
                }

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: limit_kpi_configID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật limit_kpi_config (không bắt buộc)
         * @param {objectId} limit_kpi_configID
		* @param {object} store
		* @param {object} employee
		* @param {object} criteria
		* @param {number} amount
		* @param {number} month
		* @param {number} year

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        limit_kpi_configID,
        store,
        employee,
        criteria,
        amount,
        month,
        year,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([limit_kpi_configID])) {
                    return resolve({
                        error: true,
                        message: 'limit_kpi_configID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                if (criteria && !checkObjectIDs([criteria])) {
                    return resolve({
                        error: true,
                        message: 'tiêu chí không hợp lệ'
                    });
                }

                const checkExists = await LIMIT_KPI_CONFIG_COLL.findById(limit_kpi_configID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'cấu hình kpi không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                store && (dataUpdate.store = store);
                employee && (dataUpdate.employee = employee);
                criteria && (dataUpdate.criteria = criteria);
                amount && (dataUpdate.amount = amount);
                month && (dataUpdate.month = month);
                year && (dataUpdate.year = year);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: limit_kpi_configID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa limit_kpi_config 
     * @param {objectId} limit_kpi_configID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(limit_kpi_configID) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 2
                };
                if (!checkObjectIDs([limit_kpi_configID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị limit_kpi_configID không hợp lệ'
                    });
                }


                const infoAfterDelete = await this.updateById(limit_kpi_configID, conditionObj);

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa limit_kpi_config 
     * @param {array} limit_kpi_configID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(limit_kpi_configID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(limit_kpi_configID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị limit_kpi_configID không hợp lệ'
                    });
                }

                const infoAfterDelete = await LIMIT_KPI_CONFIG_COLL.deleteMany({
                    _id: {
                        $in: limit_kpi_configID
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin limit_kpi_config 
     * @param {objectId} limit_kpi_configID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(limit_kpi_configID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([limit_kpi_configID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị limit_kpi_configID không hợp lệ'
                    });
                }

                const infoLimit_kpi_config = await LIMIT_KPI_CONFIG_COLL.findById(limit_kpi_configID)
                    .populate('region area distributor store employee criteria')

                if (!infoLimit_kpi_config) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin cấu hình kpi'
                    });
                }

                return resolve({
                    error: false,
                    data: infoLimit_kpi_config
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách limit_kpi_config 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listLimit_kpi_config = await LIMIT_KPI_CONFIG_COLL
                    .find({
                        state: 1
                    }).populate('region area distributor store employee criteria')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listLimit_kpi_config) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách cấu hình kpi'
                    });
                }

                return resolve({
                    error: false,
                    data: listLimit_kpi_config
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách limit_kpi_config theo bộ lọc
		* @param {number} amountFromNumber
		* @param {number} amountToNumber
		* @param {number} month
		* @param {number} year

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        amountFromNumber,
        amountToNumber,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };


                if (amountFromNumber && amountToNumber) {
                    conditionObj.amount = {
                        $gte: amountFromNumber,
                        $lt: amountToNumber,
                    };
                }


                const listLimit_kpi_configByFilter = await LIMIT_KPI_CONFIG_COLL
                    .find(conditionObj).populate('region area distributor store employee criteria')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listLimit_kpi_configByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách cấu hình kpi"
                    });
                }

                return resolve({
                    error: false,
                    data: listLimit_kpi_configByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách limit_kpi_config theo bộ lọc (server side)
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        filter,
        condition,
        page,
        limit,
        field,
        dir,
        region
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                region && (conditionObj.region = ObjectID(region));

                const skip = (page - 1) * limit;
                const totalLimit_kpi_config = await LIMIT_KPI_CONFIG_COLL.countDocuments(conditionObj);

                if(region) {
                    delete conditionObj.region;
                    conditionObj['region._id'] = ObjectID(region);
                }

                const listLimit_kpi_configByFilter = await LIMIT_KPI_CONFIG_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'criterias',
                            localField: 'criteria',
                            foreignField: '_id',
                            as: 'criteria'
                        }
                    },
                    {
                        $unwind: {
                            path: '$criteria',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listLimit_kpi_configByFilter) {
                    return resolve({
                        recordsTotal: totalLimit_kpi_config,
                        recordsFiltered: totalLimit_kpi_config,
                        data: []
                    });
                }

                const listLimit_kpi_configDataTable = listLimit_kpi_configByFilter.map((limit_kpi_config, index) => {

                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${limit_kpi_config._id}" type="checkbox" class="check-record check-record-${limit_kpi_config._id}" _index ="${index + 1}"><label for="${limit_kpi_config._id}"></label></div></td>`,
                        indexSTT: skip + index + 1,
                        region: limit_kpi_config.region ? limit_kpi_config.region.name : '',
                        area: limit_kpi_config.area ? limit_kpi_config.area.name : '',
                        distributor: limit_kpi_config.distributor ? limit_kpi_config.distributor.name : '',
                        store: limit_kpi_config.store ? limit_kpi_config.store.name : '',
                        employee: limit_kpi_config.employee ? limit_kpi_config.employee.fullname : '',
                        criteria: limit_kpi_config.criteria ? limit_kpi_config.criteria.name : '',
                        amount: formatCurrency('###,###.', limit_kpi_config.amount),
                        month: `${limit_kpi_config.month && limit_kpi_config.month.length > 50 ? limit_kpi_config.month.substr(0,50) + "..." : limit_kpi_config.month}`,
                        year: `${limit_kpi_config.year && limit_kpi_config.year.length > 50 ? limit_kpi_config.year.substr(0,50) + "..." : limit_kpi_config.year}`,

                        createAt: moment(limit_kpi_config.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalLimit_kpi_config,
                    recordsFiltered: totalLimit_kpi_config,
                    data: listLimit_kpi_configDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách limit_kpi_config theo bộ lọc import
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterImport({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };




                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }



                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalLimit_kpi_config = await LIMIT_KPI_CONFIG_COLL.countDocuments(conditionObj);

                const listLimit_kpi_configByFilter = await LIMIT_KPI_CONFIG_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'criterias',
                            localField: 'criteria',
                            foreignField: '_id',
                            as: 'criteria'
                        }
                    },
                    {
                        $unwind: {
                            path: '$criteria',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listLimit_kpi_configByFilter) {
                    return resolve({
                        recordsTotal: totalLimit_kpi_config,
                        recordsFiltered: totalLimit_kpi_config,
                        data: []
                    });
                }

                return resolve({
                    error: false,
                    recordsTotal: totalLimit_kpi_config,
                    recordsFiltered: totalLimit_kpi_config,
                    data: listLimit_kpi_configByFilter || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc limit_kpi_config
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc limit_kpi_config
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listLimit_kpi_configByFilter = await LIMIT_KPI_CONFIG_COLL.aggregate(arrayFilter)

                if (!listLimit_kpi_configByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listLimit_kpi_configByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listLimit_kpi_configByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('HH:mm DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Tải file import excel mẫu limit_kpi_config
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    fileImportExcelPreview({
        opts,
        arrayItemCustomerChoice
    }) {
        return new Promise(async resolve => {
            try {
                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition; // BỘ LỌC && LOẠI IMPORT
                let {
                    listFieldPrimaryKey
                } = condition; // DANH SÁCH PRIMARY KEY
                console.log({
                    opts, condition
                });

                // if (!listFieldPrimaryKey) {
                //     listFieldPrimaryKey = [];
                // }

                if (!isEmptyObject(opts)) {
                    condition.conditionDeleteImport.filter    = opts.filter;   // BỘ LỌC CỦA ADMIN
                    condition.conditionDeleteImport.condition = opts.condition // BỘ LỌC CỦA ADMIN
                }
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        let index = 0;
                        arrayItemCustomerChoice.map((elem) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            if (elem.dataDynamic && elem.dataDynamic.length) {
                                // console.log({
                                //     listFieldPrimaryKey,
                                //     elem
                                // });
                                listFieldPrimaryKey = listFieldPrimaryKey && listFieldPrimaryKey.length && listFieldPrimaryKey.filter(key => key != elem.nameFieldRef); // LỌC PRIMARY KEY MÀ KHÔNG PHẢI REF

                                elem.dataDynamic.map(item => {
                                    workbook.sheet("Report").row(1).cell(index + 1).value(item);
                                    index++;
                                })
                            } else {
                                workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                                index++;
                            }
                        });

                        if (isTrue(condition.checkDownloadDataOld)) { // KIỂM TRA CÓ ĐÍNH ĐÈM DỮ LIỆU CŨ THEO ĐIỀU KIỆN
                            let listItemImport = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].listItemImport; // LIST FIELD ĐÃ ĐƯỢC CẤU HÌNH
                            let {
                                arrayFilter
                            } = this.getConditionArrayFilterExcel(listItemImport, condition.conditionDeleteImport.filter, condition.conditionDeleteImport.condition); // LẤY RA ARRAY ARREGATE

                            let groupByLimit_kpi_config = {};
                            listFieldPrimaryKey.map(key => {
                                groupByLimit_kpi_config = {
                                    ...groupByLimit_kpi_config,
                                    [key]: '$' + key
                                }
                            });

                            arrayFilter = [
                                ...arrayFilter,
                                {
                                    $group: {
                                        _id: {
                                            groupByLimit_kpi_config
                                        },
                                        listData: {
                                            $addToSet: "$$CURRENT"
                                        },
                                    }
                                }
                            ];
                            // console.log({
                            //     condition,
                            //     arrayFilter
                            // });
                            const listLimit_kpi_configByFilter = await LIMIT_KPI_CONFIG_COLL.aggregate(arrayFilter);
                            console.log({
                                listLimit_kpi_configByFilter: listLimit_kpi_configByFilter[0].listData,
                                arrayItemCustomerChoice
                            });
                            listLimit_kpi_configByFilter && listLimit_kpi_configByFilter.length && listLimit_kpi_configByFilter.map((item, indexLimit_kpi_config) => {
                                let indexValue = 0;
                                
                                arrayItemCustomerChoice.map((elem, indexChoice) => {
                                    let variable = elem.name.split('.');

                                    if (elem.dataDynamic && elem.dataDynamic.length) { // KIỂM TRA FIELD CÓ CHỌN DYNAMIC
                                        let listLimitKPIConfigAfterFilter = item.listData && item.listData.length && item.listData.filter(value => {
                                            let valueOfField;
                                            if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                if (objDataOfVariable) {
                                                    valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                }
                                            } else {
                                                valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                            }
                                           
                                            if (elem.dataDynamic && elem.dataDynamic.includes(valueOfField)) { // CHECK NẾU VALUE === CỘT
                                                return value;
                                            }
                                        });

                                        // for (const kpi of listLimitKPIConfigAfterFilter) {
                                        //     console.log({ criteria: kpi.criteria });
                                        // }

                                        // console.log({
                                        //     // listLimitKPI: listLimitKPIConfigAfterFilter,
                                        //     dataDynamic: elem.dataDynamic,
                                        //     dataDynamicLenght: elem.dataDynamic.length,
                                        //     listLimitKPILenght: listLimitKPIConfigAfterFilter.length,
                                        // });

                                        let listValueExist = listLimitKPIConfigAfterFilter.map(kpi => {
                                            let valueOfField;
                                            if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                let objDataOfVariable = kpi[variable[0]] ? kpi[variable[0]] : '';
                                                if (objDataOfVariable) {
                                                    valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                }
                                            } else {
                                                valueOfField = kpi[variable[0]] ? kpi[variable[0]] : '';
                                            }
                                            return valueOfField;
                                        });
                                        console.log({
                                            listLimitKPIConfigAfterFilter,
                                            listValueExist
                                        });
                                        // kpi.criteria.name
                                        elem.dataDynamic.map(dynamic => {
                                        
                                            if (item.listData.length > elem.dataDynamic.length) { // KIỂM TRA ĐỘ DÀI CỦA DATA SO VỚI SỐ CỘT
                                                // TODO: XỬ LÝ NHIỀU DYNAMIC
                                                console.log({ ['IF_DATA_DYNAMIC']: item.listData.length > elem.dataDynamic.length });
                                            } else {
                                                // ARRAY DATA DYNAMIC
                                                for (const value of listLimitKPIConfigAfterFilter) {
                                                    let valueOfField;
                                                    if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                        let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                        if (objDataOfVariable) {
                                                            valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                        }
                                                    } else {
                                                        valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                                    }

                                                    if (valueOfField == dynamic) { // CHECK NẾU VALUE === CỘT
                                                        let valueImportDynamic = value[elem.variableChoice] ? value[elem.variableChoice] : '';
                                                        console.log('---------------- IF -----------------')
                                                        console.log({
                                                            valueOfField,
                                                            dynamic,
                                                            indexValue
                                                        });
                                                        // INSERT DỮ LIỆU VÀO BẢNG VỚI FIELD ĐƯỢC CHỌN THEO DẠNG DYNAMIC
                                                        workbook.sheet("Report").row(indexLimit_kpi_config + 2).cell(indexValue + 1).value(valueImportDynamic);
                                                        indexValue++;
                                                        break;
                                                    } else {
                                                        console.log('---------------- ELSE -----------------')
                                                        console.log({
                                                            valueOfField, 
                                                            dynamic,
                                                            indexValue
                                                        });

                                                        if(!listValueExist.includes(dynamic)){
                                                            indexValue++;
                                                            break;
                                                        }

                                                        // if (elem.dataDynamic.length > listLimitKPIConfigAfterFilter.length) {
                                                        //     indexValue++;
                                                        // } 
                                                        // else if (elem.dataDynamic.length == item.listData.length) {
                                                        //     console.log("===============================elem.dataDynamic.length == item.listData.length");
                                                        //     indexValue++;
                                                        //     // mark = true;
                                                        // }`
                                                    }
                                                }
                                            }
                                        })
                                    } else { // DẠNG STATIC
                                        let valueLimit_kpi_config;
                                        if (variable.length > 1) {
                                            let objDataOfVariable = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                            if (objDataOfVariable) {
                                                valueLimit_kpi_config = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                            }
                                        } else {
                                            valueLimit_kpi_config = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                        }

                                        workbook.sheet("Report").row(indexLimit_kpi_config + 2).cell(indexValue + 1).value(valueLimit_kpi_config);
                                        indexValue++;
                                    }
                                });
                            });
                        }

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    pathWriteFile,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Upload File Excel Import Lưu Dữ Liệu limit_kpi_config
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    importExcel({
        nameCollParent,
        arrayItemCustomerChoice,
        file,
    }) {
        return new Promise(async resolve => {
            try {

                XlsxPopulate.fromFileAsync(file.path)
                    .then(async workbook => {
                        let listData = [];
                        let index = 2;
                        let listFieldPrimaryKey      = []; //PRIMARY KEY
                        let conditionDeleteValuePrimaryKey = []; //array value của PRIMARY KEY từ file IMPORT
                        let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition;
                        let listFieldNameConditionDelete = [];
                        if (condition) {
                            listFieldPrimaryKey = condition.listFieldPrimaryKey && condition.listFieldPrimaryKey.length && condition.listFieldPrimaryKey;
                            listFieldNameConditionDelete = condition.conditionDeleteImport && condition.conditionDeleteImport.filter.map(item => item.fieldName);
                        }
                        console.log({
                            listFieldNameConditionDelete, condition
                        });
                        const client = await MongoClient.connect(URL_DATABASE);
                        const db     = client.db(NAME_DATABASE);

                        for (; true;) {
                            if (arrayItemCustomerChoice && arrayItemCustomerChoice.length) {
                                let conditionObj = {};
                                let conditionObj__PrimaryKey = {}; //value của PRIMARY KEY từ file IMPORT

                                let totalLength = 0;
                                arrayItemCustomerChoice.map((item, index) => {
                                    if (item.dataDynamic && item.dataDynamic.length) {
                                        totalLength += item.dataDynamic.length;
                                    } else {
                                        totalLength++;
                                    }
                                });

                                let indexOfListField = 0;
                                let checkIsRequire = false;
                                let arrayConditionObjDynamic = [];
                                let arrayConditionObjDynamic__PrimaryKey = [];

                                for (let i = 0; i < totalLength; i++) {
                                    if (arrayItemCustomerChoice[indexOfListField].dataDynamic && arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                        let indexOfDynamic = 1;
                                        for (let valueDynamic of arrayItemCustomerChoice[indexOfListField].dataDynamic) {
                                            let letter = colName(i);
                                            let indexOfCeil = letter.toUpperCase() + index;

                                            let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                            if (variable) {

                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }
                                               
                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: valueDynamic.trim()
                                                });
                                              
                                                let conditionOfOneValueDynamic = {
                                                    [arrayItemCustomerChoice[indexOfListField].variableChoice]: variable
                                                }
                                                // console.log({
                                                //     docs,
                                                //     collName,
                                                //     [arrayItemCustomerChoice[indexOfListField].variable]: valueDynamic.trim(),
                                                //     ___item: arrayItemCustomerChoice[indexOfListField]
                                                // });
                                                if (docs) {
                                                    conditionOfOneValueDynamic = {
                                                        ...conditionOfOneValueDynamic,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    }

                                                    // console.log({
                                                    //     nameFieldRef: [arrayItemCustomerChoice[indexOfListField].nameFieldRef],
                                                    //     listFieldNameConditionDelete
                                                    // });
                                                    // LẤY VALUE để xóa ở dữ liệu DYNAMIC
                                                    if (listFieldNameConditionDelete && listFieldNameConditionDelete.length && listFieldNameConditionDelete.includes(arrayItemCustomerChoice[indexOfListField].nameFieldRef)) {
                                                        arrayConditionObjDynamic__PrimaryKey = [
                                                            ...arrayConditionObjDynamic__PrimaryKey,
                                                            {
                                                                [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                            }
                                                        ]
                                                    }

                                                    // if (listFieldNameConditionDelete && listFieldNameConditionDelete.length && listFieldNameConditionDelete.includes(arrayItemCustomerChoice[indexOfListField].name)) {
                                                    //     conditionObj__PrimaryKey = {
                                                    //         ...conditionObj__PrimaryKey,
                                                    //         [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    //     }
                                                    // }
                                                }

                                                arrayConditionObjDynamic = [
                                                    ...arrayConditionObjDynamic,
                                                    conditionOfOneValueDynamic
                                                ];
                                            }

                                            if (indexOfDynamic < arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                                i++;
                                            }
                                            indexOfDynamic++;
                                        }
                                    } else {
                                        let letter = colName(i);
                                        let indexOfCeil = letter.toUpperCase() + index;
                                        let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                        // console.log({
                                        //     letter,
                                        //     indexOfCeil,
                                        //     variable
                                        // });
                                        if (arrayItemCustomerChoice[indexOfListField].isRequire && !variable) {
                                            checkIsRequire = true;
                                            break;
                                        }
                                        if (arrayItemCustomerChoice[indexOfListField].ref && arrayItemCustomerChoice[indexOfListField].ref != nameCollParent) {
                                            if (arrayItemCustomerChoice[indexOfListField].isRequire) {
                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];
                                                
                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }
                                              
                                                if (arrayItemCustomerChoice[indexOfListField].type == "text") {
                                                    variable = variable.toString().trim();
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: variable
                                                })
                                           
                                                if (docs) {
                                                    conditionObj = {
                                                        ...conditionObj,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                    };
                                                    
                                                    if (listFieldNameConditionDelete && listFieldNameConditionDelete.length && listFieldNameConditionDelete.includes(arrayItemCustomerChoice[indexOfListField].ref)) {
                                                        conditionObj__PrimaryKey = {
                                                            ...conditionObj__PrimaryKey,
                                                            [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                        }
                                                    }

                                                    if (arrayItemCustomerChoice[indexOfListField].mappingRef && arrayItemCustomerChoice[indexOfListField].mappingRef.length) {
                                                        arrayItemCustomerChoice[indexOfListField].mappingRef.map(mapping => {
                                                            conditionObj = {
                                                                ...conditionObj,
                                                                [mapping]: docs[mapping]
                                                            }
                                                        })
                                                    }
                                                } else {
                                                    checkIsRequire = true;
                                                    break;
                                                }
                                            }
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                [arrayItemCustomerChoice[indexOfListField].name]: variable
                                            };
                                            if (listFieldNameConditionDelete && listFieldNameConditionDelete.length && listFieldNameConditionDelete.includes(arrayItemCustomerChoice[indexOfListField].name)) {
                                                conditionObj__PrimaryKey = {
                                                    ...conditionObj__PrimaryKey,
                                                    [arrayItemCustomerChoice[indexOfListField].name]: variable
                                                }
                                            }
                                        }
                                    }

                                    indexOfListField++;
                                }
                                if (checkIsRequire) {
                                    break;
                                }

                                conditionObj = {
                                    ...conditionObj,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                }

                                let arrayCondditionObj = [];
                                if (arrayConditionObjDynamic && arrayConditionObjDynamic.length) {
                                    arrayConditionObjDynamic.map(item => {
                                        arrayCondditionObj = [
                                            ...arrayCondditionObj,
                                            {
                                                ...conditionObj,
                                                ...item
                                            }
                                        ];
                                    });

                                    // Xóa với điều kiện => với PRIMARY KEY
                                    if (listFieldNameConditionDelete && listFieldNameConditionDelete.length) {
                                        if (arrayConditionObjDynamic__PrimaryKey && arrayConditionObjDynamic__PrimaryKey.length) { //value của PRIMARY KEY với DYNAMIC REF 
                                            arrayConditionObjDynamic__PrimaryKey.map(item => {
                                                conditionDeleteValuePrimaryKey = [
                                                    ...conditionDeleteValuePrimaryKey,
                                                    {
                                                        ...conditionObj__PrimaryKey,
                                                        ...item
                                                    }
                                                ];
                                            });
                                        } else {
                                            conditionDeleteValuePrimaryKey = [
                                                ...conditionDeleteValuePrimaryKey,
                                                conditionObj__PrimaryKey
                                            ];
                                        }
                                    }
                                } else {
                                    arrayCondditionObj = [
                                        ...arrayCondditionObj,
                                        conditionObj
                                    ];
                                    if (listFieldNameConditionDelete && listFieldNameConditionDelete.length) {
                                        conditionDeleteValuePrimaryKey = [
                                            ...conditionDeleteValuePrimaryKey,
                                            conditionObj__PrimaryKey
                                        ];
                                    }
                                }
                                
                               
                                listData = [
                                    ...listData,
                                    ...arrayCondditionObj
                                ];

                                index++;
                            }
                        }
                        console.log({
                            conditionDeleteValuePrimaryKey,
                            listData
                        });
                     
                        await fs.unlinkSync(file.path);

                        if (listData.length) {
                            await this.changeDataImport({
                                condition,
                                listLimit_kpi_config: listData,
                                conditionDeleteValuePrimaryKey
                            });
                        } else {
                            return resolve({
                                error: true,
                                message: 'Import thất bại'
                            });
                        }

                        return resolve({
                            error: false,
                            message: 'Import thành công'
                        });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lưu Dữ Liệu Theo Lựa Chọn limit_kpi_config
     * @param {object} listLimit_kpi_config
     * @param {object} condition
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    changeDataImport({
        condition,
        listLimit_kpi_config,
        conditionDeleteValuePrimaryKey
    }) {
        return new Promise(async resolve => {
            try {
                if (isTrue(condition.delete)) { // XÓA DATA CŨ
                    if (isTrue(condition.deleteAll)) { // XÓA TẤT CẢ DỮ LIỆU
                        console.log("====================XÓA TẤT CẢ DỮ LIỆU====================");
                        let listDataAfterInsert = await LIMIT_KPI_CONFIG_COLL.insertMany(listLimit_kpi_config);
                       
                        if (listDataAfterInsert && listDataAfterInsert.length) {
                            let error = false;
                            listDataAfterInsert.map(item => {
                                if (!item) {
                                    error = true;
                                }
                            });
                            if (!error) {
                                let listLimitKPIID = listDataAfterInsert.map(item => item._id);
                                await LIMIT_KPI_CONFIG_COLL.deleteMany({
                                    _id: {
                                        $nin: listLimitKPIID
                                    }
                                });
                                return resolve({
                                    error: false,
                                    message: 'Insert success',
                                    data: listDataAfterInsert
                                });
                            } else {
                                return resolve({
                                    error: true,
                                    message: 'Insert failed',
                                    // data: listDataAfterInsert
                                });
                            }
                        } else {
                            return resolve({
                                error: true,
                                message: 'Insert failed',
                                // data: listDataAfterInsert
                            });
                        }
                      
                    } else { // XÓA VỚI ĐIỀU KIỆN
                        console.log("====================XÓA VỚI ĐIỀU KIỆN====================");

                        /**
                         * ===========================================================================
                         * =========================XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===========================================================================
                         */
                        let {
                            filter,
                            condition: conditionMultiple,
                        } = condition.conditionDeleteImport;
                        
                        let conditionObj = {
                            state: 1,
                            $or: []
                        };
                        

                        if (!filter || !filter.length) {
                            return resolve({
                                error: true,
                                message: 'Filter do not exist'
                            });
                        }


                        if (filter && filter.length) {
                            if (filter.length > 1) {

                                filter.map(filterObj => {
                                    if (filterObj.type === 'ref') {
                                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionFieldRef);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionFieldRef
                                            };
                                        }
                                    } else {
                                        const conditionByFilter = this.getConditionObj(filterObj);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionByFilter);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionByFilter
                                            };
                                        }
                                    }
                                });

                            } else {
                                let {
                                    type,
                                    ref,
                                    fieldRefName
                                } = filter[0];

                                if (type === 'ref') {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(ref, fieldRefName)
                                    };
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(filter[0])
                                    };
                                }
                            }
                        }

                        if (conditionObj.$or && !conditionObj.$or.length) {
                            delete conditionObj.$or;
                        }

                        // let listAfterDelete = await LIMIT_KPI_CONFIG_COLL.deleteMany({
                        //     ...conditionObj
                        // });
                        /**
                         * ===============================================================================
                         * =========================END XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===============================================================================
                         */
                        
                        if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                            let error = false;
                            let listLimit_kpi_configID = [];
                            for (let item of listLimit_kpi_config) {
                                let listConditionFindOneUpdate = {};
                                let {
                                    listFieldPrimaryKey
                                } = condition;
                                listFieldPrimaryKey.map(elem => {
                                    listConditionFindOneUpdate = {
                                        ...listConditionFindOneUpdate,
                                        [elem]: item[elem]
                                    }
                                });

                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    state: 1
                                }
                                console.log({
                                    item
                                });
                                let checkExist = await LIMIT_KPI_CONFIG_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                    $set: item
                                }, {
                                    upsert: true
                                });
                                if (!checkExist) {
                                    error = true;
                                } else {
                                    listLimit_kpi_configID = [
                                        ...listLimit_kpi_configID,
                                        checkExist._id
                                    ];
                                }
                            }

                            if (error) {
                                return resolve({
                                    error: true,
                                    message: 'Insert falied'
                                });
                            } else {
                                console.log({
                                    listLimit_kpi_configID
                                });
                                let listValueAfterDelete = conditionDeleteValuePrimaryKey && conditionDeleteValuePrimaryKey.length && conditionDeleteValuePrimaryKey.map(value => {
                                    return LIMIT_KPI_CONFIG_COLL.deleteMany({
                                        ...value,
                                        _id: {
                                            $nin: listLimit_kpi_configID
                                        }
                                    });
                                });
    
                                let result = await Promise.all(listValueAfterDelete);
                                console.log({
                                    result, listLimit_kpi_config
                                });
    
                                return resolve({
                                    error: false,
                                    message: 'Insert success'
                                });
                            }
                        } else { // INSERT CÁI MỚI
                            // let listAfterDelete = await LIMIT_KPI_CONFIG_COLL.deleteMany({
                            //     ...conditionObj
                            // });
                            console.log("====================INSERT CÁI MỚI 2====================");
                            let listDataAfterInsert = await LIMIT_KPI_CONFIG_COLL.insertMany(listLimit_kpi_config);
                            // console.log({
                            //     listDataAfterInsert
                            // });
                            if (listDataAfterInsert && listDataAfterInsert.length) {
                                let error = false;
                                listDataAfterInsert.map(item => {
                                    if (!item) {
                                        error = true;
                                    }
                                });
                                if (!error) {
                                    let listLimitKPIID = listDataAfterInsert.map(item => item._id);
                                    let listValueAfterDelete = conditionDeleteValuePrimaryKey && conditionDeleteValuePrimaryKey.length && conditionDeleteValuePrimaryKey.map(value => {
                                        return LIMIT_KPI_CONFIG_COLL.deleteMany({
                                            ...value,
                                            _id: {
                                                $nin: listLimitKPIID
                                            }
                                        });
                                    });
                                    let result = await Promise.all(listValueAfterDelete);

                                    // await LIMIT_KPI_CONFIG_COLL.deleteMany({
                                    //     ...conditionObj,
                                    //     _id: {
                                    //         $nin: listLimitKPIID
                                    //     }
                                    // });
                                    return resolve({
                                        error: false,
                                        message: 'Insert success',
                                        data: listDataAfterInsert
                                    });
                                } else {
                                    return resolve({
                                        error: true,
                                        message: 'Insert failed',
                                        // data: listDataAfterInsert
                                    });
                                }
                            } else {
                                return resolve({
                                    error: true,
                                    message: 'Insert failed',
                                    // data: listDataAfterInsert
                                });
                            }
                        }
                    }
                } else { // KHÔNG XÓA DATA CŨ
                    if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                        console.log("====================KIỂM TRA TỒN TẠI VÀ UPDATE====================");
                        for (let item of listLimit_kpi_config) {
                            let listConditionFindOneUpdate = {};
                            let {
                                listFieldPrimaryKey
                            } = condition;
                            listFieldPrimaryKey.map(elem => {
                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    [elem]: item[elem]
                                }
                            });

                            listConditionFindOneUpdate = {
                                ...listConditionFindOneUpdate,
                                state: 1
                            }

                            let checkExist = await LIMIT_KPI_CONFIG_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                $set: item
                            }, {
                                upsert: true
                            });
                        }
                        return resolve({
                            error: false,
                            message: 'Insert success'
                        });
                    } else { // INSERT CÁI MỚI
                        console.log("====================INSERT CÁI MỚI====================");
                        let listDataAfterInsert = await LIMIT_KPI_CONFIG_COLL.insertMany(listLimit_kpi_config);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    }
                }

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc limit_kpi_config
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
            $or: []
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            let mappingRef = [];
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                    mappingRef = element.mappingRef;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            fieldID: item.fieldID,
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            ref: item.coll,
                            variable: item.name,
                            nameFieldRef,
                            dataEnum: item.dataEnum,
                            isRequire: item.isRequire,
                            dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                            variableChoice: item.variableChoice,
                            nameCollChoice: item.nameCollChoice,
                            mappingRef: mappingRef,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                fieldID: item.fieldID,
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                ref: item.coll,
                                variable: item.name,
                                nameFieldRef,
                                dataEnum: item.dataEnum,
                                isRequire: item.isRequire,
                                dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                variableChoice: item.variableChoice,
                                nameCollChoice: item.nameCollChoice,
                                mappingRef: mappingRef,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    dataEnum: item.dataEnum,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {
            if (filter.length > 1) {

                filter.map(filterObj => {
                    if (filterObj.type === 'ref') {
                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionFieldRef);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionFieldRef
                            };
                        }
                    } else {
                        const conditionByFilter = this.getConditionObj(filterObj);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionByFilter);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionByFilter
                            };
                        }
                    }
                });

            } else {
                let {
                    type,
                    ref,
                    fieldRefName
                } = filter[0];

                if (type === 'ref') {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(ref, fieldRefName)
                    };
                } else {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(filter[0])
                    };
                }
            }
        }

        if (conditionObj.$or && !conditionObj.$or.length) {
            delete conditionObj.$or;
        }



        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let collName = pluralize.plural(item.coll);
                let checkPluralColl = collName[collName.length - 1];

                if (checkPluralColl.toLowerCase() != 's') {
                    collName += 's';
                }

                let lookup = [{
                        $lookup: {
                            from: collName,
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        let collNamePopulate = pluralize.plural(populate.coll);
                        let checkPluralColl = collNamePopulate[collNamePopulate.length - 1];

                        if (checkPluralColl.toLowerCase() != 's') {
                            collNamePopulate += 's';
                        }

                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: collNamePopulate,
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Lấy thông tin limit_kpi_config
     * @param {objectId} limit_kpi_configID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoLimit_kpi_config({
        limit_kpi_configID,
        select,
        filter = {},
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];
                let conditionObj = {
                    state: 1
                };

                if (!checkObjectIDs([limit_kpi_configID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị limit_kpi_configID không hợp lệ',
                        status: 400
                    });
                }

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                Object.keys(filter).map(key => {
                    if (!['region', 'area', 'distributor', 'store', 'employee', 'criteria', 'amountFromNumber', 'amountToNumber'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    region,
                    area,
                    distributor,
                    store,
                    employee,
                    criteria,
                    amountFromNumber,
                    amountToNumber,
                } = filter;

                region && (conditionObj.region = region);

                area && (conditionObj.area = area);

                distributor && (conditionObj.distributor = distributor);

                store && (conditionObj.store = store);

                employee && (conditionObj.employee = employee);

                criteria && (conditionObj.criteria = criteria);

                if (amountFromNumber && amountToNumber) {
                    conditionObj.amount = {
                        $gte: amountFromNumber,
                        $lte: amountToNumber,
                    };
                }


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'store', 'employee', 'criteria'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoLimit_kpi_config = await LIMIT_KPI_CONFIG_COLL
                    .findOne({
                        _id: limit_kpi_configID,
                        ...conditionObj
                    })
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoLimit_kpi_config = await LIMIT_KPI_CONFIG_COLL.populate(infoLimit_kpi_config, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoLimit_kpi_config = await LIMIT_KPI_CONFIG_COLL.populate(infoLimit_kpi_config, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoLimit_kpi_config) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin limit_kpi_config',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoLimit_kpi_config,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách limit_kpi_config
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListLimit_kpi_configs({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['region', 'area', 'distributor', 'store', 'employee', 'criteria', 'amountFromNumber', 'amountToNumber', 'month', 'year'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    region,
                    area,
                    distributor,
                    store,
                    employee,
                    criteria,
                    month,
                    year,
                    amountFromNumber,
                    amountToNumber,
                } = filter;

                region && (conditionObj.region = region);
                area && (conditionObj.area = area);
                distributor && (conditionObj.distributor = distributor);
                store && (conditionObj.store = store);
                employee && (conditionObj.employee = employee);
                criteria && (conditionObj.criteria = criteria);
                month && (conditionObj.month = month);
                year && (conditionObj.year = year);

                if (amountFromNumber && amountToNumber) {
                    conditionObj.amount = {
                        $gte: amountFromNumber,
                        $lte: amountToNumber,
                    };
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'store', 'employee', 'criteria'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listLimit_kpi_configs = await LIMIT_KPI_CONFIG_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listLimit_kpi_configs = await LIMIT_KPI_CONFIG_COLL.populate(listLimit_kpi_configs, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listLimit_kpi_configs = await LIMIT_KPI_CONFIG_COLL.populate(listLimit_kpi_configs, `${ref}.${field}`);
                        }
                    }
                }

                if (!listLimit_kpi_configs) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách limit_kpi_config',
                        status: 400
                    });
                }

                let totalRecord = await LIMIT_KPI_CONFIG_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listLimit_kpi_configs,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    getListKPISaleOut({ employeeID, date }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    employee: ObjectID(employeeID)
                };

                if(date) {
                    const [month, year] = date.split('/');
                    const datetime = moment().set({ 'months': month - 1, 'years': year });

                    conditionObj.createAt = {
                        $gte: ISOdate(datetime.startOf('months').format()),
                        $lte: ISOdate(datetime.endOf('months').format())
                    }
                }

                // Danh sách sản phẩm trong chỉ tiêu
                // const listProductCriteria = await PRODUCT_CRITERIA_COLL
                //     .find({ criteria: TYPE_CRITERIA.SALEOUT, status: 1 })
                //     .select('_id')
                //     .lean();

                // if(listProductCriteria && listProductCriteria.length) {
                //     const listProductCriteriaID = listProductCriteria.map(productCriteria => ObjectID(productCriteria._id));
                //     conditionObj.product = { $in: listProductCriteriaID };
                // }
                
                const listSaleOut = await SALEOUT_COLL.aggregate([
                    {
                        $match: conditionObj
                    },
                    {
                        $group: {
                            _id: { store: "$store" },
                            total: { $sum: "$price" }
                        }
                    }
                ]);

                return resolve({ error: false, data: listSaleOut, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    getListKPIExchangeGift({ employeeID, date }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    employee: ObjectID(employeeID)
                };

                if(date) {
                    const [month, year] = date.split('/');
                    const datetime = moment().set({ 'months': month - 1, 'years': year });

                    conditionObj.createAt = {
                        $gte: ISOdate(datetime.startOf('months').format()),
                        $lte: ISOdate(datetime.endOf('months').format())
                    }
                }

                // Danh sách sản phẩm trong chỉ tiêu
                // const listProductCriteria = await PRODUCT_CRITERIA_COLL
                //     .find({ criteria: TYPE_CRITERIA.EXCHANGE_GIFT, status: 1 })
                //     .select('_id')
                //     .lean();

                // if(listProductCriteria && listProductCriteria.length) {
                //     const listProductCriteriaID = listProductCriteria.map(productCriteria => ObjectID(productCriteria._id));
                //     conditionObj["reward.gift.item"] = { $in: listProductCriteriaID };
                // }

                const listExchangeGift = await HISTORY_EXCHANGE_GIFT_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'rewards',
                            localField: 'reward',
                            foreignField: '_id',
                            as: 'reward'
                        }
                    },
                    {
                        $unwind: {
                            path: '$reward',
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $group: {
                            _id: { store: "$store" },
                            total: { $sum: "$amount" }
                        }
                    }
                ]);

                return resolve({ error: false, data: listExchangeGift, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    getListKPIExchangeSampling({ employeeID, date }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    employee: ObjectID(employeeID)
                };

                if(date) {
                    const [month, year] = date.split('/');
                    const datetime = moment().set({ 'months': month - 1, 'years': year });

                    conditionObj.createAt = {
                        $gte: ISOdate(datetime.startOf('months').format()),
                        $lte: ISOdate(datetime.endOf('months').format())
                    }
                }

                // Danh sách sản phẩm trong chỉ tiêu
                // const listProductCriteria = await PRODUCT_CRITERIA_COLL
                //     .find({ criteria: TYPE_CRITERIA.EXCHANGE_SAMPLING, status: 1 })
                //     .select('_id')
                //     .lean();

                // if(listProductCriteria && listProductCriteria.length) {
                //     const listProductCriteriaID = listProductCriteria.map(productCriteria => ObjectID(productCriteria._id));
                //     conditionObj.product = { $in: listProductCriteriaID };
                // }

                const listExchangeSampling = await HISTORY_EXCHANGE_SAMPLING_COLL.aggregate([
                    {
                        $match: conditionObj
                    },
                    {
                        $group: {
                            _id: { store: "$store" },
                            total: { $sum: "$amount" }
                        }
                    }
                ]);

                return resolve({ error: false, data: listExchangeSampling, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    getListKPIInitCustomer({ employeeID, date }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    employee: ObjectID(employeeID)
                };

                if(date) {
                    const [month, year] = date.split('/');
                    const datetime = moment().set({ 'months': month - 1, 'years': year });

                    conditionObj.createAt = {
                        $gte: ISOdate(datetime.startOf('months').format()),
                        $lte: ISOdate(datetime.endOf('months').format())
                    }
                }

                const listInitCustomer = await HISTORY_INIT_CUSTOMER_COLL.aggregate([
                    {
                        $match: conditionObj
                    },
                    {
                        $group: {
                            _id: { store: "$store" },
                            total: { $sum: 1 }
                        }
                    }
                ]);

                return resolve({ error: false, data: listInitCustomer, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    /**
    * Sau khi Anh Tuấn đã bổ sung phần KPI 08/02/2022. Phần KPI đã gôm lại còn
    *  - KPI Salein
    *  - KPI Trưng Bày
    *  - KPI SBD (Mặt hàng trọng tâm)
    *  - KPI Chương Trình 1 (KH đăng ký app, nhập sdt nhân viên -> người dùng cũ (tồn tại trên AMS))
    *  - KPI Chương Trình 2 (KH đăng ký app, nhập sdt nhân viên -> người dùng mới (chưa tồn tại trên hệ thống))
    */
    //====chỉ tiêu salein
    getListKPISaleinRequire({ employeeID, date }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    employee: ObjectID(employeeID)
                };

                if(date) {
                    const [month, year] = date.split('/');
                    // const datetime = moment().set({ 'months': month - 1, 'years': year });

                    // conditionObj.month = {
                    //     $gte: ISOdate(datetime.startOf('months').format()),
                    //     $lte: ISOdate(datetime.endOf('months').format())
                    // }
                    conditionObj.month = +month;
                    conditionObj.year  = +year;
                }

                // Danh sách sản phẩm trong chỉ tiêu
                // const listProductCriteria = await PRODUCT_CRITERIA_COLL
                //     .find({ criteria: TYPE_CRITERIA.SALEOUT, status: 1 })
                //     .select('_id')
                //     .lean();

                // if(listProductCriteria && listProductCriteria.length) {
                //     const listProductCriteriaID = listProductCriteria.map(productCriteria => ObjectID(productCriteria._id));
                //     conditionObj.product = { $in: listProductCriteriaID };
                // }
                const listSalein = await KPI_SALEIN_REQUIRE_COLL.aggregate([
                    {
                        $match: conditionObj
                    }
                ]);

                return resolve({ error: false, data: listSalein, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    //====thực đạt SALEIN
    getListKPISaleinActually({ employeeID, date, typeMHTT }) {
        return new Promise(async resolve => {
            try {
                /**
                 * IS_FREE_ITEM
                 *      1: LÀ HÀNG TẶNG
                 *      0: LÀ HÀNG BÁN
                 */
                const PRODUCT_FOR_BUY = 0;
                let conditionObj = {
                    employee: ObjectID(employeeID),

                    /**
                     * 2 điều kiện IS_FREE_ITEM, store
                     *  -> đảm bảo rằng record trong kpi_salein_actually là record tính KPI (tồn tại store và is_free_item = 0)
                     */
                    IS_FREE_ITEM: PRODUCT_FOR_BUY,
                    store: {
                        $exists: true
                    }
                };

                /**
                 * BỔ SUNG THÊM typeMHTT = true
                 * -> đây là lấy MHTT(Salein)
                 */
                if (typeMHTT) {
                    const [month, year] = date.split('/');

                    let listProductMHTTOfCurrentMonth = await KPI_MHTT_CONFIG_PRODUCTS_COLL.find({ 
                        month: month,
                        year: year
                    });
                    if (!listProductMHTTOfCurrentMonth.length) return;
                    conditionObj = {
                        ...conditionObj,
                        ['product']: {
                            $in: listProductMHTTOfCurrentMonth.map(item => ObjectID(item.product))
                        }
                    }
                }

                if(date) {
                    const [month, year] = date.split('/');
                    const datetime = moment().set({ 'months': month - 1, 'years': year });

                    conditionObj.ORDER_DATE = {
                        $gte: ISOdate(datetime.startOf('months').format()),
                        $lte: ISOdate(datetime.endOf('months').format())
                    }
                }

                console.log({
                    conditionObj
                })
                
                const listSalein = await KPI_SALEIN_ACTUALLY_COLL.aggregate([
                    {
                        $match: conditionObj
                    },
                    {
                        $lookup: {
                            from: 'products',
                            localField: 'product',
                            foreignField: '_id',
                            as: 'product'
                        }
                    }, {
                        $unwind: "$product"
                    },
                    {
                        $project: {
                            employee: 1,
                            total: {
                                // 8% * sản phẩm = giá_sale_in (của mỗi sản phẩm)
                                $sum: [
                                    // {
                                    //     $multiply: [
                                    //         { $toInt: "$product.price" }, { $toInt: "$QUANTITY" }, 0.08  
                                    //     ]
                                    // },
                                    {
                                        $multiply: [
                                            { $toInt: "$product.priceOfQStore" }, { $toInt: "$QUANTITY" } //update field: 'priceOfQStore' để tính KPI
                                        ]
                                    }
                                ]
                            }
                        }
                    },
                    {
                        $group: {
                            _id: { employee: "$employee" },
                            total: { $sum: "$total" }
                        }
                    }
                ]);

                return resolve({ error: false, data: listSalein, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    //====chỉ tiêu TRƯNG BÀY
    getListKPIPresentRequire({ employeeID, date }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    employee: ObjectID(employeeID)
                };

                if(date) {
                    const [month, year] = date.split('/');
                    // const datetime = moment().set({ 'months': month - 1, 'years': year });

                    // conditionObj.month = {
                    //     $gte: ISOdate(datetime.startOf('months').format()),
                    //     $lte: ISOdate(datetime.endOf('months').format())
                    // }
                    conditionObj.month = +month;
                    conditionObj.year  = +year;
                }

                // Danh sách sản phẩm trong chỉ tiêu
                // const listProductCriteria = await PRODUCT_CRITERIA_COLL
                //     .find({ criteria: TYPE_CRITERIA.SALEOUT, status: 1 })
                //     .select('_id')
                //     .lean();

                // if(listProductCriteria && listProductCriteria.length) {
                //     const listProductCriteriaID = listProductCriteria.map(productCriteria => ObjectID(productCriteria._id));
                //     conditionObj.product = { $in: listProductCriteriaID };
                // }
                
                const listPresent = await KPI_PRESENT_REQUIRE_COLL.aggregate([
                    {
                        $match: conditionObj
                    }
                ]);

                return resolve({ error: false, data: listPresent, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    //====thực đạt TRƯNG BÀY
    getListKPIPresentActully({ employeeID, date }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    employee: ObjectID(employeeID)
                };

                // if(date) {
                //     const [month, year] = date.split('/');
                //     const datetime = moment().set({ 'months': month - 1, 'years': year });

                //     conditionObj.createAt = {
                //         $gte: ISOdate(datetime.startOf('months').format()),
                //         $lte: ISOdate(datetime.endOf('months').format())
                //     }
                // }
                if(date) {
                    const [month, year] = date.split('/');
                   
                    conditionObj.month = +month;
                    conditionObj.year  = +year;
                }

                const listPresent = await KPI_PRESENT_ACTUALLY_COLL.aggregate([
                    {
                        $match: conditionObj
                    },
                    {
                        $group: {
                            _id: { employee: "$employee" },
                            total: { $sum: "$amount" }
                        }
                    }
                ]);

                return resolve({ error: false, data: listPresent, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    //====chỉ tiêu SBD
    getListKPISBDRequire({ employeeID, date }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    // employee: ObjectID(employeeID)
                };

                if(date) {
                    const [month, year] = date.split('/');
                    // const datetime = moment().set({ 'months': month - 1, 'years': year });

                    // conditionObj.month = {
                    //     $gte: ISOdate(datetime.startOf('months').format()),
                    //     $lte: ISOdate(datetime.endOf('months').format())
                    // }
                    conditionObj.month = +month;
                    conditionObj.year  = +year;
                }

                // Danh sách sản phẩm trong chỉ tiêu
                // const listProductCriteria = await PRODUCT_CRITERIA_COLL
                //     .find({ criteria: TYPE_CRITERIA.SALEOUT, status: 1 })
                //     .select('_id')
                //     .lean();

                // if(listProductCriteria && listProductCriteria.length) {
                //     const listProductCriteriaID = listProductCriteria.map(productCriteria => ObjectID(productCriteria._id));
                //     conditionObj.product = { $in: listProductCriteriaID };
                // }
                
                const listSBD = await KPI_SBD_REQUIRE_COLL.aggregate([
                    {
                        $match: conditionObj
                    }, {
                        $lookup: {
                            from: 'products',
                            localField: 'product',
                            foreignField: '_id',
                            as: 'product'
                        }
                    }, {
                        $unwind: '$product'
                    }
                ]);

                return resolve({ error: false, data: listSBD, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    getListKPISBDActually({ employeeID, date }) {
        return new Promise(async resolve => {
            try {
                // theo dõi ảnh tại 'www/packages/config/models/note/ảnh minh hoạt SBD.png'
                /**
                 * b1: lấy danh sách SKU của tháng hiện tại theo (date)
                 *  => output: danh sách SKU (theo tháng hiện tại)
                 */
                let conditionObj = { //vì SKU áp dụng cho tất cả employee, nên ko cần lọc theo Employee
                    // employee: ObjectID(employeeID)
                };
                if (employeeID && checkObjectIDs([employeeID])) {
                    conditionObj = {
                        ...conditionObj, 
                        employee: ObjectID(employeeID)
                    }
                }
               
                if(date) {
                    const [month, year] = date.split('/');

                    conditionObj.month = +month;
                    conditionObj.year  = +year;
                } else {
                    return resolve({ error: true, message: 'date_require' })
                }

                // danh sách SKU lây theo 'date' được truyền lên
                let listSBDSKUWithMonth = await KPI_SBD_REQUIRE_COLL.aggregate([
                    {
                        $match: conditionObj
                    }
                ]);
                /**
                 * listSBDSKUWithMonth có format:
                 *  [
                 *      {
                 *          product: '...',
                 *          month_apply: 05,
                 *          month: 06,
                 *          year: 2021
                 *      },
                 *  ]
                 */

                /**
                 * b2: từ danh sách SKU của (month, year) => lấy được
                 *  - SKU và month_apply
                 *  => output: danh sách SKU (theo tháng thứ mấy được áp dụng)
                 */
                let listSKUWithConditionMonthApply = listSBDSKUWithMonth.map(item => {
                    return {
                        product : item.product,
                        month   : item.month_apply  // vì cần lấy danh sách các SKU theo 'tháng được áp dụng' nên cần chuyển để truy vấn
                    }
                });
                /**
                 * b3: từ 2 danh sách SKU 
                 *  => truy vấn (salein_actually) lấy danh sách thực đạt SALEIN từ 2 nhóm SKU
                 */
                // 1. điều kiện theo month
                let conditionObjStep3WithMonth      = {};
                let [monthStep3WithMonth, yearStep3WithMonth] = date.split('/');
                let datetimeStep3WithMonth = moment().set({ 'months': monthStep3WithMonth - 1, 'years': yearStep3WithMonth });

                conditionObjStep3WithMonth.createAt = {
                    $gte: ISOdate(datetimeStep3WithMonth.startOf('months').format()),
                    $lte: ISOdate(datetimeStep3WithMonth.endOf('months').format())
                }

                // 2. điều kiện theo month_apply
                let conditionObjStep3WithMonthApply = {};
                // với điều kiện thời gian thì tât cả SKU sẽ là 1 thời gian (vì cột tháng chỉ có 1 cột), nên chỉ cần lấy giá trị đầu tiên của item đầu tiên
                if (listSKUWithConditionMonthApply && Array.isArray(listSKUWithConditionMonthApply) && listSKUWithConditionMonthApply.length) {
                    let [firstItemInList] = listSKUWithConditionMonthApply;

                    let monthForMonthApply = firstItemInList.month;
                    // year của month_apply luôn lấy theo 'month'
                    let yearForMonthApply  = yearStep3WithMonth;

                    let datetimeStep3WithMonthApply = moment().set({ 'months': monthForMonthApply - 1, 'years': yearForMonthApply });
                    conditionObjStep3WithMonthApply.createAt = {
                        $gte: ISOdate(datetimeStep3WithMonthApply.startOf('months').format()),
                        $lte: ISOdate(datetimeStep3WithMonthApply.endOf('months').format())
                    }
                }
                console.log("🚀 ~ file: limit_kpi_config.js ~ line 3584 ~ getListKPISBDActually ~ conditionObjStep3WithMonth", conditionObjStep3WithMonth)
                console.log("🚀 ~ file: limit_kpi_config.js ~ line 3590 ~ getListKPISBDActually ~ conditionObjStep3WithMonthApply", conditionObjStep3WithMonthApply)

                /**
                 * IS_FREE_ITEM
                 *      1: LÀ HÀNG TẶNG
                 *      0: LÀ HÀNG BÁN
                 */
                const PRODUCT_FOR_BUY = 0;
                let listSaleInActually = await KPI_SALEIN_ACTUALLY_COLL.aggregate([
                    {
                        $match: {
                            // employee: ObjectID(employeeID),
                            
                            /**
                             * 2 điều kiện IS_FREE_ITEM, store
                             *  -> đảm bảo rằng record trong kpi_salein_actually là record tính KPI (tồn tại store và is_free_item = 0)
                             */
                            IS_FREE_ITEM: PRODUCT_FOR_BUY,
                            store: {
                                $exists: true
                            },

                            $or: [
                                {
                                    // truy vấn danh sách theo 'month'
                                    /**
                                     * ý nghĩa truy vấn:
                                     *  - lấy danh sách sale_in thực đạt theo product và có thời gian theo date
                                     */
                                     ...conditionObjStep3WithMonth,
                                    product: {
                                        $in: [
                                            ...listSBDSKUWithMonth.map(item => ObjectID(item.product))
                                        ]
                                    },
                                },
                                {
                                    // truy vấn danh sách theo 'month_apply'
                                    ...conditionObjStep3WithMonthApply,
                                    product: {
                                        $in: [
                                            ...listSKUWithConditionMonthApply.map(item => ObjectID(item.product))
                                        ]
                                    },
                                }
                            ]
                        }
                    }, {
                        $group: {
                            _id: { product: "$product" },
                            employee: {
                                $first: "$employee"
                            },
                            total: { $sum: "$QUANTITY" }
                        }
                    }, {
                        $lookup: {
                            from: 'products',
                            localField: '_id.product',
                            foreignField: '_id',
                            as: 'product'
                        }
                    }, {
                        $unwind: '$product'
                    }
                ])
                return resolve({ error: false, data: listSaleInActually, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    /**
     * về tính chất thì tương tự func getListKPISBDActually,
     * nhưng cần tách riêng để xuất KPI
     */
    getListKPISBDActuallyForReportKPI({ date }) {
        return new Promise(async resolve => {
            try {
                // theo dõi ảnh tại 'www/packages/config/models/note/ảnh minh hoạt SBD.png'
                /**
                 * b1: lấy danh sách SKU của tháng hiện tại theo (date)
                 *  => output: danh sách SKU (theo tháng hiện tại)
                 */
                let conditionObj = { //vì SKU áp dụng cho tất cả employee, nên ko cần lọc theo Employee
                };
               
                if(date) {
                    const [month, year] = date.split('/');

                    conditionObj.month = +month;
                    conditionObj.year  = +year;
                } else {
                    return resolve({ error: true, message: 'date_require' })
                }

                // danh sách SKU lây theo 'date' được truyền lên
                let listSBDSKUWithMonth = await KPI_SBD_REQUIRE_COLL.aggregate([
                    {
                        $match: conditionObj
                    }
                ]);
                /**
                 * listSBDSKUWithMonth có format:
                 *  [
                 *      {
                 *          product: '...',
                 *          month_apply: 05,
                 *          month: 06,
                 *          year: 2021
                 *      },
                 *  ]
                 */

                /**
                 * b2: từ danh sách SKU của (month, year) => lấy được
                 *  - SKU và month_apply
                 *  => output: danh sách SKU (theo tháng thứ mấy được áp dụng)
                 */
                let listSKUWithConditionMonthApply = listSBDSKUWithMonth.map(item => {
                    return {
                        product : item.product,
                        month   : item.month_apply  // vì cần lấy danh sách các SKU theo 'tháng được áp dụng' nên cần chuyển để truy vấn
                    }
                });
                /**
                 * b3: từ 2 danh sách SKU 
                 *  => truy vấn (salein_actually) lấy danh sách thực đạt SALEIN từ 2 nhóm SKU
                 */
                // 1. điều kiện theo month
                let conditionObjStep3WithMonth      = {};
                let [monthStep3WithMonth, yearStep3WithMonth] = date.split('/');
                let datetimeStep3WithMonth = moment().set({ 'months': monthStep3WithMonth - 1, 'years': yearStep3WithMonth });

                conditionObjStep3WithMonth.createAt = {
                    $gte: ISOdate(datetimeStep3WithMonth.startOf('months').format()),
                    $lte: ISOdate(datetimeStep3WithMonth.endOf('months').format())
                }

                // 2. điều kiện theo month_apply
                let conditionObjStep3WithMonthApply = {};
                // với điều kiện thời gian thì tât cả SKU sẽ là 1 thời gian (vì cột tháng chỉ có 1 cột), nên chỉ cần lấy giá trị đầu tiên của item đầu tiên
                if (listSKUWithConditionMonthApply && Array.isArray(listSKUWithConditionMonthApply) && listSKUWithConditionMonthApply.length) {
                    let [firstItemInList] = listSKUWithConditionMonthApply;

                    let monthForMonthApply = firstItemInList.month;
                    // year của month_apply luôn lấy theo 'month'
                    let yearForMonthApply  = yearStep3WithMonth;

                    let datetimeStep3WithMonthApply = moment().set({ 'months': monthForMonthApply - 1, 'years': yearForMonthApply });
                    conditionObjStep3WithMonthApply.createAt = {
                        $gte: ISOdate(datetimeStep3WithMonthApply.startOf('months').format()),
                        $lte: ISOdate(datetimeStep3WithMonthApply.endOf('months').format())
                    }
                }

                /**
                 * IS_FREE_ITEM
                 *      1: LÀ HÀNG TẶNG
                 *      0: LÀ HÀNG BÁN
                 */
                const PRODUCT_FOR_BUY = 0;
                let listSaleInActually = await KPI_SALEIN_ACTUALLY_COLL.aggregate([
                    {
                        $match: {
                            /**
                             * 2 điều kiện IS_FREE_ITEM, store
                             *  -> đảm bảo rằng record trong kpi_salein_actually là record tính KPI (tồn tại store và is_free_item = 0)
                             */
                            IS_FREE_ITEM: PRODUCT_FOR_BUY,
                            store: {
                                $exists: true
                            },

                            $or: [
                                {
                                    // truy vấn danh sách theo 'month'
                                    /**
                                     * ý nghĩa truy vấn:
                                     *  - lấy danh sách sale_in thực đạt theo product và có thời gian theo date
                                     */
                                     ...conditionObjStep3WithMonth,
                                    product: {
                                        $in: [
                                            ...listSBDSKUWithMonth.map(item => ObjectID(item.product))
                                        ]
                                    },
                                },
                                {
                                    // truy vấn danh sách theo 'month_apply'
                                    ...conditionObjStep3WithMonthApply,
                                    product: {
                                        $in: [
                                            ...listSKUWithConditionMonthApply.map(item => ObjectID(item.product))
                                        ]
                                    },
                                }
                            ]
                        }
                    }, {
                        $group: { 
                            _id: "$employee",
                            products: {
                                $addToSet: "$product"
                            }
                        },
                    }    
                ])
                return resolve({ error: false, data: listSaleInActually, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    //====DANH SÁCH SẢN PHẨM được chọn cho MHTT cuả THÁNG
    getListKPIMHTTRequire({ employeeID, date }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    // employee: ObjectID(employeeID)
                };

                if(date) {
                    const [month, year] = date.split('/');
                    conditionObj.month = +month;
                    conditionObj.year  = +year;
                }
                const listMHTT = await KPI_MHTT_CONFIG_PRODUCTS_COLL.aggregate([
                    {
                        $match: conditionObj
                    }, {
                        $lookup: {
                            from: 'products',
                            localField: 'product',
                            foreignField: '_id',
                            as: 'product'
                        }
                    }, {
                        $unwind: '$product'
                    }
                ]);

                return resolve({ error: false, data: listMHTT, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    /**
     * báo cáo KPI
     */
    reportKPI({ region, monthAndYear }) {
        return new Promise(async resolve => {
            try {
                /**
                 * Danh sách NHÂN VIÊN với các thông tin:
                 *  1/ Vùng
                 *  2/ Miền
                 *  3/ Mã NPP
                 *  4/ Tên NPP
                 *  5/ Mã NV
                 *  6/ Tên NV
                 */
                let conditionObj = {};
                if(region){
                    conditionObj.region = ObjectID(region);
                }
                
                let listEmployees = await EMPLOYEE_COLL.aggregate([
                    {
                        $match: conditionObj
                    },
                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                
                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },    
                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },  
                     {
                        $lookup: {
                            from: 'type_employees',
                            localField: 'type_employee',
                            foreignField: '_id',
                            as: 'type_employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$type_employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },  
                      {
                        $lookup: {
                            from: 'detail_type_employees',
                            localField: 'detail_type_employee',
                            foreignField: '_id',
                            as: 'detail_type_employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$detail_type_employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },  
                    {
                        $project: {
                            fullname:1,
                            code: 1,
                            state: 1,
                            status: 1,
                            "type_employee.name": 1,
                            "detail_type_employee.name": 1,
                            "region.name": 1,
                            "region.code": 1,
                            "area.name": 1,
                            "area.code": 1,
                            "distributor.name": 1,
                            "distributor.code": 1
                        }
                    }
                ]);

                /**
                 * Danh sách KPI_SALEIN_REQUIRE (SALE_IN yêu cầu)
                 */
                let dayFormat = monthAndYear + `-1`;
                const dateCurrent= new Date(dayFormat);
                let monthCurrent = dateCurrent.getMonth() + 1;
                let yearCurrent  = dateCurrent.getFullYear();
                const datetime   = moment().set({ 'months': monthCurrent - 1, 'years': yearCurrent });
                const listSaleinRequire = await KPI_SALEIN_REQUIRE_COLL.aggregate([
                    {
                        $match: {
                            month: monthCurrent,
                            year: yearCurrent
                        }
                    }
                ]);

                /**
                 * Danh Sách KPI_SALEIN_ACTUALLY(SALE_IN thực đạt)
                 */
                /**
                 * IS_FREE_ITEM
                 *      1: LÀ HÀNG TẶNG
                 *      0: LÀ HÀNG BÁN
                 */
                const PRODUCT_FOR_BUY = 0;
                let conditionObjSaleinActually = {
                    /**
                     * 2 điều kiện IS_FREE_ITEM, store
                     *  -> đảm bảo rằng record trong kpi_salein_actually là record tính KPI (tồn tại store và is_free_item = 0)
                     */
                    IS_FREE_ITEM: PRODUCT_FOR_BUY,
                    store: {
                        $exists: true
                    },
                    ORDER_DATE: {
                        $gte: ISOdate(datetime.startOf('months').format()),
                        $lte: ISOdate(datetime.endOf('months').format())
                    }
                 };
                
                const listSaleinActually = await KPI_SALEIN_ACTUALLY_COLL.aggregate([
                    {
                        $match: {
                            ...conditionObjSaleinActually,
                        }
                    },
                    {
                        $lookup: {
                            from: 'products',
                            localField: 'product',
                            foreignField: '_id',
                            as: 'product'
                        }
                    }, {
                        $unwind: "$product"
                    },
                    {
                        $project: {
                            employee: 1,
                            total: {
                                // 8% * sản phẩm = giá_sale_in (của mỗi sản phẩm)
                                $sum: [
                                    // {
                                    //     $multiply: [
                                    //         { $toInt: "$product.price" }, { $toInt: "$QUANTITY" } , 0.08  
                                    //     ]
                                    // },
                                    {
                                        $multiply: [
                                            { $toInt: "$product.priceOfQStore" }, { $toInt: "$QUANTITY" }   //update field: 'priceOfQStore' để tính KPI
                                        ]
                                    }
                                ]
                           }
                       }
                   },
                   {
                        $group: {
                            _id: { employee: "$employee" },
                            total: { $sum: "$total" }
                        }
                    }
                ]);
                let item26HDUONGNA = listSaleinActually.find(_item => `${_item._id.employee}` == '618acf23b581ee40fcefeddc');
                if (item26HDUONGNA)
                    console.log({
                        item26HDUONGNA,
                        conditionObjSaleinActually
                    })
                /**
                 * Danh Sách KPI_PRESENT_REQUIRE (CHỈ TIÊU TRƯNG_BÀY)
                 */
                const listPresentRequire = await KPI_PRESENT_REQUIRE_COLL.aggregate([
                    {
                        $match: {
                            month: monthCurrent,
                            year: yearCurrent
                        }
                    }
                ]);

                /**
                 * Danh Sách KPI_PRESENT_ACTUALLY (THỰC ĐẠT TRƯNG_BÀY)
                 */
                const listPresentActually = await KPI_PRESENT_ACTUALLY_COLL.aggregate([
                    {
                        $match: {
                            month: monthCurrent,
                            year: yearCurrent
                        }
                    }
                ]);

                /**
                 * Danh Sách KPI_SBD_REQUIRE (CHỈ TIÊU SBD)
                 */
                const listSBDRequire = await KPI_SBD_REQUIRE_COLL.aggregate([
                    {
                        $match: {
                            month: monthCurrent,
                            year: yearCurrent
                        }
                    }, {
                        $lookup: {
                            from: 'products',
                            localField: 'product',
                            foreignField: '_id',
                            as: 'product'
                        }
                    }, {
                        $unwind: '$product'
                    }
                ]);
                /**
                 * Danh Sách KPI_SBD_ACTUALLY (THỰC ĐẠT SBD)
                */
                const listSBDActually = await this.getListKPISBDActuallyForReportKPI({ date: `${monthCurrent}/${yearCurrent}` })

                /**
                 * Danh Sách KPI_SBD_REQUIRE (CHỈ TIÊU SBD)
                    */
                const listMHTTRequire = await KPI_MHTT_REQUIRE_COLL.aggregate([
                    {
                        $match: {
                            month: monthCurrent,
                            year: yearCurrent
                        }
                    }
                ]);
                /*
                * Danh Sách THỰC ĐẠT MHTT (salein)
                */
                let listProductMHTTOfCurrentMonth = await KPI_MHTT_CONFIG_PRODUCTS_COLL.find({ 
                    month: monthCurrent,
                    year: yearCurrent
                });
                let conditionObjMHTT = {
                    ...conditionObjSaleinActually,
                    ['product']: {
                        $in: listProductMHTTOfCurrentMonth.length ? listProductMHTTOfCurrentMonth.map(item => ObjectID(item.product)) : []
                    }
                }
                const listSaleinActually__MHTT = await KPI_SALEIN_ACTUALLY_COLL.aggregate([
                    {
                        $match: {
                            ...conditionObjMHTT,
                        }
                    },
                    {
                        $lookup: {
                            from: 'products',
                            localField: 'product',
                            foreignField: '_id',
                            as: 'product'
                        }
                    }, {
                        $unwind: "$product"
                    },
                    {
                        $project: {
                            employee: 1,
                            total: {
                                // 8% * sản phẩm = giá_sale_in (của mỗi sản phẩm)
                                $sum: [
                                    // {
                                    //     $multiply: [
                                    //         { $toInt: "$product.price" }, { $toInt: "$QUANTITY" } , 0.08  
                                    //     ]
                                    // },
                                    {
                                        $multiply: [
                                            { $toInt: "$product.priceOfQStore" }, { $toInt: "$QUANTITY" } //update field: 'priceOfQStore' để tính KPI
                                        ]
                                    }
                                ]
                           }
                       }
                   },
                   {
                        $group: {
                            _id: { employee: "$employee" },
                            total: { $sum: "$total" }
                        }
                    }
                ]);


                const listCriteriasCampaign = await CRITERIA_CAMPAIGN_CONFIG_MODEL.getList();
                return resolve({ 
                    listEmployees,
                    listSaleinRequire,
                    listSaleinActually,
                    listPresentRequire,
                    listPresentActually,
                    listSBDRequire,
                    listSBDActually: listSBDActually.data,
                    listCriteriasCampaign: listCriteriasCampaign.data,
                    listMHTTRequire,
                    listSaleinActually__MHTT,
                }) 
 
                // let listCentralizeOrderSampling = await CENTRALIZE_ORDER_COLL
                //     .find({
                //         kind: KIND_SAMPLING,
                //         mergeCentralizeOrderIDGHN: {
                //             $exists: true
                //         }
                //     })
                //     .populate({
                //         path: "customer",
                //         select: 'fullname email phone'
                //     })
                //     .populate({
                //         path: "type",
                //         select: 'totalPoint rewards createAt',
                //         populate: {
                //             path: 'rewards.reward',
                //             select: 'title '
                //         }
                //     })
                //     ;

                // let fileNameRandom = `${moment(new Date()).format('LT')}_${moment(new Date()).format('DD-MM-YYYY')}.xlsx`
                // let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                
                // XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                //     .then(async workbook => {
                //         workbook.sheet("Sheet1").row(1).cell(1).value("STT");
                //         workbook.sheet("Sheet1").row(1).cell(2).value("Tên KH");
                //         workbook.sheet("Sheet1").row(1).cell(3).value("SĐT");
                //         workbook.sheet("Sheet1").row(1).cell(4).value("Email");
                //         workbook.sheet("Sheet1").row(1).cell(5).value("Địa Chỉ");
                //         workbook.sheet("Sheet1").row(1).cell(6).value('Mã đơn hàng');
                //         workbook.sheet("Sheet1").row(1).cell(7).value('Trạng thái đơn hàng');
                //         workbook.sheet("Sheet1").row(1).cell(8).value('Số điểm đổi');
                //         workbook.sheet("Sheet1").row(1).cell(9).value('Mã quà (mã SP)');
                //         workbook.sheet("Sheet1").row(1).cell(10).value('Tên quà (tên Sản phẩm)');
                //         workbook.sheet("Sheet1").row(1).cell(11).value('Số lượng Quà (SP)');
                //         workbook.sheet("Sheet1").row(1).cell(12).value('Ngày đổi');
                //         workbook.sheet("Sheet1").row(1).cell(13).value('Ngày nhận');
                //         workbook.sheet("Sheet1").row(1).cell(14).value('Phí vận chuyển');
                //         workbook.sheet("Sheet1").row(1).cell(15).value('Mã chương trình');
                //         workbook.sheet("Sheet1").row(1).cell(16).value('Chi nhánh');
                        
                //         if (listCentralizeOrderSampling && listCentralizeOrderSampling.length) {
                //             let index = 0;
                //             for (const order of listCentralizeOrderSampling) {
                //                 if (order.type && order.type.rewards && order.type.rewards.length) {
                //                     for (const r of order.type.rewards) {
                //                         workbook.sheet("Sheet1").row(index + 2).cell(1).value(index + 1);
                //                         workbook.sheet("Sheet1").row(index + 2).cell(2).value(order.customer && order.customer.fullname);
                //                         workbook.sheet("Sheet1").row(index + 2).cell(3).value(order.customer && order.customer.phone);
                //                         workbook.sheet("Sheet1").row(index + 2).cell(4).value(order.customer && order.customer.email);
                //                         let infoAddressOfCustomer = await ADDRESS_MODEL.getInfoByCustomerDefault({ customerID: order.customer._id });
                                                
                //                         if (!infoAddressOfCustomer.error && infoAddressOfCustomer.data && !isEmptyObject(infoAddressOfCustomer.data)) {
                //                             let address = infoAddressOfCustomer.data.address + ', ' + infoAddressOfCustomer.data.wardText + ', ' + infoAddressOfCustomer.data.districtText + ', ' + infoAddressOfCustomer.data.cityText;
                //                             workbook.sheet("Sheet1").row(index + 2).cell(5).value(address ? address : '');
                //                         }

                //                         workbook.sheet("Sheet1").row(index + 2).cell(6).value(order.centralizeOrderID);

                //                         STATUS_ORDER.forEach(status => {
                //                             if (status.value == order.status) {
                //                                 workbook.sheet("Sheet1").row(index + 2).cell(7).value(status.text);
                //                             }
                //                         });

                //                         workbook.sheet("Sheet1").row(index + 2).cell(8).value(order.type.totalPoint ? order.type.totalPoint : 0);
                //                         workbook.sheet("Sheet1").row(index + 2).cell(9).value(r.reward.SKU && r.reward.SKU);
                //                         workbook.sheet("Sheet1").row(index + 2).cell(10).value(r.reward.title && r.reward.title);
                //                         workbook.sheet("Sheet1").row(index + 2).cell(11).value(r.quantities ? r.quantities : 0);
                //                         workbook.sheet("Sheet1").row(index + 2).cell(12).value(moment(order.type.createAt).format('DD-MM-YYYY'));
                //                         workbook.sheet("Sheet1").row(index + 2).cell(13).value("");
                //                         workbook.sheet("Sheet1").row(index + 2).cell(14).value(order.deliveryPrice ? order.deliveryPrice : 0);
                //                         workbook.sheet("Sheet1").row(index + 2).cell(15).value("");
                //                         workbook.sheet("Sheet1").row(index + 2).cell(16).value(order.storeID);
                //                         index ++;
                //                     }
                //                 }
                //             }
                //         }

                       
                //         workbook.toFileAsync(pathWriteFile)
                //         .then(_ => {
                //             // Download file from server
                //             return resolve({
                //                 error: false,
                //                 data: "/files/upload_excel_temp/" + fileNameRandom,
                //                 path: fileNameRandom,
                //             });
                //         });
                //     });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy SKU và tồn kho tương ứng
     */
    getListSKUWithInventory({ employeeID, date }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employeeID]))
                    return resolve({ error: true, message: 'employee_invalid' })

                if(!date) 
                    return resolve({ error: true, message: 'date_is_require' })

                let conditionObj = {};
                const [month, year] = date.split('/');
                const datetime = moment().set({ 'months': month - 1, 'years': year });

                // conditionObj.createAt = {
                //     $gte: ISOdate(datetime.startOf('months').format()),
                //     $lte: ISOdate(datetime.endOf('months').format())
                // }

                /**
                 * (1) LIMIT_GIFT_CONFIG
                 */
                let listLimitGiftConfig = await LIMIT_GIFT_CONFIG_COLL.find({ 
                    employee: employeeID, 
                    month,
                    year
                });
                /**
                 * (2) INVENTORY_BEGIN_MONTH
                 */
                let listInventoryBeginMonth = await INVENTORY_BEGIN_MONTH_COLL.find({
                    employee: employeeID, 
                    month,
                    year
                })

                /**
                 * (3) SALEIN_ACTUALLY
                 */
				const PRODUCT_FOR_BUY = 0;
                let listSaleinActually = await KPI_SALEIN_ACTUALLY_COLL.find({
                    employee: employeeID, 
                    /**
					 * 2 điều kiện IS_FREE_ITEM, store
					 *  -> đảm bảo rằng record trong kpi_salein_actually là record tính KPI (tồn tại store và is_free_item = 0)
					 */
					IS_FREE_ITEM: PRODUCT_FOR_BUY,
					store: {
						$exists: false
					},
                    ORDER_DATE: {
                        $gte: ISOdate(datetime.startOf('months').format()),
                        $lte: ISOdate(datetime.endOf('months').format())
                    }
                })

                /**
                 * (4) HISTORY_EXCHANGE_GIFT
                 */
                 let listHistoryExchangeGift = await HISTORY_EXCHANGE_GIFT_COLL.find({
                    employee: employeeID, 
                    createAt: {
                        $gte: ISOdate(datetime.startOf('months').format()),
                        $lte: ISOdate(datetime.endOf('months').format())
                    }
                })

                /**
                 * (5) HISTORY_EXCHANGE_SAMPLING
                 */
                 let listHistoryExchangeSampling = await HISTORY_EXCHANGE_SAMPLING_COLL.find({
                    employee: employeeID, 
                    createAt: {
                        $gte: ISOdate(datetime.startOf('months').format()),
                        $lte: ISOdate(datetime.endOf('months').format())
                    }
                });

                /**
                 * ============= BƯỚC LÀM CHUẨN DỮ LIỆU ===========
                 * output: [
                 *   { sku: 'objectID', amount: 10 },
                 *    ...
                 * ]
                 */
                let listLimitGiftConfigConverted     = listLimitGiftConfig.map(item => {
                    return {
                        sku: item.sku,
                        amount: item.amount
                    }
                });
                let listInventoryBeginMonthConverted = listInventoryBeginMonth.map(item => {
                    return {
                        sku: item.sku,
                        amount: item.amount
                    }
                });
                let listSaleinActuallyConverted      = listSaleinActually.map(item => {
                    return {
                        sku: item.product,
                        amount: item.QUANTITY
                    }
                });
                // Xuất (sampling)
                let listHistoryExchangeGiftConverted      = listHistoryExchangeGift.map(item => {
                    return {
                        sku: item.sku,
                        amount: item.amount
                    }
                });
                // Xuất (quà tặng)
                let listHistoryExchangeSamplingConverted  = listHistoryExchangeSampling.map(item => {
                    return {
                        sku: item.product,
                        amount: item.amount,
                        isExportFromQShopESampling: item.isExportFromQShopESampling
                    }
                });

                // tổng số lượng có ban đầu
                const listSKUWithOperationSumTotal = Object.values([...listLimitGiftConfigConverted, ...listInventoryBeginMonthConverted, ...listSaleinActuallyConverted].reduce((acc, { sku, amount }) => {
                    acc[sku] = { sku, amount: (acc[sku] ? acc[sku].amount : 0) + amount  };
                    return acc;
                }, {}));
                  
                /**
                 * ===================START===================
                 * DANH SÁCH SKU VÀ SỐ LƯƠNG XUẤT TƯƠNG ỨNG
                 * ============================================
                 */
                // DANH SÁCH XUẤT GIFT (MÀN HÌNH QUẢN LÝ XUẤT_NHẬP_QUÀ)
                const listSKUWithOperationGiftExport = Object.values([...listHistoryExchangeGiftConverted].reduce((acc, { sku, amount }) => {
                    acc[sku] = { sku, amount: (acc[sku] ? acc[sku].amount : 0) + amount  };
                    return acc;
                }, {}));

                // DANH SÁCH XUẤT SAMPLING (MÀN HÌNH QUẢN LÝ HÀNG_DÙNG_THỬ)
                const listSKUWithOperationSamplingExport = Object.values([...listHistoryExchangeSamplingConverted].reduce((acc, { sku, amount }) => {
                    acc[sku] = { sku, amount: (acc[sku] ? acc[sku].amount : 0) + amount  };
                    return acc;
                }, {}));

                /**
                 * listSKUWithOperationSamplingExportQSHOP: SỐ LƯỢNG XUẤT SAMPLING QSHOP
                 * listSKUWithOperationSamplingExportQSTORE: SỐ LƯỢNG XUẤT SAMPLING QSTORE
                 * 
                 * listSKUWithOperationSamplingExport = listSKUWithOperationSamplingExportQSHOP + listSKUWithOperationSamplingExportQSTORE
                 *  -> vì mục đích tách ra để xuất thêm cột bổ sung khi có module ESAMPLING
                 */
                const listSKUWithOperationSamplingExportQSHOP = Object.values([...listHistoryExchangeSamplingConverted.filter(item => item.isExportFromQShopESampling)].reduce((acc, { sku, amount }) => {
                    acc[sku] = { sku, amount: (acc[sku] ? acc[sku].amount : 0) + amount  };
                    return acc;
                }, {}));
                const listSKUWithOperationSamplingExportQSTORE = Object.values([...listHistoryExchangeSamplingConverted.filter(item => !item.isExportFromQShopESampling)].reduce((acc, { sku, amount }) => {
                    acc[sku] = { sku, amount: (acc[sku] ? acc[sku].amount : 0) + amount  };
                    return acc;
                }, {}));

                // DANH SÁCH XUẤT TỔNG: SAMPLING + GIFT
                 const listSKUWithOperationSumFromHistory = Object.values([...listHistoryExchangeGiftConverted, ...listHistoryExchangeSamplingConverted].reduce((acc, { sku, amount }) => {
                    acc[sku] = { sku, amount: (acc[sku] ? acc[sku].amount : 0) + amount  };
                    return acc;
                }, {}));
                /**
                 * ===================END===================
                 * DANH SÁCH SKU VÀ SỐ LƯƠNG XUẤT TƯƠNG ỨNG
                 * ============================================
                 */

                const listSKUWithInventory = listSKUWithOperationSumTotal.map(itemTotal => {
                    // kiểm tra nếu tồn tại sku trong xuất -> thì trừ số lượng tương ứng
                    let isExistItemInHistory = listSKUWithOperationSumFromHistory && listSKUWithOperationSumFromHistory.length 
                                                && listSKUWithOperationSumFromHistory.find(item => item.sku && item.sku.toString() == itemTotal.sku && itemTotal.sku.toString());
                    if (isExistItemInHistory) {
                        console.log({ isExistItemInHistory })
                        return {
                            ...itemTotal,
                            amount: itemTotal.amount - isExistItemInHistory.amount
                        };
                    }
                    return itemTotal;
                })

                const listProductsWithInfo = await PRODUCT_COLL.find({
                    _id: {
                        $in: [...listSKUWithInventory.map(item => ObjectID(item.sku))]
                    }
                }, {
                    name: 1,
                    SKU: 1
                })

                // là phần hiển thị 'Xuất' ~ tức tổng_xuất của nhân viên đó (này là tổng xuất SAMPLING) //TODO đổi tên biến thành totalQuantityExportSampling
                const totalQuantityExport = (listHistoryExchangeSamplingConverted && listHistoryExchangeSamplingConverted.reduce((previousVal, currentVal) => previousVal + currentVal.amount, 0)) || 0;
                const totalQuantityExportSampling = (listHistoryExchangeSamplingConverted && listHistoryExchangeSamplingConverted.reduce((previousVal, currentVal) => previousVal + currentVal.amount, 0)) || 0;
                const totalQuantityExportGift     = (listHistoryExchangeGiftConverted && listHistoryExchangeGiftConverted.reduce((previousVal, currentVal) => previousVal + currentVal.amount, 0)) || 0;

                const totalTargetExportSamplingQuery = await LIMIT_SAMPLING_CONFIG_COLL.aggregate([
                    {
                        $match: { 
                            employee: ObjectID(employeeID), 
                            month: +month,
                            year: +year
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            total: { $sum: "$amount" }
                        }
                    }
                ]);

                let totalTargetExportSampling = totalTargetExportSamplingQuery.length > 0 ? totalTargetExportSamplingQuery[0].total : 0;
                
                return resolve({ error: false, data: {
                    listSKUWithOperationSumTotal, // danh sách SKU với số lượng có (số lượng có này là TỔNG_CÓ ban đầu)
                    listSKUWithOperationSumFromHistory, // danh sách SKU với số lượng xuất (số lượng đã xuất khỏi kho NA) TỔNG: SAMPLING+GIFT
                    listSKUWithOperationGiftExport, // danh sách SKU với số lượng xuất (số lượng đã xuất khỏi kho NA) TỔNG: GIFT
                    listSKUWithOperationSamplingExport,// danh sách SKU với số lượng xuất (số lượng đã xuất khỏi kho NA) TỔNG: SAMPLING 
                    listSKUWithOperationSamplingExportQSHOP,// (phần mở rộng 'listSKUWithOperationSamplingExport' -> lượng sampling xuất của qshop)
                    listSKUWithOperationSamplingExportQSTORE,// (phần mở rộng 'listSKUWithOperationSamplingExport' -> lượng sampling xuất của qstore)
                    listSKUWithInventory, // danh sách SKU với  số lượng (TỔNG_CÓ (-) trừ XUẤT)
                    listProductsWithInfo, // danh sách SKU tương ứng Thông Tin PRODUCT
                    totalQuantityExport, // hiển thị tại mục 'Xuất' //(deprecated)
                    totalTargetExportSampling, // hiển thị tại mục 'Chỉ Tiêu Tháng'
                    totalQuantityExportSampling, // hiển thị ở cột Xuất 'Màn hình Sampling'
                    totalQuantityExportGift, // hiển thị ở cột Xuất 'Màn hình quà tặng'
                }, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }
}

exports.MODEL = new Model;