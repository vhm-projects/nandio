"use strict";

/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const path = require('path');
const fs = require('fs');
const XlsxPopulate = require('xlsx-populate');
const ObjectID = require('mongoose').Types.ObjectId;
const mongodb = require("mongodb");
const {
    MongoClient
} = mongodb;
const formatCurrency = require('number-format.js');

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    renderOptionFilter,
    colName,
} = require('../../../utils/utils');
const {
    isTrue
} = require('../../../tools/module/check');
const {
    randomStringFixLength
} = require('../../../utils/string_utils');

const {} = require('../constants/limit_gift_config');


/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const URL_DATABASE = process.env.URL_DATABASE || 'mongodb://localhost:27017';
const NAME_DATABASE = process.env.NAME_DATABASE || 'nandio_staging2';

/**
 * COLLECTIONS
 */
const LIMIT_GIFT_CONFIG_COLL = require('../databases/limit_gift_config-coll');

const {
    REGION_COLL
} = require('../../region_area');

const {
    AREA_COLL
} = require('../../region_area');

const {
    DISTRIBUTOR_COLL
} = require('../../distributor');

const {
    EMPLOYEE_COLL
} = require('../../employee');

const {
    PRODUCT_COLL
} = require('../../product');
const { truncate } = require('fs/promises');


class Model extends BaseModel {
    constructor() {
        super(LIMIT_GIFT_CONFIG_COLL);
    }

    /**
         * Tạo mới limit_gift_config
		* @param {object} employee
		* @param {object} sku
		* @param {number} amount
		* @param {number} month
		* @param {number} year

         * @param {objectId} userCreate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        employee,
        sku,
        amount,
        month,
        year,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (!amount) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập số lượng'
                    });
                }

                if (!month) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tháng'
                    });
                }

                if (!year) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập năm'
                    });
                }





                if (!checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                if (!checkObjectIDs([sku])) {
                    return resolve({
                        error: true,
                        message: 'sku không hợp lệ'
                    });
                }



                let dataInsert = {
                    employee,
                    sku,
                    amount,
                    month,
                    year,
                    userCreate
                };


                if (employee) {
                    const employeeInfo = await EMPLOYEE_COLL.findById(employee);
                    dataInsert.region = employeeInfo.region;
                    dataInsert.area = employeeInfo.area;
                    dataInsert.distributor = employeeInfo.distributor;
                }

                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo cấu hình giới hạn quà tặng thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật limit_gift_config 
         * @param {objectId} limit_gift_configID
		* @param {object} employee
		* @param {object} sku
		* @param {number} amount
		* @param {number} month
		* @param {number} year

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        limit_gift_configID,
        employee,
        sku,
        amount,
        month,
        year,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([limit_gift_configID])) {
                    return resolve({
                        error: true,
                        message: 'limit_gift_configID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (!amount) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập số lượng cho limit_gift_config'
                    });
                }

                if (!month) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tháng cho limit_gift_config'
                    });
                }

                if (!year) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập năm cho limit_gift_config'
                    });
                }





                if (!checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                if (!checkObjectIDs([sku])) {
                    return resolve({
                        error: true,
                        message: 'sku không hợp lệ'
                    });
                }

                const checkExists = await LIMIT_GIFT_CONFIG_COLL.findById(limit_gift_configID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'cấu hình giới hạn quà tặng không tồn tại'
                    });
                }


                let dataUpdate = {
                    userUpdate
                };
                employee && (dataUpdate.employee = employee);
                sku && (dataUpdate.sku = sku);
                amount && (dataUpdate.amount = amount);
                month && (dataUpdate.month = month);
                year && (dataUpdate.year = year);

                if (employee) {
                    const employeeInfo = await EMPLOYEE_COLL.findById(employee);
                    dataUpdate.region = employeeInfo.region;
                    dataUpdate.area = employeeInfo.area;
                    dataUpdate.distributor = employeeInfo.distributor;
                }

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: limit_gift_configID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật limit_gift_config (không bắt buộc)
         * @param {objectId} limit_gift_configID
		* @param {object} employee
		* @param {object} sku
		* @param {number} amount
		* @param {number} month
		* @param {number} year

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        limit_gift_configID,
        employee,
        sku,
        amount,
        month,
        year,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([limit_gift_configID])) {
                    return resolve({
                        error: true,
                        message: 'limit_gift_configID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                if (sku && !checkObjectIDs([sku])) {
                    return resolve({
                        error: true,
                        message: 'sku không hợp lệ'
                    });
                }

                const checkExists = await LIMIT_GIFT_CONFIG_COLL.findById(limit_gift_configID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'cấu hình giới hạn quà tặng không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                employee && (dataUpdate.employee = employee);
                sku && (dataUpdate.sku = sku);
                amount && (dataUpdate.amount = amount);
                month && (dataUpdate.month = month);
                year && (dataUpdate.year = year);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: limit_gift_configID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa limit_gift_config 
     * @param {objectId} limit_gift_configID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(limit_gift_configID) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 2
                };
                if (!checkObjectIDs([limit_gift_configID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị limit_gift_configID không hợp lệ'
                    });
                }


                const infoAfterDelete = await this.updateById(limit_gift_configID, conditionObj);

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa limit_gift_config 
     * @param {array} limit_gift_configID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(limit_gift_configID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(limit_gift_configID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị limit_gift_configID không hợp lệ'
                    });
                }

                const infoAfterDelete = await LIMIT_GIFT_CONFIG_COLL.deleteMany({
                    _id: {
                        $in: limit_gift_configID
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin limit_gift_config 
     * @param {objectId} limit_gift_configID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(limit_gift_configID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([limit_gift_configID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị limit_gift_configID không hợp lệ'
                    });
                }

                const infoLimit_gift_config = await LIMIT_GIFT_CONFIG_COLL.findById(limit_gift_configID)
                    .populate('region area distributor employee sku')

                if (!infoLimit_gift_config) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin cấu hình giới hạn quà tặng'
                    });
                }

                return resolve({
                    error: false,
                    data: infoLimit_gift_config
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách limit_gift_config 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listLimit_gift_config = await LIMIT_GIFT_CONFIG_COLL
                    .find({
                        state: 1
                    }).populate('region area distributor employee sku')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listLimit_gift_config) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách cấu hình giới hạn quà tặng'
                    });
                }

                return resolve({
                    error: false,
                    data: listLimit_gift_config
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách limit_gift_config theo bộ lọc
		* @param {number} amountFromNumber
		* @param {number} amountToNumber
		* @param {number} month
		* @param {number} year

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        amountFromNumber,
        amountToNumber,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };


                if (amountFromNumber && amountToNumber) {
                    conditionObj.amount = {
                        $gte: amountFromNumber,
                        $lt: amountToNumber,
                    };
                }


                const listLimit_gift_configByFilter = await LIMIT_GIFT_CONFIG_COLL
                    .find(conditionObj).populate('region area distributor employee sku')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listLimit_gift_configByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách cấu hình giới hạn quà tặng"
                    });
                }

                return resolve({
                    error: false,
                    data: listLimit_gift_configByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách limit_gift_config theo bộ lọc (server side)
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir,
        region
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                region && (conditionObj.region = ObjectID(region));
                const skip = (page - 1) * limit;

                if(region) {
                    delete conditionObj.region;
                    conditionObj['region._id'] = ObjectID(region);
                }

                const [{ metadata, data: listLimit_gift_configByFilter }] = await LIMIT_GIFT_CONFIG_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'products',
                            localField: 'sku',
                            foreignField: '_id',
                            as: 'sku'
                        }
                    },
                    {
                        $unwind: {
                            path: '$sku',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $facet: {
                            metadata: [ { $count: "total" }, { $addFields: { page } } ],
                            data: [ { $sort: sort }, { $skip: +skip }, { $limit: +limit } ]
                        }
                    }
                ]).allowDiskUse(true);

                const totalLimit_gift_config = metadata.length && metadata[0].total;

                if (!listLimit_gift_configByFilter) {
                    return resolve({
                        recordsTotal: totalLimit_gift_config,
                        recordsFiltered: totalLimit_gift_config,
                        data: []
                    });
                }

                const listLimit_gift_configDataTable = listLimit_gift_configByFilter.map((limit_gift_config, index) => {
                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${limit_gift_config._id}" type="checkbox" class="check-record check-record-${limit_gift_config._id}" _index ="${index + 1}"><label for="${limit_gift_config._id}"></label></div></td>`,
                        indexSTT: skip + index + 1,
                        region: limit_gift_config.region ? limit_gift_config.region.name : '',
                        area: limit_gift_config.area ? limit_gift_config.area.name : '',
                        distributor: limit_gift_config.distributor ? limit_gift_config.distributor.name : '',
                        employee: limit_gift_config.employee ? limit_gift_config.employee.fullname : '',
                        sku: limit_gift_config.sku ? limit_gift_config.sku.name : '',
                        amount: limit_gift_config.amount ? formatCurrency('###,###.', limit_gift_config.amount) : 0,
                        month: `${limit_gift_config.month && limit_gift_config.month.length > 50 ? limit_gift_config.month.substr(0,50) + "..." : limit_gift_config.month}`,
                        year: `${limit_gift_config.year && limit_gift_config.year.length > 50 ? limit_gift_config.year.substr(0,50) + "..." : limit_gift_config.year}`,
                        createAt: moment(limit_gift_config.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalLimit_gift_config,
                    recordsFiltered: totalLimit_gift_config,
                    data: listLimit_gift_configDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách limit_gift_config theo bộ lọc import
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterImport({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }



                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalLimit_gift_config = await LIMIT_GIFT_CONFIG_COLL.countDocuments(conditionObj);
               
                const listLimit_gift_configByFilter = await LIMIT_GIFT_CONFIG_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'products',
                            localField: 'sku',
                            foreignField: '_id',
                            as: 'sku'
                        }
                    },
                    {
                        $unwind: {
                            path: '$sku',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listLimit_gift_configByFilter) {
                    return resolve({
                        recordsTotal: totalLimit_gift_config,
                        recordsFiltered: totalLimit_gift_config,
                        data: []
                    });
                }

                return resolve({
                    error: false,
                    recordsTotal: totalLimit_gift_config,
                    recordsFiltered: totalLimit_gift_config,
                    data: listLimit_gift_configByFilter || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc limit_gift_config
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc limit_gift_config
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listLimit_gift_configByFilter = await LIMIT_GIFT_CONFIG_COLL.aggregate(arrayFilter)

                if (!listLimit_gift_configByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listLimit_gift_configByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listLimit_gift_configByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('HH:mm DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Tải file import excel mẫu limit_gift_config
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    fileImportExcelPreview({
        opts,
        arrayItemCustomerChoice
    }) {
        return new Promise(async resolve => {
            try {
                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition; // BỘ LỌC && LOẠI IMPORT
                let {
                    listFieldPrimaryKey
                } = condition; // DANH SÁCH PRIMARY KEY
                console.log({
                    opts
                });
                if (!isEmptyObject(opts)) {
                    condition.conditionDeleteImport.filter    = opts.filter;   // BỘ LỌC CỦA ADMIN
                    condition.conditionDeleteImport.condition = opts.condition // BỘ LỌC CỦA ADMIN
                }
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        let index = 0;
                        arrayItemCustomerChoice.map((elem) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            if (elem.dataDynamic && elem.dataDynamic.length) {
                                listFieldPrimaryKey = listFieldPrimaryKey && listFieldPrimaryKey.length && listFieldPrimaryKey.filter(key => key != elem.nameFieldRef); // LỌC PRIMARY KEY MÀ KHÔNG PHẢI REF

                                elem.dataDynamic.map(item => {
                                    workbook.sheet("Report").row(1).cell(index + 1).value(item);
                                    index++;
                                })
                            } else {
                                workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                                index++;
                            }
                        });

                        if (isTrue(condition.checkDownloadDataOld)) { // KIỂM TRA CÓ ĐÍNH ĐÈM DỮ LIỆU CŨ THEO ĐIỀU KIỆN
                            let listItemImport = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].listItemImport; // LIST FIELD ĐÃ ĐƯỢC CẤU HÌNH
                            let {
                                arrayFilter
                            } = this.getConditionArrayFilterExcel(listItemImport, condition.conditionDeleteImport.filter, condition.conditionDeleteImport.condition); // LẤY RA ARRAY ARREGATE

                            let groupByLimit_gift_config = {};
                            listFieldPrimaryKey.map(key => {
                                groupByLimit_gift_config = {
                                    ...groupByLimit_gift_config,
                                    [key]: '$' + key
                                }
                            });

                            arrayFilter = [
                                ...arrayFilter,
                                {
                                    $group: {
                                        _id: {
                                            groupByLimit_gift_config
                                        },
                                        listData: {
                                            $addToSet: "$$CURRENT"
                                        },
                                    }
                                }
                            ];
                            
                            const listLimit_gift_configByFilter = await LIMIT_GIFT_CONFIG_COLL.aggregate(arrayFilter);

                            listLimit_gift_configByFilter && listLimit_gift_configByFilter.length && listLimit_gift_configByFilter.map((item, indexLimit_gift_config) => {
                                let indexValue = 0;
                                arrayItemCustomerChoice.map((elem, indexChoice) => {
                                    let variable = elem.name.split('.');

                                    if (elem.dataDynamic && elem.dataDynamic.length) { // KIỂM TRA FIELD CÓ CHỌN DYNAMIC
                                        let listLimit_gift_configAfterFilter = item.listData && item.listData.length && item.listData.filter(value => {
                                            let valueOfField;
                                            if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                if (objDataOfVariable) {
                                                    valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                }
                                            } else {
                                                valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                            }
                                          
                                            if (elem.dataDynamic.includes(valueOfField)) { // CHECK NẾU VALUE === CỘT
                                                return value;
                                            }
                                        });

                                        let listValueExist = listLimit_gift_configAfterFilter.map(kpi => {
                                            let valueOfField;
                                            if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                let objDataOfVariable = kpi[variable[0]] ? kpi[variable[0]] : '';
                                                if (objDataOfVariable) {
                                                    valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                }
                                            } else {
                                                valueOfField = kpi[variable[0]] ? kpi[variable[0]] : '';
                                            }
                                            return valueOfField;
                                        });
                                        console.log({
                                            listValueExist
                                        });

                                        // console.log({
                                        //     listLimit_gift_configAfterFilter,
                                        //     listData: item.listData.length,
                                        //     dataDynamic: elem.dataDynamic.length
                                        // });
                                        elem.dataDynamic.map(dynamic => {
                                            if (item.listData.length > elem.dataDynamic.length) { // KIỂM TRA ĐỘ DÀI CỦA DATA SO VỚI SỐ CỘT
                                                // TODO: XỬ LÝ NHIỀU DYNAMIC
                                                console.log("======================");
                                            } else {
                                                // listLimit_gift_configAfterFilter.map(value => { // ARRAY DATA DYNAMIC
                                                for (const value of listLimit_gift_configAfterFilter) {
                                                    let valueOfField;
                                                    if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                        let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                        if (objDataOfVariable) {
                                                            valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                        }
                                                    } else {
                                                        valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                                    }
                                                    console.log({
                                                        valueOfField,
                                                        dynamic,
                                                        valueImportDynamic: value[elem.variableChoice]
                                                    });
                                                    if (valueOfField == dynamic) { // CHECK NẾU VALUE === CỘT
                                                        let valueImportDynamic = value[elem.variableChoice] ? value[elem.variableChoice] : '';

                                                        // INSERT DỮ LIỆU VÀO BẢNG VỚI FIELD ĐƯỢC CHỌN THEO DẠNG DYNAMIC
                                                        workbook.sheet("Report").row(indexLimit_gift_config + 2).cell(indexValue + 1).value(valueImportDynamic);
                                                        indexValue++;
                                                        break;
                                                    } else {
                                                        if(!listValueExist.includes(dynamic)){
                                                            indexValue++;
                                                            break;
                                                        }
                                                        // else if (elem.dataDynamic.length == item.listData.length) {
                                                        //     mark = true;
                                                        // }
                                                    }
                                                };
                                                // if (mark) {
                                                //     indexValue++;
                                                // }
                                            }
                                        })
                                    } else { // DẠNG STATIC
                                        let valueLimit_gift_config;
                                        if (variable.length > 1) {
                                            let objDataOfVariable = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                            if (objDataOfVariable) {
                                                valueLimit_gift_config = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                            }
                                        } else {
                                            valueLimit_gift_config = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                        }

                                        workbook.sheet("Report").row(indexLimit_gift_config + 2).cell(indexValue + 1).value(valueLimit_gift_config);
                                        indexValue++;
                                    }
                                });
                            });
                        }

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    pathWriteFile,
                                    domain
                                });
                            });
                    });
            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Upload File Excel Import Lưu Dữ Liệu limit_gift_config
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    importExcel({
        nameCollParent,
        arrayItemCustomerChoice,
        file,
    }) {
        return new Promise(async resolve => {
            try {

                XlsxPopulate.fromFileAsync(file.path)
                    .then(async workbook => {
                        let listData = [];
                        let index = 2;
                        let listFieldPrimaryKey      = []; //PRIMARY KEY
                        let conditionDeleteValuePrimaryKey = []; //array value của PRIMARY KEY từ file IMPORT
                        let condition     = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition;
                        let listFieldNameConditionDelete = [];
                        if (condition) {
                            listFieldPrimaryKey = condition.listFieldPrimaryKey && condition.listFieldPrimaryKey.length && condition.listFieldPrimaryKey;
                            listFieldNameConditionDelete = condition.conditionDeleteImport && condition.conditionDeleteImport.filter.map(item => item.fieldName);
                        }
                        console.log({
                            listFieldNameConditionDelete
                        });
                        const client = await MongoClient.connect(URL_DATABASE);
                        const db     = client.db(NAME_DATABASE);
                        console.log({
                            NAME_DATABASE
                        });
                        
                        for (; true;) {
                            if (arrayItemCustomerChoice && arrayItemCustomerChoice.length) {
                                let conditionObj = {};
                                let conditionObj__PrimaryKey = {}; //value của PRIMARY KEY từ file IMPORT

                                let totalLength = 0;
                                arrayItemCustomerChoice.map((item, index) => {
                                    if (item.dataDynamic && item.dataDynamic.length) {
                                        totalLength += item.dataDynamic.length;
                                    } else {
                                        totalLength++;
                                    }
                                });

                                let indexOfListField = 0;
                                let checkIsRequire = false;
                                let arrayConditionObjDynamic = [];
                                let arrayConditionObjDynamic__PrimaryKey = [];

                                for (let i = 0; i < totalLength; i++) {
                                    if (arrayItemCustomerChoice[indexOfListField].dataDynamic && arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                        let indexOfDynamic = 1;
                                        for (let valueDynamic of arrayItemCustomerChoice[indexOfListField].dataDynamic) {
                                            let letter = colName(i);
                                            let indexOfCeil = letter.toUpperCase() + index;

                                            let variable = workbook.sheet(0).cell(indexOfCeil).value().toString();
                                            // console.log({
                                            //     variable,
                                            //     _____this: arrayItemCustomerChoice[indexOfListField]
                                            // });
                                            if (variable) {

                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }
                                               
                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: valueDynamic.trim()
                                                });
                                              
                                                let conditionOfOneValueDynamic = {
                                                    [arrayItemCustomerChoice[indexOfListField].variableChoice]: variable
                                                }
                                                if (docs) {
                                                    conditionOfOneValueDynamic = {
                                                        ...conditionOfOneValueDynamic,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    }

                                                    console.log({
                                                        nameFieldRef: [arrayItemCustomerChoice[indexOfListField].nameFieldRef],
                                                        listFieldNameConditionDelete
                                                    });
                                                    // LẤY VALUE để xóa ở dữ liệu DYNAMIC
                                                    if (listFieldNameConditionDelete && listFieldNameConditionDelete.length && listFieldNameConditionDelete.includes(arrayItemCustomerChoice[indexOfListField].nameFieldRef)) {
                                                        arrayConditionObjDynamic__PrimaryKey = [
                                                            ...arrayConditionObjDynamic__PrimaryKey,
                                                            {
                                                                [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                            }
                                                        ]
                                                    }

                                                    // if (listFieldNameConditionDelete && listFieldNameConditionDelete.length && listFieldNameConditionDelete.includes(arrayItemCustomerChoice[indexOfListField].name)) {
                                                    //     conditionObj__PrimaryKey = {
                                                    //         ...conditionObj__PrimaryKey,
                                                    //         [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    //     }
                                                    // }
                                                }

                                                arrayConditionObjDynamic = [
                                                    ...arrayConditionObjDynamic,
                                                    conditionOfOneValueDynamic
                                                ];
                                            }

                                            if (indexOfDynamic < arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                                i++;
                                            }
                                            indexOfDynamic++;
                                        }
                                    } else {
                                        let letter = colName(i);
                                        let indexOfCeil = letter.toUpperCase() + index;
                                        let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                        if (arrayItemCustomerChoice[indexOfListField].isRequire && !variable) {
                                            checkIsRequire = true;
                                            break;
                                        }
                                        if (arrayItemCustomerChoice[indexOfListField].ref && arrayItemCustomerChoice[indexOfListField].ref != nameCollParent) {
                                            if (arrayItemCustomerChoice[indexOfListField].isRequire) {
                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: variable.trim()
                                                })
                                                // .sort({
                                                //     _id: -1
                                                // })
                                                // .limit(100)
                                                // .toArray(); 

                                                // console.log({
                                                //     docs,
                                                //     ___item: arrayItemCustomerChoice[indexOfListField]
                                                // });
                                                if (docs) {
                                                    conditionObj = {
                                                        ...conditionObj,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                    };
                                                    
                                                    if (listFieldNameConditionDelete && listFieldNameConditionDelete.length && listFieldNameConditionDelete.includes(arrayItemCustomerChoice[indexOfListField].ref)) {
                                                        conditionObj__PrimaryKey = {
                                                            ...conditionObj__PrimaryKey,
                                                            [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                        }
                                                    }

                                                    if (arrayItemCustomerChoice[indexOfListField].mappingRef && arrayItemCustomerChoice[indexOfListField].mappingRef.length) {
                                                        arrayItemCustomerChoice[indexOfListField].mappingRef.map(mapping => {
                                                            conditionObj = {
                                                                ...conditionObj,
                                                                [mapping]: docs[mapping]
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                [arrayItemCustomerChoice[indexOfListField].name]: variable
                                            };
                                            if (listFieldNameConditionDelete && listFieldNameConditionDelete.length && listFieldNameConditionDelete.includes(arrayItemCustomerChoice[indexOfListField].name)) {
                                                conditionObj__PrimaryKey = {
                                                    ...conditionObj__PrimaryKey,
                                                    [arrayItemCustomerChoice[indexOfListField].name]: variable
                                                }
                                            }
                                        }
                                    }

                                    indexOfListField++;
                                }
                                if (checkIsRequire) {
                                    break;
                                }

                                conditionObj = {
                                    ...conditionObj,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                }

                                let arrayCondditionObj = [];
                                if (arrayConditionObjDynamic && arrayConditionObjDynamic.length) {
                                    arrayConditionObjDynamic.map(item => {
                                        arrayCondditionObj = [
                                            ...arrayCondditionObj,
                                            {
                                                ...conditionObj,
                                                ...item
                                            }
                                        ];
                                    });

                                    // Xóa với điều kiện => với PRIMARY KEY
                                    if (listFieldNameConditionDelete && listFieldNameConditionDelete.length) {
                                        if (arrayConditionObjDynamic__PrimaryKey && arrayConditionObjDynamic__PrimaryKey.length) { //value của PRIMARY KEY với DYNAMIC REF 
                                            arrayConditionObjDynamic__PrimaryKey.map(item => {
                                                conditionDeleteValuePrimaryKey = [
                                                    ...conditionDeleteValuePrimaryKey,
                                                    {
                                                        ...conditionObj__PrimaryKey,
                                                        ...item
                                                    }
                                                ];
                                            });
                                        } else {
                                            conditionDeleteValuePrimaryKey = [
                                                ...conditionDeleteValuePrimaryKey,
                                                conditionObj__PrimaryKey
                                            ];
                                        }
                                    }
                                } else {
                                    arrayCondditionObj = [
                                        ...arrayCondditionObj,
                                        conditionObj
                                    ];
                                    if (listFieldNameConditionDelete && listFieldNameConditionDelete.length) {
                                        conditionDeleteValuePrimaryKey = [
                                            ...conditionDeleteValuePrimaryKey,
                                            conditionObj__PrimaryKey
                                        ];
                                    }
                                }
                                
                               
                                listData = [
                                    ...listData,
                                    ...arrayCondditionObj
                                ];

                                index++;
                            }
                        }
                        console.log({
                            conditionDeleteValuePrimaryKey,
                        });
                     
                        await fs.unlinkSync(file.path);

                        if (listData.length) {
                            await this.changeDataImport({
                                condition,
                                listLimit_gift_config: listData,
                                conditionDeleteValuePrimaryKey
                            });
                        } else {
                            return resolve({
                                error: true,
                                message: 'Import thất bại'
                            });
                        }

                        return resolve({
                            error: false,
                            message: 'Import thành công'
                        });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lưu Dữ Liệu Theo Lựa Chọn limit_gift_config
     * @param {object} listLimit_gift_config
     * @param {object} condition
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    changeDataImport({
        condition,
        listLimit_gift_config,
        conditionDeleteValuePrimaryKey
    }) {
        return new Promise(async resolve => {
            try {
                if (isTrue(condition.delete)) { // XÓA DATA CŨ
                    if (isTrue(condition.deleteAll)) { // XÓA TẤT CẢ DỮ LIỆU
                        console.log("====================XÓA TẤT CẢ DỮ LIỆU====================");
                        // await LIMIT_GIFT_CONFIG_COLL.deleteMany({});
                        let listDataAfterInsert = await LIMIT_GIFT_CONFIG_COLL.insertMany(listLimit_gift_config);
                        
                        if (listDataAfterInsert && listDataAfterInsert.length) {
                            let error = false;
                            listDataAfterInsert.map(item => {
                                if (!item) {
                                    error = true;
                                }
                            });
                            if (!error) {
                                let listLimitGiftID = listDataAfterInsert.map(item => item._id);
                                await LIMIT_GIFT_CONFIG_COLL.deleteMany({
                                    _id: {
                                        $nin: listLimitGiftID
                                    }
                                });
                                return resolve({
                                    error: false,
                                    message: 'Insert success',
                                    data: listDataAfterInsert
                                });
                            } else {
                                return resolve({
                                    error: true,
                                    message: 'Insert failed',
                                    // data: listDataAfterInsert
                                });
                            }
                        } else {
                            return resolve({
                                error: true,
                                message: 'Insert failed',
                                // data: listDataAfterInsert
                            });
                        }
                        
                    } else { // XÓA VỚI ĐIỀU KIỆN
                        console.log("====================XÓA VỚI ĐIỀU KIỆN====================");

                        /**
                         * ===========================================================================
                         * =========================XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===========================================================================
                         */
                        let {
                            filter,
                            condition: conditionMultiple,
                        } = condition.conditionDeleteImport;
                        
                        let conditionObj = {
                            state: 1,
                            $or: []
                        };
                        

                        if (!filter || !filter.length) {
                            return resolve({
                                error: true,
                                message: 'Filter do not exist'
                            });
                        }


                        if (filter && filter.length) {
                            if (filter.length > 1) {

                                filter.map(filterObj => {
                                    if (filterObj.type === 'ref') {
                                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionFieldRef);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionFieldRef
                                            };
                                        }
                                    } else {
                                        const conditionByFilter = this.getConditionObj(filterObj);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionByFilter);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionByFilter
                                            };
                                        }
                                    }
                                });

                            } else {
                                let {
                                    type,
                                    ref,
                                    fieldRefName
                                } = filter[0];

                                if (type === 'ref') {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(ref, fieldRefName)
                                    };
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(filter[0])
                                    };
                                }
                            }
                        }

                        if (conditionObj.$or && !conditionObj.$or.length) {
                            delete conditionObj.$or;
                        }

                        // let listAfterDelete = await LIMIT_GIFT_CONFIG_COLL.deleteMany({
                        //     ...conditionObj
                        // });
                        /**
                         * ===============================================================================
                         * =========================END XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===============================================================================
                         */

                        if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                            // let listValueAfterDelete = conditionDeleteValuePrimaryKey && conditionDeleteValuePrimaryKey.length && conditionDeleteValuePrimaryKey.map(value => {
                            //     return LIMIT_GIFT_CONFIG_COLL.deleteMany(value);
                            // });

                            // let result = await Promise.all(listValueAfterDelete);
                            // console.log({
                            //     result
                            // });
                           
                            let error = false;
                            let listLimit_gift_configID = [];
                            for (let item of listLimit_gift_config) {
                                let listConditionFindOneUpdate = {};
                                let {
                                    listFieldPrimaryKey
                                } = condition;
                                listFieldPrimaryKey.map(elem => {
                                    listConditionFindOneUpdate = {
                                        ...listConditionFindOneUpdate,
                                        [elem]: item[elem]
                                    }
                                });

                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    state: 1
                                }

                                let checkExist = await LIMIT_GIFT_CONFIG_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                    $set: item
                                }, {
                                    upsert: true
                                });
                                if (!checkExist) {
                                    error = true;
                                } else {
                                    listLimit_gift_configID = [
                                        ...listLimit_gift_configID,
                                        checkExist._id
                                    ];
                                }
                            }

                            if (error) {
                                return resolve({
                                    error: true,
                                    message: 'Insert falied'
                                });
                            } else {
                                console.log({
                                    listLimit_gift_configID
                                });
                                let listValueAfterDelete = conditionDeleteValuePrimaryKey && conditionDeleteValuePrimaryKey.length && conditionDeleteValuePrimaryKey.map(value => {
                                    return LIMIT_GIFT_CONFIG_COLL.deleteMany({
                                        ...value,
                                        _id: {
                                            $nin: listLimit_gift_configID
                                        }
                                    });
                                });
    
                                let result = await Promise.all(listValueAfterDelete);
                                console.log({
                                    result, listLimit_kpi_config
                                });
    
                                return resolve({
                                    error: false,
                                    message: 'Insert success'
                                });
                            }

                        } else { // INSERT CÁI MỚI
                            // let listAfterDelete = await LIMIT_GIFT_CONFIG_COLL.deleteMany({
                            //     ...conditionObj
                            // });

                            console.log("====================INSERT CÁI MỚI 2====================");
                            let listDataAfterInsert = await LIMIT_GIFT_CONFIG_COLL.insertMany(listLimit_gift_config);
                            if (listDataAfterInsert && listDataAfterInsert.length) {
                                let error = false;
                                listDataAfterInsert.map(item => {
                                    if (!item) {
                                        error = true;
                                    }
                                });
                                if (!error) {
                                    let listLimit_gift_configID = listDataAfterInsert.map(item => item._id);
                                    let listValueAfterDelete = conditionDeleteValuePrimaryKey && conditionDeleteValuePrimaryKey.length && conditionDeleteValuePrimaryKey.map(value => {
                                        return LIMIT_GIFT_CONFIG_COLL.deleteMany({
                                            ...value,
                                            _id: {
                                                $nin: listLimit_gift_configID
                                            }
                                        });
                                    });
                                    let result = await Promise.all(listValueAfterDelete);

                                    // await LIMIT_KPI_CONFIG_COLL.deleteMany({
                                    //     ...conditionObj,
                                    //     _id: {
                                    //         $nin: listLimit_gift_configID
                                    //     }
                                    // });
                                    return resolve({
                                        error: false,
                                        message: 'Insert success',
                                        data: listDataAfterInsert
                                    });
                                } else {
                                    return resolve({
                                        error: true,
                                        message: 'Insert failed',
                                        // data: listDataAfterInsert
                                    });
                                }
                            } else {
                                return resolve({
                                    error: true,
                                    message: 'Insert failed',
                                    // data: listDataAfterInsert
                                });
                            }
                        }
                    }
                } else { // KHÔNG XÓA DATA CŨ
                    if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                        console.log("====================KIỂM TRA TỒN TẠI VÀ UPDATE====================");
                        for (let item of listLimit_gift_config) {
                            let listConditionFindOneUpdate = {};
                            let {
                                listFieldPrimaryKey
                            } = condition;
                            listFieldPrimaryKey.map(elem => {
                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    [elem]: item[elem]
                                }
                            });

                            listConditionFindOneUpdate = {
                                ...listConditionFindOneUpdate,
                                state: 1
                            }
                            console.log({
                                listConditionFindOneUpdate
                            });
                            let checkExist = await LIMIT_GIFT_CONFIG_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                $set: item
                            }, {
                                upsert: true
                            });
                        }
                        return resolve({
                            error: false,
                            message: 'Insert success'
                        });
                    } else { // INSERT CÁI MỚI
                        console.log("====================INSERT CÁI MỚI====================");
                        let listDataAfterInsert = await LIMIT_GIFT_CONFIG_COLL.insertMany(listLimit_gift_config);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    }
                }

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc limit_gift_config
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
            $or: []
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            let mappingRef = [];
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                    mappingRef = element.mappingRef;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            fieldID: item.fieldID,
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            ref: item.coll,
                            variable: item.name,
                            nameFieldRef,
                            dataEnum: item.dataEnum,
                            isRequire: item.isRequire,
                            dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                            variableChoice: item.variableChoice,
                            nameCollChoice: item.nameCollChoice,
                            mappingRef: mappingRef,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                fieldID: item.fieldID,
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                ref: item.coll,
                                variable: item.name,
                                nameFieldRef,
                                dataEnum: item.dataEnum,
                                isRequire: item.isRequire,
                                dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                variableChoice: item.variableChoice,
                                nameCollChoice: item.nameCollChoice,
                                mappingRef: mappingRef,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    dataEnum: item.dataEnum,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {
            if (filter.length > 1) {

                filter.map(filterObj => {
                    if (filterObj.type === 'ref') {
                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionFieldRef);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionFieldRef
                            };
                        }
                    } else {
                        const conditionByFilter = this.getConditionObj(filterObj);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionByFilter);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionByFilter
                            };
                        }
                    }
                });

            } else {
                let {
                    type,
                    ref,
                    fieldRefName
                } = filter[0];

                if (type === 'ref') {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(ref, fieldRefName)
                    };
                } else {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(filter[0])
                    };
                }
            }
        }

        if (conditionObj.$or && !conditionObj.$or.length) {
            delete conditionObj.$or;
        }



        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let collName = pluralize.plural(item.coll);
                let checkPluralColl = collName[collName.length - 1];

                if (checkPluralColl.toLowerCase() != 's') {
                    collName += 's';
                }

                let lookup = [{
                        $lookup: {
                            from: collName,
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        let collNamePopulate = pluralize.plural(populate.coll);
                        let checkPluralColl = collNamePopulate[collNamePopulate.length - 1];

                        if (checkPluralColl.toLowerCase() != 's') {
                            collNamePopulate += 's';
                        }

                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: collNamePopulate,
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Xóa limit_gift_config
     * @param {objectId} limit_gift_configID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteLimit_gift_config(limit_gift_configID) {
        return new Promise(async resolve => {
            try {
                let ids = [limit_gift_configID];

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị limit_gift_configID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.updateWhereClause({
                    _id: {
                        $in: ids
                    }
                }, {
                    state: 2
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy thông tin limit_gift_config
     * @param {objectId} limit_gift_configID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoLimit_gift_config({
        limit_gift_configID,
        select,
        filter = {},
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];
                let conditionObj = {
                    state: 1
                };

                if (!checkObjectIDs([limit_gift_configID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị limit_gift_configID không hợp lệ',
                        status: 400
                    });
                }

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                Object.keys(filter).map(key => {
                    if (![].includes(key)) {
                        delete filter[key];
                    }
                });

                let {} = filter;


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'employee', 'sku'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoLimit_gift_config = await LIMIT_GIFT_CONFIG_COLL
                    .findOne({
                        _id: limit_gift_configID,
                        ...conditionObj
                    })
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoLimit_gift_config = await LIMIT_GIFT_CONFIG_COLL.populate(infoLimit_gift_config, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoLimit_gift_config = await LIMIT_GIFT_CONFIG_COLL.populate(infoLimit_gift_config, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoLimit_gift_config) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin limit_gift_config',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoLimit_gift_config,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách limit_gift_config
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListLimit_gift_configs({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                } else {

                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['region', 'area', 'distributor', 'employee', 'sku', 'amountFromNumber', 'amountToNumber', 'month', 'year'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    region,
                    area,
                    distributor,
                    employee,
                    sku,
                    amountFromNumber,
                    amountToNumber,
                    month,
                    year
                } = filter;

                month && (conditionObj.month = month);
                year && (conditionObj.year = year);

                region && (conditionObj.region = region);
                area && (conditionObj.area = area);
                distributor && (conditionObj.distributor = distributor);
                employee && (conditionObj.employee = employee);
                sku && (conditionObj.sku = sku);

                if (amountFromNumber && amountToNumber) {
                    conditionObj.amount = {
                        $gte: amountFromNumber,
                        $lte: amountToNumber,
                    };
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'employee', 'sku'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                console.log({ 'BEFORE': conditionObj })
                let listLimit_gift_configs = await LIMIT_GIFT_CONFIG_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listLimit_gift_configs = await LIMIT_GIFT_CONFIG_COLL.populate(listLimit_gift_configs, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listLimit_gift_configs = await LIMIT_GIFT_CONFIG_COLL.populate(listLimit_gift_configs, `${ref}.${field}`);
                        }
                    }
                }

                if (!listLimit_gift_configs) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách limit_gift_config',
                        status: 400
                    });
                }

                let totalRecord = await LIMIT_GIFT_CONFIG_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                console.log({ 'AFTER': conditionObj })

                return resolve({
                    error: false,
                    data: {
                        records: listLimit_gift_configs,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    downloadLimit_gift_configsExcel({
        keyword,
        filter,
        condition,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1,
                    $or: [],
                    $and: []
                };

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj.$and.push(conditionFieldRef);
                                    // conditionObj = {
                                    //     ...conditionObj,
                                    //     ...conditionFieldRef
                                    // };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj.$and.push(conditionByFilter);
                                    // conditionObj = {
                                    //     ...conditionObj,
                                    //     ...conditionByFilter
                                    // };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }
                if (conditionObj.$and && !conditionObj.$and.length) {
                    delete conditionObj.$and;
                }

                console.log({
                    conditionObj
                });

                const listLimit_gift_configByFilter = await LIMIT_GIFT_CONFIG_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    
                    {
                        $lookup: {
                            from: 'products',
                            localField: 'sku',
                            foreignField: '_id',
                            as: 'sku'
                        }
                    },
                    {
                        $unwind: {
                            path: '$sku',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'brands',
                            localField: 'sku.brand',
                            foreignField: '_id',
                            as: 'sku.brand'
                        }
                    },
                    {
                        $unwind: {
                            path: '$sku.brand',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $sort: {
                            modifyAt: -1
                        }
                    }
                ]);

                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        workbook.sheet("Report").row(1).cell(1).value("Miền");
                        workbook.sheet("Report").row(1).cell(2).value("Vùng");
                        workbook.sheet("Report").row(1).cell(3).value("Mã Nhà Phân Phối");
                        workbook.sheet("Report").row(1).cell(4).value("Nhà Phân Phối");
                        workbook.sheet("Report").row(1).cell(5).value("Mã Nhân viên");
                        workbook.sheet("Report").row(1).cell(6).value("Tên Nhân Viên");
                        workbook.sheet("Report").row(1).cell(7).value("Mã Sản Phẩm");
                        workbook.sheet("Report").row(1).cell(8).value("Tên Sản Phẩm");
                        workbook.sheet("Report").row(1).cell(9).value("Brand");
                        workbook.sheet("Report").row(1).cell(10).value("Quy Cách");
                        workbook.sheet("Report").row(1).cell(11).value("Số lượng");
                        workbook.sheet("Report").row(1).cell(12).value("Tháng");
                        workbook.sheet("Report").row(1).cell(13).value("Năm");
                        workbook.sheet("Report").row(1).cell(14).value("Ngày");

                        listLimit_gift_configByFilter && listLimit_gift_configByFilter.length && listLimit_gift_configByFilter.map((item, index) => {
                            workbook.sheet("Report").row(index + 2).cell(1).value(item.region && item.region.name);
                            workbook.sheet("Report").row(index + 2).cell(2).value(item.area && item.area.name);
                            workbook.sheet("Report").row(index + 2).cell(3).value(item.distributor && item.distributor.code);
                            workbook.sheet("Report").row(index + 2).cell(4).value(item.distributor && item.distributor.name);
                            workbook.sheet("Report").row(index + 2).cell(5).value(item.employee && item.employee.code);
                            workbook.sheet("Report").row(index + 2).cell(6).value(item.employee && item.employee.fullname);
                            workbook.sheet("Report").row(index + 2).cell(7).value(item.sku && item.sku.SKU);
                            workbook.sheet("Report").row(index + 2).cell(8).value(item.sku && item.sku.name);
                            workbook.sheet("Report").row(index + 2).cell(9).value(item.sku && item.sku.brand && item.sku.brand.name);
                            workbook.sheet("Report").row(index + 2).cell(10).value(item.sku && item.sku.description);
                            workbook.sheet("Report").row(index + 2).cell(11).value(item.amount);
                            workbook.sheet("Report").row(index + 2).cell(12).value(item.month);
                            workbook.sheet("Report").row(index + 2).cell(13).value(item.year);
                            workbook.sheet("Report").row(index + 2).cell(14).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));
                        });

                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    status: 200
                                });
                            });
                    });

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

}

exports.MODEL = new Model;