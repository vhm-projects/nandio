"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const XlsxPopulate = require('xlsx-populate');
const ISOdate = require('isodate');
const { PromisePool } = require('@supercharge/promise-pool');

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    checkNumberIsValidWithRange
} = require('../../../utils/utils');
const timeUtils = require('../../../utils/time_utils');
const {
    STATUS_EMPLOYEE_TYPE
} = require('../../employee/constants/employee');

/**
 * BASE
 */
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const NOTIFICATION_COLL = require('../databases/notification-coll');
const RECEIVER_NOTIFICATION_COLL = require('../databases/receiver_notification-coll');
const { EMPLOYEE_COLL } = require('../../employee');


class Model extends BaseModel {
    constructor() {
        super(NOTIFICATION_COLL);
    }

    /**
     * Tạo mới notification
    * @param {string} name
    * @param {string} description
    * @param {string} content
    * @param {array} receivers
    * @param {number} status
    * @param {date} from
    * @param {date} to
    * @param {objectId} userCreate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    insert({
        name,
        description,
        content,
        receivers,
        order,
        status = 1,
        from,
        to,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (name.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tiêu đề không được lớn hơn 125 ký tự'
                    });
                }

                if (!name) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tiêu đề'
                    });
                }

                if (description.length > 256) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mô tả không được lớn hơn 256 ký tự'
                    });
                }

                if (receivers && !receivers.length) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập danh sách người nhận'
                    });
                }

                if (!from) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập ngày bắt đầu'
                    });
                }

                if (!to) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập ngày kết thúc'
                    });
                }


                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trang thái không hợp lệ'
                    });
                }

                let dataInsert = {
                    name,
                    description,
                    content,
                    order,
                    status,
                    from: new Date(from),
                    to: new Date(to),
                    userCreate
                };

                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo thông báo thất bại'
                    });
                }

                if (receivers && receivers.length) {
                    if (receivers.length === 1 && receivers[0] === 'all') {
                        let arrEmployee = await EMPLOYEE_COLL.find({
                            state: 1
                        }).lean();

                        let arrNotificationEmployee = arrEmployee.map(receiver => ({
                            receiver,
                            notification: infoAfterInsert._id
                        }))

                        await RECEIVER_NOTIFICATION_COLL.insertMany(arrNotificationEmployee);
                    } else {
                        let arrNotificationEmployee = receivers.map(receiver => ({
                            receiver,
                            notification: infoAfterInsert._id
                        }))

                        await RECEIVER_NOTIFICATION_COLL.insertMany(arrNotificationEmployee);
                    }
                }


                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Cập nhật notification 
     * @param {objectId} notificationID
    * @param {string} name
    * @param {string} description
    * @param {string} content
    * @param {array} receivers
    * @param {number} status
    * @param {date} from
    * @param {date} to
    * @param {number} order
    * @param {objectId} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    update({
        notificationID,
        name,
        description,
        content,
        receivers,
        status,
        from,
        to,
        order,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([notificationID])) {
                    return resolve({
                        error: true,
                        message: 'notificationID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (name.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tiêu đề không được lớn hơn 125 ký tự'
                    });
                }

                if (!name) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tiêu đề cho notification'
                    });
                }

                if (description.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mô tả không được lớn hơn 125 ký tự'
                    });
                }

                if (receivers && !receivers.length) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập danh sách người nhận cho notification'
                    });
                }

                if (!from) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập ngày bắt đầu cho notification'
                    });
                }

                if (!to) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập ngày kết thúc cho notification'
                    });
                }


                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trang thái không hợp lệ'
                    });
                }

                const checkExists = await NOTIFICATION_COLL.findById(notificationID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'thông báo không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                name && (dataUpdate.name = name);
                from && (dataUpdate.from = new Date(from));
                to && (dataUpdate.to = new Date(to));
                dataUpdate.description = description;
                dataUpdate.content = content;
                dataUpdate.status = status;
                dataUpdate.order = order;

                // if(from){
                //     dataUpdate.from = ISOdate(moment(from).format());
                // }

                // if(to){
                //     dataUpdate.to = ISOdate(moment(to).format('YYYY-MM-DDT23:00:ssZ'));
                // }
                
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: notificationID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                if (receivers && receivers.length) {
                    if (receivers.length === 1 && receivers[0] === 'all') {
                        let arrEmployee = await EMPLOYEE_COLL.find({
                            state: 1
                        }).lean();

                        let arrNotificationEmployee = arrEmployee.map(receiver => ({
                            receiver,
                            notification: infoAfterUpdate._id
                        }))

                        await RECEIVER_NOTIFICATION_COLL.deleteMany({
                            notification: infoAfterUpdate._id
                        });
                        await RECEIVER_NOTIFICATION_COLL.insertMany(arrNotificationEmployee);
                    } else {
                        let arrNotificationEmployee = receivers.map(receiver => ({
                            receiver,
                            notification: infoAfterUpdate._id
                        }))

                        await RECEIVER_NOTIFICATION_COLL.deleteMany({
                            notification: infoAfterUpdate._id
                        });
                        await RECEIVER_NOTIFICATION_COLL.insertMany(arrNotificationEmployee);
                    }
                } else {
                    await RECEIVER_NOTIFICATION_COLL.deleteMany({
                        notification: infoAfterUpdate._id
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Cập nhật notification (không bắt buộc)
     * @param {objectId} notificationID
    * @param {string} name
    * @param {string} description
    * @param {string} content
    * @param {array} receivers
    * @param {number} status
    * @param {date} from
    * @param {date} to
    * @param {number} order
    * @param {objectId} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    updateNotRequire({
        notificationID,
        name,
        description,
        content,
        receivers,
        status,
        from,
        to,
        order,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([notificationID])) {
                    return resolve({
                        error: true,
                        message: 'notificationID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (receivers && !receivers.length) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập danh sách người nhận cho notification'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trang thái không hợp lệ'
                    });
                }

                const checkExists = await NOTIFICATION_COLL.findById(notificationID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'thông báo không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                name && (dataUpdate.name = name);
                description && (dataUpdate.description = description);
                content && (dataUpdate.content = content);
                status && (dataUpdate.status = status);
                from && (dataUpdate.from = new Date(from));
                to && (dataUpdate.to = new Date(to));
                order && (dataUpdate.order = order);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: notificationID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa notification 
     * @param {objectId} notificationID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(notificationID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([notificationID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị notificationID không hợp lệ'
                    });
                }

                const infoAfterDelete = await this.updateById(notificationID, {
                    state: 2
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa notification 
     * @param {array} notificationID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(notificationID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(notificationID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị notificationID không hợp lệ'
                    });
                }

                const infoAfterDelete = await NOTIFICATION_COLL.updateMany({
                    _id: {
                        $in: notificationID
                    }
                }, {
                    state: 2,
                    modifyAt: new Date()
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin notification 
     * @param {objectId} notificationID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(notificationID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([notificationID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị notificationID không hợp lệ'
                    });
                }

                const infoNotification = await NOTIFICATION_COLL.findById(notificationID)
                    .populate('receivers')

                if (!infoNotification) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin thông báo'
                    });
                }

                return resolve({
                    error: false,
                    data: infoNotification
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách notification 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listNotification = await NOTIFICATION_COLL
                    .find({
                        state: 1
                    }).populate('receivers')
                    .sort({
                        order: 1,
                        modifyAt: -1
                    })
                    .lean();

                if (!listNotification) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách thông báo'
                    });
                }

                return resolve({
                    error: false,
                    data: listNotification
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách notification theo bộ lọc
    * @param {string} keyword
    * @enum {number} status
    * @param {date} fromDateRange
    * @param {date} toDateRange
    * @param {number} order
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: array, message?: string }}
    */
    getListByFilter({
        keyword,
        status,
        fromDateRange,
        toDateRange,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        name: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        description: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        content: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                status && (conditionObj.status = status);

                if (fromDateRange) {
                    let [fromDate, toDate] = fromDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.from = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                if (toDateRange) {
                    let [fromDate, toDate] = toDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.to = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }


                const listNotificationByFilter = await NOTIFICATION_COLL
                    .find(conditionObj).populate('receivers')
                    .sort({
                        order: 1,
                        modifyAt: -1
                    })
                    .lean();

                if (!listNotificationByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách thông báo"
                    });
                }

                return resolve({
                    error: false,
                    data: listNotificationByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách notification theo bộ lọc (server side)
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        name: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        description: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        content: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }



                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trang thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }


                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    order: 1,
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalNotification = await NOTIFICATION_COLL.countDocuments(conditionObj);

                const listNotificationByFilter = await NOTIFICATION_COLL.aggregate([

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listNotificationByFilter) {
                    return resolve({
                        recordsTotal: totalNotification,
                        recordsFiltered: totalNotification,
                        data: []
                    });
                }

                if(listNotificationByFilter && listNotificationByFilter.length > 0) {
                    for(let notification of listNotificationByFilter) {
                        let totalEmployeeReceivce = await RECEIVER_NOTIFICATION_COLL.find({
                            notification: notification._id,
                            state: 1
                        }).lean();

                        let totalNotiSeen = await RECEIVER_NOTIFICATION_COLL.find({
                            notification: notification._id,
                            state: 1,
                            status: 2
                        }).lean();

                        notification.totalEmployeeReceivce = totalEmployeeReceivce.length;
                        notification.totalNotiSeen         = totalNotiSeen.length;
                    }
                }

                const listNotificationDataTable = listNotificationByFilter.map((notification, index) => {
                    let status = '';
                    if (notification.status == 1) {
                        status = 'checked';
                    }

                    let order = '';
                    if (notification.order || notification.order == 0) {
                        order = notification.order;
                    }

                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${notification._id}" type="checkbox" class="check-record check-record-${notification._id}" _index ="${index + 1}"><label for="${notification._id}"></label></div></td>`,
                        name: `<a href="/notification/update-notification-by-id?notificationID=${notification._id}">${notification.name && notification.name.length > 50 ? notification.name.substr(0,50) + "..." : notification.name} </a>`,
                        totalEmployeeReceivce: notification.totalEmployeeReceivce || 0,
                        totalNotiSeen: notification.totalNotiSeen || 0,
                        status: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-status" _notificationID="${notification._id}" type="checkbox" id="${notification._id}" ${status} style="width: 40px;height: 20px;"></div></td>`,
                        from: moment(notification.from).format("L"),
                        to: moment(notification.to).format("L"),
                        order: `<td><input type="number" _notificationID="${notification._id}" class="form-control change-order" value="${order}"></td>`,
                        createAt: moment(notification.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalNotification,
                    recordsFiltered: totalNotification,
                    data: listNotificationDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    getListEmployeeServerSideTableSub({
        idsSelected = [],
        keyword,
        filter,
        condition,
        page,
        limit,
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        fullname: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        code: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        username: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        email: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        password: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                if (filter && filter.length) {
                    if (filter.length > 1) {
                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });
                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = this.getConditionObj(ref, fieldRefName);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                const skip = (page - 1) * limit;
                const totalEmployee = await EMPLOYEE_COLL.countDocuments(conditionObj);

                const listEmployeeByFilter = await EMPLOYEE_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'type_employees',
                            localField: 'type_employee',
                            foreignField: '_id',
                            as: 'type_employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$type_employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                    {
                        $sort: {
                            modifyAt: -1
                        }
                    },
                ])

                if (!listEmployeeByFilter) {
                    return resolve({
                        recordsTotal: totalEmployee,
                        recordsFiltered: totalEmployee,
                        data: []
                    });
                }

                const listEmployeeDataTable = listEmployeeByFilter.map((employee) => {
                    let action = `<button type="button" class="btn btn-soft-primary btn-sm btn-select" data-id="${employee._id}">Chọn</button>`;

                    if (idsSelected.includes(employee._id.toString())) {
                        action = `<button type="button" class="btn btn-soft-danger btn-sm btn-unselect" data-id="${employee._id}">Bỏ chọn</button>`;
                    }

                    return {
                        fullname: `<a href="/employee/update-employee-by-id?employeeID=${employee._id}">${employee.fullname && employee.fullname.length > 50 ? employee.fullname.substr(0,50) + "..." : employee.fullname} </a>`,
                        code: `${employee.code && employee.code.length > 50 ? employee.code.substr(0,50) + "..." : employee.code}`,
                        username: `${employee.username && employee.username.length > 50 ? employee.username.substr(0,50) + "..." : employee.username}`,
                        email: `${employee.email && employee.email.length > 50 ? employee.email.substr(0,50) + "..." : employee.email}`,
                        region: employee.region ? employee.region.name : '',
                        area: employee.area ? employee.area.name : '',
                        distributor: employee.distributor ? employee.distributor.name : '',
                        type_employee: employee.type_employee ? employee.type_employee.name : '',
                        status: `<span class="badge" style="background-color: ${STATUS_EMPLOYEE_TYPE[employee.status].color}">${STATUS_EMPLOYEE_TYPE[employee.status].value} </span>`,
                        createAt: moment(employee.createAt).format('HH:mm DD/MM/YYYY'),
                        action
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalEmployee,
                    recordsFiltered: totalEmployee,
                    data: listEmployeeDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc notification
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc notification
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listNotificationByFilter = await NOTIFICATION_COLL.aggregate(arrayFilter)

                if (!listNotificationByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listNotificationByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listNotificationByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc notification
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            dataEnum: item.dataEnum,
                            nameCollChoice: item.nameCollChoice,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                dataEnum: item.dataEnum,
                                nameCollChoice: item.nameCollChoice,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (markPopulate == true) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {

            if (filter.length > 1) {
                condition === 'OR' && (conditionObj.$or = []);
                condition === 'AND' && (conditionObj.$and = []);

                filter.map(filterObj => {
                    const getConditionByFilter = this.getConditionObj(filterObj);

                    if (condition === 'OR') {
                        conditionObj.$or.push(getConditionByFilter);
                    } else {
                        conditionObj.$and.push(getConditionByFilter);
                    }
                })
            } else {
                conditionObj = {
                    ...conditionObj,
                    ...this.getConditionObj(filter[0])
                };
            }
        }


        if (!isEmptyObject(objFilterStatic)) {

            if (objFilterStatic.status) {
                if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                    return resolve({
                        error: true,
                        message: "trang thái không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    status: Number(objFilterStatic.status)
                }
            }

        }


        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let lookup = [{
                        $lookup: {
                            from: pluralize.plural(item.coll),
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: pluralize.plural(populate.coll),
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            order: 1,
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Chi tiết
     * @param {objectId} notificationID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoNotification({
        notificationID,
        select,
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];

                if (!checkObjectIDs([notificationID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị notificationID không hợp lệ',
                        status: 400
                    });
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['receivers'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoNotification = await NOTIFICATION_COLL
                    .findById(notificationID)
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoNotification = await NOTIFICATION_COLL.populate(infoNotification, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoNotification = await NOTIFICATION_COLL.populate(infoNotification, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoNotification) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin notification',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoNotification,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Danh sách
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListNotifications({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { state: 1 };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['receivers', 'status', 'fromDateRange', 'toDateRange'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    receivers,
                    status,
                    fromDate,
                    toDate,
                } = filter;

                if (receivers && rece2ivers.length) {
                    conditionObj.receivers = {
                        $in: receivers
                    };
                }

                status && (conditionObj.status = status);

                if (fromDate && toDate) {
                    let _fromDate = moment(fromDate).startOf('day').format();
                    let _toDate = moment(toDate).endOf('day').format();

                    conditionObj.from = {
                        $gte: new Date(_fromDate),
                    }
                    conditionObj.to = {
                        $lte: new Date(_toDate)
                    }
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['receivers'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listNotifications = await NOTIFICATION_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listNotifications = await NOTIFICATION_COLL.populate(listNotifications, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listNotifications = await NOTIFICATION_COLL.populate(listNotifications, `${ref}.${field}`);
                        }
                    }
                }

                if (!listNotifications) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách notification',
                        status: 400
                    });
                }

                let totalRecord = await NOTIFICATION_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listNotifications,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Import excel thông báo
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     * }}
     */
    importNotification(fileInfo) {
        return new Promise(async resolve => {
            try {
                XlsxPopulate.fromFileAsync(fileInfo.path)
                    .then(async workbook => {
                        let index = 2;
                        let listNotification = [];
                        let listReceiverNotification = [];

                        for (;true;) {
                            let code            = workbook.sheet(0).cell(`A${index}`).value();
                            let title           = workbook.sheet(0).cell(`B${index}`).value();
                            let contentOne      = workbook.sheet(0).cell(`C${index}`).value();
                            let contentTwo      = workbook.sheet(0).cell(`D${index}`).value();
                            let order           = workbook.sheet(0).cell(`E${index}`).value();
                            let startDate       = workbook.sheet(0).cell(`F${index}`).value();
                            let endDate         = workbook.sheet(0).cell(`G${index}`).value();
                            let status          = workbook.sheet(0).cell(`H${index}`).value();

                            if(!code || !title || !contentOne || !startDate || !endDate) break;

                            code    = code.trim();
                            title   = title.trim();

                            const infoEmployee = await EMPLOYEE_COLL.findOne({ code }).select('_id').lean();

                            if(infoEmployee){
                                startDate = moment(timeUtils.setFormatDDMMYYYYtoMMDDYYYY(startDate)).format();
                                endDate = moment(timeUtils.setFormatDDMMYYYYtoMMDDYYYY(endDate)).format();

                                listNotification[listNotification.length] = {
                                    code,
                                    title,
                                    contentOne,
                                    contentTwo,
                                    order,
                                    status: +status === 0 ? 2 : 1,
                                    employeeID: infoEmployee._id,
                                    startDate,
                                    endDate,
                                    createAt: timeUtils.getCurrentTime(),
                                    modifyAt: timeUtils.getCurrentTime()
                                }
                            }

                            index++;
                        }

                        await fs.unlinkSync(fileInfo.path);

                        let groupByCode = lodash.chain(listNotification)
                            .groupBy("code")
                            .map((value, key) => ({ code: key, notification: value }))
                            .value()

                        for (const codeGrouped of groupByCode) {
                            let groupByTitle = lodash.chain(codeGrouped.notification)
                                .groupBy("title")
                                .map((value) => ({
                                    info: value[0],
                                    content: value.map(v => `${v.contentOne} ${v.contentTwo}`).join(`\n`)
                                }))
                                .value()

                            for (const notification of groupByTitle) {
                                const { 
                                    title, order, status, employeeID, startDate, endDate, createAt, modifyAt
                                } = notification.info;

                                listReceiverNotification[listReceiverNotification.length] = {
                                    name: title,
                                    content: notification.content,
                                    receivers: [employeeID],
                                    order,
                                    status,
                                    from: startDate,
                                    to: endDate,
                                    state: 1,
                                    createAt,
                                    modifyAt
                                }
                            }
                        }

                        if(listReceiverNotification.length){
                            const { errors } = await PromisePool
                                .for(listReceiverNotification)
                                .withConcurrency(2)
                                .process(async notification => {
                                    const infoAfterInsertNotification = await NOTIFICATION_COLL.create(notification);

                                    await RECEIVER_NOTIFICATION_COLL.create({
                                        notification: infoAfterInsertNotification._id,
                                        receiver: notification.receivers[0],
                                        status: 1,
                                        state: 1,
                                        order: notification.order,
                                        createAt: notification.createAt,
                                        modifyAt: notification.modifyAt
                                    })

                                    return;
                                })

                            if(errors.length){
                                return resolve({ error: true, message: 'Import thông báo thất bại', data: errors });
                            }
                        } else{
                            return resolve({ error: true, message: 'Import thông báo thất bại', data: listReceiverNotification });
                        }

                        return resolve({ error: false, message: 'Import thông báo thành công' });
                    });

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

}

exports.MODEL = new Model;