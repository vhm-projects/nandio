"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const beautifyer = require('js-beautify').js_beautify;
const fs = require('fs');
const moment = require('moment');
const logger = require('../../../config/logger/winston.config');
const chalk = require('chalk');
const log = console.log;
/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_RECEIVER_NOTIFICATION_TYPE,
} = require('../constants/receiver_notification');
const {
    CF_ROUTINGS_RECEIVER_NOTIFICATION
} = require('../constants/receiver_notification/receiver_notification.uri');

/**
 * MODELS
 */
const RECEIVER_NOTIFICATION_MODEL = require('../models/receiver_notification').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const {
    EMPLOYEE_COLL
} = require('../../employee');

const NOTIFICATION_COLL = require('../databases/notification-coll');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ RECEIVER_NOTIFICATION  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Receiver_notification (API, VIEW)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.ADD_RECEIVER_NOTIFICATION]: {
                config: {
                    scopes: ['create:receiver_notification'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Receiver_notification',
                    code: CF_ROUTINGS_RECEIVER_NOTIFICATION.ADD_RECEIVER_NOTIFICATION,
                    inc: path.resolve(__dirname, '../views/receiver_notification/add_receiver_notification.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listEmployees = await EMPLOYEE_COLL.find({
                            state: 1,
                            status: 1
                        }).lean()

                        let listNotifications = await NOTIFICATION_COLL.find({
                                state: 1,
                                status: 1
                            }).lean()
                            .sort({
                                order: 1,
                                modifyAt: -1
                            })
                        ChildRouter.renderToView(req, res, {
                            listEmployees,
                            listNotifications,
                            CF_ROUTINGS_RECEIVER_NOTIFICATION
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            receiver,
                            notification,
                            order,
                        } = req.body;


                        let infoAfterInsert = await RECEIVER_NOTIFICATION_MODEL.insert({
                            receiver,
                            notification,
                            order,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Receiver_notification By Id (API, VIEW)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.UPDATE_RECEIVER_NOTIFICATION_BY_ID]: {
                config: {
                    scopes: ['update:receiver_notification'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Receiver_notification',
                    code: CF_ROUTINGS_RECEIVER_NOTIFICATION.UPDATE_RECEIVER_NOTIFICATION_BY_ID,
                    inc: path.resolve(__dirname, '../views/receiver_notification/update_receiver_notification.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            receiver_notificationID
                        } = req.query;

                        let infoReceiver_notification = await RECEIVER_NOTIFICATION_MODEL.getInfoById(receiver_notificationID);
                        if (infoReceiver_notification.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let listEmployees = await EMPLOYEE_COLL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean()

                        let listNotifications = await NOTIFICATION_COLL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean()


                        ChildRouter.renderToView(req, res, {
                            infoReceiver_notification: infoReceiver_notification.data || {},

                            listEmployees,
                            listNotifications,
                            CF_ROUTINGS_RECEIVER_NOTIFICATION
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            receiver_notificationID,
                            receiver,
                            notification,
                            status,
                            order,
                        } = req.body;


                        const infoAfterUpdate = await RECEIVER_NOTIFICATION_MODEL.update({
                            receiver_notificationID,
                            receiver,
                            notification,
                            status,
                            order,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Receiver_notification By Id (API)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.UPDATE_RECEIVER_NOTIFICATION_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:receiver_notification'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            receiver_notificationID,
                            receiver,
                            notification,
                            status,
                            order,
                        } = req.body;


                        const infoAfterUpdate = await RECEIVER_NOTIFICATION_MODEL.updateNotRequire({
                            receiver_notificationID,
                            receiver,
                            notification,
                            status,
                            order,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Receiver_notification By Id (API)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.DELETE_RECEIVER_NOTIFICATION_BY_ID]: {
                config: {
                    scopes: ['delete:receiver_notification'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            receiver_notificationID
                        } = req.params;

                        const infoAfterDelete = await RECEIVER_NOTIFICATION_MODEL.deleteById(receiver_notificationID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Receiver_notification By List Id (API)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.DELETE_RECEIVER_NOTIFICATION_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:receiver_notification'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            receiver_notificationID
                        } = req.body;

                        const infoAfterDelete = await RECEIVER_NOTIFICATION_MODEL.deleteByListId(receiver_notificationID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Receiver_notification By Id (API)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.GET_INFO_RECEIVER_NOTIFICATION_BY_ID]: {
                config: {
                    scopes: ['read:info_receiver_notification'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            receiver_notificationID
                        } = req.params;

                        const infoReceiver_notificationById = await RECEIVER_NOTIFICATION_MODEL.getInfoById(receiver_notificationID);
                        res.json(infoReceiver_notificationById);
                    }]
                },
            },

            /**
             * Function: Get List Receiver_notification (API, VIEW)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.GET_LIST_RECEIVER_NOTIFICATION]: {
                config: {
                    scopes: ['read:list_receiver_notification'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Receiver_notification',
                    code: CF_ROUTINGS_RECEIVER_NOTIFICATION.GET_LIST_RECEIVER_NOTIFICATION,
                    inc: path.resolve(__dirname, '../views/receiver_notification/list_receiver_notifications.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            status,
                            typeGetList
                        } = req.query;

                        let listReceiver_notifications = [];
                        if (typeGetList === 'FILTER') {
                            listReceiver_notifications = await RECEIVER_NOTIFICATION_MODEL.getListByFilter({
                                status,
                            });
                        } else {
                            listReceiver_notifications = await RECEIVER_NOTIFICATION_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listReceiver_notifications: listReceiver_notifications.data || [],
                            STATUS_RECEIVER_NOTIFICATION_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Receiver_notification By Field (API, VIEW)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.GET_LIST_RECEIVER_NOTIFICATION_BY_FIELD]: {
                config: {
                    scopes: ['read:list_receiver_notification'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Receiver_notification by field isStatus',
                    code: CF_ROUTINGS_RECEIVER_NOTIFICATION.GET_LIST_RECEIVER_NOTIFICATION_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/receiver_notification/list_receiver_notifications.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            status,
                            type
                        } = req.query;

                        let listReceiver_notifications = await RECEIVER_NOTIFICATION_MODEL.getListByFilter({
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listReceiver_notifications: listReceiver_notifications.data || [],
                            STATUS_RECEIVER_NOTIFICATION_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Receiver_notification Server Side (API)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.GET_LIST_RECEIVER_NOTIFICATION_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_receiver_notification'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listReceiver_notificationServerSide = await RECEIVER_NOTIFICATION_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listReceiver_notificationServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Receiver_notification Excel Server Side (API)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.GET_LIST_RECEIVER_NOTIFICATION_EXCEL]: {
                config: {
                    scopes: ['read:list_receiver_notification'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            filter,
                            condition,
                            nameOfParentColl,
                            objFilterStatic,
                            order,
                            keyword
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let conditionObj = RECEIVER_NOTIFICATION_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword)
                        let listReceiver_notification = await RECEIVER_NOTIFICATION_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice
                        })

                        res.json(listReceiver_notification)
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.API_GET_INFO_RECEIVER_NOTIFICATION]: {
                config: {
                    scopes: ['read:info_receiver_notification'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            receiver_notificationID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoReceiver_notification = await RECEIVER_NOTIFICATION_MODEL.getInfoReceiver_notification({
                            receiver_notificationID,
                            select,
                            explain
                        });
                        res.json(infoReceiver_notification);
                    }]
                },
            },
                
            /**
             * Function: Danh sách
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.API_GET_LIST_RECEIVER_NOTIFICATIONS]: {
                config: {
                    scopes: ['read:list_receiver_notification'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            filter,
                            explain,
                            sort,
                            limit,
                            page
                        } = req.query;

                        const listReceiver_notifications = await RECEIVER_NOTIFICATION_MODEL.getListReceiver_notifications({
                            filter,
                            explain,
                            sort,
                            limit,
                            page
                        });
                        res.json(listReceiver_notifications);
                    }]
                },
            },

            /**
             * Function: Cập nhật status
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_RECEIVER_NOTIFICATION.API_UPDATE_RECEIVER_NOTIFICATION]: {
                config: {
                    scopes: ['update:receiver_notification'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            receiver_notificationID
                        } = req.params;
                        let {
                            status,
                        } = req.body;


                        let infoAfterUpdate = await RECEIVER_NOTIFICATION_MODEL.updateReceiver_notification({
                            receiver_notificationID,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

        }
    }
};