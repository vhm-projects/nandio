"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const { uploadSingle } = require('../../../config/cf_helpers_multer');
const {
    STATUS_NOTIFICATION_TYPE,
} = require('../constants/notification');
const {
    CF_ROUTINGS_NOTIFICATION
} = require('../constants/notification/notification.uri');

/**
 * MODELS
 */
const NOTIFICATION_MODEL = require('../models/notification').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */
const {
    EMPLOYEE_COLL
} = require('../../employee');

const RECEIVER_NOTIFICATION_COLL = require('../databases/receiver_notification-coll');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ NOTIFICATION  ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: Get List employee table sub (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            '/notification/list-employee-table-sub': {
                config: {
                    scopes: ['read:list_notification'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            idsSelected,
                            keyword,
                            filter,
                            condition,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listEmployeeServerSide = await NOTIFICATION_MODEL.getListEmployeeServerSideTableSub({
                            idsSelected,
                            keyword,
                            filter,
                            condition,
                            page,
                            limit: length,
                        });
                        res.json(listEmployeeServerSide);
                    }]
                },
            },


            /**
             * Function: Insert Notification (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_NOTIFICATION.ADD_NOTIFICATION]: {
                config: {
                    scopes: ['create:notification'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Notification',
                    code: CF_ROUTINGS_NOTIFICATION.ADD_NOTIFICATION,
                    inc: path.resolve(__dirname, '../views/notification/add_notification.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let listEmployees = await EMPLOYEE_COLL.find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                order: 1,
                                modifyAt: -1
                            }).lean()

                        ChildRouter.renderToView(req, res, {
                            listEmployees,
                            CF_ROUTINGS_NOTIFICATION
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            name,
                            description,
                            content,
                            receivers,
                            order,
                            status,
                            from,
                            to,
                        } = req.body;

                        let infoAfterInsert = await NOTIFICATION_MODEL.insert({
                            name,
                            description,
                            content,
                            receivers,
                            order,
                            status,
                            from,
                            to,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Notification By Id (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_NOTIFICATION.UPDATE_NOTIFICATION_BY_ID]: {
                config: {
                    scopes: ['update:notification'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Notification',
                    code: CF_ROUTINGS_NOTIFICATION.UPDATE_NOTIFICATION_BY_ID,
                    inc: path.resolve(__dirname, '../views/notification/update_notification.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            notificationID
                        } = req.query;

                        let infoNotification = await NOTIFICATION_MODEL.getInfoById(notificationID);
                        if (infoNotification.error) {
                            return res.redirect('/something-went-wrong');
                        }

                        let listEmployees = await EMPLOYEE_COLL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean()

                        let listReceiver_notification = await RECEIVER_NOTIFICATION_COLL.find({
                            notification: infoNotification.data._id
                        }).lean();

                        ChildRouter.renderToView(req, res, {
                            infoNotification: infoNotification.data || {},
                            listReceiver_notification,
                            listEmployees,
                            CF_ROUTINGS_NOTIFICATION
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            notificationID,
                            name,
                            description,
                            content,
                            receivers,
                            status,
                            from,
                            to,
                            order,
                        } = req.body;

                        const infoAfterUpdate = await NOTIFICATION_MODEL.update({
                            notificationID,
                            name,
                            description,
                            content,
                            receivers,
                            status,
                            from,
                            to,
                            order,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Notification By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_NOTIFICATION.UPDATE_NOTIFICATION_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:notification'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            notificationID,
                            name,
                            description,
                            content,
                            receivers,
                            status,
                            from,
                            to,
                            order,
                        } = req.body;


                        const infoAfterUpdate = await NOTIFICATION_MODEL.updateNotRequire({
                            notificationID,
                            name,
                            description,
                            content,
                            receivers,
                            status,
                            from,
                            to,
                            order,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Notification By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_NOTIFICATION.DELETE_NOTIFICATION_BY_ID]: {
                config: {
                    scopes: ['delete:notification'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            notificationID
                        } = req.params;

                        const infoAfterDelete = await NOTIFICATION_MODEL.deleteById(notificationID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Notification By List Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_NOTIFICATION.DELETE_NOTIFICATION_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:notification'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            notificationID
                        } = req.body;

                        const infoAfterDelete = await NOTIFICATION_MODEL.deleteByListId(notificationID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Notification By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_NOTIFICATION.GET_INFO_NOTIFICATION_BY_ID]: {
                config: {
                    scopes: ['read:info_notification'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            notificationID
                        } = req.params;

                        const infoNotificationById = await NOTIFICATION_MODEL.getInfoById(notificationID);
                        res.json(infoNotificationById);
                    }]
                },
            },

            /**
             * Function: Get List Notification (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_NOTIFICATION.GET_LIST_NOTIFICATION]: {
                config: {
                    scopes: ['read:list_notification'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Notification',
                    code: CF_ROUTINGS_NOTIFICATION.GET_LIST_NOTIFICATION,
                    inc: path.resolve(__dirname, '../views/notification/list_notifications.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            status,
                            fromDateRange,
                            toDateRange,
                            typeGetList
                        } = req.query;

                        let listNotifications = [];
                        if (typeGetList === 'FILTER') {
                            listNotifications = await NOTIFICATION_MODEL.getListByFilter({
                                keyword,
                                status,
                                fromDateRange,
                                toDateRange,
                            });
                        } else {
                            listNotifications = await NOTIFICATION_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listNotifications: listNotifications.data || [],
                            STATUS_NOTIFICATION_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Notification By Field (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_NOTIFICATION.GET_LIST_NOTIFICATION_BY_FIELD]: {
                config: {
                    scopes: ['read:list_notification'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Notification by field isStatus',
                    code: CF_ROUTINGS_NOTIFICATION.GET_LIST_NOTIFICATION_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/notification/list_notifications.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            status,
                            fromDateRange,
                            toDateRange,
                            type
                        } = req.query;

                        let listNotifications = await NOTIFICATION_MODEL.getListByFilter({
                            keyword,
                            status,
                            fromDateRange,
                            toDateRange,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listNotifications: listNotifications.data || [],
                            STATUS_NOTIFICATION_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Notification Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_NOTIFICATION.GET_LIST_NOTIFICATION_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_notification'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listNotificationServerSide = await NOTIFICATION_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listNotificationServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Notification Excel Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_NOTIFICATION.GET_LIST_NOTIFICATION_EXCEL]: {
                config: {
                    scopes: ['read:list_notification'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            filter,
                            condition,
                            nameOfParentColl,
                            objFilterStatic,
                            order,
                            keyword
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let conditionObj = NOTIFICATION_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword)
                        let listNotification = await NOTIFICATION_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice
                        })

                        res.json(listNotification)
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_NOTIFICATION.API_GET_INFO_NOTIFICATION]: {
                config: {
                    scopes: ['read:info_notification'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            notificationID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoNotification = await NOTIFICATION_MODEL.getInfoNotification({
                            notificationID,
                            select,
                            explain
                        });
                        res.json(infoNotification);
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_NOTIFICATION.API_GET_LIST_NOTIFICATIONS]: {
                config: {
                    scopes: ['read:list_notification'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listNotifications = await NOTIFICATION_MODEL.getListNotifications({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listNotifications);
                    }]
                },
            },

            /**
             * Function: Import excel notification (API)
             * Date: 17/11/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_NOTIFICATION.API_IMPORT_NOTIFICATION]: {
                config: {
                    scopes: ['create:notification'],
                    type: 'json',
                },
                methods: {
                    post: [ uploadSingle, async function(req, res) {
                        const infoAfterImport = await NOTIFICATION_MODEL.importNotification(req.file);
                        res.json(infoAfterImport);
                    }]
                },
            },

        }
    }
};