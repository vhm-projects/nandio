"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('store_receiver_notification', {

    /**
     * Nhân viên
     */
    receiver: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },
    /**
     * Thông báo
     */
    notification: {
        type: Schema.Types.ObjectId,
        ref: 'store_notification',
    },
    /**
     * Trạng thái 
     * 1: Chưa xem,
     * 2: Đã xem
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
    /**
     * Thứ tự(Lấy từ noti cha)
     */
    order: {
        type: Number,
    },
});