"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('store_notification', {
    
    /**
     * Tiêu đề
     */
    name: {
        type: String,
        required: true,
    },
    /**
     * Mô tả
     */
    description: {
        type: String,
    },
    /**
     * Nội dung
     */
    content: {
        type: String,
    },
    /**
     * Danh sách người nhận
     */
    receivers: [{
        type: Schema.Types.ObjectId,
        ref: 'employee',
    }],
    /**
     * Trang thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
    /**
     * Ngày bắt đầu
     */
    from: {
        type: Date,
        required: true,
    },
    /**
     * Ngày kết thúc
     */
    to: {
        type: Date,
        required: true,
    },
    /**
     * Thứ tự
     */
    order: {
        type: Number,
        default: 1,
    },
});