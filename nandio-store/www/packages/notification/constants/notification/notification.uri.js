const BASE_ROUTE = '/notification';
const API_BASE_ROUTE = '/api/notification';

const CF_ROUTINGS_NOTIFICATION = {
    ADD_NOTIFICATION: `${BASE_ROUTE}/add-notification`,
    UPDATE_NOTIFICATION_BY_ID: `${BASE_ROUTE}/update-notification-by-id`,
    DELETE_NOTIFICATION_BY_ID: `${BASE_ROUTE}/delete/:notificationID`,

    GET_INFO_NOTIFICATION_BY_ID: `${BASE_ROUTE}/info/:notificationID`,
    GET_LIST_NOTIFICATION: `${BASE_ROUTE}/list-notification`,
    GET_LIST_NOTIFICATION_BY_FIELD: `${BASE_ROUTE}/list-notification/:field/:value`,
    GET_LIST_NOTIFICATION_SERVER_SIDE: `${BASE_ROUTE}/list-notification-server-side`,

    UPDATE_NOTIFICATION_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-notification-by-id-v2`,
    DELETE_NOTIFICATION_BY_LIST_ID: `${BASE_ROUTE}/delete-notification-by-list-id`,
    GET_LIST_NOTIFICATION_EXCEL: `${BASE_ROUTE}/list-notification-excel`,

    API_GET_INFO_NOTIFICATION: `${API_BASE_ROUTE}/info-notification/:notificationID`,
    API_GET_LIST_NOTIFICATIONS: `${API_BASE_ROUTE}/list-notifications`,
    API_IMPORT_NOTIFICATION: `${API_BASE_ROUTE}/import-notification`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_NOTIFICATION = CF_ROUTINGS_NOTIFICATION;