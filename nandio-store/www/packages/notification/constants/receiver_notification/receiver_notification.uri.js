const BASE_ROUTE = '/receiver_notification';
const API_BASE_ROUTE = '/api/receiver_notification';

const CF_ROUTINGS_RECEIVER_NOTIFICATION = {
    ADD_RECEIVER_NOTIFICATION: `${BASE_ROUTE}/add-receiver_notification`,
    UPDATE_RECEIVER_NOTIFICATION_BY_ID: `${BASE_ROUTE}/update-receiver_notification-by-id`,
    DELETE_RECEIVER_NOTIFICATION_BY_ID: `${BASE_ROUTE}/delete/:receiver_notificationID`,

    GET_INFO_RECEIVER_NOTIFICATION_BY_ID: `${BASE_ROUTE}/info/:receiver_notificationID`,
    GET_LIST_RECEIVER_NOTIFICATION: `${BASE_ROUTE}/list-receiver_notification`,
    GET_LIST_RECEIVER_NOTIFICATION_BY_FIELD: `${BASE_ROUTE}/list-receiver_notification/:field/:value`,
    GET_LIST_RECEIVER_NOTIFICATION_SERVER_SIDE: `${BASE_ROUTE}/list-receiver_notification-server-side`,

    UPDATE_RECEIVER_NOTIFICATION_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-receiver_notification-by-id-v2`,
    DELETE_RECEIVER_NOTIFICATION_BY_LIST_ID: `${BASE_ROUTE}/delete-receiver_notification-by-list-id`,
    GET_LIST_RECEIVER_NOTIFICATION_EXCEL: `${BASE_ROUTE}/list-receiver_notification-excel`,

    API_GET_INFO_RECEIVER_NOTIFICATION: `${API_BASE_ROUTE}/info-receiver_notification/:receiver_notificationID`,
    API_GET_LIST_RECEIVER_NOTIFICATIONS: `${API_BASE_ROUTE}/list-receiver_notifications`,
    API_UPDATE_RECEIVER_NOTIFICATION: `${API_BASE_ROUTE}/update-receiver_notification/:receiver_notificationID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_RECEIVER_NOTIFICATION = CF_ROUTINGS_RECEIVER_NOTIFICATION;