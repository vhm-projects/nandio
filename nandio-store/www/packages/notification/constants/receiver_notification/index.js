/**
 * Trạng thái 
 * 1: Chưa xem
 * 2: Đã xem
 */
exports.STATUS_RECEIVER_NOTIFICATION_TYPE = {

    "1": {
        value: "Chưa xem",
        color: "#0b51b7"
    },

    "2": {
        value: "Đã xem",
        color: "#d63031"
    },

};