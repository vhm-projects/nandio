const NOTIFICATION_COLL = require('./databases/notification-coll');
const NOTIFICATION_MODEL = require('./models/notification').MODEL;
const NOTIFICATION_ROUTES = require('./apis/notification');

const RECEIVER_NOTIFICATION_COLL  = require('./databases/receiver_notification-coll');
const RECEIVER_NOTIFICATION_MODEL  = require('./models/receiver_notification').MODEL;
const RECEIVER_NOTIFICATION_ROUTES  = require('./apis/receiver_notification');
    
// MARK REQUIRE

module.exports = {
    NOTIFICATION_COLL,
    NOTIFICATION_MODEL,
    NOTIFICATION_ROUTES,

    RECEIVER_NOTIFICATION_COLL,
    RECEIVER_NOTIFICATION_MODEL,
    RECEIVER_NOTIFICATION_ROUTES,
    
    // MARK EXPORT
}