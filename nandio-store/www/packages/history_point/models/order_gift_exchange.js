// "use strict";

/**
 * EXTERNAL PACKAGE
 */

/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs } 			= require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
 var ORDER_GIFT_EXCHANGE_COLL  	= require('../databases/order_gift_exchange-coll');

class Model extends BaseModel {
    constructor() {
        super(ORDER_GIFT_EXCHANGE_COLL);
        this.TYPE_EXCHANGE_GIFT_STORE_NEAR = 1; // Đổi quà từ cửa hàng gần nhất
		this.TYPE_EXCHANGE_GIFT_FROM_HOME  = 2; // Nhận quà tại nhà
    }
	
	insert({ customer, rewards, type }) {
        return new Promise(async resolve => {
            try {
               
				if(!checkObjectIDs(customer))
                    return resolve({ error: true, message: 'params_invalid' });

				if (!rewards || !rewards.length) {
                    return resolve({ error: true, message: 'Sản phẩm không hợp lệ' });
				}

                let mark       = false;
                let message    = '';
                let listRewardInsertOrderGiftExchange = [];
                // let listProductCentralizeOrder        = [];
                let totalPoint = 0;

                for (let reward of rewards) {
                    let infoReward = await REWARD_COLL.findById(reward.rewardID);
                    if (!infoReward) {
                        mark    = true;
                        message = 'Quà tặng không hợp lệ';
                        return resolve({ error: true, message: 'Quà tặng không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(reward.quantities)) || Number(reward.quantities) < 0) {
                            mark    = true;
                            message = 'Số lượng quà tặng không hợp lệ';
                            return resolve({ error: true, message: 'Số lượng quà tặng không hợp lệ' });
                        }
                        totalPoint += infoReward.point * Number(reward.quantities);
                        listRewardInsertOrderGiftExchange = [
                            ...listRewardInsertOrderGiftExchange,
                            {
                                reward:     reward.rewardID,
                                quantities: reward.quantities,
                                point:      infoReward.point
                            }
                        ];

                        // listProductCentralizeOrder = [
                        //     ...listProductCentralizeOrder,
                        //     {
                        //         product:    infoReward.gift.item,
                        //         onModel:    'gift_additional',
                        //         quantities: reward.quantities,
                        //     }
                        // ];
                    }
                }
                console.log({
                    totalPoint,
                });

                if (mark) {
                    return resolve({ error: true, message });
                }

				if(![this.TYPE_EXCHANGE_GIFT_STORE_NEAR, this.TYPE_EXCHANGE_GIFT_FROM_HOME].includes(type)) 
                    return resolve({ error: true, message: 'Loại đổi quà không hợp lệ' });

                // const ADDRESS_ACTIVE = 1;
                // let infoAddress = await ADDRESS_COLL.findOne({ 
                //     customer: customer, isDefault: true, stage: ADDRESS_ACTIVE 
                // });

                let dataInsert = {
                    customer,
                    rewards: listRewardInsertOrderGiftExchange,
                    type,
                    totalPoint,
                    flatform: 'STORE'
                };

                let pointDeliveryPointConfig = 0;
                if (type == this.TYPE_EXCHANGE_GIFT_FROM_HOME) { 
                    // if (!infoAddress) {
                    //     return resolve({ error: true, message: 'Khách hàng chưa cập nhật địa chỉ' });
                    // }

                    // let infoPointGiftExchange = await DELIVERY_POINT_CONFIG_COLL.findOne({ // ĐIỂM CÓ ĐƯỢC MIỄN PHÍ VẬN CHUYỂN
                    //     point: {
                    //         $lte: totalPoint
                    //     }
                    // });
    
                    // if (infoPointGiftExchange) {
                    //     pointDeliveryPointConfig = infoPointGiftExchange.point;
                    //     dataInsert = {
                    //         ...dataInsert,
                    //         deliveryPointConfig: infoPointGiftExchange._id
                    //     }
                    // }
                    // totalPoint += pointDeliveryPointConfig; // TRỪ ĐIỂM MIỄN PHÍ VẬN CHUYỂN
                    // dataInsert = {
                    //     ...dataInsert,
                    //     pointDeliveryPointConfig,
                    //     totalPoint,
                    // }
                }
                console.log({
                    dataInsert
                });
                let infoAfterInsert = await this.insertData(dataInsert);
                if (!infoAfterInsert)
                    return resolve({ error: true, message: 'add_order_gift_exchange_failed' });

                // INSERT HISTORY POINT 
                // let infoHistoryPointAfterInsert = await HISTORY_POINT_MODEL.insert({ 
                //     customerID: customer, 
                //     type: HISTORY_POINT_MODEL.TYPE_EXCHANGE_POINT, 
                //     currentPoint: totalPoint 
                // });
                // if(infoHistoryPointAfterInsert.error)
                //     return resolve(infoHistoryPointAfterInsert);

                // // LẤY QUÀ TẠI NHÀ THÌ LƯU CENTRALIZE ORDER
                // if (type == this.TYPE_EXCHANGE_GIFT_FROM_HOME) {
                //     console.log("=================TYPE_EXCHANGE_GIFT_FROM_HOME===================");
                //     // // LƯU VÀO QUẢN LÝ TẬP TRUNG
                //     let { address, ward, district, city } = infoAddress;

                //     const cityInfo     = COMMON_MODEL.getInfoProvince({ provinceCode: city });
                //     const districtInfo = COMMON_MODEL.getInfoDistrict({ districtCode: district });
                //     const wardInfo     = await COMMON_MODEL.getInfoWard({ district: district, wardCode: ward });

                //     let cityText     = cityInfo.data && cityInfo.data[1].name_with_type;
                //     let districtText = districtInfo.data && districtInfo.data[1].name_with_type;
                //     let wardText     = wardInfo.data && wardInfo.data[1].name_with_type;

                //     const TYPE_GIFT = 3;
                //     let conditionObj__CentralizeOrder = {
                //         type: infoAfterInsert._id, 
                //         onModel: 'order_gift_exchange',
                //         customer: customer,
                //         onModelCustomer: 'customer',
                //         address, ward, district, city, cityText, wardText, districtText,
                //         products: listProductCentralizeOrder,
                //         kind: TYPE_GIFT
                //     }
                    
                //     let infoAfterInsertCentralizeOrder = await CENTRALIZE_ORDER_MODEL.insert(conditionObj__CentralizeOrder);
                //     if (infoAfterInsertCentralizeOrder.error) {
                //         return resolve(infoAfterInsertCentralizeOrder);
                //     }
                // }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoWith__Rewards({ customer, rewards }) {
        return new Promise(async resolve => {
            try {
				if(!checkObjectIDs(customer))
                    return resolve({ error: true, message: 'params_invalid' });

				// if(!checkObjectIDs(rewards))
                //     return resolve({ error: true, message: 'params_invalid' });

				if (!rewards || !rewards.length) {
                    return resolve({ error: true, message: 'Sản phẩm không hợp lệ' });
				}

                let mark       = false;
                let message    = '';
                let listReward = [];
                let totalPoint = 0;

                for (let reward of rewards) {
                    let infoReward = await REWARD_COLL.findById(reward.rewardID)
                        .select('gift point')
                        .populate({
                            path: 'gift.item',
                            select: 'name'
                        });
                    
                    if (!infoReward) {
                        mark    = true;
                        message = 'Quà tặng không hợp lệ';
                        return resolve({ error: true, message: 'Quà tặng không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(reward.quantities)) || Number(reward.quantities) < 0) {
                            mark    = true;
                            message = 'Số lượng quà tặng không hợp lệ';
                            return resolve({ error: true, message: 'Số lượng quà tặng không hợp lệ' });
                        }
                        totalPoint += infoReward.point * Number(reward.quantities);
                        listReward = [
                            ...listReward,
                            {
                                name:       infoReward.gift.item.name,
                                reward:     reward.rewardID,
                                quantities: reward.quantities,
                                point:      infoReward.point
                            }
                        ];
                    }
                }

                if (mark) {
                    return resolve({ error: true, message });
                }
                
                return resolve({ error: false, data: {
                    listReward,
                    totalPoint
                }});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getOrderGiftExchangesBy__Rewards({ customer, fromDate, toDate, page = 1, limit = 30 }) {
        return new Promise(async resolve => {
            try {
				page  = +page;
				limit = +limit;

				if(!checkObjectIDs(customer))
                    return resolve({ error: true, message: 'params_invalid' });

				if(!page || !limit)
					return resolve({ error: true, message: 'param_invalid' });

				let conditionObj = { customer: customer };

				if(fromDate && toDate){
					conditionObj.createAt = {
                        $gte: new Date(fromDate),
                        $lt: new Date(toDate)
                    }
				}
				console.log({
					conditionObj,
                    ____aaaa: "==============================="
				});

                let listOrderGiftExchange = await ORDER_GIFT_EXCHANGE_COLL.find(conditionObj)
                    .select("rewards totalPoint createAt")
                    .populate({
                        path: 'rewards.reward',
                        select: 'gift',
                        populate: {
                            path: 'gift.item',
                            select: 'name'
                        }
                    })
                    .sort({ createAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
                    .lean();

                if(!listOrderGiftExchange)
                    return resolve({ error: true, message: 'cannot_get_list_history_point' });


                let totalOrderGiftExchange = await ORDER_GIFT_EXCHANGE_COLL.count(conditionObj);
                let pages = Math.ceil(totalOrderGiftExchange/limit);
                
                return resolve({ 
                    error: false, 
                    data: {
                        listOrderGiftExchange,
                        currentPage: +page,
                        // perPage: limit,
                        totalPage: +pages
                    }
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

module.exports.MODEL = new Model;
// var HISTORY_POINT_MODEL         	= require('../../history_point/models/history_point').MODEL;
// var { MODEL: HISTORY_POINT_MODEL }			= require('../../history_point/models/history_point');
var REWARD_COLL               	= require('../../reward/databases/reward-coll');
// var DELIVERY_POINT_CONFIG_COLL 	= require('../databases/delivery_point_config-coll');
// var ADDRESS_COLL  				= require('../../address/databases/address-coll');

/**
 * MODELS
 */
// var CENTRALIZE_ORDER_MODEL        = require('../../order/models/centralize_order').MODEL;
// var COMMON_MODEL                  = require('../../common/models/common').MODEL;
// var GIAO_HANG_NHANH_MODEL         = require('../../giao_hang_nhanh/models/giao_hang_nhanh').MODEL;
// var AGENCY_MODEL   				= require('../../store/models/agency').MODEL;
