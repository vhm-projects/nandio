"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                      	= require('moment');
const ISOdate                      	= require('isodate');
const request                      	= require('request');
const NANDIO_SHOP_DOMAIN            = process.env.NANDIO_SHOP_DOMAIN || 'http://localhost:5002';
/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs } 			= require('../../../utils/utils');

/** 
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');

/**
 * MODELS
 */
// const EMPLOYEE_MODEL				= require('../../employee/models/employee').MODEL;

/**
 * COLLECTIONS
 */
const HISTORY_POINT_COLL  			= require('../databases/history_point-coll');
const { CUSTOMER_COLL } 			= require('../../customer');
const REWARD_COLL  					= require('../../reward/databases/reward-coll');
const EXCHANGE_GIFT_COLL  			= require('../../history/databases/history_exchange_gift-coll');
const INVENTORY_BEGIN_MONTH_COLL    = require('../../inventory_begin_month/databases/inventory_begin_month-coll');
const EMPLOYEE_COLL  				= require('../../employee/databases/employee-coll');
const SYSTEM_CONFIG_COLL  			= require('../../config/databases/system_config-coll');
const LIMIT_GIFT_CONFIG_COLL		= require('../../config/databases/limit_gift_config-coll');
const KPI_SALEIN_ACTUALLY_COLL		= require('../../kpi/databases/kpi_salein_actually-coll');
const HISTORY_EXCHANGE_GIFT_COLL	= require('../../history/databases/history_exchange_gift-coll');
const HISTORY_EXCHANGE_SAMPLING_COLL	= require('../../history/databases/history_exchange_sampling-coll');


class Model extends BaseModel {
    constructor() {
        super(HISTORY_POINT_COLL);
		
		/**
		 * Loại lịch sử:
		 * 1: Tích điểm
		 * 2: Đổi điểm
		 * 3: Đăng ký tài khoản Nhận điểm
		 * 4: Nhận điểm chuyển tới
		 * 5: Chuyển điểm
		 * 6: Member get member (mã giới thiệu)
		 * 7: GLOQ nạp điểm
		 */
		this.TYPE_ACCUMULATE_POINT 			= 1;
		this.TYPE_EXCHANGE_POINT   			= 2;
		this.TYPE_REGISTER_RECEIVE_POINT	= 3; 
		this.TYPE_RECEIVE_POINT 			= 4;
		this.TYPE_TRANSFER_POINT 			= 5;
		this.TYPE_MEMBER_GET_MEMBER 		= 6;
		this.TYPE_GLOQ_ADD_POINT     		= 7;

		this.ARR_TYPE_VALID 			 = [1,2,3,4,5,6,7];
		this.ARR_TYPE_RECEIVE_POINT 	 = [1,3,4,6,7];
		this.ARR_TYPE_TRANSFER_POINT 	 = [2,5];
		this.ARR_TYPE_MINUS_POINT_NANDIO = [1,2,3,6];
		this.ARR_TYPE_MINUS_POINT        = [1,2,3,5,6]; //TYPE SẼ TRỪ ĐIỂM SENDER

		// DEFINE CÁC THAM SỐ ĐỒNG BỘ AMS
		this.TYPE_SYNC_AMS_ACCUMULATE_POINT = 1;
		this.TYPE_SYNC_AMS_EXCHANGE_POINT   = 2;
    }

	insert({ productID, customerID, transactionID, type, rewardID, buyType, amount, employeeID }) {
        return new Promise(async resolve => {
            try {
				console.log({ productID, customerID, transactionID, type, rewardID, buyType, amount, employeeID })
				if(employeeID && !checkObjectIDs(employeeID))
					return resolve({ error: true, message: 'ID nhân viên không hợp lệ', status: 400 });

				if(!checkObjectIDs(customerID))
                    return resolve({ error: true, message: 'ID khách hàng không hợp lệ', status: 400 });

                if(!this.ARR_TYPE_VALID.includes(type)) 
                    return resolve({ error: true, message: 'Loại lịch sử đổi điểm không hợp lệ', status: 400 });

				let dataInsert = {
					customer: customerID,
					flatform: 'STORE',
					type
				};

				if(productID){
					dataInsert.product = productID;
				}

				let point = 0;
				let dayStart = moment(new Date()).startOf('day').format();
				let dayEnd = moment(new Date()).endOf('day').format();

				const systemConfig = await SYSTEM_CONFIG_COLL.findOne({}).lean();
				if(systemConfig){
					const limitExchangeGift = systemConfig.limitExchangeGift;
					const totalExchangeGift = await EXCHANGE_GIFT_COLL.countDocuments({
						createAt: {
							$gte: ISOdate(dayStart),
							$lte: ISOdate(dayEnd),
						}
					})

					if(totalExchangeGift >= limitExchangeGift){
						return resolve({ error: true, message: 'Hết lượt đổi quà trong ngày', status: 400 });
					}
				}

				if(rewardID){
					// dataInsert.reward = rewardID;

					let infoReward = await REWARD_COLL.findById(rewardID);
					if(infoReward){
						point = infoReward.point * +amount;

						dataInsert.point = point;
					}
				}

				if(amount){
					dataInsert.amount = +amount;
				}

				if(transactionID){
					dataInsert.transaction = transactionID;
					let { order } = await TRANSACTION_COLL.findById(transactionID);
					let infoOrder = await ORDER_COLL.findById(order);
					let { orderLine } = infoOrder;
					let listOrderLine = await ORDER_LINE_COLL.find({ _id: { $in: orderLine }}).populate("product").lean()
					listOrderLine.forEach(oderLine => {
						point += oderLine.product.point;
					});
				}

				if(point <= 0)
					return resolve({ error: true, message: 'point_invalid' });

				if(buyType && [1, 2, 3].includes(buyType)){
					dataInsert.buyType = buyType;
				}

				let infoCustomer = await CUSTOMER_COLL.findById(customerID);
				if(!infoCustomer)
					return resolve({ error: true, message: 'Khách hàng không tồn tại' });

				if(infoCustomer.point < point)
					return resolve({ error: true, message: 'Số điểm không đủ để đổi' });

				// Điểm của user trước khi thao tác	
				dataInsert.beforPoint   = infoCustomer.point;
				dataInsert.currentPoint = point;
				dataInsert.point        = infoCustomer.point + Number(point);


				const infoEmployeeAndCheckIn = await EMPLOYEE_MODEL.getInfo__CheckIn_Vs_Employee__ByEmployeeID({
					employeeID
				})

				if(infoEmployeeAndCheckIn.error)
					return resolve({ error: true, message: 'Bạn chưa checkin vui lòng checkin' });

				// Check kho
				// if(employeeID && productID){
				// 	const checkInventory = await this.checkInventory({
				// 		employeeID, productID
				// 	})

				// 	if(checkInventory.error)
				// 		return resolve(checkInventory);
				// }

				let typeNandioAdmin = this.TYPE_MEMBER_GET_MEMBER;
				// if (this.ARR_TYPE_MINUS_POINT.includes(Number(typeSender))) {
				// 	typeNandioAdmin = this.TYPE_TRANSFER_POINT ;
				// }

				// let infoHistoryPointAfterInsert = await this.createHistoryPoint__NandioShop({ 
				// 	customerID, 
				// 	typeNandioAdmin, 
				// 	type, 
				// 	currentPoint: dataInsert.point 
				// });

				// if(infoHistoryPointAfterInsert.error)
                //     return resolve(infoHistoryPointAfterInsert);

                // let infoAfterInsert = await this.insertData(dataInsert);
                // if(!infoAfterInsert)
                //     return resolve({ error: true, message: 'add_history_point_failed' });

				let infoHistoryPointAfterInsert;
				// Tích điểm
				if(type == 1){
					
					// await CUSTOMER_COLL.updateOne({ _id: customerID }, {
					// 	$inc: { point, pointRanking: point }
					// });
					// console.log({ point, pointUserCurrent });
					// // Đồng bộ điểm sang DMS
					// var options = {
					// 	'method': 'POST',
					// 	'url': `http://dms1.yensaothienviet.vn:8680/thsams/web/index.php?r=api%2Fcapnhatdiemkh&p1=crm&p2=123456&p3=4PBagn81jWU3ouPVt4OeqzDeYrU7iYH9&dtdd=0937362026&diemtichluy=${point}&diemdoiqua=1&tran_type=1&diemhientai=${pointUserCurrent}&id_order=1`,
					// 	'headers': {
					// 	  'Cookie': 'PHPSESSID=4kot1ts242d9eouslr4poc3863'
					// 	}
					//   };
					//   request(options, function (error, response) {
					// 	if (error) throw new Error(error);
					// 		console.log(response.body);
					//   });

				}
				console.log({ type })

				// Đổi điểm
				if(type === 2){
					infoHistoryPointAfterInsert = await this.createHistoryPoint__NandioShop({ 
						customerID:      customerID, 
						typeNandioAdmin: this.TYPE_RECEIVE_POINT, 
						type:            this.TYPE_EXCHANGE_POINT, 
						currentPoint:    point,
					})
					console.log({ infoHistoryPointAfterInsert })

					if (infoHistoryPointAfterInsert.error) {
						return resolve(infoHistoryPointAfterInsert);
					}

					const { infoCheckInOut, infoEmployee } = infoEmployeeAndCheckIn.data;
					const { _id: checkinID, store } = infoCheckInOut;
					const { region, area, distributor } = infoEmployee;
					
					let beforePoint = infoCustomer.point ? infoCustomer.point : 0;
					let afterPoint  = infoCustomer.point ? infoCustomer.point - Number(point) : 0;

					let dataInsertHistoryExchangeGift = {
						region, area, distributor, store, 
						employee: employeeID,
						customer: customerID,
						reward: rewardID,
						checkin: checkinID,
						beforePoint,
						point,
						afterPoint,
						amount,
						createAt: ISOdate(new Date()),
						modyfyAt: ISOdate(new Date()),
					};

					if (rewardID) {
						let infoRewardForInsertExchangeGift = await REWARD_COLL.findById(rewardID)
							.select('gift point')
							.populate({
								path: 'gift.item',
								select: 'name'
							});

						dataInsertHistoryExchangeGift = {
							...dataInsertHistoryExchangeGift,
							sku: infoRewardForInsertExchangeGift.gift.item._id
						}
					}

					const infoAfterCreate = await EXCHANGE_GIFT_COLL.create(dataInsertHistoryExchangeGift);

					await ORDER_GIFT_EXCHANGE_MODEL.insert({ 
						customer: customerID, 
						rewards: [{
							rewardID: rewardID,
							quantities: Number(amount)
						}], 
						type: ORDER_GIFT_EXCHANGE_MODEL.TYPE_EXCHANGE_GIFT_STORE_NEAR
						// productID, customerID, transactionID, rewards, type, typeGiftExchange, amount
					});
					if (!infoHistoryPointAfterInsert) {
						return resolve({ error: true, message: 'Không thể tạo lịch sử điểm' });
					}
					// await CUSTOMER_COLL.updateOne({ _id: customerID }, {
					// 	$inc: { point: -point }
					// });
					return resolve(infoHistoryPointAfterInsert);
				} 
				
                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
				console.error(error);
                return resolve({ error: true, message: error.message });
            }
        })
    }

	/**
	 * KIỂM TRA TỒN KHO  (CẬP NHẬT NGÀY 10/03/2022)
	 * 	mô tả dựa vào file note 'www/packages/history_point/help/note'
	 * 	- Kiểm tra tồn kho của 1 PRODUCT_EMPLOYEE
	 * 		- limit_gift_config 
	 * 			(+)
	 * 		- kpi_salein_actually
	 * 			(+)
	 * 		- inventory_begin_month
	 * 			(-)
	 * 		- history_exchange_gift
	 * 			(-)
	 * 		- history_exchange_sampling
	 */
	checkInventory({ employeeID, productID }) {
		return new Promise(async resolve => {
			try {
				if(!checkObjectIDs(employeeID))
					return resolve({ error: true, message: 'ID nhân viên không hợp lệ' });

				if(!checkObjectIDs(productID))
					return resolve({ error: true, message: 'ID sản phẩm không hợp lệ' });

				const infoEmployee = await EMPLOYEE_COLL.findById(employeeID).populate('type_employee');
				if(!infoEmployee)
					return resolve({ error: true, message: 'Nhân viên không tồn tại' });

				const { distributor } = infoEmployee;
				const monthCurrent 	= moment().get('months') + 1;
				const yearCurrent 	= moment().get('year');

				const datetime = moment().set({ 'months': monthCurrent - 1, 'years': yearCurrent });

				/**
				 * 1/ kiểm tra tồn kho từ LIMIT_GIFT_CONFIG (1)
				 * ===============BEGIN kiểm tra inventory từ LIMIT_GIFT_CONFIG_COLL=========
				 */
				const infoInventoryFromLimitGiftConfig = await LIMIT_GIFT_CONFIG_COLL.aggregate([
					{
						$match: {
							//distributor: ObjectID(distributor),
							employee: ObjectID(employeeID),
							sku: ObjectID(productID),
							month: monthCurrent,
							year: yearCurrent
						}
					},
					{
						$group: {
							_id: null,
							totalInventory: { $sum: "$amount" }
						}
					}
				])
				/**
				 * ===============BEGIN kiểm tra inventory từ KPI_SALEIN_ACTUALLY=========
				 * 2/ kiểm tra tồn kho từ đơn DMS gửi qua KPI_SALEIN_ACTUALLY (2)
				 */
					/**
					 * IS_FREE_ITEM
					 *      1: LÀ HÀNG TẶNG
					 *      0: LÀ HÀNG BÁN
					 */
				const PRODUCT_FOR_BUY = 0;
				let conditionObj = {
					employee: ObjectID(employeeID),
					product: ObjectID(productID),

					/**
					 * 2 điều kiện IS_FREE_ITEM, store
					 *  -> đảm bảo rằng record trong kpi_salein_actually là record tính KPI (tồn tại store và is_free_item = 0)
					 */
					IS_FREE_ITEM: PRODUCT_FOR_BUY,
					store: {
						$exists: false
					},
					ORDER_DATE: {
						$gte: ISOdate(datetime.startOf('months').format()),
						$lte: ISOdate(datetime.endOf('months').format())
					}
				};
				const infoInventoryFromSaleinActually = await KPI_SALEIN_ACTUALLY_COLL.aggregate([
                    {
                        $match: conditionObj
                    },
                    {
                        $group: {
                            _id: null,
                            totalInventory: { $sum: "$QUANTITY" }
                        }
                    },
                ]);
				//* ===============END kiểm tra inventory từ KPI_SALEIN_ACTUALLY=========

				/**
				 * ===============BEGIN kiểm tra inventory từ INVENTORY_BEGIN_MONTH=========
				 * 3/ kiểm tra tồn kho từ tháng trước chuyển sang tháng hiện tại (3)
				 */
				const infoInventoryFromInventoryBeginMonth = await INVENTORY_BEGIN_MONTH_COLL.aggregate([
					{
						$match: { 
							employee: ObjectID(employeeID),
							sku: ObjectID(productID),
							month: monthCurrent, 
							year: yearCurrent
						}
					}
					,
					{
						$group: {
							_id: null,
							totalInventory: { $sum: "$amount" }
						}
					}
				]);

				/**
				 * ===============BEGIN kiểm tra inventory (cần trừ HISTORY_EXCHANGE_GIFT)=========
				 * 4/
				 */
				 const infoInventoryFromHistoryExchangeGift = await HISTORY_EXCHANGE_GIFT_COLL.aggregate([
					{
						$match: { 
							employee: ObjectID(employeeID),
							sku: ObjectID(productID),
							createAt: {
								$gte: ISOdate(datetime.startOf('months').format()),
								$lte: ISOdate(datetime.endOf('months').format())
							}
						}
					},
					{
						$group: {
							_id: null,
							totalInventory: { $sum: "$amount" }
						}
					}
				]);

				/**
				 * ===============BEGIN kiểm tra inventory (cần trừ HISTORY_EXCHANGE_SAMPLING)=========
				 * 5/
				 */
				const infoInventoryFromHistoryExchangeSampling = await HISTORY_EXCHANGE_SAMPLING_COLL.aggregate([
					{
						$match: { 
							employee: ObjectID(employeeID),
							product: ObjectID(productID),
							createAt: {
								$gte: ISOdate(datetime.startOf('months').format()),
								$lte: ISOdate(datetime.endOf('months').format())
							}
						}
					},
					{
						$group: {
							_id: null,
							totalInventory: { $sum: "$amount" }
						}
					}
				]);

				// totalInventoryCombine: tổng từ 2 nhóm (LIMIT_GIFT_CONFIG) + (KPI_SALEIN_ACTUALLY)
				let totalInventoryFromLimitGiftConfig 		= (infoInventoryFromLimitGiftConfig.length && infoInventoryFromLimitGiftConfig[0].totalInventory);
				let totalInventoryFromSaleinActually  		= (infoInventoryFromSaleinActually.length && infoInventoryFromSaleinActually[0].totalInventory);
				let totalInventoryFromInventoryBeginMonth 	= (infoInventoryFromInventoryBeginMonth.length && infoInventoryFromInventoryBeginMonth[0].totalInventory);
				let totalInventoryFromHistoryExchangeGift 	= (infoInventoryFromHistoryExchangeGift.length && infoInventoryFromHistoryExchangeGift[0].totalInventory);
				let totalInventoryFromHistoryExchangeSampling 	= (infoInventoryFromHistoryExchangeSampling.length && infoInventoryFromHistoryExchangeSampling[0].totalInventory);
				let totalInventoryCombine = 
					(totalInventoryFromLimitGiftConfig + totalInventoryFromSaleinActually + totalInventoryFromInventoryBeginMonth) 
						- 
					(totalInventoryFromHistoryExchangeGift+totalInventoryFromHistoryExchangeSampling);

				return resolve({
					error: false,
					data: {
						productID,
						// totalInventory
						totalInventory: totalInventoryCombine
					}
				});
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

	createHistoryPoint__NandioShop({ productID, customerID, typeNandioAdmin, type, currentPoint }) {
        return new Promise(async resolve => {
            try {
				let conditionObj = {
					'customerID': `${customerID}`,
					'typeNandioAdmin': typeNandioAdmin,
					'type': type,
					'currentPoint': currentPoint,
					'flatform': 'STORE',
				}
				if (productID) {
					conditionObj = {
						...conditionObj,
						'productID': `${productID}`,
					}
				}
				var options = {
					'method': 'POST',
					'url': `${NANDIO_SHOP_DOMAIN}/history/add-history-point-transfer`,
					'headers': {
						'Content-Type': 'application/x-www-form-urlencoded',
						'user-agent': 'PostmanRuntime/7.28.4',
						'postman_test_sandbox': 'ldk_postman',
					},
					form: conditionObj
				};
				request(options, function (error, response) {
					if (error) throw new Error(error);
					console.log(response && response.body);
					return resolve(JSON.parse(response.body));
				});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ historyPointID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(historyPointID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoHistoryPoint = await HISTORY_POINT_COLL
					.findById(historyPointID)
					.select("point reward createAt buyType transaction")
					.populate({
						path: "reward",
						select: "title"
					})
					.populate({
						path: "transaction",
						select: "transactionID"
					})
					.lean();

                if(!infoHistoryPoint)
                    return resolve({ error: true, message: 'cannot_get_history_point' });

                return resolve({ error: false, data: infoHistoryPoint });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
	
	getListByCustomer({ customerID, fromDate, toDate, type, page = 1, limit = 30 }){
		return new Promise(async resolve => {
			try {
				page  = +page;
				limit = +limit;

				if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });

				if(!page || !limit)
					return resolve({ error: true, message: 'param_invalid' });

				let conditionObj = { customer: customerID };
				if([1,2].includes(type)){
					conditionObj.type = type;
				} else{
					conditionObj.type = { $in: [1,2] };
				}

				if(fromDate && toDate){
					conditionObj.createAt = {
                        $gte: new Date(fromDate),
                        $lt: new Date(toDate)
                    }
				}
				let listHistoryPoint = await HISTORY_POINT_COLL
					.find(conditionObj)
					.select("reward transaction createAt point")
					.sort({ createAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
					.populate({
						path: "reward",
						select: "title"
					})
					.populate({
						path: "transaction",
						select: "transactionID"
					})
					.lean();

                if(!listHistoryPoint)
                    return resolve({ error: true, message: 'cannot_get_list_history_point' });

				let totalHistoryPoint = await HISTORY_POINT_COLL.count(conditionObj);
				let pages = Math.ceil(totalHistoryPoint/limit);

				return resolve({ 
					error: false,
					data: {
						listHistoryPoint,
						currentPage: +page,
						perPage: limit,
						totalPage: +pages
					}
				});
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

	getListWithPaging({ type, page = 1, limit = 30 }){
		return new Promise(async resolve => {
			try {
				page  = +page;
				limit = +limit;
				type  = +type;

				if(!page || !limit)
					return resolve({ error: true, message: 'param_invalid' });

				let conditionObj = {};
				if([1,2].includes(type)){
					conditionObj.type = type;
				} else{
					conditionObj.type = { $in: [1,2] };
				}

				let listHistoryPoint = await HISTORY_POINT_COLL
					.find(conditionObj)
					.populate('product customer transaction buyType')
					.sort({ createAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
					.lean();

                if(!listHistoryPoint)
                    return resolve({ error: true, message: 'cannot_get_list_history_point' });

				let totalHistoryPoint = await HISTORY_POINT_COLL.countDocuments(conditionObj);

				return resolve({ 
					error: false, 
					data: {
						listHistoryPoint,
						currentPage: +page,
						totalPage: +totalHistoryPoint
					}
				});
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

	getList({ type }){
		return new Promise(async resolve => {
			try {

				let conditionObj = {};
				if([1,2].includes(type)){
					conditionObj.type = type;
				} else{
					conditionObj.type = { $in: [1,2] };
				}

				let listHistoryPoint = await HISTORY_POINT_COLL
					.find(conditionObj)
					.populate('reward customer order buyType')
					.sort({ createAt: -1 })
					.lean();
                if(!listHistoryPoint)
                    return resolve({ error: true, message: 'cannot_get_list_history_point' });

				return resolve({ error: false, data: listHistoryPoint });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

	getListRewardOfYou({ customerID, page = 1, limit = 3 }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(customerID))
					return resolve({ error: true, message: 'params_invalid' });

				let listReward = await HISTORY_POINT_COLL
					.find({ customer: customerID, reward: { $exists: true }})  
					.select("reward createAt")                     
					.populate({
						path: "reward",
						select: "title image",
						populate: {
							path: "image",
							select: "path"
						}
					})
					.sort({ createAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
					.lean();

                if(!listReward)
                    return resolve({ error: true, message: 'cannot_get_list_history_point' });

				let totalHistoryPoint = await HISTORY_POINT_COLL.countDocuments({ customer: customerID, reward: { $exists: true }});

				return resolve({ 
					error: false, 
					data: {
						listReward,
						currentPage: +page,
						totalPage: +totalHistoryPoint
					}
				});
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}
}

exports.MODEL = new Model;

/**
 * MODELS
 */
var { MODEL: EMPLOYEE_MODEL }			= require('../../employee/models/employee');
var ORDER_GIFT_EXCHANGE_MODEL           = require('./order_gift_exchange').MODEL;
