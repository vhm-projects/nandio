const HISTORY_POINT_MODEL    = require('./models/history_point').MODEL;
const HISTORY_POINT_COLL     = require('./databases/history_point-coll');
const HISTORY_POINT_ROUTES   = require('./apis/history_point');

module.exports = {
    HISTORY_POINT_ROUTES,
    HISTORY_POINT_COLL,
    HISTORY_POINT_MODEL,
}
