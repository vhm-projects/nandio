"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path									= require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const { CF_ROUTINGS_HISTORY_POINT } 		= require('../constants/history_point.uri');

/**
 * MODELS
 */
const HISTORY_POINT_MODEL 					= require('../models/history_point').MODEL;

/**
 * COLLECTIONS
 */
const HISTORY_POINT_COLL 					= require('../databases/history_point-coll');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ====================== ****************************** ================================
             * ======================  QUẢN LÝ LỊCH SỬ ĐỔI ĐIỂM  ================================
             * ====================== ****************************** ================================
             */

            /**
             * Function: Thên lịch sử đổi điểm (API)
             * Date: 05/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.ADD_HISTORY_POINT]: {
                config: {
                    scopes: ['create:add_history_point'],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
                        let { _id: customerID } = req.customer;
                        const { productID, rewardID, type } = req.body;

                        const infoAfterInsert = await HISTORY_POINT_MODEL.insert({ 
							productID, customerID, rewardID, type
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
             * Function: Thông tin lịch sử đổi điểm (API)
             * Date: 05/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.INFO_HISTORY_POINT]: {
                config: {
                    scopes: ['read:get_info_history_point'],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { historyPointID } = req.query;
                        const infoHistoryPoint = await HISTORY_POINT_MODEL.getInfo({ historyPointID });
                        res.json(infoHistoryPoint);
                    }]
                },
            },

			/**
             * Function: Danh sách lịch sử đổi điểm của customer (API)
             * Date: 05/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.LIST_HISTORY_POINT_BY_CUSTOMER]: {
                config: {
                    scopes: ['read:list_history_point'],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { customerID, type, fromDate, toDate, page, limit } = req.query;

                        const listHistoryPointOfCustomer = await HISTORY_POINT_MODEL.getListByCustomer({ 
							customerID, type, fromDate, toDate, page, limit
						});
                        res.json(listHistoryPointOfCustomer);
                    }]
                },
            },

            /**
             * Function: Danh sách lịch sử tích đổi điểm của customer theo type (API)
             * Date: 05/07/2021
             * Dev: depv
             */
            [CF_ROUTINGS_HISTORY_POINT.LIST_HISTORY_POINT_OF_CUSTOMER_BY_TYPE]: {
                config: {
                    scopes: ['read:list_history_point'],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } = req.customer;
                        const { type, fromDate, toDate, page, limit } = req.query;
                        const listHistoryPointOfCustomer = await HISTORY_POINT_MODEL.getListByCustomer({ 
							customerID, type: +type, fromDate, toDate, page, limit
						});
                        res.json(listHistoryPointOfCustomer);
                    }]
                },
            },

			/**
             * Function: Danh sách lịch sử đổi điểm (API)
             * Date: 05/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.API_LIST_HISTORY_POINT]: {
                config: {
                    scopes: ['read:list_history_point'],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
						const { type, page, limit } = req.query;
                        console.log({
                            type, page, limit 
                        });
                        const listHistoryPoint = await HISTORY_POINT_MODEL.getListWithPaging({ type, page, limit });
                        res.json(listHistoryPoint);
                    }]
                },
            },

			/**
             * Function: Danh sách lịch sử đổi điểm (VIEW)
             * Date: 05/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.LIST_HISTORY_POINT]: {
                config: {
					scopes: ['read:list_history_point'],
					type: 'view',
                    view: 'index.ejs',
					title: 'List History Point - NANDIO',
					code: CF_ROUTINGS_HISTORY_POINT.LIST_HISTORY_POINT,
					inc: path.resolve(__dirname, '../views/list_history_point.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
						const { typePoint } = req.query;
                        const listHistoryPoint = await HISTORY_POINT_MODEL.getList({ type: +typePoint });

                        ChildRouter.renderToView(req, res, {
							listHistoryPoint: listHistoryPoint.data || [],
							typePoint,
						})
                    }]
                },
            },

			/**
             * Function: Danh sách quà đã đổi (API)
             * Date: 05/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.API_LIST_REWARD_OF_YOU]: {
                config: {
                    scopes: ['read:list_history_point'],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } = req.customer;
						const { page, limit } = req.query;
                        const listHistoryPoint = await HISTORY_POINT_MODEL.getListRewardOfYou({ customerID, page: +page, limit: +limit });
                        res.json(listHistoryPoint);
                    }]
                },
            },

            /**
             * Function: Thên lịch sử đổi điểm + tạo record lịch sử đổi quà (exchange_gift) (API)
             * Date: 04/12/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.API_ADD_HISTORY_POINT]: {
                config: {
                    scopes: ['create:history_point'],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
                        const { _id: employeeID } = req.user;
                        const { customerID, rewardID, type, amount } = req.body;

                        const infoAfterInsert = await HISTORY_POINT_MODEL.insert({ 
							customerID, employeeID, rewardID, type: +type, amount
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            [CF_ROUTINGS_HISTORY_POINT.API_CHECK_INVENTORY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
                        const { employeeID, productID } = req.query;

                        const response = await HISTORY_POINT_MODEL.checkInventory({ 
							employeeID, productID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Danh sách quà đã đổi theo danh sách customer (API)
             * Date: 03/12/2021
             * Dev: MinhVH
             */
            // [CF_ROUTINGS_HISTORY_POINT.API_LIST_REWARD_OF_LIST_CUSTOMER]: {
            //     config: {
            //         auth: [ roles.role.all.bin ],
            //         type: 'json'
            //     },
            //     methods: {
            //         post: [ async function (req, res) {
            //             let { customersID } = req.body;

            //             const listHistoryPoint = await HISTORY_POINT_COLL
            //                 .find({ customer: { $in: customersID } })
            //                 .lean();

            //             res.json(listHistoryPoint);
            //         }]
            //     },
            // },

        }
    }
};
