"use strict";

const { Schema } = require('mongoose');
const BASE_COLL  = require('../../../database/intalize/base-coll');

/**
 * COLLECTION LỊCH SỬ ĐỔI ĐIỂM CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('history_point', {
	/**
     * KHÁCH HÀNG
     */
	customer: {
        type:  Schema.Types.ObjectId,
        ref: 'customer'
    },
	/**
	 * CASE: do khách hàng mua hàng về cần tích điểm trên từng sản phẩm
	 * 	-> chỉ lưu product vào history_point, không cần lưu order 
	 */
	product: {
		type:  Schema.Types.ObjectId,
        ref: 'product'
	},
	//________ID đổi quà
	// reward: {
	// 	type:  Schema.Types.ObjectId,
    //     ref: 'reward'
	// },
	//________Điểm trước khi tác động
	beforPoint: {
		type: Number,
	},

	//________Điểm được cộng/trừ
	currentPoint: {
		type: Number,
	},

	//________Điểm tác động cộng trừ
	point: {
		type: Number,
		required: true
	},
	/**
	 * Loại lịch sử:
	 * 1: Tích điểm
	 * 2: Đổi điểm
	 */
	type: {
		type: Number,
		default: 1
	},
	/**
	 * Loại nền tảng:
	 * NANDIO STORE
	 * NANDIO SHOP
	 */
	flatform: {
		type: String,
		enum: ['STORE', 'SHOP'],
	},
});
