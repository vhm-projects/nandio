"use strict";

const { Schema } = require('mongoose');
const BASE_COLL  = require('../../../database/intalize/base-coll');

/**
 * COLLECTION LỊCH SỬ ĐỔI ĐIỂM CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('order_gift_exchange', {
	/**
     * KHÁCH HÀNG
     */
	customer: {
        type:  Schema.Types.ObjectId,
        ref: 'customer'
    },
	
	/**
	 * CASE: 
	 * 	do khách hàng mua hàng về cần tích điểm trên từng sản phẩm
	 * 	-> chỉ lưu product vào history_point, không cần lưu order 
	 */
	rewards: [
		{
			reward: {
				type:  Schema.Types.ObjectId,
				ref: 'reward'
			},
			// onModel: {
			// 	type: String,
			// 	enum: ['product', 'reward']
			// },
			quantities: {
				type: Number,
				default: 1
			},
			point: {
				type: Number,
				default: 0
			}
		}
	],
	
	deliveryPointConfig: {
		type:  Schema.Types.ObjectId,
        ref: 'delivery_point_config'
	},
	//________Tổng điểm của quà tặng
	totalPoint : {
		type: Number,
		default: 0
	},

	//________Điểm được miễn phí giao hàng
	pointDeliveryPointConfig : {
		type: Number,
		default: 0
	},

	/**
	 * 
	 * 1: Đổi quà từ cửa hàng gần nhất
	 * 2: Nhận quà tại nhà
	 */
	type: {
		type: Number,
		default: 1
	},
	
	/**
	 * Loại nền tảng:
	 * NANDIO STORE
	 * NANDIO SHOP
	 */
	 flatform: {
		type: String,
		enum: ['STORE', 'SHOP'],
	},

});
