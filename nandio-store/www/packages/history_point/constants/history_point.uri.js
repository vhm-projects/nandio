const BASE_ROUTE = '/history';
const API_BASE_ROUTE = '/api';

const CF_ROUTINGS_HISTORY_POINT = {
	ADD_HISTORY_POINT: `${BASE_ROUTE}/add-history-point`,
	INFO_HISTORY_POINT: `${BASE_ROUTE}/info-history-point`,

	LIST_HISTORY_POINT: `${BASE_ROUTE}/list-history-point`,
	LIST_HISTORY_POINT_BY_CUSTOMER: `${BASE_ROUTE}/list-history-point-by-customer`,
	LIST_HISTORY_POINT_OF_CUSTOMER_BY_TYPE: `${BASE_ROUTE}/list-history-point-of-customer-by-type`,

	API_ADD_HISTORY_POINT: `${API_BASE_ROUTE}${BASE_ROUTE}/add-history-point`,
	API_CHECK_INVENTORY: `${API_BASE_ROUTE}${BASE_ROUTE}/check-inventory`,
	API_LIST_HISTORY_POINT: `${API_BASE_ROUTE}${BASE_ROUTE}/list-history-point`,
	API_LIST_REWARD_OF_YOU: `${API_BASE_ROUTE}${BASE_ROUTE}/list-reward-of-you`,
	API_LIST_REWARD_OF_LIST_CUSTOMER: `${API_BASE_ROUTE}${BASE_ROUTE}/list-reward-of-list-customer`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_HISTORY_POINT = CF_ROUTINGS_HISTORY_POINT;
