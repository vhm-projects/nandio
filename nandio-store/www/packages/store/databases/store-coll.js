"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

const GeoSchema = new Schema({
    type: {
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        index: "2dsphere"
    }
});

module.exports = BASE_COLL('store', {
    /**
     * Mã cửa hàng
     */
    code: {
        type: String,
        unique: true,
        required: true,
    },
    /**
     * Tên cửa hàng
     */
    name: {
        type: String,
        required: true,
    },
    /**
     * Điện thoại
     */
    phone: {
        type: String,
        required: true,
    },
    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Vùng
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * Nhà phân phối
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    location: GeoSchema,
    /**
     * Địa chỉ
     */
    address: {
        type: String,
    },
    /**
     * Tỉnh/Thành phố
     */
    city: {
        type: String,
    },
    cityName: {
        type: String,
    },
    /**
     * Quận/huyện
     */
    district: {
        type: String,
    },
    districtName: {
        type: String,
    },
    /**
     * Xã phường
     */
    ward: {
        type: String,
    },
    wardName: {
        type: String,
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
    /**
     * Danh sách thương hiệu
     */
    brands: [{
        type: Schema.Types.ObjectId,
        ref: 'brand',
    }],
    /**
     * Giờ mở cửa
     */
    openTime: {
        type: Date,
    },
    /**
     * Giờ đóng cửa
     */
    closeTime: {
        type: Date,
    },
    /**
     * Ảnh đại diện
     */
    avatar: {
        type: Schema.Types.ObjectId,
        ref: 'image',
    },
    /**
     * Bộ sưu tập
     */
    gallery: [{
        type: Schema.Types.ObjectId,
        ref: 'image',
    }],

    employees: [{
        type: Schema.Types.ObjectId,
        ref: 'employee',
    }]

});