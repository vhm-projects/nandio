"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_STORE_TYPE,
} = require('../constants/store');
const {
    CF_ROUTINGS_STORE
} = require('../constants/store/store.uri');

/**
 * MODELS
 */
const STORE_MODEL = require('../models/store').MODEL;
const IMAGE_MODEL = require('../../image/models/image').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;
const COMMON_MODEL    = require("../../../packages/common/models/common").MODEL;

/**
 * COLLECTIONS
 */

const {
    REGION_COLL
} = require('../../region_area');

const {
    AREA_COLL
} = require('../../region_area');

const {
    DISTRIBUTOR_COLL
} = require('../../distributor');

const {
    BRAND_COLL
} = require('../../brand');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ STORE  ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: Get List area By parent (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            '/store/list-area-by-parent': {
                config: {
                    scopes: ['read:list_store'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            parent
                        } = req.query;

                        const listAreaByParent = await AREA_COLL
                            .find({
                                parent
                            })
                            .lean();
                        res.json(listAreaByParent);
                    }]
                },
            },

            /**
             * Function: Insert Store (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            // [CF_ROUTINGS_STORE.ADD_STORE]: {
            //     config: {
            //         scopes: ['create:store'],
            //         type: 'view',
            //         view: 'index.ejs',
            //         title: 'Thêm cửa hàng',
            //         code: CF_ROUTINGS_STORE.ADD_STORE,
            //         inc: path.resolve(__dirname, '../views/store/add_store.ejs')
            //     },
            //     methods: {
            //         get: [async function(req, res) {
            //             let { region } = req.user;
            //             let condition = { state: 1, status: 1 };
            //             region && (condition._id = region);

            //             let listRegions = await REGION_COLL.find(condition).lean();
            //             let listDistributors = await DISTRIBUTOR_COLL.find({ state: 1, status: 1 }).lean();

            //             let listBrands = await BRAND_COLL.find({
            //                 state: 1,
            //                 status: 1
            //             }).lean()
            //             .sort({
            //                 modifyAt: -1
            //             })

            //             let listProvince = COMMON_MODEL.listProvinceAll({  });
            //             ChildRouter.renderToView(req, res, {
            //                 listRegions,
            //                 listDistributors,
            //                 listBrands,
            //                 CF_ROUTINGS_STORE,
            //                 listProvince: listProvince.data,
            //             });
            //         }],
            //         post: [async function(req, res) {
            //             let userCreate = req.user && req.user._id;
            //             let {
            //                 code,
            //                 name,
            //                 phone,
            //                 region,
            //                 area,
            //                 distributor,
            //                 lat,
            //                 lng,
            //                 address,
            //                 city,
            //                 district,
            //                 ward,
            //                 cityName, districtName, wardName,
            //                 brands,
            //                 openTime,
            //                 closeTime,
            //                 avatar,
            //             } = req.body;


            //             if (avatar) {
            //                 let infoImageAfterInsert = await IMAGE_MODEL.insert({
            //                     name: avatar.name,
            //                     path: avatar.path,
            //                     size: avatar.size
            //                 });
            //                 avatar = infoImageAfterInsert.data._id;
            //             }

            //             let infoAfterInsert = await STORE_MODEL.insert({
            //                 code,
            //                 name,
            //                 phone,
            //                 region,
            //                 area,
            //                 distributor,
            //                 lat,
            //                 lng,
            //                 address,
            //                 city,
            //                 district,
            //                 ward,
            //                 cityName, districtName, wardName,
            //                 brands,
            //                 openTime,
            //                 closeTime,
            //                 avatar,
            //                 userCreate
            //             });

            //             res.json(infoAfterInsert);
            //         }]
            //     },
            // },

            /**
             * Function: Update Store By Id (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_STORE.UPDATE_STORE_BY_ID]: {
                config: {
                    scopes: ['update:store'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật cửa hàng',
                    code: CF_ROUTINGS_STORE.UPDATE_STORE_BY_ID,
                    inc: path.resolve(__dirname, '../views/store/update_store.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            storeID
                        } = req.query;
                        let { region } = req.user;
                        let condition = {};
                        region && (condition._id = region);

                        let infoStore = await STORE_MODEL.getInfoById(storeID);
                        if (infoStore.error) {
                            return res.redirect('/something-went-wrong');
                        }

                        let listRegions = await REGION_COLL
                            .find(condition)
                            .sort({
                                modifyAt: -1
                            }).lean()

                        let listAreas = [];
                        if (infoStore.data.region) {
                            listAreas = await AREA_COLL.find({
                                state: 1,
                                status: 1,
                                parent: infoStore.data.region._id
                            }).sort({
                                modifyAt: -1
                            }).lean()
                        }

                        let listDistributors = await DISTRIBUTOR_COLL
                            .find({ state: 1, status: 1 })
                            .sort({
                                modifyAt: -1
                            }).lean()

                        let listBrands = await BRAND_COLL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean()

                        // console.log({ STORE: infoStore.data })

                        let listProvince   = COMMON_MODEL.listProvinceAll({});
                        let listDistrict   = COMMON_MODEL.listDistrict({
                            province: infoStore.data.city
                        });
                        let listWard       = await COMMON_MODEL.listWard({
                            district: infoStore.data.district
                        });

                        ChildRouter.renderToView(req, res, {
                            infoStore: infoStore.data || {},
                            listProvince: listProvince.data,
                            listDistrict: listDistrict.data,
                            listWard: listWard.data,
                            listRegions,
                            listAreas,
                            listDistributors,
                            listBrands,
                            CF_ROUTINGS_STORE
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            storeID,
                            // code,
                            // name,
                            // phone,
                            // region,
                            // area,
                            // distributor,
                            lat,
                            lng,
                            // address,
                            // city,
                            // district,
                            // ward,
                            // cityName, districtName, wardName,
                            // status,
                            // brands,
                            // openTime,
                            // closeTime,
                            // avatar,
                        } = req.body;

                        // if (avatar) {
                        //     let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        //         name: avatar.name,
                        //         path: avatar.path,
                        //         size: avatar.size
                        //     });
                        //     avatar = infoImageAfterInsert.data._id;
                        // }

                        const infoAfterUpdate = await STORE_MODEL.update({
                            storeID,
                            // code,
                            // name,
                            // phone,
                            // region,
                            // area,
                            // distributor,
                            lat,
                            lng,
                            // address,
                            // city,
                            // district,
                            // ward,
                            // cityName, districtName, wardName,
                            // status,
                            // brands,
                            // openTime,
                            // closeTime,
                            // avatar,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Store By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_STORE.UPDATE_STORE_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:store'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            storeID,
                            code,
                            name,
                            phone,
                            region,
                            area,
                            distributor,
                            lat,
                            lng,
                            address,
                            city,
                            district,
                            ward,
                            status,
                            brands,
                            openTime,
                            closeTime,
                            avatar,
                        } = req.body;


                        if (avatar) {
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                name: avatar.name,
                                path: avatar.path,
                                size: avatar.size
                            });
                            avatar = infoImageAfterInsert.data._id;
                        }

                        const infoAfterUpdate = await STORE_MODEL.updateNotRequire({
                            storeID,
                            code,
                            name,
                            phone,
                            region,
                            area,
                            distributor,
                            lat,
                            lng,
                            address,
                            city,
                            district,
                            ward,
                            status,
                            brands,
                            openTime,
                            closeTime,
                            avatar,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Store By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_STORE.DELETE_STORE_BY_ID]: {
                config: {
                    scopes: ['delete:store'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            storeID
                        } = req.params;

                        const infoAfterDelete = await STORE_MODEL.deleteById(storeID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Store By List Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_STORE.DELETE_STORE_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:store'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            storeID
                        } = req.body;

                        const infoAfterDelete = await STORE_MODEL.deleteByListId(storeID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Store By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_STORE.GET_INFO_STORE_BY_ID]: {
                config: {
                    scopes: ['read:info_store'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            storeID
                        } = req.params;

                        const infoStoreById = await STORE_MODEL.getInfoById(storeID);
                        res.json(infoStoreById);
                    }]
                },
            },

            /**
             * Function: Get List Store (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_STORE.GET_LIST_STORE]: {
                config: {
                    scopes: ['read:list_store'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách cửa hàng',
                    code: CF_ROUTINGS_STORE.GET_LIST_STORE,
                    inc: path.resolve(__dirname, '../views/store/list_stores.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        console.log("===========================123123123123");
                        let {
                            keyword,
                            status,
                            openTimeDateRange,
                            closeTimeDateRange,
                            typeGetList
                        } = req.query;

                        let listStores = [];
                        // if (typeGetList === 'FILTER') {
                        //     listStores = await STORE_MODEL.getListByFilter({
                        //         keyword,
                        //         status,
                        //         openTimeDateRange,
                        //         closeTimeDateRange,
                        //         region
                        //     });
                        // } else {
                        //     listStores = await STORE_MODEL.getList({ region });
                        // }

                        ChildRouter.renderToView(req, res, {
                            // listStores: listStores.data || [],
                            STATUS_STORE_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Store By Field (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_STORE.GET_LIST_STORE_BY_FIELD]: {
                config: {
                    scopes: ['read:list_store'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách cửa hàng',
                    code: CF_ROUTINGS_STORE.GET_LIST_STORE_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/store/list_stores.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            status,
                            openTimeDateRange,
                            closeTimeDateRange,
                            type
                        } = req.query;

                        let listStores = await STORE_MODEL.getListByFilter({
                            keyword,
                            status,
                            openTimeDateRange,
                            closeTimeDateRange,
                            [field]: value,
                            region
                        });

                        ChildRouter.renderToView(req, res, {
                            listStores: listStores.data || [],
                            STATUS_STORE_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Store Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_STORE.GET_LIST_STORE_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_store'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listStoreServerSide = await STORE_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(listStoreServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Store Excel Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_STORE.GET_LIST_STORE_EXCEL]: {
                config: {
                    scopes: ['read:list_store'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = STORE_MODEL.getConditionArrayFilterExcel(listItemExport)
                       
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Employee Excel Export (API)
             * Date: 22/11/2021
             * Dev: Automatic
             */
             [CF_ROUTINGS_STORE.DOWNLOAD_LIST_STORE_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_store'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'store'
                        });
                       
                        let conditionObj = STORE_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listEmployee = await STORE_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listEmployee)
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_STORE.API_GET_INFO_STORE]: {
                config: {
                    scopes: ['read:info_store'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            storeID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoStore = await STORE_MODEL.getInfoStore({
                            storeID,
                            select,
                            explain
                        });
                        res.json(infoStore);
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_STORE.API_GET_LIST_STORES]: {
                config: {
                    scopes: ['read:list_store'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { region: regionAdmin } = req.user;
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listStores = await STORE_MODEL.getListStores({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                            regionAdmin
                        });
                        console.log({ listStores: listStores.data.records })
                        res.json(listStores);
                    }]
                },
            },

        }
    }
};
