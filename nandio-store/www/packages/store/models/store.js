"use strict";

/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const ObjectID = require('mongoose').Types.ObjectId;
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const path = require('path');
const fs = require('fs');
const XlsxPopulate = require('xlsx-populate');
const Excel = require('exceljs');

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    checkPhoneNumber,
    checkNumberIsValidWithRange
} = require('../../../utils/utils');
const {
    STATUS_STORE_TYPE,
} = require('../constants/store');


/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const STORE_COLL = require('../databases/store-coll');

const {
    REGION_COLL
} = require('../../region_area');

const {
    AREA_COLL
} = require('../../region_area');

const {
    DISTRIBUTOR_COLL
} = require('../../distributor');

const {
    BRAND_COLL
} = require('../../brand');

const {
    IMAGE_COLL
} = require('../../image');


class Model extends BaseModel {
    constructor() {
        super(STORE_COLL);
    }

    /**
         * Tạo mới store
		* @param {string} code
		* @param {string} name
		* @param {string} phone
		* @param {object} region
		* @param {object} area
		* @param {object} distributor
		* @param {string} lat
		* @param {string} lng
		* @param {string} address
		* @param {string} city
		* @param {string} district
		* @param {string} ward
		* @param {array} brands
		* @param {date} openTime
		* @param {date} closeTime
		* @param {object} avatar

         * @param {objectId} userCreate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        code,
        name,
        phone,
        region,
        area,
        distributor,
        lat,
        lng,
        address,
        city,
        district,
        ward,
        cityName, districtName, wardName,
        brands,
        openTime,
        closeTime,
        avatar,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (code.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mã cửa hàng không được lớn hơn 125 ký tự'
                    });
                }

                if (!code) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mã cửa hàng'
                    });
                }

                if (name.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tên cửa hàng không được lớn hơn 125 ký tự'
                    });
                }

                if (!name) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tên cửa hàng'
                    });
                }

                if (phone.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài điện thoại không được lớn hơn 125 ký tự'
                    });
                }

                if (!phone) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập điện thoại'
                    });
                }

                if (!lat) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập latitude'
                    });
                }

                if (!lng) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập longtitude'
                    });
                }

                if (address.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài địa chỉ không được lớn hơn 125 ký tự'
                    });
                }

                if (city.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tỉnh/thành phố không được lớn hơn 125 ký tự'
                    });
                }

                if (district.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài quận/huyện không được lớn hơn 125 ký tự'
                    });
                }

                if (ward.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài xã phường không được lớn hơn 125 ký tự'
                    });
                }

                if (!checkPhoneNumber(phone)) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại không đúng định dạng'
                    });
                }


                if (region && !checkObjectIDs([region])) {
                    return resolve({
                        error: true,
                        message: 'miền không hợp lệ'
                    });
                }

                if (area && !checkObjectIDs([area])) {
                    return resolve({
                        error: true,
                        message: 'vùng không hợp lệ'
                    });
                }

                if (distributor && !checkObjectIDs([distributor])) {
                    return resolve({
                        error: true,
                        message: 'nhà phân phối không hợp lệ'
                    });
                }

                if (brands && !checkObjectIDs(brands)) {
                    return resolve({
                        error: true,
                        message: 'danh sách thương hiệu không hợp lệ'
                    });
                }

                if (avatar && !checkObjectIDs([avatar])) {
                    return resolve({
                        error: true,
                        message: 'ảnh đại diện không hợp lệ'
                    });
                }


                const checkCodeExits = await STORE_COLL.findOne({
                    code
                });
                if (checkCodeExits) {
                    return resolve({
                        error: true,
                        message: 'Mã cửa hàng đã tồn tại'
                    });
                }

                let dataInsert = {
                    code,
                    name,
                    phone,
                    region,
                    area,
                    distributor,
                    address,
                    city,
                    district,
                    ward,
                    cityName, districtName, wardName,
                    brands,
                    avatar,
                    location: {
                        type: "Point",
                        coordinates: [parseFloat(lng), parseFloat(lat)]
                    },
                    userCreate
                };

                if (openTime) {
                    const hours = openTime.slice(0, 2);
                    const minutes = openTime.slice(3);
                    const date = new Date();
                    date.setHours(hours, minutes);

                    dataInsert.openTime = date;
                }

                if (closeTime) {
                    const hours = closeTime.slice(0, 2);
                    const minutes = closeTime.slice(3);
                    const date = new Date();
                    date.setHours(hours, minutes);

                    dataInsert.closeTime = date;
                }

                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo cửa hàng thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật store
         * @param {objectId} storeID
		* @param {string} code
		* @param {string} name
		* @param {string} phone
		* @param {object} region
		* @param {object} area
		* @param {object} distributor
		* @param {string} lat
		* @param {string} lng
		* @param {string} address
		* @param {string} city
		* @param {string} district
		* @param {string} ward
		* @param {number} status
		* @param {array} brands
		* @param {date} openTime
		* @param {date} closeTime
		* @param {object} avatar

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        storeID,
        // code,
        // name,
        // phone,
        // region,
        // area,
        // distributor,
        lat,
        lng,
        // address,
        // city,
        // district,
        // ward,
        // cityName, districtName, wardName,
        // status,
        // brands,
        // openTime,
        // closeTime,
        // avatar,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([storeID])) {
                    return resolve({
                        error: true,
                        message: 'storeID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                // if (code.length > 125) {
                //     return resolve({
                //         error: true,
                //         message: 'Độ dài mã cửa hàng không được lớn hơn 125 ký tự'
                //     });
                // }
                //
                // if (!code) {
                //     return resolve({
                //         error: true,
                //         message: 'Bạn cần nhập mã cửa hàng cho store'
                //     });
                // }
                //
                // if (name.length > 125) {
                //     return resolve({
                //         error: true,
                //         message: 'Độ dài tên cửa hàng không được lớn hơn 125 ký tự'
                //     });
                // }
                //
                // if (!name) {
                //     return resolve({
                //         error: true,
                //         message: 'Bạn cần nhập tên cửa hàng cho store'
                //     });
                // }
                //
                // if (phone.length > 125) {
                //     return resolve({
                //         error: true,
                //         message: 'Độ dài điện thoại không được lớn hơn 125 ký tự'
                //     });
                // }
                //
                // if (!phone) {
                //     return resolve({
                //         error: true,
                //         message: 'Bạn cần nhập điện thoại cho store'
                //     });
                // }

                if (!lat) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập latitude cho store'
                    });
                }

                if (!lng) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập longtitude cho store'
                    });
                }

                // if (address.length > 125) {
                //     return resolve({
                //         error: true,
                //         message: 'Độ dài địa chỉ không được lớn hơn 125 ký tự'
                //     });
                // }
                //
                // if (city.length > 125) {
                //     return resolve({
                //         error: true,
                //         message: 'Độ dài tỉnh/thành phố không được lớn hơn 125 ký tự'
                //     });
                // }
                //
                // if (district.length > 125) {
                //     return resolve({
                //         error: true,
                //         message: 'Độ dài quận/huyện không được lớn hơn 125 ký tự'
                //     });
                // }
                //
                // if (ward.length > 125) {
                //     return resolve({
                //         error: true,
                //         message: 'Độ dài xã phường không được lớn hơn 125 ký tự'
                //     });
                // }
                //
                //
                // if (status && !checkNumberIsValidWithRange({
                //         arrValid: [1, 2],
                //         val: status
                //     })) {
                //     return resolve({
                //         error: true,
                //         message: 'trạng thái không hợp lệ'
                //     });
                // }
                //
                // if (!checkPhoneNumber(phone)) {
                //     return resolve({
                //         error: true,
                //         message: 'Số điện thoại không đúng định dạng'
                //     });
                // }
                //
                //
                // if (region && !checkObjectIDs([region])) {
                //     return resolve({
                //         error: true,
                //         message: 'miền không hợp lệ'
                //     });
                // }
                //
                // if (area && !checkObjectIDs([area])) {
                //     return resolve({
                //         error: true,
                //         message: 'vùng không hợp lệ'
                //     });
                // }
                //
                // if (distributor && !checkObjectIDs([distributor])) {
                //     return resolve({
                //         error: true,
                //         message: 'nhà phân phối không hợp lệ'
                //     });
                // }
                //
                // if (brands && !checkObjectIDs(brands)) {
                //     return resolve({
                //         error: true,
                //         message: 'danh sách thương hiệu không hợp lệ'
                //     });
                // }
                //
                // if (avatar && !checkObjectIDs([avatar])) {
                //     return resolve({
                //         error: true,
                //         message: 'ảnh đại diện không hợp lệ'
                //     });
                // }

                const checkExists = await STORE_COLL.findById(storeID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không tồn tại'
                    });
                }

                // const checkCodeExits = await STORE_COLL.findOne({
                //     code
                // });
                // if (checkCodeExits && checkExists.code !== code) {
                //     return resolve({
                //         error: true,
                //         message: 'mã cửa hàng đã tồn tại'
                //     });
                // }


                let dataUpdate = {
                    userUpdate
                };
                // code && (dataUpdate.code = code);
                // name && (dataUpdate.name = name);
                // phone && (dataUpdate.phone = phone);
                // region && (dataUpdate.region = region);
                // area && (dataUpdate.area = area);
                // distributor && (dataUpdate.distributor = distributor);
                // dataUpdate.address = address;
                // dataUpdate.city = city;
                // dataUpdate.district = district;
                // dataUpdate.ward = ward;
                // dataUpdate.status = status;
                // brands && (dataUpdate.brands = brands);
                // dataUpdate.cityName = cityName;
                // dataUpdate.districtName = districtName;
                // dataUpdate.wardName = wardName;

                if(lat && lng){
                    dataUpdate.location = {
                        type: "Point",
                        coordinates: [parseFloat(lng), parseFloat(lat)]
                    }
                }

                // if (openTime) {
                //     const hours = openTime.slice(0, 2);
                //     const minutes = openTime.slice(3);
                //     const date = new Date();
                //     date.setHours(hours, minutes);
                //
                //     dataUpdate.openTime = date;
                // }
                //
                // if (closeTime) {
                //     const hours = closeTime.slice(0, 2);
                //     const minutes = closeTime.slice(3);
                //     const date = new Date();
                //     date.setHours(hours, minutes);
                //
                //     dataUpdate.closeTime = date;
                // }
                // avatar && (dataUpdate.avatar = avatar);

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: storeID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật store (không bắt buộc)
         * @param {objectId} storeID
		* @param {string} code
		* @param {string} name
		* @param {string} phone
		* @param {object} region
		* @param {object} area
		* @param {object} distributor
		* @param {string} lat
		* @param {string} lng
		* @param {string} address
		* @param {string} city
		* @param {string} district
		* @param {string} ward
		* @param {number} status
		* @param {array} brands
		* @param {date} openTime
		* @param {date} closeTime
		* @param {object} avatar

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        storeID,
        code,
        name,
        phone,
        region,
        area,
        distributor,
        lat,
        lng,
        address,
        city,
        district,
        ward,
        status,
        brands,
        openTime,
        closeTime,
        avatar,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([storeID])) {
                    return resolve({
                        error: true,
                        message: 'storeID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (phone && !checkPhoneNumber(phone)) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại không đúng định dạng'
                    });
                }

                if (region && !checkObjectIDs([region])) {
                    return resolve({
                        error: true,
                        message: 'miền không hợp lệ'
                    });
                }

                if (area && !checkObjectIDs([area])) {
                    return resolve({
                        error: true,
                        message: 'vùng không hợp lệ'
                    });
                }

                if (distributor && !checkObjectIDs([distributor])) {
                    return resolve({
                        error: true,
                        message: 'nhà phân phối không hợp lệ'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trạng thái không hợp lệ'
                    });
                }

                if (brands && !checkObjectIDs(brands)) {
                    return resolve({
                        error: true,
                        message: 'danh sách thương hiệu không hợp lệ'
                    });
                }

                if (avatar && !checkObjectIDs([avatar])) {
                    return resolve({
                        error: true,
                        message: 'ảnh đại diện không hợp lệ'
                    });
                }

                const checkExists = await STORE_COLL.findById(storeID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                code && (dataUpdate.code = code);
                name && (dataUpdate.name = name);
                phone && (dataUpdate.phone = phone);
                region && (dataUpdate.region = region);
                area && (dataUpdate.area = area);
                distributor && (dataUpdate.distributor = distributor);
                lat && (dataUpdate.lat = lat);
                lng && (dataUpdate.lng = lng);
                address && (dataUpdate.address = address);
                city && (dataUpdate.city = city);
                district && (dataUpdate.district = district);
                ward && (dataUpdate.ward = ward);
                status && (dataUpdate.status = status);
                brands && (dataUpdate.brands = brands);
                if (openTime) {
                    const hours = openTime.slice(0, 2);
                    const minutes = openTime.slice(3);
                    const date = new Date();
                    date.setHours(hours, minutes);

                    dataUpdate.openTime = date;
                }

                if (closeTime) {
                    const hours = closeTime.slice(0, 2);
                    const minutes = closeTime.slice(3);
                    const date = new Date();
                    date.setHours(hours, minutes);

                    dataUpdate.closeTime = date;
                }
                avatar && (dataUpdate.avatar = avatar);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: storeID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa store
     * @param {objectId} storeID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(storeID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([storeID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị storeID không hợp lệ'
                    });
                }

                const infoAfterDelete = await this.updateById(storeID, {
                    state: 2
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa store
     * @param {array} storeID
     * @extends {BaseModel}
     * @returns {{
     *      error: boolean,
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number },
     * }}
     */
    deleteByListId(storeID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(storeID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị storeID không hợp lệ'
                    });
                }

                const infoAfterDelete = await STORE_COLL.updateMany({
                    _id: {
                        $in: storeID
                    }
                }, {
                    state: 2,
                    modifyAt: new Date()
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin store
     * @param {objectId} storeID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(storeID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([storeID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị storeID không hợp lệ'
                    });
                }

                const infoStore = await STORE_COLL.findById(storeID)
                    .populate('region area distributor avatar gallery')

                if (!infoStore) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin cửa hàng'
                    });
                }

                return resolve({
                    error: false,
                    data: infoStore
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách store
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList({ region }) {
        return new Promise(async resolve => {
            try {
                let condition = { state: 1 };
                region && (condition.region = region);

                const listStore = await STORE_COLL
                    .find(condition)
                    .populate('region area distributor brands avatar gallery')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listStore) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách cửa hàng'
                    });
                }

                return resolve({
                    error: false,
                    data: listStore
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách store theo bộ lọc
		* @param {string} keyword
		* @enum {number} status
		* @param {date} openTimeDateRange
		* @param {date} closeTimeDateRange

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        keyword,
        status,
        openTimeDateRange,
        closeTimeDateRange,
        region
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        code: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        name: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        phone: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        lat: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        lng: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        address: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        city: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        cityName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        district: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        districtName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        ward: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        wardName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }

                status && (conditionObj.status = status);
                region && (conditionObj.region = region);

                if (openTimeDateRange) {
                    let [fromDate, toDate] = openTimeDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.openTime = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                if (closeTimeDateRange) {
                    let [fromDate, toDate] = closeTimeDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.closeTime = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                const listStoreByFilter = await STORE_COLL
                    .find(conditionObj).populate('region area distributor brands avatar gallery')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listStoreByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách cửa hàng"
                    });
                }

                return resolve({
                    error: false,
                    data: listStoreByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách store theo bộ lọc (server side)

     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition

     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir,
        region
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        code: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        name: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        phone: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        lat: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        lng: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        address: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        city: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        cityName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        district: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        districtName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        ward: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        wardName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trạng thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                region && (conditionObj.region = ObjectID(region));

                const skip = (page - 1) * limit;
                const totalStore = await STORE_COLL.countDocuments(conditionObj);

                const listStoreByFilter = await STORE_COLL.aggregate([
                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'images',
                            localField: 'avatar',
                            foreignField: '_id',
                            as: 'avatar'
                        }
                    },
                    {
                        $unwind: {
                            path: '$avatar',
                            preserveNullAndEmptyArrays: true
                        },
                    }
                ]).allowDiskUse(true);

                if (!listStoreByFilter) {
                    return resolve({
                        recordsTotal: totalStore,
                        recordsFiltered: totalStore,
                        data: []
                    });
                }

                const listStoreDataTable = listStoreByFilter.map((store, index) => {
                    let status = '';
                    if (store.status == 1) {
                        status = 'checked';
                    }
                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${store._id}" type="checkbox" class="check-record check-record-${store._id}" _index ="${index + 1}"><label for="${store._id}"></label></div></td>`,
                        indexChange: skip + index + 1,
                        code: `<a href="/store/update-store-by-id?storeID=${store._id}">${store.code && store.code.length > 50 ? store.code.substr(0,50) + "..." : store.code} </a>`,
                        name: `${store.name && store.name.length > 50 ? store.name.substr(0,50) + "..." : store.name}`,
                        phone: `${store.phone && store.phone.length > 50 ? store.phone.substr(0,50) + "..." : store.phone}`,
                        region: store.region ? store.region.name : '',
                        area: store.area ? store.area.name : '',
                        distributor: store.distributor ? store.distributor.name : '',
                        lat: `${store.location ? store.location.coordinates[1] : ''}`,
                        lng: `${store.location ? store.location.coordinates[0] : ''}`,
                        status: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-status" _storeID="${store._id}" type="checkbox" id="${store._id}" ${status} style="width: 40px;height: 20px;"></div></td>`,
                        avatar: `${store.avatar ? `<a class="user-avatar me-2 fancybox" href="${store.avatar.path}"><img src="${process.env.AWS_S3_URI}${store.avatar.path}" alt="" class="thumb-xxl rounded"> </a>` : ""}`,
                        createAt: moment(store.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalStore,
                    recordsFiltered: totalStore,
                    data: listStoreDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc store
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc store
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listStoreByFilter = await STORE_COLL.aggregate(arrayFilter)

                if (!listStoreByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listStoreByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listStoreByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                console.time("TEST_TIME");
                // const options = {
                //     filename: pathWriteFile,
                //     useStyles: true,
                //     useSharedStrings: true
                // };
                
                // const workbook = new Excel.stream.xlsx.WorkbookWriter(options);
                // const worksheet = workbook.addWorksheet('Sheet');
                // worksheet.columns = [];

                // arrayItemCustomerChoice.map((elem, index) => {
                //     let nameCollChoice = '';
                //     if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                //         nameCollChoice = ' ' + elem.nameCollChoice
                //     }
                 
                //     worksheet.columns = [
                //         ...worksheet.columns,
                //         {
                //             header: elem.note + nameCollChoice,
                //             key: elem.name
                //         }
                //     ];

                // });

                // worksheet.columns = [
                //     ...worksheet.columns,
                //     {
                //         header: 'Ngày tạo',
                //         key: 'createAt'
                //     }
                // ];

                // listData && listData.length && listData.map((item, index) => {
                //     let conditionObj = {};
                //     arrayItemCustomerChoice.map((elem, indexChoice) => {
                //         let variable = elem.name.split('.');

                //         let value;
                //         if (variable.length > 1) {
                //             let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                //             if (objDataOfVariable) {
                //                 value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                //             }
                //         } else {
                //             value = item[variable[0]] ? item[variable[0]] : '';
                //         }

                //         if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                //             elem.dataEnum.map(isStatus => {
                //                 if (isStatus.value == value) {
                //                     value = isStatus.title;
                //                 }
                //             })
                //         }

                //         if (elem.type == 'date') { // TYPE: DATE
                //             value = moment(value).format('HH:mm DD/MM/YYYY');
                //             if (value == 'Invalid date') {
                //                 value = '';
                //             }
                //             console.log({
                //                 value
                //             });
                //         }

                //         conditionObj = {
                //             ...conditionObj,
                //             [elem.name]: value
                //         }
                //     });
                //     worksheet.addRow({
                //         ...conditionObj,
                //         createAt: moment(item.createAt).format('HH:mm DD/MM/YYYY')
                //     }).commit();
                // });

                // workbook.commit().then(function() {
                //     console.timeEnd("TEST_TIME");
                //     console.log('excel file created');
                //     return resolve({
                //         error: false,
                //         data: "/files/upload_excel_temp/" + fileNameRandom,
                //         path: fileNameRandom,
                //         domain
                //     });
                // });

                
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                           
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('HH:mm DD/MM/YYYY');
                                    if (value == 'Invalid date') {
                                        value = '';
                                    }
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                console.timeEnd("TEST_TIME");
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc store
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            dataEnum: item.dataEnum,
                            nameCollChoice: item.nameCollChoice,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                dataEnum: item.dataEnum,
                                nameCollChoice: item.nameCollChoice,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {

            if (filter.length > 1) {
                condition === 'OR' && (conditionObj.$or = []);
                condition === 'AND' && (conditionObj.$and = []);

                filter.map(filterObj => {
                    const getConditionByFilter = this.getConditionObj(filterObj);

                    if (condition === 'OR') {
                        conditionObj.$or.push(getConditionByFilter);
                    } else {
                        conditionObj.$and.push(getConditionByFilter);
                    }
                })
            } else {
                conditionObj = {
                    ...conditionObj,
                    ...this.getConditionObj(filter[0])
                };
            }
        }


        if (!isEmptyObject(objFilterStatic)) {

            if (objFilterStatic.status) {
                if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                    return resolve({
                        error: true,
                        message: "trạng thái không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    status: Number(objFilterStatic.status)
                }
            }

        }

        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }


        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let lookup = [{
                        $lookup: {
                            from: pluralize.plural(item.coll),
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: pluralize.plural(populate.coll),
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Chi tiết
     * @param {objectId} storeID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoStore({
        storeID,
        select,
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];

                if (!checkObjectIDs([storeID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị storeID không hợp lệ',
                        status: 400
                    });
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'brands', 'avatar', 'gallery'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoStore = await STORE_COLL
                    .findById(storeID)
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoStore = await STORE_COLL.populate(infoStore, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoStore = await STORE_COLL.populate(infoStore, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoStore) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin store',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoStore,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Danh sách
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{
     *   error: boolean,
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListStores({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page,
        regionAdmin
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { state: 1 };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$or = [
                        { name: new RegExp(search, 'i') },
                        { code: new RegExp(search, 'i') }
                    ]
                    // console.log({ search });
                    // conditionObj.$text = {
                    //     $search: search
                    // };
                    // objSort.score = {
                    //     $meta: "textScore"
                    // };
                    // sortBy.score = {
                    //     $meta: "textScore"
                    // };
                }

                Object.keys(filter).map(key => {
                    if (!['region', 'area', 'distributor', 'status', 'brands', 'openTimeDateRange', 'closeTimeDateRange', 'avatar', 'gallery'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    region,
                    area,
                    distributor,
                    status,
                    brands,
                    openTimeDateRange,
                    closeTimeDateRange,
                    avatar,
                    gallery,
                } = filter;

                region && (conditionObj.region = region);
                area && (conditionObj.area = area);
                distributor && (conditionObj.distributor = distributor);
                status && (conditionObj.status = status);
                regionAdmin && (conditionObj.region = regionAdmin);

                if (brands && brands.length) {
                    conditionObj.brands = {
                        $in: brands
                    };
                }

                if (openTimeDateRange) {
                    let [fromDate, toDate] = openTimeDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.openTime = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                if (closeTimeDateRange) {
                    let [fromDate, toDate] = closeTimeDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.closeTime = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                avatar && (conditionObj.avatar = avatar);

                if (gallery && gallery.length) {
                    conditionObj.gallery = {
                        $in: gallery
                    };
                }


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'brands', 'avatar', 'gallery'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }
                let listStores = await STORE_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()
                    
                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listStores = await STORE_COLL.populate(listStores, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listStores = await STORE_COLL.populate(listStores, `${ref}.${field}`);
                        }
                    }
                }

                if (!listStores) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách store',
                        status: 400
                    });
                }

                let totalRecord = await STORE_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listStores,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

}

exports.MODEL = new Model;
