const STORE_COLL = require('./databases/store-coll');
const STORE_MODEL = require('./models/store').MODEL;
const STORE_ROUTES = require('./apis/store');
// MARK REQUIRE

module.exports = {
    STORE_COLL,
    STORE_MODEL,
    STORE_ROUTES,
    // MARK EXPORT
}