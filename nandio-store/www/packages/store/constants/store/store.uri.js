const BASE_ROUTE = '/store';
const API_BASE_ROUTE = '/api/store';

const CF_ROUTINGS_STORE = {
    ADD_STORE: `${BASE_ROUTE}/add-store`,
    UPDATE_STORE_BY_ID: `${BASE_ROUTE}/update-store-by-id`,
    DELETE_STORE_BY_ID: `${BASE_ROUTE}/delete/:storeID`,

    GET_INFO_STORE_BY_ID: `${BASE_ROUTE}/info/:storeID`,
    GET_LIST_STORE: `${BASE_ROUTE}/list-store`,
    GET_LIST_STORE_BY_FIELD: `${BASE_ROUTE}/list-store/:field/:value`,
    GET_LIST_STORE_SERVER_SIDE: `${BASE_ROUTE}/list-store-server-side`,

    UPDATE_STORE_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-store-by-id-v2`,
    DELETE_STORE_BY_LIST_ID: `${BASE_ROUTE}/delete-store-by-list-id`,
    GET_LIST_STORE_EXCEL: `${BASE_ROUTE}/list-store-excel`,
    DOWNLOAD_LIST_STORE_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-store-excel-export`,

    API_GET_INFO_STORE: `${API_BASE_ROUTE}/info-store/:storeID`,
    API_GET_LIST_STORES: `${API_BASE_ROUTE}/list-stores`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_STORE = CF_ROUTINGS_STORE;