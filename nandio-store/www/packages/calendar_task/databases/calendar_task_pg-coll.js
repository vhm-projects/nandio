"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('calendar_task_pg', {

    /**
     * Cửa hàng
     */
    store: {
        type: Schema.Types.ObjectId,
        ref: 'store',
    },
    /**
     * Nhân viên
     */
    employee: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },
    /**
     * PG cố định 
     * 1: Có,
     * 2: Không
     */
    PGCD: {
        type: Number,
        default: 2,
        enum: [1, 2],
    },
    /**
     * PG Mobile 
     * 1: Có,
     * 2: Không
     */
    PGM: {
        type: Number,
        default: 2,
        enum: [1, 2],
    },
    /**
     * PG tăng cường 
     * 1: Có,
     * 2: Không
     */
    PGTC: {
        type: Number,
        default: 2,
        enum: [1, 2],
    },
    /**
     * PG Sampling 
     * 1: Có,
     * 2: Không
     */
    PGS: {
        type: Number,
        default: 2,
        enum: [1, 2],
    },
    /**
     * Tháng
     */
     month: {
        type: Number,
        required: true,
    },
    /**
     * Năm
     */
    year: {
        type: Number,
        required: true,
    },
});