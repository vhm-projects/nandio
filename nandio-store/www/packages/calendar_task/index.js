const CALENDAR_TASK_NA_COLL = require('./databases/calendar_task_na-coll');
const CALENDAR_TASK_NA_MODEL = require('./models/calendar_task_na').MODEL;
const CALENDAR_TASK_NA_ROUTES = require('./apis/calendar_task_na');

const CALENDAR_TASK_PG_COLL  = require('./databases/calendar_task_pg-coll');
const CALENDAR_TASK_PGM_MODEL  = require('./models/calendar_task_pg').MODEL;
const CALENDAR_TASK_PG_ROUTES  = require('./apis/calendar_task_pg');
    
// MARK REQUIRE

module.exports = {
    CALENDAR_TASK_NA_COLL,
    CALENDAR_TASK_NA_MODEL,
    CALENDAR_TASK_NA_ROUTES,

    CALENDAR_TASK_PG_COLL,
    CALENDAR_TASK_PGM_MODEL,
    CALENDAR_TASK_PG_ROUTES,
    
    // MARK EXPORT
}