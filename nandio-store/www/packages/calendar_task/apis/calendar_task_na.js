"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const XlsxPopulate = require('xlsx-populate');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const { uploadSingle } = require('../../../config/cf_helpers_multer');
const {
    CF_ROUTINGS_CALENDAR_TASK_NA
} = require('../constants/calendar_task_na/calendar_task_na.uri');

/**
 * MODELS
 */
const CALENDAR_TASK_NA_MODEL = require('../models/calendar_task_na').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */
const {
    STORE_COLL
} = require('../../store');

const {
    EMPLOYEE_COLL
} = require('../../employee');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ CALENDAR_TASK_NA  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Calendar_task_NA (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.ADD_CALENDAR_TASK_NA]: {
                config: {
                    scopes: ['create:calendar_task_na'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm lịch làm việc NA',
                    code: CF_ROUTINGS_CALENDAR_TASK_NA.ADD_CALENDAR_TASK_NA,
                    inc: path.resolve(__dirname, '../views/calendar_task_na/add_calendar_task_na.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let condition = { state: 1, status: 1 };
                        region && (condition.region = region);

                        let listStores = await STORE_COLL.find(condition).limit(100).lean();

                        let listEmployees = await EMPLOYEE_COLL.find(condition)
                            .sort({
                                modifyAt: -1
                            })
                            .lean()

                        ChildRouter.renderToView(req, res, {
                            listStores,
                            listEmployees,
                            CF_ROUTINGS_CALENDAR_TASK_NA
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            store,
                            employee,
                            month,
                            year,
                        } = req.body;


                        let infoAfterInsert = await CALENDAR_TASK_NA_MODEL.insert({
                            store,
                            employee,
                            month,
                            year,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Calendar_task_NA By Id (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.UPDATE_CALENDAR_TASK_NA_BY_ID]: {
                config: {
                    scopes: ['update:calendar_task_na'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật lịch làm việc NA',
                    code: CF_ROUTINGS_CALENDAR_TASK_NA.UPDATE_CALENDAR_TASK_NA_BY_ID,
                    inc: path.resolve(__dirname, '../views/calendar_task_na/update_calendar_task_na.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            calendar_task_naID
                        } = req.query;

                        let { region } = req.user;
                        let condition = { state: 1, status: 1 };
                        region && (condition.region = region);

                        let infoCalendar_task_NA = await CALENDAR_TASK_NA_MODEL.getInfoById(calendar_task_naID);
                        if (infoCalendar_task_NA.error) {
                            return res.redirect('/something-went-wrong');
                        }

                        let listEmployees = await EMPLOYEE_COLL
                            .find(condition)
                            .sort({
                                modifyAt: -1
                            }).lean()

                        if(infoCalendar_task_NA.data.store){
                            condition._id = infoCalendar_task_NA.data.store._id;
                        }

                        let listStores = await STORE_COLL
                            .find(condition)
                            .sort({
                                modifyAt: -1
                            }).limit(100).lean();

                        ChildRouter.renderToView(req, res, {
                            infoCalendar_task_NA: infoCalendar_task_NA.data || {},
                            listStores,
                            listEmployees,
                            CF_ROUTINGS_CALENDAR_TASK_NA
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            calendar_task_naID,
                            store,
                            employee,
                            month,
                            year,
                        } = req.body;


                        const infoAfterUpdate = await CALENDAR_TASK_NA_MODEL.update({
                            calendar_task_naID,
                            store,
                            employee,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Calendar_task_NA By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.UPDATE_CALENDAR_TASK_NA_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:calendar_task_na'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            calendar_task_naID,
                            store,
                            employee,
                            month,
                            year,
                        } = req.body;


                        const infoAfterUpdate = await CALENDAR_TASK_NA_MODEL.updateNotRequire({
                            calendar_task_naID,
                            store,
                            employee,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Calendar_task_NA By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.DELETE_CALENDAR_TASK_NA_BY_ID]: {
                config: {
                    scopes: ['delete:calendar_task_na'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            calendar_task_naID
                        } = req.params;

                        const infoAfterDelete = await CALENDAR_TASK_NA_MODEL.deleteById(calendar_task_naID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Calendar_task_NA By List Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.DELETE_CALENDAR_TASK_NA_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:calendar_task_na'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            calendar_task_naID
                        } = req.body;

                        const infoAfterDelete = await CALENDAR_TASK_NA_MODEL.deleteByListId(calendar_task_naID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Calendar_task_NA By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.GET_INFO_CALENDAR_TASK_NA_BY_ID]: {
                config: {
                    scopes: ['read:info_calendar_task_na'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            calendar_task_naID
                        } = req.params;

                        const infoCalendar_task_NAById = await CALENDAR_TASK_NA_MODEL.getInfoById(calendar_task_naID);
                        res.json(infoCalendar_task_NAById);
                    }]
                },
            },

            /**
             * Function: Get List Calendar_task_NA (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.GET_LIST_CALENDAR_TASK_NA]: {
                config: {
                    scopes: ['read:list_calendar_task_na'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách lịch làm việc NA',
                    code: CF_ROUTINGS_CALENDAR_TASK_NA.GET_LIST_CALENDAR_TASK_NA,
                    inc: path.resolve(__dirname, '../views/calendar_task_na/list_calendar_task_nas.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let {
                            typeGetList
                        } = req.query;

                        let listCalendar_task_NAs = [];
                        if (typeGetList === 'FILTER') {
                            listCalendar_task_NAs = await CALENDAR_TASK_NA_MODEL.getListByFilter({
                                region
                            });
                        } else {
                            listCalendar_task_NAs = await CALENDAR_TASK_NA_MODEL.getList({ region });
                        }

                        ChildRouter.renderToView(req, res, {
                            listCalendar_task_NAs: listCalendar_task_NAs.data || [],

                        });
                    }]
                },
            },

            /**
             * Function: Get List Calendar_task_NA By Field (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.GET_LIST_CALENDAR_TASK_NA_BY_FIELD]: {
                config: {
                    scopes: ['read:list_calendar_task_na'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách lịch làm việc NA',
                    code: CF_ROUTINGS_CALENDAR_TASK_NA.GET_LIST_CALENDAR_TASK_NA_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/calendar_task_na/list_calendar_task_nas.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            type
                        } = req.query;

                        let listCalendar_task_NAs = await CALENDAR_TASK_NA_MODEL.getListByFilter({
                            [field]: value,
                            region
                        });

                        ChildRouter.renderToView(req, res, {
                            listCalendar_task_NAs: listCalendar_task_NAs.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Calendar_task_NA Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.GET_LIST_CALENDAR_TASK_NA_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_calendar_task_na'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listCalendar_task_NAServerSide = await CALENDAR_TASK_NA_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(listCalendar_task_NAServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Calendar_task_NA Excel Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.GET_LIST_CALENDAR_TASK_NA_EXCEL]: {
                config: {
                    scopes: ['read:list_calendar_task_na'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = CALENDAR_TASK_NA_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl);
                    }]
                },
            },

            /**
             * Function: Download Employee Excel Export (API)
             * Date: 22/11/2021
             * Dev: Automatic
             */
             [CF_ROUTINGS_CALENDAR_TASK_NA.DOWNLOAD_LIST_CALENDAR_TASK_NA_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_calendar_task_na'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'calendar_task_NA'
                        });

                        let conditionObj = CALENDAR_TASK_NA_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listCalendarNA = await CALENDAR_TASK_NA_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listCalendarNA)
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.API_GET_INFO_CALENDAR_TASK_NA]: {
                config: {
                    scopes: ['read:info_calendar_task_na'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            calendar_task_naID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoCalendar_task_NA = await CALENDAR_TASK_NA_MODEL.getInfoCalendar_task_NA({
                            calendar_task_naID,
                            select,
                            explain
                        });
                        res.json(infoCalendar_task_NA);
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.API_GET_LIST_CALENDAR_TASK_NAS]: {
                config: {
                    scopes: ['read:list_calendar_task_na'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listCalendar_task_NAs = await CALENDAR_TASK_NA_MODEL.getListCalendar_task_NAs({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                            region
                        });
                        res.json(listCalendar_task_NAs);
                    }]
                },
            },

            [CF_ROUTINGS_CALENDAR_TASK_NA.API_INFO_EMPLOYEE_AT_STORE]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            storeID
                        } = req.params;

                        const listCalendar_task_NAs = await CALENDAR_TASK_NA_MODEL.getListEmployeesOfStore({
                            storeID
                        });
                        res.json(listCalendar_task_NAs);
                    }]
                },
        },

            /**
             * Function: Download template excel lịch làm việc của NA (API)
             * Date: 20/11/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.API_DOWNLOAD_CALENDAR_NA]: {
                config: {
                    scopes: ['read:list_calendar_task_na'],
                    type: 'json',
                },
                methods: {
                    post: [ async function(req, res) {
                        const { region } = req.user;
                        const { filter, condition } = req.body;

                        const infoDownload = await CALENDAR_TASK_NA_MODEL.downloadCalendarNA({
                            filter, condition, region
                        });
                        res.json(infoDownload);
                    }]
                },
            },

            /**
             * Function: Import excel lịch làm việc của NA (API)
             * Date: 30/10/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_CALENDAR_TASK_NA.API_IMPORT_CALENDAR_NA]: {
                config: {
                    scopes: ['create:calendar_task_na'],
                    type: 'json',
                },
                methods: {
                    post: [ uploadSingle, async function(req, res) {
                        const { region } = req.user;
                        const response = await CALENDAR_TASK_NA_MODEL.importCalendarNA({
                            fileInfo: req.file,
                            region
                        });
                        res.json(response);
                    }]
                },
            },

        }
    }
};