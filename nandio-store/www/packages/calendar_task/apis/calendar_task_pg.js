"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    PGCD_CALENDAR_TASK_PG_TYPE,
    PGM_CALENDAR_TASK_PG_TYPE,
    PGTC_CALENDAR_TASK_PG_TYPE,
    PGS_CALENDAR_TASK_PG_TYPE,
} = require('../constants/calendar_task_pg');
const {
    CF_ROUTINGS_CALENDAR_TASK_PG
} = require('../constants/calendar_task_pg/calendar_task_pg.uri');
const { uploadSingle } = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const CALENDAR_TASK_PG_MODEL = require('../models/calendar_task_pg').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */
const {
    STORE_COLL
} = require('../../store');
const {
    EMPLOYEE_COLL
} = require('../../employee');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ CALENDAR_TASK_PG  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Calendar_task_PG (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.ADD_CALENDAR_TASK_PG]: {
                config: {
                    scopes: ['create:calendar_task_pg'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm lịch làm việc PG',
                    code: CF_ROUTINGS_CALENDAR_TASK_PG.ADD_CALENDAR_TASK_PG,
                    inc: path.resolve(__dirname, '../views/calendar_task_pg/add_calendar_task_pg.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let condition = { state: 1, status: 1 };
                        region && (condition.region = region);

                        let listStores = await STORE_COLL.find(condition).limit(100).lean();
                        let listEmployees = await EMPLOYEE_COLL.find(condition)
                            .sort({
                                modifyAt: -1
                            })
                            .lean()

                        ChildRouter.renderToView(req, res, {
                            listStores,
                            listEmployees,
                            CF_ROUTINGS_CALENDAR_TASK_PG
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            store,
                            employee,
                            PGCD,
                            PGM,
                            PGTC,
                            PGS,
                            month,
                            year
                        } = req.body;

                        let infoAfterInsert = await CALENDAR_TASK_PG_MODEL.insert({
                            store,
                            employee,
                            PGCD,
                            PGM,
                            PGTC,
                            PGS,
                            month,
                            year,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Calendar_task_PG By Id (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.UPDATE_CALENDAR_TASK_PG_BY_ID]: {
                config: {
                    scopes: ['update:calendar_task_pg'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật lịch làm việc PG',
                    code: CF_ROUTINGS_CALENDAR_TASK_PG.UPDATE_CALENDAR_TASK_PG_BY_ID,
                    inc: path.resolve(__dirname, '../views/calendar_task_pg/update_calendar_task_pg.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            calendar_task_pgID
                        } = req.query;

                        let { region } = req.user;
                        let condition = { state: 1, status: 1 };
                        region && (condition.region = region);

                        let infoCalendar_task_PG = await CALENDAR_TASK_PG_MODEL.getInfoById(calendar_task_pgID);
                        if (infoCalendar_task_PG.error) {
                            return res.redirect('/something-went-wrong');
                        }

                        let listEmployees = await EMPLOYEE_COLL
                            .find(condition)
                            .sort({
                                modifyAt: -1
                            }).lean()

                        if(infoCalendar_task_PG.data.store){
                            condition._id = infoCalendar_task_PG.data.store._id;
                        }

                        let listStores = await STORE_COLL
                            .find(condition)
                            .sort({
                                modifyAt: -1
                            }).limit(100).lean()

                        ChildRouter.renderToView(req, res, {
                            infoCalendar_task_PG: infoCalendar_task_PG.data || {},
                            listStores,
                            listEmployees,
                            CF_ROUTINGS_CALENDAR_TASK_PG
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            calendar_task_pgID,
                            store,
                            employee,
                            PGCD,
                            PGM,
                            PGTC,
                            PGS,
                            month,
                            year,
                        } = req.body;

                        const infoAfterUpdate = await CALENDAR_TASK_PG_MODEL.update({
                            calendar_task_pgID,
                            store,
                            employee,
                            PGCD,
                            PGM,
                            PGTC,
                            PGS,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Calendar_task_PG By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.UPDATE_CALENDAR_TASK_PG_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:calendar_task_pg'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            calendar_task_pgID,
                            store,
                            employee,
                            PGCD,
                            PGM,
                            PGTC,
                            PGS,
                        } = req.body;

                        const infoAfterUpdate = await CALENDAR_TASK_PG_MODEL.updateNotRequire({
                            calendar_task_pgID,
                            store,
                            employee,
                            PGCD,
                            PGM,
                            PGTC,
                            PGS,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Calendar_task_PG By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.DELETE_CALENDAR_TASK_PG_BY_ID]: {
                config: {
                    scopes: ['delete:calendar_task_pg'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            calendar_task_pgID
                        } = req.params;

                        const infoAfterDelete = await CALENDAR_TASK_PG_MODEL.deleteById(calendar_task_pgID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Calendar_task_PG By List Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.DELETE_CALENDAR_TASK_PG_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:calendar_task_pg'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            calendar_task_pgID
                        } = req.body;

                        const infoAfterDelete = await CALENDAR_TASK_PG_MODEL.deleteByListId(calendar_task_pgID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Calendar_task_PG By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.GET_INFO_CALENDAR_TASK_PG_BY_ID]: {
                config: {
                    scopes: ['read:info_calendar_task_pg'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            calendar_task_pgID
                        } = req.params;

                        const infoCalendar_task_PGById = await CALENDAR_TASK_PG_MODEL.getInfoById(calendar_task_pgID);
                        res.json(infoCalendar_task_PGById);
                    }]
                },
            },

            /**
             * Function: Get List Calendar_task_PG (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.GET_LIST_CALENDAR_TASK_PG]: {
                config: {
                    scopes: ['read:list_calendar_task_pg'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách lịch làm việc PG',
                    code: CF_ROUTINGS_CALENDAR_TASK_PG.GET_LIST_CALENDAR_TASK_PG,
                    inc: path.resolve(__dirname, '../views/calendar_task_pg/list_calendar_task_pgs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let {
                            PGCD,
                            PGM,
                            PGTC,
                            PGS,
                            typeGetList
                        } = req.query;

                        let listCalendar_task_PGs = [];
                        if (typeGetList === 'FILTER') {
                            listCalendar_task_PGs = await CALENDAR_TASK_PG_MODEL.getListByFilter({
                                PGCD,
                                PGM,
                                PGTC,
                                PGS,
                                region
                            });
                        } else {
                            listCalendar_task_PGs = await CALENDAR_TASK_PG_MODEL.getList({ region });
                        }

                        ChildRouter.renderToView(req, res, {
                            listCalendar_task_PGs: listCalendar_task_PGs.data || [],
                            PGCD_CALENDAR_TASK_PG_TYPE,
                            PGM_CALENDAR_TASK_PG_TYPE,
                            PGTC_CALENDAR_TASK_PG_TYPE,
                            PGS_CALENDAR_TASK_PG_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Calendar_task_PG By Field (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.GET_LIST_CALENDAR_TASK_PG_BY_FIELD]: {
                config: {
                    scopes: ['read:list_calendar_task_pg'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách lịch làm việc PG',
                    code: CF_ROUTINGS_CALENDAR_TASK_PG.GET_LIST_CALENDAR_TASK_PG_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/calendar_task_pg/list_calendar_task_pgs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            PGCD,
                            PGM,
                            PGTC,
                            PGS,
                            type
                        } = req.query;

                        let listCalendar_task_PGs = await CALENDAR_TASK_PG_MODEL.getListByFilter({
                            PGCD,
                            PGM,
                            PGTC,
                            PGS,
                            [field]: value,
                            region
                        });

                        ChildRouter.renderToView(req, res, {
                            listCalendar_task_PGs: listCalendar_task_PGs.data || [],
                            PGCD_CALENDAR_TASK_PG_TYPE,
                            PGM_CALENDAR_TASK_PG_TYPE,
                            PGTC_CALENDAR_TASK_PG_TYPE,
                            PGS_CALENDAR_TASK_PG_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Calendar_task_PG Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.GET_LIST_CALENDAR_TASK_PGSERVER_SIDE]: {
                config: {
                    scopes: ['read:list_calendar_task_pg'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listCalendar_task_PGServerSide = await CALENDAR_TASK_PG_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(listCalendar_task_PGServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Calendar_task_PG Excel Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.GET_LIST_CALENDAR_TASK_PG_EXCEL]: {
                config: {
                    scopes: ['read:list_calendar_task_pg'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = CALENDAR_TASK_PG_MODEL.getConditionArrayFilterExcel(listItemExport)
                       
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Employee Excel Export (API)
             * Date: 22/11/2021
             * Dev: Automatic
             */
             [CF_ROUTINGS_CALENDAR_TASK_PG.DOWNLOAD_LIST_CALENDAR_TASK_PG_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_calendar_task_pg'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'calendar_task_PG'
                        });
                        
                        let conditionObj = CALENDAR_TASK_PG_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listCalendarPG = await CALENDAR_TASK_PG_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listCalendarPG)
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.API_GET_INFO_CALENDAR_TASK_PG]: {
                config: {
                    scopes: ['read:info_calendar_task_pg'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            calendar_task_pgID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoCalendar_task_PG = await CALENDAR_TASK_PG_MODEL.getInfoCalendar_task_PG({
                            calendar_task_pgID,
                            select,
                            explain
                        });
                        res.json(infoCalendar_task_PG);
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.API_GET_LIST_CALENDAR_TASK_PGS]: {
                config: {
                    scopes: ['read:list_calendar_task_pg'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listCalendar_task_PGs = await CALENDAR_TASK_PG_MODEL.getListCalendar_task_PGs({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                        });
                        res.json(listCalendar_task_PGs);
                    }]
                },
            },

            /**
             * Function: Download template excel lịch làm việc của PG (API)
             * Date: 20/11/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_CALENDAR_TASK_PG.API_DOWNLOAD_CALENDAR_PG]: {
                config: {
                    scopes: ['read:list_calendar_task_pg'],
                    type: 'json',
                },
                methods: {
                    post: [ async function(req, res) {
                        const { region } = req.user; 
                        const { filter, condition } = req.body;

                        const infoDownload = await CALENDAR_TASK_PG_MODEL.downloadCalendarPG({
                            filter, condition, region
                        });
                        res.json(infoDownload);
                    }]
                },
            },

            /**
             * Function: Import excel lịch làm việc của PG (API)
             * Date: 30/10/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_CALENDAR_TASK_PG.API_IMPORT_CALENDAR_PG]: {
                config: {
                    scopes: ['create:calendar_task_pg'],
                    type: 'json',
                },
                methods: {
                    post: [ uploadSingle, async function(req, res) {
                        const { region } = req.user; 

                        const infoAfterImport = await CALENDAR_TASK_PG_MODEL.importCalendarPG(req.file, region);
                        res.json(infoAfterImport);
                    }]
                },
            },

        }
    }
};