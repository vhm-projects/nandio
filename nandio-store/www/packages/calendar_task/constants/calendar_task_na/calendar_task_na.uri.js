const BASE_ROUTE = '/calendar_task_na';
const API_BASE_ROUTE = '/api/calendar_task_na';

const CF_ROUTINGS_CALENDAR_TASK_NA = {
    ADD_CALENDAR_TASK_NA: `${BASE_ROUTE}/add-calendar_task_na`,
    UPDATE_CALENDAR_TASK_NA_BY_ID: `${BASE_ROUTE}/update-calendar_task_na-by-id`,
    DELETE_CALENDAR_TASK_NA_BY_ID: `${BASE_ROUTE}/delete/:calendar_task_naID`,

    GET_INFO_CALENDAR_TASK_NA_BY_ID: `${BASE_ROUTE}/info/:calendar_task_naID`,
    GET_LIST_CALENDAR_TASK_NA: `${BASE_ROUTE}/list-calendar_task_na`,
    GET_LIST_CALENDAR_TASK_NA_BY_FIELD: `${BASE_ROUTE}/list-calendar_task_na/:field/:value`,
    GET_LIST_CALENDAR_TASK_NA_SERVER_SIDE: `${BASE_ROUTE}/list-calendar_task_na-server-side`,

    UPDATE_CALENDAR_TASK_NA_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-calendar_task_na-by-id-v2`,
    DELETE_CALENDAR_TASK_NA_BY_LIST_ID: `${BASE_ROUTE}/delete-calendar_task_na-by-list-id`,
    GET_LIST_CALENDAR_TASK_NA_EXCEL: `${BASE_ROUTE}/list-calendar_task_na-excel`,
    DOWNLOAD_LIST_CALENDAR_TASK_NA_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-calendar_task_na-excel-export`,

    API_GET_INFO_CALENDAR_TASK_NA: `${API_BASE_ROUTE}/info-calendar-task-na/:calendar_task_naID`,
    API_GET_LIST_CALENDAR_TASK_NAS: `${API_BASE_ROUTE}/list-calendar-task-nas`,
    API_IMPORT_CALENDAR_NA: `${API_BASE_ROUTE}/import-calendar-na`,
    API_DOWNLOAD_CALENDAR_NA: `${API_BASE_ROUTE}/download-calendar-na`,
    // API này dùng cho QSHOP để lấy ra employee nào có record gần nhất của 1 STORE
    API_INFO_EMPLOYEE_AT_STORE: `${API_BASE_ROUTE}/employees-of-store/:storeID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_CALENDAR_TASK_NA = CF_ROUTINGS_CALENDAR_TASK_NA;