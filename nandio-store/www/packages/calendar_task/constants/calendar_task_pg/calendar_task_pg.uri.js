const BASE_ROUTE = '/calendar_task_pg';
const API_BASE_ROUTE = '/api/calendar_task_pg';

const CF_ROUTINGS_CALENDAR_TASK_PG = {
    ADD_CALENDAR_TASK_PG: `${BASE_ROUTE}/add-calendar_task_pg`,
    UPDATE_CALENDAR_TASK_PG_BY_ID: `${BASE_ROUTE}/update-calendar_task_pg-by-id`,
    DELETE_CALENDAR_TASK_PG_BY_ID: `${BASE_ROUTE}/delete/:calendar_task_pgID`,

    GET_INFO_CALENDAR_TASK_PG_BY_ID: `${BASE_ROUTE}/info/:calendar_task_pgID`,
    GET_LIST_CALENDAR_TASK_PG: `${BASE_ROUTE}/list-calendar_task_pg`,
    GET_LIST_CALENDAR_TASK_PG_BY_FIELD: `${BASE_ROUTE}/list-calendar_task_pg/:field/:value`,
    GET_LIST_CALENDAR_TASK_PGSERVER_SIDE: `${BASE_ROUTE}/list-calendar_task_pg-server-side`,

    UPDATE_CALENDAR_TASK_PG_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-calendar_task_pg-by-id-v2`,
    DELETE_CALENDAR_TASK_PG_BY_LIST_ID: `${BASE_ROUTE}/delete-calendar_task_pg-by-list-id`,
    GET_LIST_CALENDAR_TASK_PG_EXCEL: `${BASE_ROUTE}/list-calendar_task_pg-excel`,
    DOWNLOAD_LIST_CALENDAR_TASK_PG_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-calendar_task_pg-excel-export`,

    API_GET_INFO_CALENDAR_TASK_PG: `${API_BASE_ROUTE}/info-calendar-task-pg/:calendar_task_pgID`,
    API_GET_LIST_CALENDAR_TASK_PGS: `${API_BASE_ROUTE}/list-calendar-task-pgs`,
    API_IMPORT_CALENDAR_PG: `${API_BASE_ROUTE}/import-calendar-pg`,
    API_DOWNLOAD_CALENDAR_PG: `${API_BASE_ROUTE}/download-calendar-pg`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_CALENDAR_TASK_PG = CF_ROUTINGS_CALENDAR_TASK_PG;