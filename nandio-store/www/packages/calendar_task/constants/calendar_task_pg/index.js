/**
 * PG cố định 
 * 1: Có
 * 2: Không
 */
exports.PGCD_CALENDAR_TASK_PG_TYPE = {

    "1": {
        value: "Có",
        color: "#0b51b7"
    },

    "2": {
        value: "Không",
        color: "#d63031"
    },

};

/**
 * PG Mobile 
 * 1: Có
 * 2: Không
 */
exports.PGM_CALENDAR_TASK_PG_TYPE = {

    "1": {
        value: "Có",
        color: "#0b51b7"
    },

    "2": {
        value: "Không",
        color: "#d63031"
    },

};

/**
 * PG tăng cường 
 * 1: Có
 * 2: Không
 */
exports.PGTC_CALENDAR_TASK_PG_TYPE = {

    "1": {
        value: "Có",
        color: "#0b51b7"
    },

    "2": {
        value: "Không",
        color: "#d63031"
    },

};

/**
 * PG Sampling 
 * 1: Có
 * 2: Không
 */
exports.PGS_CALENDAR_TASK_PG_TYPE = {

    "1": {
        value: "Có",
        color: "#0b51b7"
    },

    "2": {
        value: "Không",
        color: "#d63031"
    },

};