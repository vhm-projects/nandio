"use strict";

/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const ObjectID = require('mongoose').Types.ObjectId;
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const path = require('path');
const fs = require('fs');
const XlsxPopulate = require('xlsx-populate');

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    checkNumberIsValidWithRange
} = require('../../../utils/utils');
const timeUtils = require('../../../utils/time_utils');


/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const CALENDAR_TASK_PG_COLL = require('../databases/calendar_task_pg-coll');

const {
    STORE_COLL
} = require('../../store');

const {
    EMPLOYEE_COLL
} = require('../../employee');


class Model extends BaseModel {
    constructor() {
        super(CALENDAR_TASK_PG_COLL);
    }

    /**
         * Tạo mới calendar_task_pg
		* @param {object} store
		* @param {object} employee
		* @param {number} PGCD
		* @param {number} PGM
		* @param {number} PGTC
		* @param {number} PGS

         * @param {objectId} userCreate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        store,
        employee,
        PGCD = 2,
        PGM = 2,
        PGTC = 2,
        PGS = 2,
        month,
        year,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (!month) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tháng'
                    });
                }

                if (!year) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập năm'
                    });
                }


                if (PGCD && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: PGCD
                    })) {
                    return resolve({
                        error: true,
                        message: 'pg cố định không hợp lệ'
                    });
                }

                if (PGM && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: PGM
                    })) {
                    return resolve({
                        error: true,
                        message: 'pg mobile không hợp lệ'
                    });
                }

                if (PGTC && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: PGTC
                    })) {
                    return resolve({
                        error: true,
                        message: 'pg tăng cường không hợp lệ'
                    });
                }

                if (PGS && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: PGS
                    })) {
                    return resolve({
                        error: true,
                        message: 'pg sampling không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                let dataInsert = {
                    store,
                    employee,
                    PGCD,
                    PGM,
                    PGTC,
                    PGS,
                    month,
                    year,
                    userCreate
                };

                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo lịch làm việc PG thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật calendar_task_pg 
         * @param {objectId} calendar_task_pgID
		* @param {object} store
		* @param {object} employee
		* @param {number} PGCD
		* @param {number} PGM
		* @param {number} PGTC
		* @param {number} PGS

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        calendar_task_pgID,
        store,
        employee,
        PGCD,
        PGM,
        PGTC,
        PGS,
        month,
        year,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([calendar_task_pgID])) {
                    return resolve({
                        error: true,
                        message: 'calendar_task_pgID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (!month) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tháng cho calendar_task_na'
                    });
                }

                if (!year) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập năm cho calendar_task_na'
                    });
                }

                if (PGCD && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: PGCD
                    })) {
                    return resolve({
                        error: true,
                        message: 'pg cố định không hợp lệ'
                    });
                }

                if (PGM && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: PGM
                    })) {
                    return resolve({
                        error: true,
                        message: 'pg mobile không hợp lệ'
                    });
                }

                if (PGTC && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: PGTC
                    })) {
                    return resolve({
                        error: true,
                        message: 'pg tăng cường không hợp lệ'
                    });
                }

                if (PGS && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: PGS
                    })) {
                    return resolve({
                        error: true,
                        message: 'pg sampling không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                const checkExists = await CALENDAR_TASK_PG_COLL.findById(calendar_task_pgID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'lịch làm việc PG không tồn tại'
                    });
                }


                let dataUpdate = {
                    userUpdate
                };
                store && (dataUpdate.store = store);
                employee && (dataUpdate.employee = employee);
                month && (dataUpdate.month = month);
                year && (dataUpdate.year = year);
                dataUpdate.PGCD = PGCD;
                dataUpdate.PGM = PGM;
                dataUpdate.PGTC = PGTC;
                dataUpdate.PGS = PGS;

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: calendar_task_pgID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật calendar_task_pg (không bắt buộc)
         * @param {objectId} calendar_task_pgID
		* @param {object} store
		* @param {object} employee
		* @param {number} PGCD
		* @param {number} PGM
		* @param {number} PGTC
		* @param {number} PGS

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        calendar_task_pgID,
        store,
        employee,
        PGCD,
        PGM,
        PGTC,
        PGS,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([calendar_task_pgID])) {
                    return resolve({
                        error: true,
                        message: 'calendar_task_pgID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                if (PGCD && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: PGCD
                    })) {
                    return resolve({
                        error: true,
                        message: 'pg cố định không hợp lệ'
                    });
                }

                if (PGM && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: PGM
                    })) {
                    return resolve({
                        error: true,
                        message: 'pg mobile không hợp lệ'
                    });
                }

                if (PGTC && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: PGTC
                    })) {
                    return resolve({
                        error: true,
                        message: 'pg tăng cường không hợp lệ'
                    });
                }

                if (PGS && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: PGS
                    })) {
                    return resolve({
                        error: true,
                        message: 'pg sampling không hợp lệ'
                    });
                }

                const checkExists = await CALENDAR_TASK_PG_COLL.findById(calendar_task_pgID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'lịch làm việc PG không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                store && (dataUpdate.store = store);
                employee && (dataUpdate.employee = employee);
                PGCD && (dataUpdate.PGCD = PGCD);
                PGM && (dataUpdate.PGM = PGM);
                PGTC && (dataUpdate.PGTC = PGTC);
                PGS && (dataUpdate.PGS = PGS);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: calendar_task_pgID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa calendar_task_pg 
     * @param {objectId} calendar_task_pgID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(calendar_task_pgID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([calendar_task_pgID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị calendar_task_pgID không hợp lệ'
                    });
                }

                const infoAfterDelete = await this.updateById(calendar_task_pgID, {
                    state: 2
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa calendar_task_pg 
     * @param {array} calendar_task_pgID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(calendar_task_pgID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(calendar_task_pgID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị calendar_task_pgID không hợp lệ'
                    });
                }

                const infoAfterDelete = await CALENDAR_TASK_PG_COLL.updateMany({
                    _id: {
                        $in: calendar_task_pgID
                    }
                }, {
                    state: 2,
                    modifyAt: new Date()
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin calendar_task_pg 
     * @param {objectId} calendar_task_pgID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(calendar_task_pgID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([calendar_task_pgID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị calendar_task_pgID không hợp lệ'
                    });
                }

                const infoCalendar_task_PG = await CALENDAR_TASK_PG_COLL.findById(calendar_task_pgID)
                    .populate('store employee')

                if (!infoCalendar_task_PG) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin lịch làm việc PG'
                    });
                }

                return resolve({
                    error: false,
                    data: infoCalendar_task_PG
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách calendar_task_pg 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList({ region }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };
                region && (conditionObj['store.region'] = ObjectID(region));

                const listCalendar_task_PG = await CALENDAR_TASK_PG_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $sort: { modifyAt: -1 }
                    }
                ])

                if (!listCalendar_task_PG) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách lịch làm việc PG'
                    });
                }

                return resolve({
                    error: false,
                    data: listCalendar_task_PG
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách calendar_task_pg theo bộ lọc
		* @enum {number} PGCD
		* @enum {number} PGM
		* @enum {number} PGTC
		* @enum {number} PGS

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        PGCD,
        PGM,
        PGTC,
        PGS,
        region
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                PGCD && (conditionObj.PGCD = PGCD);

                PGM && (conditionObj.PGM = PGM);

                PGTC && (conditionObj.PGTC = PGTC);

                PGS && (conditionObj.PGS = PGS);

                region && (conditionObj['store.region'] = ObjectID(region));

                const listCalendar_task_PGByFilter = await CALENDAR_TASK_PG_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $sort: { modifyAt: -1 }
                    }
                ])

                if (!listCalendar_task_PGByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách lịch làm việc PG"
                    });
                }

                return resolve({
                    error: false,
                    data: listCalendar_task_PGByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách calendar_task_pg theo bộ lọc (server side)
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir,
        region
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.PGCD) {
                        if (![1, 2].includes(Number(objFilterStatic.PGCD)) || Number.isNaN(Number(objFilterStatic.PGCD))) {
                            return resolve({
                                error: true,
                                message: "pg cố định không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            PGCD: Number(objFilterStatic.PGCD)
                        }
                    }

                    if (objFilterStatic.PGM) {
                        if (![1, 2].includes(Number(objFilterStatic.PGM)) || Number.isNaN(Number(objFilterStatic.PGM))) {
                            return resolve({
                                error: true,
                                message: "pg mobile không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            PGM: Number(objFilterStatic.PGM)
                        }
                    }

                    if (objFilterStatic.PGTC) {
                        if (![1, 2].includes(Number(objFilterStatic.PGTC)) || Number.isNaN(Number(objFilterStatic.PGTC))) {
                            return resolve({
                                error: true,
                                message: "pg tăng cường không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            PGTC: Number(objFilterStatic.PGTC)
                        }
                    }

                    if (objFilterStatic.PGS) {
                        if (![1, 2].includes(Number(objFilterStatic.PGS)) || Number.isNaN(Number(objFilterStatic.PGS))) {
                            return resolve({
                                error: true,
                                message: "pg sampling không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            PGS: Number(objFilterStatic.PGS)
                        }
                    }

                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                region && (conditionObj['store.region'] = ObjectID(region));
                const skip = (page - 1) * limit;

                const [{ metadata, data: listCalendar_task_PGByFilter }] = await CALENDAR_TASK_PG_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $facet: {
                            metadata: [ { $count: "total" }, { $addFields: { page } } ],
                            data: [ { $sort: sort }, { $skip: +skip }, { $limit: +limit } ]
                        }
                    }
                ]).allowDiskUse(true);

                const totalCalendar_task_PG = metadata.length && metadata[0].total;

                if (!listCalendar_task_PGByFilter) {
                    return resolve({
                        recordsTotal: totalCalendar_task_PG,
                        recordsFiltered: totalCalendar_task_PG,
                        data: []
                    });
                }

                const listCalendar_task_PGDataTable = listCalendar_task_PGByFilter.map((calendar_task_pg, index) => {
                    let PGCD = '';
                    if (calendar_task_pg.PGCD == 1) {
                        PGCD = 'checked';
                    }

                    let PGM = '';
                    if (calendar_task_pg.PGM == 1) {
                        PGM = 'checked';
                    }

                    let PGTC = '';
                    if (calendar_task_pg.PGTC == 1) {
                        PGTC = 'checked';
                    }

                    let PGS = '';
                    if (calendar_task_pg.PGS == 1) {
                        PGS = 'checked';
                    }

                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${calendar_task_pg._id}" type="checkbox" class="check-record check-record-${calendar_task_pg._id}" _index ="${index + 1}"><label for="${calendar_task_pg._id}"></label></div></td>`,
                        store: `<a class="btn btn-outline-primary" style="padding: 2px 10px" href="/store/update-store-by-id?storeID=${calendar_task_pg.store && calendar_task_pg.store._id}"> ${calendar_task_pg.store ? calendar_task_pg.store.name : ""} </a>`,
                        employee: `<a class="btn btn-outline-primary" style="padding: 2px 10px" href="/employee/update-employee-by-id?employeeID=${calendar_task_pg.employee && calendar_task_pg.employee._id}"> ${calendar_task_pg.employee ? calendar_task_pg.employee.fullname : ""} </a>`,
                        PGCD: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-PGCD" _calendar_task_pgID="${calendar_task_pg._id}" type="checkbox" id="${calendar_task_pg._id}" ${PGCD} style="width: 40px;height: 20px;"></div></td>`,
                        PGM: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-PGM" _calendar_task_pgID="${calendar_task_pg._id}" type="checkbox" id="${calendar_task_pg._id}" ${PGM} style="width: 40px;height: 20px;"></div></td>`,
                        PGTC: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-PGTC" _calendar_task_pgID="${calendar_task_pg._id}" type="checkbox" id="${calendar_task_pg._id}" ${PGTC} style="width: 40px;height: 20px;"></div></td>`,
                        PGS: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-PGS" _calendar_task_pgID="${calendar_task_pg._id}" type="checkbox" id="${calendar_task_pg._id}" ${PGS} style="width: 40px;height: 20px;"></div></td>`,
                        month: calendar_task_pg.month,
                        year: calendar_task_pg.year,
                        createAt: moment(calendar_task_pg.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalCalendar_task_PG,
                    recordsFiltered: totalCalendar_task_PG,
                    data: listCalendar_task_PGDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc calendar_task_pg
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    // value = timeUtils.parseTimeUTC(value, true);

                    // let _fromDate = moment(value).startOf('day').format('Y-MM-DD HH:mm:sZ');
                    // let _toDate = moment(value).endOf('day').format('Y-MM-DD HH:mm:sZ');

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc calendar_task_pg
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listCalendar_task_PGByFilter = await CALENDAR_TASK_PG_COLL.aggregate(arrayFilter)

                
                if (!listCalendar_task_PGByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listCalendar_task_PGByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listCalendar_task_PGByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                              
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }
                                
                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                   
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc calendar_task_pg
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            dataEnum: item.dataEnum,
                            nameCollChoice: item.nameCollChoice,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                dataEnum: item.dataEnum,
                                nameCollChoice: item.nameCollChoice,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {

            if (filter.length > 1) {
                condition === 'OR' && (conditionObj.$or = []);
                condition === 'AND' && (conditionObj.$and = []);

                filter.map(filterObj => {
                    const getConditionByFilter = this.getConditionObj(filterObj);

                    if (condition === 'OR') {
                        conditionObj.$or.push(getConditionByFilter);
                    } else {
                        conditionObj.$and.push(getConditionByFilter);
                    }
                })
            } else {
                conditionObj = {
                    ...conditionObj,
                    ...this.getConditionObj(filter[0])
                };
            }
        }


        if (!isEmptyObject(objFilterStatic)) {

            if (objFilterStatic.PGCD) {
                if (![1, 2].includes(Number(objFilterStatic.PGCD)) || Number.isNaN(Number(objFilterStatic.PGCD))) {
                    return resolve({
                        error: true,
                        message: "pg cố định không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    PGCD: Number(objFilterStatic.PGCD)
                }
            }

            if (objFilterStatic.PGM) {
                if (![1, 2].includes(Number(objFilterStatic.PGM)) || Number.isNaN(Number(objFilterStatic.PGM))) {
                    return resolve({
                        error: true,
                        message: "pg mobile không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    PGM: Number(objFilterStatic.PGM)
                }
            }

            if (objFilterStatic.PGTC) {
                if (![1, 2].includes(Number(objFilterStatic.PGTC)) || Number.isNaN(Number(objFilterStatic.PGTC))) {
                    return resolve({
                        error: true,
                        message: "pg tăng cường không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    PGTC: Number(objFilterStatic.PGTC)
                }
            }

            if (objFilterStatic.PGS) {
                if (![1, 2].includes(Number(objFilterStatic.PGS)) || Number.isNaN(Number(objFilterStatic.PGS))) {
                    return resolve({
                        error: true,
                        message: "pg sampling không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    PGS: Number(objFilterStatic.PGS)
                }
            }

        }

        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let lookup = [{
                        $lookup: {
                            from: pluralize.plural(item.coll),
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: pluralize.plural(populate.coll),
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Chi tiết
     * @param {objectId} calendar_task_pgID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoCalendar_task_PG({
        calendar_task_pgID,
        select,
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];

                if (!checkObjectIDs([calendar_task_pgID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị calendar_task_pgID không hợp lệ',
                        status: 400
                    });
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['store', 'employee'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoCalendar_task_PG = await CALENDAR_TASK_PG_COLL
                    .findById(calendar_task_pgID)
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoCalendar_task_PG = await CALENDAR_TASK_PG_COLL.populate(infoCalendar_task_PG, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoCalendar_task_PG = await CALENDAR_TASK_PG_COLL.populate(infoCalendar_task_PG, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoCalendar_task_PG) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin calendar_task_pg',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoCalendar_task_PG,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Danh sách
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListCalendar_task_PGs({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { state: 1 };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }
                
                Object.keys(filter).map(key => {
                    if (!['store', 'employee', 'PGCD', 'PGM', 'PGTC', 'PGS'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    store,
                    employee,
                    PGCD,
                    PGM,
                    PGTC,
                    PGS,
                } = filter;

                store && (conditionObj.store = store);
                employee && (conditionObj.employee = employee);

                PGCD && (conditionObj.PGCD = PGCD);
                PGM && (conditionObj.PGM = PGM);
                PGTC && (conditionObj.PGTC = PGTC);
                PGS && (conditionObj.PGS = PGS);

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['store', 'employee'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listCalendar_task_PGs = await CALENDAR_TASK_PG_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listCalendar_task_PGs = await CALENDAR_TASK_PG_COLL.populate(listCalendar_task_PGs, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listCalendar_task_PGs = await CALENDAR_TASK_PG_COLL.populate(listCalendar_task_PGs, `${ref}.${field}`);
                        }
                    }
                }

                if (!listCalendar_task_PGs) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách calendar_task_pg',
                        status: 400
                    });
                }

                let totalRecord = await CALENDAR_TASK_PG_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listCalendar_task_PGs,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Download template excel lịch làm việc của PG
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     * }}
     */
    downloadCalendarPG({ filter, condition, region }){
        return new Promise(async resolve => {
            try {
                // Open the workbook.
                XlsxPopulate.fromFileAsync("files/excel/Template_calendar_PG.xlsx")
                .then(async workbook => {

                    let conditionObj = {
                        state: 1,
                        $or: []
                    };
    
                    if (filter && filter.length) {
                        if (filter.length > 1) {
    
                            filter.map(filterObj => {
                                if (filterObj.type === 'ref') {
                                    const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);
    
                                    if (condition === 'OR') {
                                        conditionObj.$or.push(conditionFieldRef);
                                    } else {
                                        conditionObj = {
                                            ...conditionObj,
                                            ...conditionFieldRef
                                        };
                                    }
                                } else {
                                    const conditionByFilter = this.getConditionObj(filterObj);
    
                                    if (condition === 'OR') {
                                        conditionObj.$or.push(conditionByFilter);
                                    } else {
                                        conditionObj = {
                                            ...conditionObj,
                                            ...conditionByFilter
                                        };
                                    }
                                }
                            });
    
                        } else {
                            let {
                                type,
                                ref,
                                fieldRefName
                            } = filter[0];
    
                            if (type === 'ref') {
                                conditionObj = {
                                    ...conditionObj,
                                    ...this.getConditionObj(ref, fieldRefName)
                                };
                            } else {
                                conditionObj = {
                                    ...conditionObj,
                                    ...this.getConditionObj(filter[0])
                                };
                            }
                        }
                    }

                    if (conditionObj.$or && !conditionObj.$or.length) {
                        delete conditionObj.$or;
                    }

                    region && (conditionObj['store.region'] = ObjectID(region));

                    const listCalendarFiltered = await CALENDAR_TASK_PG_COLL.aggregate([
                        {
                            $lookup: {
                                from: 'stores',
                                localField: 'store',
                                foreignField: '_id',
                                as: 'store'
                            }
                        },
                        {
                            $unwind: {
                                path: '$store',
                                preserveNullAndEmptyArrays: true
                            },
                        },
                        {
                            $lookup: {
                                from: 'employees',
                                localField: 'employee',
                                foreignField: '_id',
                                as: 'employee'
                            }
                        },
                        {
                            $unwind: {
                                path: '$employee',
                                preserveNullAndEmptyArrays: true
                            },
                        },
                        {
                            $match: conditionObj
                        },
                        {
                            $sort: { modifyAt: -1 }
                        }
                    ])

                    if(listCalendarFiltered && listCalendarFiltered.length){
                        let index = 2;
                        for (const infoCalendar of listCalendarFiltered) {
                            // Make edits.
                            workbook.sheet(0).cell(`A${index}`).value(`${infoCalendar.employee.code}`);
                            workbook.sheet(0).cell(`B${index}`).value(`${infoCalendar.store.code}`);
                            workbook.sheet(0).cell(`C${index}`).value(`${infoCalendar.PGCD === 2 ? 0: 1}`);
                            workbook.sheet(0).cell(`D${index}`).value(`${infoCalendar.PGM === 2 ? 0: 1}`);
                            workbook.sheet(0).cell(`E${index}`).value(`${infoCalendar.PGTC === 2 ? 0: 1}`);
                            workbook.sheet(0).cell(`F${index}`).value(`${infoCalendar.PGS === 2 ? 0: 1}`);
                            workbook.sheet(0).cell(`G${index}`).value(`${infoCalendar.month}`);
                            workbook.sheet(0).cell(`H${index}`).value(`${infoCalendar.year}`);

                            index++;
                        }
                    }

                    // Get the output
                    return workbook.outputAsync();
                })
                .then(data => resolve({ error: false, data, fileName: "Template_calendar_PG.xlsx" }))
                .catch(err => resolve({ error: true, message: err.message }));
            } catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Import excel lịch làm việc của PG
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     * }}
     */
    importCalendarPG(fileInfo, region) {
        return new Promise(async resolve => {
            try {
                XlsxPopulate.fromFileAsync(fileInfo.path)
                    .then(async workbook => {
                        let index = 2;
                        let listCalendarPG = [];
                        let dateDelete = [];
                        let messageStore = '';
                        let messageEmployee = '';
                        let messageExists = '';

                        for (;true;) {
                            let code        = workbook.sheet(0).cell(`A${index}`).value();
                            let storeCode   = workbook.sheet(0).cell(`B${index}`).value();
                            let PGCD       = workbook.sheet(0).cell(`C${index}`).value();
                            let PGM        = workbook.sheet(0).cell(`D${index}`).value();
                            let PGTC       = workbook.sheet(0).cell(`E${index}`).value();
                            let PGS        = workbook.sheet(0).cell(`F${index}`).value();
                            let month       = workbook.sheet(0).cell(`G${index}`).value();
                            let year        = workbook.sheet(0).cell(`H${index}`).value();

                            if(!code || !storeCode) break;

                            // Check month different
                            // if(index > 2){
                            //     let monthPrevious = workbook.sheet(0).cell(`G${index - 1}`).value();
                            //     let yearPrevious  = workbook.sheet(0).cell(`H${index - 1}`).value();

                            //     console.log({ monthPrevious, yearPrevious, month, year })

                            //     if(month !== monthPrevious || year !== yearPrevious){
                            //         listCalendarPG = [];
                            //         break;
                            //     }
                            // }

                            // if(!monthCalendar){
                            //     monthCalendar = month;
                            //     yearCalendar = year;
                            // }


                            const infoEmployee = await EMPLOYEE_COLL.findOne({
                                $or: [
                                    { code: code.toUpperCase().trim() },
                                    { code: code.toLowerCase().trim() },
                                ]
                            }).lean();

                            let condtion_store = {
                                $or: [
                                    { code: storeCode.toUpperCase().trim() },
                                    { code: storeCode.toLowerCase().trim() },
                                ],
                            }

                            region && (condtion_store.region = region);
                            const infoStore = await STORE_COLL.findOne({
                               ...condtion_store
                            }).lean();

                            if (region) {
                                region && (dateDelete[dateDelete.length] = { month, year, store: infoStore._id });
                            } else {
                                dateDelete[dateDelete.length] = { month, year };
                            }

                            if(!infoStore) {
                                messageStore += `${storeCode}, `;
                            }

                            if(!infoEmployee) {
                                messageEmployee += `${code}, `;
                            }

                            if(infoEmployee && infoStore){

                                let checkCalendarExist = listCalendarPG && listCalendarPG.length && listCalendarPG.filter(calendar => {
                                    if (calendar.store.toString() == infoStore._id.toString() && calendar.month == month && calendar.year == year) {
                                        return calendar;
                                    }
                                });

                                if (checkCalendarExist && checkCalendarExist.length) {
                                    messageExists = 'Dữ liệu đã bị trùng lặp, Mời bạn nhập lại';
                                    break;
                                }
                                listCalendarPG[listCalendarPG.length] = {
                                    store: infoStore._id,
                                    employee: infoEmployee._id,
                                    PGCD: +PGCD === 0 ? 2 : 1,
                                    PGM: +PGM === 0 ? 2 : 1,
                                    PGTC: +PGTC === 0 ? 2 : 1,
                                    PGS: +PGS === 0 ? 2 : 1,
                                    month,
                                    year,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                }
                            }

                            index++;
                        }

                        await fs.unlinkSync(fileInfo.path);
                        console.log({ LENGTH_PG: listCalendarPG.length })

                        if(messageStore) return resolve({ error: true, message: `Mã cửa hàng ${messageStore} không tồn tại` })
                        if(messageEmployee) return resolve({ error: true, message: `Mã nhân viên ${messageEmployee} không tồn tại` })
                        if(messageExists) return resolve({ error: true, message: messageExists })

                        if(!listCalendarPG.length) return resolve({ error: true, message: 'Import lịch làm việc thất bại' });

                        for (const { month, year, store } of dateDelete) {
                            if(month && year){
                                if (region) {
                                    await CALENDAR_TASK_PG_COLL.deleteMany({ store, month, year });
                                } else {
                                    await CALENDAR_TASK_PG_COLL.deleteMany({ month, year });
                                }
                                dateDelete = dateDelete.filter(date => date.month !== month && date.year !== year);
                            }
                        }

                        await CALENDAR_TASK_PG_COLL.insertMany(listCalendarPG);

                        return resolve({ error: false, message: 'Import lịch làm việc thành công' });
                    });

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

}

exports.MODEL = new Model;