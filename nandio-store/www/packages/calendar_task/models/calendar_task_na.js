"use strict";

/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const ObjectID = require('mongoose').Types.ObjectId;
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const path = require('path');
const fs = require('fs');
const XlsxPopulate = require('xlsx-populate');

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
} = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const CALENDAR_TASK_NA_COLL = require('../databases/calendar_task_na-coll');

const {
    STORE_COLL
} = require('../../store');

const {
    EMPLOYEE_COLL
} = require('../../employee');


class Model extends BaseModel {
    constructor() {
        super(CALENDAR_TASK_NA_COLL);
    }

    /**
         * Tạo mới calendar_task_na
		* @param {object} store
		* @param {object} employee
		* @param {number} month
		* @param {number} year

         * @param {objectId} userCreate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        store,
        employee,
        month,
        year,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (!month) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tháng'
                    });
                }

                if (!year) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập năm'
                    });
                }





                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }



                let dataInsert = {
                    store,
                    employee,
                    month,
                    year,
                    userCreate
                };

                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo lịch làm việc NA thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật calendar_task_na
         * @param {objectId} calendar_task_naID
		* @param {object} store
		* @param {object} employee
		* @param {number} month
		* @param {number} year

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        calendar_task_naID,
        store,
        employee,
        month,
        year,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([calendar_task_naID])) {
                    return resolve({
                        error: true,
                        message: 'calendar_task_naID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (!month) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tháng cho calendar_task_na'
                    });
                }

                if (!year) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập năm cho calendar_task_na'
                    });
                }





                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                const checkExists = await CALENDAR_TASK_NA_COLL.findById(calendar_task_naID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'lịch làm việc NA không tồn tại'
                    });
                }


                let dataUpdate = {
                    userUpdate
                };
                store && (dataUpdate.store = store);
                employee && (dataUpdate.employee = employee);
                month && (dataUpdate.month = month);
                year && (dataUpdate.year = year);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: calendar_task_naID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật calendar_task_na (không bắt buộc)
         * @param {objectId} calendar_task_naID
		* @param {object} store
		* @param {object} employee
		* @param {number} month
		* @param {number} year

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        calendar_task_naID,
        store,
        employee,
        month,
        year,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([calendar_task_naID])) {
                    return resolve({
                        error: true,
                        message: 'calendar_task_naID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                const checkExists = await CALENDAR_TASK_NA_COLL.findById(calendar_task_naID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'lịch làm việc NA không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                store && (dataUpdate.store = store);
                employee && (dataUpdate.employee = employee);
                month && (dataUpdate.month = month);
                year && (dataUpdate.year = year);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: calendar_task_naID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa calendar_task_na
     * @param {objectId} calendar_task_naID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(calendar_task_naID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([calendar_task_naID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị calendar_task_naID không hợp lệ'
                    });
                }

                const infoAfterDelete = await this.updateById(calendar_task_naID, {
                    state: 2
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa calendar_task_na
     * @param {array} calendar_task_naID
     * @extends {BaseModel}
     * @returns {{
     *      error: boolean,
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number },
     * }}
     */
    deleteByListId(calendar_task_naID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(calendar_task_naID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị calendar_task_naID không hợp lệ'
                    });
                }

                const infoAfterDelete = await CALENDAR_TASK_NA_COLL.updateMany({
                    _id: {
                        $in: calendar_task_naID
                    }
                }, {
                    state: 2,
                    modifyAt: new Date()
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin calendar_task_na
     * @param {objectId} calendar_task_naID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(calendar_task_naID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([calendar_task_naID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị calendar_task_naID không hợp lệ'
                    });
                }

                const infoCalendar_task_NA = await CALENDAR_TASK_NA_COLL.findById(calendar_task_naID)
                    .populate('store employee')

                if (!infoCalendar_task_NA) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin lịch làm việc NA'
                    });
                }

                return resolve({
                    error: false,
                    data: infoCalendar_task_NA
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách calendar_task_na
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList({ region }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };
                region && (conditionObj['store.region'] = ObjectID(region));

                const listCalendar_task_NA = await CALENDAR_TASK_NA_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $sort: { modifyAt: -1 }
                    }
                ])

                if (!listCalendar_task_NA) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách lịch làm việc NA'
                    });
                }

                return resolve({
                    error: false,
                    data: listCalendar_task_NA
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách calendar_task_na theo bộ lọc
		* @param {number} month
		* @param {number} year

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({ region }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };
                region && (conditionObj['store.region'] = ObjectID(region));

                const listCalendar_task_NAByFilter = await CALENDAR_TASK_NA_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $sort: { modifyAt: -1 }
                    }
                ])

                if (!listCalendar_task_NAByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách lịch làm việc NA"
                    });
                }

                return resolve({
                    error: false,
                    data: listCalendar_task_NAByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách calendar_task_na theo bộ lọc (server side)

     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition

     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        filter,
        condition,
        page,
        limit,
        field,
        dir,
        region
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                region && (conditionObj['store.region'] = ObjectID(region));
                const skip = (page - 1) * limit;

                const [{ metadata, data: listCalendar_task_NAByFilter }] = await CALENDAR_TASK_NA_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $facet: {
                            metadata: [ { $count: "total" }, { $addFields: { page } } ],
                            data: [ { $sort: sort }, { $skip: +skip }, { $limit: +limit } ]
                        }
                    }
                ]).allowDiskUse(true)

                const totalCalendar_task_NA = metadata.length && metadata[0].total;

                if (!listCalendar_task_NAByFilter) {
                    return resolve({
                        recordsTotal: totalCalendar_task_NA,
                        recordsFiltered: totalCalendar_task_NA,
                        data: []
                    });
                }

                const listCalendar_task_NADataTable = listCalendar_task_NAByFilter.map((calendar_task_na, index) => {
                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${calendar_task_na._id}" type="checkbox" class="check-record check-record-${calendar_task_na._id}" _index ="${index + 1}"><label for="${calendar_task_na._id}"></label></div></td>`,
                        store: `<a class="btn btn-outline-primary" style="padding: 2px 10px" href="/store/update-store-by-id?storeID=${calendar_task_na.store && calendar_task_na.store._id}"> ${calendar_task_na.store ? calendar_task_na.store.name : ""} </a>`,
                        employee: `<a class="btn btn-outline-primary" style="padding: 2px 10px" href="/employee/update-employee-by-id?employeeID=${calendar_task_na.employee && calendar_task_na.employee._id}"> ${calendar_task_na.employee ? calendar_task_na.employee.fullname : ""} </a>`,
                        month: `${calendar_task_na.month && calendar_task_na.month.length > 50 ? calendar_task_na.month.substr(0,50) + "..." : calendar_task_na.month}`,
                        year: `${calendar_task_na.year && calendar_task_na.year.length > 50 ? calendar_task_na.year.substr(0,50) + "..." : calendar_task_na.year}`,
                        createAt: moment(calendar_task_na.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalCalendar_task_NA,
                    recordsFiltered: totalCalendar_task_NA,
                    data: listCalendar_task_NADataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc calendar_task_na
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc calendar_task_na
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listCalendar_task_NAByFilter = await CALENDAR_TASK_NA_COLL.aggregate(arrayFilter)

                if (!listCalendar_task_NAByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listCalendar_task_NAByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listCalendar_task_NAByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc calendar_task_na
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
        };

        if(!listItemExport) {
            return {
                error: true,
                message: 'Giá trị calendar_task_naID không hợp lệ',
                status: 400
            };
        }

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            dataEnum: item.dataEnum,
                            nameCollChoice: item.nameCollChoice,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                dataEnum: item.dataEnum,
                                nameCollChoice: item.nameCollChoice,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {

            if (filter.length > 1) {
                condition === 'OR' && (conditionObj.$or = []);
                condition === 'AND' && (conditionObj.$and = []);

                filter.map(filterObj => {
                    const getConditionByFilter = this.getConditionObj(filterObj);

                    if (condition === 'OR') {
                        conditionObj.$or.push(getConditionByFilter);
                    } else {
                        conditionObj.$and.push(getConditionByFilter);
                    }
                })
            } else {
                conditionObj = {
                    ...conditionObj,
                    ...this.getConditionObj(filter[0])
                };
            }
        }

        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let lookup = [{
                        $lookup: {
                            from: pluralize.plural(item.coll),
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: pluralize.plural(populate.coll),
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Chi tiết
     * @param {objectId} calendar_task_naID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoCalendar_task_NA({
        calendar_task_naID,
        select,
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];

                if (!checkObjectIDs([calendar_task_naID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị calendar_task_naID không hợp lệ',
                        status: 400
                    });
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['store', 'employee'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoCalendar_task_NA = await CALENDAR_TASK_NA_COLL
                    .findById(calendar_task_naID)
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoCalendar_task_NA = await CALENDAR_TASK_NA_COLL.populate(infoCalendar_task_NA, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoCalendar_task_NA = await CALENDAR_TASK_NA_COLL.populate(infoCalendar_task_NA, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoCalendar_task_NA) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin calendar_task_na',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoCalendar_task_NA,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Danh sách
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{
     *   error: boolean,
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListCalendar_task_NAs({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page,
        region
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { state: 1 };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['store', 'employee', 'month', 'year'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    store,
                    employee,
                    month,
                    year
                } = filter;

                store && (conditionObj.store = store);
                month && (conditionObj.month = month);
                year && (conditionObj.year = year);
                employee && (conditionObj.employee = employee);

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['store', 'employee'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listCalendar_task_NAs = await CALENDAR_TASK_NA_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listCalendar_task_NAs = await CALENDAR_TASK_NA_COLL.populate(listCalendar_task_NAs, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listCalendar_task_NAs = await CALENDAR_TASK_NA_COLL.populate(listCalendar_task_NAs, `${ref}.${field}`);
                        }
                    }
                }

                if (!listCalendar_task_NAs) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách calendar_task_na',
                        status: 400
                    });
                }

                let totalRecord = await CALENDAR_TASK_NA_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listCalendar_task_NAs,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Download template excel lịch làm việc của NA
     * @extends {BaseModel}
     * @returns {{
     *      error: boolean,
     *      message?: string,
     * }}
     */
    downloadCalendarNA({ filter, condition, region }){
        return new Promise(async resolve => {
            try {
                // Open the workbook.
                XlsxPopulate.fromFileAsync("files/excel/Template_calendar_NA.xlsx")
                .then(async workbook => {

                    let conditionObj = {
                        state: 1,
                        $or: []
                    };

                    if (filter && filter.length) {
                        if (filter.length > 1) {

                            filter.map(filterObj => {
                                if (filterObj.type === 'ref') {
                                    const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                    if (condition === 'OR') {
                                        conditionObj.$or.push(conditionFieldRef);
                                    } else {
                                        conditionObj = {
                                            ...conditionObj,
                                            ...conditionFieldRef
                                        };
                                    }
                                } else {
                                    const conditionByFilter = this.getConditionObj(filterObj);

                                    if (condition === 'OR') {
                                        conditionObj.$or.push(conditionByFilter);
                                    } else {
                                        conditionObj = {
                                            ...conditionObj,
                                            ...conditionByFilter
                                        };
                                    }
                                }
                            });

                        } else {
                            let {
                                type,
                                ref,
                                fieldRefName
                            } = filter[0];

                            if (type === 'ref') {
                                conditionObj = {
                                    ...conditionObj,
                                    ...this.getConditionObj(ref, fieldRefName)
                                };
                            } else {
                                conditionObj = {
                                    ...conditionObj,
                                    ...this.getConditionObj(filter[0])
                                };
                            }
                        }
                    }

                    if (conditionObj.$or && !conditionObj.$or.length) {
                        delete conditionObj.$or;
                    }

                    region && (conditionObj['store.region'] = ObjectID(region));

                    const listCalendarFiltered = await CALENDAR_TASK_NA_COLL.aggregate([
                        {
                            $lookup: {
                                from: 'stores',
                                localField: 'store',
                                foreignField: '_id',
                                as: 'store'
                            }
                        },
                        {
                            $unwind: {
                                path: '$store',
                                preserveNullAndEmptyArrays: true
                            },
                        },
                        {
                            $lookup: {
                                from: 'employees',
                                localField: 'employee',
                                foreignField: '_id',
                                as: 'employee'
                            }
                        },
                        {
                            $unwind: {
                                path: '$employee',
                                preserveNullAndEmptyArrays: true
                            },
                        },
                        {
                            $match: conditionObj
                        },
                        {
                            $sort: { modifyAt: -1 }
                        }
                    ])

                    if(listCalendarFiltered && listCalendarFiltered.length){
                        let index = 2;
                        for (const infoCalendar of listCalendarFiltered) {
                            // Make edits.
                            workbook.sheet(0).cell(`A${index}`).value(`${infoCalendar.employee.code}`);
                            workbook.sheet(0).cell(`B${index}`).value(`${infoCalendar.store.code}`);
                            workbook.sheet(0).cell(`C${index}`).value(`${infoCalendar.month}`);
                            workbook.sheet(0).cell(`D${index}`).value(`${infoCalendar.year}`);

                            index++;
                        }
                    }

                    // Get the output
                    return workbook.outputAsync();
                })
                .then(data => resolve({ error: false, data, fileName: "Template_calendar_NA.xlsx" }))
                .catch(err => resolve({ error: true, message: err.message }));
            } catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Import excel lịch làm việc của NA
     * @extends {BaseModel}
     * @returns {{
     *      error: boolean,
     *      message?: string,
     * }}
     */
    importCalendarNA({ fileInfo, region }) {
        return new Promise(async resolve => {
            try {
                XlsxPopulate.fromFileAsync(fileInfo.path)
                    .then(async workbook => {
                        let index = 2;
                        let listCalendarNA = [];
                        let dateDelete = [];
                        let messageStore = '';
                        let messageEmployee = '';
                        // let listEmployeeCode = [];
                        // let listStoreCode = [];

                        // CHECK CODE EMPLOYEE IN VALID
                        // let arrayCodeInValid = [];
                        // let arrayStoreCodeInValid = [];
                        // let i = 0;
                        // let index2 = 2;

                        // for (;true;) {
                        //     let code        = workbook.sheet(0).cell(`A${index2}`).value();
                        //     let storeCode   = workbook.sheet(0).cell(`B${index2}`).value();
                        //     let month       = workbook.sheet(0).cell(`C${index2}`).value();
                        //     let year        = workbook.sheet(0).cell(`D${index2}`).value();

                        //     if(!code || !storeCode || !month || !year) break;

                        //     const infoEmployee = await EMPLOYEE_COLL.findOne({
                        //         $or: [
                        //             { code: code.toUpperCase().trim() },
                        //             { code: code.toLowerCase().trim() },
                        //         ]
                        //     }).lean();
                        //     if(!infoEmployee) {
                        //         console.log({
                        //             code,
                        //             i,
                        //         });
                        //         arrayCodeInValid = [
                        //             ...arrayCodeInValid,
                        //             code
                        //         ];
                        //     }
                        //     index2++;
                        //     i++;
                        // }
                        // console.log("---------------CHECK_CODE-VALID---------------");
                        // console.log({
                        //     arrayCodeInValid
                        // });
                        // console.log("---------------CHECK_CODE-VALID---------------");

                        for (;true;) {
                            let code        = workbook.sheet(0).cell(`A${index}`).value();
                            let storeCode   = workbook.sheet(0).cell(`B${index}`).value();
                            let month       = workbook.sheet(0).cell(`C${index}`).value();
                            let year        = workbook.sheet(0).cell(`D${index}`).value();

                            if(!code || !storeCode || !month || !year) break;

                            // Check month different
                            // if(index > 2){
                            //     let monthPrevious = workbook.sheet(0).cell(`C${index - 1}`).value();
                            //     let yearPrevious  = workbook.sheet(0).cell(`D${index - 1}`).value();

                            //     if(month !== monthPrevious || year !== yearPrevious){
                            //         listCalendarNA = [];
                            //         break;
                            //     }
                            // }

                            let conditionFindStore = {
                                $or: [
                                    { code: storeCode.toUpperCase().trim() },
                                    { code: storeCode.toLowerCase().trim() },
                                ],
                            }

                            dateDelete[dateDelete.length] = { month, year };

                            if(region && checkObjectIDs(region)) {
                                conditionFindStore.region = region;
                            }

                            const infoEmployee = await EMPLOYEE_COLL.findOne({
                                $or: [
                                    { code: code.toUpperCase().trim() },
                                    { code: code.toLowerCase().trim() },
                                ]
                            }).lean();

                            const infoStore = await STORE_COLL
                                .findOne(conditionFindStore)
                                .lean();

                            if(!infoStore) {
                                messageStore += `${storeCode}, `;
                            }

                            if(!infoEmployee) {
                                messageEmployee += `${code}, `;
                            }

                            if(infoEmployee && infoStore){
                                listCalendarNA[listCalendarNA.length] = {
                                    store: infoStore._id,
                                    employee: infoEmployee._id,
                                    month,
                                    year,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                }
                            }

                            index++;
                        }
                        console.log({ length: listCalendarNA.length })

                        await fs.unlinkSync(fileInfo.path);

                        if(messageStore) return resolve({ error: true, message: `Mã cửa hàng ${messageStore} không tồn tại` })
                        if(messageEmployee) return resolve({ error: true, message: `Mã nhân viên ${messageEmployee} không tồn tại` })

                        if(!listCalendarNA.length)
                            return resolve({ error: true, message: 'Import lịch làm việc thất bại' });

                        for (const { month, year } of dateDelete) {
                            if(month && year){
                                await CALENDAR_TASK_NA_COLL.deleteMany({ month, year });
                                dateDelete = dateDelete.filter(date => date.month !== month && date.year !== year);
                            }
                        }

                        await CALENDAR_TASK_NA_COLL.insertMany(listCalendarNA);

                        return resolve({ error: false, message: 'Import lịch làm việc thành công' });
                    });

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }
    
    getListEmployeesOfStore({
        storeID
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([storeID]))
                    return resolve({ error: true, message: 'storeID_invalid', status: 500 })

                let listEmployeesOfStore = await CALENDAR_TASK_NA_COLL.find({
                    store: storeID
                })
                .sort({ _id: -1 })
                .limit(3)
                .populate({
                    path: 'employee',
                    select: {
                        fullname: 1,
                        username: 1,
                        phone: 1,
                        email: 1
                    }
                })
                if (!listEmployeesOfStore)
                    return resolve({ error: true, message: 'cannot_get_list_employee_of_store', status: 500 })

                return resolve({
                    error: false,
                    data: listEmployeesOfStore,
                    status: 200
                });
            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

}

exports.MODEL = new Model;