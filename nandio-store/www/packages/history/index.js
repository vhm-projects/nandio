const SALEOUT_COLL  = require('./databases/saleout-coll');
const SALEOUT_MODEL  = require('./models/saleout').MODEL;
const SALEOUT_ROUTES  = require('./apis/saleout');
    

const HISTORY_INIT_CUSTOMER_COLL  = require('./databases/history_init_customer-coll');
const HISTORY_INIT_CUSTOMER_MODEL  = require('./models/history_init_customer').MODEL;
const HISTORY_INIT_CUSTOMER_ROUTES  = require('./apis/history_init_customer');
    

const HISTORY_EXCHANGE_GIFT_COLL  = require('./databases/history_exchange_gift-coll');
const HISTORY_EXCHANGE_GIFT_MODEL  = require('./models/history_exchange_gift').MODEL;
const HISTORY_EXCHANGE_GIFT_ROUTES  = require('./apis/history_exchange_gift');
    

const HISTORY_EXCHANGE_SAMPLING_COLL  = require('./databases/history_exchange_sampling-coll');
const HISTORY_EXCHANGE_SAMPLING_MODEL  = require('./models/history_exchange_sampling').MODEL;
const HISTORY_EXCHANGE_SAMPLING_ROUTES  = require('./apis/history_exchange_sampling');
    
// MARK REQUIRE

module.exports = {
    SALEOUT_COLL,
    SALEOUT_MODEL,
    SALEOUT_ROUTES,
    

    HISTORY_INIT_CUSTOMER_COLL,
    HISTORY_INIT_CUSTOMER_MODEL,
    HISTORY_INIT_CUSTOMER_ROUTES,
    

    HISTORY_EXCHANGE_GIFT_COLL,
    HISTORY_EXCHANGE_GIFT_MODEL,
    HISTORY_EXCHANGE_GIFT_ROUTES,
    

    HISTORY_EXCHANGE_SAMPLING_COLL,
    HISTORY_EXCHANGE_SAMPLING_MODEL,
    HISTORY_EXCHANGE_SAMPLING_ROUTES,
    
    // MARK EXPORT
}