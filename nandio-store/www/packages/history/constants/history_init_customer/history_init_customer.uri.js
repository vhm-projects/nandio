const BASE_ROUTE = '/history_init_customer';
const API_BASE_ROUTE = '/api/history_init_customer';

const CF_ROUTINGS_HISTORY_INIT_CUSTOMER = {
    ADD_HISTORY_INIT_CUSTOMER: `${BASE_ROUTE}/add-history_init_customer`,
    UPDATE_HISTORY_INIT_CUSTOMER_BY_ID: `${BASE_ROUTE}/update-history_init_customer-by-id`,
    DELETE_HISTORY_INIT_CUSTOMER_BY_ID: `${BASE_ROUTE}/delete/:history_init_customerID`,

    GET_INFO_HISTORY_INIT_CUSTOMER_BY_ID: `${BASE_ROUTE}/info/:history_init_customerID`,
    GET_LIST_HISTORY_INIT_CUSTOMER: `${BASE_ROUTE}/list-history_init_customer`,
    GET_LIST_HISTORY_INIT_CUSTOMER_BY_FIELD: `${BASE_ROUTE}/list-history_init_customer/:field/:value`,
    GET_LIST_HISTORY_INIT_CUSTOMER_SERVER_SIDE: `${BASE_ROUTE}/list-history_init_customer-server-side`,

    UPDATE_HISTORY_INIT_CUSTOMER_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-history_init_customer-by-id-v2`,
    DELETE_HISTORY_INIT_CUSTOMER_BY_LIST_ID: `${BASE_ROUTE}/delete-history_init_customer-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_HISTORY_INIT_CUSTOMER_EXCEL: `${BASE_ROUTE}/list-history_init_customer-excel`,
    DOWNLOAD_LIST_HISTORY_INIT_CUSTOMER_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-history_init_customer-excel-export`,
    DOWNLOAD_LIST_HISTORY_INIT_CUSTOMER_EXCEL_EXPORT_MANIUAL: `${BASE_ROUTE}/dowload-history_init_customer-excel-export-maniual`,

    // IMPORT EXCEL
    GET_LIST_HISTORY_INIT_CUSTOMER_IMPORT: `${BASE_ROUTE}/list-history_init_customer-import`,
    SETTING_FILE_HISTORY_INIT_CUSTOMER_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-history_init_customer-import-setting`,
    DOWNLOAD_FILE_HISTORY_INIT_CUSTOMER_EXCEL_IMPORT: `${BASE_ROUTE}/list-history_init_customer-import-dowload`,
    CREATE_HISTORY_INIT_CUSTOMER_IMPORT_EXCEL: `${BASE_ROUTE}/create-history_init_customer-import-excel`,

    API_ADD_HISTORY_INIT_CUSTOMER: `${API_BASE_ROUTE}/add-history_init_customer`,
    API_UPDATE_HISTORY_INIT_CUSTOMER: `${API_BASE_ROUTE}/update-history_init_customer/:history_init_customerID`,
    API_DELETE_HISTORY_INIT_CUSTOMER: `${API_BASE_ROUTE}/delete-history_init_customer/:history_init_customerID`,
    API_GET_INFO_HISTORY_INIT_CUSTOMER: `${API_BASE_ROUTE}/info-history_init_customer/:history_init_customerID`,
    API_GET_LIST_HISTORY_INIT_CUSTOMERS: `${API_BASE_ROUTE}/list-history_init_customers`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_HISTORY_INIT_CUSTOMER = CF_ROUTINGS_HISTORY_INIT_CUSTOMER;