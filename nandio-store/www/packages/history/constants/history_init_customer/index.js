/**
 * Loại thết bị 
 * ANDROID: ANDROID
 * IOS: IOS
 */
exports.DEVICETYPE_HISTORY_INIT_CUSTOMER_TYPE = {

    "ANDROID": {
        value: "ANDROID",
        color: "#0b51b7"
    },

    "IOS": {
        value: "IOS",
        color: "#d63031"
    },

};