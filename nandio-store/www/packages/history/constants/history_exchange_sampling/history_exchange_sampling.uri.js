const BASE_ROUTE = '/history_exchange_sampling';
const API_BASE_ROUTE = '/api/history_exchange_sampling';

const CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING = {
    ADD_HISTORY_EXCHANGE_SAMPLING: `${BASE_ROUTE}/add-history_exchange_sampling`,
    UPDATE_HISTORY_EXCHANGE_SAMPLING_BY_ID: `${BASE_ROUTE}/update-history_exchange_sampling-by-id`,
    DELETE_HISTORY_EXCHANGE_SAMPLING_BY_ID: `${BASE_ROUTE}/delete/:history_exchange_samplingID`,

    GET_INFO_HISTORY_EXCHANGE_SAMPLING_BY_ID: `${BASE_ROUTE}/info/:history_exchange_samplingID`,
    GET_LIST_HISTORY_EXCHANGE_SAMPLING: `${BASE_ROUTE}/list-history_exchange_sampling`,
    GET_LIST_HISTORY_EXCHANGE_SAMPLING_BY_FIELD: `${BASE_ROUTE}/list-history_exchange_sampling/:field/:value`,
    GET_LIST_HISTORY_EXCHANGE_SAMPLING_SERVER_SIDE: `${BASE_ROUTE}/list-history_exchange_sampling-server-side`,

    UPDATE_HISTORY_EXCHANGE_SAMPLING_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-history_exchange_sampling-by-id-v2`,
    DELETE_HISTORY_EXCHANGE_SAMPLING_BY_LIST_ID: `${BASE_ROUTE}/delete-history_exchange_sampling-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_HISTORY_EXCHANGE_SAMPLING_EXCEL: `${BASE_ROUTE}/list-history_exchange_sampling-excel`,
    DOWNLOAD_LIST_HISTORY_EXCHANGE_SAMPLING_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-history_exchange_sampling-excel-export`,
    DOWNLOAD_LIST_HISTORY_EXCHANGE_SAMPLING_EXCEL_EXPORT_MANIUAL: `${BASE_ROUTE}/dowload-excel-maniual`,

    // IMPORT EXCEL
    GET_LIST_HISTORY_EXCHANGE_SAMPLING_IMPORT: `${BASE_ROUTE}/list-history_exchange_sampling-import`,
    SETTING_FILE_HISTORY_EXCHANGE_SAMPLING_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-history_exchange_sampling-import-setting`,
    DOWNLOAD_FILE_HISTORY_EXCHANGE_SAMPLING_EXCEL_IMPORT: `${BASE_ROUTE}/list-history_exchange_sampling-import-dowload`,
    CREATE_HISTORY_EXCHANGE_SAMPLING_IMPORT_EXCEL: `${BASE_ROUTE}/create-history_exchange_sampling-import-excel`,

    API_GET_INFO_HISTORY_EXCHANGE_SAMPLING: `${API_BASE_ROUTE}/info-history_exchange_sampling/:history_exchange_samplingID`,
    API_GET_LIST_HISTORY_EXCHANGE_SAMPLINGS: `${API_BASE_ROUTE}/list-history_exchange_samplings`,
    API_ADD_HISTORY_EXCHANGE_SAMPLING: `${API_BASE_ROUTE}/add-history_exchange_sampling`,
    API_UPDATE_HISTORY_EXCHANGE_SAMPLING: `${API_BASE_ROUTE}/update-history_exchange_sampling/:history_exchange_samplingID`,
    API_DELETE_HISTORY_EXCHANGE_SAMPLING: `${API_BASE_ROUTE}/delete-history_exchange_sampling/:history_exchange_samplingID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING = CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING;