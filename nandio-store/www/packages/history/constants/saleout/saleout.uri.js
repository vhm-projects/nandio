const BASE_ROUTE = '/saleout';
const API_BASE_ROUTE = '/api/saleout';

const CF_ROUTINGS_SALEOUT = {
    ADD_SALEOUT: `${BASE_ROUTE}/add-saleout`,
    UPDATE_SALEOUT_BY_ID: `${BASE_ROUTE}/update-saleout-by-id`,
    DELETE_SALEOUT_BY_ID: `${BASE_ROUTE}/delete/:saleoutID`,

    GET_INFO_SALEOUT_BY_ID: `${BASE_ROUTE}/info/:saleoutID`,
    GET_LIST_SALEOUT: `${BASE_ROUTE}/list-saleout`,
    GET_LIST_SALEOUT_BY_FIELD: `${BASE_ROUTE}/list-saleout/:field/:value`,
    GET_LIST_SALEOUT_SERVER_SIDE: `${BASE_ROUTE}/list-saleout-server-side`,

    UPDATE_SALEOUT_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-saleout-by-id-v2`,
    DELETE_SALEOUT_BY_LIST_ID: `${BASE_ROUTE}/delete-saleout-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_SALEOUT_EXCEL: `${BASE_ROUTE}/list-saleout-excel`,
    DOWNLOAD_LIST_SALEOUT_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-saleout-excel-export`,
    DOWNLOAD_SALEOUT_EXCEL: `${BASE_ROUTE}/dowload-saleout-excel`,

    // IMPORT EXCEL
    GET_LIST_SALEOUT_IMPORT: `${BASE_ROUTE}/list-saleout-import`,
    SETTING_FILE_SALEOUT_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-saleout-import-setting`,
    DOWNLOAD_FILE_SALEOUT_EXCEL_IMPORT: `${BASE_ROUTE}/list-saleout-import-dowload`,
    CREATE_SALEOUT_IMPORT_EXCEL: `${BASE_ROUTE}/create-saleout-import-excel`,

    API_ADD_SALEOUT: `${API_BASE_ROUTE}/add-saleout`,
    API_UPDATE_SALEOUT: `${API_BASE_ROUTE}/update-saleout/:saleoutID`,
    API_DELETE_SALEOUT: `${API_BASE_ROUTE}/delete-saleout/:saleoutID`,
    API_GET_INFO_SALEOUT: `${API_BASE_ROUTE}/info-saleout/:saleoutID`,
    API_GET_LIST_SALEOUTS: `${API_BASE_ROUTE}/list-saleouts`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_SALEOUT = CF_ROUTINGS_SALEOUT;