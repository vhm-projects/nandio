/**
 * Loại thiết bị 
 * ANDROID: ANDROID
 * IOS: IOS
 */
exports.DEVICETYPE_SALEOUT_TYPE = {

    "ANDROID": {
        value: "ANDROID",
        color: "#0b51b7"
    },

    "IOS": {
        value: "IOS",
        color: "#d63031"
    },

};