const BASE_ROUTE = '/history_exchange_gift';
const API_BASE_ROUTE = '/api/history_exchange_gift';

const CF_ROUTINGS_HISTORY_EXCHANGE_GIFT = {
    ADD_HISTORY_EXCHANGE_GIFT: `${BASE_ROUTE}/add-history_exchange_gift`,
    UPDATE_HISTORY_EXCHANGE_GIFT_BY_ID: `${BASE_ROUTE}/update-history_exchange_gift-by-id`,
    DELETE_HISTORY_EXCHANGE_GIFT_BY_ID: `${BASE_ROUTE}/delete/:history_exchange_giftID`,

    GET_INFO_HISTORY_EXCHANGE_GIFT_BY_ID: `${BASE_ROUTE}/info/:history_exchange_giftID`,
    GET_LIST_HISTORY_EXCHANGE_GIFT: `${BASE_ROUTE}/list-history_exchange_gift`,
    GET_LIST_HISTORY_EXCHANGE_GIFT_BY_FIELD: `${BASE_ROUTE}/list-history_exchange_gift/:field/:value`,
    GET_LIST_HISTORY_EXCHANGE_GIFT_SERVER_SIDE: `${BASE_ROUTE}/list-history_exchange_gift-server-side`,

    UPDATE_HISTORY_EXCHANGE_GIFT_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-history_exchange_gift-by-id-v2`,
    DELETE_HISTORY_EXCHANGE_GIFT_BY_LIST_ID: `${BASE_ROUTE}/delete-history_exchange_gift-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_HISTORY_EXCHANGE_GIFT_EXCEL: `${BASE_ROUTE}/list-history_exchange_gift-excel`,
    DOWNLOAD_LIST_HISTORY_EXCHANGE_GIFT_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-history_exchange_gift-excel-export`,
    DOWNLOAD_HISTORY_EXCHANGE_GIFT_EXCEL: `${BASE_ROUTE}/dowload-history_exchange_gift-excel`,

    // IMPORT EXCEL
    GET_LIST_HISTORY_EXCHANGE_GIFT_IMPORT: `${BASE_ROUTE}/list-history_exchange_gift-import`,
    SETTING_FILE_HISTORY_EXCHANGE_GIFT_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-history_exchange_gift-import-setting`,
    DOWNLOAD_FILE_HISTORY_EXCHANGE_GIFT_EXCEL_IMPORT: `${BASE_ROUTE}/list-history_exchange_gift-import-dowload`,
    CREATE_HISTORY_EXCHANGE_GIFT_IMPORT_EXCEL: `${BASE_ROUTE}/create-history_exchange_gift-import-excel`,

    API_GET_INFO_HISTORY_EXCHANGE_GIFT: `${API_BASE_ROUTE}/info-history_exchange_gift/:history_exchange_giftID`,
    API_GET_LIST_HISTORY_EXCHANGE_GIFTS: `${API_BASE_ROUTE}/list-history_exchange_gifts`,
    API_ADD_HISTORY_EXCHANGE_GIFT: `${API_BASE_ROUTE}/add-history_exchange_gift`,
    API_UPDATE_HISTORY_EXCHANGE_GIFT: `${API_BASE_ROUTE}/update-history_exchange_gift/:history_exchange_giftID`,
    API_DELETE_HISTORY_EXCHANGE_GIFT: `${API_BASE_ROUTE}/delete-history_exchange_gift/:history_exchange_giftID`,
    
    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_HISTORY_EXCHANGE_GIFT = CF_ROUTINGS_HISTORY_EXCHANGE_GIFT;