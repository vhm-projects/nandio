"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    DEVICETYPE_HISTORY_INIT_CUSTOMER_TYPE,
} = require('../constants/history_init_customer');
const {
    CF_ROUTINGS_HISTORY_INIT_CUSTOMER
} = require('../constants/history_init_customer/history_init_customer.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const HISTORY_INIT_CUSTOMER_MODEL = require('../models/history_init_customer').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const {
    STORE_COLL
} = require('../../store');

const
    EMPLOYEE_COLL
 = require('../../employee/databases/employee-coll');

const {
    CUSTOMER_COLL
} = require('../../customer');

const
    CHECKIN_CHECKOUT_COLL
= require('../../checkin_checkout/databases/checkin_checkout-coll');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ HISTORY_INIT_CUSTOMER  ===============================
             * =============================== ************* ===============================
             */


            /**
             * Function: Get List area By parent (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            '/history_init_customer/list-area-by-parent': {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            region
                        } = req.query;

                        const listAreaByParent = await AREA_COLL
                            .find({
                                region
                            })
                            .lean();
                        res.json(listAreaByParent);
                    }]
                },
            },

            /**
             * Function: Get List distributor By parent (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            '/history_init_customer/list-distributor-by-parent': {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            area
                        } = req.query;

                        const listDistributorByParent = await DISTRIBUTOR_COLL
                            .find({
                                area
                            })
                            .lean();
                        res.json(listDistributorByParent);
                    }]
                },
            },

            /**
             * Function: Get List store By parent (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            '/history_init_customer/list-store-by-parent': {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            distributor
                        } = req.query;

                        const listStoreByParent = await STORE_COLL
                            .find({
                                distributor
                            })
                            .lean();
                        res.json(listStoreByParent);
                    }]
                },
            },



            /**
             * Function: Insert History_init_customer (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.ADD_HISTORY_INIT_CUSTOMER]: {
                config: {
                    scopes: ['create:history_init_customer'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm History_init_customer',
                    code: CF_ROUTINGS_HISTORY_INIT_CUSTOMER.ADD_HISTORY_INIT_CUSTOMER,
                    inc: path.resolve(__dirname, '../views/history_init_customer/add_history_init_customer.ejs')
                },
                methods: {
                    get: [async function(req, res) {


                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_HISTORY_INIT_CUSTOMER
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            store,
                            employee,
                            customer,
                        } = req.body;


                        let infoAfterInsert = await HISTORY_INIT_CUSTOMER_MODEL.insert({
                            store,
                            employee,
                            customer,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update History_init_customer By Id (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.UPDATE_HISTORY_INIT_CUSTOMER_BY_ID]: {
                config: {
                    scopes: ['update:history_init_customer'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật History_init_customer',
                    code: CF_ROUTINGS_HISTORY_INIT_CUSTOMER.UPDATE_HISTORY_INIT_CUSTOMER_BY_ID,
                    inc: path.resolve(__dirname, '../views/history_init_customer/update_history_init_customer.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            history_init_customerID
                        } = req.query;

                        let infoHistory_init_customer = await HISTORY_INIT_CUSTOMER_MODEL.getInfoById(history_init_customerID);
                        if (infoHistory_init_customer.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionStore = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_init_customer.data.store) {

                            conditionStore._id = infoHistory_init_customer.data.store._id;

                        }

                        let listStores = await STORE_COLL
                            .find(conditionStore)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionEmployee = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_init_customer.data.employee) {

                            conditionEmployee._id = infoHistory_init_customer.data.employee._id;

                        }

                        let listEmployees = await EMPLOYEE_COLL
                            .find(conditionEmployee)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionCustomer = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_init_customer.data.customer) {

                            conditionCustomer._id = infoHistory_init_customer.data.customer._id;

                        }

                        let listCustomers = await CUSTOMER_COLL
                            .find(conditionCustomer)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionCheckin = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_init_customer.data.checkin) {

                            conditionCheckin._id = infoHistory_init_customer.data.checkin._id;

                        }

                        let listCheckins = await CHECKIN_CHECKOUT_COLL
                            .find(conditionCheckin)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoHistory_init_customer: infoHistory_init_customer.data || {},

                            listStores,
                            listEmployees,
                            listCustomers,
                            listCheckins,
                            CF_ROUTINGS_HISTORY_INIT_CUSTOMER
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            history_init_customerID,
                            store,
                            employee,
                            customer,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        const infoAfterUpdate = await HISTORY_INIT_CUSTOMER_MODEL.update({
                            history_init_customerID,
                            store,
                            employee,
                            customer,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require History_init_customer By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.UPDATE_HISTORY_INIT_CUSTOMER_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:history_init_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            history_init_customerID,
                            store,
                            employee,
                            customer,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        const infoAfterUpdate = await HISTORY_INIT_CUSTOMER_MODEL.updateNotRequire({
                            history_init_customerID,
                            store,
                            employee,
                            customer,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete History_init_customer By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.DELETE_HISTORY_INIT_CUSTOMER_BY_ID]: {
                config: {
                    scopes: ['delete:history_init_customer'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            history_init_customerID
                        } = req.params;

                        const infoAfterDelete = await HISTORY_INIT_CUSTOMER_MODEL.deleteById(history_init_customerID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete History_init_customer By List Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.DELETE_HISTORY_INIT_CUSTOMER_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:history_init_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            history_init_customerID
                        } = req.body;

                        const infoAfterDelete = await HISTORY_INIT_CUSTOMER_MODEL.deleteByListId(history_init_customerID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info History_init_customer By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.GET_INFO_HISTORY_INIT_CUSTOMER_BY_ID]: {
                config: {
                    scopes: ['read:info_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            history_init_customerID
                        } = req.params;

                        const infoHistory_init_customerById = await HISTORY_INIT_CUSTOMER_MODEL.getInfoById(history_init_customerID);
                        res.json(infoHistory_init_customerById);
                    }]
                },
            },

            /**
             * Function: Get List History_init_customer (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.GET_LIST_HISTORY_INIT_CUSTOMER]: {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách lịch sử tạo khách hàng',
                    code: CF_ROUTINGS_HISTORY_INIT_CUSTOMER.GET_LIST_HISTORY_INIT_CUSTOMER,
                    inc: path.resolve(__dirname, '../views/history_init_customer/list_history_init_customers.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            typeGetList
                        } = req.query;

                        let listHistory_init_customers = [];
                        if (typeGetList === 'FILTER') {
                            listHistory_init_customers = await HISTORY_INIT_CUSTOMER_MODEL.getListByFilter({
                                keyword,
                            });
                        } else {
                            listHistory_init_customers = await HISTORY_INIT_CUSTOMER_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listHistory_init_customers: listHistory_init_customers.data || [],
                            DEVICETYPE_HISTORY_INIT_CUSTOMER_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List History_init_customer By Field (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.GET_LIST_HISTORY_INIT_CUSTOMER_BY_FIELD]: {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách History_init_customer by field isStatus',
                    code: CF_ROUTINGS_HISTORY_INIT_CUSTOMER.GET_LIST_HISTORY_INIT_CUSTOMER_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/history_init_customer/list_history_init_customers.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            type
                        } = req.query;

                        let listHistory_init_customers = await HISTORY_INIT_CUSTOMER_MODEL.getListByFilter({
                            keyword,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listHistory_init_customers: listHistory_init_customers.data || [],
                            DEVICETYPE_HISTORY_INIT_CUSTOMER_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List History_init_customer Server Side (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.GET_LIST_HISTORY_INIT_CUSTOMER_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listHistory_init_customerServerSide = await HISTORY_INIT_CUSTOMER_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(listHistory_init_customerServerSide);
                    }]
                },
            },

            /**
             * Function: Get List History_init_customer Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.GET_LIST_HISTORY_INIT_CUSTOMER_IMPORT]: {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listHistory_init_customerImport = await HISTORY_INIT_CUSTOMER_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listHistory_init_customerImport);
                    }]
                },
            },

            /**
             * Function: Get List History_init_customer Excel Server Side (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.GET_LIST_HISTORY_INIT_CUSTOMER_EXCEL]: {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = HISTORY_INIT_CUSTOMER_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download History_init_customer Excel Export (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.DOWNLOAD_LIST_HISTORY_INIT_CUSTOMER_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'history_init_customer'
                        });

                        let conditionObj = HISTORY_INIT_CUSTOMER_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listHistory_init_customer = await HISTORY_INIT_CUSTOMER_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listHistory_init_customer)
                    }]
                },
            },

            /**
             * Function: Setting History_init_customer Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.SETTING_FILE_HISTORY_INIT_CUSTOMER_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = HISTORY_INIT_CUSTOMER_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download History_init_customer Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.DOWNLOAD_FILE_HISTORY_INIT_CUSTOMER_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'history_init_customer'
                        });

                        let listHistory_init_customerImport = await HISTORY_INIT_CUSTOMER_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listHistory_init_customerImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listHistory_init_customerImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload History_init_customer Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.CREATE_HISTORY_INIT_CUSTOMER_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:history_init_customer'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'history_init_customer'
                        });

                        let infoHistory_init_customerAfterImport = await HISTORY_INIT_CUSTOMER_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'history_init_customer',
                        });

                        res.json(infoHistory_init_customerAfterImport);
                    }]
                },
            },

            /**
             * Function: API Insert History_init_customer
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.API_ADD_HISTORY_INIT_CUSTOMER]: {
                config: {
                    scopes: ['create:history_init_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            region,
                            area,
                            distributor,
                            store,
                            employee,
                            customer,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        let infoAfterInsert = await HISTORY_INIT_CUSTOMER_MODEL.insertHistory_init_customer({
                            region,
                            area,
                            distributor,
                            store,
                            employee,
                            customer,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: API Update History_init_customer
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.API_UPDATE_HISTORY_INIT_CUSTOMER]: {
                config: {
                    scopes: ['update:history_init_customer'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            history_init_customerID
                        } = req.params;
                        let {
                            region,
                            area,
                            distributor,
                            store,
                            employee,
                            customer,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        let infoAfterUpdate = await HISTORY_INIT_CUSTOMER_MODEL.updateHistory_init_customer({
                            history_init_customerID,
                            region,
                            area,
                            distributor,
                            store,
                            employee,
                            customer,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: API Delete History_init_customer
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.API_DELETE_HISTORY_INIT_CUSTOMER]: {
                config: {
                    scopes: ['delete:history_init_customer'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            history_init_customerID
                        } = req.params;

                        const infoAfterDelete = await HISTORY_INIT_CUSTOMER_MODEL.deleteHistory_init_customer(history_init_customerID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: API Get info History_init_customer
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.API_GET_INFO_HISTORY_INIT_CUSTOMER]: {
                config: {
                    scopes: ['read:info_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            history_init_customerID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoHistory_init_customer = await HISTORY_INIT_CUSTOMER_MODEL.getInfoHistory_init_customer({
                            history_init_customerID,
                            select,
                            filter,
                            explain
                        });
                        res.json(infoHistory_init_customer);
                    }]
                },
            },

            /**
             * Function: API Get info History_init_customer
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.API_GET_LIST_HISTORY_INIT_CUSTOMERS]: {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listHistory_init_customers = await HISTORY_INIT_CUSTOMER_MODEL.getListHistory_init_customers({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listHistory_init_customers);
                    }]
                },
            },

            /**
             * Function: API Download History init customer excel
             * Date: 04/12/2021
             * Dev: Automatic
             */
             [CF_ROUTINGS_HISTORY_INIT_CUSTOMER.DOWNLOAD_LIST_HISTORY_INIT_CUSTOMER_EXCEL_EXPORT_MANIUAL]: {
                config: {
                    scopes: ['read:list_history_init_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { opt } = req.query;
                        opt = JSON.parse(opt);
                        console.log({
                            opt
                        });

                        const infoHistory_init_customer = await HISTORY_INIT_CUSTOMER_MODEL.downloadHistoryInitCustomer({
                            ...opt
                        });
                        
                        res.json(infoHistory_init_customer);
                    }]
                },
            },

        }
    }
};