"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    DEVICETYPE_HISTORY_EXCHANGE_GIFT_TYPE,
} = require('../constants/history_exchange_gift');
const {
    CF_ROUTINGS_HISTORY_EXCHANGE_GIFT
} = require('../constants/history_exchange_gift/history_exchange_gift.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const HISTORY_EXCHANGE_GIFT_MODEL = require('../models/history_exchange_gift').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */
const {
    STORE_COLL
} = require('../../store');

const EMPLOYEE_COLL = require('../../employee/databases/employee-coll');

const {
    CUSTOMER_COLL
} = require('../../customer');

const {
    REWARD_COLL
} = require('../../reward');

const 
    CHECKIN_CHECKOUT_COLL
 = require('../../checkin_checkout/databases/checkin_checkout-coll');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ HISTORY_EXCHANGE_GIFT  ===============================
             * =============================== ************* ===============================
             */


            /**
             * Function: Get List area By parent (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            '/history_exchange_gift/list-area-by-parent': {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            region
                        } = req.query;

                        const listAreaByParent = await AREA_COLL
                            .find({
                                region
                            })
                            .lean();
                        res.json(listAreaByParent);
                    }]
                },
            },

            /**
             * Function: Get List distributor By parent (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            '/history_exchange_gift/list-distributor-by-parent': {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            area
                        } = req.query;

                        const listDistributorByParent = await DISTRIBUTOR_COLL
                            .find({
                                area
                            })
                            .lean();
                        res.json(listDistributorByParent);
                    }]
                },
            },

            /**
             * Function: Get List store By parent (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            '/history_exchange_gift/list-store-by-parent': {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            distributor
                        } = req.query;

                        const listStoreByParent = await STORE_COLL
                            .find({
                                distributor
                            })
                            .lean();
                        res.json(listStoreByParent);
                    }]
                },
            },



            /**
             * Function: Insert History_exchange_gift (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.ADD_HISTORY_EXCHANGE_GIFT]: {
                config: {
                    scopes: ['create:history_exchange_gift'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm History_exchange_gift',
                    code: CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.ADD_HISTORY_EXCHANGE_GIFT,
                    inc: path.resolve(__dirname, '../views/history_exchange_gift/add_history_exchange_gift.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listRewards = await REWARD_COLL.find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1
                            }).lean()

                        ChildRouter.renderToView(req, res, {
                            listRewards,
                            CF_ROUTINGS_HISTORY_EXCHANGE_GIFT
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            store,
                            employee,
                            customer,
                            reward,
                            point,
                        } = req.body;

                        let infoAfterInsert = await HISTORY_EXCHANGE_GIFT_MODEL.insert({
                            store,
                            employee,
                            customer,
                            reward,
                            point,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update History_exchange_gift By Id (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.UPDATE_HISTORY_EXCHANGE_GIFT_BY_ID]: {
                config: {
                    scopes: ['update:history_exchange_gift'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật History_exchange_gift',
                    code: CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.UPDATE_HISTORY_EXCHANGE_GIFT_BY_ID,
                    inc: path.resolve(__dirname, '../views/history_exchange_gift/update_history_exchange_gift.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            history_exchange_giftID
                        } = req.query;

                        let infoHistory_exchange_gift = await HISTORY_EXCHANGE_GIFT_MODEL.getInfoById(history_exchange_giftID);
                        if (infoHistory_exchange_gift.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionStore = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_exchange_gift.data.store) {

                            conditionStore._id = infoHistory_exchange_gift.data.store._id;

                        }

                        let listStores = await STORE_COLL
                            .find(conditionStore)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionEmployee = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_exchange_gift.data.employee) {

                            conditionEmployee._id = infoHistory_exchange_gift.data.employee._id;

                        }

                        let listEmployees = await EMPLOYEE_COLL
                            .find(conditionEmployee)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionCustomer = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_exchange_gift.data.customer) {

                            conditionCustomer._id = infoHistory_exchange_gift.data.customer._id;

                        }

                        let listCustomers = await CUSTOMER_COLL
                            .find(conditionCustomer)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let listRewards = await REWARD_COLL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();

                        let conditionCheckin = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_exchange_gift.data.checkin) {

                            conditionCheckin._id = infoHistory_exchange_gift.data.checkin._id;

                        }

                        let listCheckins = await CHECKIN_CHECKOUT_COLL
                            .find(conditionCheckin)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoHistory_exchange_gift: infoHistory_exchange_gift.data || {},

                            listStores,
                            listEmployees,
                            listCustomers,
                            listRewards,
                            listCheckins,
                            CF_ROUTINGS_HISTORY_EXCHANGE_GIFT
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            history_exchange_giftID,
                            store,
                            employee,
                            customer,
                            reward,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        const infoAfterUpdate = await HISTORY_EXCHANGE_GIFT_MODEL.update({
                            history_exchange_giftID,
                            store,
                            employee,
                            customer,
                            reward,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require History_exchange_gift By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.UPDATE_HISTORY_EXCHANGE_GIFT_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            history_exchange_giftID,
                            store,
                            employee,
                            customer,
                            reward,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        const infoAfterUpdate = await HISTORY_EXCHANGE_GIFT_MODEL.updateNotRequire({
                            history_exchange_giftID,
                            store,
                            employee,
                            customer,
                            reward,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete History_exchange_gift By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.DELETE_HISTORY_EXCHANGE_GIFT_BY_ID]: {
                config: {
                    scopes: ['delete:history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            history_exchange_giftID
                        } = req.params;

                        const infoAfterDelete = await HISTORY_EXCHANGE_GIFT_MODEL.deleteById(history_exchange_giftID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete History_exchange_gift By List Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.DELETE_HISTORY_EXCHANGE_GIFT_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            history_exchange_giftID
                        } = req.body;

                        const infoAfterDelete = await HISTORY_EXCHANGE_GIFT_MODEL.deleteByListId(history_exchange_giftID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info History_exchange_gift By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.GET_INFO_HISTORY_EXCHANGE_GIFT_BY_ID]: {
                config: {
                    scopes: ['read:info_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            history_exchange_giftID
                        } = req.params;

                        const infoHistory_exchange_giftById = await HISTORY_EXCHANGE_GIFT_MODEL.getInfoById(history_exchange_giftID);
                        res.json(infoHistory_exchange_giftById);
                    }]
                },
            },

            /**
             * Function: Get List History_exchange_gift (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.GET_LIST_HISTORY_EXCHANGE_GIFT]: {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách lịch sử đổi quà',
                    code: CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.GET_LIST_HISTORY_EXCHANGE_GIFT,
                    inc: path.resolve(__dirname, '../views/history_exchange_gift/list_history_exchange_gifts.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            pointFromNumber,
                            pointToNumber,
                            keyword,
                            typeGetList
                        } = req.query;

                        let listHistory_exchange_gifts = [];
                        if (typeGetList === 'FILTER') {
                            listHistory_exchange_gifts = await HISTORY_EXCHANGE_GIFT_MODEL.getListByFilter({
                                pointFromNumber,
                                pointToNumber,
                                keyword,
                            });
                        } else {
                            listHistory_exchange_gifts = await HISTORY_EXCHANGE_GIFT_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listHistory_exchange_gifts: listHistory_exchange_gifts.data || [],
                            DEVICETYPE_HISTORY_EXCHANGE_GIFT_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List History_exchange_gift By Field (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.GET_LIST_HISTORY_EXCHANGE_GIFT_BY_FIELD]: {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách History_exchange_gift by field isStatus',
                    code: CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.GET_LIST_HISTORY_EXCHANGE_GIFT_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/history_exchange_gift/list_history_exchange_gifts.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            pointFromNumber,
                            pointToNumber,
                            keyword,
                            type
                        } = req.query;

                        let listHistory_exchange_gifts = await HISTORY_EXCHANGE_GIFT_MODEL.getListByFilter({
                            pointFromNumber,
                            pointToNumber,
                            keyword,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listHistory_exchange_gifts: listHistory_exchange_gifts.data || [],
                            DEVICETYPE_HISTORY_EXCHANGE_GIFT_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List History_exchange_gift Server Side (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.GET_LIST_HISTORY_EXCHANGE_GIFT_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listHistory_exchange_giftServerSide = await HISTORY_EXCHANGE_GIFT_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(listHistory_exchange_giftServerSide);
                    }]
                },
            },

            /**
             * Function: Get List History_exchange_gift Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.GET_LIST_HISTORY_EXCHANGE_GIFT_IMPORT]: {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listHistory_exchange_giftImport = await HISTORY_EXCHANGE_GIFT_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listHistory_exchange_giftImport);
                    }]
                },
            },

            /**
             * Function: Get List History_exchange_gift Excel Server Side (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.GET_LIST_HISTORY_EXCHANGE_GIFT_EXCEL]: {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = HISTORY_EXCHANGE_GIFT_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download History_exchange_gift Excel Export (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.DOWNLOAD_LIST_HISTORY_EXCHANGE_GIFT_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'history_exchange_gift'
                        });

                        let conditionObj = HISTORY_EXCHANGE_GIFT_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listHistory_exchange_gift = await HISTORY_EXCHANGE_GIFT_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listHistory_exchange_gift)
                    }]
                },
            },

            /**
             * Function: Setting History_exchange_gift Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.SETTING_FILE_HISTORY_EXCHANGE_GIFT_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = HISTORY_EXCHANGE_GIFT_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download History_exchange_gift Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.DOWNLOAD_FILE_HISTORY_EXCHANGE_GIFT_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'history_exchange_gift'
                        });

                        let listHistory_exchange_giftImport = await HISTORY_EXCHANGE_GIFT_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listHistory_exchange_giftImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listHistory_exchange_giftImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload History_exchange_gift Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.CREATE_HISTORY_EXCHANGE_GIFT_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'history_exchange_gift'
                        });

                        let infoHistory_exchange_giftAfterImport = await HISTORY_EXCHANGE_GIFT_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'history_exchange_gift',
                        });

                        res.json(infoHistory_exchange_giftAfterImport);
                    }]
                },
            },

            /**
             * Function: API Get info History_exchange_gift
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.API_GET_INFO_HISTORY_EXCHANGE_GIFT]: {
                config: {
                    scopes: ['read:info_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            history_exchange_giftID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoHistory_exchange_gift = await HISTORY_EXCHANGE_GIFT_MODEL.getInfoHistory_exchange_gift({
                            history_exchange_giftID,
                            select,
                            filter,
                            explain
                        });
                        res.json(infoHistory_exchange_gift);
                    }]
                },
            },

            /**
             * Function: API Get info History_exchange_gift
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.API_GET_LIST_HISTORY_EXCHANGE_GIFTS]: {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listHistory_exchange_gifts = await HISTORY_EXCHANGE_GIFT_MODEL.getListHistory_exchange_gifts({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listHistory_exchange_gifts);
                    }]
                },
            },

            /**
             * Function: API Insert History_exchange_gift
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.API_ADD_HISTORY_EXCHANGE_GIFT]: {
                config: {
                    scopes: ['create:history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            region,
                            area,
                            distributor,
                            store,
                            employee,
                            customer,
                            reward,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        let infoAfterInsert = await HISTORY_EXCHANGE_GIFT_MODEL.insertHistory_exchange_gift({
                            region,
                            area,
                            distributor,
                            store,
                            employee,
                            customer,
                            reward,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: API Update History_exchange_gift
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.API_UPDATE_HISTORY_EXCHANGE_GIFT]: {
                config: {
                    scopes: ['update:history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            history_exchange_giftID
                        } = req.params;
                        let {
                            region,
                            area,
                            distributor,
                            store,
                            employee,
                            customer,
                            reward,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        let infoAfterUpdate = await HISTORY_EXCHANGE_GIFT_MODEL.updateHistory_exchange_gift({
                            history_exchange_giftID,
                            region,
                            area,
                            distributor,
                            store,
                            employee,
                            customer,
                            reward,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: API Delete History_exchange_gift
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.API_DELETE_HISTORY_EXCHANGE_GIFT]: {
                config: {
                    scopes: ['delete:history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            history_exchange_giftID
                        } = req.params;

                        const infoAfterDelete = await HISTORY_EXCHANGE_GIFT_MODEL.deleteHistory_exchange_gift(history_exchange_giftID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            [CF_ROUTINGS_HISTORY_EXCHANGE_GIFT.DOWNLOAD_HISTORY_EXCHANGE_GIFT_EXCEL]: {
                config: {
                    scopes: ['read:list_history_exchange_gift'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { opt } = req.query;
                        opt = JSON.parse(opt);
                        
                        let response = await HISTORY_EXCHANGE_GIFT_MODEL.downloadHistory_exchange_giftExcel({ 
                           ...opt
                        });

                        res.json(response);
                    }]
                },
            },

        }
    }
};