"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    DEVICETYPE_SALEOUT_TYPE,
} = require('../constants/saleout');
const {
    CF_ROUTINGS_SALEOUT
} = require('../constants/saleout/saleout.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const SALEOUT_MODEL = require('../models/saleout').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const 
    EMPLOYEE_COLL
 = require('../../employee/databases/employee-coll');

const {
    CUSTOMER_COLL
} = require('../../customer');

const {
    STORE_COLL
} = require('../../store');

const {
    PRODUCT_COLL
} = require('../../product');

const 
    CHECKIN_CHECKOUT_COLL
 = require('../../checkin_checkout/databases/checkin_checkout-coll');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ SALEOUT  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Saleout (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.ADD_SALEOUT]: {
                config: {
                    scopes: ['create:saleout'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Saleout',
                    code: CF_ROUTINGS_SALEOUT.ADD_SALEOUT,
                    inc: path.resolve(__dirname, '../views/saleout/add_saleout.ejs')
                },
                methods: {
                    get: [async function(req, res) {


                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_SALEOUT
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            employee,
                            customer,
                            store,
                            product,
                            price,
                            point,
                        } = req.body;


                        let infoAfterInsert = await SALEOUT_MODEL.insert({
                            employee,
                            customer,
                            store,
                            product,
                            price,
                            point,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Saleout By Id (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.UPDATE_SALEOUT_BY_ID]: {
                config: {
                    scopes: ['update:saleout'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Saleout',
                    code: CF_ROUTINGS_SALEOUT.UPDATE_SALEOUT_BY_ID,
                    inc: path.resolve(__dirname, '../views/saleout/update_saleout.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            saleoutID
                        } = req.query;

                        let infoSaleout = await SALEOUT_MODEL.getInfoById(saleoutID);
                        if (infoSaleout.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionEmployee = {
                            state: 1,
                            status: 1
                        };
                        if (infoSaleout.data.employee) {

                            conditionEmployee._id = infoSaleout.data.employee._id;

                        }

                        let listEmployees = await EMPLOYEE_COLL
                            .find(conditionEmployee)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionCustomer = {
                            state: 1,
                            status: 1
                        };
                        if (infoSaleout.data.customer) {

                            conditionCustomer._id = infoSaleout.data.customer._id;

                        }

                        let listCustomers = await CUSTOMER_COLL
                            .find(conditionCustomer)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionStore = {
                            state: 1,
                            status: 1
                        };
                        if (infoSaleout.data.store) {

                            conditionStore._id = infoSaleout.data.store._id;

                        }

                        let listStores = await STORE_COLL
                            .find(conditionStore)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionProduct = {
                            state: 1,
                            status: 1
                        };
                        if (infoSaleout.data.product) {

                            conditionProduct._id = infoSaleout.data.product._id;

                        }

                        let listProducts = await PRODUCT_COLL
                            .find(conditionProduct)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionCheckin = {
                            state: 1,
                            status: 1
                        };
                        if (infoSaleout.data.checkin) {

                            conditionCheckin._id = infoSaleout.data.checkin._id;

                        }

                        let listCheckins = await CHECKIN_CHECKOUT_COLL
                            .find(conditionCheckin)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoSaleout: infoSaleout.data || {},

                            listEmployees,
                            listCustomers,
                            listStores,
                            listProducts,
                            listCheckins,
                            CF_ROUTINGS_SALEOUT
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            saleoutID,
                            employee,
                            customer,
                            store,
                            product,
                            price,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        const infoAfterUpdate = await SALEOUT_MODEL.update({
                            saleoutID,
                            employee,
                            customer,
                            store,
                            product,
                            price,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Saleout By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.UPDATE_SALEOUT_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:saleout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            saleoutID,
                            employee,
                            customer,
                            store,
                            product,
                            price,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        const infoAfterUpdate = await SALEOUT_MODEL.updateNotRequire({
                            saleoutID,
                            employee,
                            customer,
                            store,
                            product,
                            price,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Saleout By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.DELETE_SALEOUT_BY_ID]: {
                config: {
                    scopes: ['delete:saleout'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            saleoutID
                        } = req.params;

                        const infoAfterDelete = await SALEOUT_MODEL.deleteById(saleoutID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Saleout By List Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.DELETE_SALEOUT_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:saleout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            saleoutID
                        } = req.body;

                        const infoAfterDelete = await SALEOUT_MODEL.deleteByListId(saleoutID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Saleout By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.GET_INFO_SALEOUT_BY_ID]: {
                config: {
                    scopes: ['read:info_saleout'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            saleoutID
                        } = req.params;

                        const infoSaleoutById = await SALEOUT_MODEL.getInfoById(saleoutID);
                        res.json(infoSaleoutById);
                    }]
                },
            },

            /**
             * Function: Get List Saleout (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.GET_LIST_SALEOUT]: {
                config: {
                    scopes: ['read:list_saleout'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách lịch sử tích điểm',
                    code: CF_ROUTINGS_SALEOUT.GET_LIST_SALEOUT,
                    inc: path.resolve(__dirname, '../views/saleout/list_saleouts.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            priceFromNumber,
                            priceToNumber,
                            typeGetList
                        } = req.query;

                        let listSaleouts = [];
                        if (typeGetList === 'FILTER') {
                            listSaleouts = await SALEOUT_MODEL.getListByFilter({
                                keyword,
                                priceFromNumber,
                                priceToNumber,
                            });
                        } else {
                            listSaleouts = await SALEOUT_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listSaleouts: listSaleouts.data || [],
                            DEVICETYPE_SALEOUT_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Saleout By Field (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.GET_LIST_SALEOUT_BY_FIELD]: {
                config: {
                    scopes: ['read:list_saleout'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Saleout by field isStatus',
                    code: CF_ROUTINGS_SALEOUT.GET_LIST_SALEOUT_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/saleout/list_saleouts.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            priceFromNumber,
                            priceToNumber,
                            type
                        } = req.query;

                        let listSaleouts = await SALEOUT_MODEL.getListByFilter({
                            keyword,
                            priceFromNumber,
                            priceToNumber,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listSaleouts: listSaleouts.data || [],
                            DEVICETYPE_SALEOUT_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Saleout Server Side (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.GET_LIST_SALEOUT_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_saleout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order,
                            customerID
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listSaleoutServerSide = await SALEOUT_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            customerID,
                            dir,
                            region
                        });
                        res.json(listSaleoutServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Saleout Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.GET_LIST_SALEOUT_IMPORT]: {
                config: {
                    scopes: ['read:list_saleout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listSaleoutImport = await SALEOUT_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listSaleoutImport);
                    }]
                },
            },

            /**
             * Function: Get List Saleout Excel Server Side (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.GET_LIST_SALEOUT_EXCEL]: {
                config: {
                    scopes: ['read:list_saleout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = SALEOUT_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Saleout Excel Export (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.DOWNLOAD_LIST_SALEOUT_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_saleout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'saleout'
                        });

                        let conditionObj = SALEOUT_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listSaleout = await SALEOUT_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listSaleout)
                    }]
                },
            },

            /**
             * Function: Setting Saleout Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.SETTING_FILE_SALEOUT_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_saleout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = SALEOUT_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Saleout Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.DOWNLOAD_FILE_SALEOUT_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_saleout'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'saleout'
                        });

                        let listSaleoutImport = await SALEOUT_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listSaleoutImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listSaleoutImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Saleout Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.CREATE_SALEOUT_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:saleout'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'saleout'
                        });

                        let infoSaleoutAfterImport = await SALEOUT_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'saleout',
                        });

                        res.json(infoSaleoutAfterImport);
                    }]
                },
            },

            /**
             * Function: API Insert Saleout
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.API_ADD_SALEOUT]: {
                config: {
                    scopes: ['create:saleout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            region,
                            area,
                            distributor,
                            employee,
                            customer,
                            store,
                            product,
                            order,
                            price,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        let infoAfterInsert = await SALEOUT_MODEL.insertSaleout({
                            region,
                            area,
                            distributor,
                            employee,
                            customer,
                            store,
                            product,
                            order,
                            price,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: API Update Saleout
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.API_UPDATE_SALEOUT]: {
                config: {
                    scopes: ['update:saleout'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            saleoutID
                        } = req.params;
                        let {
                            region,
                            area,
                            distributor,
                            employee,
                            customer,
                            store,
                            product,
                            order,
                            price,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        let infoAfterUpdate = await SALEOUT_MODEL.updateSaleout({
                            saleoutID,
                            region,
                            area,
                            distributor,
                            employee,
                            customer,
                            store,
                            product,
                            order,
                            price,
                            point,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: API Delete Saleout
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.API_DELETE_SALEOUT]: {
                config: {
                    scopes: ['delete:saleout'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            saleoutID
                        } = req.params;

                        const infoAfterDelete = await SALEOUT_MODEL.deleteSaleout(saleoutID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: API Get info Saleout
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.API_GET_INFO_SALEOUT]: {
                config: {
                    scopes: ['read:info_saleout'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            saleoutID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoSaleout = await SALEOUT_MODEL.getInfoSaleout({
                            saleoutID,
                            select,
                            filter,
                            explain
                        });
                        res.json(infoSaleout);
                    }]
                },
            },

            /**
             * Function: API Get info Saleout
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SALEOUT.API_GET_LIST_SALEOUTS]: {
                config: {
                    scopes: ['read:list_saleout'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listSaleouts = await SALEOUT_MODEL.getListSaleouts({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listSaleouts);
                    }]
                },
            },

            [CF_ROUTINGS_SALEOUT.DOWNLOAD_SALEOUT_EXCEL]: {
                config: {
                    scopes: ['read:list_saleout'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { opt } = req.query;
                        opt = JSON.parse(opt);
                        
                        let response = await SALEOUT_MODEL.downloadHistory_exchange_giftExcel({ 
                           ...opt
                        });

                        res.json(response);
                    }]
                },
            },

        }
    }
};