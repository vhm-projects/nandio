"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    DEVICETYPE_HISTORY_EXCHANGE_SAMPLING_TYPE,
} = require('../constants/history_exchange_sampling');
const {
    CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING
} = require('../constants/history_exchange_sampling/history_exchange_sampling.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const HISTORY_EXCHANGE_SAMPLING_MODEL = require('../models/history_exchange_sampling').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */
const {
    STORE_COLL
} = require('../../store');

// const {
//     EMPLOYEE_COLL
// } = require('../../employee');

const 
    EMPLOYEE_COLL
= require('../../employee/databases/employee-coll');

const {
    CUSTOMER_COLL
} = require('../../customer');

const {
    PRODUCT_COLL
} = require('../../product');

// const {
//     CHECKIN_CHECKOUT_COLL
// } = require('../../checkin_checkout');

const
    CHECKIN_CHECKOUT_COLL
= require('../../checkin_checkout/databases/checkin_checkout-coll');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ HISTORY_EXCHANGE_SAMPLING  ===============================
             * =============================== ************* ===============================
             */


            /**
             * Function: Get List area By parent (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            '/history_exchange_sampling/list-area-by-parent': {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            region
                        } = req.query;

                        const listAreaByParent = await AREA_COLL
                            .find({
                                region
                            })
                            .lean();
                        res.json(listAreaByParent);
                    }]
                },
            },

            /**
             * Function: Get List distributor By parent (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            '/history_exchange_sampling/list-distributor-by-parent': {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            area
                        } = req.query;

                        const listDistributorByParent = await DISTRIBUTOR_COLL
                            .find({
                                area
                            })
                            .lean();
                        res.json(listDistributorByParent);
                    }]
                },
            },

            /**
             * Function: Get List store By parent (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            '/history_exchange_sampling/list-store-by-parent': {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            distributor
                        } = req.query;

                        const listStoreByParent = await STORE_COLL
                            .find({
                                distributor
                            })
                            .lean();
                        res.json(listStoreByParent);
                    }]
                },
            },



            /**
             * Function: Insert History_exchange_sampling (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.ADD_HISTORY_EXCHANGE_SAMPLING]: {
                config: {
                    scopes: ['create:history_exchange_sampling'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm History_exchange_sampling',
                    code: CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.ADD_HISTORY_EXCHANGE_SAMPLING,
                    inc: path.resolve(__dirname, '../views/history_exchange_sampling/add_history_exchange_sampling.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        ChildRouter.renderToView(req, res, {
                            CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            store,
                            employee,
                            customer,
                            product,
                            amount,
                        } = req.body;

                        let infoAfterInsert = await HISTORY_EXCHANGE_SAMPLING_MODEL.insert({
                            store,
                            employee,
                            customer,
                            product,
                            amount,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update History_exchange_sampling By Id (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.UPDATE_HISTORY_EXCHANGE_SAMPLING_BY_ID]: {
                config: {
                    scopes: ['update:history_exchange_sampling'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật History_exchange_sampling',
                    code: CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.UPDATE_HISTORY_EXCHANGE_SAMPLING_BY_ID,
                    inc: path.resolve(__dirname, '../views/history_exchange_sampling/update_history_exchange_sampling.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            history_exchange_samplingID
                        } = req.query;

                        let infoHistory_exchange_sampling = await HISTORY_EXCHANGE_SAMPLING_MODEL.getInfoById(history_exchange_samplingID);
                        if (infoHistory_exchange_sampling.error) {
                            return res.redirect('/something-went-wrong');
                        }

                        let conditionStore = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_exchange_sampling.data.store) {
                            conditionStore._id = infoHistory_exchange_sampling.data.store._id;
                        }

                        let listStores = await STORE_COLL
                            .find(conditionStore)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionEmployee = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_exchange_sampling.data.employee) {
                            conditionEmployee._id = infoHistory_exchange_sampling.data.employee._id;
                        }

                        let listEmployees = await EMPLOYEE_COLL
                            .find(conditionEmployee)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionCustomer = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_exchange_sampling.data.customer) {
                            conditionCustomer._id = infoHistory_exchange_sampling.data.customer._id;
                        }

                        let listCustomers = await CUSTOMER_COLL
                            .find(conditionCustomer)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionProduct = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_exchange_sampling.data.product) {
                            conditionProduct._id = infoHistory_exchange_sampling.data.product._id;
                        }

                        let listProducts = await PRODUCT_COLL
                            .find(conditionProduct)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionCheckin = {
                            state: 1,
                            status: 1
                        };
                        if (infoHistory_exchange_sampling.data.checkin) {

                            conditionCheckin._id = infoHistory_exchange_sampling.data.checkin._id;

                        }

                        let listCheckins = await CHECKIN_CHECKOUT_COLL
                            .find(conditionCheckin)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoHistory_exchange_sampling: infoHistory_exchange_sampling.data || {},
                            listStores,
                            listEmployees,
                            listCustomers,
                            listProducts,
                            listCheckins,
                            CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            history_exchange_samplingID,
                            store,
                            employee,
                            customer,
                            product,
                            amount,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;

                        const infoAfterUpdate = await HISTORY_EXCHANGE_SAMPLING_MODEL.update({
                            history_exchange_samplingID,
                            store,
                            employee,
                            customer,
                            product,
                            amount,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require History_exchange_sampling By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.UPDATE_HISTORY_EXCHANGE_SAMPLING_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            history_exchange_samplingID,
                            store,
                            employee,
                            customer,
                            product,
                            amount,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;

                        const infoAfterUpdate = await HISTORY_EXCHANGE_SAMPLING_MODEL.updateNotRequire({
                            history_exchange_samplingID,
                            store,
                            employee,
                            customer,
                            product,
                            amount,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete History_exchange_sampling By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.DELETE_HISTORY_EXCHANGE_SAMPLING_BY_ID]: {
                config: {
                    scopes: ['delete:history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            history_exchange_samplingID
                        } = req.params;

                        const infoAfterDelete = await HISTORY_EXCHANGE_SAMPLING_MODEL.deleteById(history_exchange_samplingID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete History_exchange_sampling By List Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.DELETE_HISTORY_EXCHANGE_SAMPLING_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            history_exchange_samplingID
                        } = req.body;

                        const infoAfterDelete = await HISTORY_EXCHANGE_SAMPLING_MODEL.deleteByListId(history_exchange_samplingID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info History_exchange_sampling By Id (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.GET_INFO_HISTORY_EXCHANGE_SAMPLING_BY_ID]: {
                config: {
                    scopes: ['read:info_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            history_exchange_samplingID
                        } = req.params;

                        const infoHistory_exchange_samplingById = await HISTORY_EXCHANGE_SAMPLING_MODEL.getInfoById(history_exchange_samplingID);
                        res.json(infoHistory_exchange_samplingById);
                    }]
                },
            },

            /**
             * Function: Get List History_exchange_sampling (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.GET_LIST_HISTORY_EXCHANGE_SAMPLING]: {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách lịch sử hàng dùng thử',
                    code: CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.GET_LIST_HISTORY_EXCHANGE_SAMPLING,
                    inc: path.resolve(__dirname, '../views/history_exchange_sampling/list_history_exchange_samplings.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            typeGetList
                        } = req.query;

                        let listHistory_exchange_samplings = [];
                        if (typeGetList === 'FILTER') {
                            listHistory_exchange_samplings = await HISTORY_EXCHANGE_SAMPLING_MODEL.getListByFilter({
                                keyword,
                            });
                        } else {
                            listHistory_exchange_samplings = await HISTORY_EXCHANGE_SAMPLING_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listHistory_exchange_samplings: listHistory_exchange_samplings.data || [],
                            DEVICETYPE_HISTORY_EXCHANGE_SAMPLING_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List History_exchange_sampling By Field (API, VIEW)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.GET_LIST_HISTORY_EXCHANGE_SAMPLING_BY_FIELD]: {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách History_exchange_sampling by field isStatus',
                    code: CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.GET_LIST_HISTORY_EXCHANGE_SAMPLING_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/history_exchange_sampling/list_history_exchange_samplings.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                        } = req.query;

                        let listHistory_exchange_samplings = await HISTORY_EXCHANGE_SAMPLING_MODEL.getListByFilter({
                            keyword,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listHistory_exchange_samplings: listHistory_exchange_samplings.data || [],
                            DEVICETYPE_HISTORY_EXCHANGE_SAMPLING_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List History_exchange_sampling Server Side (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.GET_LIST_HISTORY_EXCHANGE_SAMPLING_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listHistory_exchange_samplingServerSide = await HISTORY_EXCHANGE_SAMPLING_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(listHistory_exchange_samplingServerSide);
                    }]
                },
            },

            /**
             * Function: Get List History_exchange_sampling Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.GET_LIST_HISTORY_EXCHANGE_SAMPLING_IMPORT]: {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listHistory_exchange_samplingImport = await HISTORY_EXCHANGE_SAMPLING_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listHistory_exchange_samplingImport);
                    }]
                },
            },

            /**
             * Function: Get List History_exchange_sampling Excel Server Side (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.GET_LIST_HISTORY_EXCHANGE_SAMPLING_EXCEL]: {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = HISTORY_EXCHANGE_SAMPLING_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download History_exchange_sampling Excel Export (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.DOWNLOAD_LIST_HISTORY_EXCHANGE_SAMPLING_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'history_exchange_sampling'
                        });

                        let conditionObj = HISTORY_EXCHANGE_SAMPLING_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listHistory_exchange_sampling = await HISTORY_EXCHANGE_SAMPLING_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listHistory_exchange_sampling)
                    }]
                },
            },

            /**
             * Function: Setting History_exchange_sampling Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.SETTING_FILE_HISTORY_EXCHANGE_SAMPLING_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = HISTORY_EXCHANGE_SAMPLING_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download History_exchange_sampling Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.DOWNLOAD_FILE_HISTORY_EXCHANGE_SAMPLING_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'history_exchange_sampling'
                        });

                        let listHistory_exchange_samplingImport = await HISTORY_EXCHANGE_SAMPLING_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listHistory_exchange_samplingImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listHistory_exchange_samplingImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload History_exchange_sampling Excel Import (API)
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.CREATE_HISTORY_EXCHANGE_SAMPLING_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'history_exchange_sampling'
                        });

                        let infoHistory_exchange_samplingAfterImport = await HISTORY_EXCHANGE_SAMPLING_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'history_exchange_sampling',
                        });

                        res.json(infoHistory_exchange_samplingAfterImport);
                    }]
                },
            },

            /**
             * Function: API Get info History_exchange_sampling
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.API_GET_INFO_HISTORY_EXCHANGE_SAMPLING]: {
                config: {
                    scopes: ['read:info_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            history_exchange_samplingID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoHistory_exchange_sampling = await HISTORY_EXCHANGE_SAMPLING_MODEL.getInfoHistory_exchange_sampling({
                            history_exchange_samplingID,
                            select,
                            filter,
                            explain
                        });
                        res.json(infoHistory_exchange_sampling);
                    }]
                },
            },

            /**
             * Function: API Get info History_exchange_sampling
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.API_GET_LIST_HISTORY_EXCHANGE_SAMPLINGS]: {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        // lưu ý: ở api này do Nguyên ko gửi month, year -> K cần gán cứng phía server
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listHistory_exchange_samplings = await HISTORY_EXCHANGE_SAMPLING_MODEL.getListHistory_exchange_samplings({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listHistory_exchange_samplings);
                    }]
                },
            },

            /**
             * Function: API Insert History_exchange_sampling
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.API_ADD_HISTORY_EXCHANGE_SAMPLING]: {
                config: {
                    scopes: ['create:history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let employee = req.user && req.user._id;
                        let {
                            product,
                            amount,
                            deviceType,
                            deviceID,
                            deviceName,
                            isExportFromQShopESampling,
                            user_register_trial_programID,
                        } = req.body;

                        let infoAfterInsert = await HISTORY_EXCHANGE_SAMPLING_MODEL.insertHistory_exchange_sampling({
                            employee,
                            product,
                            amount,
                            deviceType,
                            deviceID,
                            deviceName,
                            isExportFromQShopESampling,
                            user_register_trial_programID,
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: API Update History_exchange_sampling
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.API_UPDATE_HISTORY_EXCHANGE_SAMPLING]: {
                config: {
                    scopes: ['update:history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            history_exchange_samplingID
                        } = req.params;
                        let {
                            region,
                            area,
                            distributor,
                            store,
                            employee,
                            customer,
                            product,
                            amount,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                        } = req.body;


                        let infoAfterUpdate = await HISTORY_EXCHANGE_SAMPLING_MODEL.updateHistory_exchange_sampling({
                            history_exchange_samplingID,
                            region,
                            area,
                            distributor,
                            store,
                            employee,
                            customer,
                            product,
                            amount,
                            checkin,
                            deviceType,
                            deviceID,
                            deviceName,
                            lat,
                            lng,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: API Delete History_exchange_sampling
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.API_DELETE_HISTORY_EXCHANGE_SAMPLING]: {
                config: {
                    scopes: ['delete:history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            history_exchange_samplingID
                        } = req.params;

                        const infoAfterDelete = await HISTORY_EXCHANGE_SAMPLING_MODEL.deleteHistory_exchange_sampling(history_exchange_samplingID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: API Delete History_exchange_sampling
             * Date: 04/12/2021
             * Dev: Automatic
             */
             [CF_ROUTINGS_HISTORY_EXCHANGE_SAMPLING.DOWNLOAD_LIST_HISTORY_EXCHANGE_SAMPLING_EXCEL_EXPORT_MANIUAL]: {
                config: {
                    scopes: ['read:list_history_exchange_sampling'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { opt } = req.query;
                        opt = JSON.parse(opt);
                        console.log({
                            opt
                        });

                        const infoExportExcel = await HISTORY_EXCHANGE_SAMPLING_MODEL.downloadHistory_exchange_samplingExcel({
                            ...opt
                        });
                        res.json(infoExportExcel);
                    }]
                },
            },

        }
    }
};