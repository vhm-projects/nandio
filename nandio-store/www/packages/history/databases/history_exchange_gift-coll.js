"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('history_exchange_gift', {

    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Vùng
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * Nhà phân phối
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    /**
     * Cửa hàng
     */
    store: {
        type: Schema.Types.ObjectId,
        ref: 'store',
    },
    /**
     * Nhân viên
     */
    employee: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },
    /**
     * Khách hàng
     */
    customer: {
        type: Schema.Types.ObjectId,
        ref: 'customer',
    },
    /**
     * Quà
     */
    reward: {
        type: Schema.Types.ObjectId,
        ref: 'reward',
    },

    /**
     * hiện tại reward sẽ là loại dynamic reference,
     *  -> cần lưu product để truy vấn nhanh (nếu reward có kind là 'product')
     */
    sku: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },

    /**
     * số lượng đổi
     */
    amount: {
        type: Number,
        default: 1
    },

    /**
     * Điểm trước khi đổi
     */
    beforePoint: {
        type: Number,
    },

    /**
     * Điểm
     */
    point: {
        type: Number,
        required: true,
    },

    /**
     * Điểm sau khi đổi
     */
    afterPoint: {
        type: Number,
    },
    /**
     * Checkin
     */
    checkin: {
        type: Schema.Types.ObjectId,
        ref: 'checkin_checkout',
    },
    /**
     * Loại thiết bị 
     * ANDROID: ANDROID,
     * IOS: IOS
     */
    deviceType: {
        type: String,
        default: "ANDROID",
        enum: ["ANDROID", "IOS"],
    },
    /**
     * ID thiết bị
     */
    deviceID: {
        type: String,
    },
    /**
     * Tên thiết bị
     */
    deviceName: {
        type: String,
    },
    /**
     * latitude
     */
    lat: {
        type: String,
    },
    /**
     * longtitude
     */
    lng: {
        type: String,
    },
});