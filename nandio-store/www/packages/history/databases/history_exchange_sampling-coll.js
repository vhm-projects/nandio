"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('history_exchange_sampling', {

    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Vùng
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * Nhà phân phối
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    /**
     * Cửa hàng
     */
    store: {
        type: Schema.Types.ObjectId,
        ref: 'store',
    },
    /**
     * Nhân viên
     */
    employee: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },
    /**
     * Khách hàng
     */
    customer: {
        type: Schema.Types.ObjectId,
        ref: 'customer',
    },
    /**
     * Sản phẩm
     */
    product: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },
    /**
     * Số lượng
     */
    amount: {
        type: Number,
        required: true,
    },
    /**
     * Checkin
     */
    checkin: {
        type: Schema.Types.ObjectId,
        ref: 'checkin_checkout',
    },
    /**
     * Loại thiết bị 
     * ANDROID: ANDROID,
     * IOS: IOS
     */
    deviceType: {
        type: String,
        default: "ANDROID",
        enum: ["ANDROID", "IOS"],
    },
    /**
     * ID thiết bị
     */
    deviceID: {
        type: String,
    },
    /**
     * Tên thiết bị
     */
    deviceName: {
        type: String,
    },
    /**
     * latitude
     */
    lat: {
        type: String,
    },
    /**
     * longtitude
     */
    lng: {
        type: String,
    },
    // field nhận biết xuất từ QSHOP(Esampling) hay là xuất từ QSTORE
    isExportFromQShopESampling: {
        type: Boolean
    }
});