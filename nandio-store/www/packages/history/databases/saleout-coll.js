"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('saleout', {

    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Vùng
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * Nhà phân phối
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    /**
     * Nhân viên
     */
    employee: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },
    /**
     * Khách hàng
     */
    customer: {
        type: Schema.Types.ObjectId,
        ref: 'customer',
    },
    /**
     * cửa hàng
     */
    store: {
        type: Schema.Types.ObjectId,
        ref: 'store',
    },
    /**
     * Sản phẩm
     */
    product: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },
    /**
     * Đơn hàng
     */
    order: {
        type: String,
    },
    /**
     * Giá
     */
    price: {
        type: Number,
        required: true,
    },
    /**
     * Điểm
     */
    point: {
        type: Number,
        required: true,
    },
    /**
     * Checkin
     */
    checkin: {
        type: Schema.Types.ObjectId,
        ref: 'checkin_checkout',
    },
    /**
     * Loại thiết bị 
     * ANDROID: ANDROID,
     * IOS: IOS
     */
    deviceType: {
        type: String,
        default: "IOS",
        enum: ["ANDROID", "IOS"],
    },
    /**
     * ID thiết bị
     */
    deviceID: {
        type: String,
    },
    /**
     * Tên thiết bị
     */
    deviceName: {
        type: String,
    },
    /**
     * latitude
     */
    lat: {
        type: String,
    },
    /**
     * longtitude
     */
    lng: {
        type: String,
    },
});