"use strict";

/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const path = require('path');
const fs = require('fs');
const ISOdate = require('isodate');
const ObjectID = require('mongoose').Types.ObjectId;
const XlsxPopulate = require('xlsx-populate');
const mongodb = require("mongodb");
const {
    MongoClient
} = mongodb;
const formatCurrency = require('number-format.js');

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    colName,
} = require('../../../utils/utils');
const {
    isTrue
} = require('../../../tools/module/check');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const URL_DATABASE = process.env.URL_DATABASE || 'mongodb://localhost:27017';
const NAME_DATABASE = process.env.NAME_DATABASE || 'ldk_tools_op';

/**
 * COLLECTIONS
 */
const HISTORY_EXCHANGE_GIFT_COLL = require('../databases/history_exchange_gift-coll');
const REWARD_COLL                = require('../../reward/databases/reward-coll');

const {
    EMPLOYEE_COLL
} = require('../../employee');

class Model extends BaseModel {
    constructor() {
        super(HISTORY_EXCHANGE_GIFT_COLL);
    }

    /**
         * Tạo mới history_exchange_gift
		* @param {object} store
		* @param {object} employee
		* @param {object} customer
		* @param {object} reward
		* @param {number} point

         * @param {objectId} userCreate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        store,
        employee,
        customer,
        reward,
        point,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (!point) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập điểm'
                    });
                }

                if (!checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (!checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                if (!checkObjectIDs([customer])) {
                    return resolve({
                        error: true,
                        message: 'khách hàng không hợp lệ'
                    });
                }

                if (!checkObjectIDs([reward])) {
                    return resolve({
                        error: true,
                        message: 'quà không hợp lệ'
                    });
                }

                let infoReward = await REWARD_COLL.findById(reward);
                if (!infoReward)
                    return resolve({
                        error: true,
                        message: 'quà không hợp lệ'
                    });

                let { gift: { item: sku } } = infoReward;
 
                let dataInsert = {
                    store,
                    employee,
                    customer,
                    reward,
                    sku,
                    point,
                    userCreate
                };

                if (employee) {
                    const employeeInfo = await EMPLOYEE_COLL.findById(employee);
                    dataInsert.region = employeeInfo.region;
                    dataInsert.area = employeeInfo.area;
                    dataInsert.distributor = employeeInfo.distributor;
                }

                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo lịch sử đổi quà thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật history_exchange_gift 
         * @param {objectId} history_exchange_giftID
		* @param {object} store
		* @param {object} employee
		* @param {object} customer
		* @param {object} reward
		* @param {number} point
		* @param {object} checkin
		* @param {string} deviceType
		* @param {string} deviceID
		* @param {string} deviceName
		* @param {string} lat
		* @param {string} lng

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        history_exchange_giftID,
        store,
        employee,
        customer,
        reward,
        point,
        checkin,
        deviceType,
        deviceID,
        deviceName,
        lat,
        lng,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([history_exchange_giftID])) {
                    return resolve({
                        error: true,
                        message: 'history_exchange_giftID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (!point) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập điểm cho history_exchange_gift'
                    });
                }

                if (deviceType.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài loại thiết bị không được lớn hơn 125 ký tự'
                    });
                }

                if (deviceID.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài id thiết bị không được lớn hơn 125 ký tự'
                    });
                }

                if (deviceName.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tên thiết bị không được lớn hơn 125 ký tự'
                    });
                }

                if (lat.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài latitude không được lớn hơn 125 ký tự'
                    });
                }

                if (lng.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài longtitude không được lớn hơn 125 ký tự'
                    });
                }


                if (deviceType && !["ANDROID", "IOS"].includes(deviceType)) {
                    return resolve({
                        error: true,
                        message: 'loại thiết bị không hợp lệ'
                    });
                }




                if (!checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (!checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                if (!checkObjectIDs([customer])) {
                    return resolve({
                        error: true,
                        message: 'khách hàng không hợp lệ'
                    });
                }

                if (!checkObjectIDs([reward])) {
                    return resolve({
                        error: true,
                        message: 'quà không hợp lệ'
                    });
                }

                if (checkin && !checkObjectIDs([checkin])) {
                    return resolve({
                        error: true,
                        message: 'checkin không hợp lệ'
                    });
                }

                const checkExists = await HISTORY_EXCHANGE_GIFT_COLL.findById(history_exchange_giftID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'lịch sử đổi quà không tồn tại'
                    });
                }


                let dataUpdate = {
                    userUpdate
                };
                store && (dataUpdate.store = store);
                employee && (dataUpdate.employee = employee);
                customer && (dataUpdate.customer = customer);
                reward && (dataUpdate.reward = reward);
                point && (dataUpdate.point = point);
                checkin && (dataUpdate.checkin = checkin);
                dataUpdate.deviceType = deviceType;
                dataUpdate.deviceID = deviceID;
                dataUpdate.deviceName = deviceName;
                dataUpdate.lat = lat;
                dataUpdate.lng = lng;

                if (employee) {
                    const employeeInfo = await EMPLOYEE_COLL.findById(employee);
                    dataUpdate.region = employeeInfo.region;
                    dataUpdate.area = employeeInfo.area;
                    dataUpdate.distributor = employeeInfo.distributor;
                }

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: history_exchange_giftID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật history_exchange_gift (không bắt buộc)
         * @param {objectId} history_exchange_giftID
		* @param {object} store
		* @param {object} employee
		* @param {object} customer
		* @param {object} reward
		* @param {number} point
		* @param {object} checkin
		* @param {string} deviceType
		* @param {string} deviceID
		* @param {string} deviceName
		* @param {string} lat
		* @param {string} lng

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        history_exchange_giftID,
        store,
        employee,
        customer,
        reward,
        point,
        checkin,
        deviceType,
        deviceID,
        deviceName,
        lat,
        lng,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([history_exchange_giftID])) {
                    return resolve({
                        error: true,
                        message: 'history_exchange_giftID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                if (customer && !checkObjectIDs([customer])) {
                    return resolve({
                        error: true,
                        message: 'khách hàng không hợp lệ'
                    });
                }

                if (reward && !checkObjectIDs([reward])) {
                    return resolve({
                        error: true,
                        message: 'quà không hợp lệ'
                    });
                }

                if (checkin && !checkObjectIDs([checkin])) {
                    return resolve({
                        error: true,
                        message: 'checkin không hợp lệ'
                    });
                }

                if (deviceType && !["ANDROID", "IOS"].includes(deviceType)) {
                    return resolve({
                        error: true,
                        message: 'loại thiết bị không hợp lệ'
                    });
                }

                const checkExists = await HISTORY_EXCHANGE_GIFT_COLL.findById(history_exchange_giftID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'lịch sử đổi quà không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                store && (dataUpdate.store = store);
                employee && (dataUpdate.employee = employee);
                customer && (dataUpdate.customer = customer);
                reward && (dataUpdate.reward = reward);
                point && (dataUpdate.point = point);
                checkin && (dataUpdate.checkin = checkin);
                deviceType && (dataUpdate.deviceType = deviceType);
                deviceID && (dataUpdate.deviceID = deviceID);
                deviceName && (dataUpdate.deviceName = deviceName);
                lat && (dataUpdate.lat = lat);
                lng && (dataUpdate.lng = lng);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: history_exchange_giftID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa history_exchange_gift 
     * @param {objectId} history_exchange_giftID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(history_exchange_giftID) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 2
                };
                if (!checkObjectIDs([history_exchange_giftID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị history_exchange_giftID không hợp lệ'
                    });
                }


                const infoAfterDelete = await this.updateById(history_exchange_giftID, conditionObj);

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa history_exchange_gift 
     * @param {array} history_exchange_giftID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(history_exchange_giftID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(history_exchange_giftID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị history_exchange_giftID không hợp lệ'
                    });
                }

                const infoAfterDelete = await HISTORY_EXCHANGE_GIFT_COLL.deleteMany({
                    _id: {
                        $in: history_exchange_giftID
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin history_exchange_gift 
     * @param {objectId} history_exchange_giftID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(history_exchange_giftID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([history_exchange_giftID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị history_exchange_giftID không hợp lệ'
                    });
                }

                const infoHistory_exchange_gift = await HISTORY_EXCHANGE_GIFT_COLL.findById(history_exchange_giftID)
                    .populate('region area distributor store employee customer reward checkin')

                if (!infoHistory_exchange_gift) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin lịch sử đổi quà'
                    });
                }

                return resolve({
                    error: false,
                    data: infoHistory_exchange_gift
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách history_exchange_gift 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listHistory_exchange_gift = await HISTORY_EXCHANGE_GIFT_COLL
                    .find({
                        state: 1
                    }).populate('region area distributor store employee customer reward checkin')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listHistory_exchange_gift) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách lịch sử đổi quà'
                    });
                }

                return resolve({
                    error: false,
                    data: listHistory_exchange_gift
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách history_exchange_gift theo bộ lọc
		* @param {number} pointFromNumber
		* @param {number} pointToNumber
		* @param {string} keyword

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        pointFromNumber,
        pointToNumber,
        keyword,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        deviceType: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        lat: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        lng: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                if (pointFromNumber && pointToNumber) {
                    conditionObj.point = {
                        $gte: pointFromNumber,
                        $lt: pointToNumber,
                    };
                }


                const listHistory_exchange_giftByFilter = await HISTORY_EXCHANGE_GIFT_COLL
                    .find(conditionObj).populate('region area distributor store employee customer reward checkin')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listHistory_exchange_giftByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách lịch sử đổi quà"
                    });
                }

                return resolve({
                    error: false,
                    data: listHistory_exchange_giftByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách history_exchange_gift theo bộ lọc (server side)
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir,
        region
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        deviceType: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        lat: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        lng: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                region && (conditionObj.region = ObjectID(region));
                const skip = (page - 1) * limit;

                if(region) {
                    delete conditionObj.region;
                    conditionObj['region._id'] = ObjectID(region);
                }

                const [{ metadata, data: listHistory_exchange_giftByFilter }] = await HISTORY_EXCHANGE_GIFT_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'customers',
                            localField: 'customer',
                            foreignField: '_id',
                            as: 'customer'
                        }
                    },
                    {
                        $unwind: {
                            path: '$customer',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'rewards',
                            localField: 'reward',
                            foreignField: '_id',
                            as: 'reward'
                        }
                    },
                    {
                        $unwind: {
                            path: '$reward',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'checkin_checkouts',
                            localField: 'checkin',
                            foreignField: '_id',
                            as: 'checkin'
                        }
                    },
                    {
                        $unwind: {
                            path: '$checkin',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $facet: {
                            metadata: [ { $count: "total" }, { $addFields: { page } } ],
                            data: [ { $sort: sort }, { $skip: +skip }, { $limit: +limit } ]
                        }
                    }
                ]).allowDiskUse(true);

                const totalHistory_exchange_gift = metadata.length && metadata[0].total;

                if (!listHistory_exchange_giftByFilter) {
                    return resolve({
                        recordsTotal: totalHistory_exchange_gift,
                        recordsFiltered: totalHistory_exchange_gift,
                        data: []
                    });
                }

                const listHistory_exchange_giftDataTable = listHistory_exchange_giftByFilter.map((history_exchange_gift, index) => {
                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${history_exchange_gift._id}" type="checkbox" class="check-record check-record-${history_exchange_gift._id}" _index ="${index + 1}"><label for="${history_exchange_gift._id}"></label></div></td>`,
                        indexSTT: skip + index + 1,
                        region: history_exchange_gift.region ? history_exchange_gift.region.name : '',
                        area: history_exchange_gift.area ? history_exchange_gift.area.name : '',
                        distributor: history_exchange_gift.distributor ? history_exchange_gift.distributor.name : '',
                        store: history_exchange_gift.store ? history_exchange_gift.store.name : '',
                        employee: history_exchange_gift.employee ? history_exchange_gift.employee.fullname : '',
                        customer: history_exchange_gift.customer ? history_exchange_gift.customer.fullname : '',
                        reward: history_exchange_gift.reward ? history_exchange_gift.reward.title : '',
                        point: formatCurrency('###,###.', history_exchange_gift.point),
                        createAt: moment(history_exchange_gift.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalHistory_exchange_gift,
                    recordsFiltered: totalHistory_exchange_gift,
                    data: listHistory_exchange_giftDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách history_exchange_gift theo bộ lọc import
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterImport({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        deviceType: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        lat: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        lng: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }



                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalHistory_exchange_gift = await HISTORY_EXCHANGE_GIFT_COLL.countDocuments(conditionObj);

                const listHistory_exchange_giftByFilter = await HISTORY_EXCHANGE_GIFT_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'customers',
                            localField: 'customer',
                            foreignField: '_id',
                            as: 'customer'
                        }
                    },
                    {
                        $unwind: {
                            path: '$customer',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'rewards',
                            localField: 'reward',
                            foreignField: '_id',
                            as: 'reward'
                        }
                    },
                    {
                        $unwind: {
                            path: '$reward',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'checkin_checkouts',
                            localField: 'checkin',
                            foreignField: '_id',
                            as: 'checkin'
                        }
                    },
                    {
                        $unwind: {
                            path: '$checkin',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listHistory_exchange_giftByFilter) {
                    return resolve({
                        recordsTotal: totalHistory_exchange_gift,
                        recordsFiltered: totalHistory_exchange_gift,
                        data: []
                    });
                }

                return resolve({
                    error: false,
                    recordsTotal: totalHistory_exchange_gift,
                    recordsFiltered: totalHistory_exchange_gift,
                    data: listHistory_exchange_giftByFilter || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc history_exchange_gift
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc history_exchange_gift
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listHistory_exchange_giftByFilter = await HISTORY_EXCHANGE_GIFT_COLL.aggregate(arrayFilter)

                if (!listHistory_exchange_giftByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listHistory_exchange_giftByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listHistory_exchange_giftByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('HH:mm DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Tải file import excel mẫu history_exchange_gift
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    fileImportExcelPreview({
        arrayItemCustomerChoice
    }) {
        return new Promise(async resolve => {
            try {
                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition; // BỘ LỌC && LOẠI IMPORT
                let {
                    listFieldPrimaryKey
                } = condition; // DANH SÁCH PRIMARY KEY

                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        let index = 0;
                        arrayItemCustomerChoice.map((elem) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            if (elem.dataDynamic && elem.dataDynamic.length) {
                                listFieldPrimaryKey = listFieldPrimaryKey.filter(key => key != elem.nameFieldRef); // LỌC PRIMARY KEY MÀ KHÔNG PHẢI REF

                                elem.dataDynamic.map(item => {
                                    workbook.sheet("Report").row(1).cell(index + 1).value(item);
                                    index++;
                                })
                            } else {
                                workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                                index++;
                            }
                        });

                        if (isTrue(condition.checkDownloadDataOld)) { // KIỂM TRA CÓ ĐÍNH ĐÈM DỮ LIỆU CŨ THEO ĐIỀU KIỆN
                            let listItemImport = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].listItemImport; // LIST FIELD ĐÃ ĐƯỢC CẤU HÌNH
                            let {
                                arrayFilter
                            } = this.getConditionArrayFilterExcel(listItemImport, condition.conditionDeleteImport.filter, condition.conditionDeleteImport.condition); // LẤY RA ARRAY ARREGATE

                            let groupByHistory_exchange_gift = {};
                            listFieldPrimaryKey.map(key => {
                                groupByHistory_exchange_gift = {
                                    ...groupByHistory_exchange_gift,
                                    [key]: '$' + key
                                }
                            });

                            arrayFilter = [
                                ...arrayFilter,
                                {
                                    $group: {
                                        _id: {
                                            groupByHistory_exchange_gift
                                        },
                                        listData: {
                                            $addToSet: "$$CURRENT"
                                        },
                                    }
                                }
                            ];

                            const listHistory_exchange_giftByFilter = await HISTORY_EXCHANGE_GIFT_COLL.aggregate(arrayFilter);

                            listHistory_exchange_giftByFilter && listHistory_exchange_giftByFilter.length && listHistory_exchange_giftByFilter.map((item, indexHistory_exchange_gift) => {
                                let indexValue = 0;
                                arrayItemCustomerChoice.map((elem, indexChoice) => {
                                    let variable = elem.name.split('.');

                                    if (elem.dataDynamic && elem.dataDynamic.length) { // KIỂM TRA FIELD CÓ CHỌN DYNAMIC
                                        item.listData && item.listData.length && item.listData.map(value => { // ARRAY DATA DYNAMIC
                                            if (item.listData.length > elem.dataDynamic.length) { // KIỂM TRA ĐỘ DÀI CỦA DATA SO VỚI SỐ CỘT
                                                // TODO: XỬ LÝ NHIỀU DYNAMIC

                                            } else {
                                                elem.dataDynamic.map(dynamic => {
                                                    let valueOfField;
                                                    if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                        let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                        if (objDataOfVariable) {
                                                            valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                        }
                                                    } else {
                                                        valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                                    }

                                                    if (valueOfField == dynamic) { // CHECK NẾU VALUE === CỘT
                                                        let valueImportDynamic = value[elem.variableChoice] ? value[elem.variableChoice] : '';

                                                        // INSERT DỮ LIỆU VÀO BẢNG VỚI FIELD ĐƯỢC CHỌN THEO DẠNG DYNAMIC
                                                        workbook.sheet("Report").row(indexHistory_exchange_gift + 2).cell(indexValue + 1).value(valueImportDynamic);
                                                        indexValue++;
                                                    } else {
                                                        if (elem.dataDynamic && item.listData && elem.dataDynamic.length > item.listData.length) {
                                                            indexValue++;
                                                        }
                                                    }
                                                })
                                            }
                                        })
                                    } else { // DẠNG STATIC
                                        let valueHistory_exchange_gift;
                                        if (variable.length > 1) {
                                            let objDataOfVariable = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                            if (objDataOfVariable) {
                                                valueHistory_exchange_gift = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                            }
                                        } else {
                                            valueHistory_exchange_gift = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                        }

                                        workbook.sheet("Report").row(indexHistory_exchange_gift + 2).cell(indexValue + 1).value(valueHistory_exchange_gift);
                                        indexValue++;
                                    }
                                });
                            });
                        }

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    pathWriteFile,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Upload File Excel Import Lưu Dữ Liệu history_exchange_gift
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    importExcel({
        nameCollParent,
        arrayItemCustomerChoice,
        file,
    }) {
        return new Promise(async resolve => {
            try {

                XlsxPopulate.fromFileAsync(file.path)
                    .then(async workbook => {
                        let listData = [];
                        let index = 2;
                        let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition;

                        const client = await MongoClient.connect(URL_DATABASE);
                        const db = client.db(NAME_DATABASE)

                        for (; true;) {
                            if (arrayItemCustomerChoice && arrayItemCustomerChoice.length) {
                                let conditionObj = {};

                                let totalLength = 0;
                                arrayItemCustomerChoice.map((item, index) => {
                                    if (item.dataDynamic && item.dataDynamic.length) {
                                        totalLength += item.dataDynamic.length;
                                    } else {
                                        totalLength++;
                                    }
                                });

                                let indexOfListField = 0;
                                let checkIsRequire = false;
                                let arrayConditionObjDynamic = [];

                                for (let i = 0; i < totalLength; i++) {
                                    if (arrayItemCustomerChoice[indexOfListField].dataDynamic && arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                        let indexOfDynamic = 1;
                                        for (let valueDynamic of arrayItemCustomerChoice[indexOfListField].dataDynamic) {
                                            let letter = colName(i);
                                            let indexOfCeil = letter.toUpperCase() + index;

                                            let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                            if (variable) {

                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: valueDynamic.trim()
                                                });

                                                let conditionOfOneValueDynamic = {
                                                    [arrayItemCustomerChoice[indexOfListField].variableChoice]: variable
                                                }
                                                if (docs) {
                                                    conditionOfOneValueDynamic = {
                                                        ...conditionOfOneValueDynamic,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    }
                                                }

                                                arrayConditionObjDynamic = [
                                                    ...arrayConditionObjDynamic,
                                                    conditionOfOneValueDynamic
                                                ];
                                            }

                                            if (indexOfDynamic < arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                                i++;
                                            }
                                            indexOfDynamic++;
                                        }
                                    } else {
                                        let letter = colName(i);
                                        let indexOfCeil = letter.toUpperCase() + index;
                                        let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                        if (arrayItemCustomerChoice[indexOfListField].isRequire && !variable) {
                                            checkIsRequire = true;
                                            break;
                                        }
                                        if (arrayItemCustomerChoice[indexOfListField].ref && arrayItemCustomerChoice[indexOfListField].ref != nameCollParent) {
                                            if (arrayItemCustomerChoice[indexOfListField].isRequire) {
                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: variable.trim()
                                                })
                                                // .sort({
                                                //     _id: -1
                                                // })
                                                // .limit(100)
                                                // .toArray();
                                                if (docs) {
                                                    conditionObj = {
                                                        ...conditionObj,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                    };

                                                    if (arrayItemCustomerChoice[indexOfListField].mappingRef && arrayItemCustomerChoice[indexOfListField].mappingRef.length) {
                                                        arrayItemCustomerChoice[indexOfListField].mappingRef.map(mapping => {
                                                            conditionObj = {
                                                                ...conditionObj,
                                                                [mapping]: docs[mapping]
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                [arrayItemCustomerChoice[indexOfListField].name]: variable
                                            };
                                        }
                                    }

                                    indexOfListField++;
                                }
                                if (checkIsRequire) {
                                    break;
                                }

                                conditionObj = {
                                    ...conditionObj,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                }

                                let arrayCondditionObj = []
                                if (arrayConditionObjDynamic && arrayConditionObjDynamic.length) {
                                    arrayConditionObjDynamic.map(item => {
                                        arrayCondditionObj = [
                                            ...arrayCondditionObj,
                                            {
                                                ...conditionObj,
                                                ...item
                                            }
                                        ]
                                    });
                                } else {
                                    arrayCondditionObj = [
                                        ...arrayCondditionObj,
                                        conditionObj
                                    ]
                                }
                                listData = [
                                    ...listData,
                                    ...arrayCondditionObj
                                ];

                                index++;
                            }
                        }

                        await fs.unlinkSync(file.path);

                        if (listData.length) {
                            await this.changeDataImport({
                                condition,
                                listHistory_exchange_gift: listData
                            });
                        } else {
                            return resolve({
                                error: true,
                                message: 'Import thất bại'
                            });
                        }

                        return resolve({
                            error: false,
                            message: 'Import thành công'
                        });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lưu Dữ Liệu Theo Lựa Chọn history_exchange_gift
     * @param {object} listHistory_exchange_gift
     * @param {object} condition
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    changeDataImport({
        condition,
        listHistory_exchange_gift
    }) {
        return new Promise(async resolve => {
            try {
                if (isTrue(condition.delete)) { // XÓA DATA CŨ
                    if (isTrue(condition.deleteAll)) { // XÓA TẤT CẢ DỮ LIỆU
                        console.log("====================XÓA TẤT CẢ DỮ LIỆU====================");
                        await HISTORY_EXCHANGE_GIFT_COLL.deleteMany({});
                        let listDataAfterInsert = await HISTORY_EXCHANGE_GIFT_COLL.insertMany(listHistory_exchange_gift);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    } else { // XÓA VỚI ĐIỀU KIỆN
                        console.log("====================XÓA VỚI ĐIỀU KIỆN====================");

                        /**
                         * ===========================================================================
                         * =========================XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===========================================================================
                         */
                        let {
                            filter,
                            condition: conditionMultiple,
                        } = condition.conditionDeleteImport;

                        if (!filter || !filter.length) {
                            return resolve({
                                error: true,
                                message: 'Filter do not exist'
                            });
                        }

                        let conditionObj = {
                            state: 1,
                            $or: []
                        };

                        if (filter && filter.length) {
                            if (filter.length > 1) {

                                filter.map(filterObj => {
                                    if (filterObj.type === 'ref') {
                                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionFieldRef);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionFieldRef
                                            };
                                        }
                                    } else {
                                        const conditionByFilter = this.getConditionObj(filterObj);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionByFilter);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionByFilter
                                            };
                                        }
                                    }
                                });

                            } else {
                                let {
                                    type,
                                    ref,
                                    fieldRefName
                                } = filter[0];

                                if (type === 'ref') {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(ref, fieldRefName)
                                    };
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(filter[0])
                                    };
                                }
                            }
                        }

                        if (conditionObj.$or && !conditionObj.$or.length) {
                            delete conditionObj.$or;
                        }

                        let listAfterDelete = await HISTORY_EXCHANGE_GIFT_COLL.deleteMany({
                            ...conditionObj
                        });
                        /**
                         * ===============================================================================
                         * =========================END XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===============================================================================
                         */

                        if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                            for (let item of listHistory_exchange_gift) {
                                let listConditionFindOneUpdate = {};
                                let {
                                    listFieldPrimaryKey
                                } = condition;
                                listFieldPrimaryKey.map(elem => {
                                    listConditionFindOneUpdate = {
                                        ...listConditionFindOneUpdate,
                                        [elem]: item[elem]
                                    }
                                });

                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    state: 1
                                }

                                let checkExist = await HISTORY_EXCHANGE_GIFT_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                    $set: item
                                }, {
                                    upsert: true
                                });
                            }
                            return resolve({
                                error: false,
                                message: 'Insert success'
                            });

                        } else { // INSERT CÁI MỚI
                            console.log("====================INSERT CÁI MỚI 2====================");
                            let listDataAfterInsert = await HISTORY_EXCHANGE_GIFT_COLL.insertMany(listHistory_exchange_gift);
                            return resolve({
                                error: false,
                                message: 'Insert success',
                                data: listDataAfterInsert
                            });
                        }
                    }
                } else { // KHÔNG XÓA DATA CŨ
                    if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                        console.log("====================KIỂM TRA TỒN TẠI VÀ UPDATE====================");
                        for (let item of listHistory_exchange_gift) {
                            let listConditionFindOneUpdate = {};
                            let {
                                listFieldPrimaryKey
                            } = condition;
                            listFieldPrimaryKey.map(elem => {
                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    [elem]: item[elem]
                                }
                            });

                            listConditionFindOneUpdate = {
                                ...listConditionFindOneUpdate,
                                state: 1
                            }

                            let checkExist = await HISTORY_EXCHANGE_GIFT_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                $set: item
                            }, {
                                upsert: true
                            });
                        }
                        return resolve({
                            error: false,
                            message: 'Insert success'
                        });
                    } else { // INSERT CÁI MỚI
                        console.log("====================INSERT CÁI MỚI====================");
                        let listDataAfterInsert = await HISTORY_EXCHANGE_GIFT_COLL.insertMany(listHistory_exchange_gift);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    }
                }

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc history_exchange_gift
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
            $or: []
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            let mappingRef = [];
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                    mappingRef = element.mappingRef;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            fieldID: item.fieldID,
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            ref: item.coll,
                            variable: item.name,
                            nameFieldRef,
                            dataEnum: item.dataEnum,
                            isRequire: item.isRequire,
                            dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                            variableChoice: item.variableChoice,
                            nameCollChoice: item.nameCollChoice,
                            mappingRef: mappingRef,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                fieldID: item.fieldID,
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                ref: item.coll,
                                variable: item.name,
                                nameFieldRef,
                                dataEnum: item.dataEnum,
                                isRequire: item.isRequire,
                                dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                variableChoice: item.variableChoice,
                                nameCollChoice: item.nameCollChoice,
                                mappingRef: mappingRef,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    dataEnum: item.dataEnum,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {
            if (filter.length > 1) {

                filter.map(filterObj => {
                    if (filterObj.type === 'ref') {
                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionFieldRef);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionFieldRef
                            };
                        }
                    } else {
                        const conditionByFilter = this.getConditionObj(filterObj);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionByFilter);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionByFilter
                            };
                        }
                    }
                });

            } else {
                let {
                    type,
                    ref,
                    fieldRefName
                } = filter[0];

                if (type === 'ref') {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(ref, fieldRefName)
                    };
                } else {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(filter[0])
                    };
                }
            }
        }

        if (conditionObj.$or && !conditionObj.$or.length) {
            delete conditionObj.$or;
        }



        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let collName = pluralize.plural(item.coll);
                let checkPluralColl = collName[collName.length - 1];

                if (checkPluralColl.toLowerCase() != 's') {
                    collName += 's';
                }

                let lookup = [{
                        $lookup: {
                            from: collName,
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        let collNamePopulate = pluralize.plural(populate.coll);
                        let checkPluralColl = collNamePopulate[collNamePopulate.length - 1];

                        if (checkPluralColl.toLowerCase() != 's') {
                            collNamePopulate += 's';
                        }

                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: collNamePopulate,
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Lấy thông tin history_exchange_gift
     * @param {objectId} history_exchange_giftID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoHistory_exchange_gift({
        history_exchange_giftID,
        select,
        filter = {},
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];
                let conditionObj = {
                    state: 1
                };

                if (!checkObjectIDs([history_exchange_giftID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị history_exchange_giftID không hợp lệ',
                        status: 400
                    });
                }

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                Object.keys(filter).map(key => {
                    if (![].includes(key)) {
                        delete filter[key];
                    }
                });

                let {} = filter;


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'store', 'employee', 'customer', 'reward', 'checkin'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoHistory_exchange_gift = await HISTORY_EXCHANGE_GIFT_COLL
                    .findOne({
                        _id: history_exchange_giftID,
                        ...conditionObj
                    })
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoHistory_exchange_gift = await HISTORY_EXCHANGE_GIFT_COLL.populate(infoHistory_exchange_gift, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoHistory_exchange_gift = await HISTORY_EXCHANGE_GIFT_COLL.populate(infoHistory_exchange_gift, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoHistory_exchange_gift) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin history_exchange_gift',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoHistory_exchange_gift,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách history_exchange_gift
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListHistory_exchange_gifts({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['region', 'area', 'distributor', 'store', 'employee', 'customer', 'reward', 'pointFromNumber', 'pointToNumber', 'checkin', 'fromDate', 'toDate', 'monthCreate', 'yearCreate'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    region,
                    area,
                    distributor,
                    store,
                    employee,
                    customer,
                    reward,
                    pointFromNumber,
                    pointToNumber,
                    checkin,
                    fromDate,
                    toDate,
                    monthCreate,
                    yearCreate,
                } = filter;

                region && (conditionObj.region = region);
                area && (conditionObj.area = area);
                distributor && (conditionObj.distributor = distributor);
                store && (conditionObj.store = store);
                employee && (conditionObj.employee = employee);
                customer && (conditionObj.customer = customer);
                reward && (conditionObj.reward = reward);
                checkin && (conditionObj.checkin = checkin);

                if (fromDate && toDate) {
                    conditionObj.createAt = {
                        $gte: ISOdate(moment(fromDate).startOf('day').format()),
                        $lte: ISOdate(moment(toDate).endOf('day').format()),
                    };
                }

                if(monthCreate){
                    conditionObj.createAt = {
                        $gte: ISOdate(moment(monthCreate).startOf('month').format()),
                        $lte: ISOdate(moment(monthCreate).endOf('month').format()),
                    };
                }

                if(yearCreate){
                    conditionObj.createAt = {
                        $gte: ISOdate(moment(yearCreate).startOf('year').format()),
                        $lte: ISOdate(moment(yearCreate).endOf('year').format()),
                    };
                }

                if (pointFromNumber && pointToNumber) {
                    conditionObj.point = {
                        $gte: pointFromNumber,
                        $lte: pointToNumber,
                    };
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'store', 'employee', 'customer', 'reward', 'checkin', 'sku'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listHistory_exchange_gifts = await HISTORY_EXCHANGE_GIFT_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .populate({
                        path: "reward",
                        select: "amount point gift startTime endTime createAt",
                        populate: {
                            path : 'gift.item',
                            select: "brand name avatar SKU",
                            populate: {
                                path: "brand avatar",
                                select: "name path",
                            }
                        }
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listHistory_exchange_gifts = await HISTORY_EXCHANGE_GIFT_COLL.populate(listHistory_exchange_gifts, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listHistory_exchange_gifts = await HISTORY_EXCHANGE_GIFT_COLL.populate(listHistory_exchange_gifts, `${ref}.${field}`);
                        }
                    }
                }

                if (!listHistory_exchange_gifts) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách history_exchange_gift',
                        status: 400
                    });
                }

                let totalRecord = await HISTORY_EXCHANGE_GIFT_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listHistory_exchange_gifts,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Tạo history_exchange_gift
    * @param {string} deviceType
	* @param {string} deviceID
	* @param {string} deviceName
	* @param {string} lat
	* @param {string} lng
    * @param {objectId} userCreate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    insertHistory_exchange_gift({
        region,
        area,
        distributor,
        store,
        employee,
        customer,
        reward,
        point,
        checkin,
        deviceType,
        deviceID,
        deviceName,
        lat,
        lng,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([userCreate]))
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ',
                        status: 400
                    });



                if (region && !checkObjectIDs(region)) {
                    return resolve({
                        error: true,
                        message: 'Miền không hợp lệ',
                        status: 400
                    });
                }

                if (area && !checkObjectIDs(area)) {
                    return resolve({
                        error: true,
                        message: 'Vùng không hợp lệ',
                        status: 400
                    });
                }

                if (distributor && !checkObjectIDs(distributor)) {
                    return resolve({
                        error: true,
                        message: 'Nhà phân phối không hợp lệ',
                        status: 400
                    });
                }

                if (store && !checkObjectIDs(store)) {
                    return resolve({
                        error: true,
                        message: 'Cửa hàng không hợp lệ',
                        status: 400
                    });
                }

                if (employee && !checkObjectIDs(employee)) {
                    return resolve({
                        error: true,
                        message: 'Nhân viên không hợp lệ',
                        status: 400
                    });
                }

                if (customer && !checkObjectIDs(customer)) {
                    return resolve({
                        error: true,
                        message: 'Khách hàng không hợp lệ',
                        status: 400
                    });
                }

                if (reward && !checkObjectIDs(reward)) {
                    return resolve({
                        error: true,
                        message: 'Quà không hợp lệ',
                        status: 400
                    });
                }

                if (checkin && !checkObjectIDs(checkin)) {
                    return resolve({
                        error: true,
                        message: 'Checkin không hợp lệ',
                        status: 400
                    });
                }

                if (deviceType && !["ANDROID", "IOS"].includes(deviceType)) {
                    return resolve({
                        error: true,
                        message: 'Loại thiết bị không hợp lệ',
                        status: 400
                    });
                }

                let dataInsert = {
                    region,
                    area,
                    distributor,
                    store,
                    employee,
                    customer,
                    reward,
                    point,
                    checkin,
                    deviceType,
                    deviceID,
                    deviceName,
                    lat,
                    lng,
                    userCreate
                };
                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);


                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo HISTORY_EXCHANGE_GIFT thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Cập nhật history_exchange_gift
    * @param {string} deviceType
	* @param {string} deviceID
	* @param {string} deviceName
	* @param {string} lat
	* @param {string} lng
    * @param {objectId} history_exchange_giftID
    * @param {objectId} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    updateHistory_exchange_gift({
        history_exchange_giftID,
        region,
        area,
        distributor,
        store,
        employee,
        customer,
        reward,
        point,
        checkin,
        deviceType,
        deviceID,
        deviceName,
        lat,
        lng,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([history_exchange_giftID]))
                    return resolve({
                        error: true,
                        message: 'history_exchange_giftID không hợp lệ',
                        status: 400
                    });

                if (!checkObjectIDs([userUpdate]))
                    return resolve({
                        error: true,
                        message: 'ID user cập nhật không hợp lệ',
                        status: 400
                    });


                if (region && !checkObjectIDs(region)) {
                    return resolve({
                        error: true,
                        message: 'Miền không hợp lệ',
                        status: 400
                    });
                }

                if (area && !checkObjectIDs(area)) {
                    return resolve({
                        error: true,
                        message: 'Vùng không hợp lệ',
                        status: 400
                    });
                }

                if (distributor && !checkObjectIDs(distributor)) {
                    return resolve({
                        error: true,
                        message: 'Nhà phân phối không hợp lệ',
                        status: 400
                    });
                }

                if (store && !checkObjectIDs(store)) {
                    return resolve({
                        error: true,
                        message: 'Cửa hàng không hợp lệ',
                        status: 400
                    });
                }

                if (employee && !checkObjectIDs(employee)) {
                    return resolve({
                        error: true,
                        message: 'Nhân viên không hợp lệ',
                        status: 400
                    });
                }

                if (customer && !checkObjectIDs(customer)) {
                    return resolve({
                        error: true,
                        message: 'Khách hàng không hợp lệ',
                        status: 400
                    });
                }

                if (reward && !checkObjectIDs(reward)) {
                    return resolve({
                        error: true,
                        message: 'Quà không hợp lệ',
                        status: 400
                    });
                }

                if (checkin && !checkObjectIDs(checkin)) {
                    return resolve({
                        error: true,
                        message: 'Checkin không hợp lệ',
                        status: 400
                    });
                }

                if (deviceType && !["ANDROID", "IOS"].includes(deviceType)) {
                    return resolve({
                        error: true,
                        message: 'Loại thiết bị không hợp lệ',
                        status: 400
                    });
                }

                const checkExists = await HISTORY_EXCHANGE_GIFT_COLL.findOne({
                    _id: history_exchange_giftID,
                    state: 1
                });
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'history_exchange_gift không tồn tại',
                        status: 404
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                region && (dataUpdate.region = region);
                area && (dataUpdate.area = area);
                distributor && (dataUpdate.distributor = distributor);
                store && (dataUpdate.store = store);
                employee && (dataUpdate.employee = employee);
                customer && (dataUpdate.customer = customer);
                reward && (dataUpdate.reward = reward);
                point && (dataUpdate.point = point);
                checkin && (dataUpdate.checkin = checkin);
                deviceType && (dataUpdate.deviceType = deviceType);
                deviceID && (dataUpdate.deviceID = deviceID);
                deviceName && (dataUpdate.deviceName = deviceName);
                lat && (dataUpdate.lat = lat);
                lng && (dataUpdate.lng = lng);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: history_exchange_giftID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa history_exchange_gift
     * @param {objectId} history_exchange_giftID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteHistory_exchange_gift(history_exchange_giftID) {
        return new Promise(async resolve => {
            try {
                let ids = [history_exchange_giftID];

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị history_exchange_giftID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.updateWhereClause({
                    _id: {
                        $in: ids
                    }
                }, {
                    state: 2
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    downloadHistory_exchange_giftExcel({
        keyword,
        filter,
        condition,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1,
                    $or: [],
                    $and: []
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        deviceType: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        lat: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        lng: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj.$and.push(conditionFieldRef);

                                    // conditionObj = {
                                    //     ...conditionObj,
                                    //     ...conditionFieldRef
                                    // };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj.$and.push(conditionByFilter);

                                    // conditionObj = {
                                    //     ...conditionObj,
                                    //     ...conditionByFilter
                                    // };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }
                if (conditionObj.$and && !conditionObj.$and.length) {
                    delete conditionObj.$and;
                }

                const listHistory_exchange_giftByFilter = await HISTORY_EXCHANGE_GIFT_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'customers',
                            localField: 'customer',
                            foreignField: '_id',
                            as: 'customer'
                        }
                    },
                    {
                        $unwind: {
                            path: '$customer',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'rewards',
                            localField: 'reward',
                            foreignField: '_id',
                            as: 'reward'
                        }
                    },
                    {
                        $unwind: {
                            path: '$reward',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'brands',
                            localField: 'reward.brand',
                            foreignField: '_id',
                            as: 'reward.brand'
                        }
                    },
                    {
                        $unwind: {
                            path: '$reward.brand',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                      $addFields: {
                        items: '$$REMOVE',
                        product_ref: { $cond: [{ $eq: ['$reward.gift.kind', 'product'] }, '$reward.gift.item', ""] },
                        gift_additional_ref: { $cond: [{ $eq: ['$reward.gift.kind', 'gift_additional'] }, '$reward.gift.item', ""] },
                      },
                    },
                    {
                        $lookup: {
                            from: 'products',
                            localField: 'product_ref',
                            foreignField: '_id',
                            as: 'product_ref'
                        }
                    },
                    {
                        $unwind: {
                            path: '$product_ref',
                            preserveNullAndEmptyArrays: true
                        },
                    }, 
                    {
                        $lookup: {
                            from: 'gift_additionals',
                            localField: 'gift_additional_ref',
                            foreignField: '_id',
                            as: 'gift_additional_ref'
                        }
                    },
                    {
                        $unwind: {
                            path: '$gift_additional_ref',
                            preserveNullAndEmptyArrays: true
                        },
                    }, 
                    
                    {
                        $lookup: {
                            from: 'checkin_checkouts',
                            localField: 'checkin',
                            foreignField: '_id',
                            as: 'checkin'
                        }
                    },
                    {
                        $unwind: {
                            path: '$checkin',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'shifts',
                            localField: 'checkin.shift',
                            foreignField: '_id',
                            as: 'checkin.shift'
                        }
                    },
                    {
                        $unwind: {
                            path: '$checkin.shift',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $sort: {
                            modifyAt: -1
                        }
                    }
                ])

                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        workbook.sheet("Report").row(1).cell(1).value("Miền");
                        workbook.sheet("Report").row(1).cell(2).value("Vùng");
                        workbook.sheet("Report").row(1).cell(3).value("Mã Nhà Phân Phối");
                        workbook.sheet("Report").row(1).cell(4).value("Nhà Phân Phối");
                        workbook.sheet("Report").row(1).cell(5).value("Tên Cửa Hàng");
                        workbook.sheet("Report").row(1).cell(6).value("Mã Nhân Viên");
                        workbook.sheet("Report").row(1).cell(7).value("Tên Nhân Viên");
                        workbook.sheet("Report").row(1).cell(8).value("SĐT");
                        workbook.sheet("Report").row(1).cell(9).value("Tên Khách Hàng");
                        workbook.sheet("Report").row(1).cell(10).value("SĐT Khách Hàng");
                        workbook.sheet("Report").row(1).cell(11).value("Điểm Tích Lũy");
                        workbook.sheet("Report").row(1).cell(12).value("Điểm Đổi");
                        workbook.sheet("Report").row(1).cell(13).value("Còn Lại");
                        workbook.sheet("Report").row(1).cell(14).value("Mã Sản Phẩm");
                        workbook.sheet("Report").row(1).cell(15).value("Tên Sản Phẩm");
                        workbook.sheet("Report").row(1).cell(16).value("Brand");
                        workbook.sheet("Report").row(1).cell(17).value("Quy Cách");
                        workbook.sheet("Report").row(1).cell(18).value("Số lượng Đổi quà");
                        workbook.sheet("Report").row(1).cell(19).value("Tháng");
                        workbook.sheet("Report").row(1).cell(20).value("Năm");
                        workbook.sheet("Report").row(1).cell(21).value("Ngày");
                        workbook.sheet("Report").row(1).cell(22).value("Ca");

                        listHistory_exchange_giftByFilter && listHistory_exchange_giftByFilter.length && listHistory_exchange_giftByFilter.map((item, index) => {
                            workbook.sheet("Report").row(index + 2).cell(1).value(item.region && item.region.name);
                            workbook.sheet("Report").row(index + 2).cell(2).value(item.area && item.area.name);
                            workbook.sheet("Report").row(index + 2).cell(3).value(item.distributor && item.distributor.code);
                            workbook.sheet("Report").row(index + 2).cell(4).value(item.distributor && item.distributor.name);
                            workbook.sheet("Report").row(index + 2).cell(5).value(item.store && item.store.name);
                            workbook.sheet("Report").row(index + 2).cell(6).value(item.employee && item.employee.code);
                            workbook.sheet("Report").row(index + 2).cell(7).value(item.employee && item.employee.fullname);
                            workbook.sheet("Report").row(index + 2).cell(8).value("");
                            workbook.sheet("Report").row(index + 2).cell(9).value(item.customer && item.customer.fullname);
                            workbook.sheet("Report").row(index + 2).cell(10).value(item.customer && item.customer.phone);
                            workbook.sheet("Report").row(index + 2).cell(11).value(item.beforePoint ? item.beforePoint : 0);
                            workbook.sheet("Report").row(index + 2).cell(12).value(item.point);
                            workbook.sheet("Report").row(index + 2).cell(13).value(item.afterPoint ? item.afterPoint : 0);

                            if (item.product_ref) {
                                workbook.sheet("Report").row(index + 2).cell(14).value(item.product_ref.SKU);
                                workbook.sheet("Report").row(index + 2).cell(15).value(item.product_ref.name);
                                workbook.sheet("Report").row(index + 2).cell(16).value(item.product_ref.description);
                            }

                            if (item.gift_additional_ref) {
                                workbook.sheet("Report").row(index + 2).cell(14).value("");
                                workbook.sheet("Report").row(index + 2).cell(15).value(item.reward && item.reward.title);
                                workbook.sheet("Report").row(index + 2).cell(17).value("");
                            }
                            workbook.sheet("Report").row(index + 2).cell(16).value(item.reward.brand && item.reward.brand.name);
                            workbook.sheet("Report").row(index + 2).cell(18).value(item.amount);

                            const MONTH = moment(item.createAt).format('DD/MM/YYYY').substring(3,5);
                            const YEAR  = moment(item.createAt).format('DD/MM/YYYY').substring(6);
                            
                            workbook.sheet("Report").row(index + 2).cell(19).value(MONTH);
                            workbook.sheet("Report").row(index + 2).cell(20).value(YEAR);
                            workbook.sheet("Report").row(index + 2).cell(21).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));
                            workbook.sheet("Report").row(index + 2).cell(22).value(item.checkin.shift.name);
                        });

                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    status: 200
                                });
                            });
                    });
                
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

}

exports.MODEL = new Model;