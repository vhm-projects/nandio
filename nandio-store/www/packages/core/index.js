const SHIFT_COLL = require('./databases/shift-coll');
const SHIFT_MODEL = require('./models/shift').MODEL;
const SHIFT_ROUTES = require('./apis/shift');
// MARK REQUIRE

module.exports = {
    SHIFT_COLL,
    SHIFT_MODEL,
    SHIFT_ROUTES,
    // MARK EXPORT
}