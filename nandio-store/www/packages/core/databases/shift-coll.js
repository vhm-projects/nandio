"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('shift', {

    /**
     * Tên ca làm việc
     */
    name: {
        type: String,
        required: true,
    },
    /**
     * Giờ bắt đầu
     */
    startTime: {
        type: Date,
        required: true,
    },
    /**
     * Giờ kết thúc
     */
    endTime: {
        type: Date,
        required: true,
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
    /**
     * Thứ tự
     */
    order: {
        type: Number,
    },
});