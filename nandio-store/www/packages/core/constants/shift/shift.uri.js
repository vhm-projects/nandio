const BASE_ROUTE = '/shift';
const API_BASE_ROUTE = '/api/shift';

const CF_ROUTINGS_SHIFT = {
    ADD_SHIFT: `${BASE_ROUTE}/add-shift`,
    UPDATE_SHIFT_BY_ID: `${BASE_ROUTE}/update-shift-by-id`,
    DELETE_SHIFT_BY_ID: `${BASE_ROUTE}/delete/:shiftID`,

    GET_INFO_SHIFT_BY_ID: `${BASE_ROUTE}/info/:shiftID`,
    GET_LIST_SHIFT: `${BASE_ROUTE}/list-shift`,
    GET_LIST_SHIFT_BY_FIELD: `${BASE_ROUTE}/list-shift/:field/:value`,
    GET_LIST_SHIFT_SERVER_SIDE: `${BASE_ROUTE}/list-shift-server-side`,

    UPDATE_SHIFT_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-shift-by-id-v2`,
    DELETE_SHIFT_BY_LIST_ID: `${BASE_ROUTE}/delete-shift-by-list-id`,
    GET_LIST_SHIFT_EXCEL: `${BASE_ROUTE}/list-shift-excel`,

    API_GET_INFO_SHIFT: `${API_BASE_ROUTE}/info-shift/:shiftID`,
    API_GET_LIST_SHIFTS: `${API_BASE_ROUTE}/list-shifts`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_SHIFT = CF_ROUTINGS_SHIFT;