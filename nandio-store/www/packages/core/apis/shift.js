"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const beautifyer = require('js-beautify').js_beautify;
const fs = require('fs');
const moment = require('moment');
const logger = require('../../../config/logger/winston.config');
const chalk = require('chalk');
const log = console.log;
/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_SHIFT_TYPE,
} = require('../constants/shift');
const {
    CF_ROUTINGS_SHIFT
} = require('../constants/shift/shift.uri');

/**
 * MODELS
 */
const SHIFT_MODEL = require('../models/shift').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ SHIFT  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Shift (API, VIEW)
             * Date: 04/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SHIFT.ADD_SHIFT]: {
                config: {
                    scopes: ['create:shift'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm ca làm việc',
                    code: CF_ROUTINGS_SHIFT.ADD_SHIFT,
                    inc: path.resolve(__dirname, '../views/shift/add_shift.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_SHIFT
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            name,
                            startTime,
                            endTime,
                        } = req.body;


                        let infoAfterInsert = await SHIFT_MODEL.insert({
                            name,
                            startTime,
                            endTime,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Shift By Id (API, VIEW)
             * Date: 04/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SHIFT.UPDATE_SHIFT_BY_ID]: {
                config: {
                    scopes: ['update:shift'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật ca làm việc',
                    code: CF_ROUTINGS_SHIFT.UPDATE_SHIFT_BY_ID,
                    inc: path.resolve(__dirname, '../views/shift/update_shift.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            shiftID
                        } = req.query;

                        let infoShift = await SHIFT_MODEL.getInfoById(shiftID);
                        if (infoShift.error) {
                            return res.redirect('/something-went-wrong');
                        }



                        ChildRouter.renderToView(req, res, {
                            infoShift: infoShift.data || {},


                            CF_ROUTINGS_SHIFT
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            shiftID,
                            name,
                            startTime,
                            endTime,
                            status,
                            order,
                        } = req.body;


                        const infoAfterUpdate = await SHIFT_MODEL.update({
                            shiftID,
                            name,
                            startTime,
                            endTime,
                            status,
                            order,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Shift By Id (API)
             * Date: 04/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SHIFT.UPDATE_SHIFT_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:shift'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            shiftID,
                            name,
                            startTime,
                            endTime,
                            status,
                            order,
                        } = req.body;


                        const infoAfterUpdate = await SHIFT_MODEL.updateNotRequire({
                            shiftID,
                            name,
                            startTime,
                            endTime,
                            status,
                            order,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Shift By Id (API)
             * Date: 04/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SHIFT.DELETE_SHIFT_BY_ID]: {
                config: {
                    scopes: ['delete:shift'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            shiftID
                        } = req.params;

                        const infoAfterDelete = await SHIFT_MODEL.deleteById(shiftID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Shift By List Id (API)
             * Date: 04/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SHIFT.DELETE_SHIFT_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:shift'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            shiftID
                        } = req.body;

                        const infoAfterDelete = await SHIFT_MODEL.deleteByListId(shiftID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Shift By Id (API)
             * Date: 04/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SHIFT.GET_INFO_SHIFT_BY_ID]: {
                config: {
                    scopes: ['read:info_shift'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            shiftID
                        } = req.params;

                        const infoShiftById = await SHIFT_MODEL.getInfoById(shiftID);
                        res.json(infoShiftById);
                    }]
                },
            },

            /**
             * Function: Get List Shift (API, VIEW)
             * Date: 04/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SHIFT.GET_LIST_SHIFT]: {
                config: {
                    scopes: ['read:list_shift'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách ca làm việc',
                    code: CF_ROUTINGS_SHIFT.GET_LIST_SHIFT,
                    inc: path.resolve(__dirname, '../views/shift/list_shifts.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            startTimeDateRange,
                            endTimeDateRange,
                            status,
                            typeGetList
                        } = req.query;

                        let listShifts = [];
                        if (typeGetList === 'FILTER') {
                            listShifts = await SHIFT_MODEL.getListByFilter({
                                keyword,
                                startTimeDateRange,
                                endTimeDateRange,
                                status,
                            });
                        } else {
                            listShifts = await SHIFT_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listShifts: listShifts.data || [],
                            STATUS_SHIFT_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Shift By Field (API, VIEW)
             * Date: 04/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SHIFT.GET_LIST_SHIFT_BY_FIELD]: {
                config: {
                    scopes: ['read:list_shift'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách ca làm việc',
                    code: CF_ROUTINGS_SHIFT.GET_LIST_SHIFT_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/shift/list_shifts.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            startTimeDateRange,
                            endTimeDateRange,
                            status,
                            type
                        } = req.query;

                        let listShifts = await SHIFT_MODEL.getListByFilter({
                            keyword,
                            startTimeDateRange,
                            endTimeDateRange,
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listShifts: listShifts.data || [],
                            STATUS_SHIFT_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Shift Server Side (API)
             * Date: 04/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SHIFT.GET_LIST_SHIFT_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_shift'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listShiftServerSide = await SHIFT_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listShiftServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Shift Excel Server Side (API)
             * Date: 04/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SHIFT.GET_LIST_SHIFT_EXCEL]: {
                config: {
                    scopes: ['read:list_shift'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            filter,
                            condition,
                            nameOfParentColl,
                            objFilterStatic,
                            order,
                            keyword
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let conditionObj = SHIFT_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword)
                        let listShift = await SHIFT_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice
                        })

                        res.json(listShift)
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 04/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SHIFT.API_GET_INFO_SHIFT]: {
                config: {
                    scopes: ['read:info_shift'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            shiftID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoShift = await SHIFT_MODEL.getInfoShift({
                            shiftID,
                            select,
                            explain
                        });
                        res.json(infoShift);
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 04/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_SHIFT.API_GET_LIST_SHIFTS]: {
                config: {
                    scopes: ['read:list_shift'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listShifts = await SHIFT_MODEL.getListShifts({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listShifts);
                    }]
                },
            },

        }
    }
};