const BASE_ROUTE = '/type_employee';
const API_BASE_ROUTE = '/api/type_employee';

const CF_ROUTINGS_TYPE_EMPLOYEE = {
    ADD_TYPE_EMPLOYEE: `${BASE_ROUTE}/add-type_employee`,
    UPDATE_TYPE_EMPLOYEE_BY_ID: `${BASE_ROUTE}/update-type_employee-by-id`,
    DELETE_TYPE_EMPLOYEE_BY_ID: `${BASE_ROUTE}/delete/:type_employeeID`,

    GET_INFO_TYPE_EMPLOYEE_BY_ID: `${BASE_ROUTE}/info/:type_employeeID`,
    GET_LIST_TYPE_EMPLOYEE: `${BASE_ROUTE}/list-type_employee`,
    GET_LIST_TYPE_EMPLOYEE_BY_FIELD: `${BASE_ROUTE}/list-type_employee/:field/:value`,
    GET_LIST_TYPE_EMPLOYEE_SERVER_SIDE: `${BASE_ROUTE}/list-type_employee-server-side`,

    UPDATE_TYPE_EMPLOYEE_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-type_employee-by-id-v2`,
    DELETE_TYPE_EMPLOYEE_BY_LIST_ID: `${BASE_ROUTE}/delete-type_employee-by-list-id`,
    GET_LIST_TYPE_EMPLOYEE_EXCEL: `${BASE_ROUTE}/list-type_employee-excel`,

    API_GET_INFO_TYPE_EMPLOYEE: `${API_BASE_ROUTE}/info-type_employee/:type_employeeID`,
    API_GET_LIST_TYPE_EMPLOYEES: `${API_BASE_ROUTE}/list-type_employees`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_TYPE_EMPLOYEE = CF_ROUTINGS_TYPE_EMPLOYEE;