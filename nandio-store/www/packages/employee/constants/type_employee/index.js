/**
 * Trạng thái 
 * 1: Hoạt động
 * 2: Không hoạt động
 */
exports.STATUS_TYPE_EMPLOYEE_TYPE = {

    "1": {
        value: "Hoạt động",
        color: "#0b51b7"
    },

    "2": {
        value: "Không hoạt động",
        color: "#d63031"
    },

};

/**
 * Loại 
 * 1: NA
 * 2: PG
 */
exports.TYPE_TYPE_EMPLOYEE_TYPE = {

    "1": {
        value: "NA",
        color: "#0b51b7"
    },

    "2": {
        value: "PG",
        color: "#d63031"
    },

};