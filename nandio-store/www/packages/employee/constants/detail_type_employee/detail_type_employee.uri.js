const BASE_ROUTE = '/detail_type_employee';
const API_BASE_ROUTE = '/api/detail_type_employee';

const CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE = {
    ADD_DETAIL_TYPE_EMPLOYEE: `${BASE_ROUTE}/add-detail_type_employee`,
    UPDATE_DETAIL_TYPE_EMPLOYEE_BY_ID: `${BASE_ROUTE}/update-detail_type_employee-by-id`,
    DELETE_DETAIL_TYPE_EMPLOYEE_BY_ID: `${BASE_ROUTE}/delete/:detail_type_employeeID`,

    GET_INFO_DETAIL_TYPE_EMPLOYEE_BY_ID: `${BASE_ROUTE}/info/:detail_type_employeeID`,
    GET_LIST_DETAIL_TYPE_EMPLOYEE: `${BASE_ROUTE}/list-detail_type_employee`,
    GET_LIST_DETAIL_TYPE_EMPLOYEE_BY_FIELD: `${BASE_ROUTE}/list-detail_type_employee/:field/:value`,
    GET_LIST_DETAIL_TYPE_EMPLOYEE_SERVER_SIDE: `${BASE_ROUTE}/list-detail_type_employee-server-side`,

    UPDATE_DETAIL_TYPE_EMPLOYEE_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-detail_type_employee-by-id-v2`,
    DELETE_DETAIL_TYPE_EMPLOYEE_BY_LIST_ID: `${BASE_ROUTE}/delete-detail_type_employee-by-list-id`,
    GET_LIST_DETAIL_TYPE_EMPLOYEE_EXCEL: `${BASE_ROUTE}/list-detail_type_employee-excel`,



    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE = CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE;