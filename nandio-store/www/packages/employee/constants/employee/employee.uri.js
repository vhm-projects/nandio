const BASE_ROUTE = '/employee';
const API_BASE_ROUTE = '/api/employee';

const CF_ROUTINGS_EMPLOYEE = {
    ADD_EMPLOYEE: `${BASE_ROUTE}/add-employee`,
    UPDATE_EMPLOYEE_BY_ID: `${BASE_ROUTE}/update-employee-by-id`,
    DELETE_EMPLOYEE_BY_ID: `${BASE_ROUTE}/delete/:employeeID`,

    GET_INFO_EMPLOYEE_BY_ID: `${BASE_ROUTE}/info/:employeeID`,
    GET_LIST_EMPLOYEE: `${BASE_ROUTE}/list-employee`,
    GET_LIST_EMPLOYEE_BY_FIELD: `${BASE_ROUTE}/list-employee/:field/:value`,
    GET_LIST_EMPLOYEE_SERVER_SIDE: `${BASE_ROUTE}/list-employee-server-side`,

    UPDATE_EMPLOYEE_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-employee-by-id-v2`,
    DELETE_EMPLOYEE_BY_LIST_ID: `${BASE_ROUTE}/delete-employee-by-list-id`,
    GET_LIST_EMPLOYEE_EXCEL: `${BASE_ROUTE}/list-employee-excel`,
    DOWNLOAD_LIST_EMPLOYEE_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-employee-excel-export`,

    API_GET_INFO_EMPLOYEE: `${API_BASE_ROUTE}/info-employee/:employeeID`,
    API_GET_LIST_EMPLOYEES: `${API_BASE_ROUTE}/list-employees`,
    API_IMPORT_EMPLOYEE: `${API_BASE_ROUTE}/import-employee`,
    API_DOWNLOAD_EMPLOYEE: `${API_BASE_ROUTE}/download-employee`,
    API_UPDATE_REGISTER_FACE: `${API_BASE_ROUTE}/update-register-face`,
    API_CLEAR_IMAGE_FACE_REGISTER: `${API_BASE_ROUTE}/clear-image-face-register`,

    API_LOGIN_EMPLOYEE: `${API_BASE_ROUTE}/login`,
    API_REGISTER_EMPLOYEE: `${API_BASE_ROUTE}/register`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_EMPLOYEE = CF_ROUTINGS_EMPLOYEE;