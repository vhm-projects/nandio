const BASE_ROUTE = '/employee_hr';
const API_BASE_ROUTE = '/api/employee_hr';

const CF_ROUTINGS_EMPLOYEE_HR = {
    ADD_EMPLOYEE_HR: `${BASE_ROUTE}/add-employee_hr`,
    UPDATE_EMPLOYEE_HR_BY_ID: `${BASE_ROUTE}/update-employee_hr-by-id`,
    DELETE_EMPLOYEE_HR_BY_ID: `${BASE_ROUTE}/delete/:employee_hrID`,

    GET_INFO_EMPLOYEE_HR_BY_ID: `${BASE_ROUTE}/info/:employee_hrID`,
    GET_LIST_EMPLOYEE_HR: `${BASE_ROUTE}/list-employee_hr`,
    GET_LIST_EMPLOYEE_HR_BY_FIELD: `${BASE_ROUTE}/list-employee_hr/:field/:value`,
    GET_LIST_EMPLOYEE_HR_SERVER_SIDE: `${BASE_ROUTE}/list-employee_hr-server-side`,

    UPDATE_EMPLOYEE_HR_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-employee_hr-by-id-v2`,
    DELETE_EMPLOYEE_HR_BY_LIST_ID: `${BASE_ROUTE}/delete-employee_hr-by-list-id`,
    GET_LIST_EMPLOYEE_HR_EXCEL: `${BASE_ROUTE}/list-employee_hr-excel`,

    API_GET_INFO_EMPLOYEE_HR: `${API_BASE_ROUTE}/info-employee_hr/:employee_hrID`,
    API_GET_LIST_EMPLOYEE_HRS: `${API_BASE_ROUTE}/list-employee_hrs`,
    API_IMPORT_EMPLOYEE_HR: `${API_BASE_ROUTE}/import-employee-hr`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_EMPLOYEE_HR = CF_ROUTINGS_EMPLOYEE_HR;