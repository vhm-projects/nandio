/**
 * EXTERNAL PACKAGE
 */
const path = require('path');


/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const { uploadSingle } = require('../../../config/cf_helpers_multer');
const {
    STATUS_EMPLOYEE_HR_TYPE,
} = require('../constants/employee_hr');
const {
    CF_ROUTINGS_EMPLOYEE_HR
} = require('../constants/employee_hr/employee_hr.uri');

/**
 * MODELS
 */
const EMPLOYEE_HR_MODEL = require('../models/employee_hr').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const EMPLOYEE_COLL = require('../databases/employee-coll');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ EMPLOYEE_HR  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Employee_hr (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_HR.ADD_EMPLOYEE_HR]: {
                config: {
                    scopes: ['create:employee_hr'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Employee_hr',
                    code: CF_ROUTINGS_EMPLOYEE_HR.ADD_EMPLOYEE_HR,
                    inc: path.resolve(__dirname, '../views/employee_hr/add_employee_hr.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let condition = { state: 1, status: 1 };
                        region && (condition.region = region);

                        let listEmployees = await EMPLOYEE_COLL
                            .find(condition)
                            .sort({
                                modifyAt: -1
                            })
                            .lean()

                        ChildRouter.renderToView(req, res, {
                            listEmployees,
                            CF_ROUTINGS_EMPLOYEE_HR
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            code,
                            fullname,
                            from,
                            to,
                            employee,
                        } = req.body;


                        let infoAfterInsert = await EMPLOYEE_HR_MODEL.insert({
                            code,
                            fullname,
                            from,
                            to,
                            employee,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Employee_hr By Id (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_HR.UPDATE_EMPLOYEE_HR_BY_ID]: {
                config: {
                    scopes: ['update:employee_hr'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Employee_hr',
                    code: CF_ROUTINGS_EMPLOYEE_HR.UPDATE_EMPLOYEE_HR_BY_ID,
                    inc: path.resolve(__dirname, '../views/employee_hr/update_employee_hr.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            employee_hrID
                        } = req.query;
                        let { region } = req.user;
                        let condition = { state: 1, status: 1 };
                        region && (condition.region = region);

                        let infoEmployee_hr = await EMPLOYEE_HR_MODEL.getInfoById(employee_hrID);
                        if (infoEmployee_hr.error) {
                            return res.redirect('/something-went-wrong');
                        }

                        let listEmployees = await EMPLOYEE_COLL
                            .find(condition)
                            .sort({
                                modifyAt: -1
                            }).lean()

                        ChildRouter.renderToView(req, res, {
                            infoEmployee_hr: infoEmployee_hr.data || {},
                            listEmployees,
                            CF_ROUTINGS_EMPLOYEE_HR
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            employee_hrID,
                            code,
                            fullname,
                            from,
                            to,
                            employee,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await EMPLOYEE_HR_MODEL.update({
                            employee_hrID,
                            code,
                            fullname,
                            from,
                            to,
                            employee,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Employee_hr By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_HR.UPDATE_EMPLOYEE_HR_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:employee_hr'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            employee_hrID,
                            code,
                            fullname,
                            from,
                            to,
                            employee,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await EMPLOYEE_HR_MODEL.updateNotRequire({
                            employee_hrID,
                            code,
                            fullname,
                            from,
                            to,
                            employee,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Employee_hr By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_HR.DELETE_EMPLOYEE_HR_BY_ID]: {
                config: {
                    scopes: ['delete:employee_hr'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            employee_hrID
                        } = req.params;

                        const infoAfterDelete = await EMPLOYEE_HR_MODEL.deleteById(employee_hrID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Employee_hr By List Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_HR.DELETE_EMPLOYEE_HR_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:employee_hr'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            employee_hrID
                        } = req.body;

                        const infoAfterDelete = await EMPLOYEE_HR_MODEL.deleteByListId(employee_hrID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Employee_hr By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_HR.GET_INFO_EMPLOYEE_HR_BY_ID]: {
                config: {
                    scopes: ['read:info_employee_hr'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            employee_hrID
                        } = req.params;

                        const infoEmployee_hrById = await EMPLOYEE_HR_MODEL.getInfoById(employee_hrID);
                        res.json(infoEmployee_hrById);
                    }]
                },
            },

            /**
             * Function: Get List Employee_hr (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_HR.GET_LIST_EMPLOYEE_HR]: {
                config: {
                    scopes: ['read:list_employee_hr'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách nhân viên HR',
                    code: CF_ROUTINGS_EMPLOYEE_HR.GET_LIST_EMPLOYEE_HR,
                    inc: path.resolve(__dirname, '../views/employee_hr/list_employee_hrs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let {
                            keyword,
                            fromDateRange,
                            toDateRange,
                            status,
                            typeGetList
                        } = req.query;

                        let listEmployee_hrs = [];
                        if (typeGetList === 'FILTER') {
                            listEmployee_hrs = await EMPLOYEE_HR_MODEL.getListByFilter({
                                keyword,
                                fromDateRange,
                                toDateRange,
                                status,
                                region
                            });
                        } else {
                            listEmployee_hrs = await EMPLOYEE_HR_MODEL.getList({ region });
                        }

                        ChildRouter.renderToView(req, res, {
                            listEmployee_hrs: listEmployee_hrs.data || [],
                            STATUS_EMPLOYEE_HR_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Employee_hr By Field (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_HR.GET_LIST_EMPLOYEE_HR_BY_FIELD]: {
                config: {
                    scopes: ['read:list_employee_hr'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Employee_hr by field isStatus',
                    code: CF_ROUTINGS_EMPLOYEE_HR.GET_LIST_EMPLOYEE_HR_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/employee_hr/list_employee_hrs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            fromDateRange,
                            toDateRange,
                            status,
                            type
                        } = req.query;

                        let listEmployee_hrs = await EMPLOYEE_HR_MODEL.getListByFilter({
                            keyword,
                            fromDateRange,
                            toDateRange,
                            status,
                            [field]: value,
                            region
                        });

                        ChildRouter.renderToView(req, res, {
                            listEmployee_hrs: listEmployee_hrs.data || [],
                            STATUS_EMPLOYEE_HR_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Employee_hr Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_HR.GET_LIST_EMPLOYEE_HR_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_employee_hr'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listEmployee_hrServerSide = await EMPLOYEE_HR_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(listEmployee_hrServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Employee_hr Excel Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_HR.GET_LIST_EMPLOYEE_HR_EXCEL]: {
                config: {
                    scopes: ['read:list_employee_hr'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            filter,
                            condition,
                            nameOfParentColl,
                            objFilterStatic,
                            order,
                            keyword
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let conditionObj = EMPLOYEE_HR_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword)
                        let listEmployee_hr = await EMPLOYEE_HR_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice
                        })

                        res.json(listEmployee_hr)
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_HR.API_GET_INFO_EMPLOYEE_HR]: {
                config: {
                    scopes: ['read:info_employee_hr'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            employee_hrID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoEmployee_hr = await EMPLOYEE_HR_MODEL.getInfoEmployee_hr({
                            employee_hrID,
                            select,
                            explain
                        });
                        res.json(infoEmployee_hr);
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE_HR.API_GET_LIST_EMPLOYEE_HRS]: {
                config: {
                    scopes: ['read:list_employee_hr'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { region: regionAdmin } = req.user;
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listEmployee_hrs = await EMPLOYEE_HR_MODEL.getListEmployee_hrs({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                            regionAdmin
                        });
                        res.json(listEmployee_hrs);
                    }]
                },
            },

            /**
             * Function: Import excel nhân viên HR (API)
             * Date: 17/11/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_EMPLOYEE_HR.API_IMPORT_EMPLOYEE_HR]: {
                config: {
                    scopes: ['create:employee_hr'],
                    type: 'json',
                },
                methods: {
                    post: [ uploadSingle, async function(req, res) {
                        const infoAfterImport = await EMPLOYEE_HR_MODEL.importEmployeeHR(req.file);
                        res.json(infoAfterImport);
                    }]
                },
            },

        }
    }
};