/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const beautifyer = require('js-beautify').js_beautify;
const fs = require('fs');
const moment = require('moment');
const logger = require('../../../config/logger/winston.config');
const chalk = require('chalk');
const log = console.log;
/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_TYPE_EMPLOYEE_TYPE,
    TYPE_TYPE_EMPLOYEE_TYPE,
} = require('../constants/type_employee');
const {
    CF_ROUTINGS_TYPE_EMPLOYEE
} = require('../constants/type_employee/type_employee.uri');

/**
 * MODELS
 */
const TYPE_EMPLOYEE_MODEL = require('../models/type_employee').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ TYPE_EMPLOYEE  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Type_employee (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_TYPE_EMPLOYEE.ADD_TYPE_EMPLOYEE]: {
                config: {
                    scopes: ['create:type_employee'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Type_employee',
                    code: CF_ROUTINGS_TYPE_EMPLOYEE.ADD_TYPE_EMPLOYEE,
                    inc: path.resolve(__dirname, '../views/type_employee/add_type_employee.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_TYPE_EMPLOYEE
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            name,
                            type,
                        } = req.body;


                        let infoAfterInsert = await TYPE_EMPLOYEE_MODEL.insert({
                            name,
                            type,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Type_employee By Id (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_TYPE_EMPLOYEE.UPDATE_TYPE_EMPLOYEE_BY_ID]: {
                config: {
                    scopes: ['update:type_employee'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Type_employee',
                    code: CF_ROUTINGS_TYPE_EMPLOYEE.UPDATE_TYPE_EMPLOYEE_BY_ID,
                    inc: path.resolve(__dirname, '../views/type_employee/update_type_employee.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            type_employeeID
                        } = req.query;

                        let infoType_employee = await TYPE_EMPLOYEE_MODEL.getInfoById(type_employeeID);
                        if (infoType_employee.error) {
                            return res.redirect('/something-went-wrong');
                        }



                        ChildRouter.renderToView(req, res, {
                            infoType_employee: infoType_employee.data || {},


                            CF_ROUTINGS_TYPE_EMPLOYEE
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            type_employeeID,
                            name,
                            order,
                            status,
                            type,
                        } = req.body;


                        const infoAfterUpdate = await TYPE_EMPLOYEE_MODEL.update({
                            type_employeeID,
                            name,
                            order,
                            status,
                            type,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Type_employee By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_TYPE_EMPLOYEE.UPDATE_TYPE_EMPLOYEE_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:type_employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            type_employeeID,
                            name,
                            order,
                            status,
                            type,
                        } = req.body;


                        const infoAfterUpdate = await TYPE_EMPLOYEE_MODEL.updateNotRequire({
                            type_employeeID,
                            name,
                            order,
                            status,
                            type,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Type_employee By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_TYPE_EMPLOYEE.DELETE_TYPE_EMPLOYEE_BY_ID]: {
                config: {
                    scopes: ['delete:type_employee'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            type_employeeID
                        } = req.params;

                        const infoAfterDelete = await TYPE_EMPLOYEE_MODEL.deleteById(type_employeeID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Type_employee By List Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_TYPE_EMPLOYEE.DELETE_TYPE_EMPLOYEE_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:type_employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            type_employeeID
                        } = req.body;

                        const infoAfterDelete = await TYPE_EMPLOYEE_MODEL.deleteByListId(type_employeeID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Type_employee By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_TYPE_EMPLOYEE.GET_INFO_TYPE_EMPLOYEE_BY_ID]: {
                config: {
                    scopes: ['read:info_type_employee'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            type_employeeID
                        } = req.params;

                        const infoType_employeeById = await TYPE_EMPLOYEE_MODEL.getInfoById(type_employeeID);
                        res.json(infoType_employeeById);
                    }]
                },
            },

            /**
             * Function: Get List Type_employee (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_TYPE_EMPLOYEE.GET_LIST_TYPE_EMPLOYEE]: {
                config: {
                    scopes: ['read:list_type_employee'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Type_employee',
                    code: CF_ROUTINGS_TYPE_EMPLOYEE.GET_LIST_TYPE_EMPLOYEE,
                    inc: path.resolve(__dirname, '../views/type_employee/list_type_employees.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            status,
                            type,
                            typeGetList
                        } = req.query;

                        let listType_employees = [];
                        if (typeGetList === 'FILTER') {
                            listType_employees = await TYPE_EMPLOYEE_MODEL.getListByFilter({
                                keyword,
                                status,
                                type,
                            });
                        } else {
                            listType_employees = await TYPE_EMPLOYEE_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listType_employees: listType_employees.data || [],
                            STATUS_TYPE_EMPLOYEE_TYPE,
                            TYPE_TYPE_EMPLOYEE_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Type_employee By Field (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_TYPE_EMPLOYEE.GET_LIST_TYPE_EMPLOYEE_BY_FIELD]: {
                config: {
                    scopes: ['read:list_type_employee'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Type_employee by field isStatus',
                    code: CF_ROUTINGS_TYPE_EMPLOYEE.GET_LIST_TYPE_EMPLOYEE_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/type_employee/list_type_employees.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            status,
                            type
                        } = req.query;

                        let listType_employees = await TYPE_EMPLOYEE_MODEL.getListByFilter({
                            keyword,
                            status,
                            type,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listType_employees: listType_employees.data || [],
                            STATUS_TYPE_EMPLOYEE_TYPE,
                            TYPE_TYPE_EMPLOYEE_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Type_employee Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_TYPE_EMPLOYEE.GET_LIST_TYPE_EMPLOYEE_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_type_employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listType_employeeServerSide = await TYPE_EMPLOYEE_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listType_employeeServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Type_employee Excel Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_TYPE_EMPLOYEE.GET_LIST_TYPE_EMPLOYEE_EXCEL]: {
                config: {
                    scopes: ['read:list_type_employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            filter,
                            condition,
                            nameOfParentColl,
                            objFilterStatic,
                            order,
                            keyword
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let conditionObj = TYPE_EMPLOYEE_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword)
                        let listType_employee = await TYPE_EMPLOYEE_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice
                        })

                        res.json(listType_employee)
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_TYPE_EMPLOYEE.API_GET_INFO_TYPE_EMPLOYEE]: {
                config: {
                    scopes: ['read:info_type_employee'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            type_employeeID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoType_employee = await TYPE_EMPLOYEE_MODEL.getInfoType_employee({
                            type_employeeID,
                            select,
                            explain
                        });
                        res.json(infoType_employee);
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_TYPE_EMPLOYEE.API_GET_LIST_TYPE_EMPLOYEES]: {
                config: {
                    scopes: ['read:list_type_employee'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listType_employees = await TYPE_EMPLOYEE_MODEL.getListType_employees({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listType_employees);
                    }]
                },
            },

        }
    }
};