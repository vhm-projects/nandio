/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const { uploadSingle } = require('../../../config/cf_helpers_multer');
const {
    STATUS_EMPLOYEE_TYPE,
} = require('../constants/employee');
const {
    CF_ROUTINGS_EMPLOYEE
} = require('../constants/employee/employee.uri');

/**
 * MODELS
 */
const EMPLOYEE_MODEL = require('../models/employee').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;
const REFERRAL_CODE_MODEL = require('../../customer/models/referral_code').MODEL;

/**
 * COLLECTIONS
 */
const {
    REGION_COLL
} = require('../../region_area');

const {
    AREA_COLL
} = require('../../region_area');

const {
    DISTRIBUTOR_COLL
} = require('../../distributor');

const TYPE_EMPLOYEE_COLL = require('../databases/type_employee-coll');
const DETAIL_TYPE_EMPLOYEE_COLL = require('../databases/detail_type_employee-coll');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ EMPLOYEE  ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: Get List area By parent (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            '/employee/list-area-by-parent': {
                config: {
                    scopes: ['read:list_employee'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            region
                        } = req.query;

                        const listAreaByParent = await AREA_COLL
                            .find({
                                region
                            })
                            .lean();
                        res.json(listAreaByParent);
                    }]
                },
            },

            /**
             * Function: Get List distributor By parent (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            '/employee/list-distributor-by-parent': {
                config: {
                    scopes: ['read:list_employee'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            area
                        } = req.query;

                        const listDistributorByParent = await DISTRIBUTOR_COLL
                            .find({
                                area
                            })
                            .lean();
                        res.json(listDistributorByParent);
                    }]
                },
            },

            /**
             * Function: Insert Employee (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.ADD_EMPLOYEE]: {
                config: {
                    scopes: ['create:employee'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm nhân viên',
                    code: CF_ROUTINGS_EMPLOYEE.ADD_EMPLOYEE,
                    inc: path.resolve(__dirname, '../views/employee/add_employee.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;

                        let condition = { state: 1, status: 1 };
                        region && (condition._id = region);

                        let listRegions = await REGION_COLL.find(condition).lean();

                        let listType_employees = await TYPE_EMPLOYEE_COLL.find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1
                            })
                            .lean()

                        let listDetail_type_employees = await DETAIL_TYPE_EMPLOYEE_COLL.find({
                            state: 1,
                            status: 1
                        })
                        .sort({
                            modifyAt: -1
                        })
                        .lean()

                        ChildRouter.renderToView(req, res, {
                            listRegions,
                            listType_employees,
                            listDetail_type_employees,
                            CF_ROUTINGS_EMPLOYEE
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            code,
                            fullname,
                            username,
                            email,
                            password,
                            confirmPassword,
                            region,
                            area,
                            distributor,
                            type_employee,
                            detail_type_employee,
                            numberK,
                        } = req.body;

                        let infoAfterInsert = await EMPLOYEE_MODEL.insert({
                            code,
                            fullname,
                            username,
                            email,
                            password,
                            confirmPassword,
                            region,
                            area,
                            distributor,
                            type_employee,
                            detail_type_employee,
                            numberK,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Employee By Id (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.UPDATE_EMPLOYEE_BY_ID]: {
                config: {
                    scopes: ['update:employee'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật nhân viên',
                    code: CF_ROUTINGS_EMPLOYEE.UPDATE_EMPLOYEE_BY_ID,
                    inc: path.resolve(__dirname, '../views/employee/update_employee.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            employeeID
                        } = req.query;
                        let { region } = req.user;

                        let condition = { state: 1, status: 1 };
                        region && (condition._id = region);

                        let infoEmployee = await EMPLOYEE_MODEL.getInfoById(employeeID);
                        if (infoEmployee.error) {
                            return res.redirect('/something-went-wrong');
                        }

                        let listRegions = await REGION_COLL
                            .find(condition)
                            .sort({
                                modifyAt: -1
                            }).lean()

                        let listAreas = [];
                        if (infoEmployee.data.region) {
                            listAreas = await AREA_COLL.find({
                                state: 1,
                                status: 1,
                                region: infoEmployee.data.region._id
                            }).sort({
                                modifyAt: -1
                            }).lean()
                        }

                        let listDistributors = [];
                        if (infoEmployee.data.area) {
                            listDistributors = await DISTRIBUTOR_COLL.find({
                                state: 1,
                                status: 1,
                                area: infoEmployee.data.area._id
                            }).sort({
                                modifyAt: -1
                            }).lean()
                        }

                        let listType_employees = await TYPE_EMPLOYEE_COLL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean()

                        let listDetail_type_employees = await DETAIL_TYPE_EMPLOYEE_COLL.find({
                            state: 1,
                            status: 1
                        }).lean()
                        .sort({
                            modifyAt: -1
                        })

                        const listImagesFace = await EMPLOYEE_MODEL.getListImagesFace({ employeeID: infoEmployee.data._id });
                        ChildRouter.renderToView(req, res, {
                            infoEmployee: infoEmployee.data || {},
                            listImagesFace,
                            listRegions,
                            listAreas,
                            listDistributors,
                            listType_employees,
                            listDetail_type_employees,
                            CF_ROUTINGS_EMPLOYEE
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            employeeID,
                            code,
                            fullname,
                            username,
                            email,
                            phone,
                            password,
                            confirmPassword,
                            region,
                            area,
                            distributor,
                            type_employee,
                            detail_type_employee,
                            numberK,
                            status,
                        } = req.body;

                        const infoAfterUpdate = await EMPLOYEE_MODEL.update({
                            employeeID,
                            code,
                            fullname,
                            username,
                            email,
                            phone,
                            password,
                            confirmPassword,
                            region,
                            area,
                            distributor,
                            type_employee,
                            detail_type_employee,
                            numberK,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Employee By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.UPDATE_EMPLOYEE_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            employeeID,
                            code,
                            fullname,
                            username,
                            email,
                            password,
                            region,
                            area,
                            distributor,
                            type_employee,
                            numberK,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await EMPLOYEE_MODEL.updateNotRequire({
                            employeeID,
                            code,
                            fullname,
                            username,
                            email,
                            password,
                            region,
                            area,
                            distributor,
                            type_employee,
                            numberK,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Employee By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.DELETE_EMPLOYEE_BY_ID]: {
                config: {
                    scopes: ['delete:employee'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            employeeID
                        } = req.params;

                        const infoAfterDelete = await EMPLOYEE_MODEL.deleteById(employeeID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Employee By List Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.DELETE_EMPLOYEE_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            employeeID
                        } = req.body;

                        const infoAfterDelete = await EMPLOYEE_MODEL.deleteByListId(employeeID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Employee By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.GET_INFO_EMPLOYEE_BY_ID]: {
                config: {
                    scopes: ['read:info_employee'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            employeeID
                        } = req.params;

                        const infoEmployeeById = await EMPLOYEE_MODEL.getInfoById(employeeID);
                        res.json(infoEmployeeById);
                    }]
                },
            },

            /**
             * Function: Get List Employee (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.GET_LIST_EMPLOYEE]: {
                config: {
                    scopes: ['read:list_employee'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách nhân viên',
                    code: CF_ROUTINGS_EMPLOYEE.GET_LIST_EMPLOYEE,
                    inc: path.resolve(__dirname, '../views/employee/list_employees.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            status,
                            typeGetList
                        } = req.query;
                        let { region } = req.user;
                        let listEmployees = [];

                        if (typeGetList === 'FILTER') {
                            listEmployees = await EMPLOYEE_MODEL.getListByFilter({
                                keyword,
                                status,
                                region
                            });
                        } else {
                            listEmployees = await EMPLOYEE_MODEL.getList({ region });
                        }

                        ChildRouter.renderToView(req, res, {
                            listEmployees: listEmployees.data || [],
                            STATUS_EMPLOYEE_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Employee By Field (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.GET_LIST_EMPLOYEE_BY_FIELD]: {
                config: {
                    scopes: ['read:list_employee'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách nhân viên',
                    code: CF_ROUTINGS_EMPLOYEE.GET_LIST_EMPLOYEE_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/employee/list_employees.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            status,
                        } = req.query;

                        let listEmployees = await EMPLOYEE_MODEL.getListByFilter({
                            keyword,
                            status,
                            [field]: value,
                            region
                        });

                        ChildRouter.renderToView(req, res, {
                            listEmployees: listEmployees.data || [],
                            STATUS_EMPLOYEE_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Employee Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.GET_LIST_EMPLOYEE_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listEmployeeServerSide = await EMPLOYEE_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(listEmployeeServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Employee Excel Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.GET_LIST_EMPLOYEE_EXCEL]: {
                config: {
                    scopes: ['read:list_employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = EMPLOYEE_MODEL.getConditionArrayFilterExcel(listItemExport)
                       
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Employee Excel Export (API)
             * Date: 22/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.DOWNLOAD_LIST_EMPLOYEE_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'employee'
                        });
                        
                        let conditionObj = EMPLOYEE_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listEmployee = await EMPLOYEE_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listEmployee)
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.API_GET_INFO_EMPLOYEE]: {
                config: {
                    scopes: ['read:info_employee'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            employeeID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoEmployee = await EMPLOYEE_MODEL.getInfoEmployee({
                            employeeID,
                            select,
                            explain
                        });
                        res.json(infoEmployee);
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_EMPLOYEE.API_GET_LIST_EMPLOYEES]: {
                config: {
                    scopes: ['read:list_employee'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { region: regionAdmin } = req.user;
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listEmployees = await EMPLOYEE_MODEL.getListEmployees({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                            regionAdmin
                        });
                        console.log({ listEmployees })
                        res.json(listEmployees);
                    }]
                },
            },

            /**
             * Function: Download template excel employee (API)
             * Date: 22/11/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_EMPLOYEE.API_DOWNLOAD_EMPLOYEE]: {
                config: {
                    scopes: ['read:list_employee'],
                    type: 'json',
                },
                methods: {
                    post: [ async function(req, res) {
                        const { region } = req.user; 
                        const { filter, condition } = req.body;

                        const infoDownload = await EMPLOYEE_MODEL.downloadEmployee({
                            filter, condition, region
                        });
                        res.json(infoDownload);
                    }]
                },
            },

            /**
             * Function: Import excel nhân viên (API)
             * Date: 22/11/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_EMPLOYEE.API_IMPORT_EMPLOYEE]: {
                config: {
                    scopes: ['create:employee'],
                    type: 'json',
                },
                methods: {
                    post: [ uploadSingle, async function(req, res) {
                        const { region } = req.user;
                        const response = await EMPLOYEE_MODEL.importEmployee({
                            fileInfo: req.file,
                            region
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Đăng nhập employee (API)
             * Date: 28/10/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_EMPLOYEE.API_LOGIN_EMPLOYEE]: {
                config: {
					scopes: ['public'],
					type: 'json',
				},
				methods: {
                    post: [ async (req, res) => {
                        const { account, password, deviceID, deviceName, registrationID } = req.body;

                        const infoSignIn = await EMPLOYEE_MODEL.signInEmployee({ account, password, deviceID, deviceName, registrationID });
                        res.json(infoSignIn);
                    }],
				},
            },

            /**
             * Function: Đăng kí employee (API)
             * Date: 18/11/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_EMPLOYEE.API_REGISTER_EMPLOYEE]: {
                config: {
					scopes: ['public'],
					type: 'json',
				},
				methods: {
                    post: [ async (req, res) => {

                        return res.json({
                            error: true, message: "Đăng Ký Thất Bại, vui lòng liên hệ Quản Trị Viên để hướng dẫn xử lý"
                        });
                        const { fullname, username, email, password } = req.body;

                        const infoRegister = await EMPLOYEE_MODEL.signUpEmployee({
                            fullname, username, email, password
                        });
                        res.json(infoRegister);
                    }],
				},
            },

            /**
             * Function: Cập nhật field isRegisterFace trong employee (API)
             * Date: 29/12/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_EMPLOYEE.API_UPDATE_REGISTER_FACE]: {
                config: {
					scopes: ['update:employee'],
					type: 'json',
				},
				methods: {
                    post: [ async (req, res) => {
                        const { _id: employeeID } = req.user;

                        const response = await EMPLOYEE_MODEL.updateRegisterFace({
                            employeeID
                        });
                        res.json(response);
                    }],
				},
            },
            
            /**
             * Function: Refferal code
             * Date: 02/12/2021
             * Dev: Automatic
             */
            "/api/employee/list-referral-code-of-employee": {
                config: {
                    scopes: ['read:list_employee'],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
						const { _id: employeeID }  = req.user;
                        const infoData = await REFERRAL_CODE_MODEL.getReferralCodeOfEmployee({ 
							employeeID
						});
                        res.json(infoData);
                    }]
                },
            },

            [CF_ROUTINGS_EMPLOYEE.API_CLEAR_IMAGE_FACE_REGISTER]: {
                config: {
					scopes: ['public'],
					type: 'json',
				},
				methods: {
                    get: [ async (req, res) => {
                        const { employeeID } = req.query;

                        const response = await EMPLOYEE_MODEL.clearImagesFaceRegister({
                            employeeID
                        });
                        // res.json(response);
                        return ChildRouter.redirect(res, `/employee/update-employee-by-id?employeeID=${employeeID}`);
                    }],
				},
            },
            

        }
    }
};
