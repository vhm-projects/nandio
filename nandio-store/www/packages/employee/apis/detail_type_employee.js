/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const beautifyer = require('js-beautify').js_beautify;
const fs = require('fs');
const moment = require('moment');
const logger = require('../../../config/logger/winston.config');
const chalk = require('chalk');
const log = console.log;
/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_DETAIL_TYPE_EMPLOYEE_TYPE,
} = require('../constants/detail_type_employee');
const {
    CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE
} = require('../constants/detail_type_employee/detail_type_employee.uri');

/**
 * MODELS
 */
const DETAIL_TYPE_EMPLOYEE_MODEL = require('../models/detail_type_employee').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const TYPE_EMPLOYEE_COLL = require('../databases/type_employee-coll');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ DETAIL_TYPE_EMPLOYEE  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Detail_type_employee (API, VIEW)
             * Date: 06/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.ADD_DETAIL_TYPE_EMPLOYEE]: {
                config: {
                    scopes: ['create:detail_type_employee'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Detail_type_employee',
                    code: CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.ADD_DETAIL_TYPE_EMPLOYEE,
                    inc: path.resolve(__dirname, '../views/detail_type_employee/add_detail_type_employee.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listType_employees = await TYPE_EMPLOYEE_COLL.find({
                                state: 1,
                                status: 1
                            }).lean()
                            .sort({
                                order: 1,
                                modifyAt: -1
                            })
                        ChildRouter.renderToView(req, res, {
                            listType_employees,
                            CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            name,
                            parent,
                            description,
                        } = req.body;


                        let infoAfterInsert = await DETAIL_TYPE_EMPLOYEE_MODEL.insert({
                            name,
                            parent,
                            description,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Detail_type_employee By Id (API, VIEW)
             * Date: 06/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.UPDATE_DETAIL_TYPE_EMPLOYEE_BY_ID]: {
                config: {
                    scopes: ['update:detail_type_employee'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Detail_type_employee',
                    code: CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.UPDATE_DETAIL_TYPE_EMPLOYEE_BY_ID,
                    inc: path.resolve(__dirname, '../views/detail_type_employee/update_detail_type_employee.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            detail_type_employeeID
                        } = req.query;

                        let infoDetail_type_employee = await DETAIL_TYPE_EMPLOYEE_MODEL.getInfoById(detail_type_employeeID);
                        if (infoDetail_type_employee.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let listType_employees = await TYPE_EMPLOYEE_COLL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean()


                        ChildRouter.renderToView(req, res, {
                            infoDetail_type_employee: infoDetail_type_employee.data || {},

                            listType_employees,
                            CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            detail_type_employeeID,
                            name,
                            parent,
                            order,
                            status,
                            description,
                        } = req.body;


                        const infoAfterUpdate = await DETAIL_TYPE_EMPLOYEE_MODEL.update({
                            detail_type_employeeID,
                            name,
                            parent,
                            order,
                            status,
                            description,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Detail_type_employee By Id (API)
             * Date: 06/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.UPDATE_DETAIL_TYPE_EMPLOYEE_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:detail_type_employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            detail_type_employeeID,
                            name,
                            parent,
                            order,
                            status,
                            description,
                        } = req.body;


                        const infoAfterUpdate = await DETAIL_TYPE_EMPLOYEE_MODEL.updateNotRequire({
                            detail_type_employeeID,
                            name,
                            parent,
                            order,
                            status,
                            description,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Detail_type_employee By Id (API)
             * Date: 06/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.DELETE_DETAIL_TYPE_EMPLOYEE_BY_ID]: {
                config: {
                    scopes: ['delete:detail_type_employee'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            detail_type_employeeID
                        } = req.params;

                        const infoAfterDelete = await DETAIL_TYPE_EMPLOYEE_MODEL.deleteById(detail_type_employeeID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Detail_type_employee By List Id (API)
             * Date: 06/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.DELETE_DETAIL_TYPE_EMPLOYEE_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:detail_type_employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            detail_type_employeeID
                        } = req.body;

                        const infoAfterDelete = await DETAIL_TYPE_EMPLOYEE_MODEL.deleteByListId(detail_type_employeeID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Detail_type_employee By Id (API)
             * Date: 06/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.GET_INFO_DETAIL_TYPE_EMPLOYEE_BY_ID]: {
                config: {
                    scopes: ['read:info_detail_type_employee'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            detail_type_employeeID
                        } = req.params;

                        const infoDetail_type_employeeById = await DETAIL_TYPE_EMPLOYEE_MODEL.getInfoById(detail_type_employeeID);
                        res.json(infoDetail_type_employeeById);
                    }]
                },
            },

            /**
             * Function: Get List Detail_type_employee (API, VIEW)
             * Date: 06/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.GET_LIST_DETAIL_TYPE_EMPLOYEE]: {
                config: {
                    scopes: ['read:list_detail_type_employee'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách chi tiết loại nhân viên',
                    code: CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.GET_LIST_DETAIL_TYPE_EMPLOYEE,
                    inc: path.resolve(__dirname, '../views/detail_type_employee/list_detail_type_employees.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            status,
                            typeGetList
                        } = req.query;

                        let listDetail_type_employees = [];
                        if (typeGetList === 'FILTER') {
                            listDetail_type_employees = await DETAIL_TYPE_EMPLOYEE_MODEL.getListByFilter({
                                keyword,
                                status,
                            });
                        } else {
                            listDetail_type_employees = await DETAIL_TYPE_EMPLOYEE_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listDetail_type_employees: listDetail_type_employees.data || [],
                            STATUS_DETAIL_TYPE_EMPLOYEE_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Detail_type_employee By Field (API, VIEW)
             * Date: 06/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.GET_LIST_DETAIL_TYPE_EMPLOYEE_BY_FIELD]: {
                config: {
                    scopes: ['read:list_detail_type_employee'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Detail_type_employee by field isStatus',
                    code: CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.GET_LIST_DETAIL_TYPE_EMPLOYEE_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/detail_type_employee/list_detail_type_employees.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            status,
                            type
                        } = req.query;

                        let listDetail_type_employees = await DETAIL_TYPE_EMPLOYEE_MODEL.getListByFilter({
                            keyword,
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listDetail_type_employees: listDetail_type_employees.data || [],
                            STATUS_DETAIL_TYPE_EMPLOYEE_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Detail_type_employee Server Side (API)
             * Date: 06/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.GET_LIST_DETAIL_TYPE_EMPLOYEE_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_detail_type_employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listDetail_type_employeeServerSide = await DETAIL_TYPE_EMPLOYEE_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listDetail_type_employeeServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Detail_type_employee Excel Server Side (API)
             * Date: 06/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DETAIL_TYPE_EMPLOYEE.GET_LIST_DETAIL_TYPE_EMPLOYEE_EXCEL]: {
                config: {
                    scopes: ['read:list_detail_type_employee'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            filter,
                            condition,
                            nameOfParentColl,
                            objFilterStatic,
                            order,
                            keyword
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let conditionObj = DETAIL_TYPE_EMPLOYEE_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword)
                        let listDetail_type_employee = await DETAIL_TYPE_EMPLOYEE_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice
                        })

                        res.json(listDetail_type_employee)
                    }]
                },
            },

        }
    }
};