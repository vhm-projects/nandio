const EMPLOYEE_COLL = require('./databases/employee-coll');
const EMPLOYEE_MODEL = require('./models/employee').MODEL;
const EMPLOYEE_ROUTES = require('./apis/employee');

const TYPE_EMPLOYEE_COLL  = require('./databases/type_employee-coll');
const TYPE_EMPLOYEE_MODEL  = require('./models/type_employee').MODEL;
const TYPE_EMPLOYEE_ROUTES  = require('./apis/type_employee');
    

const DETAIL_TYPE_EMPLOYEE_COLL  = require('./databases/detail_type_employee-coll');
const DETAIL_TYPE_EMPLOYEE_MODEL  = require('./models/detail_type_employee').MODEL;
const DETAIL_TYPE_EMPLOYEE_ROUTES  = require('./apis/detail_type_employee');
    

const EMPLOYEE_HR_COLL  = require('./databases/employee_hr-coll');
const EMPLOYEE_HR_MODEL  = require('./models/employee_hr').MODEL;
const EMPLOYEE_HR_ROUTES  = require('./apis/employee_hr');
    
// MARK REQUIRE

module.exports = {
    EMPLOYEE_COLL,
    EMPLOYEE_MODEL,
    EMPLOYEE_ROUTES,

    TYPE_EMPLOYEE_COLL,
    TYPE_EMPLOYEE_MODEL,
    TYPE_EMPLOYEE_ROUTES,
    

    DETAIL_TYPE_EMPLOYEE_COLL,
    DETAIL_TYPE_EMPLOYEE_MODEL,
    DETAIL_TYPE_EMPLOYEE_ROUTES,
    

    EMPLOYEE_HR_COLL,
    EMPLOYEE_HR_MODEL,
    EMPLOYEE_HR_ROUTES,
    
    // MARK EXPORT
}