"use strict";
const Schema 	= require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION EMPLOYEE-DEVICE CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('employee_device', {
	employee: {
		type: Schema.Types.ObjectId,
		ref: 'employee'
	},
	deviceName      : { type: String },
	deviceID        : { type: String },
	registrationID  : { type: String },
});