const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('employee_hr', {

    /**
     * Mã nhân viên HR
     */
    code: {
        type: String,
        unique: true,
        required: true,
    },
    /**
     * Họ và tên
     */
    fullname: {
        type: String,
        required: true,
    },
    /**
     * Ngày bắt đầu
     */
    from: {
        type: Date,
        default: Date.now
    },
    /**
     * Ngày  kết thúc
     */
    to: {
        type: Date,
    },
    /**
     * Nhân viên NA
     */
    employee: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },
    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Vùng
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * NPP
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
});