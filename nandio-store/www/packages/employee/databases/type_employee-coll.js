const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('type_employee', {
    /**
     * Tên Loại
     */
    name: {
        type: String,
        required: true,
    },
    /**
     * Thứ tự
     */
    order: {
        type: Number,
        default: 1,
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
    /**
     * Loại 
     * 1: NA,
     * 2: PG
     */
    type: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
});