const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('detail_type_employee', {

    /**
     * Tên loại
     */
    name: {
        type: String,
        required: true,
    },
    /**
     * Loại cha
     */
    parent: {
        type: Schema.Types.ObjectId,
        ref: 'type_employee',
    },
    /**
     * Thứ tự
     */
    order: {
        type: Number,
        default: 1,
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
    /**
     * Mô tả
     */
    description: {
        type: String,
    },
});