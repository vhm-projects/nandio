const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('employee', {
    /**
     * Mã nhân viên
     */
    code: {
        type: String,
        unique: true,
        required: true,
    },
    /**
     * Họ và tên
     */
    fullname: {
        type: String,
        required: true,
    },
    /**
     * Tên đăng nhập
     */
    username: {
        type: String,
        unique: true,
        required: true,
    },
    /**
     * Email
     */
    email: {
        type: String,
        required: true,
    },

    /**
     * Phone
     */
    phone: {
        type: String,
    },
    /**
     * Mật khẩu
     */
    password: {
        type: String,
        required: true,
    },
    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Vùng
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * Nhà phân phối
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    /**
     * Loại Nhân viên
     */
    type_employee: {
        type: Schema.Types.ObjectId,
        ref: 'type_employee',
    },

    /**
     * NA loại A, Loại B, Loại C
     */
    detail_type_employee: {
        type: Schema.Types.ObjectId,
        ref: 'detail_type_employee',
    },
    /**
     * Hệ số K (Chỉ có hiệu lực cho PGS)
     */
    numberK: {
        type: Number,
        default: 0,
    },
    /**
     * Đã đk gương mặt
     */
    isRegisterFace: {
        type: Boolean,
        default: false
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
    /**
	 * RBAC
	 */
	roles: [{
		type:  Schema.Types.ObjectId,
		ref : 'role_base'
	}],
     /**
     * Phone
     */
    deviceID: {
        type: String,
    },
});