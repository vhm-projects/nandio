"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      		= require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGES
 */

/**
 * BASES
 */
const BaseModel 							= require('../../../models/intalize/base_model');
const Logger                          		= require('../../../config/logger/winston.config')

/**
 * COLLECTIONS
 */
const EMPLOYEE_DEVICE_COLL 					= require('../databases/employee_device-coll');
const EMPLOYEE_COLL 						= require('../databases/employee-coll');

class Model extends BaseModel {
    constructor() {
        super(EMPLOYEE_DEVICE_COLL);
    }

	updateEmployeeDevice({ deviceID, employeeID, deviceName, registrationID }) {
        return new Promise(async resolve => {
            try {
				if(!ObjectID.isValid(employeeID))
					return resolve({ error: true, message: 'params_invalid' });
              
                let infoAfterUpdate = await EMPLOYEE_DEVICE_COLL.findOneAndUpdate({deviceID}, {
					employee: employeeID, deviceName, registrationID
				}, { new: true, upsert: true })
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'update_user_device_failed' });

                let infoAfterUpdateDeviceIDIntoCustomer = await EMPLOYEE_COLL.findByIdAndUpdate(employeeID, { deviceID }, { new: true });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getList({ page = 1, limit = 30 }){
        return new Promise(async resolve => {
            try {
				limit = +limit;
				page  = +page;

                let listUserDevice = await EMPLOYEE_DEVICE_COLL
					.find({})
					.populate('employee')
					.limit(limit)
					.skip((page - 1) * limit)
					.sort({ modifyAt: -1 })
					.lean();

                if(!listUserDevice)
                    return resolve({ error: true, message: "cannot_get_list_user_device" });

				let totalUserDevice = await EMPLOYEE_DEVICE_COLL.countDocuments({});

                return resolve({ 
					error: false, 
					data: {
						listUserDevice,
						currentPage: page,
						perPage: limit,
						total: totalUserDevice
					}
				});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfo({ userDeviceID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userDeviceID))
                    return resolve({ error: true, message: "param_invalid" });

				let infoUserDevice = await EMPLOYEE_DEVICE_COLL
					.findById(userDeviceID)
					.lean();

                if(!infoUserDevice) 
                    return resolve({ error: true, message: "cannot_get_info_user_device" });

                return resolve({ error: false, data: infoUserDevice });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfoByDeviceID({ deviceID }) {
        return new Promise(async resolve => {
            try {
				let infoUserDevice = await EMPLOYEE_DEVICE_COLL
					.findOne({ deviceID })
					.lean();

                if(!infoUserDevice) 
                    return resolve({ error: true, message: "cannot_get_info_user_device" });

                return resolve({ error: false, data: infoUserDevice });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ userDeviceID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userDeviceID))
                    return resolve({ error: true, message: "param_invalid" });

				let infoAfterDelete = await EMPLOYEE_DEVICE_COLL.findByIdAndRemove(userDeviceID);

                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "cannot_delete_user_device" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
