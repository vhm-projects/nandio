/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const ObjectID = require('mongoose').Types.ObjectId;
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const path = require('path');
const fs = require('fs');
const XlsxPopulate = require('xlsx-populate');

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    checkNumberIsValidWithRange
} = require('../../../utils/utils');
const timeUtils = require('../../../utils/time_utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const EMPLOYEE_HR_COLL = require('../databases/employee_hr-coll');
const EMPLOYEE_COLL = require('../databases/employee-coll');

const {
    REGION_COLL
} = require('../../region_area');

const {
    AREA_COLL
} = require('../../region_area');

const {
    DISTRIBUTOR_COLL
} = require('../../distributor');


class Model extends BaseModel {
    constructor() {
        super(EMPLOYEE_HR_COLL);
    }

    /**
     * Tạo mới employee_hr
    * @param {string} code
    * @param {string} fullname
    * @param {date} from
    * @param {date} to
    * @param {object} employee
    * @param {objectId} userCreate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    insert({
        code,
        fullname,
        from,
        to,
        employee,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (code.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mã nhân viên hr không được lớn hơn 125 ký tự'
                    });
                }

                if (!code) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mã nhân viên hr'
                    });
                }

                if (fullname.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài họ và tên không được lớn hơn 125 ký tự'
                    });
                }

                if (!fullname) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập họ và tên'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên na không hợp lệ'
                    });
                }


                const checkCodeExits = await EMPLOYEE_HR_COLL.findOne({
                    code
                });
                if (checkCodeExits) {
                    return resolve({
                        error: true,
                        message: 'Mã NhâN ViêN HR đã tồn tại'
                    });
                }


                let dataInsert = {
                    code,
                    fullname,
                    employee,
                    userCreate
                };
                let infoEmployee = await EMPLOYEE_COLL.findById(employee);

                if(infoEmployee){
                    let { region, area, distributor } = infoEmployee;
                    dataInsert.region = region;
                    dataInsert.area   = area;
                    dataInsert.distributor = distributor;
                }

                from && (dataInsert.from = new Date(from));
                to && (dataInsert.to = new Date(to));
                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo nhân viên HR thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Cập nhật employee_hr 
     * @param {objectId} employee_hrID
    * @param {string} code
    * @param {string} fullname
    * @param {date} from
    * @param {date} to
    * @param {object} employee
    * @param {number} status
    * @param {objectId} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    update({
        employee_hrID,
        code,
        fullname,
        from,
        to,
        employee,
        status,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employee_hrID])) {
                    return resolve({
                        error: true,
                        message: 'employee_hrID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (code.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mã nhân viên hr không được lớn hơn 125 ký tự'
                    });
                }

                if (!code) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mã nhân viên hr cho employee_hr'
                    });
                }

                if (fullname.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài họ và tên không được lớn hơn 125 ký tự'
                    });
                }

                if (!fullname) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập họ và tên cho employee_hr'
                    });
                }


                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trạng thái không hợp lệ'
                    });
                }




                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên na không hợp lệ'
                    });
                }

                const checkExists = await EMPLOYEE_HR_COLL.findById(employee_hrID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'nhân viên HR không tồn tại'
                    });
                }

                const checkCodeExits = await EMPLOYEE_HR_COLL.findOne({
                    code
                });
                if (checkCodeExits && checkExists.code !== code) {
                    return resolve({
                        error: true,
                        message: 'mã nhân viên hr đã tồn tại'
                    });
                }


                let dataUpdate = {
                    userUpdate
                };
                code && (dataUpdate.code = code);
                fullname && (dataUpdate.fullname = fullname);
                from && (dataUpdate.from = new Date(from));
                to && (dataUpdate.to = new Date(to));
                employee && (dataUpdate.employee = employee);
                dataUpdate.status = status;

                let infoEmployee = await EMPLOYEE_COLL.findById(employee);

                if(infoEmployee){
                    let { region, area, distributor } = infoEmployee;
                    dataUpdate.region = region;
                    dataUpdate.area   = area;
                    dataUpdate.distributor = distributor;
                }

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: employee_hrID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Cập nhật employee_hr (không bắt buộc)
     * @param {objectId} employee_hrID
    * @param {string} code
    * @param {string} fullname
    * @param {date} from
    * @param {date} to
    * @param {object} employee
    * @param {number} status
    * @param {objectId} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    updateNotRequire({
        employee_hrID,
        code,
        fullname,
        from,
        to,
        employee,
        status,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employee_hrID])) {
                    return resolve({
                        error: true,
                        message: 'employee_hrID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên na không hợp lệ'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trạng thái không hợp lệ'
                    });
                }

                const checkExists = await EMPLOYEE_HR_COLL.findById(employee_hrID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'nhân viên HR không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                code && (dataUpdate.code = code);
                fullname && (dataUpdate.fullname = fullname);
                from && (dataUpdate.from = new Date(from));
                to && (dataUpdate.to = new Date(to));
                employee && (dataUpdate.employee = employee);
                status && (dataUpdate.status = status);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: employee_hrID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa employee_hr 
     * @param {objectId} employee_hrID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(employee_hrID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employee_hrID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị employee_hrID không hợp lệ'
                    });
                }

                const infoAfterDelete = await this.updateById(employee_hrID, {
                    state: 2
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa employee_hr 
     * @param {array} employee_hrID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(employee_hrID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(employee_hrID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị employee_hrID không hợp lệ'
                    });
                }

                const infoAfterDelete = await EMPLOYEE_HR_COLL.updateMany({
                    _id: {
                        $in: employee_hrID
                    }
                }, {
                    state: 2,
                    modifyAt: new Date()
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin employee_hr 
     * @param {objectId} employee_hrID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(employee_hrID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employee_hrID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị employee_hrID không hợp lệ'
                    });
                }

                const infoEmployee_hr = await EMPLOYEE_HR_COLL.findById(employee_hrID)
                    .populate('employee region area distributor')

                if (!infoEmployee_hr) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin nhân viên HR'
                    });
                }

                return resolve({
                    error: false,
                    data: infoEmployee_hr
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách employee_hr 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList({ region }) {
        return new Promise(async resolve => {
            try {
                let condition = { state: 1 };
                region && (condition.region = region);

                const listEmployee_hr = await EMPLOYEE_HR_COLL
                    .find(condition)
                    .populate('employee region area distributor')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listEmployee_hr) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách nhân viên HR'
                    });
                }

                return resolve({
                    error: false,
                    data: listEmployee_hr
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách employee_hr theo bộ lọc
    * @param {string} keyword
    * @param {date} fromDateRange
    * @param {date} toDateRange
    * @enum {number} status
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: array, message?: string }}
    */
    getListByFilter({
        keyword,
        fromDateRange,
        toDateRange,
        status,
        region
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        code: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullname: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                if (fromDateRange) {
                    let [fromDate, toDate] = fromDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.from = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                if (toDateRange) {
                    let [fromDate, toDate] = toDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.to = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                status && (conditionObj.status = status);
                region && (conditionObj.region = region);

                const listEmployee_hrByFilter = await EMPLOYEE_HR_COLL
                    .find(conditionObj).populate('employee region area distributor')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listEmployee_hrByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách nhân viên HR"
                    });
                }

                return resolve({
                    error: false,
                    data: listEmployee_hrByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách employee_hr theo bộ lọc (server side)
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir,
        region
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        code: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullname: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        "employee.code": {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trạng thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                region && (conditionObj.region = ObjectID(region));

                const skip = (page - 1) * limit;
                const totalEmployee_hr = await EMPLOYEE_HR_COLL.countDocuments(conditionObj);

                if(region) {
                    delete conditionObj.region;
                    conditionObj['region._id'] = ObjectID(region);
                }

                const listEmployee_hrByFilter = await EMPLOYEE_HR_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'detail_type_employees',
                            localField: 'employee.detail_type_employee',
                            foreignField: '_id',
                            as: 'detail_type_employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$detail_type_employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listEmployee_hrByFilter) {
                    return resolve({
                        recordsTotal: totalEmployee_hr,
                        recordsFiltered: totalEmployee_hr,
                        data: []
                    });
                }
                const listEmployee_hrDataTable = listEmployee_hrByFilter.map((employee_hr, index) => {
                    let status = '';
                    if (employee_hr.status == 1) {
                        status = 'checked';
                    }
                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${employee_hr._id}" type="checkbox" class="check-record check-record-${employee_hr._id}" _index ="${index + 1}"><label for="${employee_hr._id}"></label></div></td>`,
                        code: `<a href="/employee_hr/update-employee_hr-by-id?employee_hrID=${employee_hr._id}">${employee_hr.code && employee_hr.code.length > 50 ? employee_hr.code.substr(0,50) + "..." : employee_hr.code} </a>`,
                        fullname: `${employee_hr.fullname && employee_hr.fullname.length > 50 ? employee_hr.fullname.substr(0,50) + "..." : employee_hr.fullname}`,
                        from: moment(employee_hr.from).format("L"),
                        to: moment(employee_hr.to).format("L"),
                        employee: `<a class="btn btn-outline-primary" style="padding: 2px 10px" href="/employee/update-employee-by-id?employeeID=${employee_hr.employee && employee_hr.employee._id}"> ${employee_hr.employee ? employee_hr.employee.code : ""} </a>`,
                        detail_type_employee: employee_hr.detail_type_employee?.name || "",
                        region: employee_hr.region ? employee_hr.region.name : '',
                        area: employee_hr.area ? employee_hr.area.name : '',
                        distributor: `<a class="btn btn-outline-primary" style="padding: 2px 10px" href="/distributor/update-distributor-by-id?distributorID=${employee_hr.distributor && employee_hr.distributor._id}"> ${employee_hr.distributor ? employee_hr.distributor?.name : ""} </a>`,
                        status: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-status" _employee_hrID="${employee_hr._id}" type="checkbox" id="${employee_hr._id}" ${status} style="width: 40px;height: 20px;"></div></td>`,
                        createAt: moment(employee_hr.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalEmployee_hr,
                    recordsFiltered: totalEmployee_hr,
                    data: listEmployee_hrDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc employee_hr
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc employee_hr
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listEmployee_hrByFilter = await EMPLOYEE_HR_COLL.aggregate(arrayFilter)

                if (!listEmployee_hrByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listEmployee_hrByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listEmployee_hrByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc employee_hr
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            dataEnum: item.dataEnum,
                            nameCollChoice: item.nameCollChoice,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                dataEnum: item.dataEnum,
                                nameCollChoice: item.nameCollChoice,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (markPopulate == true) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {

            if (filter.length > 1) {
                condition === 'OR' && (conditionObj.$or = []);
                condition === 'AND' && (conditionObj.$and = []);

                filter.map(filterObj => {
                    const getConditionByFilter = this.getConditionObj(filterObj);

                    if (condition === 'OR') {
                        conditionObj.$or.push(getConditionByFilter);
                    } else {
                        conditionObj.$and.push(getConditionByFilter);
                    }
                })
            } else {
                conditionObj = {
                    ...conditionObj,
                    ...this.getConditionObj(filter[0])
                };
            }
        }


        if (!isEmptyObject(objFilterStatic)) {

            if (objFilterStatic.status) {
                if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                    return resolve({
                        error: true,
                        message: "trạng thái không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    status: Number(objFilterStatic.status)
                }
            }

        }


        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let lookup = [{
                        $lookup: {
                            from: pluralize.plural(item.coll),
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: pluralize.plural(populate.coll),
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Chi tiết
     * @param {objectId} employee_hrID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoEmployee_hr({
        employee_hrID,
        select,
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];

                if (!checkObjectIDs([employee_hrID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị employee_hrID không hợp lệ',
                        status: 400
                    });
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['employee', 'region', 'area', 'distributor'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoEmployee_hr = await EMPLOYEE_HR_COLL
                    .findById(employee_hrID)
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoEmployee_hr = await EMPLOYEE_HR_COLL.populate(infoEmployee_hr, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoEmployee_hr = await EMPLOYEE_HR_COLL.populate(infoEmployee_hr, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoEmployee_hr) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin employee_hr',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoEmployee_hr,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Danh sách
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListEmployee_hrs({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page,
        regionAdmin
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { state: 1 };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['fromDateRange', 'toDateRange', 'employee', 'region', 'area', 'distributor', 'status'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    fromDateRange,
                    toDateRange,
                    employee,
                    region,
                    area,
                    distributor,
                    status,
                } = filter;

                if (fromDateRange) {
                    let [fromDate, toDate] = fromDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.from = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                if (toDateRange) {
                    let [fromDate, toDate] = toDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.to = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                employee && (conditionObj.employee = employee);

                region && (conditionObj.region = region);

                area && (conditionObj.area = area);

                distributor && (conditionObj.distributor = distributor);

                status && (conditionObj.status = status);

                regionAdmin && (conditionObj.region = regionAdmin);

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['employee', 'region', 'area', 'distributor'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listEmployee_hrs = await EMPLOYEE_HR_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listEmployee_hrs = await EMPLOYEE_HR_COLL.populate(listEmployee_hrs, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listEmployee_hrs = await EMPLOYEE_HR_COLL.populate(listEmployee_hrs, `${ref}.${field}`);
                        }
                    }
                }

                if (!listEmployee_hrs) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách employee_hr',
                        status: 400
                    });
                }

                let totalRecord = await EMPLOYEE_HR_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listEmployee_hrs,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Import excel nhân viên HR
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     * }}
     */
    importEmployeeHR(fileInfo) {
        return new Promise(async resolve => {
            try {
                XlsxPopulate.fromFileAsync(fileInfo.path)
                    .then(async workbook => {
                        let index = 2;
                        let listEmployeeHR = [];

                        for (;true;) {
                            let code_hr     = workbook.sheet(0).cell(`A${index}`).value();
                            let fullname    = workbook.sheet(0).cell(`B${index}`).value();
                            let code_na     = workbook.sheet(0).cell(`C${index}`).value();
                            let code_region = workbook.sheet(0).cell(`D${index}`).value();
                            let code_area   = workbook.sheet(0).cell(`E${index}`).value();
                            let code_npp    = workbook.sheet(0).cell(`F${index}`).value();
                            let fromDate    = workbook.sheet(0).cell(`G${index}`).value();
                            let toDate      = workbook.sheet(0).cell(`H${index}`).value();
                            let status      = workbook.sheet(0).cell(`I${index}`).value();

                            if(!code_hr || !fullname || !code_na || !code_region || !code_area || !code_npp) break;

                            code_hr     = code_hr.trim();
                            code_na     = code_na.trim();
                            code_region = code_region.trim();
                            code_area   = code_area.trim();
                            code_npp    = code_npp.trim();

                            const infoEmployeeHR    = await EMPLOYEE_HR_COLL.findOne({ code: code_hr }).select('_id').lean();
                            const infoEmployeeNA    = await EMPLOYEE_COLL.findOne({ code: code_na }).select('_id').lean();
                            const infoRegion        = await REGION_COLL.findOne({ code: code_region }).select('_id').lean();
                            const infoArea          = await AREA_COLL.findOne({ code: code_area }).select('_id').lean();
                            const infoDistributor   = await DISTRIBUTOR_COLL.findOne({ code: code_npp }).select('_id').lean();

                            if(!infoEmployeeHR && infoEmployeeNA && infoRegion && infoArea && infoDistributor){
                                console.log({
                                    code: code_hr,
                                    fullname,
                                    from: moment(fromDate).isValid() ? new Date(fromDate) : '',
                                    to: moment(toDate).isValid() ? new Date(toDate) : '',
                                    employee: infoEmployeeNA._id,
                                    region: infoRegion._id,
                                    area: infoArea._id,
                                    distributor: infoDistributor._id,
                                    status: +status === 0 ? 2 : 1,
                                    createAt: timeUtils.getCurrentTime(),
                                    modifyAt: timeUtils.getCurrentTime()
                                });
                                listEmployeeHR[listEmployeeHR.length] = {
                                    code: code_hr,
                                    fullname,
                                    from: moment(fromDate).isValid() ? new Date(fromDate) : '',
                                    to: moment(toDate).isValid() ? new Date(toDate) : '',
                                    employee: infoEmployeeNA._id,
                                    region: infoRegion._id,
                                    area: infoArea._id,
                                    distributor: infoDistributor._id,
                                    status: +status === 0 ? 2 : 1,
                                    createAt: timeUtils.getCurrentTime(),
                                    modifyAt: timeUtils.getCurrentTime()
                                }
                            }

                            index++;
                        }

                        await fs.unlinkSync(fileInfo.path);

                        if(listEmployeeHR.length){
                            await EMPLOYEE_HR_COLL.insertMany(listEmployeeHR);
                        } else{
                            return resolve({ error: true, message: 'Import nhân viên HR thất bại' });
                        }

                        return resolve({ error: false, message: 'Import nhân viên HR thành công' });
                    });

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

}

exports.MODEL = new Model;