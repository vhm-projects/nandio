/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const request = require('request');
const fastcsv = require('fast-csv');
const ObjectID = require('mongoose').Types.ObjectId;
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const path = require('path');
const fs = require('fs');
const XlsxPopulate = require('xlsx-populate');
const jwt = require('jsonwebtoken');
const { hash, hashSync, compare } = require('bcryptjs');

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    checkEmail,
    checkNumberIsValidWithRange
} = require('../../../utils/utils');
const { randomStringFixLength } = require('../../../utils/string_utils');
const cfJWS = require('../../../config/cf_jws');
const timeUtils = require('../../../utils/time_utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');


class Model extends BaseModel {

    constructor() {
        super(require('../databases/employee-coll'));
    }

    /**
     * Tạo mới employee
    * @param {string} code
    * @param {string} fullname
    * @param {string} username
    * @param {string} email
    * @param {string} password
    * @param {object} region
    * @param {object} area
    * @param {object} distributor
    * @param {object} type_employee
    * @param {number} numberK

    * @param {objectId} userCreate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    insert({
        code,
        fullname,
        username,
        email,
        password,
        confirmPassword,
        region,
        area,
        distributor,
        type_employee,
        detail_type_employee,
        numberK = 0,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (code.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mã nhân viên không được lớn hơn 125 ký tự'
                    });
                }

                if (!code) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mã nhân viên'
                    });
                }

                if (fullname.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài họ và tên không được lớn hơn 125 ký tự'
                    });
                }

                if (!fullname) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập họ và tên'
                    });
                }

                if (username.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tên đăng nhập không được lớn hơn 125 ký tự'
                    });
                }

                if (!username) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tên đăng nhập'
                    });
                }

                if (email.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài email không được lớn hơn 125 ký tự'
                    });
                }

                if (!email) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập email'
                    });
                }

                if (password.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mật khẩu không được lớn hơn 125 ký tự'
                    });
                }

                if (!password) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mật khẩu'
                    });
                }

                if(password !== confirmPassword){
                    return resolve({
                        error: true,
                        message: 'Mật khẩu không trùng khớp'
                    });
                }

                if (!checkEmail(email)) {
                    return resolve({
                        error: true,
                        message: 'Email không hợp lệ'
                    });
                }



                if (!checkObjectIDs([region])) {
                    return resolve({
                        error: true,
                        message: 'Miền không hợp lệ'
                    });
                }

                if (!checkObjectIDs([area])) {
                    return resolve({
                        error: true,
                        message: 'Vùng không hợp lệ'
                    });
                }

                if (!checkObjectIDs([distributor])) {
                    return resolve({
                        error: true,
                        message: 'Nhà phân phối không hợp lệ'
                    });
                }

                if (!checkObjectIDs([type_employee])) {
                    return resolve({
                        error: true,
                        message: 'Loại nhân viên không hợp lệ'
                    });
                }

                let hashPassword = await hash(password, 8);
				if (!hashPassword){
                    return resolve({ error: true, message: 'Không thể hash password' });
                }

                const checkCodeExits = await EMPLOYEE_COLL.findOne({
                    code, state: 1
                });
                if (checkCodeExits) {
                    return resolve({
                        error: true,
                        message: 'Mã nhân viên đã tồn tại'
                    });
                }

                // username = username.toLowerCase();
                const checkUsernameExits = await EMPLOYEE_COLL.findOne({
                    username, state: 1
                });
                if (checkUsernameExits) {
                    return resolve({
                        error: true,
                        message: 'Tên đăng nhập đã tồn tại'
                    });
                }

                const checkEmailExits = await EMPLOYEE_COLL.findOne({
                    email, state: 1
                });
                if (checkEmailExits) {
                    return resolve({
                        error: true,
                        message: 'Email đã tồn tại'
                    });
                }

                let dataInsert = {
                    code,
                    fullname,
                    username,
                    email,
                    password: hashPassword,
                    region,
                    area,
                    distributor,
                    type_employee,
                    numberK,
                    roles: [ObjectID('61799ef6ccd7cf418c13caa3')],
                    userCreate
                };

                if(detail_type_employee){
                    dataInsert.detail_type_employee = detail_type_employee;
                }
                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);
                delete infoAfterInsert.password;

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo nhân viên thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Cập nhật employee
     * @param {objectId} employeeID
    * @param {string} code
    * @param {string} fullname
    * @param {string} username
    * @param {string} email
    * @param {string} password
    * @param {object} region
    * @param {object} area
    * @param {object} distributor
    * @param {object} type_employee
    * @param {number} numberK
    * @param {number} status

    * @param {objectId} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    update({
        employeeID,
        code,
        fullname,
        username,
        email,
        phone,
        password,
        confirmPassword,
        region,
        area,
        distributor,
        type_employee,
        detail_type_employee,
        numberK,
        status,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employeeID])) {
                    return resolve({
                        error: true,
                        message: 'employeeID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (code.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mã nhân viên không được lớn hơn 125 ký tự'
                    });
                }

                if (!code) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mã nhân viên cho employee'
                    });
                }

                if (fullname.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài họ và tên không được lớn hơn 125 ký tự'
                    });
                }

                if (!fullname) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập họ và tên cho employee'
                    });
                }

                if (username.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tên đăng nhập không được lớn hơn 125 ký tự'
                    });
                }

                if (!username) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tên đăng nhập cho employee'
                    });
                }

                if (email.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài email không được lớn hơn 125 ký tự'
                    });
                }

                if (!email) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập email cho employee'
                    });
                }

                if (password) {
                    if(password.length > 125){
                        return resolve({
                            error: true,
                            message: 'Độ dài mật khẩu không được lớn hơn 125 ký tự'
                        });
                    }

                    if(password !== confirmPassword){
                        return resolve({
                            error: true,
                            message: 'Mật khẩu không trùng khớp'
                        });
                    }
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'Trạng thái không hợp lệ'
                    });
                }

                if (!checkEmail(email)) {
                    return resolve({
                        error: true,
                        message: 'Email không hợp lệ'
                    });
                }

                if (!checkObjectIDs([region])) {
                    return resolve({
                        error: true,
                        message: 'Miền không hợp lệ'
                    });
                }

                if (!checkObjectIDs([area])) {
                    return resolve({
                        error: true,
                        message: 'Vùng không hợp lệ'
                    });
                }

                if (!checkObjectIDs([distributor])) {
                    return resolve({
                        error: true,
                        message: 'Nhà phân phối không hợp lệ'
                    });
                }

                if (!checkObjectIDs([type_employee])) {
                    return resolve({
                        error: true,
                        message: 'Loại nhân viên không hợp lệ'
                    });
                }

                const checkExists = await EMPLOYEE_COLL.findById(employeeID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'Nhân viên không tồn tại'
                    });
                }

                const checkCodeExits = await EMPLOYEE_COLL.findOne({
                    code, state: 1
                });
                if (checkCodeExits && checkExists.code !== code) {
                    return resolve({
                        error: true,
                        message: 'Mã nhân viên đã tồn tại'
                    });
                }

                // username = username.toLowerCase();
                const checkUsernameExits = await EMPLOYEE_COLL.findOne({
                    username, state: 1
                });
                if (checkUsernameExits && checkExists.username !== username) {
                    return resolve({
                        error: true,
                        message: 'Tên đăng nhập đã tồn tại'
                    });
                }

                const checkEmailExits = await EMPLOYEE_COLL.findOne({
                    email, state: 1
                });
                if (checkEmailExits && checkExists.email !== email) {
                    return resolve({
                        error: true,
                        message: 'Email đã tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                code && (dataUpdate.code = code);
                fullname && (dataUpdate.fullname = fullname);
                username && (dataUpdate.username = username);
                email && (dataUpdate.email = email);
                phone && (dataUpdate.phone = phone);
                region && (dataUpdate.region = region);
                area && (dataUpdate.area = area);
                distributor && (dataUpdate.distributor = distributor);
                type_employee && (dataUpdate.type_employee = type_employee);
                detail_type_employee &&  (dataUpdate.detail_type_employee = detail_type_employee);
                password && (dataUpdate.password = hashSync(password, 8));
                dataUpdate.numberK = numberK;
                dataUpdate.status = status;

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: employeeID
                }, dataUpdate);
                delete infoAfterUpdate.password;

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Cập nhật employee (không bắt buộc)
     * @param {objectId} employeeID
    * @param {string} code
    * @param {string} fullname
    * @param {string} username
    * @param {string} email
    * @param {string} password
    * @param {object} region
    * @param {object} area
    * @param {object} distributor
    * @param {object} type_employee
    * @param {number} numberK
    * @param {number} status

    * @param {objectId} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    updateNotRequire({
        employeeID,
        code,
        fullname,
        username,
        email,
        password,
        region,
        area,
        distributor,
        type_employee,
        numberK,
        status,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employeeID])) {
                    return resolve({
                        error: true,
                        message: 'employeeID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (email && !checkEmail(email)) {
                    return resolve({
                        error: true,
                        message: 'Email không hợp lệ'
                    });
                }

                if (region && !checkObjectIDs([region])) {
                    return resolve({
                        error: true,
                        message: 'miền không hợp lệ'
                    });
                }

                if (area && !checkObjectIDs([area])) {
                    return resolve({
                        error: true,
                        message: 'vùng không hợp lệ'
                    });
                }

                if (distributor && !checkObjectIDs([distributor])) {
                    return resolve({
                        error: true,
                        message: 'nhà phân phối không hợp lệ'
                    });
                }

                if (type_employee && !checkObjectIDs([type_employee])) {
                    return resolve({
                        error: true,
                        message: 'loại nhân viên không hợp lệ'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trạng thái không hợp lệ'
                    });
                }

                const checkExists = await EMPLOYEE_COLL.findById(employeeID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                code && (dataUpdate.code = code);
                fullname && (dataUpdate.fullname = fullname);
                username && (dataUpdate.username = username);
                email && (dataUpdate.email = email);
                password && (dataUpdate.password = password);
                region && (dataUpdate.region = region);
                area && (dataUpdate.area = area);
                distributor && (dataUpdate.distributor = distributor);
                type_employee && (dataUpdate.type_employee = type_employee);
                numberK && (dataUpdate.numberK = numberK);
                status && (dataUpdate.status = status);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: employeeID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa employee
     * @param {objectId} employeeID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(employeeID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employeeID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị employeeID không hợp lệ'
                    });
                }

                const infoAfterDelete = await this.updateById(employeeID, {
                    state: 2,
                    username: randomStringFixLength(30),
                    email: randomStringFixLength(30),
                    code: randomStringFixLength(30)
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa employee
     * @param {array} employeeID
     * @extends {BaseModel}
     * @returns {{
     *      error: boolean,
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number },
     * }}
     */
    deleteByListId(employeeID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(employeeID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị employeeID không hợp lệ'
                    });
                }

                let listEmployeeID = employeeID.map(item => {
                    return EMPLOYEE_COLL.findByIdAndUpdate(item, {
                        state: 2,
                        username: randomStringFixLength(30),
                        email: randomStringFixLength(30),
                        code: randomStringFixLength(30),
                    });
                })

                await Promise.all(listEmployeeID);

                return resolve({
                    error: false,
                    message: "Remove success"
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin employee
     * @param {objectId} employeeID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(employeeID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employeeID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị employeeID không hợp lệ'
                    });
                }

                const infoEmployee = await EMPLOYEE_COLL.findById(employeeID)
                    .populate('region area distributor type_employee')

                if (!infoEmployee) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin nhân viên'
                    });
                }

                return resolve({
                    error: false,
                    data: infoEmployee
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách employee
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList({ region }) {
        return new Promise(async resolve => {
            try {
                let condition = { state: 1 };
                region && (condition.region = region);

                const listEmployee = await EMPLOYEE_COLL
                    .find(condition)
                    .populate('region area distributor type_employee')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listEmployee) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách nhân viên'
                    });
                }

                return resolve({
                    error: false,
                    data: listEmployee
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách employee theo bộ lọc
		* @param {string} keyword
		* @param {number} numberK
		* @enum {number} status

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        keyword,
        status,
        region
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        code: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullname: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        username: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        email: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        password: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                status && (conditionObj.status = status);
                region && (conditionObj.region = region);

                const listEmployeeByFilter = await EMPLOYEE_COLL
                    .find(conditionObj).populate('region area distributor type_employee')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listEmployeeByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách nhân viên"
                    });
                }

                return resolve({
                    error: false,
                    data: listEmployeeByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách employee theo bộ lọc (server side)
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir,
        region
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        code: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullname: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        username: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        email: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        password: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trạng thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;

                if(region) {
                    conditionObj['region._id'] = ObjectID(region);
                }

                const [{ metadata, data: listEmployeeByFilter }] = await EMPLOYEE_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'type_employees',
                            localField: 'type_employee',
                            foreignField: '_id',
                            as: 'type_employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$type_employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $facet: {
                            metadata: [ { $count: "total" }, { $addFields: { page } } ],
                            data: [ { $sort: sort }, { $skip: +skip }, { $limit: +limit } ]
                        }
                    }
                ])

                const totalEmployee = metadata.length && metadata[0].total;

                if (!listEmployeeByFilter) {
                    return resolve({
                        recordsTotal: totalEmployee,
                        recordsFiltered: totalEmployee,
                        data: []
                    });
                }

                const listEmployeeDataTable = listEmployeeByFilter.map((employee, index) => {
                    let status = '';
                    if (employee.status == 1) {
                        status = 'checked';
                    }

                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${employee._id}" type="checkbox" class="check-record check-record-${employee._id}" _index ="${index + 1}"><label for="${employee._id}"></label></div></td>`,
                        code: `<a href="/employee/update-employee-by-id?employeeID=${employee._id}">${employee.code && employee.code.length > 50 ? employee.code.substr(0,50) + "..." : employee.code} </a>`,
                        fullname: `${employee.fullname && employee.fullname.length > 50 ? employee.fullname.substr(0,50) + "..." : employee.fullname}`,
                        username: `${employee.username && employee.username.length > 50 ? employee.username.substr(0,50) + "..." : employee.username}`,
                        email: `${employee.email && employee.email.length > 50 ? employee.email.substr(0,50) + "..." : employee.email}`,
                        region: employee.region ? employee.region.name : '',
                        area: employee.area ? employee.area.name : '',
                        distributor: employee.distributor ? employee.distributor.name : '',
                        type_employee: employee.type_employee ? employee.type_employee.name : '',
                        status: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-status" _employeeID="${employee._id}" type="checkbox" id="${employee._id}" ${status} style="width: 40px;height: 20px;"></div></td>`,
                        createAt: moment(employee.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalEmployee,
                    recordsFiltered: totalEmployee,
                    data: listEmployeeDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc employee
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc employee
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                arrayFilter.map(item => {
                    console.log(item);
                })

                const listEmployeeByFilter = await EMPLOYEE_COLL.aggregate(arrayFilter)

                if (!listEmployeeByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listEmployeeByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listEmployeeByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc employee
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
            $or: []
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            dataEnum: item.dataEnum,
                            nameCollChoice: item.nameCollChoice,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                dataEnum: item.dataEnum,
                                nameCollChoice: item.nameCollChoice,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                fullname: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                username: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                email: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                password: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {

            if (filter.length > 1) {
                condition === 'OR' && (conditionObj.$or = []);
                condition === 'AND' && (conditionObj.$and = []);

                filter.map(filterObj => {
                    const getConditionByFilter = this.getConditionObj(filterObj);

                    if (condition === 'OR') {
                        conditionObj.$or.push(getConditionByFilter);
                    } else {
                        conditionObj.$and.push(getConditionByFilter);
                    }
                })
            } else {
                conditionObj = {
                    ...conditionObj,
                    ...this.getConditionObj(filter[0])
                };
            }
        }

        if (!isEmptyObject(objFilterStatic)) {

            if (objFilterStatic.status) {
                if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                    return resolve({
                        error: true,
                        message: "trạng thái không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    status: Number(objFilterStatic.status)
                }
            }

        }

        if (conditionObj.$or && !conditionObj.$or.length) {
            delete conditionObj.$or;
        }

        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let lookup = [{
                        $lookup: {
                            from: pluralize.plural(item.coll),
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: pluralize.plural(populate.coll),
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]

        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Chi tiết
     * @param {objectId} employeeID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoEmployee({
        employeeID,
        select,
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];

                if (!checkObjectIDs([employeeID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị employeeID không hợp lệ',
                        status: 400
                    });
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'type_employee', 'detail_type_employee'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoEmployee = await EMPLOYEE_COLL
                    .findById(employeeID)
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoEmployee = await EMPLOYEE_COLL.populate(infoEmployee, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoEmployee = await EMPLOYEE_COLL.populate(infoEmployee, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoEmployee) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin employee',
                        status: 400
                    });
                }

                infoEmployee.typeEmployee = infoEmployee.type_employee && infoEmployee.type_employee.type;

                return resolve({
                    error: false,
                    data: infoEmployee,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Danh sách
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{
     *   error: boolean,
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListEmployees({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page,
        regionAdmin
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { state: 1 };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['region', 'area', 'distributor', 'type_employee', 'status'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    region,
                    area,
                    distributor,
                    type_employee,
                    status,
                } = filter;

                region && (conditionObj.region = region);

                area && (conditionObj.area = area);

                distributor && (conditionObj.distributor = distributor);

                type_employee && (conditionObj.type_employee = type_employee);

                status && (conditionObj.status = status);

                regionAdmin && (conditionObj.region = regionAdmin);


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'type_employee'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listEmployees = await EMPLOYEE_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listEmployees = await EMPLOYEE_COLL.populate(listEmployees, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listEmployees = await EMPLOYEE_COLL.populate(listEmployees, `${ref}.${field}`);
                        }
                    }
                }

                if (!listEmployees) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách employee',
                        status: 400
                    });
                }

                let totalRecord = await EMPLOYEE_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listEmployees,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Download template excel employee
     * @extends {BaseModel}
     * @returns {{
     *      error: boolean,
     *      message?: string,
     * }}
     */
    downloadEmployee({ filter, condition, region }){
        return new Promise(async resolve => {
            try {
                // Open the workbook.
                XlsxPopulate.fromFileAsync("files/excel/Template_employee.xlsx")
                .then(async workbook => {

                    let conditionObj = {
                        state: 1,
                        $or: []
                    };

                    if (filter && filter.length) {
                        if (filter.length > 1) {

                            filter.map(filterObj => {
                                if (filterObj.type === 'ref') {
                                    const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                    if (condition === 'OR') {
                                        conditionObj.$or.push(conditionFieldRef);
                                    } else {
                                        conditionObj = {
                                            ...conditionObj,
                                            ...conditionFieldRef
                                        };
                                    }
                                } else {
                                    const conditionByFilter = this.getConditionObj(filterObj);

                                    if (condition === 'OR') {
                                        conditionObj.$or.push(conditionByFilter);
                                    } else {
                                        conditionObj = {
                                            ...conditionObj,
                                            ...conditionByFilter
                                        };
                                    }
                                }
                            });

                        } else {
                            let {
                                type,
                                ref,
                                fieldRefName
                            } = filter[0];

                            if (type === 'ref') {
                                conditionObj = {
                                    ...conditionObj,
                                    ...this.getConditionObj(ref, fieldRefName)
                                };
                            } else {
                                conditionObj = {
                                    ...conditionObj,
                                    ...this.getConditionObj(filter[0])
                                };
                            }
                        }
                    }

                    if (conditionObj.$or && !conditionObj.$or.length) {
                        delete conditionObj.$or;
                    }

                    region && (conditionObj['region._id'] = ObjectID(region));

                    const listEmployeeByFilter = await EMPLOYEE_COLL.aggregate([
                        {
                            $lookup: {
                                from: 'regions',
                                localField: 'region',
                                foreignField: '_id',
                                as: 'region'
                            }
                        },
                        {
                            $unwind: {
                                path: '$region',
                                preserveNullAndEmptyArrays: true
                            },
                        },
                        {
                            $lookup: {
                                from: 'areas',
                                localField: 'area',
                                foreignField: '_id',
                                as: 'area'
                            }
                        },
                        {
                            $unwind: {
                                path: '$area',
                                preserveNullAndEmptyArrays: true
                            },
                        },
                        {
                            $lookup: {
                                from: 'distributors',
                                localField: 'distributor',
                                foreignField: '_id',
                                as: 'distributor'
                            }
                        },
                        {
                            $unwind: {
                                path: '$distributor',
                                preserveNullAndEmptyArrays: true
                            },
                        },
                        {
                            $lookup: {
                                from: 'type_employees',
                                localField: 'type_employee',
                                foreignField: '_id',
                                as: 'type_employee'
                            }
                        },
                        {
                            $unwind: {
                                path: '$type_employee',
                                preserveNullAndEmptyArrays: true
                            },
                        },
                        {
                            $match: conditionObj
                        },
                        {
                            $sort: { modifyAt: -1 }
                        }
                    ])

                    if(listEmployeeByFilter && listEmployeeByFilter.length){
                        let index = 2;
                        for (const infoEmployee of listEmployeeByFilter) {
                            // Make edits.
                            workbook.sheet(0).cell(`A${index}`).value(`${infoEmployee.code}`);
                            workbook.sheet(0).cell(`B${index}`).value(`${infoEmployee.fullname}`);
                            workbook.sheet(0).cell(`C${index}`).value(`${infoEmployee.username}`);
                            workbook.sheet(0).cell(`D${index}`).value(`${infoEmployee.email}`);
                            workbook.sheet(0).cell(`E${index}`).value(`${infoEmployee.region.code}`);
                            workbook.sheet(0).cell(`F${index}`).value(`${infoEmployee.area.code}`);
                            workbook.sheet(0).cell(`G${index}`).value(`${infoEmployee.distributor.code}`);
                            workbook.sheet(0).cell(`H${index}`).value(`${infoEmployee.type_employee.name}`);
                            workbook.sheet(0).cell(`I${index}`).value(`${infoEmployee.status === 2 ? 0 : 1}`);

                            index++;
                        }
                    }

                    // Get the output
                    return workbook.outputAsync();
                })
                .then(data => resolve({ error: false, data, fileName: "Template_employee.xlsx" }))
                .catch(err => resolve({ error: true, message: err.message }));
            } catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Import excel nhân viên
     * @extends {BaseModel}
     * @returns {{
     *      error: boolean,
     *      message?: string,
     * }}
     */
    importEmployee({ fileInfo, region }) {
        return new Promise(async resolve => {
            try {
                XlsxPopulate.fromFileAsync(fileInfo.path)
                    .then(async workbook => {
                        let index = 2;
                        let bulkUpdateOps = [];
                        let messageEmployee = '';
                        
                        for (;true;) {
                            let code            = workbook.sheet(0).cell(`A${index}`).value();
                            let fullname        = workbook.sheet(0).cell(`B${index}`).value();
                            let username        = workbook.sheet(0).cell(`C${index}`).value();
                            let email           = workbook.sheet(0).cell(`D${index}`).value();
                            let regionCode      = workbook.sheet(0).cell(`E${index}`).value();
                            let areaCode        = workbook.sheet(0).cell(`F${index}`).value();
                            let distributorCode = workbook.sheet(0).cell(`G${index}`).value();
                            let typeEmployee    = workbook.sheet(0).cell(`H${index}`).value();
                            let status          = workbook.sheet(0).cell(`I${index}`).value();

                            if(!code || !fullname || !username || !regionCode || !areaCode || !distributorCode || !typeEmployee) break;
                            code            = `${code}`.trim();
                            username        = `${username}`.trim();
                            regionCode      = regionCode.trim();
                            areaCode        = areaCode.trim();
                            distributorCode = distributorCode.trim();
                            typeEmployee    = typeEmployee.trim() === 'NA' ? 1 : 2;

                            const infoEmployee          = await EMPLOYEE_COLL.findOne({ code }).lean();
                            const checkExistsUsername   = await EMPLOYEE_COLL.findOne({ username }).lean();
                            const infoRegion            = await REGION_COLL.findOne({ code: regionCode }).select('_id').lean();
                            const infoArea              = await AREA_COLL.findOne({ code: areaCode }).select('_id').lean();
                            const infoDistributor       = await DISTRIBUTOR_COLL.findOne({ code: distributorCode }).select('_id').lean();
                            const infoTypeEmployee      = await TYPE_EMPLOYEE_COLL.findOne({ type: typeEmployee }).select('_id').lean();

                            const dataUpdateOrInsert = {
                                fullname,
                                email,
                                status: status === 0 ? 2 : 1,
                                modifyAt: timeUtils.getCurrentTime(),
                            }

                            infoRegion          && (dataUpdateOrInsert.region           = infoRegion._id);
                            infoArea            && (dataUpdateOrInsert.area             = infoArea._id);
                            infoDistributor     && (dataUpdateOrInsert.distributor      = infoDistributor._id);
                            infoTypeEmployee    && (dataUpdateOrInsert.type_employee    = infoTypeEmployee._id);

                            if(region && checkObjectIDs([region])) {
                                dataUpdateOrInsert.region = region;

                                // Nếu nhân viên import tồn tại nhưng không nằm trong miền của admin -> báo lỗi
                                if(infoEmployee && infoEmployee.region != region) {
                                    messageEmployee += `${code}, `;
                                }
                            }

                            if(infoEmployee){
                                bulkUpdateOps[bulkUpdateOps.length] = {
                                    updateOne: {
                                        filter: { code },
                                        update: {
                                            $set: {
                                                ...dataUpdateOrInsert,
                                                ...!checkExistsUsername && { username }
                                            }
                                        }
                                    }
                                }
                                // console.log({ updateOne: bulkUpdateOps[bulkUpdateOps.length - 1].updateOne.update })
                            } else if(!checkExistsUsername) {
                                bulkUpdateOps[bulkUpdateOps.length] = {
                                    insertOne: {
                                        document: {
                                            ...dataUpdateOrInsert,
                                            code,
                                            username,
                                            password: hashSync('12345', 8),
                                            roles: [ObjectID('61799ef6ccd7cf418c13caa3')],
                                            createAt: timeUtils.getCurrentTime(),
                                        }
                                    }
                                }
                                // console.log({ insertOne: bulkUpdateOps[bulkUpdateOps.length - 1].insertOne.document })
                                // END ELSE IF
                            }

                            index++;
                        }

                        await fs.unlinkSync(fileInfo.path);

                        if(messageEmployee) return resolve({ error: true, message: `Mã nhân viên ${messageEmployee} không thuộc miền của bạn` })

                        const infoAfterBulkWrite = await EMPLOYEE_COLL.bulkWrite(bulkUpdateOps);
                        console.log({ infoAfterBulkWrite })

                        return resolve({ error: false, message: 'Import nhân viên thành công', data: infoAfterBulkWrite });
                    });

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    signInEmployee({ account, password, deviceID, deviceName, registrationID }) {
        return new Promise(async resolve => {
            try {
                if(!account || !password)
                    return resolve({ error: true, message: 'Bạn cần nhập đầy đủ tên đăng nhập và mật khẩu', status: 400 });

                // let [username, prefix] = account.split('_');
                let username = account;

                let infoAccount = await EMPLOYEE_COLL
                    .findOne({ username: username.trim() })
                    .populate('type_employee')
                    .lean();

                if (!infoAccount)
                    return resolve({ error: true, message: 'Tài khoản không tồn tại', status: 400 });

                // if(infoAccount.type_employee.type === 2){
                //     if(!prefix)
                //         return resolve({ error: true, message: 'Vui lòng nhập thêm thông tin PGS, PGM, PGTC, PGCD', status: 400 });

                //     if(!['PGCD', 'PGM', 'PGTC', 'PGS'].includes(prefix))
                //         return resolve({ error: true, message: 'Loại PG không hợp lệ (PGS, PGM, PGTC, PGCD)', status: 400 });
                // }

                // if (infoAccount.type_employee.type === 1 && prefix)
                //     return resolve({ error: true, message: 'Tài khoản không tồn tại', status: 400 });

				if (infoAccount.status === 2)
                    return resolve({ error: true, message: 'Người dùng đã bị khoá', status: 403 });

                let isMatchPass = await compare(password, infoAccount.password);
                if (!isMatchPass)
                    return resolve({ error: true, message: 'Mật khẩu không trùng khớp', status: 400 });

                let infoUser = {
                    _id: infoAccount._id,
                    username: infoAccount.username,
                    email: infoAccount.email,
                    status: infoAccount.status,
                    region: infoAccount.region,
                    area: infoAccount.area,
                    roles: infoAccount.roles,
                    roles: infoAccount.roles,
                    isRegisterFace: infoAccount.isRegisterFace,
                    typeEmployee: infoAccount.type_employee.type,
                    // typePG: prefix
                }
                let token = jwt.sign(infoUser, cfJWS.secret);

                // Update customer cho deviceID
                const infoAfterUpdateEmployeeDevice = await EMPLOYEE_DEVICE_MODEL.updateEmployeeDevice({ 
                    deviceID, employeeID: infoAccount._id, deviceName, registrationID
                });

                return resolve({
                    error: false,
                    status: 200,
                    data: { user: infoUser, token }
                });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    signUpEmployee({ fullname, username, email, password }) {
        return new Promise(async resolve => {
            try {
                console.table({ fullname, username, email, password })
                if(!username || !password)
                    return resolve({ error: true, message: 'Bạn cần nhập đầy đủ tên đăng nhập và mật khẩu', status: 400 });

                if(!fullname)
                    return resolve({ error: true, message: 'Bạn cần nhập đầy đủ họ và tên', status: 400 });

                let infoAccount = await EMPLOYEE_COLL
                    .findOne({ username: username.trim(), state: 1 })
                    .lean();

                if (infoAccount)
                    return resolve({ error: true, message: 'Tài khoản tồn tại', status: 400 });

                password = await hashSync(password, 10);

                let dataInsert = {
                    code: randomStringFixLength(9),
                    fullname,
                    username,
                    email,
                    password,
                    status: 1,
                    region: ObjectID('618184e1e0cc0dd18284696c'),
                    area: ObjectID('618184e1e0cc0dd182846983'),
                    roles: [ObjectID('61799ef6ccd7cf418c13caa3')],
                    type_employee: ObjectID('616da3c5ee78f90f7c04f155'),
                    detail_type_employee: ObjectID('616da66437fe943310c6519e')
                }

                let infoAfterRegister = await this.insertData(dataInsert);

                if(!infoAfterRegister)
                    return resolve({ error: true, message: 'Đăng ký thất bại', status: 422 });

                return resolve({
                    error: false,
                    status: 200,
                    data: infoAfterRegister
                });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    /**
     * Gửi OTP quên mật khẩu EMPLOYEE
     * @param {number} phone
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
     sendOTPForgot__PHONE({ phone }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                /**
                 * NẾU có mail: gửi OTP qua mail
                 * NẾU ko mail: gửi OTP qua SĐT
                 */
                if (!validPhone(phone))
                    return resolve({ error: true, message: 'SĐT không hợp lệ' });

                // if (email && !validEmail(email))
                //     return resolve({ error: true, message: 'Mail không hợp lệ' });

                let isExistPhone = await EMPLOYEE_COLL.findOne({ phone, status: STATUS_ACTIVE });
                if (!isExistPhone)
                    return resolve({ error: true, message: 'SĐT chưa được đăng ký' });

                let infoOTPAfterInsert = await OTP_MODEL.insertOTP__PHONE({ phone, type: OTP_MODEL.TYPE_FORGET_PASS });
                if(infoOTPAfterInsert.error)
                    return resolve(infoOTPAfterInsert)

                return resolve({ error: false, data: infoOTPAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    /**
     * LÂY THÔNG TIN CHECKIN VS INFO EMPLOYEE
     * @param {ObjectID} employeeID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfo__CheckIn_Vs_Employee__ByEmployeeID({ employeeID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employeeID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị employeeID không hợp lệ',
                        status: 400
                    });
                }
                /**
                 *  1. KIỂM TRA EMPLOYEE CÓ ĐANG CHECKIN HAY KHÔNG
                 */
                const STATUS_COMPLETE_CHECKIN = 2;
                let conditionObj__CheckInCheckOut = {
                    createAt: {
                        $gte: moment(new Date()).startOf('day').format(),
                        $lte: moment(new Date()).endOf('day').format(),
                    },
                    employee: employeeID,
                    status: STATUS_COMPLETE_CHECKIN,
                    state: 1
                };

                let infoCheckInOut = await CHECKIN_CHECKOUT_COLL.findOne(conditionObj__CheckInCheckOut).sort({ _id: -1 });
                /**
                 * ===================================================================
                 *  1.1. NẾU ĐANG CHECKIN
                 * ===================================================================
                 */
                if (infoCheckInOut) {
                    /**
                     *  1.1.1. LẤY THÔNG TIN EMPLOYEE
                     */
                    let infoEmployee = await EMPLOYEE_COLL.findById(employeeID);
                    if (!infoEmployee) {
                        return resolve({ error: true, message: 'Nhân viên không tồn tại', status: 400 });
                    }

                    return resolve({ error: false, data: { infoCheckInOut, infoEmployee }, status: 200 });
                } else {
                    /**
                     *  1.2. NẾU KHÔNG ĐANG CHECKIN
                     */
                    return resolve({ error: true, message: 'Bạn chưa CHECKIN, Mời bạn CHECKIN trước khi tiếp tục', status: 400 });
                }
            } catch (error) {
                console.error(error);
                return resolve({ error: true, message: error.message })
            }
        })
    }

    updateRegisterFace({ employeeID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employeeID])) {
                    return resolve({
                        error: true,
                        message: 'Nhân viên không hợp lệ'
                    });
                }

                const infoAfterUpdate = await EMPLOYEE_COLL.findByIdAndUpdate(employeeID, {
                    isRegisterFace: true
                }, { new: true });

                if(!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListImagesFace({ employeeID }){
        return new Promise(async resolve => {
            request(`${process.env.FACE_DOMAIN}/list-image-of-folder?userID=${employeeID}&api_key=${process.env.QFACEID_KEY_API}`, (error, _, body) => {
                console.log({
                    error, body
                })
                if(error) {
                    return resolve({ error: true, message: error });
                }
                resolve(JSON.parse(body));
            });
        })
    }

    clearImagesFaceRegister({ employeeID }){
        return new Promise(async resolve => {
            request(`${process.env.FACE_DOMAIN}/clear-register-face?userID=${employeeID}&api_key=${process.env.QFACEID_KEY_API}`, (error, _, body) => {
                if(error) return resolve({ error: true, message: error });
                resolve(JSON.parse(body));
            });
        })
    }

    updateRegisterFace({ employeeID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employeeID])) {
                    return resolve({
                        error: true,
                        message: 'Nhân viên không hợp lệ'
                    });
                }

                const infoAfterUpdate = await EMPLOYEE_COLL.findByIdAndUpdate(employeeID, {
                    isRegisterFace: true
                }, { new: true });

                if(!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

module.exports.MODEL = new Model;

/**
 * COLLECTIONS
 */
var EMPLOYEE_COLL                 = require('../databases/employee-coll');
var TYPE_EMPLOYEE_COLL            = require('../databases/type_employee-coll');
var DISTRIBUTOR_COLL              = require('../../distributor/databases/distributor-coll');
var { REGION_COLL, AREA_COLL }    = require('../../region_area');
var { CHECKIN_CHECKOUT_COLL }     = require('../../checkin_checkout');
var EMPLOYEE_DEVICE_MODEL	      = require('./employee_device').MODEL;
