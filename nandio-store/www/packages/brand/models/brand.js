"use strict";

/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const path = require('path');
const fs = require('fs');
const XlsxPopulate = require('xlsx-populate');


/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    checkNumberIsValidWithRange
} = require('../../../utils/utils');
const {
    STATUS_BRAND_TYPE,
} = require('../constants/brand');


/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const BRAND_COLL = require('../databases/brand-coll');

const {
    IMAGE_COLL
} = require('../../image');


class Model extends BaseModel {
    constructor() {
        super(BRAND_COLL);
    }

    /**
         * Tạo mới brand
		* @param {string} name
		* @param {string} brandCode
		* @param {string} description
		* @param {object} image
		* @param {array} gallerys
		* @param {string} linkWebsite

         * @param {objectId} userCreate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        name,
        brandCode,
        description,
        image,
        gallerys,
        linkWebsite,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (name.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tên thương hiệu không được lớn hơn 125 ký tự'
                    });
                }

                if (!name) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tên thương hiệu'
                    });
                }

                if (brandCode.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mã thương hiệu không được lớn hơn 125 ký tự'
                    });
                }

                if (!brandCode) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mã thương hiệu'
                    });
                }

                if (description.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mô tả không được lớn hơn 125 ký tự'
                    });
                }

                if (linkWebsite.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài linhk website không được lớn hơn 125 ký tự'
                    });
                }





                if (image && !checkObjectIDs([image])) {
                    return resolve({
                        error: true,
                        message: 'ảnh đại diện không hợp lệ'
                    });
                }

                if (gallerys && !checkObjectIDs(gallerys)) {
                    return resolve({
                        error: true,
                        message: 'bộ sưu tập không hợp lệ'
                    });
                }


                const checkBrandCodeExits = await BRAND_COLL.findOne({
                    brandCode
                });
                if (checkBrandCodeExits) {
                    return resolve({
                        error: true,
                        message: 'Mã ThươNg HiệU đã tồn tại'
                    });
                }


                let dataInsert = {
                    name,
                    brandCode,
                    description,
                    image,
                    gallerys,
                    linkWebsite,
                    userCreate
                };

                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo thương hiệu thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật brand 
         * @param {objectId} brandID
		* @param {string} name
		* @param {string} brandCode
		* @param {string} description
		* @param {object} image
		* @param {number} status
		* @param {array} gallerys
		* @param {string} linkWebsite
		* @param {number} stt

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        brandID,
        name,
        brandCode,
        description,
        image,
        status,
        gallerys,
        linkWebsite,
        stt,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([brandID])) {
                    return resolve({
                        error: true,
                        message: 'brandID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (name.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tên thương hiệu không được lớn hơn 125 ký tự'
                    });
                }

                if (!name) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tên thương hiệu cho brand'
                    });
                }

                if (brandCode.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mã thương hiệu không được lớn hơn 125 ký tự'
                    });
                }

                if (!brandCode) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mã thương hiệu cho brand'
                    });
                }

                if (description.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mô tả không được lớn hơn 125 ký tự'
                    });
                }

                if (linkWebsite.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài linhk website không được lớn hơn 125 ký tự'
                    });
                }


                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trang thái không hợp lệ'
                    });
                }




                if (image && !checkObjectIDs([image])) {
                    return resolve({
                        error: true,
                        message: 'ảnh đại diện không hợp lệ'
                    });
                }

                if (gallerys && !checkObjectIDs(gallerys)) {
                    return resolve({
                        error: true,
                        message: 'bộ sưu tập không hợp lệ'
                    });
                }

                const checkExists = await BRAND_COLL.findById(brandID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'thương hiệu không tồn tại'
                    });
                }

                const checkBrandCodeExits = await BRAND_COLL.findOne({
                    brandCode
                });
                if (checkBrandCodeExits && checkExists.brandCode !== brandCode) {
                    return resolve({
                        error: true,
                        message: 'mã thương hiệu đã tồn tại'
                    });
                }


                let dataUpdate = {
                    userUpdate
                };
                name && (dataUpdate.name = name);
                brandCode && (dataUpdate.brandCode = brandCode);
                dataUpdate.description = description;
                image && (dataUpdate.image = image);
                dataUpdate.status = status;
                gallerys && (dataUpdate.gallerys = gallerys);
                dataUpdate.linkWebsite = linkWebsite;
                dataUpdate.stt = stt;
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: brandID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật brand (không bắt buộc)
         * @param {objectId} brandID
		* @param {string} name
		* @param {string} brandCode
		* @param {string} description
		* @param {object} image
		* @param {number} status
		* @param {array} gallerys
		* @param {string} linkWebsite
		* @param {number} stt

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        brandID,
        name,
        brandCode,
        description,
        image,
        status,
        gallerys,
        linkWebsite,
        stt,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([brandID])) {
                    return resolve({
                        error: true,
                        message: 'brandID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (image && !checkObjectIDs([image])) {
                    return resolve({
                        error: true,
                        message: 'ảnh đại diện không hợp lệ'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trang thái không hợp lệ'
                    });
                }

                if (gallerys && !checkObjectIDs(gallerys)) {
                    return resolve({
                        error: true,
                        message: 'bộ sưu tập không hợp lệ'
                    });
                }

                const checkExists = await BRAND_COLL.findById(brandID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'thương hiệu không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                name && (dataUpdate.name = name);
                brandCode && (dataUpdate.brandCode = brandCode);
                description && (dataUpdate.description = description);
                image && (dataUpdate.image = image);
                status && (dataUpdate.status = status);
                gallerys && (dataUpdate.gallerys = gallerys);
                linkWebsite && (dataUpdate.linkWebsite = linkWebsite);
                stt && (dataUpdate.stt = stt);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: brandID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa brand 
     * @param {objectId} brandID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(brandID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([brandID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị brandID không hợp lệ'
                    });
                }

                const infoAfterDelete = await this.updateById(brandID, {
                    state: 2
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa brand 
     * @param {array} brandID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(brandID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(brandID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị brandID không hợp lệ'
                    });
                }

                const infoAfterDelete = await BRAND_COLL.updateMany({
                    _id: {
                        $in: brandID
                    }
                }, {
                    state: 2,
                    modifyAt: new Date()
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin brand 
     * @param {objectId} brandID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(brandID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([brandID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị brandID không hợp lệ'
                    });
                }

                const infoBrand = await BRAND_COLL.findById(brandID)
                    .populate('image gallerys')

                if (!infoBrand) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin thương hiệu'
                    });
                }

                return resolve({
                    error: false,
                    data: infoBrand
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách brand 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listBrand = await BRAND_COLL
                    .find({
                        state: 1
                    }).populate('image gallerys')
                    .sort({
                        stt: 1,
                        modifyAt: -1
                    })
                    .lean();

                if (!listBrand) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách thương hiệu'
                    });
                }

                return resolve({
                    error: false,
                    data: listBrand
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách brand theo bộ lọc
		* @param {string} keyword
		* @enum {number} status
		* @param {number} stt

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        keyword,
        status,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        name: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        brandCode: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        description: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        linkWebsite: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                status && (conditionObj.status = status);


                const listBrandByFilter = await BRAND_COLL
                    .find(conditionObj).populate('image gallerys')
                    .sort({
                        stt: 1,
                        modifyAt: -1
                    })
                    .lean();

                if (!listBrandByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách thương hiệu"
                    });
                }

                return resolve({
                    error: false,
                    data: listBrandByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách brand theo bộ lọc (server side)
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        name: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        brandCode: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        description: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        linkWebsite: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trang thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    stt: 1,
                    modifyAt: -1
                };

                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalBrand = await BRAND_COLL.countDocuments(conditionObj);

                const listBrandByFilter = await BRAND_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'images',
                            localField: 'image',
                            foreignField: '_id',
                            as: 'image'
                        }
                    },
                    {
                        $unwind: {
                            path: '$image',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listBrandByFilter) {
                    return resolve({
                        recordsTotal: totalBrand,
                        recordsFiltered: totalBrand,
                        data: []
                    });
                }

                const listBrandDataTable = listBrandByFilter.map((brand, index) => {
                    let status = '';
                    if (brand.status == 1) {
                        status = 'checked';
                    }

                    let stt = '';
                    if (brand.stt || brand.stt == 0) {
                        stt = brand.stt;
                    }

                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${brand._id}" type="checkbox" class="check-record check-record-${brand._id}" _index ="${index + 1}"><label for="${brand._id}"></label></div></td>`,
                        name: `<a href="/brand/update-brand-by-id?brandID=${brand._id}">${brand.name && brand.name.length > 50 ? brand.name.substr(0,50) + "..." : brand.name} </a>`,
                        brandCode: `${brand.brandCode && brand.brandCode.length > 50 ? brand.brandCode.substr(0,50) + "..." : brand.brandCode}`,
                        image: `${brand.image ? `<a class="user-avatar me-2 fancybox" href="${brand.image.path}"><img src="${process.env.AWS_S3_URI}${brand.image.path}" alt="" class="thumb-xxl rounded"> </a>` : ""}`,
                        status: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-status" _brandID="${brand._id}" type="checkbox" id="${brand._id}" ${status} style="width: 40px;height: 20px;"></div></td>`,
                        stt: `<td><input type="number" _brandID="${brand._id}" class="form-control change-stt" value="${stt}"></td>`,
                        createAt: moment(brand.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalBrand,
                    recordsFiltered: totalBrand,
                    data: listBrandDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc brand
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc brand
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listBrandByFilter = await BRAND_COLL.aggregate(arrayFilter)

                if (!listBrandByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listBrandByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listBrandByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc brand
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            dataEnum: item.dataEnum,
                            nameCollChoice: item.nameCollChoice,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                dataEnum: item.dataEnum,
                                nameCollChoice: item.nameCollChoice,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (markPopulate == true) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {

            if (filter.length > 1) {
                condition === 'OR' && (conditionObj.$or = []);
                condition === 'AND' && (conditionObj.$and = []);

                filter.map(filterObj => {
                    const getConditionByFilter = this.getConditionObj(filterObj);

                    if (condition === 'OR') {
                        conditionObj.$or.push(getConditionByFilter);
                    } else {
                        conditionObj.$and.push(getConditionByFilter);
                    }
                })
            } else {
                conditionObj = {
                    ...conditionObj,
                    ...this.getConditionObj(filter[0])
                };
            }
        }


        if (!isEmptyObject(objFilterStatic)) {

            if (objFilterStatic.status) {
                if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                    return resolve({
                        error: true,
                        message: "trang thái không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    status: Number(objFilterStatic.status)
                }
            }

        }


        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let lookup = [{
                        $lookup: {
                            from: pluralize.plural(item.coll),
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: pluralize.plural(populate.coll),
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            stt: 1,
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Chi tiết
     * @param {objectId} brandID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoBrand({
        brandID,
        select,
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];

                if (!checkObjectIDs([brandID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị brandID không hợp lệ',
                        status: 400
                    });
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['image', 'gallerys'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoBrand = await BRAND_COLL
                    .findById(brandID)
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoBrand = await BRAND_COLL.populate(infoBrand, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoBrand = await BRAND_COLL.populate(infoBrand, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoBrand) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin brand',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoBrand,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Danh sách
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListBrands({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { state: 1 };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['image', 'status', 'gallerys'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    image,
                    status,
                    gallerys,
                } = filter;

                image && (conditionObj.image = image);

                status && (conditionObj.status = status);

                if (gallerys && gallerys.length) {
                    conditionObj.gallerys = {
                        $in: gallerys
                    };
                }


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['image', 'gallerys'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listBrands = await BRAND_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listBrands = await BRAND_COLL.populate(listBrands, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listBrands = await BRAND_COLL.populate(listBrands, `${ref}.${field}`);
                        }
                    }
                }

                if (!listBrands) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách brand',
                        status: 400
                    });
                }

                let totalRecord = await BRAND_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listBrands,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa 1
     * @param {objectId} brandID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteBrand(brandID) {
        return new Promise(async resolve => {
            try {
                let ids = [brandID];

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị brandID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.updateWhereClause({
                    _id: {
                        $in: ids
                    }
                }, {
                    state: 2
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa nhiều
     * @param {objectId} brandID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteBrands(brandsID) {
        return new Promise(async resolve => {
            try {
                let ids = brandsID.split(',');

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị brandsID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.updateWhereClause({
                    _id: {
                        $in: ids
                    }
                }, {
                    state: 2
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

}

exports.MODEL = new Model;