"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('brand', {
    /**
     * Tên thương hiệu
     */
    name: {
        type: String,
        required: true,
    },
    /**
     * Mã thương hiệu
     */
    brandCode: {
        type: String,
        unique: true,
        required: true,
    },
    /**
     * Mô tả
     */
    description: {
        type: String,
    },
    /**
     * Ảnh đại diện
     */
    image: {
        type: Schema.Types.ObjectId,
        ref: 'image',
    },
    /**
     * Trang thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
    /**
     * Bộ sưu tập
     */
    gallerys: [{
        type: Schema.Types.ObjectId,
        ref: 'image',
    }],
    /**
     * Linhk website
     */
    linkWebsite: {
        type: String,
    },
    /**
     * Số thứ tự
     */
    stt: {
        type: Number,
    },
});