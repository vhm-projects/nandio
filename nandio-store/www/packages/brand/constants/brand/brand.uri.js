const BASE_ROUTE = '/brand';
const API_BASE_ROUTE = '/api/brand';

const CF_ROUTINGS_BRAND = {
    ADD_BRAND: `${BASE_ROUTE}/add-brand`,
    UPDATE_BRAND_BY_ID: `${BASE_ROUTE}/update-brand-by-id`,
    DELETE_BRAND_BY_ID: `${BASE_ROUTE}/delete/:brandID`,

    GET_INFO_BRAND_BY_ID: `${BASE_ROUTE}/info/:brandID`,
    GET_LIST_BRAND: `${BASE_ROUTE}/list-brand`,
    GET_LIST_BRAND_BY_FIELD: `${BASE_ROUTE}/list-brand/:field/:value`,
    GET_LIST_BRAND_SERVER_SIDE: `${BASE_ROUTE}/list-brand-server-side`,

    UPDATE_BRAND_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-brand-by-id-v2`,
    DELETE_BRAND_BY_LIST_ID: `${BASE_ROUTE}/delete-brand-by-list-id`,
    GET_LIST_BRAND_EXCEL: `${BASE_ROUTE}/list-brand-excel`,

    API_GET_INFO_BRAND: `${API_BASE_ROUTE}/info-brand/:brandID`,
    API_GET_LIST_BRANDS: `${API_BASE_ROUTE}/list-brands`,
    API_DELETE_BRAND: `${API_BASE_ROUTE}/delete-brand/:brandID`,
    API_DELETE_BRANDS: `${API_BASE_ROUTE}/delete-brands/:brandsID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_BRAND = CF_ROUTINGS_BRAND;