"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const beautifyer = require('js-beautify').js_beautify;
const fs = require('fs');
const moment = require('moment');
const logger = require('../../../config/logger/winston.config');
const chalk = require('chalk');
const log = console.log;
/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_BRAND_TYPE,
} = require('../constants/brand');
const {
    CF_ROUTINGS_BRAND
} = require('../constants/brand/brand.uri');

/**
 * MODELS
 */
const BRAND_MODEL = require('../models/brand').MODEL;
const IMAGE_MODEL = require('../../image/models/image').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ BRAND  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Brand (API, VIEW)
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.ADD_BRAND]: {
                config: {
                    scopes: ['create:brand'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Brand',
                    code: CF_ROUTINGS_BRAND.ADD_BRAND,
                    inc: path.resolve(__dirname, '../views/brand/add_brand.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_BRAND
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            name,
                            brandCode,
                            description,
                            image,
                            gallerys,
                            linkWebsite,
                        } = req.body;


                        if (image) {
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                name: image.name,
                                path: image.path,
                                size: image.size
                            });
                            image = infoImageAfterInsert.data._id;
                        }

                        if (gallerys && gallerys.length) {

                            let listFiles = gallerys.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))
                            listFiles = await Promise.all(listFiles);

                            gallerys = listFiles.map(file => file.data._id);
                        }

                        let infoAfterInsert = await BRAND_MODEL.insert({
                            name,
                            brandCode,
                            description,
                            image,
                            gallerys,
                            linkWebsite,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Brand By Id (API, VIEW)
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.UPDATE_BRAND_BY_ID]: {
                config: {
                    scopes: ['update:brand'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Brand',
                    code: CF_ROUTINGS_BRAND.UPDATE_BRAND_BY_ID,
                    inc: path.resolve(__dirname, '../views/brand/update_brand.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            brandID
                        } = req.query;

                        let infoBrand = await BRAND_MODEL.getInfoById(brandID);
                        if (infoBrand.error) {
                            return res.redirect('/something-went-wrong');
                        }



                        ChildRouter.renderToView(req, res, {
                            infoBrand: infoBrand.data || {},


                            CF_ROUTINGS_BRAND
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            brandID,
                            name,
                            brandCode,
                            description,
                            image,
                            status,
                            gallerys,
                            linkWebsite,
                            stt,
                        } = req.body;


                        if (image) {
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                name: image.name,
                                path: image.path,
                                size: image.size
                            });
                            image = infoImageAfterInsert.data._id;
                        }

                        if (gallerys && gallerys.length) {
                            let listFiles = gallerys.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))
                            listFiles = await Promise.all(listFiles);

                            gallerys = listFiles.map(file => file.data._id);
                        }

                        const infoAfterUpdate = await BRAND_MODEL.update({
                            brandID,
                            name,
                            brandCode,
                            description,
                            image,
                            status,
                            gallerys,
                            linkWebsite,
                            stt,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Brand By Id (API)
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.UPDATE_BRAND_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:brand'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            brandID,
                            name,
                            brandCode,
                            description,
                            image,
                            status,
                            gallerys,
                            linkWebsite,
                            stt,
                        } = req.body;


                        if (image) {
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                name: image.name,
                                path: image.path,
                                size: image.size
                            });
                            image = infoImageAfterInsert.data._id;
                        }

                        if (gallerys && gallerys.length) {
                            let listFiles = gallerys.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))
                            listFiles = await Promise.all(listFiles);

                            gallerys = listFiles.map(file => file.data._id);
                        }

                        const infoAfterUpdate = await BRAND_MODEL.updateNotRequire({
                            brandID,
                            name,
                            brandCode,
                            description,
                            image,
                            status,
                            gallerys,
                            linkWebsite,
                            stt,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Brand By Id (API)
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.DELETE_BRAND_BY_ID]: {
                config: {
                    scopes: ['delete:brand'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            brandID
                        } = req.params;

                        const infoAfterDelete = await BRAND_MODEL.deleteById(brandID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Brand By List Id (API)
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.DELETE_BRAND_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:brand'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            brandID
                        } = req.body;

                        const infoAfterDelete = await BRAND_MODEL.deleteByListId(brandID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Brand By Id (API)
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.GET_INFO_BRAND_BY_ID]: {
                config: {
                    scopes: ['read:info_brand'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            brandID
                        } = req.params;

                        const infoBrandById = await BRAND_MODEL.getInfoById(brandID);
                        res.json(infoBrandById);
                    }]
                },
            },

            /**
             * Function: Get List Brand (API, VIEW)
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.GET_LIST_BRAND]: {
                config: {
                    scopes: ['read:list_brand'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách thương hiệu',
                    code: CF_ROUTINGS_BRAND.GET_LIST_BRAND,
                    inc: path.resolve(__dirname, '../views/brand/list_brands.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            status,
                            typeGetList
                        } = req.query;

                        let listBrands = [];
                        if (typeGetList === 'FILTER') {
                            listBrands = await BRAND_MODEL.getListByFilter({
                                keyword,
                                status,
                            });
                        } else {
                            listBrands = await BRAND_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listBrands: listBrands.data || [],
                            STATUS_BRAND_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Brand By Field (API, VIEW)
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.GET_LIST_BRAND_BY_FIELD]: {
                config: {
                    scopes: ['read:list_brand'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Brand by field isStatus',
                    code: CF_ROUTINGS_BRAND.GET_LIST_BRAND_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/brand/list_brands.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            status,
                            type
                        } = req.query;

                        let listBrands = await BRAND_MODEL.getListByFilter({
                            keyword,
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listBrands: listBrands.data || [],
                            STATUS_BRAND_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Brand Server Side (API)
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.GET_LIST_BRAND_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_brand'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listBrandServerSide = await BRAND_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listBrandServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Brand Excel Server Side (API)
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.GET_LIST_BRAND_EXCEL]: {
                config: {
                    scopes: ['read:list_brand'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            filter,
                            condition,
                            nameOfParentColl,
                            objFilterStatic,
                            order,
                            keyword
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let conditionObj = BRAND_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword)
                        let listBrand = await BRAND_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice
                        })

                        res.json(listBrand)
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.API_GET_INFO_BRAND]: {
                config: {
                    scopes: ['read:info_brand'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            brandID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoBrand = await BRAND_MODEL.getInfoBrand({
                            brandID,
                            select,
                            explain
                        });
                        res.json(infoBrand);
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.API_GET_LIST_BRANDS]: {
                config: {
                    scopes: ['read:list_brand'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listBrands = await BRAND_MODEL.getListBrands({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listBrands);
                    }]
                },
            },

            /**
             * Function: Xóa 1
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.API_DELETE_BRAND]: {
                config: {
                    scopes: ['delete:brand'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            brandID
                        } = req.params;

                        const infoAfterDelete = await BRAND_MODEL.deleteBrand(brandID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Xóa nhiều
             * Date: 09/11/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_BRAND.API_DELETE_BRANDS]: {
                config: {
                    scopes: ['delete:brand'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            brandsID
                        } = req.params;

                        const infoAfterDelete = await BRAND_MODEL.deleteBrands(brandsID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

        }
    }
};