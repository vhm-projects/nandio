const BRAND_COLL = require('./databases/brand-coll');
const BRAND_MODEL = require('./models/brand').MODEL;
const BRAND_ROUTES = require('./apis/brand');
// MARK REQUIRE

module.exports = {
    BRAND_COLL,
    BRAND_MODEL,
    BRAND_ROUTES,
    // MARK EXPORT
}