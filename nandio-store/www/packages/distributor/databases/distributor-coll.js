"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('distributor', {
    idSql: Number,
    id: {
        type: String,
        trim: true
    },
    idArea: {
        type: String,
        trim: true
    },
    /**
     * Tên nhà phân phối
     */
    name: {
        type: String,
        required: true,
    },
    /**
     * Mã nhà phân  phối
     */
    code: {
        type: String,
        required: true,
    },
    /**
     * Mô tả
     */
    description: {
        type: String,
    },
    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Vùng
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
});