const DISTRIBUTOR_COLL = require('./databases/distributor-coll');
const DISTRIBUTOR_MODEL = require('./models/distributor').MODEL;
const DISTRIBUTOR_ROUTES = require('./apis/distributor');
// MARK REQUIRE

module.exports = {
    DISTRIBUTOR_COLL,
    DISTRIBUTOR_MODEL,
    DISTRIBUTOR_ROUTES,
    // MARK EXPORT
}