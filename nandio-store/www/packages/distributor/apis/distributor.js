"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_DISTRIBUTOR_TYPE,
} = require('../constants/distributor');
const {
    CF_ROUTINGS_DISTRIBUTOR
} = require('../constants/distributor/distributor.uri');

/**
 * MODELS
 */
const DISTRIBUTOR_MODEL = require('../models/distributor').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */
const {
    REGION_COLL
} = require('../../region_area');

const {
    AREA_COLL
} = require('../../region_area');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ DISTRIBUTOR  ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: Get List area By parent (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            '/distributor/list-area-by-parent': {
                config: {
                    scopes: ['read:list_distributor'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            region
                        } = req.query;

                        const listAreaByParent = await AREA_COLL
                            .find({
                                region
                            })
                            .lean();
                        res.json(listAreaByParent);
                    }]
                },
            },

            /**
             * Function: Insert Distributor (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DISTRIBUTOR.ADD_DISTRIBUTOR]: {
                config: {
                    scopes: ['create:distributor'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm nhà phân phối',
                    code: CF_ROUTINGS_DISTRIBUTOR.ADD_DISTRIBUTOR,
                    inc: path.resolve(__dirname, '../views/distributor/add_distributor.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;

                        let condition = { state: 1, status: 1 };
                        region && (condition._id = region);

                        let listRegions = await REGION_COLL
                            .find(condition)
                            .sort({
                                modifyAt: -1
                            })
                            .lean();

                        ChildRouter.renderToView(req, res, {
                            listRegions,
                            CF_ROUTINGS_DISTRIBUTOR
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            name,
                            code,
                            description,
                            region,
                            area,
                        } = req.body;

                        let infoAfterInsert = await DISTRIBUTOR_MODEL.insert({
                            name,
                            code,
                            description,
                            region,
                            area,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Distributor By Id (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DISTRIBUTOR.UPDATE_DISTRIBUTOR_BY_ID]: {
                config: {
                    scopes: ['update:distributor'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật nhà phân phối',
                    code: CF_ROUTINGS_DISTRIBUTOR.UPDATE_DISTRIBUTOR_BY_ID,
                    inc: path.resolve(__dirname, '../views/distributor/update_distributor.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            distributorID
                        } = req.query;
                        let { region } = req.user;

                        let condition = { state: 1, status: 1 };
                        region && (condition._id = region);

                        let infoDistributor = await DISTRIBUTOR_MODEL.getInfoById(distributorID);
                        if (infoDistributor.error) {
                            return res.redirect('/something-went-wrong');
                        }

                        let listRegions = await REGION_COLL.find(condition)
                            .sort({
                                modifyAt: -1
                            }).lean()

                        let listAreas = [];
                        if (infoDistributor.data.region) {
                            listAreas = await AREA_COLL.find({
                                state: 1,
                                status: 1,
                                region: infoDistributor.data.region._id
                            }).sort({
                                modifyAt: -1
                            }).lean()
                        }

                        ChildRouter.renderToView(req, res, {
                            infoDistributor: infoDistributor.data || {},
                            listRegions,
                            listAreas,
                            CF_ROUTINGS_DISTRIBUTOR
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            distributorID,
                            name,
                            code,
                            description,
                            region,
                            area,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await DISTRIBUTOR_MODEL.update({
                            distributorID,
                            name,
                            code,
                            description,
                            region,
                            area,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Distributor By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DISTRIBUTOR.UPDATE_DISTRIBUTOR_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:distributor'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            distributorID,
                            name,
                            code,
                            description,
                            region,
                            area,
                            status,
                        } = req.body;

                        const infoAfterUpdate = await DISTRIBUTOR_MODEL.updateNotRequire({
                            distributorID,
                            name,
                            code,
                            description,
                            region,
                            area,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Distributor By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DISTRIBUTOR.DELETE_DISTRIBUTOR_BY_ID]: {
                config: {
                    scopes: ['delete:distributor'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            distributorID
                        } = req.params;

                        const infoAfterDelete = await DISTRIBUTOR_MODEL.deleteById(distributorID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Distributor By List Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DISTRIBUTOR.DELETE_DISTRIBUTOR_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:distributor'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            distributorID
                        } = req.body;

                        const infoAfterDelete = await DISTRIBUTOR_MODEL.deleteByListId(distributorID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Distributor By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DISTRIBUTOR.GET_INFO_DISTRIBUTOR_BY_ID]: {
                config: {
                    scopes: ['read:info_distributor'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            distributorID
                        } = req.params;

                        const infoDistributorById = await DISTRIBUTOR_MODEL.getInfoById(distributorID);
                        res.json(infoDistributorById);
                    }]
                },
            },

            /**
             * Function: Get List Distributor (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DISTRIBUTOR.GET_LIST_DISTRIBUTOR]: {
                config: {
                    scopes: ['read:list_distributor'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách nhà phân phối',
                    code: CF_ROUTINGS_DISTRIBUTOR.GET_LIST_DISTRIBUTOR,
                    inc: path.resolve(__dirname, '../views/distributor/list_distributors.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let {
                            keyword,
                            status,
                            typeGetList
                        } = req.query;

                        let listDistributors = [];
                        if (typeGetList === 'FILTER') {
                            listDistributors = await DISTRIBUTOR_MODEL.getListByFilter({
                                keyword,
                                status,
                                region
                            });
                        } else {
                            listDistributors = await DISTRIBUTOR_MODEL.getList({ region });
                        }

                        ChildRouter.renderToView(req, res, {
                            listDistributors: listDistributors.data || [],
                            STATUS_DISTRIBUTOR_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Distributor By Field (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DISTRIBUTOR.GET_LIST_DISTRIBUTOR_BY_FIELD]: {
                config: {
                    scopes: ['read:list_distributor'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách nhà phân phối',
                    code: CF_ROUTINGS_DISTRIBUTOR.GET_LIST_DISTRIBUTOR_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/distributor/list_distributors.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { region } = req.user;
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            status,
                            type
                        } = req.query;

                        let listDistributors = await DISTRIBUTOR_MODEL.getListByFilter({
                            keyword,
                            status,
                            [field]: value,
                            region
                        });

                        ChildRouter.renderToView(req, res, {
                            listDistributors: listDistributors.data || [],
                            STATUS_DISTRIBUTOR_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Distributor Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DISTRIBUTOR.GET_LIST_DISTRIBUTOR_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_distributor'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listDistributorServerSide = await DISTRIBUTOR_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(listDistributorServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Distributor Excel Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DISTRIBUTOR.GET_LIST_DISTRIBUTOR_EXCEL]: {
                config: {
                    scopes: ['read:list_distributor'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            filter,
                            condition,
                            nameOfParentColl,
                            objFilterStatic,
                            order,
                            keyword
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let conditionObj = DISTRIBUTOR_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword)
                        let listDistributor = await DISTRIBUTOR_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice
                        })

                        res.json(listDistributor)
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DISTRIBUTOR.API_GET_INFO_DISTRIBUTOR]: {
                config: {
                    scopes: ['read:info_distributor'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            distributorID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoDistributor = await DISTRIBUTOR_MODEL.getInfoDistributor({
                            distributorID,
                            select,
                            explain
                        });
                        res.json(infoDistributor);
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_DISTRIBUTOR.API_GET_LIST_DISTRIBUTORS]: {
                config: {
                    scopes: ['read:list_distributor'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { region: regionAdmin } = req.user;
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listDistributors = await DISTRIBUTOR_MODEL.getListDistributors({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                            regionAdmin
                        });
                        res.json(listDistributors);
                    }]
                },
            },

        }
    }
};