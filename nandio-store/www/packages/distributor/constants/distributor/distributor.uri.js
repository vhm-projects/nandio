const BASE_ROUTE = '/distributor';
const API_BASE_ROUTE = '/api/distributor';

const CF_ROUTINGS_DISTRIBUTOR = {
    ADD_DISTRIBUTOR: `${BASE_ROUTE}/add-distributor`,
    UPDATE_DISTRIBUTOR_BY_ID: `${BASE_ROUTE}/update-distributor-by-id`,
    DELETE_DISTRIBUTOR_BY_ID: `${BASE_ROUTE}/delete/:distributorID`,

    GET_INFO_DISTRIBUTOR_BY_ID: `${BASE_ROUTE}/info/:distributorID`,
    GET_LIST_DISTRIBUTOR: `${BASE_ROUTE}/list-distributor`,
    GET_LIST_DISTRIBUTOR_BY_FIELD: `${BASE_ROUTE}/list-distributor/:field/:value`,
    GET_LIST_DISTRIBUTOR_SERVER_SIDE: `${BASE_ROUTE}/list-distributor-server-side`,

    UPDATE_DISTRIBUTOR_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-distributor-by-id-v2`,
    DELETE_DISTRIBUTOR_BY_LIST_ID: `${BASE_ROUTE}/delete-distributor-by-list-id`,
    GET_LIST_DISTRIBUTOR_EXCEL: `${BASE_ROUTE}/list-distributor-excel`,

    API_GET_INFO_DISTRIBUTOR: `${API_BASE_ROUTE}/info-distributor/:distributorID`,
    API_GET_LIST_DISTRIBUTORS: `${API_BASE_ROUTE}/list-distributors`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_DISTRIBUTOR = CF_ROUTINGS_DISTRIBUTOR;