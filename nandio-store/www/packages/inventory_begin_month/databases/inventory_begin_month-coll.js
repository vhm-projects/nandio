"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('inventory_begin_month', {
    /**
     * Tiêu đề
     */
    title: {
        type: String,
    },
    /**
     * Tháng
     */
    month: {
        type: Number,
        required: true,
    },
    /**
     * Năm
     */
    year: {
        type: Number,
        required: true,
    },
    /**
     * Sản phẩm quà tặng
     */
    sku: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },
    /**
     * Số lượng
     */
    amount: {
        type: Number,
        default: 0,
        required: true,
    },
    /**
     * Nhà phân phối
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Vùng
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * Nhân viên
     */
    employee: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },
});