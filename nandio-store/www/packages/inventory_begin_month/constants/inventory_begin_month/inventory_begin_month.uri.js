const BASE_ROUTE = '/inventory_begin_month';
const API_BASE_ROUTE = '/api/inventory_begin_month';

const CF_ROUTINGS_INVENTORY_BEGIN_MONTH = {
    ADD_INVENTORY_BEGIN_MONTH: `${BASE_ROUTE}/add-inventory_begin_month`,
    UPDATE_INVENTORY_BEGIN_MONTH_BY_ID: `${BASE_ROUTE}/update-inventory_begin_month-by-id`,
    DELETE_INVENTORY_BEGIN_MONTH_BY_ID: `${BASE_ROUTE}/delete/:inventory_begin_monthID`,

    GET_INFO_INVENTORY_BEGIN_MONTH_BY_ID: `${BASE_ROUTE}/info/:inventory_begin_monthID`,
    GET_LIST_INVENTORY_BEGIN_MONTH: `${BASE_ROUTE}/list-inventory_begin_month`,
    GET_LIST_INVENTORY_BEGIN_MONTH_BY_FIELD: `${BASE_ROUTE}/list-inventory_begin_month/:field/:value`,
    GET_LIST_INVENTORY_BEGIN_MONTH_SERVER_SIDE: `${BASE_ROUTE}/list-inventory_begin_month-server-side`,

    UPDATE_INVENTORY_BEGIN_MONTH_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-inventory_begin_month-by-id-v2`,
    DELETE_INVENTORY_BEGIN_MONTH_BY_LIST_ID: `${BASE_ROUTE}/delete-inventory_begin_month-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_INVENTORY_BEGIN_MONTH_EXCEL: `${BASE_ROUTE}/list-inventory_begin_month-excel`,
    DOWNLOAD_LIST_INVENTORY_BEGIN_MONTH_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-inventory_begin_month-excel-export`,

    // IMPORT EXCEL
    GET_LIST_INVENTORY_BEGIN_MONTH_IMPORT: `${BASE_ROUTE}/list-inventory_begin_month-import`,
    SETTING_FILE_INVENTORY_BEGIN_MONTH_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-inventory_begin_month-import-setting`,
    DOWNLOAD_FILE_INVENTORY_BEGIN_MONTH_EXCEL_IMPORT: `${BASE_ROUTE}/list-inventory_begin_month-import-dowload`,
    CREATE_INVENTORY_BEGIN_MONTH_IMPORT_EXCEL: `${BASE_ROUTE}/create-inventory_begin_month-import-excel`,

    API_GET_INFO_INVENTORY_BEGIN_MONTH: `${API_BASE_ROUTE}/info-inventory_begin_month/:inventory_begin_monthID`,
    API_GET_LIST_INVENTORY_BEGIN_MONTHS: `${API_BASE_ROUTE}/list-inventory_begin_months`,
    API_GET_LIST_IMPORT_EXPORT_GIFT: `${API_BASE_ROUTE}/list-import-export-gift`,
    API_DELETE_INVENTORY_BEGIN_MONTH: `${API_BASE_ROUTE}/delete-inventory_begin_month/:inventory_begin_monthID`,

    API_IMPORT_INVENTORY: `${API_BASE_ROUTE}/import-inventory`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_INVENTORY_BEGIN_MONTH = CF_ROUTINGS_INVENTORY_BEGIN_MONTH;