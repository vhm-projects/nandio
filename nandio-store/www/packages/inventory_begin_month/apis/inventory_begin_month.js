"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');


/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    CF_ROUTINGS_INVENTORY_BEGIN_MONTH
} = require('../constants/inventory_begin_month/inventory_begin_month.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const INVENTORY_BEGIN_MONTH_MODEL = require('../models/inventory_begin_month').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const {
    PRODUCT_COLL
} = require('../../product');

const {
    EMPLOYEE_COLL
} = require('../../employee');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ INVENTORY_BEGIN_MONTH  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Inventory_begin_month (API, VIEW)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            // [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.ADD_INVENTORY_BEGIN_MONTH]: {
            //     config: {
            //         scopes: ['create:inventory_begin_month'],
            //         type: 'view',
            //         view: 'index.ejs',
            //         title: 'Thêm Inventory_begin_month',
            //         code: CF_ROUTINGS_INVENTORY_BEGIN_MONTH.ADD_INVENTORY_BEGIN_MONTH,
            //         inc: path.resolve(__dirname, '../views/inventory_begin_month/add_inventory_begin_month.ejs')
            //     },
            //     methods: {
            //         get: [async function(req, res) {


            //             ChildRouter.renderToView(req, res, {

            //                 CF_ROUTINGS_INVENTORY_BEGIN_MONTH
            //             });
            //         }],
            //         post: [async function(req, res) {
            //             let userCreate = req.user && req.user._id;
            //             let {
            //                 title,
            //                 month,
            //                 year,
            //                 sku,
            //                 amount,
            //                 employee,
            //             } = req.body;


            //             let infoAfterInsert = await INVENTORY_BEGIN_MONTH_MODEL.insert({
            //                 title,
            //                 month,
            //                 year,
            //                 sku,
            //                 amount,
            //                 employee,
            //                 userCreate
            //             });
            //             res.json(infoAfterInsert);
            //         }]
            //     },
            // },

            /**
             * Function: Update Inventory_begin_month By Id (API, VIEW)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.UPDATE_INVENTORY_BEGIN_MONTH_BY_ID]: {
                config: {
                    scopes: ['update:inventory_begin_month'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Inventory_begin_month',
                    code: CF_ROUTINGS_INVENTORY_BEGIN_MONTH.UPDATE_INVENTORY_BEGIN_MONTH_BY_ID,
                    inc: path.resolve(__dirname, '../views/inventory_begin_month/update_inventory_begin_month.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            inventory_begin_monthID
                        } = req.query;

                        let infoInventory_begin_month = await INVENTORY_BEGIN_MONTH_MODEL.getInfoById(inventory_begin_monthID);
                        if (infoInventory_begin_month.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionSku = {
                            state: 1,
                            status: 1
                        };
                        if (infoInventory_begin_month.data.sku) {

                            conditionSku._id = infoInventory_begin_month.data.sku._id;

                        }

                        let listSkus = await PRODUCT_COLL
                            .find(conditionSku)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionEmployee = {
                            state: 1,
                            status: 1
                        };
                        if (infoInventory_begin_month.data.employee) {

                            conditionEmployee._id = infoInventory_begin_month.data.employee._id;

                        }

                        let listEmployees = await EMPLOYEE_COLL
                            .find(conditionEmployee)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoInventory_begin_month: infoInventory_begin_month.data || {},

                            listSkus,
                            listEmployees,
                            CF_ROUTINGS_INVENTORY_BEGIN_MONTH
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            inventory_begin_monthID,
                            title,
                            month,
                            year,
                            sku,
                            amount,
                            employee,
                        } = req.body;


                        const infoAfterUpdate = await INVENTORY_BEGIN_MONTH_MODEL.update({
                            inventory_begin_monthID,
                            title,
                            month,
                            year,
                            sku,
                            amount,
                            employee,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Inventory_begin_month By Id (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.UPDATE_INVENTORY_BEGIN_MONTH_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            inventory_begin_monthID,
                            title,
                            month,
                            year,
                            sku,
                            amount,
                            employee,
                        } = req.body;


                        const infoAfterUpdate = await INVENTORY_BEGIN_MONTH_MODEL.updateNotRequire({
                            inventory_begin_monthID,
                            title,
                            month,
                            year,
                            sku,
                            amount,
                            employee,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Inventory_begin_month By Id (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.DELETE_INVENTORY_BEGIN_MONTH_BY_ID]: {
                config: {
                    scopes: ['delete:inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            inventory_begin_monthID
                        } = req.params;

                        const infoAfterDelete = await INVENTORY_BEGIN_MONTH_MODEL.deleteById(inventory_begin_monthID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Inventory_begin_month By List Id (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.DELETE_INVENTORY_BEGIN_MONTH_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            inventory_begin_monthID
                        } = req.body;

                        const infoAfterDelete = await INVENTORY_BEGIN_MONTH_MODEL.deleteByListId(inventory_begin_monthID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Inventory_begin_month By Id (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.GET_INFO_INVENTORY_BEGIN_MONTH_BY_ID]: {
                config: {
                    scopes: ['read:info_inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            inventory_begin_monthID
                        } = req.params;

                        const infoInventory_begin_monthById = await INVENTORY_BEGIN_MONTH_MODEL.getInfoById(inventory_begin_monthID);
                        res.json(infoInventory_begin_monthById);
                    }]
                },
            },

            /**
             * Function: Get List Inventory_begin_month (API, VIEW)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.GET_LIST_INVENTORY_BEGIN_MONTH]: {
                config: {
                    scopes: ['read:list_inventory_begin_month'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách xuất nhập quà',
                    code: CF_ROUTINGS_INVENTORY_BEGIN_MONTH.GET_LIST_INVENTORY_BEGIN_MONTH,
                    inc: path.resolve(__dirname, '../views/inventory_begin_month/list_inventory_begin_months.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            amountFromNumber,
                            amountToNumber,
                            typeGetList
                        } = req.query;

                        let listInventory_begin_months = [];
                        if (typeGetList === 'FILTER') {
                            listInventory_begin_months = await INVENTORY_BEGIN_MONTH_MODEL.getListByFilter({
                                keyword,
                                amountFromNumber,
                                amountToNumber,
                            });
                        } else {
                            listInventory_begin_months = await INVENTORY_BEGIN_MONTH_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listInventory_begin_months: listInventory_begin_months.data || [],
                        });
                    }]
                },
            },

            /**
             * Function: Get List Inventory_begin_month By Field (API, VIEW)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.GET_LIST_INVENTORY_BEGIN_MONTH_BY_FIELD]: {
                config: {
                    scopes: ['read:list_inventory_begin_month'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Inventory_begin_month by field isStatus',
                    code: CF_ROUTINGS_INVENTORY_BEGIN_MONTH.GET_LIST_INVENTORY_BEGIN_MONTH_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/inventory_begin_month/list_inventory_begin_months.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            amountFromNumber,
                            amountToNumber,
                            type
                        } = req.query;

                        let listInventory_begin_months = await INVENTORY_BEGIN_MONTH_MODEL.getListByFilter({
                            keyword,
                            amountFromNumber,
                            amountToNumber,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listInventory_begin_months: listInventory_begin_months.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Inventory_begin_month Server Side (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.GET_LIST_INVENTORY_BEGIN_MONTH_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listInventory_begin_monthServerSide = await INVENTORY_BEGIN_MONTH_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listInventory_begin_monthServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Inventory_begin_month Import (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.GET_LIST_INVENTORY_BEGIN_MONTH_IMPORT]: {
                config: {
                    scopes: ['read:list_inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listInventory_begin_monthImport = await INVENTORY_BEGIN_MONTH_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listInventory_begin_monthImport);
                    }]
                },
            },

            /**
             * Function: Get List Inventory_begin_month Excel Server Side (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.GET_LIST_INVENTORY_BEGIN_MONTH_EXCEL]: {
                config: {
                    scopes: ['read:list_inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = INVENTORY_BEGIN_MONTH_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Inventory_begin_month Excel Export (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.DOWNLOAD_LIST_INVENTORY_BEGIN_MONTH_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'inventory_begin_month'
                        });

                        let conditionObj = INVENTORY_BEGIN_MONTH_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listInventory_begin_month = await INVENTORY_BEGIN_MONTH_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listInventory_begin_month)
                    }]
                },
            },

            /**
             * Function: Setting Inventory_begin_month Excel Import (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.SETTING_FILE_INVENTORY_BEGIN_MONTH_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = INVENTORY_BEGIN_MONTH_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Inventory_begin_month Excel Import (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.DOWNLOAD_FILE_INVENTORY_BEGIN_MONTH_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'inventory_begin_month'
                        });

                        let listInventory_begin_monthImport = await INVENTORY_BEGIN_MONTH_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listInventory_begin_monthImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listInventory_begin_monthImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Inventory_begin_month Excel Import (API)
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.CREATE_INVENTORY_BEGIN_MONTH_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'inventory_begin_month'
                        });

                        let infoInventory_begin_monthAfterImport = await INVENTORY_BEGIN_MONTH_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'inventory_begin_month',
                        });

                        res.json(infoInventory_begin_monthAfterImport);
                    }]
                },
            },

            /**
             * Function: API Get info Inventory_begin_month
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.API_GET_INFO_INVENTORY_BEGIN_MONTH]: {
                config: {
                    scopes: ['read:info_inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            inventory_begin_monthID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoInventory_begin_month = await INVENTORY_BEGIN_MONTH_MODEL.getInfoInventory_begin_month({
                            inventory_begin_monthID,
                            select,
                            filter,
                            explain
                        });
                        res.json(infoInventory_begin_month);
                    }]
                },
            },

            /**
             * Function: API Get info Inventory_begin_month
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.API_GET_LIST_INVENTORY_BEGIN_MONTHS]: {
                config: {
                    scopes: ['read:list_inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listInventory_begin_months = await INVENTORY_BEGIN_MONTH_MODEL.getListInventory_begin_months({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listInventory_begin_months);
                    }]
                },
            },

            /**
             * Function: API Get list import export gift
             * Date: 04/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.API_GET_LIST_IMPORT_EXPORT_GIFT]: {
                config: {
                    scopes: ['read:list_inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const { _id: employeeID } = req.user;
                        const {
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listImportExportGift = await INVENTORY_BEGIN_MONTH_MODEL.getListImportExportGift({
                            employeeID,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listImportExportGift);
                    }]
                },
            },

            /**
             * Function: API Delete Inventory_begin_month
             * Date: 03/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.API_DELETE_INVENTORY_BEGIN_MONTH]: {
                config: {
                    scopes: ['delete:inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            inventory_begin_monthID
                        } = req.params;

                        const infoAfterDelete = await INVENTORY_BEGIN_MONTH_MODEL.deleteInventory_begin_month(inventory_begin_monthID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: API Import inventory
             * Date: 06/01/2022
             * Dev: MinhVH
             */
             [CF_ROUTINGS_INVENTORY_BEGIN_MONTH.API_IMPORT_INVENTORY]: {
                config: {
                    scopes: ['create:inventory_begin_month'],
                    type: 'json',
                },
                methods: {
                    post: [ uploadSingle, async function(req, res) {
                        const response = await INVENTORY_BEGIN_MONTH_MODEL.importInventory(req.file);
                        res.json(response);
                    }]
                },
            },

        }
    }
};