const INVENTORY_BEGIN_MONTH_COLL = require('./databases/inventory_begin_month-coll');
const INVENTORY_BEGIN_MONTH_MODEL = require('./models/inventory_begin_month').MODEL;
const INVENTORY_BEGIN_MONTH_ROUTES = require('./apis/inventory_begin_month');
// MARK REQUIRE

module.exports = {
    INVENTORY_BEGIN_MONTH_COLL,
    INVENTORY_BEGIN_MONTH_MODEL,
    INVENTORY_BEGIN_MONTH_ROUTES,
    // MARK EXPORT
}