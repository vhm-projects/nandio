"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                        = require('moment')
/**
 * INTERNAL PACKAGES
 */

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { checkObjectIDs, isTrue }    = require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const PARCEL_COLL  				= require('../databases/parcel-coll');
const SKU_2_COLL  				= require('../databases/sku_2-coll');
const PRODUCT_QR_COLL  			= require('../../product_qr/databases/product_qr-coll');


class Model extends BaseModel {
    constructor() {
        super(PARCEL_COLL);
    }

    /**
     * FUNCTION: INSERT 
     * AUTHOR: SONLP
     * DATE: 03/07/2021
     */
    
	insert({ 
        quantities_parcel,
        point,
        sku_2,
        manufactureDate,
        expiryDate, 
    }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK TÊN CÓ TỒN TẠI
                 */
                if(!sku_2) {
                    return resolve({ error: true, message: 'Mời bạn chọn SKU 2' });
                }

                if(!quantities_parcel) {
                    return resolve({ error: true, message: 'Mời bạn nhập số lô' });
                }
                // if (quantities_parcel) {
                //     if (Number.isNaN(Number(quantities_parcel)) || Number(quantities_parcel) < 0 ) {
                //         return resolve({ error: true, message: 'Số lô không hợp lệ' });
                //     }
                // }

                if(!point) {
                    return resolve({ error: true, message: 'Mời bạn nhập điểm' });
                }
                if (point) {
                    if (Number.isNaN(Number(point)) || Number(point) < 0 ) {
                        return resolve({ error: true, message: 'Số điểm không hợp lệ' });
                    }
                }

                if(!manufactureDate) {
                    return resolve({ error: true, message: 'Mời bạn nhập ngày sản xuất' });
                }

                if(!expiryDate) {
                    return resolve({ error: true, message: 'Mời bạn nhập hạn sử dụng' });
                }

                let dataInsert = {
                    sku_2, quantities_parcel, point, manufactureDate, expiryDate
                }
               
                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'create_parcel_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: LIST SEGEMNT
     * AUTHOR: SONLP
     * DATE: 03/07/2021
     */
	getList({ 
        brand, isGood, sku_2, fromDay, toDay,
        limit,
        keyword,
        page,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {};
                let skip = (page - 1) * limit;

                if(keyword){
                    let condition = {};
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    condition.$or = [
                        { name: new RegExp(key, 'i') },
                        { variant: new RegExp(key, 'i') },
                    ];

                    let listSKU2 = await SKU_2_COLL.find({ ...condition })
                    conditionObj = {
                        ...conditionObj,
                        sku_2: {
                            $in: [...listSKU2.map(item => item._id.toString())]
                        }
                    }
                    
                    conditionObj.$or = [
                        { quantities_parcel: new RegExp(key, 'i') },
                    ];
                }
               
                if (brand) {
                    if (!checkObjectIDs(brand)) {
                        return resolve({ error: true, message: 'id_invalid' });
                    }
                    let listSKU2 = await SKU_2_COLL.find({ brand });
                    let listSKU2ID = listSKU2.map(item => item._id.toString());
                    
                    if (conditionObj.sku_2 && conditionObj.sku_2.$in && conditionObj.sku_2.$in.length) {
                        if (conditionObj.sku_2.$in.length) {
                            let listSKU = [];
                            conditionObj.sku_2.$in.map(item => {
                                if (listSKU2ID.includes(item.toString())) {
                                    listSKU = [
                                        ...listSKU,
                                        item
                                    ]
                                }
                            });
                          
                            conditionObj = {
                                ...conditionObj,
                                sku_2: {
                                    $in: listSKU
                                }
                            }
                        }   
                    } else {
                        conditionObj = {
                            ...conditionObj,
                            sku_2: {
                                $in: listSKU2ID
                            }
                        }
                    }
                }
               
                if (isGood) {
                    if (Number.isNaN(Number(isGood)) || ![1,2].includes(Number(isGood))) {
                        return resolve({ error: true, message: 'Loại hàng không hợp lệ' });
                    }
                    const IS_GOOD_BUY = 1;
                    let infoSKU2 = [];
                    if (isGood == IS_GOOD_BUY) {
                        infoSKU2 = await SKU_2_COLL.find({ isGoodBuy: true })
                    } else {
                        infoSKU2 = await SKU_2_COLL.find({ isGoodReward: true })
                    }
                   
                    let listSKU2ID = infoSKU2.map(item => item._id.toString());
                    if (conditionObj.sku_2 && conditionObj.sku_2.$in && conditionObj.sku_2.$in.length) {
                        if (conditionObj.sku_2.$in.length) {
                            let listSKU = [];
                            conditionObj.sku_2.$in.map(item => {
                                
                                if (listSKU2ID.includes(item.toString())) {
                                    listSKU = [
                                        ...listSKU,
                                        item
                                    ]
                                }
                            });
                           
                            conditionObj = {
                                ...conditionObj,
                                sku_2: {
                                    $in: listSKU
                                }
                            }
                        }   
                    } else {
                        conditionObj = {
                            ...conditionObj,
                            sku_2: {
                                $in: listSKU2ID
                            }
                        }
                    }
                }
                
                if (sku_2) {
                    if (!checkObjectIDs(sku_2)) {
                        return resolve({ error: true, message: 'id_invalid' });
                    }
                    if (conditionObj.sku_2 && conditionObj.sku_2.$in) {
                        if (conditionObj.sku_2.$in.length) {
                            let listSKU = [];
                            conditionObj.sku_2.$in.map(item => {
                                if (item.toString() == sku_2.toString()) {
                                    listSKU = [
                                        ...listSKU,
                                        item
                                    ]
                                }
                            });
                            
                            conditionObj = {
                                ...conditionObj,
                                sku_2: {
                                    $in: listSKU
                                }
                            }
                        }
                    } else {
                        conditionObj = {
                            ...conditionObj,
                            sku_2
                        }
                    }
                }

                if (fromDay) {
                    let _fromDate   = moment(fromDay).startOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate)
                    }
                }

                if (toDay) {
                    let _toDate     = moment(toDay).endOf('day').format();
                    conditionObj.createAt = {
                        ...conditionObj.createAt,
                        $lte: new Date(_toDate)
                    }
                }

                let count = await PARCEL_COLL.count({ ...conditionObj });
                let listData = await PARCEL_COLL.find({ ...conditionObj })
                    .limit(limit * 1)
                    .skip(skip)
                    .populate({
                        path: 'sku_2',
                        populate: {
                            path: 'brand',
                            select: 'name'
                        }
                        // select: 'name'
                    })
                    .sort({ modifyAt: -1 });
                
                if(!listData)
                    return resolve({ error: true, message: 'get_list_failed' });

                let arrayData = [];
                if (listData && listData.length) {
                    let index = 0;
                    for (let item of listData) {
                        let indexChange       = skip + index + 1;
                        let nameSKU2  = '';
                        let variantSKU2       = '';
                        let brandNameSKU2     = '';
                        let isGoodBuySKU2     = '';
                        let isGoodRewardSKU2  = '';

                        if (item.sku_2) {
                            nameSKU2 = `
                                <a href="/parcel/update-parcel/${item._id}">${item.sku_2.name}</a>
                            `;
                            variantSKU2       = item.sku_2.variant;
                            brandNameSKU2     = item.sku_2.brand.name;

                            let badgeIsGoodBuy = 'badge-danger';
                            // let badgeIsGoodBuyText = 'Hàng bán';
                            if (!item.sku_2.isGoodBuy) {
                                badgeIsGoodBuy = 'badge-light';
                            } 

                            isGoodBuySKU2 = `
                                <span class="badge badge-pill ${badgeIsGoodBuy}" style="padding: 5px; font-size: 10px;">
                                    Hàng bán
                                </span>
                            `;

                            let badgeIsGoodReward = 'badge-success';
                            if (!item.sku_2.isGoodReward) {
                                badgeIsGoodReward = 'badge-light';
                            } 

                            isGoodRewardSKU2  = `
                                <span class="badge badge-pill ${badgeIsGoodReward}" style="padding: 5px; font-size: 10px;">
                                    Hàng tặng
                                </span>
                            `;
                        }

                        let quantities_parcel = item.quantities_parcel;
                        let point             =  `
                            <input type="number" id="classParcel" _parcelID ='${item._id}' class="form-control" value="${item && item.point || 0}" required="" placeholder="VD: 1">
                        `;
                        let manufactureDate   = moment(item.manufactureDate).format('L');
                        let expiryDate        = moment(item.expiryDate).format('L');
                        let createAt          = moment(item.createAt).format('LT') + " " + moment(item.createAt).format('L');

                        let countProductQR = await PRODUCT_QR_COLL.count({ parcel: item._id })
                        
                        let action = `
                            <div class="btn-group mb-2">
                                <button type="button" class="btn btn-secondary btn-sm waves-effect waves-light">
                                    Thao Tác
                                </button>
                                <button type="button" class="btn btn-secondary btn-sm waves-effect waves-light dropdown-toggle-split dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">...</span>
                                </button>
                                <div class="dropdown-menu" x-placement="right-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(143px, 0px, 0px);">
                                    <a class="dropdown-item" href="/parcel/update-parcel/${item._id}">
                                        <i class="fa fa-edit"></i>
                                        Sửa Thông Tin
                                    </a>
                                </div>
                            </div>
                        `;

                        let obj = {
                            index: indexChange,
                            nameSKU2,
                            variantSKU2,
                            brandNameSKU2,
                            isGoodBuySKU2,
                            isGoodRewardSKU2,
                            quantities_parcel,
                            point,
                            countQr: countProductQR,
                            manufactureDate,
                            expiryDate,
                            createAt,
                            action
                        };

                        arrayData = [
                            ...arrayData,
                            {
                                ...obj
                            }
                        ];

                        index ++;
                    }
                }

                return resolve({ error: false, data: arrayData, recordsTotal: count, recordsFiltered: count });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ parcelID }) {
        return new Promise(async resolve => {
            try {
                let infoData = await PARCEL_COLL.findById(parcelID)
                
                if(!infoData)
                    return resolve({ error: true, message: 'get_info_failed' });

                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: UPDATE SEGEMNT
     * AUTHOR: SONLP
     * DATE: 03/07/2021
     */
	update({ 
        parcelID,
        quantities_parcel,
        point,
        sku_2,
        manufactureDate,
        expiryDate,
    }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK TÊN CÓ TỒN TẠI
                 */
                if (!checkObjectIDs(parcelID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }
                /**
                 * CHECK TÊN CÓ TỒN TẠI
                 */
                 if(!sku_2) {
                    return resolve({ error: true, message: 'Mời bạn chọn SKU 2' });
                }

                if(!quantities_parcel) {
                    return resolve({ error: true, message: 'Mời bạn nhập số lô' });
                }
                // if (quantities_parcel) {
                //     if (Number.isNaN(Number(quantities_parcel)) || Number(quantities_parcel) < 0 ) {
                //         return resolve({ error: true, message: 'Số lô không hợp lệ' });
                //     }
                // }

                if(!point) {
                    return resolve({ error: true, message: 'Mời bạn nhập điểm' });
                }
                if (point) {
                    if (Number.isNaN(Number(point)) || Number(point) < 0 ) {
                        return resolve({ error: true, message: 'Số điểm không hợp lệ' });
                    }
                }

                if(!manufactureDate) {
                    return resolve({ error: true, message: 'Mời bạn nhập ngày sản xuất' });
                }

                if(!expiryDate) {
                    return resolve({ error: true, message: 'Mời bạn nhập hạn sử dụng' });
                }


                let dataUpdate = {
                    sku_2, quantities_parcel, point, manufactureDate, expiryDate,
                    modifyAt: new Date()
                }
               
                let infoAfterUpdate = await PARCEL_COLL.findByIdAndUpdate(parcelID, {...dataUpdate}, { new: true });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_failed' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateDynamic({ 
        parcelID,
        point,
    }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK TÊN CÓ TỒN TẠI zxc
                 */
                if (!checkObjectIDs(parcelID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }
                let dataUpdate = { modifyAt: new Date() }
                if (point) {
                    if (Number.isNaN(Number(point)) || Number(point) < 0) {
                        return resolve({ error: true, message: 'Điểm không đúng định dạng' });
                    }
                    dataUpdate = {
                        ...dataUpdate,
                        point
                    }
                }
               
                let infoAfterUpdate = await PARCEL_COLL.findByIdAndUpdate(parcelID, {
                    ...dataUpdate
                }, { 
                    new: true 
                }); 

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_failed' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    syncData({ 
        data
    }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK TÊN CÓ TỒN TẠI 
                 */
                if (!data || !data.length) {
                    return resolve({ error: true, message: 'Dữ liệu không hợp lệ' });
                }

                for (let item of data) {
                    let checkExist = await PARCEL_COLL.findOne({ quantities_parcel: item.lottable01 });
                    
                    if (!checkExist) {
                        let infoSku2 = await SKU_2_COLL.findOne({ name: item.sku });
                        
                        if (!infoSku2) {
                            return resolve({ error: true, message: "SKU không tồn tại" });
                        }
                        let infoAfterInsert = await PARCEL_COLL.create({
                            quantities_parcel: item.lottable01, // số lô
                            sku_2: infoSku2._id, // SKU 2
                            manufactureDate: item.lottable04, // Ngày sản xuất
                            expiryDate: item.lottable05, //  Hạn sử dụng
                            createAt: new Date(),
                            modifyAt: new Date(),
                        });
                        
                        if (!infoAfterInsert) {
                            return resolve({ error: true, message: "Không thể tạo Lô" });
                        }

                        let listDataQr = item.unitid.map(unitid => {
                            return {
                                parcel: infoAfterInsert._id,
                                code: unitid,
                                sku_2: infoSku2._id,
                            }
                        });

                        let listQrAfterInsert = await PRODUCT_QR_COLL.insertMany(listDataQr);
                        
                        if (!listQrAfterInsert) {
                            return resolve({ error: true, message: "Không thể tạo QR" });
                        }
                        // listData = [
                        //     ...listData,
                        //     {
                        //         quantities_parcel: item.lottable01, // số lô
                        //         sku_2: infoSku2._id, // SKU 2
                        //         manufactureDate: item.lottable04, // Ngày sản xuất
                        //         expiryDate: item.lottable05, //  Hạn sử dụng
                        //         createAt: new Date(),
                        //         modifyAt: new Date(),
                        //     }
                        // ]
                    }
                }
                
                // let resultAfterInsert = await PARCEL_COLL.insertMany(listData);
                // if (!resultAfterInsert) {
                //     return resolve({ error: true, message: 'cannot_inserted' });
                // }

                return resolve({ error: false, data: 'Done' });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    syncDataSku1({ 
        data
    }) {
        return new Promise(async resolve => {
            try {
                if (!data || !data.length) {
                    return resolve({ error: true, message: 'Dữ liệu không hợp lệ' });
                }

                for (let item of data) {
                    if (item.sku && item.sku.indexOf('1') == 0) { // SKU đầu 1
                        let checkExist = await PARCEL_COLL.findOne({ quantities_parcel: item.lottable01 });
                        if (!checkExist) {
                            let infoAfterInsert = await PARCEL_COLL.create({
                                quantities_parcel: item.lottable01, // số lô
                                manufactureDate: item.lottable04, // Ngày sản xuất
                                expiryDate: item.lottable05, //  Hạn sử dụng
                                createAt: new Date(),
                                modifyAt: new Date(),
                            });
                            
                            if (!infoAfterInsert) {
                                return resolve({ error: true, message: "Không thể tạo Lô" });
                            }

                            let listDataQr = item.unitid.map(unitid => {
                                return {
                                    parcel: infoAfterInsert._id,
                                    code: unitid,
                                    sku_1: item.sku, // SKU 1
                                }
                            });

                            let listQrAfterInsert = await PRODUCT_QR_COLL.insertMany(listDataQr);
                            
                            if (!listQrAfterInsert) {
                                return resolve({ error: true, message: "Không thể tạo QR" });
                            }
                        }
                    } 
                }
                
                return resolve({ error: false, data: 'Done' });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
