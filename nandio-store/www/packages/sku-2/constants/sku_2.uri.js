const BASE_ROUTE = '/sku-2';

const CF_ROUTINGS_SKU_2 = {
    // USER PERMISSION
	ADD_SKU_2:             `${BASE_ROUTE}/add-sku-2`,
	LIST_SKU_2:            `${BASE_ROUTE}/list-sku-2`,
	INFO_SKU_2:            `${BASE_ROUTE}/info-sku-2/:sku2ID`,
    UPDATE_SKU_2:          `${BASE_ROUTE}/update-sku-2/:sku2ID`,
    UPDATE_DYNAMIC_SKU_2:  `${BASE_ROUTE}/update-dynamic`,


    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_SKU_2 = CF_ROUTINGS_SKU_2;
