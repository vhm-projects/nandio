const BASE_ROUTE = '/parcel';

const CF_ROUTINGS_PARCEL = {
    // USER PERMISSION
	ADD_PARCEL:             `${BASE_ROUTE}/add-parcel`,
	LIST_PARCEL:            `${BASE_ROUTE}/list-parcel`,
	LIST_PARCEL_PAGINATION:            `${BASE_ROUTE}/list-parcel-pagination`,
    UPDATE_PARCEL:          `${BASE_ROUTE}/update-parcel/:parcelID`,
    UPDATE_DYNAMIC_PARCEL:  `${BASE_ROUTE}/update-dynamic`,
    SYNC_PARCEL:  `${BASE_ROUTE}/sync-parcel`,


    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_PARCEL = CF_ROUTINGS_PARCEL;
