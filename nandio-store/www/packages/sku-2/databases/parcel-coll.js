"use strict";
const Schema        = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION PRODUCT CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('parcel', {
	
	/**
	 * SKU 2
	 */
	sku_2: {
		type: Schema.Types.ObjectId,
		ref: 'sku_2',
	},
	/**
	 * số lô
	 */
	quantities_parcel: {
		type: String,
	}, 
	/**
	 * Điểm
	 */
	point: {
		type: Number,
		default: 0,
	},
	/**
	 * Ngày sản xuất
	 */
	manufactureDate: {
		type: Date,
	},
	/**
	 * Hạn sử dụng
	 */
	expiryDate: {
		type: Date,
	},

	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},
});
