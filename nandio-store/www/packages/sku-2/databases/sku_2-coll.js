"use strict";
const Schema        = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION PRODUCT CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('sku_2', {
	name: {
		type: String,
	}, 
	variant: {
		type: String,
	},
	/**
	 * Thương hiệu
	 */
	brand: {
		type: Schema.Types.ObjectId,
		ref: 'brand',
	},
	/**
	 * Hàng bán
	 */
	isGoodBuy: {
		type: Boolean,
		default: false
	},

	/**
	 * Hàng tặng
	 */
	 isGoodReward: {
		type: Boolean,
		default: false
	},

	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},
});
