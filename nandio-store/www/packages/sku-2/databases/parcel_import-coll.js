"use strict";
const Schema        = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION PRODUCT CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('parcel_import', {
	
	/**
	 * SKU 2
	 */
	
	SKU: {
		type: String,
	}, 
	/**
	 * MÃ QR
	 */
	
	UNITID: {
		type: String,
	}, 
	/**
	 * NGÀY TẠO
	 */
	ADDDATE: {
		type: String,
	}, 
	
	ADDWHO: {
		type: String,
	}, 
	/**
	 * SỐ LÔ
	 */
	COMMAND: {
		type: String,
	}, 
	/**
	 * NSX
	 */
	LOTTABLE04: {
		type: String,
	}, 
	/**
	 * HSD
	 */
	LOTTABLE05: {
		type: String,
	}, 
});
