"use strict";

/**
 * EXTERNAL PAKCAGE
 */
let fs                                    = require("fs");
let path                                  = require('path');
const moment                              = require('moment');

const ObjectID                            = require('mongoose').Types.ObjectId;
const { randomNumbers } 	              = require('../../../utils/string_utils');
const { checkObjectIDs } 	              = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel                           = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const PRODUCT_QR_COLL                         = require('../databases/product_qr-coll');
const { PRODUCT_MODEL }                       = require('../../product');
const { HISTORY_POINT_COLL, HISTORY_POINT_MODEL } = require('../../history_point');
// const { EMPLOYEE_MODEL }                      = require('../../employee');
const { SALEOUT_COLL }                        = require('../../history');
const { CUSTOMER_COLL }                       = require('../../customer');

class Model extends BaseModel {
    constructor() {
        super(PRODUCT_QR_COLL);
        this.STATUS_AFTER_SCAN = 1
    }

    accumlateHistoryPointByCode({ code, customerID, employeeID, deviceID, deviceName, deviceType, lat, lng }) {
        return new Promise(async (resolve) => {
            try {
                //=============================VALIDATION=============================
                if (!code) 
                    return resolve({ error: true, message: 'Mời bạn nhập mã', status: 400 });
                if (!checkObjectIDs(customerID)) 
                    return resolve({ error: true, message: 'ID Khách hàng không hợp lệ', status: 400 });
                if (!checkObjectIDs(employeeID)) 
                    return resolve({ error: true, message: 'ID Nhân viên không hợp lệ', status: 400 });
                /**
                 *  (*)REQUIRE: KIỂM TRA CÁC THUỘC TÍNH BUỘC PHẢI CÓ
                 */
                if (!deviceID || !deviceName || !deviceType || !lat || !lng) {
                    return resolve({ error: true, message: 'Bạn phải gửi đầy đủ thông tin về ID, Tên Thiết Bị, Loại Thiết Bị, Lat và Lng', status: 400 });
                }

                if (!["ANDROID", "IOS"].includes(deviceType))
                    return resolve({ error: true, message: 'Loại Thiết Bị không hợp lệ', status: 400 });

                let checkExist__Customer = await CUSTOMER_COLL.findById(customerID);
                if (!checkExist__Customer) {
                    return resolve({ error: true, message: '001. Khách hàng không tồn tại', status: 400 });
                }

                /**
                 * ===================================================================
                 * ==========================KHAI BÁO BIẾN============================
                 * ===================================================================
                 */
                let conditionObj__SaleOut = { // OBJECT TẠO SALE OUT
                    customer: customerID,
                    createAt: new Date(),
                    modifyAt: new Date(),
                };

                /**
                 * ===================================================================
                 *  1. KIỂM TRA EMPLOYEE CÓ ĐANG CHECKIN HAY KHÔNG => LẤY RA THỒNG TIN CHECKIN VÀ EMPLOYEE
                 * ===================================================================
                 */
                let info__CheckInVsEmployee__ByEmployeeID = await EMPLOYEE_MODEL.getInfo__CheckIn_Vs_Employee__ByEmployeeID({ employeeID });
                if (info__CheckInVsEmployee__ByEmployeeID.error) {
                    return resolve(info__CheckInVsEmployee__ByEmployeeID);
                }

                let { data: { infoCheckInOut, infoEmployee } } = info__CheckInVsEmployee__ByEmployeeID;
             
                conditionObj__SaleOut = {
                    ...conditionObj__SaleOut,
                    deviceID, deviceName, deviceType, lat, lng,
                    checkin:     infoCheckInOut._id,
                    store:       infoCheckInOut.store,
                    employee:    infoEmployee._id,
                    region:      infoEmployee.region,
                    area:        infoEmployee.area,
                    distributor: infoEmployee.distributor,
                }

                /**
                 * ===================================================================
                 *  2. LẤY THÔNG TIN LÔ
                 * ===================================================================
                 */
                let infoData = await PRODUCT_QR_COLL.findOne({ 
                    code
                }).populate({
                    path: 'parcel',
                    select: 'manufactureDate expiryDate point status'
                });

                //=============================VALIDATION=============================
                if(!infoData)
                    return resolve({ error: true, message: `002. Không tồn tại mã: ${code}`, status: 400 });
                if (infoData.sku_1)
                    return resolve({ error: true, message: `003. Mã không được tích điểm: ${code}`, status: 400 });
                if (infoData.customer)
                    return resolve({ error: true, message: `004. Sản phẩm đã được tích điểm trước đó: ${code}`, status: 400 });
                
                /**
                 * ===================================================================
                 *  3. LẤY THÔNG TIN SẢN PHẨM TỪ SKU 2
                 * ===================================================================
                 */

                let infoProductSku2 = await PRODUCT_MODEL.getInfoProductOfSku2({ sku_2: infoData.sku_2 });
                if (infoProductSku2.error) {
                    return resolve({
                        error: true,
                        message: `005: Mã đầu 2 này chưa được thiết lập hiển thị: ${code}`
                    });
                }

                conditionObj__SaleOut = {
                    ...conditionObj__SaleOut,
                    product: infoProductSku2.data._id,
                    price:   infoProductSku2.data.price
                }
                
                /**
                 * ===================================================================
                 *  4. LẤY DANH SÁCH BIG SALE VS FLASH SALE
                 * ===================================================================
                 */
                let listBigsaleAndFlashsale = await PRODUCT_MODEL.getListBigSaleAndInfoFlashSale();
                let { listBigSale, infoFlashSale } = listBigsaleAndFlashsale.data;
                let data = {
                    infoProduct: infoProductSku2.data, 
                    listBigSale, infoFlashSale, 
                    infoParcel: infoData.parcel, 
                    infoProductQR: infoData
                }

                const point     = infoData.parcel.point ? infoData.parcel.point : 0; // POINT TỪ LÔ
                const TICH_DIEM = 1;
                const TRANSFER_POINT = 5;

                let infoHistoryPointAfterInsert = await HISTORY_POINT_MODEL.createHistoryPoint__NandioShop({ 
                    productID:       infoProductSku2.data._id,
                    customerID:      customerID, 
                    typeNandioAdmin: TRANSFER_POINT, 
                    type:            TICH_DIEM, 
                    currentPoint:    point,
                })

                // if (infoHistoryPointAfterInsert.error) {
                //     return resolve(infoHistoryPointAfterInsert);
                // }
                 /**
                 * ===================================================================
                 *  5. UPDATE LẠI MÃ QR => TẮT SHOW
                 * ===================================================================
                 */
                const STATUS_SHOWED_QR = 1;
                await PRODUCT_QR_COLL.findByIdAndUpdate(infoData._id, {
                    status: STATUS_SHOWED_QR,
                    customer: customerID
                });

                /**
                 * ===================================================================
                 *  6. LẤY LAST RECORD HISTORY POINT
                 * ===================================================================
                 */

                conditionObj__SaleOut = {
                    ...conditionObj__SaleOut,
                    point: point,
                }

                // let infoLastRecordHistoryPoint = await HISTORY_POINT_COLL.findOne({ customer: customerID }).sort({ createAt: -1 });
                let pointCustomer = checkExist__Customer.point ? checkExist__Customer.point : 0;
                let conditionObjHistoryPoint = {
                    beforPoint: 0,
                    type: TICH_DIEM,
                    product: infoProductSku2.data._id,
                    customer: customerID,
                    createAt: new Date(),
                    modifyAt: new Date(),
                    beforPoint:   pointCustomer,
                    currentPoint: point,
                    point:        pointCustomer + point,
                    flatform:     'STORE',
                }
               
                // if (infoLastRecordHistoryPoint) {
                //      /**
                //      *  6.1. NẾU RECORD HISTORY POINT TÔN TẠI
                //      */
                //     conditionObjHistoryPoint = {
                //         ...conditionObjHistoryPoint,
                //         beforPoint: checkExist__Customer.point
                //     }
                // }
                /**
                 *  6.2. TÍCH ĐIỂM VỚI POINT CỦA LÔ
                 */
                // conditionObjHistoryPoint = {
                //     ...conditionObjHistoryPoint,
                //     beforPoint:   checkExist__Customer.point,
                //     currentPoint: point,
                //     point:        checkExist__Customer.point + point,
                //     flatform:     'STORE',
                // }
                
                /**
                 * ===================================================================
                 *  7. TẠO HISTORY POINT
                 * ===================================================================
                 */
                // let infoHistoryPointAfterInsert = await HISTORY_POINT_COLL.create(conditionObjHistoryPoint);
                // if (!infoHistoryPointAfterInsert) {
                //     return resolve({ error: true, message: 'Không thể tạo lịch sử điểm', status: 400 });
                // }

                /**
                 * ===================================================================
                 *  8. TẠO SALE OUT
                 * ===================================================================
                 */
                let infoSaleOutAfterInsert = await SALEOUT_COLL.create(conditionObj__SaleOut);
                // if (!infoSaleOutAfterInsert) {
                //     return resolve({ error: true, message: 'Không thể Sale Out', status: 400 });
                // }

                /**
                 * ===================================================================
                 *  9. UPDATE LẠI POINT CỦA KHÁCH HÀNG
                 * ===================================================================
                 */
                // let pointCustomerRanking = checkExist__Customer.pointRanking ? checkExist__Customer.pointRanking : 0;
                // console.log({
                //     pointRanking: pointCustomerRanking + point, // CỘNG VÀO POINT TỔNG
                //     point: pointCustomer + point, // CỘNG VÀO POINT SỬ DỤNG
                // });
                // let infoEmployeeAfterUpdatePoint = await CUSTOMER_COLL.findByIdAndUpdate(customerID, {
                //     pointRanking: pointCustomerRanking + point, // CỘNG VÀO POINT TỔNG
                //     point: pointCustomer + point, // CỘNG VÀO POINT SỬ DỤNG
                // });
                // console.log({
                //     infoEmployeeAfterUpdatePoint
                // });
                // if (!infoEmployeeAfterUpdatePoint) {
                //     return resolve({ error: true, message: 'Không thể cập nhật điểm của Khách hàng', status: 400 });
                // }

                return resolve({ 
                    error: false, 
                    data, 
                    status: 200 
                });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoByCode_v3({ code, customerID }) {
        return new Promise(async (resolve) => {
            try {
                let infoData = await PRODUCT_QR_COLL.findOne({ 
                    code, 
                    // customer: {
                    //     $exists: false
                    // }
                }).populate({
                    path: 'parcel',
                    select: 'manufactureDate expiryDate point'
                });
                if(!infoData)
                    return resolve({ error: true, message: 'Không tồn tại mã' });
              
                // if (infoData.customer)
                //     return resolve({ error: true, message: 'Sản phẩm đã được tích điểm trước đó' });
                let data = {};
                if (infoData.sku_2) { // SKU ĐẦU 2
                    let infoProductSku2 = await PRODUCT_MODEL.getInfoProductOfSku2({ sku_2: infoData.sku_2 });
                    if (infoProductSku2.error) {
                        return resolve(infoProductSku2);
                    }
                    data = {
                        infoProduct: infoProductSku2.data, 
                    }
                } else { // SKU ĐẦU 1
                    let infoProductSku1 = await PRODUCT_MODEL.getInfoProductOfSku1({ sku_1: infoData.sku_1 });
                    if (infoProductSku1.error) {
                        return resolve(infoProductSku1);
                    }
                    data = {
                        infoProduct: infoProductSku1.data, 
                    }
                }

                // Lấy danh sách bigsale và flashsale ádasd
                let listBigsaleAndFlashsale = await PRODUCT_MODEL.getListBigSaleAndInfoFlashSale();
                let { listBigSale, infoFlashSale } = listBigsaleAndFlashsale.data;
                data = {
                    ...data,
                    listBigSale, infoFlashSale, 
                    infoParcel: infoData.parcel, 
                    infoProductQR: infoData
                }

                return resolve({ error: false, data });
                
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListHistoryPointOfCustomer__ApiStore({ customerID, type, page = 1, limit = 3 }) {
        return new Promise(async resolve => {
            try {
				limit = isNaN(limit) ? 3 : +limit;
                page = isNaN(page) ? 1 : +page;


				if (!checkObjectIDs(customerID)) 
					return resolve({ error: true, message: "ID Khách hàng không hợp lệ" });

                let conditionObj = { customer: customerID };

                if (type) {
                    if (Number.isNaN(Number(type)) || ![1, 2].includes(Number(type))) {
					    return resolve({ error: true, message: "Loại lịch sử không hợp lệ" });
                    }
                    conditionObj.type = type;
                }
                console.log({
                    conditionObj
                });

                let infoHistoryPointOfOrder = await HISTORY_POINT_COLL
                    .find(conditionObj)
                    .limit(+limit)
                    .skip((page - 1) * limit)
                    .sort({ modifyAt: -1 })
                    .select("product customer beforPoint currentPoint point type createAt")
                    .populate({
                        path: 'product',
                        select: 'SKU name'
                    })
                    .lean();

                if(!infoHistoryPointOfOrder)
                    return resolve({ error: true, message: "Không thể lấy danh sách lịch sử điểm" }); 

				return resolve({ 
					error: false, 
					data: infoHistoryPointOfOrder
				});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    //Admin lấy tất cả  mẫu tin
    getListByAdmin({ product, status, customer }){  
        return new Promise(async (resolve) => {
            try {
                let dataSearch = {}
                if(ObjectID.isValid(product)){
                    dataSearch.product = product;
                }

                if(ObjectID.isValid(customer)){
                    dataSearch.customer = customer;
                }

                if(status){
                    dataSearch.status = status;
                }

                let listData = await PRODUCT_QR_COLL.find(dataSearch)
                .populate({
                    path : 'sku_2 customer'
                })
                if(!listData)
                    return resolve({ error: true, message: 'cant_update' });
                return resolve({ error: false, data: listData});
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByPagination({ product, status, customer, 
        limit,
        keyword,
        page, 
    }){  
        return new Promise(async (resolve) => {
            try {
                let dataSearch = {};
                let skip = (page - 1) * limit;

                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    dataSearch.$or = [
                        { code: new RegExp(key, 'i') },
                    ];

                    // conditionObj.$or = [
                    //     { quantities_parcel: new RegExp(key, 'i') },
                    // ];
                }

                if(ObjectID.isValid(product)){
                    dataSearch.product = product;
                }

                if(ObjectID.isValid(customer)){
                    dataSearch.customer = customer;
                }

                if(status){
                    dataSearch.status = status;
                }
               
                let count = await PRODUCT_QR_COLL.count(dataSearch);
                let listData = await PRODUCT_QR_COLL.find(dataSearch)
                    .limit(limit * 1)
                    .skip(skip)
                    .populate({
                        path : 'sku_2 customer parcel'
                    })
                    
                if(!listData)
                    return resolve({ error: true, message: 'cant_update' });
               
                let arrayProductQR = [];
                listData && listData.length  && listData.map((item, index) => {
               
                    let indexChange       = skip + index + 1;
                    let code  = item.code && item.code || '';
                    let sku_2 = item.sku_2 && item.sku_2.name || '';
                    let sku_1 = item.sku_1 && item.sku_1 || '';
                    let parcel = item.parcel && item.parcel.quantities_parcel || '';
                    let customer = item.customer && item.customer.fullname || '';
                    
                    let badge = 'badge-success';
                    let nameStatus = 'Chưa quét'
                    if (item.status) {
                        badge = 'badge-danger';
                        nameStatus = 'Đã quét';
                    }
                    let status = `
                        <span class="badge badge-pill ${badge}" style="padding: 5px; font-size: 10px;">
                            ${nameStatus}
                        </span>
                    `;
                    arrayProductQR = [
                        ...arrayProductQR,
                        {
                            index: indexChange,
                            code,
                            sku_2,
                            sku_1,
                            parcel,
                            customer,
                            status,
                        }
                    ]
                });
              
                return resolve({ error: false, data: arrayProductQR, recordsTotal: count, recordsFiltered: count });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
/**
 * MODELS
 */
 var { MODEL: EMPLOYEE_MODEL }			= require('../../employee/models/employee');