"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');
module.exports = BASE_COLL("product_qr", {
        // Mã code
        code: {
            type: String,
            unique: true
        },
        
        // product: {
        //     type:  Schema.Types.ObjectId,
        //     ref : 'product'
        // },

        customer: {
            type:  Schema.Types.ObjectId,
            ref : 'customer'
        },

        sku_2: {
            type:  Schema.Types.ObjectId,
            ref : 'sku_2'
        },

        sku_1: {
            type:  String
        },

        parcel: {
            type:  Schema.Types.ObjectId,
            ref : 'parcel'
        },
        /**
         * Trạng thái quét
         * 0 Chưa quét
         * 1 Đã quét
         */
        status: {
            type: Number,
            default: 0,
            emun: [0, 1],
        },
        /**
         * Ngày sản xuất
         */
        // manufactureDate: Date,
        /**
         * Ngày hết hạn
         */
        // expiredTime: Date,
    });
