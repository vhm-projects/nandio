"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path						                    = require('path');
const { request }                                   = require('http');
const TOKEN_STORE                                   = process.env.TOKEN_STORE;
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_PRODUCT_QR } 						= require('../constants/product_qr.uri');
/**
 * MODELS
 */
const PRODUCT_QR_MODEL                                   = require('../models/product_qr').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }
    registerRouting() {
        return {


            [CF_ROUTINGS_PRODUCT_QR.LIST_PRODUCT_QR_PAGINATION]: {
                config: {
                    scopes: ['read:list_product_qr'],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        // let { product, status, customer } = req.query;
                        let { start, length, search } = req.body;
                        let page = Number(start)/Number(length) + 1;

                        let listProductQR = await PRODUCT_QR_MODEL.getListByPagination({  
                            page: Number(page), 
                            limit: Number(length), 
                            keyword: search.value  
                        });
                        res.json(listProductQR);
                    }]
                },
            },

            /**
             * Function: TÍch điểm Qr product
             * Date: 01/04/2021
             */
            [CF_ROUTINGS_PRODUCT_QR.ACCUMULATE_PRODUCT_QR]: {
                config: {
                    scopes: ['create:product_qr'],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const {
                            customerID, code, employeeID, deviceID, deviceName, deviceType, lat, lng
                        } = req.query;

                        const infoData = await PRODUCT_QR_MODEL.accumlateHistoryPointByCode({
                            code, customerID, employeeID, deviceID, deviceName, deviceType, lat, lng
                        });
                        res.json(infoData);
                    }]
                },
            },

             /**
             * Function: Thông tin Qr product
             * Date: 01/04/2021
             */
            [CF_ROUTINGS_PRODUCT_QR.DETAIL_PRODUCT_QR]: {
                config: {
                    scopes: ['read:info_product_qr'],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let  { customerID, code } =  req.query;

                        let infoData  = await PRODUCT_QR_MODEL.getInfoByCode_v3({ code, customerID });
                        res.json(infoData);
                     }]
                },
            },

            /**
             * Function: Danh sách đơn hàng (API) => GỌI TỪ NANDIO STORE
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRODUCT_QR.API_LIST_HISTORY_POINT]: {
                config: {
                    scopes: ['read:list_product_qr'],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
                        const { customerID }  = req.params
						const { type, page, limit } = req.query;

						const listHistory = await PRODUCT_QR_MODEL.getListHistoryPointOfCustomer__ApiStore({ customerID, type, page, limit });
						res.json(listHistory);
					}]
                },
            },
            
        }
    }
};
