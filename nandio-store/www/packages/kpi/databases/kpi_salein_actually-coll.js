"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('kpi_salein_actually', {

    /**
     * Vùng
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Miền
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * Nhà Phân Phối
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    /**
     * Cửa Hàng
     */
    store: {
        type: Schema.Types.ObjectId,
        ref: 'store',
    },
    /**
     * Nhân Viên
     */
    employee: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },
    /**
     * SALE_ORDER_ID
     */
    SALE_ORDER_ID: {
        type: String,
    },
    // được bổ sung sau này, mục đích làm PRIMARYKEY cho dữ liệu
    SALE_ORDER_DETAIL_ID: {
        type: String
    },
    ORDER_NUMBER: {
        type: String,
    },
    ORDER_DATE: {
        type: Date,
    },
    /**
     * Sản Phẩm
     */
    product: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },
    QUANTITY: {
        type: Number,
    },
    IS_FREE_ITEM: {
        type: Number,
    },
});