"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('kpi_sbd_require', {

    /**
     * Sản Phẩm
     */
    product: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },
    /**
     * Tháng Thứ Áp Dụng
     */
    month_apply: {
        type: Number,
        required: true,
    },
    /**
     * Tháng
     */
    month: {
        type: Number,
        required: true,
    },
    /**
     * Năm
     */
    year: {
        type: Number,
        required: true,
    },
});