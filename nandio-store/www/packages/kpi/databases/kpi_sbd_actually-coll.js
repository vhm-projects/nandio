"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');
// hiện tại chưa dùng đến, vì SBD_ACTUALLY thì đnag dùng collection KPI SALEIN
module.exports = BASE_COLL('kpi_sbd_actually', {

    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Vùng
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * Nhà Phân Phối
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    /**
     * Nhân Viên
     */
    employee: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },
    /**
     * Thực Đạt
     */
    amount: {
        type: Number,
        required: true,
    },
    /**
     * Đã tạo thành công 
     * 1: Đã Tạo trong tháng,
     * 2: Chưa được tạo
     */
    isChecked: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
    /**
     * Tháng
     */
    month: {
        type: Number,
        required: true,
    },
    /**
     * Năm
     */
    year: {
        type: Number,
        required: true,
    },
});