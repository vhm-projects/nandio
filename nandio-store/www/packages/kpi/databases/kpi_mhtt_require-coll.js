"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('kpi_mhtt_require', {

    /**
     * region
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * area
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * NPP
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    /**
     * Chỉ Tiêu
     */
    amount: {
        type: Number,
        required: true,
    },
    /**
     * Nhân Viên
     */
    employee: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },
    /**
     * Tháng
     */
    month: {
        type: Number,
        required: true,
    },
    /**
     * Năm
     */
    year: {
        type: Number,
        required: true,
    },
});