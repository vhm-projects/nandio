/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const path = require('path');
const fs = require('fs');
const {
    hash
} = require('bcryptjs');
const XlsxPopulate = require('xlsx-populate');
const {
    MongoClient
} = require('mongodb');
const formatCurrency = require('number-format.js');

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    renderOptionFilter,
    colName,
    checkNumberIsValidWithRange
} = require('../../../utils/utils');
const {
    isTrue
} = require('../../../tools/module/check');
const {
    randomStringFixLength
} = require('../../../utils/string_utils');
const {
    compareTwoTimeWithCondition
} = require('../../../utils/time_utils');

const {
    ISCHECKED_KPI_SBD_ACTUALLY_TYPE,
} = require('../constants/kpi_sbd_actually');


/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const URL_DATABASE = process.env.URL_DATABASE || 'mongodb://localhost:27017';
const NAME_DATABASE = process.env.NAME_DATABASE || 'ldk_tools_op';

class Model extends BaseModel {
    constructor() {
        super(require('../databases/kpi_sbd_actually-coll'))
    }

    /**
         * Tạo mới kpi_sbd_actually
		* @param {object} employee
		* @param {number} amount
		* @param {number} isChecked
		* @param {number} month
		* @param {number} year

         * @param {objectId} author
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        employee,
        amount,
        isChecked = 1,
        month,
        year,
        author
    }) {
        return new Promise(async resolve => {
            try {
                if (author && !checkObjectIDs([author])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ',
                        status: 400
                    });
                }

                if (!amount) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập thực đạt',
                        status: 400
                    });
                }

                if (!month) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tháng',
                        status: 400
                    });
                }

                if (!year) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập năm',
                        status: 400
                    });
                }


                if (isChecked && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isChecked
                    })) {
                    return resolve({
                        error: true,
                        message: 'đã tạo thành công không hợp lệ',
                        status: 400
                    });
                }





                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ',
                        status: 400
                    });
                }




                let dataInsert = {
                    employee,
                    amount,
                    isChecked,
                    month,
                    year,
                    author
                };


                if (employee) {
                    const employeeInfo = await EMPLOYEE_COLL.findById(employee);
                    dataInsert.region = employeeInfo.region;
                    dataInsert.distributor = employeeInfo.distributor;
                    dataInsert.area = employeeInfo.area;
                }

                dataInsert = cleanObject(dataInsert);

                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo KPI Thực Đạt SBD (Sản Phẩm Trọng Tâm) thất bại'
                    });
                }


                return resolve({
                    error: false,
                    data: infoAfterInsert,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
         * Cập nhật kpi_sbd_actually 
         * @param {objectId} kpi_sbd_actuallyID
		* @param {object} employee
		* @param {number} amount
		* @param {number} isChecked
		* @param {number} month
		* @param {number} year

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        kpi_sbd_actuallyID,
        employee,
        amount,
        isChecked,
        month,
        year,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([kpi_sbd_actuallyID])) {
                    return resolve({
                        error: true,
                        message: 'kpi_sbd_actuallyID không hợp lệ',
                        status: 400
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ',
                        status: 400
                    });
                }

                if (!amount) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập thực đạt',
                        status: 400
                    });
                }

                if (!month) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tháng',
                        status: 400
                    });
                }

                if (!year) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập năm',
                        status: 400
                    });
                }


                if (isChecked && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isChecked
                    })) {
                    return resolve({
                        error: true,
                        message: 'đã tạo thành công không hợp lệ',
                        status: 400
                    });
                }





                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ',
                        status: 400
                    });
                }

                const checkExists = await KPI_SBD_ACTUALLY_COLL.findById(kpi_sbd_actuallyID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'KPI Thực Đạt SBD (Sản Phẩm Trọng Tâm) không tồn tại',
                        status: 403
                    });
                }



                let dataUpdate = {
                    userUpdate
                };
                employee && (dataUpdate.employee = employee);
                amount && (dataUpdate.amount = amount);
                dataUpdate.isChecked = isChecked;
                month && (dataUpdate.month = month);
                year && (dataUpdate.year = year);

                if (employee) {
                    const employeeInfo = await EMPLOYEE_COLL.findById(employee);
                    dataUpdate.region = employeeInfo.region;
                    dataUpdate.distributor = employeeInfo.distributor;
                    dataUpdate.area = employeeInfo.area;
                }

                let infoAfterUpdate = await this.findOneAndUpdate({
                    _id: kpi_sbd_actuallyID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại',
                        status: 422
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
         * Cập nhật kpi_sbd_actually (không bắt buộc)
         * @param {objectId} kpi_sbd_actuallyID
		* @param {object} employee
		* @param {number} amount
		* @param {number} isChecked
		* @param {number} month
		* @param {number} year

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        kpi_sbd_actuallyID,
        employee,
        amount,
        isChecked,
        month,
        year,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([kpi_sbd_actuallyID])) {
                    return resolve({
                        error: true,
                        message: 'kpi_sbd_actuallyID không hợp lệ',
                        status: 400
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ',
                        status: 400
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ',
                        status: 400
                    });
                }

                if (isChecked && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isChecked
                    })) {
                    return resolve({
                        error: true,
                        message: 'đã tạo thành công không hợp lệ',
                        status: 400
                    });
                }

                const checkExists = await KPI_SBD_ACTUALLY_COLL.findById(kpi_sbd_actuallyID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'KPI Thực Đạt SBD (Sản Phẩm Trọng Tâm) không tồn tại',
                        status: 400
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                employee && (dataUpdate.employee = employee);
                amount && (dataUpdate.amount = amount);
                isChecked && (dataUpdate.isChecked = isChecked);
                month && (dataUpdate.month = month);
                year && (dataUpdate.year = year);
                let infoAfterUpdate = await this.findOneAndUpdate({
                    _id: kpi_sbd_actuallyID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại',
                        status: 422
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa kpi_sbd_actually 
     * @param {objectId} kpi_sbd_actuallyID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(kpi_sbd_actuallyID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([kpi_sbd_actuallyID])) {
                    return resolve({
                        error: true,
                        message: 'kpi_sbd_actuallyID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.findByIdAndDelete(kpi_sbd_actuallyID);

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa kpi_sbd_actually 
     * @param {array} kpi_sbd_actuallyID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(kpi_sbd_actuallyID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(kpi_sbd_actuallyID)) {
                    return resolve({
                        error: true,
                        message: 'kpi_sbd_actuallyID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await KPI_SBD_ACTUALLY_COLL.deleteMany({
                    _id: {
                        $in: kpi_sbd_actuallyID
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy thông tin kpi_sbd_actually 
     * @param {objectId} kpi_sbd_actuallyID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(kpi_sbd_actuallyID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([kpi_sbd_actuallyID])) {
                    return resolve({
                        error: true,
                        message: 'kpi_sbd_actuallyID không hợp lệ',
                        status: 400
                    });
                }

                const infoKpi_sbd_actually = await KPI_SBD_ACTUALLY_COLL.findById(kpi_sbd_actuallyID)
                    .populate('region area distributor employee')

                if (!infoKpi_sbd_actually) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin KPI Thực Đạt SBD (Sản Phẩm Trọng Tâm)',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoKpi_sbd_actually,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách kpi_sbd_actually 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listKpi_sbd_actually = await KPI_SBD_ACTUALLY_COLL
                    .find({
                        state: 1
                    }).populate('region area distributor employee')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listKpi_sbd_actually) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách KPI Thực Đạt SBD (Sản Phẩm Trọng Tâm)',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: listKpi_sbd_actually,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
         * Lấy danh sách kpi_sbd_actually theo bộ lọc
		* @param {number} amountFromNumber
		* @param {number} amountToNumber
		* @enum {number} isChecked
		* @param {number} month
		* @param {number} year

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        amountFromNumber,
        amountToNumber,
        isChecked,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };


                if (amountFromNumber && amountToNumber) {
                    conditionObj.amount = {
                        $gte: amountFromNumber,
                        $lt: amountToNumber,
                    };
                }

                isChecked && (conditionObj.isChecked = isChecked);


                const listKpi_sbd_actuallyByFilter = await KPI_SBD_ACTUALLY_COLL
                    .find(conditionObj).populate('region area distributor employee')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listKpi_sbd_actuallyByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách KPI Thực Đạt SBD (Sản Phẩm Trọng Tâm)",
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: listKpi_sbd_actuallyByFilter,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách kpi_sbd_actually theo bộ lọc (server side)
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };




                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }



                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.isChecked) {
                        if (![1, 2].includes(Number(objFilterStatic.isChecked)) || Number.isNaN(Number(objFilterStatic.isChecked))) {
                            return resolve({
                                error: true,
                                message: "đã tạo thành công không hợp lệ",
                                status: 400
                            });
                        }

                        conditionObj.isChecked = Number(objFilterStatic.isChecked);
                    }

                }


                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalKpi_sbd_actually = await KPI_SBD_ACTUALLY_COLL.countDocuments(conditionObj);

                const listKpi_sbd_actuallyByFilter = await KPI_SBD_ACTUALLY_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listKpi_sbd_actuallyByFilter) {
                    return resolve({
                        recordsTotal: totalKpi_sbd_actually,
                        recordsFiltered: totalKpi_sbd_actually,
                        data: []
                    });
                }

                const listKpi_sbd_actuallyDataTable = listKpi_sbd_actuallyByFilter.map((kpi_sbd_actually, index) => {

                    let isChecked = '';
                    if (kpi_sbd_actually.isChecked == 1) {
                        isChecked = 'checked';
                    }

                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${kpi_sbd_actually._id}" type="checkbox" class="check-record check-record-${kpi_sbd_actually._id}" _index ="${index + 1}"><label for="${kpi_sbd_actually._id}"></label></div></td>`,
                        indexSTT: skip + index + 1,
                        employee: kpi_sbd_actually.employee ?
                            kpi_sbd_actually.employee.username :
                            '<span class="badge bg-danger"> <span class="badge bg-danger"> không tồn tại hoặc đã xoá </span> </span>',
                        amount: formatCurrency('###,###.', kpi_sbd_actually.amount),
                        isChecked: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-isChecked" _kpi_sbd_actuallyID="${kpi_sbd_actually._id}" type="checkbox" id="${kpi_sbd_actually._id}" ${isChecked} style="width: 40px;height: 20px;"></div></td>`,
                        month: `${kpi_sbd_actually.month && kpi_sbd_actually.month.length > 50 ? kpi_sbd_actually.month.substr(0,50) + "..." : kpi_sbd_actually.month}`,
                        year: `${kpi_sbd_actually.year && kpi_sbd_actually.year.length > 50 ? kpi_sbd_actually.year.substr(0,50) + "..." : kpi_sbd_actually.year}`,

                        createAt: moment(kpi_sbd_actually.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalKpi_sbd_actually,
                    recordsFiltered: totalKpi_sbd_actually,
                    data: listKpi_sbd_actuallyDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách kpi_sbd_actually theo bộ lọc import
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterImport({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };




                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }


                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.isChecked) {
                        if (![1, 2].includes(Number(objFilterStatic.isChecked)) || Number.isNaN(Number(objFilterStatic.isChecked))) {
                            return resolve({
                                error: true,
                                message: "đã tạo thành công không hợp lệ",
                                status: 400
                            });
                        }

                        conditionObj.isChecked = Number(objFilterStatic.isChecked);
                    }

                }


                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalKpi_sbd_actually = await KPI_SBD_ACTUALLY_COLL.countDocuments(conditionObj);

                const listKpi_sbd_actuallyByFilter = await KPI_SBD_ACTUALLY_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listKpi_sbd_actuallyByFilter) {
                    return resolve({
                        recordsTotal: totalKpi_sbd_actually,
                        recordsFiltered: totalKpi_sbd_actually,
                        data: []
                    });
                }

                return resolve({
                    error: false,
                    recordsTotal: totalKpi_sbd_actually,
                    recordsFiltered: totalKpi_sbd_actually,
                    data: listKpi_sbd_actuallyByFilter || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc kpi_sbd_actually
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc kpi_sbd_actually
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listKpi_sbd_actuallyByFilter = await KPI_SBD_ACTUALLY_COLL.aggregate(arrayFilter)

                if (!listKpi_sbd_actuallyByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listKpi_sbd_actuallyByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listKpi_sbd_actuallyByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('HH:mm DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Tải file import excel mẫu kpi_sbd_actually
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    fileImportExcelPreview({
        opts,
        arrayItemCustomerChoice
    }) {
        return new Promise(async resolve => {
            try {
                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition; // BỘ LỌC && LOẠI IMPORT
                let {
                    listFieldPrimaryKey
                } = condition; // DANH SÁCH PRIMARY KEY

                if (!isEmptyObject(opts)) {
                    condition.conditionDeleteImport.filter = opts.filter; // BỘ LỌC CỦA ADMIN
                    condition.conditionDeleteImport.condition = opts.condition // BỘ LỌC CỦA ADMIN
                }

                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        let index = 0;
                        arrayItemCustomerChoice.map((elem) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            if (elem.dataDynamic && elem.dataDynamic.length) {
                                listFieldPrimaryKey = listFieldPrimaryKey.filter(key => key != elem.nameFieldRef); // LỌC PRIMARY KEY MÀ KHÔNG PHẢI REF

                                elem.dataDynamic.map(item => {
                                    workbook.sheet("Report").row(1).cell(index + 1).value(item);
                                    index++;
                                })
                            } else {
                                workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                                index++;
                            }
                        });

                        if (isTrue(condition.checkDownloadDataOld)) { // KIỂM TRA CÓ ĐÍNH ĐÈM DỮ LIỆU CŨ THEO ĐIỀU KIỆN
                            let listItemImport = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].listItemImport; // LIST FIELD ĐÃ ĐƯỢC CẤU HÌNH
                            let {
                                arrayFilter
                            } = this.getConditionArrayFilterExcel(listItemImport, condition.conditionDeleteImport.filter, condition.conditionDeleteImport.condition); // LẤY RA ARRAY ARREGATE

                            let groupByKpi_sbd_actually = {};
                            listFieldPrimaryKey.map(key => {
                                groupByKpi_sbd_actually = {
                                    ...groupByKpi_sbd_actually,
                                    [key]: '$' + key
                                }
                            });

                            arrayFilter = [
                                ...arrayFilter,
                                {
                                    $group: {
                                        _id: {
                                            groupByKpi_sbd_actually
                                        },
                                        listData: {
                                            $addToSet: "$$CURRENT"
                                        },
                                    }
                                }
                            ];

                            const listKpi_sbd_actuallyByFilter = await KPI_SBD_ACTUALLY_COLL.aggregate(arrayFilter);

                            listKpi_sbd_actuallyByFilter && listKpi_sbd_actuallyByFilter.length && listKpi_sbd_actuallyByFilter.map((item, indexKpi_sbd_actually) => {
                                let indexValue = 0;
                                arrayItemCustomerChoice.map((elem, indexChoice) => {
                                    let variable = elem.name.split('.');

                                    if (elem.dataDynamic && elem.dataDynamic.length) { // KIỂM TRA FIELD CÓ CHỌN DYNAMIC
                                        // LỌC ITEM NÀO TỒN TẠI VỚI MẢNG DATA DYNAMIC
                                        let listKpi_sbd_actuallyAfterFilter = item.listData && item.listData.length && item.listData.filter(value => {
                                            let valueOfField;
                                            if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                if (objDataOfVariable) {
                                                    valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                }
                                            } else {
                                                valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                            }

                                            if (elem.dataDynamic.includes(valueOfField)) { // CHECK NẾU VALUE === CỘT
                                                return value;
                                            }
                                        });

                                        // LỌC BIẾN DYNAMIC NÀO TỒN TẠI VỚI MẢNG DATA DYNAMIC
                                        let listValueExist = listKpi_sbd_actuallyAfterFilter.map(Kpi_sbd_actually => {
                                            let valueOfField;
                                            if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                let objDataOfVariable = Kpi_sbd_actually[variable[0]] ? Kpi_sbd_actually[variable[0]] : '';
                                                if (objDataOfVariable) {
                                                    valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                }
                                            } else {
                                                valueOfField = Kpi_sbd_actually[variable[0]] ? Kpi_sbd_actually[variable[0]] : '';
                                            }
                                            return valueOfField;
                                        });
                                        console.log({
                                            listValueExist
                                        });
                                        elem.dataDynamic.map(dynamic => {

                                            if (item.listData.length > elem.dataDynamic.length) { // KIỂM TRA ĐỘ DÀI CỦA DATA SO VỚI SỐ CỘT
                                                // TODO: XỬ LÝ NHIỀU DYNAMIC

                                            } else {
                                                for (const value of listKpi_sbd_actuallyAfterFilter) {
                                                    let valueOfField;
                                                    if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                        let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                        if (objDataOfVariable) {
                                                            valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                        }
                                                    } else {
                                                        valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                                    }

                                                    if (valueOfField == dynamic) { // CHECK NẾU VALUE === CỘT
                                                        let valueImportDynamic = value[elem.variableChoice] ? value[elem.variableChoice] : '';

                                                        // INSERT DỮ LIỆU VÀO BẢNG VỚI FIELD ĐƯỢC CHỌN THEO DẠNG DYNAMIC
                                                        workbook.sheet("Report").row(indexKpi_sbd_actually + 2).cell(indexValue + 1).value(valueImportDynamic);
                                                        indexValue++;
                                                        break;
                                                    } else {
                                                        if (!listValueExist.includes(dynamic)) {
                                                            indexValue++;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                    } else { // DẠNG STATIC
                                        let valueKpi_sbd_actually;
                                        if (variable.length > 1) {
                                            let objDataOfVariable = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                            if (objDataOfVariable) {
                                                valueKpi_sbd_actually = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                            }
                                        } else {
                                            valueKpi_sbd_actually = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                        }

                                        workbook.sheet("Report").row(indexKpi_sbd_actually + 2).cell(indexValue + 1).value(valueKpi_sbd_actually);
                                        indexValue++;
                                    }
                                });
                            });
                        }

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    pathWriteFile,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Upload File Excel Import Lưu Dữ Liệu kpi_sbd_actually
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    importExcel({
        nameCollParent,
        arrayItemCustomerChoice,
        file,
    }) {
        return new Promise(async resolve => {
            try {

                XlsxPopulate.fromFileAsync(file.path)
                    .then(async workbook => {
                        let listData = [];
                        let index = 2;
                        let conditionDeleteValuePrimaryKey = []; //array value của PRIMARY KEY từ file IMPORT

                        let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition;

                        let listFieldNameConditionDelete = [];
                        if (condition) {
                            listFieldNameConditionDelete = condition.conditionDeleteImport.filter.map(item => item.fieldName);
                        }
                        console.log({
                            listFieldNameConditionDelete
                        });
                        const client = await MongoClient.connect(URL_DATABASE);
                        const db = client.db(NAME_DATABASE)

                        for (; true;) {
                            if (arrayItemCustomerChoice && arrayItemCustomerChoice.length) {
                                let conditionObj = {};
                                let conditionObj__PrimaryKey = {}; //value của PRIMARY KEY từ file IMPORT

                                let totalLength = 0;
                                arrayItemCustomerChoice.map((item, index) => {
                                    if (item.dataDynamic && item.dataDynamic.length) {
                                        totalLength += item.dataDynamic.length;
                                    } else {
                                        totalLength++;
                                    }
                                });

                                let indexOfListField = 0;
                                let checkIsRequire = false;
                                let arrayConditionObjDynamic = [];
                                let arrayConditionObjDynamic__PrimaryKey = [];

                                for (let i = 0; i < totalLength; i++) {
                                    if (arrayItemCustomerChoice[indexOfListField].dataDynamic && arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                        let indexOfDynamic = 1;
                                        for (let valueDynamic of arrayItemCustomerChoice[indexOfListField].dataDynamic) {
                                            let letter = colName(i);
                                            let indexOfCeil = letter.toUpperCase() + index;

                                            let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                            if (variable) {

                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: valueDynamic.trim()
                                                });

                                                let conditionOfOneValueDynamic = {
                                                    [arrayItemCustomerChoice[indexOfListField].variableChoice]: variable
                                                }
                                                if (docs) {
                                                    conditionOfOneValueDynamic = {
                                                        ...conditionOfOneValueDynamic,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    }

                                                    console.log({
                                                        nameFieldRef: [arrayItemCustomerChoice[indexOfListField].nameFieldRef],
                                                        listFieldNameConditionDelete
                                                    });
                                                    // LẤY VALUE để xóa ở dữ liệu DYNAMIC
                                                    if (listFieldNameConditionDelete && listFieldNameConditionDelete.length && listFieldNameConditionDelete.includes(arrayItemCustomerChoice[indexOfListField].nameFieldRef)) {
                                                        arrayConditionObjDynamic__PrimaryKey = [
                                                            ...arrayConditionObjDynamic__PrimaryKey,
                                                            {
                                                                [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                            }
                                                        ]
                                                    }

                                                    // if (listFieldNameConditionDelete && listFieldNameConditionDelete.length && listFieldNameConditionDelete.includes(arrayItemCustomerChoice[indexOfListField].name)) {
                                                    //     conditionObj__PrimaryKey = {
                                                    //         ...conditionObj__PrimaryKey,
                                                    //         [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    //     }
                                                    // }
                                                }

                                                arrayConditionObjDynamic = [
                                                    ...arrayConditionObjDynamic,
                                                    conditionOfOneValueDynamic
                                                ];
                                            }

                                            if (indexOfDynamic < arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                                i++;
                                            }
                                            indexOfDynamic++;
                                        }
                                    } else {
                                        let letter = colName(i);
                                        let indexOfCeil = letter.toUpperCase() + index;
                                        let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                        if (arrayItemCustomerChoice[indexOfListField].isRequire && !variable) {
                                            checkIsRequire = true;
                                            break;
                                        }
                                        if (arrayItemCustomerChoice[indexOfListField].ref && arrayItemCustomerChoice[indexOfListField].ref != nameCollParent) {
                                            if (arrayItemCustomerChoice[indexOfListField].isRequire) {
                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: variable.trim()
                                                })
                                                // .sort({
                                                //     _id: -1
                                                // })
                                                // .limit(100)
                                                // .toArray(); 

                                                // console.log({
                                                //     docs,
                                                //     ___item: arrayItemCustomerChoice[indexOfListField]
                                                // });
                                                if (docs) {
                                                    conditionObj = {
                                                        ...conditionObj,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                    };

                                                    if (listFieldNameConditionDelete && listFieldNameConditionDelete.length && listFieldNameConditionDelete.includes(arrayItemCustomerChoice[indexOfListField].ref)) {
                                                        conditionObj__PrimaryKey = {
                                                            ...conditionObj__PrimaryKey,
                                                            [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                        }
                                                    }

                                                    if (arrayItemCustomerChoice[indexOfListField].mappingRef && arrayItemCustomerChoice[indexOfListField].mappingRef.length) {
                                                        arrayItemCustomerChoice[indexOfListField].mappingRef.map(mapping => {
                                                            conditionObj = {
                                                                ...conditionObj,
                                                                [mapping]: docs[mapping]
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                [arrayItemCustomerChoice[indexOfListField].name]: variable
                                            };
                                            if (listFieldNameConditionDelete && listFieldNameConditionDelete.length && listFieldNameConditionDelete.includes(arrayItemCustomerChoice[indexOfListField].name)) {
                                                conditionObj__PrimaryKey = {
                                                    ...conditionObj__PrimaryKey,
                                                    [arrayItemCustomerChoice[indexOfListField].name]: variable
                                                }
                                            }
                                        }
                                    }

                                    indexOfListField++;
                                }
                                if (checkIsRequire) {
                                    break;
                                }

                                conditionObj = {
                                    ...conditionObj,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                }

                                let arrayCondditionObj = [];
                                if (arrayConditionObjDynamic && arrayConditionObjDynamic.length) {
                                    arrayConditionObjDynamic.map(item => {
                                        arrayCondditionObj = [
                                            ...arrayCondditionObj,
                                            {
                                                ...conditionObj,
                                                ...item
                                            }
                                        ];
                                    });

                                    // Xóa với điều kiện => với PRIMARY KEY
                                    if (listFieldNameConditionDelete && listFieldNameConditionDelete.length) {
                                        if (arrayConditionObjDynamic__PrimaryKey && arrayConditionObjDynamic__PrimaryKey.length) { //value của PRIMARY KEY với DYNAMIC REF 
                                            arrayConditionObjDynamic__PrimaryKey.map(item => {
                                                conditionDeleteValuePrimaryKey = [
                                                    ...conditionDeleteValuePrimaryKey,
                                                    {
                                                        ...conditionObj__PrimaryKey,
                                                        ...item
                                                    }
                                                ];
                                            });
                                        } else {
                                            conditionDeleteValuePrimaryKey = [
                                                ...conditionDeleteValuePrimaryKey,
                                                conditionObj__PrimaryKey
                                            ];
                                        }
                                    }
                                } else {
                                    arrayCondditionObj = [
                                        ...arrayCondditionObj,
                                        conditionObj
                                    ];
                                    if (listFieldNameConditionDelete && listFieldNameConditionDelete.length) {
                                        conditionDeleteValuePrimaryKey = [
                                            ...conditionDeleteValuePrimaryKey,
                                            conditionObj__PrimaryKey
                                        ];
                                    }
                                }


                                listData = [
                                    ...listData,
                                    ...arrayCondditionObj
                                ];

                                index++;
                            }
                        }

                        await fs.unlinkSync(file.path);

                        if (listData.length) {
                            await this.changeDataImport({
                                condition,
                                listKpi_sbd_actually: listData,
                                conditionDeleteValuePrimaryKey
                            });
                        } else {
                            return resolve({
                                error: true,
                                message: 'Import thất bại'
                            });
                        }

                        return resolve({
                            error: false,
                            message: 'Import thành công'
                        });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lưu Dữ Liệu Theo Lựa Chọn kpi_sbd_actually
     * @param {object} listKpi_sbd_actually
     * @param {object} condition
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    changeDataImport({
        condition,
        listKpi_sbd_actually,
        conditionDeleteValuePrimaryKey
    }) {
        return new Promise(async resolve => {
            try {
                if (isTrue(condition.delete)) { // XÓA DATA CŨ
                    if (isTrue(condition.deleteAll)) { // XÓA TẤT CẢ DỮ LIỆU
                        console.log("====================XÓA TẤT CẢ DỮ LIỆU====================");
                        await KPI_SBD_ACTUALLY_COLL.deleteMany({});
                        let listDataAfterInsert = await KPI_SBD_ACTUALLY_COLL.insertMany(listKpi_sbd_actually);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    } else { // XÓA VỚI ĐIỀU KIỆN
                        console.log("====================XÓA VỚI ĐIỀU KIỆN====================");

                        /**
                         * ===========================================================================
                         * =========================XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===========================================================================
                         */
                        let {
                            filter,
                            condition: conditionMultiple,
                        } = condition.conditionDeleteImport;

                        if (!filter || !filter.length) {
                            return resolve({
                                error: true,
                                message: 'Filter do not exist'
                            });
                        }

                        let conditionObj = {
                            state: 1,
                            $or: []
                        };

                        if (filter && filter.length) {
                            if (filter.length > 1) {

                                filter.map(filterObj => {
                                    if (filterObj.type === 'ref') {
                                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionFieldRef);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionFieldRef
                                            };
                                        }
                                    } else {
                                        const conditionByFilter = this.getConditionObj(filterObj);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionByFilter);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionByFilter
                                            };
                                        }
                                    }
                                });

                            } else {
                                let {
                                    type,
                                    ref,
                                    fieldRefName
                                } = filter[0];

                                if (type === 'ref') {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(ref, fieldRefName)
                                    };
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(filter[0])
                                    };
                                }
                            }
                        }

                        if (conditionObj.$or && !conditionObj.$or.length) {
                            delete conditionObj.$or;
                        }

                        // let listAfterDelete = await KPI_SBD_ACTUALLY_COLL.deleteMany({ 
                        //     ...conditionObj 
                        // });

                        let listValueAfterDelete = conditionDeleteValuePrimaryKey && conditionDeleteValuePrimaryKey.length && conditionDeleteValuePrimaryKey.map(value => {
                            return KPI_SBD_ACTUALLY_COLL.deleteMany(value);
                        });
                        let result = await Promise.all(listValueAfterDelete);
                        /**
                         * ===============================================================================
                         * =========================END XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===============================================================================
                         */

                        if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                            let listKpi_sbd_actuallyAfterInsert = listKpi_sbd_actually.map(item => {
                                let listConditionFindOneUpdate = {};
                                let {
                                    listFieldPrimaryKey
                                } = condition;
                                listFieldPrimaryKey.map(elem => {
                                    listConditionFindOneUpdate = {
                                        ...listConditionFindOneUpdate,
                                        [elem]: item[elem]
                                    }
                                });

                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    state: 1
                                }

                                return KPI_SBD_ACTUALLY_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                    $set: item
                                }, {
                                    upsert: true
                                });
                            });
                            let resultAfterUpdate = await Promise.all(listKpi_sbd_actuallyAfterInsert);
                            return resolve({
                                error: false,
                                message: 'Insert success'
                            });

                        } else { // INSERT CÁI MỚI
                            console.log("====================INSERT CÁI MỚI 2====================");
                            let listDataAfterInsert = await KPI_SBD_ACTUALLY_COLL.insertMany(listKpi_sbd_actually);
                            return resolve({
                                error: false,
                                message: 'Insert success',
                                data: listDataAfterInsert
                            });
                        }
                    }
                } else { // KHÔNG XÓA DATA CŨ
                    if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                        console.log("====================KIỂM TRA TỒN TẠI VÀ UPDATE====================");
                        let listKpi_sbd_actuallyAfterInsert = listKpi_sbd_actually.map(item => {
                            let listConditionFindOneUpdate = {};
                            let {
                                listFieldPrimaryKey
                            } = condition;
                            listFieldPrimaryKey.map(elem => {
                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    [elem]: item[elem]
                                }
                            });

                            listConditionFindOneUpdate = {
                                ...listConditionFindOneUpdate,
                                state: 1
                            }

                            return KPI_SBD_ACTUALLY_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                $set: item
                            }, {
                                upsert: true
                            });
                        });
                        let resultAfterUpdate = await Promise.all(listKpi_sbd_actuallyAfterInsert);

                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: resultAfterUpdate
                        });
                    } else { // INSERT CÁI MỚI
                        console.log("====================INSERT CÁI MỚI====================");
                        let listDataAfterInsert = await KPI_SBD_ACTUALLY_COLL.insertMany(listKpi_sbd_actually);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    }
                }

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc kpi_sbd_actually
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
            $or: []
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            let mappingRef = [];
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                    mappingRef = element.mappingRef;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            fieldID: item.fieldID,
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            ref: item.coll,
                            variable: item.name,
                            nameFieldRef,
                            dataEnum: item.dataEnum,
                            isRequire: item.isRequire,
                            dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                            variableChoice: item.variableChoice,
                            nameCollChoice: item.nameCollChoice,
                            mappingRef: mappingRef,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                fieldID: item.fieldID,
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                ref: item.coll,
                                variable: item.name,
                                nameFieldRef,
                                dataEnum: item.dataEnum,
                                isRequire: item.isRequire,
                                dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                variableChoice: item.variableChoice,
                                nameCollChoice: item.nameCollChoice,
                                mappingRef: mappingRef,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    dataEnum: item.dataEnum,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {
            if (filter.length > 1) {

                filter.map(filterObj => {
                    if (filterObj.type === 'ref') {
                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionFieldRef);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionFieldRef
                            };
                        }
                    } else {
                        const conditionByFilter = this.getConditionObj(filterObj);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionByFilter);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionByFilter
                            };
                        }
                    }
                });

            } else {
                let {
                    type,
                    ref,
                    fieldRefName
                } = filter[0];

                if (type === 'ref') {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(ref, fieldRefName)
                    };
                } else {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(filter[0])
                    };
                }
            }
        }

        if (conditionObj.$or && !conditionObj.$or.length) {
            delete conditionObj.$or;
        }


        if (!isEmptyObject(objFilterStatic)) {

            if (objFilterStatic.isChecked) {
                if (![1, 2].includes(Number(objFilterStatic.isChecked)) || Number.isNaN(Number(objFilterStatic.isChecked))) {
                    return resolve({
                        error: true,
                        message: "đã tạo thành công không hợp lệ",
                        status: 400
                    });
                }

                conditionObj.isChecked = Number(objFilterStatic.isChecked);
            }

        }


        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let collName = pluralize.plural(item.coll);
                let checkPluralColl = collName[collName.length - 1];

                if (checkPluralColl.toLowerCase() != 's') {
                    collName += 's';
                }

                let lookup = [{
                        $lookup: {
                            from: collName,
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        let collNamePopulate = pluralize.plural(populate.coll);
                        let checkPluralColl = collNamePopulate[collNamePopulate.length - 1];

                        if (checkPluralColl.toLowerCase() != 's') {
                            collNamePopulate += 's';
                        }

                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: collNamePopulate,
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Lấy thông tin kpi_sbd_actually
     * @param {objectId} kpi_sbd_actuallyID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoKpi_sbd_actually({
        kpi_sbd_actuallyID,
        select,
        filter = {},
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];
                let conditionObj = {
                    state: 1
                };

                if (!checkObjectIDs([kpi_sbd_actuallyID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị kpi_sbd_actuallyID không hợp lệ',
                        status: 400
                    });
                }

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                Object.keys(filter).map(key => {
                    if (![].includes(key)) {
                        delete filter[key];
                    }
                });

                let {} = filter;


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'employee'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoKpi_sbd_actually = await KPI_SBD_ACTUALLY_COLL
                    .findOne({
                        _id: kpi_sbd_actuallyID,
                        ...conditionObj
                    })
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoKpi_sbd_actually = await KPI_SBD_ACTUALLY_COLL.populate(infoKpi_sbd_actually, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoKpi_sbd_actually = await KPI_SBD_ACTUALLY_COLL.populate(infoKpi_sbd_actually, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoKpi_sbd_actually) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin kpi_sbd_actually',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoKpi_sbd_actually,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách kpi_sbd_actually
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListKpi_sbd_actuallys({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                } else {

                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['region', 'area', 'distributor', 'employee', 'amountFromNumber', 'amountToNumber', 'isChecked'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    region,
                    area,
                    distributor,
                    employee,
                    amountFromNumber,
                    amountToNumber,
                    isChecked,
                } = filter;

                region && (conditionObj.region = region);

                area && (conditionObj.area = area);

                distributor && (conditionObj.distributor = distributor);

                employee && (conditionObj.employee = employee);

                if (amountFromNumber && amountToNumber) {
                    conditionObj.amount = {
                        $gte: amountFromNumber,
                        $lte: amountToNumber,
                    };
                }

                isChecked && (conditionObj.isChecked = isChecked);


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'employee'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listKpi_sbd_actuallys = await KPI_SBD_ACTUALLY_COLL
                    .find({
                        ...conditionObj
                    }, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listKpi_sbd_actuallys = await KPI_SBD_ACTUALLY_COLL.populate(listKpi_sbd_actuallys, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listKpi_sbd_actuallys = await KPI_SBD_ACTUALLY_COLL.populate(listKpi_sbd_actuallys, `${ref}.${field}`);
                        }
                    }
                }

                if (!listKpi_sbd_actuallys) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách kpi_sbd_actually',
                        status: 400
                    });
                }

                let totalRecord = await KPI_SBD_ACTUALLY_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listKpi_sbd_actuallys,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Tạo kpi_sbd_actually
     
     * @param {objectId} authorID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    insertKpi_sbd_actually({
        region,
        area,
        distributor,
        employee,
        amount,
        isChecked,
        month,
        year,
        authorID
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([authorID]))
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ',
                        status: 400
                    });



                if (region && !checkObjectIDs(region)) {
                    return resolve({
                        error: true,
                        message: 'Miền không hợp lệ',
                        status: 400
                    });
                }

                if (area && !checkObjectIDs(area)) {
                    return resolve({
                        error: true,
                        message: 'Vùng không hợp lệ',
                        status: 400
                    });
                }

                if (distributor && !checkObjectIDs(distributor)) {
                    return resolve({
                        error: true,
                        message: 'Nhà Phân Phối không hợp lệ',
                        status: 400
                    });
                }

                if (employee && !checkObjectIDs(employee)) {
                    return resolve({
                        error: true,
                        message: 'Nhân Viên không hợp lệ',
                        status: 400
                    });
                }

                if (isChecked && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isChecked
                    })) {
                    return resolve({
                        error: true,
                        message: 'Đã tạo thành công không hợp lệ',
                        status: 400
                    });
                }

                let dataInsert = {
                    region,
                    area,
                    distributor,
                    employee,
                    amount,
                    isChecked,
                    month,
                    year,
                    author: authorID
                };
                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);


                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo KPI_SBD_ACTUALLY thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Cập nhật kpi_sbd_actually
     
     * @param {objectId} kpi_sbd_actuallyID
     * @param {objectId} authorID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    updateKpi_sbd_actually({
        kpi_sbd_actuallyID,
        region,
        area,
        distributor,
        employee,
        amount,
        isChecked,
        month,
        year,
        authorID
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([kpi_sbd_actuallyID]))
                    return resolve({
                        error: true,
                        message: 'kpi_sbd_actuallyID không hợp lệ',
                        status: 400
                    });

                if (!checkObjectIDs([authorID]))
                    return resolve({
                        error: true,
                        message: 'ID user cập nhật không hợp lệ',
                        status: 400
                    });


                if (region && !checkObjectIDs(region)) {
                    return resolve({
                        error: true,
                        message: 'Miền không hợp lệ',
                        status: 400
                    });
                }

                if (area && !checkObjectIDs(area)) {
                    return resolve({
                        error: true,
                        message: 'Vùng không hợp lệ',
                        status: 400
                    });
                }

                if (distributor && !checkObjectIDs(distributor)) {
                    return resolve({
                        error: true,
                        message: 'Nhà Phân Phối không hợp lệ',
                        status: 400
                    });
                }

                if (employee && !checkObjectIDs(employee)) {
                    return resolve({
                        error: true,
                        message: 'Nhân Viên không hợp lệ',
                        status: 400
                    });
                }

                if (isChecked && !checkNumberIsValidWithRange({
                        arrValid: [1, 2],
                        val: isChecked
                    })) {
                    return resolve({
                        error: true,
                        message: 'Đã tạo thành công không hợp lệ',
                        status: 400
                    });
                }

                const checkExists = await KPI_SBD_ACTUALLY_COLL.findOne({
                    _id: kpi_sbd_actuallyID,
                    state: 1
                });
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'kpi_sbd_actually không tồn tại',
                        status: 404
                    });
                }

                let dataUpdate = {
                    userUpdate: authorID
                };
                region && (dataUpdate.region = region);
                area && (dataUpdate.area = area);
                distributor && (dataUpdate.distributor = distributor);
                employee && (dataUpdate.employee = employee);
                amount && (dataUpdate.amount = amount);
                isChecked && (dataUpdate.isChecked = isChecked);
                month && (dataUpdate.month = month);
                year && (dataUpdate.year = year);
                let infoAfterUpdate = await this.findOneAndUpdate({
                    _id: kpi_sbd_actuallyID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa kpi_sbd_actually
     * @param {objectId} kpi_sbd_actuallyID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteKpi_sbd_actually(kpi_sbd_actuallyID) {
        return new Promise(async resolve => {
            try {
                let ids = [kpi_sbd_actuallyID];

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị kpi_sbd_actuallyID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.deleteMany({
                    _id: {
                        $in: ids
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa kpi_sbd_actually
     * @param {objectId} kpi_sbd_actuallyID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteKpi_sbd_actuallys(kpi_sbd_actuallysID) {
        return new Promise(async resolve => {
            try {
                let ids = kpi_sbd_actuallysID.split(',');

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị kpi_sbd_actuallysID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.deleteMany({
                    _id: {
                        $in: ids
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

}

module.exports.MODEL = new Model;

/**
 * COLLECTIONS
 */
var KPI_SBD_ACTUALLY_COLL = require('../databases/kpi_sbd_actually-coll');

var REGION_COLL = require('../../region_area/databases/region-coll');
var {
    MODEL: REGION_MODEL
} = require('../../region_area/models/region');

var AREA_COLL = require('../../region_area/databases/area-coll');
var {
    MODEL: AREA_MODEL
} = require('../../region_area/models/area');

var DISTRIBUTOR_COLL = require('../../distributor/databases/distributor-coll');
var {
    MODEL: DISTRIBUTOR_MODEL
} = require('../../distributor/models/distributor');

var EMPLOYEE_COLL = require('../../employee/databases/employee-coll');
var {
    MODEL: EMPLOYEE_MODEL
} = require('../../employee/models/employee');