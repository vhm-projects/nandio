/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const beautifyer = require('js-beautify').js_beautify;
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {} = require('../constants/kpi_mhtt_config_products');
const {
    CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS
} = require('../constants/kpi_mhtt_config_products/kpi_mhtt_config_products.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const KPI_MHTT_CONFIG_PRODUCTS_MODEL = require('../models/kpi_mhtt_config_products').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const {
    PRODUCT_COLL
} = require('../../product');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ KPI_MHTT_CONFIG_PRODUCTS  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Kpi_mhtt_config_products (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.ADD_KPI_MHTT_CONFIG_PRODUCTS]: {
                config: {
                    scopes: ['create:kpi_mhtt_config_products'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Kpi_mhtt_config_products',
                    code: CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.ADD_KPI_MHTT_CONFIG_PRODUCTS,
                    inc: path.resolve(__dirname, '../views/kpi_mhtt_config_products/add_kpi_mhtt_config_products.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        ChildRouter.renderToView(req, res, {
                            CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            product,
                            month,
                            year,
                        } = req.body;

                        let infoAfterInsert = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.insert({
                            product,
                            month,
                            year,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Insert manual
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.ADD_MANUAL_KPI_MHTT_CONFIG_PRODUCTS]: {
                config: {
                    scopes: ['create:kpi_mhtt_config_products'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            products,
                            month,
                            year,
                        } = req.body;

                        let infoAfterInsert = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.insertManual({
                            products,
                            month,
                            year,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Kpi_mhtt_config_products By Id (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.UPDATE_KPI_MHTT_CONFIG_PRODUCTS_BY_ID]: {
                config: {
                    scopes: ['update:kpi_mhtt_config_products'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Kpi_mhtt_config_products',
                    code: CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.UPDATE_KPI_MHTT_CONFIG_PRODUCTS_BY_ID,
                    inc: path.resolve(__dirname, '../views/kpi_mhtt_config_products/update_kpi_mhtt_config_products.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            kpi_mhtt_config_productsID
                        } = req.query;

                        let infoKpi_mhtt_config_products = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.getInfoById(kpi_mhtt_config_productsID);
                        if (infoKpi_mhtt_config_products.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionProduct = {
                            state: 1,
                            status: 1
                        };
                        if (infoKpi_mhtt_config_products.data?.product) {

                            conditionProduct._id = infoKpi_mhtt_config_products.data.product?._id;

                        }

                        let listProducts = await PRODUCT_COLL
                            .find(conditionProduct)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoKpi_mhtt_config_products: infoKpi_mhtt_config_products.data || {},

                            listProducts,
                            CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_mhtt_config_productsID,
                            product,
                            month,
                            year,
                        } = req.body;


                        const infoAfterUpdate = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.update({
                            kpi_mhtt_config_productsID,
                            product,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Kpi_mhtt_config_products By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.UPDATE_KPI_MHTT_CONFIG_PRODUCTS_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:kpi_mhtt_config_products'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_mhtt_config_productsID,
                            product,
                            month,
                            year,
                        } = req.body;


                        const infoAfterUpdate = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.updateNotRequire({
                            kpi_mhtt_config_productsID,
                            product,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_mhtt_config_products By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.DELETE_KPI_MHTT_CONFIG_PRODUCTS_BY_ID]: {
                config: {
                    scopes: ['delete:kpi_mhtt_config_products'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            kpi_mhtt_config_productsID
                        } = req.params;

                        const infoAfterDelete = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.deleteById(kpi_mhtt_config_productsID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_mhtt_config_products By List Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.DELETE_KPI_MHTT_CONFIG_PRODUCTS_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:kpi_mhtt_config_products'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            kpi_mhtt_config_productsID
                        } = req.body;

                        const infoAfterDelete = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.deleteByListId(kpi_mhtt_config_productsID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Kpi_mhtt_config_products By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.GET_INFO_KPI_MHTT_CONFIG_PRODUCTS_BY_ID]: {
                config: {
                    scopes: ['read:info_kpi_mhtt_config_products'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            kpi_mhtt_config_productsID
                        } = req.params;

                        const infoKpi_mhtt_config_productsById = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.getInfoById(kpi_mhtt_config_productsID);
                        res.json(infoKpi_mhtt_config_productsById);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_mhtt_config_products (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.GET_LIST_KPI_MHTT_CONFIG_PRODUCTS]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_config_products'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_mhtt_config_products',
                    code: CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.GET_LIST_KPI_MHTT_CONFIG_PRODUCTS,
                    inc: path.resolve(__dirname, '../views/kpi_mhtt_config_products/list_kpi_mhtt_config_productss.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            typeGetList
                        } = req.query;

                        let variants = [
                            { value: "Y5%", text: "Y5%" },
                            { value: "Y6%", text: "Y6%" },
                            { value: "Y8%", text: "Y8%" },
                            { value: "Y9%", text: "Y9%" },
                            { value: "Y15%", text: "Y15%" },
                            { value: "Y21%", text: "Y21%" },
                            { value: "DIAMOND", text: "Diamond" },
                            { value: "GOLD", text: "Gold" },
                            { value: "PLATINUM", text: "Platinum" },
                            { value: "TRAI_CAY_VI_LUU", text: "Trái cây vị lựu" },
                            { value: "TRAI_CAY_VIET_QUAT", text: "Trái cây việt quất" }
                        ]
                    
                        let specifications = [
                            { value: "HP", text: "HP" },
                            { value: "LC", text: "LC" },
                            { value: "LO", text: "LO" },
                            { value: "TUI", text: "TUI"},
                        ]

                        let listKpi_mhtt_config_productss = [];
                        if (typeGetList === 'FILTER') {
                            listKpi_mhtt_config_productss = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.getListByFilter({

                            });
                        } else {
                            listKpi_mhtt_config_productss = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listKpi_mhtt_config_productss: listKpi_mhtt_config_productss.data || [],
                            variants,specifications
                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_mhtt_config_products By Field (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.GET_LIST_KPI_MHTT_CONFIG_PRODUCTS_BY_FIELD]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_config_products'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_mhtt_config_products by field isStatus',
                    code: CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.GET_LIST_KPI_MHTT_CONFIG_PRODUCTS_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/kpi_mhtt_config_products/list_kpi_mhtt_config_productss.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            type
                        } = req.query;

                        let listKpi_mhtt_config_productss = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.getListByFilter({
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listKpi_mhtt_config_productss: listKpi_mhtt_config_productss.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_mhtt_config_products Server Side (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.GET_LIST_KPI_MHTT_CONFIG_PRODUCTS_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_config_products'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listKpi_mhtt_config_productsServerSide = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listKpi_mhtt_config_productsServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_mhtt_config_products Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.GET_LIST_KPI_MHTT_CONFIG_PRODUCTS_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_config_products'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listKpi_mhtt_config_productsImport = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listKpi_mhtt_config_productsImport);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_mhtt_config_products Excel Server Side (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.GET_LIST_KPI_MHTT_CONFIG_PRODUCTS_EXCEL]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_config_products'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = KPI_MHTT_CONFIG_PRODUCTS_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Kpi_mhtt_config_products Excel Export (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.DOWNLOAD_LIST_KPI_MHTT_CONFIG_PRODUCTS_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_config_products'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'kpi_mhtt_config_products'
                        });

                        let conditionObj = KPI_MHTT_CONFIG_PRODUCTS_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listKpi_mhtt_config_products = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listKpi_mhtt_config_products)
                    }]
                },
            },

            /**
             * Function: Setting Kpi_mhtt_config_products Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.SETTING_FILE_KPI_MHTT_CONFIG_PRODUCTS_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_config_products'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = KPI_MHTT_CONFIG_PRODUCTS_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Kpi_mhtt_config_products Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.DOWNLOAD_FILE_KPI_MHTT_CONFIG_PRODUCTS_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_config_products'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_mhtt_config_products'
                        });

                        let listKpi_mhtt_config_productsImport = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listKpi_mhtt_config_productsImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listKpi_mhtt_config_productsImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Kpi_mhtt_config_products Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS.CREATE_KPI_MHTT_CONFIG_PRODUCTS_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:kpi_mhtt_config_products'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_mhtt_config_products'
                        });

                        let infoKpi_mhtt_config_productsAfterImport = await KPI_MHTT_CONFIG_PRODUCTS_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'kpi_mhtt_config_products',
                        });

                        res.json(infoKpi_mhtt_config_productsAfterImport);
                    }]
                },
            },

        }
    }
};