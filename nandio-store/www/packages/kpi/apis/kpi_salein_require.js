/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {} = require('../constants/kpi_salein_require');
const {
    CF_ROUTINGS_KPI_SALEIN_REQUIRE
} = require('../constants/kpi_salein_require/kpi_salein_require.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */

const {
    MODEL: KPI_SALEIN_REQUIRE_MODEL
} = require('../models/kpi_salein_require');
const {
    MODEL: MANAGE_COLL_MODEL
} = require('../../../models/manage_coll');

const {
    MODEL: EMPLOYEE_MODEL
} = require('../../employee/models/employee');

const {
    MODEL: PRODUCT_MODEL
} = require('../../product/models/product');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ KPI_SALEIN_REQUIRE  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Kpi_salein_require (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.ADD_KPI_SALEIN_REQUIRE]: {
                config: {
                    scopes: ['create:kpi_salein_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Kpi_salein_require',
                    code: CF_ROUTINGS_KPI_SALEIN_REQUIRE.ADD_KPI_SALEIN_REQUIRE,
                    inc: path.resolve(__dirname, '../views/kpi_salein_require/add_kpi_salein_require.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listProducts = await PRODUCT_MODEL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean();


                        ChildRouter.renderToView(req, res, {
                            listProducts,
                            CF_ROUTINGS_KPI_SALEIN_REQUIRE
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            amount,
                            employee,
                            month,
                            year,
                            product,
                        } = req.body;


                        let response = await KPI_SALEIN_REQUIRE_MODEL.insert({
                            amount,
                            employee,
                            month,
                            year,
                            product,
                            userCreate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update Kpi_salein_require By Id (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.UPDATE_KPI_SALEIN_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:kpi_salein_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Kpi_salein_require',
                    code: CF_ROUTINGS_KPI_SALEIN_REQUIRE.UPDATE_KPI_SALEIN_REQUIRE_BY_ID,
                    inc: path.resolve(__dirname, '../views/kpi_salein_require/update_kpi_salein_require.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            kpi_salein_requireID
                        } = req.query;

                        let infoKpi_salein_require = await KPI_SALEIN_REQUIRE_MODEL.getInfoById(kpi_salein_requireID);
                        if (infoKpi_salein_require.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionEmployee = {
                            state: 1,
                            status: 1
                        };
                        if (infoKpi_salein_require.data.employee) {

                            conditionEmployee._id = infoKpi_salein_require.data.employee._id;

                        }

                        let listEmployees = await EMPLOYEE_MODEL
                            .find(conditionEmployee)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let listProducts = await PRODUCT_MODEL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoKpi_salein_require: infoKpi_salein_require.data || {},

                            listEmployees,
                            listProducts,
                            CF_ROUTINGS_KPI_SALEIN_REQUIRE
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_salein_requireID,
                            amount,
                            employee,
                            month,
                            year,
                            product,
                        } = req.body;


                        const response = await KPI_SALEIN_REQUIRE_MODEL.update({
                            kpi_salein_requireID,
                            amount,
                            employee,
                            month,
                            year,
                            product,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update not require Kpi_salein_require By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.UPDATE_KPI_SALEIN_REQUIRE_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:kpi_salein_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_salein_requireID,
                            amount,
                            employee,
                            month,
                            year,
                            product,
                        } = req.body;


                        const response = await KPI_SALEIN_REQUIRE_MODEL.updateNotRequire({
                            kpi_salein_requireID,
                            amount,
                            employee,
                            month,
                            year,
                            product,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_salein_require By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.DELETE_KPI_SALEIN_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['delete:kpi_salein_require'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            kpi_salein_requireID
                        } = req.params;

                        const response = await KPI_SALEIN_REQUIRE_MODEL.deleteById(kpi_salein_requireID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_salein_require By List Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.DELETE_KPI_SALEIN_REQUIRE_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:kpi_salein_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            kpi_salein_requireID
                        } = req.body;

                        const response = await KPI_SALEIN_REQUIRE_MODEL.deleteByListId(kpi_salein_requireID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get Info Kpi_salein_require By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.GET_INFO_KPI_SALEIN_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['read:info_kpi_salein_require'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            kpi_salein_requireID
                        } = req.params;

                        const response = await KPI_SALEIN_REQUIRE_MODEL.getInfoById(kpi_salein_requireID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_salein_require (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.GET_LIST_KPI_SALEIN_REQUIRE]: {
                config: {
                    scopes: ['read:list_kpi_salein_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_salein_require',
                    code: CF_ROUTINGS_KPI_SALEIN_REQUIRE.GET_LIST_KPI_SALEIN_REQUIRE,
                    inc: path.resolve(__dirname, '../views/kpi_salein_require/list_kpi_salein_requires.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            amountFromNumber,
                            amountToNumber,
                            typeGetList
                        } = req.query;
                        let { region } = req.user;
                        let listKpi_salein_requires = [];
                        if (typeGetList === 'FILTER') {
                            listKpi_salein_requires = await KPI_SALEIN_REQUIRE_MODEL.getListByFilter({
                                amountFromNumber,
                                amountToNumber,
                                region
                            });
                        } else {
                            listKpi_salein_requires = await KPI_SALEIN_REQUIRE_MODEL.getList({ region });
                        }

                        ChildRouter.renderToView(req, res, {
                            listKpi_salein_requires: listKpi_salein_requires.data || [],
                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_salein_require By Field (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.GET_LIST_KPI_SALEIN_REQUIRE_BY_FIELD]: {
                config: {
                    scopes: ['read:list_kpi_salein_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_salein_require by field isStatus',
                    code: CF_ROUTINGS_KPI_SALEIN_REQUIRE.GET_LIST_KPI_SALEIN_REQUIRE_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/kpi_salein_require/list_kpi_salein_requires.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            amountFromNumber,
                            amountToNumber,
                            type
                        } = req.query;

                        let listKpi_salein_requires = await KPI_SALEIN_REQUIRE_MODEL.getListByFilter({
                            amountFromNumber,
                            amountToNumber,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listKpi_salein_requires: listKpi_salein_requires.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_salein_require Server Side (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.GET_LIST_KPI_SALEIN_REQUIRE_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_kpi_salein_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }
                        let { region } = req.user;
                        const response = await KPI_SALEIN_REQUIRE_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_salein_require Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.GET_LIST_KPI_SALEIN_REQUIRE_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_salein_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const response = await KPI_SALEIN_REQUIRE_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_salein_require Excel Server Side (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.GET_LIST_KPI_SALEIN_REQUIRE_EXCEL]: {
                config: {
                    scopes: ['read:list_kpi_salein_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = KPI_SALEIN_REQUIRE_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let response = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Kpi_salein_require Excel Export (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.DOWNLOAD_LIST_KPI_SALEIN_REQUIRE_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_kpi_salein_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'kpi_salein_require'
                        });

                        let conditionObj = KPI_SALEIN_REQUIRE_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let response = await KPI_SALEIN_REQUIRE_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Setting Kpi_salein_require Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.SETTING_FILE_KPI_SALEIN_REQUIRE_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_kpi_salein_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = KPI_SALEIN_REQUIRE_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let response = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Kpi_salein_require Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.DOWNLOAD_FILE_KPI_SALEIN_REQUIRE_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_salein_require'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            opts
                        } = req.query;
                        let condition = JSON.parse(opts);

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_salein_require'
                        });

                        let listKpi_salein_requireImport = await KPI_SALEIN_REQUIRE_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            opts: condition
                        });

                        res.download(listKpi_salein_requireImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listKpi_salein_requireImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Kpi_salein_require Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.CREATE_KPI_SALEIN_REQUIRE_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:kpi_salein_require'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_salein_require'
                        });
                        
                        let response = await KPI_SALEIN_REQUIRE_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'kpi_salein_require',
                        });

                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Get info Kpi_salein_require
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.API_GET_INFO_KPI_SALEIN_REQUIRE]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        const {
                            kpi_salein_requireID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const response = await KPI_SALEIN_REQUIRE_MODEL.getInfoKpi_salein_require({
                            kpi_salein_requireID,
                            select,
                            filter,
                            explain,
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Get list Kpi_salein_require
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.API_GET_LIST_KPI_SALEIN_REQUIRES]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listKpi_salein_requires = await KPI_SALEIN_REQUIRE_MODEL.getListKpi_salein_requires({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                        });
                        res.json(listKpi_salein_requires);
                    }]
                },
            },

            /**
             * Function: API Insert Kpi_salein_require
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.API_ADD_KPI_SALEIN_REQUIRE]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {
                            region,
                            area,
                            distributor,
                            amount,
                            employee,
                            month,
                            year,
                            product,
                        } = req.body;


                        const response = await KPI_SALEIN_REQUIRE_MODEL.insertKpi_salein_require({
                            region,
                            area,
                            distributor,
                            amount,
                            employee,
                            month,
                            year,
                            product,
                            authorID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Update Kpi_salein_require
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.API_UPDATE_KPI_SALEIN_REQUIRE]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let authorID = req.user && req.user._id;
                        let {
                            kpi_salein_requireID
                        } = req.params;
                        let {
                            region,
                            area,
                            distributor,
                            amount,
                            employee,
                            month,
                            year,
                            product,
                        } = req.body;


                        let response = await KPI_SALEIN_REQUIRE_MODEL.updateKpi_salein_require({
                            kpi_salein_requireID,
                            region,
                            area,
                            distributor,
                            amount,
                            employee,
                            month,
                            year,
                            product,
                            authorID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Delete Kpi_salein_require
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.API_DELETE_KPI_SALEIN_REQUIRE]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {

                        const {
                            kpi_salein_requireID
                        } = req.params;

                        const response = await KPI_SALEIN_REQUIRE_MODEL.deleteKpi_salein_require(kpi_salein_requireID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Delete Kpi_salein_require
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_REQUIRE.API_DELETE_KPI_SALEIN_REQUIRES]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {

                        const {
                            kpi_salein_requiresID
                        } = req.params;

                        const response = await KPI_SALEIN_REQUIRE_MODEL.deleteKpi_salein_requires(kpi_salein_requiresID);
                        res.json(response);
                    }]
                },
            },

        }
    }
};