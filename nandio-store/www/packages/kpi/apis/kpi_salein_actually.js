/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {} = require('../constants/kpi_salein_actually');
const {
    CF_ROUTINGS_KPI_SALEIN_ACTUALLY
} = require('../constants/kpi_salein_actually/kpi_salein_actually.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */

const {
    MODEL: KPI_SALEIN_ACTUALLY_MODEL
} = require('../models/kpi_salein_actually');
const {
    MODEL: MANAGE_COLL_MODEL
} = require('../../../models/manage_coll');

const {
    MODEL: STORE_MODEL
} = require('../../store/models/store');

const {
    MODEL: EMPLOYEE_MODEL
} = require('../../employee/models/employee');

const {
    MODEL: PRODUCT_MODEL
} = require('../../product/models/product');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ KPI_SALEIN_ACTUALLY  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Kpi_salein_actually (API, VIEW)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.ADD_KPI_SALEIN_ACTUALLY]: {
                config: {
                    scopes: ['create:kpi_salein_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Kpi_salein_actually',
                    code: CF_ROUTINGS_KPI_SALEIN_ACTUALLY.ADD_KPI_SALEIN_ACTUALLY,
                    inc: path.resolve(__dirname, '../views/kpi_salein_actually/add_kpi_salein_actually.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listEmployees = await EMPLOYEE_MODEL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean();

                        let listProducts = await PRODUCT_MODEL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean();


                        ChildRouter.renderToView(req, res, {
                            listEmployees,
                            listProducts,
                            CF_ROUTINGS_KPI_SALEIN_ACTUALLY
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            store,
                            employee,
                            SALE_ORDER_ID,
                            ORDER_NUMBER,
                            ORDER_DATE,
                            product,
                            QUANTITY,
                            IS_FREE_ITEM,
                        } = req.body;


                        let response = await KPI_SALEIN_ACTUALLY_MODEL.insert({
                            store,
                            employee,
                            SALE_ORDER_ID,
                            ORDER_NUMBER,
                            ORDER_DATE,
                            product,
                            QUANTITY,
                            IS_FREE_ITEM,
                            userCreate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update Kpi_salein_actually By Id (API, VIEW)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.UPDATE_KPI_SALEIN_ACTUALLY_BY_ID]: {
                config: {
                    scopes: ['update:kpi_salein_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Kpi_salein_actually',
                    code: CF_ROUTINGS_KPI_SALEIN_ACTUALLY.UPDATE_KPI_SALEIN_ACTUALLY_BY_ID,
                    inc: path.resolve(__dirname, '../views/kpi_salein_actually/update_kpi_salein_actually.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            kpi_salein_actuallyID
                        } = req.query;

                        let infoKpi_salein_actually = await KPI_SALEIN_ACTUALLY_MODEL.getInfoById(kpi_salein_actuallyID);
                        if (infoKpi_salein_actually.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionStore = {
                            state: 1,
                            status: 1
                        };
                        if (infoKpi_salein_actually.data.store) {

                            conditionStore._id = infoKpi_salein_actually.data.store._id;

                        }

                        let listStores = await STORE_MODEL
                            .find(conditionStore)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let listEmployees = await EMPLOYEE_MODEL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();

                        let listProducts = await PRODUCT_MODEL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoKpi_salein_actually: infoKpi_salein_actually.data || {},

                            listStores,
                            listEmployees,
                            listProducts,
                            CF_ROUTINGS_KPI_SALEIN_ACTUALLY
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_salein_actuallyID,
                            store,
                            employee,
                            SALE_ORDER_ID,
                            ORDER_NUMBER,
                            ORDER_DATE,
                            product,
                            QUANTITY,
                            IS_FREE_ITEM,
                        } = req.body;


                        const response = await KPI_SALEIN_ACTUALLY_MODEL.update({
                            kpi_salein_actuallyID,
                            store,
                            employee,
                            SALE_ORDER_ID,
                            ORDER_NUMBER,
                            ORDER_DATE,
                            product,
                            QUANTITY,
                            IS_FREE_ITEM,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update not require Kpi_salein_actually By Id (API)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.UPDATE_KPI_SALEIN_ACTUALLY_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_salein_actuallyID,
                            store,
                            employee,
                            SALE_ORDER_ID,
                            ORDER_NUMBER,
                            ORDER_DATE,
                            product,
                            QUANTITY,
                            IS_FREE_ITEM,
                        } = req.body;


                        const response = await KPI_SALEIN_ACTUALLY_MODEL.updateNotRequire({
                            kpi_salein_actuallyID,
                            store,
                            employee,
                            SALE_ORDER_ID,
                            ORDER_NUMBER,
                            ORDER_DATE,
                            product,
                            QUANTITY,
                            IS_FREE_ITEM,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_salein_actually By Id (API)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.DELETE_KPI_SALEIN_ACTUALLY_BY_ID]: {
                config: {
                    scopes: ['delete:kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            kpi_salein_actuallyID
                        } = req.params;

                        const response = await KPI_SALEIN_ACTUALLY_MODEL.deleteById(kpi_salein_actuallyID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_salein_actually By List Id (API)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.DELETE_KPI_SALEIN_ACTUALLY_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            kpi_salein_actuallyID
                        } = req.body;

                        const response = await KPI_SALEIN_ACTUALLY_MODEL.deleteByListId(kpi_salein_actuallyID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get Info Kpi_salein_actually By Id (API)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.GET_INFO_KPI_SALEIN_ACTUALLY_BY_ID]: {
                config: {
                    scopes: ['read:info_kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            kpi_salein_actuallyID
                        } = req.params;

                        const response = await KPI_SALEIN_ACTUALLY_MODEL.getInfoById(kpi_salein_actuallyID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_salein_actually (API, VIEW)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.GET_LIST_KPI_SALEIN_ACTUALLY]: {
                config: {
                    scopes: ['read:list_kpi_salein_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_salein_actually',
                    code: CF_ROUTINGS_KPI_SALEIN_ACTUALLY.GET_LIST_KPI_SALEIN_ACTUALLY,
                    inc: path.resolve(__dirname, '../views/kpi_salein_actually/list_kpi_salein_actuallys.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let { typeExistsIsMHTT } = req.query;
                        // let {
                        //     keyword,
                        //     ORDER_DATEDateRange,
                        //     QUANTITYFromNumber,
                        //     QUANTITYToNumber,
                        //     typeGetList
                        // } = req.query;

                        // let listKpi_salein_actuallys = [];
                        // if (typeGetList === 'FILTER') {
                        //     listKpi_salein_actuallys = await KPI_SALEIN_ACTUALLY_MODEL.getListByFilter({
                        //         keyword,
                        //         ORDER_DATEDateRange,
                        //         QUANTITYFromNumber,
                        //         QUANTITYToNumber,
                        //     });
                        // } else {
                        //     listKpi_salein_actuallys = await KPI_SALEIN_ACTUALLY_MODEL.getList();
                        // }

                        ChildRouter.renderToView(req, res, {
                            // listKpi_salein_actuallys: listKpi_salein_actuallys.data || [],
                            typeExistsIsMHTT, // bổ sung thêm field này để nhận biết đang truy cập SALEIN hay MHTT 
                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_salein_actually By Field (API, VIEW)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.GET_LIST_KPI_SALEIN_ACTUALLY_BY_FIELD]: {
                config: {
                    scopes: ['read:list_kpi_salein_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_salein_actually by field isStatus',
                    code: CF_ROUTINGS_KPI_SALEIN_ACTUALLY.GET_LIST_KPI_SALEIN_ACTUALLY_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/kpi_salein_actually/list_kpi_salein_actuallys.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            ORDER_DATEDateRange,
                            QUANTITYFromNumber,
                            QUANTITYToNumber,
                        } = req.query;

                        let listKpi_salein_actuallys = await KPI_SALEIN_ACTUALLY_MODEL.getListByFilter({
                            keyword,
                            ORDER_DATEDateRange,
                            QUANTITYFromNumber,
                            QUANTITYToNumber,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listKpi_salein_actuallys: listKpi_salein_actuallys.data || [],
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_salein_actually Server Side (API)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.GET_LIST_KPI_SALEIN_ACTUALLY_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let { typeExistsIsMHTT } = req.query;// bổ sung thêm field này để nhận biết đang truy cập SALEIN hay MHTT 
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;
                        const { region } = req.user;
                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const response = await KPI_SALEIN_ACTUALLY_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region,
                            typeExistsIsMHTT,// bổ sung thêm field này để nhận biết đang truy cập SALEIN hay MHTT 
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_salein_actually Import (API)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.GET_LIST_KPI_SALEIN_ACTUALLY_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const response = await KPI_SALEIN_ACTUALLY_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_salein_actually Excel Server Side (API)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.GET_LIST_KPI_SALEIN_ACTUALLY_EXCEL]: {
                config: {
                    scopes: ['read:list_kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = KPI_SALEIN_ACTUALLY_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let response = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Kpi_salein_actually Excel Export (API)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.DOWNLOAD_LIST_KPI_SALEIN_ACTUALLY_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'kpi_salein_actually'
                        });

                        let conditionObj = KPI_SALEIN_ACTUALLY_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let response = await KPI_SALEIN_ACTUALLY_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Setting Kpi_salein_actually Excel Import (API)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.SETTING_FILE_KPI_SALEIN_ACTUALLY_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = KPI_SALEIN_ACTUALLY_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let response = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Kpi_salein_actually Excel Import (API)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.DOWNLOAD_FILE_KPI_SALEIN_ACTUALLY_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            opts
                        } = req.query;
                        let condition = JSON.parse(opts);

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_salein_actually'
                        });

                        let listKpi_salein_actuallyImport = await KPI_SALEIN_ACTUALLY_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            opts: condition
                        });

                        res.download(listKpi_salein_actuallyImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listKpi_salein_actuallyImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Kpi_salein_actually Excel Import (API)
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.CREATE_KPI_SALEIN_ACTUALLY_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:kpi_salein_actually'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_salein_actually'
                        });

                        let response = await KPI_SALEIN_ACTUALLY_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'kpi_salein_actually',
                        });

                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Get info Kpi_salein_actually
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.API_GET_INFO_KPI_SALEIN_ACTUALLY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        const {
                            kpi_salein_actuallyID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const response = await KPI_SALEIN_ACTUALLY_MODEL.getInfoKpi_salein_actually({
                            kpi_salein_actuallyID,
                            select,
                            filter,
                            explain,
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Get list Kpi_salein_actually
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.API_GET_LIST_KPI_SALEIN_ACTUALLYS]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        const {
                            employeeID,
                            date
                        } = req.query;

                        const listKpi_salein_actuallys = await KPI_SALEIN_ACTUALLY_MODEL.getListKpi_salein_actuallys_v2({
                            employeeID,
                            date
                        });
                        res.json(listKpi_salein_actuallys);
                    }]
                },
            },

            /**
             * Function: API Insert Kpi_salein_actually
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.API_ADD_KPI_SALEIN_ACTUALLY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {} = req.body;


                        const response = await KPI_SALEIN_ACTUALLY_MODEL.insertKpi_salein_actually({
                            authorID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Update Kpi_salein_actually
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.API_UPDATE_KPI_SALEIN_ACTUALLY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let authorID = req.user && req.user._id;
                        let {
                            kpi_salein_actuallyID
                        } = req.params;
                        let {} = req.body;


                        let response = await KPI_SALEIN_ACTUALLY_MODEL.updateKpi_salein_actually({
                            kpi_salein_actuallyID,
                            authorID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Delete Kpi_salein_actually
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.API_DELETE_KPI_SALEIN_ACTUALLY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {

                        const {
                            kpi_salein_actuallyID
                        } = req.params;

                        const response = await KPI_SALEIN_ACTUALLY_MODEL.deleteKpi_salein_actually(kpi_salein_actuallyID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Delete Kpi_salein_actually
             * Date: 27/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SALEIN_ACTUALLY.API_DELETE_KPI_SALEIN_ACTUALLYS]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {

                        const {
                            kpi_salein_actuallysID
                        } = req.params;

                        const response = await KPI_SALEIN_ACTUALLY_MODEL.deleteKpi_salein_actuallys(kpi_salein_actuallysID);
                        res.json(response);
                    }]
                },
            },

        }
    }
};