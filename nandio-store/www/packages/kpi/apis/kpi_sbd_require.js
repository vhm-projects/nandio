/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {} = require('../constants/kpi_sbd_require');
const {
    CF_ROUTINGS_KPI_SBD_REQUIRE
} = require('../constants/kpi_sbd_require/kpi_sbd_require.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */

const {
    MODEL: KPI_SBD_REQUIRE_MODEL
} = require('../models/kpi_sbd_require');
const {
    MODEL: MANAGE_COLL_MODEL
} = require('../../../models/manage_coll');

const {
    MODEL: PRODUCT_MODEL
} = require('../../product/models/product');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ KPI_SBD_REQUIRE  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Kpi_sbd_require (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.ADD_KPI_SBD_REQUIRE]: {
                config: {
                    scopes: ['create:kpi_sbd_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Kpi_sbd_require',
                    code: CF_ROUTINGS_KPI_SBD_REQUIRE.ADD_KPI_SBD_REQUIRE,
                    inc: path.resolve(__dirname, '../views/kpi_sbd_require/add_kpi_sbd_require.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listProducts = await PRODUCT_MODEL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean();


                        ChildRouter.renderToView(req, res, {
                            listProducts,
                            CF_ROUTINGS_KPI_SBD_REQUIRE
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            product,
                            month_apply,
                            month,
                            year,
                        } = req.body;


                        let response = await KPI_SBD_REQUIRE_MODEL.insert({
                            product,
                            month_apply,
                            month,
                            year,
                            userCreate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update Kpi_sbd_require By Id (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.UPDATE_KPI_SBD_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:kpi_sbd_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Kpi_sbd_require',
                    code: CF_ROUTINGS_KPI_SBD_REQUIRE.UPDATE_KPI_SBD_REQUIRE_BY_ID,
                    inc: path.resolve(__dirname, '../views/kpi_sbd_require/update_kpi_sbd_require.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            kpi_sbd_requireID
                        } = req.query;

                        let infoKpi_sbd_require = await KPI_SBD_REQUIRE_MODEL.getInfoById(kpi_sbd_requireID);
                        if (infoKpi_sbd_require.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let listProducts = await PRODUCT_MODEL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoKpi_sbd_require: infoKpi_sbd_require.data || {},

                            listProducts,
                            CF_ROUTINGS_KPI_SBD_REQUIRE
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_sbd_requireID,
                            product,
                            month_apply,
                            month,
                            year,
                        } = req.body;


                        const response = await KPI_SBD_REQUIRE_MODEL.update({
                            kpi_sbd_requireID,
                            product,
                            month_apply,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update not require Kpi_sbd_require By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.UPDATE_KPI_SBD_REQUIRE_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:kpi_sbd_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_sbd_requireID,
                            product,
                            month_apply,
                            month,
                            year,
                        } = req.body;


                        const response = await KPI_SBD_REQUIRE_MODEL.updateNotRequire({
                            kpi_sbd_requireID,
                            product,
                            month_apply,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_sbd_require By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.DELETE_KPI_SBD_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['delete:kpi_sbd_require'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            kpi_sbd_requireID
                        } = req.params;

                        const response = await KPI_SBD_REQUIRE_MODEL.deleteById(kpi_sbd_requireID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_sbd_require By List Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.DELETE_KPI_SBD_REQUIRE_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:kpi_sbd_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            kpi_sbd_requireID
                        } = req.body;

                        const response = await KPI_SBD_REQUIRE_MODEL.deleteByListId(kpi_sbd_requireID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get Info Kpi_sbd_require By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.GET_INFO_KPI_SBD_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['read:info_kpi_sbd_require'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            kpi_sbd_requireID
                        } = req.params;

                        const response = await KPI_SBD_REQUIRE_MODEL.getInfoById(kpi_sbd_requireID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_sbd_require (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.GET_LIST_KPI_SBD_REQUIRE]: {
                config: {
                    scopes: ['read:list_kpi_sbd_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_sbd_require',
                    code: CF_ROUTINGS_KPI_SBD_REQUIRE.GET_LIST_KPI_SBD_REQUIRE,
                    inc: path.resolve(__dirname, '../views/kpi_sbd_require/list_kpi_sbd_requires.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            typeGetList
                        } = req.query;

                        let listKpi_sbd_requires = [];
                        if (typeGetList === 'FILTER') {
                            listKpi_sbd_requires = await KPI_SBD_REQUIRE_MODEL.getListByFilter({

                            });
                        } else {
                            listKpi_sbd_requires = await KPI_SBD_REQUIRE_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listKpi_sbd_requires: listKpi_sbd_requires.data || [],

                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_sbd_require By Field (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.GET_LIST_KPI_SBD_REQUIRE_BY_FIELD]: {
                config: {
                    scopes: ['read:list_kpi_sbd_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_sbd_require by field isStatus',
                    code: CF_ROUTINGS_KPI_SBD_REQUIRE.GET_LIST_KPI_SBD_REQUIRE_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/kpi_sbd_require/list_kpi_sbd_requires.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            type
                        } = req.query;

                        let listKpi_sbd_requires = await KPI_SBD_REQUIRE_MODEL.getListByFilter({
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listKpi_sbd_requires: listKpi_sbd_requires.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_sbd_require Server Side (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.GET_LIST_KPI_SBD_REQUIRE_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_kpi_sbd_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const response = await KPI_SBD_REQUIRE_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_sbd_require Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.GET_LIST_KPI_SBD_REQUIRE_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_sbd_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const response = await KPI_SBD_REQUIRE_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_sbd_require Excel Server Side (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.GET_LIST_KPI_SBD_REQUIRE_EXCEL]: {
                config: {
                    scopes: ['read:list_kpi_sbd_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = KPI_SBD_REQUIRE_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let response = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Kpi_sbd_require Excel Export (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.DOWNLOAD_LIST_KPI_SBD_REQUIRE_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_kpi_sbd_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'kpi_sbd_require'
                        });

                        let conditionObj = KPI_SBD_REQUIRE_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let response = await KPI_SBD_REQUIRE_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Setting Kpi_sbd_require Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.SETTING_FILE_KPI_SBD_REQUIRE_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_kpi_sbd_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = KPI_SBD_REQUIRE_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let response = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Kpi_sbd_require Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.DOWNLOAD_FILE_KPI_SBD_REQUIRE_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_sbd_require'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            opts
                        } = req.query;
                        let condition = JSON.parse(opts);

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_sbd_require'
                        });

                        let listKpi_sbd_requireImport = await KPI_SBD_REQUIRE_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            opts: condition
                        });

                        res.download(listKpi_sbd_requireImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listKpi_sbd_requireImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Kpi_sbd_require Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_REQUIRE.CREATE_KPI_SBD_REQUIRE_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:kpi_sbd_require'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_sbd_require'
                        });

                        let response = await KPI_SBD_REQUIRE_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'kpi_sbd_require',
                        });

                        res.json(response);
                    }]
                },
            },

        }
    }
};