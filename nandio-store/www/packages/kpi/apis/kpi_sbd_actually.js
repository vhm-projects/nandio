/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    ISCHECKED_KPI_SBD_ACTUALLY_TYPE,
} = require('../constants/kpi_sbd_actually');
const {
    CF_ROUTINGS_KPI_SBD_ACTUALLY
} = require('../constants/kpi_sbd_actually/kpi_sbd_actually.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */

const {
    MODEL: KPI_SBD_ACTUALLY_MODEL
} = require('../models/kpi_sbd_actually');
const {
    MODEL: MANAGE_COLL_MODEL
} = require('../../../models/manage_coll');

const {
    MODEL: REGION_MODEL
} = require('../../region_area/models/region');

const {
    MODEL: AREA_MODEL
} = require('../../region_area/models/area');

const {
    MODEL: DISTRIBUTOR_MODEL
} = require('../../distributor/models/distributor');

const {
    MODEL: EMPLOYEE_MODEL
} = require('../../employee/models/employee');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ KPI_SBD_ACTUALLY  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Kpi_sbd_actually (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.ADD_KPI_SBD_ACTUALLY]: {
                config: {
                    scopes: ['create:kpi_sbd_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Kpi_sbd_actually',
                    code: CF_ROUTINGS_KPI_SBD_ACTUALLY.ADD_KPI_SBD_ACTUALLY,
                    inc: path.resolve(__dirname, '../views/kpi_sbd_actually/add_kpi_sbd_actually.ejs')
                },
                methods: {
                    get: [async function(req, res) {


                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_KPI_SBD_ACTUALLY
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            employee,
                            amount,
                            isChecked,
                            month,
                            year,
                        } = req.body;


                        let response = await KPI_SBD_ACTUALLY_MODEL.insert({
                            employee,
                            amount,
                            isChecked,
                            month,
                            year,
                            userCreate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update Kpi_sbd_actually By Id (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.UPDATE_KPI_SBD_ACTUALLY_BY_ID]: {
                config: {
                    scopes: ['update:kpi_sbd_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Kpi_sbd_actually',
                    code: CF_ROUTINGS_KPI_SBD_ACTUALLY.UPDATE_KPI_SBD_ACTUALLY_BY_ID,
                    inc: path.resolve(__dirname, '../views/kpi_sbd_actually/update_kpi_sbd_actually.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            kpi_sbd_actuallyID
                        } = req.query;

                        let infoKpi_sbd_actually = await KPI_SBD_ACTUALLY_MODEL.getInfoById(kpi_sbd_actuallyID);
                        if (infoKpi_sbd_actually.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionEmployee = {
                            state: 1,
                            status: 1
                        };
                        if (infoKpi_sbd_actually.data.employee) {

                            conditionEmployee._id = infoKpi_sbd_actually.data.employee._id;

                        }

                        let listEmployees = await EMPLOYEE_MODEL
                            .find(conditionEmployee)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoKpi_sbd_actually: infoKpi_sbd_actually.data || {},

                            listEmployees,
                            CF_ROUTINGS_KPI_SBD_ACTUALLY
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_sbd_actuallyID,
                            employee,
                            amount,
                            isChecked,
                            month,
                            year,
                        } = req.body;


                        const response = await KPI_SBD_ACTUALLY_MODEL.update({
                            kpi_sbd_actuallyID,
                            employee,
                            amount,
                            isChecked,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update not require Kpi_sbd_actually By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.UPDATE_KPI_SBD_ACTUALLY_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:kpi_sbd_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_sbd_actuallyID,
                            employee,
                            amount,
                            isChecked,
                            month,
                            year,
                        } = req.body;


                        const response = await KPI_SBD_ACTUALLY_MODEL.updateNotRequire({
                            kpi_sbd_actuallyID,
                            employee,
                            amount,
                            isChecked,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_sbd_actually By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.DELETE_KPI_SBD_ACTUALLY_BY_ID]: {
                config: {
                    scopes: ['delete:kpi_sbd_actually'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            kpi_sbd_actuallyID
                        } = req.params;

                        const response = await KPI_SBD_ACTUALLY_MODEL.deleteById(kpi_sbd_actuallyID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_sbd_actually By List Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.DELETE_KPI_SBD_ACTUALLY_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:kpi_sbd_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            kpi_sbd_actuallyID
                        } = req.body;

                        const response = await KPI_SBD_ACTUALLY_MODEL.deleteByListId(kpi_sbd_actuallyID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get Info Kpi_sbd_actually By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.GET_INFO_KPI_SBD_ACTUALLY_BY_ID]: {
                config: {
                    scopes: ['read:info_kpi_sbd_actually'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            kpi_sbd_actuallyID
                        } = req.params;

                        const response = await KPI_SBD_ACTUALLY_MODEL.getInfoById(kpi_sbd_actuallyID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_sbd_actually (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.GET_LIST_KPI_SBD_ACTUALLY]: {
                config: {
                    scopes: ['read:list_kpi_sbd_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_sbd_actually',
                    code: CF_ROUTINGS_KPI_SBD_ACTUALLY.GET_LIST_KPI_SBD_ACTUALLY,
                    inc: path.resolve(__dirname, '../views/kpi_sbd_actually/list_kpi_sbd_actuallys.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            amountFromNumber,
                            amountToNumber,
                            isChecked,
                            typeGetList
                        } = req.query;

                        let listKpi_sbd_actuallys = [];
                        if (typeGetList === 'FILTER') {
                            listKpi_sbd_actuallys = await KPI_SBD_ACTUALLY_MODEL.getListByFilter({
                                amountFromNumber,
                                amountToNumber,
                                isChecked,
                            });
                        } else {
                            listKpi_sbd_actuallys = await KPI_SBD_ACTUALLY_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listKpi_sbd_actuallys: listKpi_sbd_actuallys.data || [],
                            ISCHECKED_KPI_SBD_ACTUALLY_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_sbd_actually By Field (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.GET_LIST_KPI_SBD_ACTUALLY_BY_FIELD]: {
                config: {
                    scopes: ['read:list_kpi_sbd_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_sbd_actually by field isStatus',
                    code: CF_ROUTINGS_KPI_SBD_ACTUALLY.GET_LIST_KPI_SBD_ACTUALLY_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/kpi_sbd_actually/list_kpi_sbd_actuallys.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            amountFromNumber,
                            amountToNumber,
                            isChecked,
                            type
                        } = req.query;

                        let listKpi_sbd_actuallys = await KPI_SBD_ACTUALLY_MODEL.getListByFilter({
                            amountFromNumber,
                            amountToNumber,
                            isChecked,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listKpi_sbd_actuallys: listKpi_sbd_actuallys.data || [],
                            ISCHECKED_KPI_SBD_ACTUALLY_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_sbd_actually Server Side (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.GET_LIST_KPI_SBD_ACTUALLY_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_kpi_sbd_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const response = await KPI_SBD_ACTUALLY_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_sbd_actually Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.GET_LIST_KPI_SBD_ACTUALLY_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_sbd_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const response = await KPI_SBD_ACTUALLY_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_sbd_actually Excel Server Side (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.GET_LIST_KPI_SBD_ACTUALLY_EXCEL]: {
                config: {
                    scopes: ['read:list_kpi_sbd_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = KPI_SBD_ACTUALLY_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let response = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Kpi_sbd_actually Excel Export (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.DOWNLOAD_LIST_KPI_SBD_ACTUALLY_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_kpi_sbd_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'kpi_sbd_actually'
                        });

                        let conditionObj = KPI_SBD_ACTUALLY_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let response = await KPI_SBD_ACTUALLY_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Setting Kpi_sbd_actually Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.SETTING_FILE_KPI_SBD_ACTUALLY_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_kpi_sbd_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = KPI_SBD_ACTUALLY_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let response = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Kpi_sbd_actually Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.DOWNLOAD_FILE_KPI_SBD_ACTUALLY_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_sbd_actually'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            opts
                        } = req.query;
                        let condition = JSON.parse(opts);

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_sbd_actually'
                        });

                        let listKpi_sbd_actuallyImport = await KPI_SBD_ACTUALLY_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            opts: condition
                        });

                        res.download(listKpi_sbd_actuallyImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listKpi_sbd_actuallyImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Kpi_sbd_actually Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.CREATE_KPI_SBD_ACTUALLY_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:kpi_sbd_actually'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_sbd_actually'
                        });

                        let response = await KPI_SBD_ACTUALLY_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'kpi_sbd_actually',
                        });

                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Get info Kpi_sbd_actually
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.API_GET_INFO_KPI_SBD_ACTUALLY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        const {
                            kpi_sbd_actuallyID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const response = await KPI_SBD_ACTUALLY_MODEL.getInfoKpi_sbd_actually({
                            kpi_sbd_actuallyID,
                            select,
                            filter,
                            explain,
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Get list Kpi_sbd_actually
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.API_GET_LIST_KPI_SBD_ACTUALLYS]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listKpi_sbd_actuallys = await KPI_SBD_ACTUALLY_MODEL.getListKpi_sbd_actuallys({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                        });
                        res.json(listKpi_sbd_actuallys);
                    }]
                },
            },

            /**
             * Function: API Insert Kpi_sbd_actually
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.API_ADD_KPI_SBD_ACTUALLY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {
                            region,
                            area,
                            distributor,
                            employee,
                            amount,
                            isChecked,
                            month,
                            year,
                        } = req.body;


                        const response = await KPI_SBD_ACTUALLY_MODEL.insertKpi_sbd_actually({
                            region,
                            area,
                            distributor,
                            employee,
                            amount,
                            isChecked,
                            month,
                            year,
                            authorID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Update Kpi_sbd_actually
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.API_UPDATE_KPI_SBD_ACTUALLY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let authorID = req.user && req.user._id;
                        let {
                            kpi_sbd_actuallyID
                        } = req.params;
                        let {
                            region,
                            area,
                            distributor,
                            employee,
                            amount,
                            isChecked,
                            month,
                            year,
                        } = req.body;


                        let response = await KPI_SBD_ACTUALLY_MODEL.updateKpi_sbd_actually({
                            kpi_sbd_actuallyID,
                            region,
                            area,
                            distributor,
                            employee,
                            amount,
                            isChecked,
                            month,
                            year,
                            authorID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Delete Kpi_sbd_actually
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.API_DELETE_KPI_SBD_ACTUALLY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {

                        const {
                            kpi_sbd_actuallyID
                        } = req.params;

                        const response = await KPI_SBD_ACTUALLY_MODEL.deleteKpi_sbd_actually(kpi_sbd_actuallyID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Delete Kpi_sbd_actually
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_SBD_ACTUALLY.API_DELETE_KPI_SBD_ACTUALLYS]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {

                        const {
                            kpi_sbd_actuallysID
                        } = req.params;

                        const response = await KPI_SBD_ACTUALLY_MODEL.deleteKpi_sbd_actuallys(kpi_sbd_actuallysID);
                        res.json(response);
                    }]
                },
            },

        }
    }
};