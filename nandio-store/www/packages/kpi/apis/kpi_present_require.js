/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {} = require('../constants/kpi_present_require');
const {
    CF_ROUTINGS_KPI_PRESENT_REQUIRE
} = require('../constants/kpi_present_require/kpi_present_require.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */

const {
    MODEL: KPI_PRESENT_REQUIRE_MODEL
} = require('../models/kpi_present_require');
const {
    MODEL: MANAGE_COLL_MODEL
} = require('../../../models/manage_coll');

const {
    MODEL: REGION_MODEL
} = require('../../region_area/models/region');

const {
    MODEL: AREA_MODEL
} = require('../../region_area/models/area');

const {
    MODEL: DISTRIBUTOR_MODEL
} = require('../../distributor/models/distributor');

const {
    MODEL: EMPLOYEE_MODEL
} = require('../../employee/models/employee');

const {
    MODEL: PRODUCT_MODEL
} = require('../../product/models/product');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ KPI_PRESENT_REQUIRE  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Kpi_present_require (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.ADD_KPI_PRESENT_REQUIRE]: {
                config: {
                    scopes: ['create:kpi_present_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Kpi_present_require',
                    code: CF_ROUTINGS_KPI_PRESENT_REQUIRE.ADD_KPI_PRESENT_REQUIRE,
                    inc: path.resolve(__dirname, '../views/kpi_present_require/add_kpi_present_require.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listProducts = await PRODUCT_MODEL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean();


                        ChildRouter.renderToView(req, res, {
                            listProducts,
                            CF_ROUTINGS_KPI_PRESENT_REQUIRE
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            employee,
                            amount,
                            month,
                            year,
                            product,
                        } = req.body;


                        let response = await KPI_PRESENT_REQUIRE_MODEL.insert({
                            employee,
                            amount,
                            month,
                            year,
                            product,
                            userCreate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update Kpi_present_require By Id (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.UPDATE_KPI_PRESENT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:kpi_present_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Kpi_present_require',
                    code: CF_ROUTINGS_KPI_PRESENT_REQUIRE.UPDATE_KPI_PRESENT_REQUIRE_BY_ID,
                    inc: path.resolve(__dirname, '../views/kpi_present_require/update_kpi_present_require.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            kpi_present_requireID
                        } = req.query;

                        let infoKpi_present_require = await KPI_PRESENT_REQUIRE_MODEL.getInfoById(kpi_present_requireID);
                        if (infoKpi_present_require.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionEmployee = {
                            state: 1,
                            status: 1
                        };
                        if (infoKpi_present_require.data.employee) {

                            conditionEmployee._id = infoKpi_present_require.data.employee._id;

                        }

                        let listEmployees = await EMPLOYEE_MODEL
                            .find(conditionEmployee)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let listProducts = await PRODUCT_MODEL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoKpi_present_require: infoKpi_present_require.data || {},

                            listEmployees,
                            listProducts,
                            CF_ROUTINGS_KPI_PRESENT_REQUIRE
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_present_requireID,
                            employee,
                            amount,
                            month,
                            year,
                            product,
                        } = req.body;


                        const response = await KPI_PRESENT_REQUIRE_MODEL.update({
                            kpi_present_requireID,
                            employee,
                            amount,
                            month,
                            year,
                            product,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update not require Kpi_present_require By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.UPDATE_KPI_PRESENT_REQUIRE_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:kpi_present_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_present_requireID,
                            employee,
                            amount,
                            month,
                            year,
                            product,
                        } = req.body;


                        const response = await KPI_PRESENT_REQUIRE_MODEL.updateNotRequire({
                            kpi_present_requireID,
                            employee,
                            amount,
                            month,
                            year,
                            product,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_present_require By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.DELETE_KPI_PRESENT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['delete:kpi_present_require'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            kpi_present_requireID
                        } = req.params;

                        const response = await KPI_PRESENT_REQUIRE_MODEL.deleteById(kpi_present_requireID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_present_require By List Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.DELETE_KPI_PRESENT_REQUIRE_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:kpi_present_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            kpi_present_requireID
                        } = req.body;

                        const response = await KPI_PRESENT_REQUIRE_MODEL.deleteByListId(kpi_present_requireID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get Info Kpi_present_require By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.GET_INFO_KPI_PRESENT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['read:info_kpi_present_require'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            kpi_present_requireID
                        } = req.params;

                        const response = await KPI_PRESENT_REQUIRE_MODEL.getInfoById(kpi_present_requireID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_present_require (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.GET_LIST_KPI_PRESENT_REQUIRE]: {
                config: {
                    scopes: ['read:list_kpi_present_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_present_require',
                    code: CF_ROUTINGS_KPI_PRESENT_REQUIRE.GET_LIST_KPI_PRESENT_REQUIRE,
                    inc: path.resolve(__dirname, '../views/kpi_present_require/list_kpi_present_requires.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            typeGetList
                        } = req.query;
                        let { region } = req.user;
                        let listKpi_present_requires = [];
                        if (typeGetList === 'FILTER') {
                            listKpi_present_requires = await KPI_PRESENT_REQUIRE_MODEL.getListByFilter({
                                region
                            });
                        } else {
                            listKpi_present_requires = await KPI_PRESENT_REQUIRE_MODEL.getList({ region });
                        }

                        ChildRouter.renderToView(req, res, {
                            listKpi_present_requires: listKpi_present_requires.data || [],

                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_present_require By Field (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.GET_LIST_KPI_PRESENT_REQUIRE_BY_FIELD]: {
                config: {
                    scopes: ['read:list_kpi_present_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_present_require by field isStatus',
                    code: CF_ROUTINGS_KPI_PRESENT_REQUIRE.GET_LIST_KPI_PRESENT_REQUIRE_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/kpi_present_require/list_kpi_present_requires.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            type
                        } = req.query;

                        let listKpi_present_requires = await KPI_PRESENT_REQUIRE_MODEL.getListByFilter({
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listKpi_present_requires: listKpi_present_requires.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_present_require Server Side (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.GET_LIST_KPI_PRESENT_REQUIRE_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_kpi_present_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;
                        const { region } = req.user;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const response = await KPI_PRESENT_REQUIRE_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_present_require Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.GET_LIST_KPI_PRESENT_REQUIRE_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_present_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const response = await KPI_PRESENT_REQUIRE_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_present_require Excel Server Side (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.GET_LIST_KPI_PRESENT_REQUIRE_EXCEL]: {
                config: {
                    scopes: ['read:list_kpi_present_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = KPI_PRESENT_REQUIRE_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let response = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Kpi_present_require Excel Export (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.DOWNLOAD_LIST_KPI_PRESENT_REQUIRE_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_kpi_present_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'kpi_present_require'
                        });

                        let conditionObj = KPI_PRESENT_REQUIRE_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let response = await KPI_PRESENT_REQUIRE_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Setting Kpi_present_require Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.SETTING_FILE_KPI_PRESENT_REQUIRE_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_kpi_present_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = KPI_PRESENT_REQUIRE_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let response = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Kpi_present_require Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.DOWNLOAD_FILE_KPI_PRESENT_REQUIRE_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_present_require'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            opts
                        } = req.query;
                        let condition = JSON.parse(opts);

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_present_require'
                        });

                        let listKpi_present_requireImport = await KPI_PRESENT_REQUIRE_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            opts: condition
                        });

                        res.download(listKpi_present_requireImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listKpi_present_requireImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Kpi_present_require Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.CREATE_KPI_PRESENT_REQUIRE_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:kpi_present_require'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_present_require'
                        });

                        let response = await KPI_PRESENT_REQUIRE_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'kpi_present_require',
                        });

                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Get info Kpi_present_require
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.API_GET_INFO_KPI_PRESENT_REQUIRE]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        const {
                            kpi_present_requireID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const response = await KPI_PRESENT_REQUIRE_MODEL.getInfoKpi_present_require({
                            kpi_present_requireID,
                            select,
                            filter,
                            explain,
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Get list Kpi_present_require
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.API_GET_LIST_KPI_PRESENT_REQUIRES]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listKpi_present_requires = await KPI_PRESENT_REQUIRE_MODEL.getListKpi_present_requires({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                        });
                        res.json(listKpi_present_requires);
                    }]
                },
            },

            /**
             * Function: API Insert Kpi_present_require
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.API_ADD_KPI_PRESENT_REQUIRE]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {} = req.body;


                        const response = await KPI_PRESENT_REQUIRE_MODEL.insertKpi_present_require({
                            authorID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Update Kpi_present_require
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.API_UPDATE_KPI_PRESENT_REQUIRE]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let authorID = req.user && req.user._id;
                        let {
                            kpi_present_requireID
                        } = req.params;
                        let {} = req.body;


                        let response = await KPI_PRESENT_REQUIRE_MODEL.updateKpi_present_require({
                            kpi_present_requireID,
                            authorID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Delete Kpi_present_require
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.API_DELETE_KPI_PRESENT_REQUIRE]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {

                        const {
                            kpi_present_requireID
                        } = req.params;

                        const response = await KPI_PRESENT_REQUIRE_MODEL.deleteKpi_present_require(kpi_present_requireID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Delete Kpi_present_require
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_REQUIRE.API_DELETE_KPI_PRESENT_REQUIRES]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {

                        const {
                            kpi_present_requiresID
                        } = req.params;

                        const response = await KPI_PRESENT_REQUIRE_MODEL.deleteKpi_present_requires(kpi_present_requiresID);
                        res.json(response);
                    }]
                },
            },

        }
    }
};