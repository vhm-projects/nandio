/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const beautifyer = require('js-beautify').js_beautify;
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {} = require('../constants/kpi_mhtt_require');
const {
    CF_ROUTINGS_KPI_MHTT_REQUIRE
} = require('../constants/kpi_mhtt_require/kpi_mhtt_require.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const KPI_MHTT_REQUIRE_MODEL = require('../models/kpi_mhtt_require').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const {
    EMPLOYEE_COLL
} = require('../../employee');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ KPI_MHTT_REQUIRE  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Kpi_mhtt_require (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.ADD_KPI_MHTT_REQUIRE]: {
                config: {
                    scopes: ['create:kpi_mhtt_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Kpi_mhtt_require',
                    code: CF_ROUTINGS_KPI_MHTT_REQUIRE.ADD_KPI_MHTT_REQUIRE,
                    inc: path.resolve(__dirname, '../views/kpi_mhtt_require/add_kpi_mhtt_require.ejs')
                },
                methods: {
                    get: [async function(req, res) {


                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_KPI_MHTT_REQUIRE
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            amount,
                            employee,
                            year,
                        } = req.body;


                        let infoAfterInsert = await KPI_MHTT_REQUIRE_MODEL.insert({
                            amount,
                            employee,
                            year,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Kpi_mhtt_require By Id (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.UPDATE_KPI_MHTT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:kpi_mhtt_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Kpi_mhtt_require',
                    code: CF_ROUTINGS_KPI_MHTT_REQUIRE.UPDATE_KPI_MHTT_REQUIRE_BY_ID,
                    inc: path.resolve(__dirname, '../views/kpi_mhtt_require/update_kpi_mhtt_require.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            kpi_mhtt_requireID
                        } = req.query;

                        let infoKpi_mhtt_require = await KPI_MHTT_REQUIRE_MODEL.getInfoById(kpi_mhtt_requireID);
                        if (infoKpi_mhtt_require.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionEmployee = {
                            state: 1,
                            status: 1
                        };
                        if (infoKpi_mhtt_require.data?.employee) {

                            conditionEmployee._id = infoKpi_mhtt_require.data.employee?._id;

                        }

                        let listEmployees = await EMPLOYEE_COLL
                            .find(conditionEmployee)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoKpi_mhtt_require: infoKpi_mhtt_require.data || {},

                            listEmployees,
                            CF_ROUTINGS_KPI_MHTT_REQUIRE
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_mhtt_requireID,
                            amount,
                            employee,
                            month,
                            year,
                        } = req.body;


                        const infoAfterUpdate = await KPI_MHTT_REQUIRE_MODEL.update({
                            kpi_mhtt_requireID,
                            amount,
                            employee,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Kpi_mhtt_require By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.UPDATE_KPI_MHTT_REQUIRE_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:kpi_mhtt_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_mhtt_requireID,
                            amount,
                            employee,
                            month,
                            year,
                        } = req.body;


                        const infoAfterUpdate = await KPI_MHTT_REQUIRE_MODEL.updateNotRequire({
                            kpi_mhtt_requireID,
                            amount,
                            employee,
                            month,
                            year,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_mhtt_require By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.DELETE_KPI_MHTT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['delete:kpi_mhtt_require'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            kpi_mhtt_requireID
                        } = req.params;

                        const infoAfterDelete = await KPI_MHTT_REQUIRE_MODEL.deleteById(kpi_mhtt_requireID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_mhtt_require By List Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.DELETE_KPI_MHTT_REQUIRE_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:kpi_mhtt_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            kpi_mhtt_requireID
                        } = req.body;

                        const infoAfterDelete = await KPI_MHTT_REQUIRE_MODEL.deleteByListId(kpi_mhtt_requireID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Kpi_mhtt_require By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.GET_INFO_KPI_MHTT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['read:info_kpi_mhtt_require'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            kpi_mhtt_requireID
                        } = req.params;

                        const infoKpi_mhtt_requireById = await KPI_MHTT_REQUIRE_MODEL.getInfoById(kpi_mhtt_requireID);
                        res.json(infoKpi_mhtt_requireById);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_mhtt_require (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.GET_LIST_KPI_MHTT_REQUIRE]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_mhtt_require',
                    code: CF_ROUTINGS_KPI_MHTT_REQUIRE.GET_LIST_KPI_MHTT_REQUIRE,
                    inc: path.resolve(__dirname, '../views/kpi_mhtt_require/list_kpi_mhtt_requires.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            amountFromNumber,
                            amountToNumber,
                            typeGetList
                        } = req.query;

                        let listKpi_mhtt_requires = [];
                        if (typeGetList === 'FILTER') {
                            listKpi_mhtt_requires = await KPI_MHTT_REQUIRE_MODEL.getListByFilter({
                                amountFromNumber,
                                amountToNumber,
                            });
                        } else {
                            listKpi_mhtt_requires = await KPI_MHTT_REQUIRE_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listKpi_mhtt_requires: listKpi_mhtt_requires.data || [],

                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_mhtt_require By Field (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.GET_LIST_KPI_MHTT_REQUIRE_BY_FIELD]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_require'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_mhtt_require by field isStatus',
                    code: CF_ROUTINGS_KPI_MHTT_REQUIRE.GET_LIST_KPI_MHTT_REQUIRE_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/kpi_mhtt_require/list_kpi_mhtt_requires.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            amountFromNumber,
                            amountToNumber,
                            type
                        } = req.query;

                        let listKpi_mhtt_requires = await KPI_MHTT_REQUIRE_MODEL.getListByFilter({
                            amountFromNumber,
                            amountToNumber,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listKpi_mhtt_requires: listKpi_mhtt_requires.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_mhtt_require Server Side (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.GET_LIST_KPI_MHTT_REQUIRE_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listKpi_mhtt_requireServerSide = await KPI_MHTT_REQUIRE_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listKpi_mhtt_requireServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_mhtt_require Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.GET_LIST_KPI_MHTT_REQUIRE_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listKpi_mhtt_requireImport = await KPI_MHTT_REQUIRE_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listKpi_mhtt_requireImport);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_mhtt_require Excel Server Side (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.GET_LIST_KPI_MHTT_REQUIRE_EXCEL]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = KPI_MHTT_REQUIRE_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Kpi_mhtt_require Excel Export (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.DOWNLOAD_LIST_KPI_MHTT_REQUIRE_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'kpi_mhtt_require'
                        });

                        let conditionObj = KPI_MHTT_REQUIRE_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listKpi_mhtt_require = await KPI_MHTT_REQUIRE_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listKpi_mhtt_require)
                    }]
                },
            },

            /**
             * Function: Setting Kpi_mhtt_require Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.SETTING_FILE_KPI_MHTT_REQUIRE_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_require'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = KPI_MHTT_REQUIRE_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Kpi_mhtt_require Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.DOWNLOAD_FILE_KPI_MHTT_REQUIRE_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_mhtt_require'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_mhtt_require'
                        });

                        let listKpi_mhtt_requireImport = await KPI_MHTT_REQUIRE_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listKpi_mhtt_requireImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listKpi_mhtt_requireImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Kpi_mhtt_require Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_MHTT_REQUIRE.CREATE_KPI_MHTT_REQUIRE_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:kpi_mhtt_require'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_mhtt_require'
                        });

                        let infoKpi_mhtt_requireAfterImport = await KPI_MHTT_REQUIRE_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'kpi_mhtt_require',
                        });

                        res.json(infoKpi_mhtt_requireAfterImport);
                    }]
                },
            },

        }
    }
};