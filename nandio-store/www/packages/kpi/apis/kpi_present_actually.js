/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {} = require('../constants/kpi_present_actually');
const {
    CF_ROUTINGS_KPI_PRESENT_ACTUALLY
} = require('../constants/kpi_present_actually/kpi_present_actually.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */

const {
    MODEL: KPI_PRESENT_ACTUALLY_MODEL
} = require('../models/kpi_present_actually');
const {
    MODEL: MANAGE_COLL_MODEL
} = require('../../../models/manage_coll');

const {
    MODEL: REGION_MODEL
} = require('../../region_area/models/region');

const {
    MODEL: AREA_MODEL
} = require('../../region_area/models/area');

const {
    MODEL: DISTRIBUTOR_MODEL
} = require('../../distributor/models/distributor');

const {
    MODEL: EMPLOYEE_MODEL
} = require('../../employee/models/employee');

const {
    MODEL: PRODUCT_MODEL
} = require('../../product/models/product');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ KPI_PRESENT_ACTUALLY  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Kpi_present_actually (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.ADD_KPI_PRESENT_ACTUALLY]: {
                config: {
                    scopes: ['create:kpi_present_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Kpi_present_actually',
                    code: CF_ROUTINGS_KPI_PRESENT_ACTUALLY.ADD_KPI_PRESENT_ACTUALLY,
                    inc: path.resolve(__dirname, '../views/kpi_present_actually/add_kpi_present_actually.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listProducts = await PRODUCT_MODEL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean();


                        ChildRouter.renderToView(req, res, {
                            listProducts,
                            CF_ROUTINGS_KPI_PRESENT_ACTUALLY
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            employee,
                            amount,
                            month,
                            year,
                            product,
                        } = req.body;


                        let response = await KPI_PRESENT_ACTUALLY_MODEL.insert({
                            employee,
                            amount,
                            month,
                            year,
                            product,
                            userCreate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update Kpi_present_actually By Id (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.UPDATE_KPI_PRESENT_ACTUALLY_BY_ID]: {
                config: {
                    scopes: ['update:kpi_present_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Kpi_present_actually',
                    code: CF_ROUTINGS_KPI_PRESENT_ACTUALLY.UPDATE_KPI_PRESENT_ACTUALLY_BY_ID,
                    inc: path.resolve(__dirname, '../views/kpi_present_actually/update_kpi_present_actually.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            kpi_present_actuallyID
                        } = req.query;

                        let infoKpi_present_actually = await KPI_PRESENT_ACTUALLY_MODEL.getInfoById(kpi_present_actuallyID);
                        if (infoKpi_present_actually.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionEmployee = {
                            state: 1,
                            status: 1
                        };
                        if (infoKpi_present_actually.data.employee) {

                            conditionEmployee._id = infoKpi_present_actually.data.employee._id;

                        }

                        let listEmployees = await EMPLOYEE_MODEL
                            .find(conditionEmployee)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let listProducts = await PRODUCT_MODEL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoKpi_present_actually: infoKpi_present_actually.data || {},

                            listEmployees,
                            listProducts,
                            CF_ROUTINGS_KPI_PRESENT_ACTUALLY
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_present_actuallyID,
                            employee,
                            amount,
                            month,
                            year,
                            product,
                        } = req.body;


                        const response = await KPI_PRESENT_ACTUALLY_MODEL.update({
                            kpi_present_actuallyID,
                            employee,
                            amount,
                            month,
                            year,
                            product,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update not require Kpi_present_actually By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.UPDATE_KPI_PRESENT_ACTUALLY_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:kpi_present_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            kpi_present_actuallyID,
                            employee,
                            amount,
                            month,
                            year,
                            product,
                        } = req.body;


                        const response = await KPI_PRESENT_ACTUALLY_MODEL.updateNotRequire({
                            kpi_present_actuallyID,
                            employee,
                            amount,
                            month,
                            year,
                            product,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_present_actually By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.DELETE_KPI_PRESENT_ACTUALLY_BY_ID]: {
                config: {
                    scopes: ['delete:kpi_present_actually'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            kpi_present_actuallyID
                        } = req.params;

                        const response = await KPI_PRESENT_ACTUALLY_MODEL.deleteById(kpi_present_actuallyID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Kpi_present_actually By List Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.DELETE_KPI_PRESENT_ACTUALLY_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:kpi_present_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            kpi_present_actuallyID
                        } = req.body;

                        const response = await KPI_PRESENT_ACTUALLY_MODEL.deleteByListId(kpi_present_actuallyID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get Info Kpi_present_actually By Id (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.GET_INFO_KPI_PRESENT_ACTUALLY_BY_ID]: {
                config: {
                    scopes: ['read:info_kpi_present_actually'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            kpi_present_actuallyID
                        } = req.params;

                        const response = await KPI_PRESENT_ACTUALLY_MODEL.getInfoById(kpi_present_actuallyID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_present_actually (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.GET_LIST_KPI_PRESENT_ACTUALLY]: {
                config: {
                    scopes: ['read:list_kpi_present_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_present_actually',
                    code: CF_ROUTINGS_KPI_PRESENT_ACTUALLY.GET_LIST_KPI_PRESENT_ACTUALLY,
                    inc: path.resolve(__dirname, '../views/kpi_present_actually/list_kpi_present_actuallys.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            typeGetList
                        } = req.query;
                        let { region } = req.user;
                        let listKpi_present_actuallys = [];
                        if (typeGetList === 'FILTER') {
                            listKpi_present_actuallys = await KPI_PRESENT_ACTUALLY_MODEL.getListByFilter({
                                region
                            });
                        } else {
                            listKpi_present_actuallys = await KPI_PRESENT_ACTUALLY_MODEL.getList({ region });
                        }

                        ChildRouter.renderToView(req, res, {
                            listKpi_present_actuallys: listKpi_present_actuallys.data || [],

                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_present_actually By Field (API, VIEW)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.GET_LIST_KPI_PRESENT_ACTUALLY_BY_FIELD]: {
                config: {
                    scopes: ['read:list_kpi_present_actually'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Kpi_present_actually by field isStatus',
                    code: CF_ROUTINGS_KPI_PRESENT_ACTUALLY.GET_LIST_KPI_PRESENT_ACTUALLY_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/kpi_present_actually/list_kpi_present_actuallys.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            type
                        } = req.query;

                        let listKpi_present_actuallys = await KPI_PRESENT_ACTUALLY_MODEL.getListByFilter({
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listKpi_present_actuallys: listKpi_present_actuallys.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Kpi_present_actually Server Side (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.GET_LIST_KPI_PRESENT_ACTUALLY_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_kpi_present_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;
                        const { region } = req.user;
                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const response = await KPI_PRESENT_ACTUALLY_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_present_actually Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.GET_LIST_KPI_PRESENT_ACTUALLY_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_present_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const response = await KPI_PRESENT_ACTUALLY_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Kpi_present_actually Excel Server Side (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.GET_LIST_KPI_PRESENT_ACTUALLY_EXCEL]: {
                config: {
                    scopes: ['read:list_kpi_present_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = KPI_PRESENT_ACTUALLY_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let response = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Kpi_present_actually Excel Export (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.DOWNLOAD_LIST_KPI_PRESENT_ACTUALLY_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_kpi_present_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'kpi_present_actually'
                        });

                        let conditionObj = KPI_PRESENT_ACTUALLY_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let response = await KPI_PRESENT_ACTUALLY_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Setting Kpi_present_actually Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.SETTING_FILE_KPI_PRESENT_ACTUALLY_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_kpi_present_actually'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = KPI_PRESENT_ACTUALLY_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let response = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Kpi_present_actually Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.DOWNLOAD_FILE_KPI_PRESENT_ACTUALLY_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_kpi_present_actually'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            opts
                        } = req.query;
                        let condition = JSON.parse(opts);

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_present_actually'
                        });

                        let listKpi_present_actuallyImport = await KPI_PRESENT_ACTUALLY_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            opts: condition
                        });

                        res.download(listKpi_present_actuallyImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listKpi_present_actuallyImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Kpi_present_actually Excel Import (API)
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.CREATE_KPI_PRESENT_ACTUALLY_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:kpi_present_actually'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'kpi_present_actually'
                        });

                        let response = await KPI_PRESENT_ACTUALLY_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'kpi_present_actually',
                        });

                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Get info Kpi_present_actually
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.API_GET_INFO_KPI_PRESENT_ACTUALLY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        const {
                            kpi_present_actuallyID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const response = await KPI_PRESENT_ACTUALLY_MODEL.getInfoKpi_present_actually({
                            kpi_present_actuallyID,
                            select,
                            filter,
                            explain,
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Get list Kpi_present_actually
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.API_GET_LIST_KPI_PRESENT_ACTUALLYS]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listKpi_present_actuallys = await KPI_PRESENT_ACTUALLY_MODEL.getListKpi_present_actuallys({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                        });
                        res.json(listKpi_present_actuallys);
                    }]
                },
            },

            /**
             * Function: API Insert Kpi_present_actually
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.API_ADD_KPI_PRESENT_ACTUALLY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {} = req.body;


                        const response = await KPI_PRESENT_ACTUALLY_MODEL.insertKpi_present_actually({
                            authorID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Update Kpi_present_actually
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.API_UPDATE_KPI_PRESENT_ACTUALLY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let authorID = req.user && req.user._id;
                        let {
                            kpi_present_actuallyID
                        } = req.params;
                        let {} = req.body;


                        let response = await KPI_PRESENT_ACTUALLY_MODEL.updateKpi_present_actually({
                            kpi_present_actuallyID,
                            authorID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Delete Kpi_present_actually
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.API_DELETE_KPI_PRESENT_ACTUALLY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {

                        const {
                            kpi_present_actuallyID
                        } = req.params;

                        const response = await KPI_PRESENT_ACTUALLY_MODEL.deleteKpi_present_actually(kpi_present_actuallyID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Delete Kpi_present_actually
             * Date: 21/02/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_KPI_PRESENT_ACTUALLY.API_DELETE_KPI_PRESENT_ACTUALLYS]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {

                        const {
                            kpi_present_actuallysID
                        } = req.params;

                        const response = await KPI_PRESENT_ACTUALLY_MODEL.deleteKpi_present_actuallys(kpi_present_actuallysID);
                        res.json(response);
                    }]
                },
            },

        }
    }
};