const BASE_ROUTE = '/kpi_mhtt_config_products';
const API_BASE_ROUTE = '/api/kpi_mhtt_config_products';

const CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS = {
    ADD_KPI_MHTT_CONFIG_PRODUCTS: `${BASE_ROUTE}/add-kpi_mhtt_config_products`,
    ADD_MANUAL_KPI_MHTT_CONFIG_PRODUCTS: `${API_BASE_ROUTE}/add-manual-kpi_mhtt_config_products`,
    UPDATE_KPI_MHTT_CONFIG_PRODUCTS_BY_ID: `${BASE_ROUTE}/update-kpi_mhtt_config_products-by-id`,
    DELETE_KPI_MHTT_CONFIG_PRODUCTS_BY_ID: `${BASE_ROUTE}/delete/:kpi_mhtt_config_productsID`,

    GET_INFO_KPI_MHTT_CONFIG_PRODUCTS_BY_ID: `${BASE_ROUTE}/info/:kpi_mhtt_config_productsID`,
    GET_LIST_KPI_MHTT_CONFIG_PRODUCTS: `${BASE_ROUTE}/list-kpi_mhtt_config_products`,
    GET_LIST_KPI_MHTT_CONFIG_PRODUCTS_BY_FIELD: `${BASE_ROUTE}/list-kpi_mhtt_config_products/:field/:value`,
    GET_LIST_KPI_MHTT_CONFIG_PRODUCTS_SERVER_SIDE: `${BASE_ROUTE}/list-kpi_mhtt_config_products-server-side`,

    UPDATE_KPI_MHTT_CONFIG_PRODUCTS_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-kpi_mhtt_config_products-by-id-v2`,
    DELETE_KPI_MHTT_CONFIG_PRODUCTS_BY_LIST_ID: `${BASE_ROUTE}/delete-kpi_mhtt_config_products-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_KPI_MHTT_CONFIG_PRODUCTS_EXCEL: `${BASE_ROUTE}/list-kpi_mhtt_config_products-excel`,
    DOWNLOAD_LIST_KPI_MHTT_CONFIG_PRODUCTS_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-kpi_mhtt_config_products-excel-export`,

    // IMPORT EXCEL
    GET_LIST_KPI_MHTT_CONFIG_PRODUCTS_IMPORT: `${BASE_ROUTE}/list-kpi_mhtt_config_products-import`,
    SETTING_FILE_KPI_MHTT_CONFIG_PRODUCTS_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-kpi_mhtt_config_products-import-setting`,
    DOWNLOAD_FILE_KPI_MHTT_CONFIG_PRODUCTS_EXCEL_IMPORT: `${BASE_ROUTE}/list-kpi_mhtt_config_products-import-dowload`,
    CREATE_KPI_MHTT_CONFIG_PRODUCTS_IMPORT_EXCEL: `${BASE_ROUTE}/create-kpi_mhtt_config_products-import-excel`,



    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS = CF_ROUTINGS_KPI_MHTT_CONFIG_PRODUCTS;