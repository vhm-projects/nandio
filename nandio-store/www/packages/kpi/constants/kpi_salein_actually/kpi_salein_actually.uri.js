const BASE_ROUTE = '/kpi_salein_actually';
const API_BASE_ROUTE = '/api/kpi_salein_actually';

const CF_ROUTINGS_KPI_SALEIN_ACTUALLY = {
    ADD_KPI_SALEIN_ACTUALLY: `${BASE_ROUTE}/add-kpi_salein_actually`,
    UPDATE_KPI_SALEIN_ACTUALLY_BY_ID: `${BASE_ROUTE}/update-kpi_salein_actually-by-id`,
    DELETE_KPI_SALEIN_ACTUALLY_BY_ID: `${BASE_ROUTE}/delete/:kpi_salein_actuallyID`,

    GET_INFO_KPI_SALEIN_ACTUALLY_BY_ID: `${BASE_ROUTE}/info/:kpi_salein_actuallyID`,
    GET_LIST_KPI_SALEIN_ACTUALLY: `${BASE_ROUTE}/list-kpi_salein_actually`,
    GET_LIST_KPI_SALEIN_ACTUALLY_BY_FIELD: `${BASE_ROUTE}/list-kpi_salein_actually/:field/:value`,
    GET_LIST_KPI_SALEIN_ACTUALLY_SERVER_SIDE: `${BASE_ROUTE}/list-kpi_salein_actually-server-side`,

    UPDATE_KPI_SALEIN_ACTUALLY_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-kpi_salein_actually-by-id-v2`,
    DELETE_KPI_SALEIN_ACTUALLY_BY_LIST_ID: `${BASE_ROUTE}/delete-kpi_salein_actually-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_KPI_SALEIN_ACTUALLY_EXCEL: `${BASE_ROUTE}/list-kpi_salein_actually-excel`,
    DOWNLOAD_LIST_KPI_SALEIN_ACTUALLY_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-kpi_salein_actually-excel-export`,

    // IMPORT EXCEL
    GET_LIST_KPI_SALEIN_ACTUALLY_IMPORT: `${BASE_ROUTE}/list-kpi_salein_actually-import`,
    SETTING_FILE_KPI_SALEIN_ACTUALLY_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-kpi_salein_actually-import-setting`,
    DOWNLOAD_FILE_KPI_SALEIN_ACTUALLY_EXCEL_IMPORT: `${BASE_ROUTE}/list-kpi_salein_actually-import-dowload`,
    CREATE_KPI_SALEIN_ACTUALLY_IMPORT_EXCEL: `${BASE_ROUTE}/create-kpi_salein_actually-import-excel`,

    API_GET_INFO_KPI_SALEIN_ACTUALLY: `${API_BASE_ROUTE}/info-kpi_salein_actually/:kpi_salein_actuallyID`,
    API_GET_LIST_KPI_SALEIN_ACTUALLYS: `${API_BASE_ROUTE}/list-kpi_salein_actuallys`,
    API_ADD_KPI_SALEIN_ACTUALLY: `${API_BASE_ROUTE}/add-kpi_salein_actually`,
    API_UPDATE_KPI_SALEIN_ACTUALLY: `${API_BASE_ROUTE}/update-kpi_salein_actually/:kpi_salein_actuallyID`,
    API_DELETE_KPI_SALEIN_ACTUALLY: `${API_BASE_ROUTE}/delete-kpi_salein_actually/:kpi_salein_actuallyID`,
    API_DELETE_KPI_SALEIN_ACTUALLYS: `${API_BASE_ROUTE}/delete-kpi_salein_actuallys/:kpi_salein_actuallysID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_KPI_SALEIN_ACTUALLY = CF_ROUTINGS_KPI_SALEIN_ACTUALLY;