const BASE_ROUTE = '/kpi_present_require';
const API_BASE_ROUTE = '/api/kpi_present_require';

const CF_ROUTINGS_KPI_PRESENT_REQUIRE = {
    ADD_KPI_PRESENT_REQUIRE: `${BASE_ROUTE}/add-kpi_present_require`,
    UPDATE_KPI_PRESENT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-kpi_present_require-by-id`,
    DELETE_KPI_PRESENT_REQUIRE_BY_ID: `${BASE_ROUTE}/delete/:kpi_present_requireID`,

    GET_INFO_KPI_PRESENT_REQUIRE_BY_ID: `${BASE_ROUTE}/info/:kpi_present_requireID`,
    GET_LIST_KPI_PRESENT_REQUIRE: `${BASE_ROUTE}/list-kpi_present_require`,
    GET_LIST_KPI_PRESENT_REQUIRE_BY_FIELD: `${BASE_ROUTE}/list-kpi_present_require/:field/:value`,
    GET_LIST_KPI_PRESENT_REQUIRE_SERVER_SIDE: `${BASE_ROUTE}/list-kpi_present_require-server-side`,

    UPDATE_KPI_PRESENT_REQUIRE_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-kpi_present_require-by-id-v2`,
    DELETE_KPI_PRESENT_REQUIRE_BY_LIST_ID: `${BASE_ROUTE}/delete-kpi_present_require-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_KPI_PRESENT_REQUIRE_EXCEL: `${BASE_ROUTE}/list-kpi_present_require-excel`,
    DOWNLOAD_LIST_KPI_PRESENT_REQUIRE_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-kpi_present_require-excel-export`,

    // IMPORT EXCEL
    GET_LIST_KPI_PRESENT_REQUIRE_IMPORT: `${BASE_ROUTE}/list-kpi_present_require-import`,
    SETTING_FILE_KPI_PRESENT_REQUIRE_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-kpi_present_require-import-setting`,
    DOWNLOAD_FILE_KPI_PRESENT_REQUIRE_EXCEL_IMPORT: `${BASE_ROUTE}/list-kpi_present_require-import-dowload`,
    CREATE_KPI_PRESENT_REQUIRE_IMPORT_EXCEL: `${BASE_ROUTE}/create-kpi_present_require-import-excel`,

    API_GET_INFO_KPI_PRESENT_REQUIRE: `${API_BASE_ROUTE}/info-kpi_present_require/:kpi_present_requireID`,
    API_GET_LIST_KPI_PRESENT_REQUIRES: `${API_BASE_ROUTE}/list-kpi_present_requires`,
    API_ADD_KPI_PRESENT_REQUIRE: `${API_BASE_ROUTE}/add-kpi_present_require`,
    API_UPDATE_KPI_PRESENT_REQUIRE: `${API_BASE_ROUTE}/update-kpi_present_require/:kpi_present_requireID`,
    API_DELETE_KPI_PRESENT_REQUIRE: `${API_BASE_ROUTE}/delete-kpi_present_require/:kpi_present_requireID`,
    API_DELETE_KPI_PRESENT_REQUIRES: `${API_BASE_ROUTE}/delete-kpi_present_requires/:kpi_present_requiresID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_KPI_PRESENT_REQUIRE = CF_ROUTINGS_KPI_PRESENT_REQUIRE;