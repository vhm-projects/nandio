const BASE_ROUTE = '/kpi_present_actually';
const API_BASE_ROUTE = '/api/kpi_present_actually';

const CF_ROUTINGS_KPI_PRESENT_ACTUALLY = {
    ADD_KPI_PRESENT_ACTUALLY: `${BASE_ROUTE}/add-kpi_present_actually`,
    UPDATE_KPI_PRESENT_ACTUALLY_BY_ID: `${BASE_ROUTE}/update-kpi_present_actually-by-id`,
    DELETE_KPI_PRESENT_ACTUALLY_BY_ID: `${BASE_ROUTE}/delete/:kpi_present_actuallyID`,

    GET_INFO_KPI_PRESENT_ACTUALLY_BY_ID: `${BASE_ROUTE}/info/:kpi_present_actuallyID`,
    GET_LIST_KPI_PRESENT_ACTUALLY: `${BASE_ROUTE}/list-kpi_present_actually`,
    GET_LIST_KPI_PRESENT_ACTUALLY_BY_FIELD: `${BASE_ROUTE}/list-kpi_present_actually/:field/:value`,
    GET_LIST_KPI_PRESENT_ACTUALLY_SERVER_SIDE: `${BASE_ROUTE}/list-kpi_present_actually-server-side`,

    UPDATE_KPI_PRESENT_ACTUALLY_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-kpi_present_actually-by-id-v2`,
    DELETE_KPI_PRESENT_ACTUALLY_BY_LIST_ID: `${BASE_ROUTE}/delete-kpi_present_actually-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_KPI_PRESENT_ACTUALLY_EXCEL: `${BASE_ROUTE}/list-kpi_present_actually-excel`,
    DOWNLOAD_LIST_KPI_PRESENT_ACTUALLY_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-kpi_present_actually-excel-export`,

    // IMPORT EXCEL
    GET_LIST_KPI_PRESENT_ACTUALLY_IMPORT: `${BASE_ROUTE}/list-kpi_present_actually-import`,
    SETTING_FILE_KPI_PRESENT_ACTUALLY_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-kpi_present_actually-import-setting`,
    DOWNLOAD_FILE_KPI_PRESENT_ACTUALLY_EXCEL_IMPORT: `${BASE_ROUTE}/list-kpi_present_actually-import-dowload`,
    CREATE_KPI_PRESENT_ACTUALLY_IMPORT_EXCEL: `${BASE_ROUTE}/create-kpi_present_actually-import-excel`,

    API_GET_INFO_KPI_PRESENT_ACTUALLY: `${API_BASE_ROUTE}/info-kpi_present_actually/:kpi_present_actuallyID`,
    API_GET_LIST_KPI_PRESENT_ACTUALLYS: `${API_BASE_ROUTE}/list-kpi_present_actuallys`,
    API_ADD_KPI_PRESENT_ACTUALLY: `${API_BASE_ROUTE}/add-kpi_present_actually`,
    API_UPDATE_KPI_PRESENT_ACTUALLY: `${API_BASE_ROUTE}/update-kpi_present_actually/:kpi_present_actuallyID`,
    API_DELETE_KPI_PRESENT_ACTUALLY: `${API_BASE_ROUTE}/delete-kpi_present_actually/:kpi_present_actuallyID`,
    API_DELETE_KPI_PRESENT_ACTUALLYS: `${API_BASE_ROUTE}/delete-kpi_present_actuallys/:kpi_present_actuallysID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_KPI_PRESENT_ACTUALLY = CF_ROUTINGS_KPI_PRESENT_ACTUALLY;