/**
 * Đã tạo thành công 
 * 1: Đã Tạo trong tháng
 * 2: Chưa được tạo
 */
exports.ISCHECKED_KPI_SBD_ACTUALLY_TYPE = {

    "1": {
        value: "Đã Tạo trong tháng",
        color: "#0b51b7"
    },

    "2": {
        value: "Chưa được tạo",
        color: "#d63031"
    },

};