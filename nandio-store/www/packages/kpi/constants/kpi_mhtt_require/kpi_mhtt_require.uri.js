const BASE_ROUTE = '/kpi_mhtt_require';
const API_BASE_ROUTE = '/api/kpi_mhtt_require';

const CF_ROUTINGS_KPI_MHTT_REQUIRE = {
    ADD_KPI_MHTT_REQUIRE: `${BASE_ROUTE}/add-kpi_mhtt_require`,
    UPDATE_KPI_MHTT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-kpi_mhtt_require-by-id`,
    DELETE_KPI_MHTT_REQUIRE_BY_ID: `${BASE_ROUTE}/delete/:kpi_mhtt_requireID`,

    GET_INFO_KPI_MHTT_REQUIRE_BY_ID: `${BASE_ROUTE}/info/:kpi_mhtt_requireID`,
    GET_LIST_KPI_MHTT_REQUIRE: `${BASE_ROUTE}/list-kpi_mhtt_require`,
    GET_LIST_KPI_MHTT_REQUIRE_BY_FIELD: `${BASE_ROUTE}/list-kpi_mhtt_require/:field/:value`,
    GET_LIST_KPI_MHTT_REQUIRE_SERVER_SIDE: `${BASE_ROUTE}/list-kpi_mhtt_require-server-side`,

    UPDATE_KPI_MHTT_REQUIRE_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-kpi_mhtt_require-by-id-v2`,
    DELETE_KPI_MHTT_REQUIRE_BY_LIST_ID: `${BASE_ROUTE}/delete-kpi_mhtt_require-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_KPI_MHTT_REQUIRE_EXCEL: `${BASE_ROUTE}/list-kpi_mhtt_require-excel`,
    DOWNLOAD_LIST_KPI_MHTT_REQUIRE_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-kpi_mhtt_require-excel-export`,

    // IMPORT EXCEL
    GET_LIST_KPI_MHTT_REQUIRE_IMPORT: `${BASE_ROUTE}/list-kpi_mhtt_require-import`,
    SETTING_FILE_KPI_MHTT_REQUIRE_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-kpi_mhtt_require-import-setting`,
    DOWNLOAD_FILE_KPI_MHTT_REQUIRE_EXCEL_IMPORT: `${BASE_ROUTE}/list-kpi_mhtt_require-import-dowload`,
    CREATE_KPI_MHTT_REQUIRE_IMPORT_EXCEL: `${BASE_ROUTE}/create-kpi_mhtt_require-import-excel`,



    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_KPI_MHTT_REQUIRE = CF_ROUTINGS_KPI_MHTT_REQUIRE;