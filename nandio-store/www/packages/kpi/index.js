const KPI_SALEIN_REQUIRE_COLL = require('./databases/kpi_salein_require-coll');
const KPI_SALEIN_REQUIRE_MODEL = require('./models/kpi_salein_require').MODEL;
const KPI_SALEIN_REQUIRE_ROUTES = require('./apis/kpi_salein_require');

const KPI_SALEIN_ACTUALLY_COLL  = require('./databases/kpi_salein_actually-coll');
const KPI_SALEIN_ACTUALLY_MODEL  = require('./models/kpi_salein_actually').MODEL;
const KPI_SALEIN_ACTUALLY_ROUTES  = require('./apis/kpi_salein_actually');
    

const KPI_PRESENT_REQUIRE_COLL  = require('./databases/kpi_present_require-coll');
const KPI_PRESENT_REQUIRE_MODEL  = require('./models/kpi_present_require').MODEL;
const KPI_PRESENT_REQUIRE_ROUTES  = require('./apis/kpi_present_require');
    

const KPI_PRESENT_ACTUALLY_COLL  = require('./databases/kpi_present_actually-coll');
const KPI_PRESENT_ACTUALLY_MODEL  = require('./models/kpi_present_actually').MODEL;
const KPI_PRESENT_ACTUALLY_ROUTES  = require('./apis/kpi_present_actually');
    

const KPI_SBD_REQUIRE_COLL  = require('./databases/kpi_sbd_require-coll');
const KPI_SBD_REQUIRE_MODEL  = require('./models/kpi_sbd_require').MODEL;
const KPI_SBD_REQUIRE_ROUTES  = require('./apis/kpi_sbd_require');
    

const KPI_SBD_ACTUALLY_COLL  = require('./databases/kpi_sbd_actually-coll');
const KPI_SBD_ACTUALLY_MODEL  = require('./models/kpi_sbd_actually').MODEL;
const KPI_SBD_ACTUALLY_ROUTES  = require('./apis/kpi_sbd_actually');
    

const KPI_MHTT_REQUIRE_COLL  = require('./databases/kpi_mhtt_require-coll');
const KPI_MHTT_REQUIRE_MODEL  = require('./models/kpi_mhtt_require').MODEL;
const KPI_MHTT_REQUIRE_ROUTES  = require('./apis/kpi_mhtt_require');
    

const KPI_MHTT_CONFIG_PRODUCTS_COLL  = require('./databases/kpi_mhtt_config_products-coll');
const KPI_MHTT_CONFIG_PRODUCTS_MODEL  = require('./models/kpi_mhtt_config_products').MODEL;
const KPI_MHTT_CONFIG_PRODUCTS_ROUTES  = require('./apis/kpi_mhtt_config_products');
    
// MARK REQUIRE

module.exports = {
    KPI_SALEIN_REQUIRE_COLL,
    KPI_SALEIN_REQUIRE_MODEL,
    KPI_SALEIN_REQUIRE_ROUTES,

    KPI_SALEIN_ACTUALLY_COLL,
    KPI_SALEIN_ACTUALLY_MODEL,
    KPI_SALEIN_ACTUALLY_ROUTES,
    

    KPI_PRESENT_REQUIRE_COLL,
    KPI_PRESENT_REQUIRE_MODEL,
    KPI_PRESENT_REQUIRE_ROUTES,
    

    KPI_PRESENT_ACTUALLY_COLL,
    KPI_PRESENT_ACTUALLY_MODEL,
    KPI_PRESENT_ACTUALLY_ROUTES,
    

    KPI_SBD_REQUIRE_COLL,
    KPI_SBD_REQUIRE_MODEL,
    KPI_SBD_REQUIRE_ROUTES,
    

    KPI_SBD_ACTUALLY_COLL,
    KPI_SBD_ACTUALLY_MODEL,
    KPI_SBD_ACTUALLY_ROUTES,
    

    KPI_MHTT_CONFIG_PRODUCTS_COLL,
    KPI_MHTT_CONFIG_PRODUCTS_MODEL,
    KPI_MHTT_CONFIG_PRODUCTS_ROUTES,
    
    // MARK EXPORT
}