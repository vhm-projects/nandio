const CHECKIN_CHECKOUT_COLL = require('./databases/checkin_checkout-coll');
const CHECKIN_CHECKOUT_MODEL = require('./models/checkin_checkout').MODEL;
const CHECKIN_CHECKOUT_ROUTES = require('./apis/checkin_checkout');
// MARK REQUIRE

module.exports = {
    CHECKIN_CHECKOUT_COLL,
    CHECKIN_CHECKOUT_MODEL,
    CHECKIN_CHECKOUT_ROUTES,
    // MARK EXPORT
}