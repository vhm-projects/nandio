"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    ERROR_LOG_CHECKIN_CHECKOUT_TYPE,
    STATUS_CHECKIN_CHECKOUT_TYPE,
    PLATFORM_CHECKIN_CHECKOUT_TYPE,
} = require('../constants/checkin_checkout');
const {
    CF_ROUTINGS_CHECKIN_CHECKOUT
} = require('../constants/checkin_checkout/checkin_checkout.uri');

/**
 * MODELS
 */
const CHECKIN_CHECKOUT_MODEL = require('../models/checkin_checkout').MODEL;
const IMAGE_MODEL = require('../../image/models/image').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;
const EMPLOYEE_MODEL = require('../../employee/models/employee').MODEL;

/**
 * COLLECTIONS
 */

const EMPLOYEE_COLL = require('../../employee/databases/employee-coll');

const {
    STORE_COLL
} = require('../../store');

const {
    SHIFT_COLL
} = require('../../core');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ CHECKIN_CHECKOUT  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Checkin_checkout (API, VIEW)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.ADD_CHECKIN_CHECKOUT]: {
                config: {
                    scopes: ['create:checkin_checkout'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Checkin_checkout',
                    code: CF_ROUTINGS_CHECKIN_CHECKOUT.ADD_CHECKIN_CHECKOUT,
                    inc: path.resolve(__dirname, '../views/checkin_checkout/add_checkin_checkout.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listEmployees = await EMPLOYEE_COLL.find({
                            state: 1,
                            status: 1
                        }).lean()

                        let listStores = await STORE_COLL.find({
                            state: 1,
                            status: 1
                        }).limit(100).lean();

                        let listShifts = await SHIFT_COLL.find({
                                state: 1,
                                status: 1
                            }).lean()
                            .sort({
                                modifyAt: -1
                            })
                        ChildRouter.renderToView(req, res, {
                            listEmployees,
                            listStores,
                            listShifts,
                            CF_ROUTINGS_CHECKIN_CHECKOUT
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            employee,
                            store,
                            shift,
                            faceImages,
                            storeImages,
                            note,
                            status,
                            deviceName,
                            deviceID,
                            platform,
                        } = req.body;

                        if (faceImages && faceImages.length) {
                            let listFiles = faceImages.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))
                            listFiles = await Promise.all(listFiles);

                            faceImages = listFiles.map(file => file.data._id);
                        }

                        if (storeImages && storeImages.length) {
                            let listFiles = storeImages.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))
                            listFiles = await Promise.all(listFiles);

                            storeImages = listFiles.map(file => file.data._id);
                        }

                        let infoAfterInsert = await CHECKIN_CHECKOUT_MODEL.insert({
                            employee,
                            store,
                            shift,
                            faceImages,
                            storeImages,
                            note,
                            status,
                            deviceName,
                            deviceID,
                            platform,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Checkin_checkout By Id (API, VIEW)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.UPDATE_CHECKIN_CHECKOUT_BY_ID]: {
                config: {
                    scopes: ['update:checkin_checkout'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật chấm công',
                    code: CF_ROUTINGS_CHECKIN_CHECKOUT.UPDATE_CHECKIN_CHECKOUT_BY_ID,
                    inc: path.resolve(__dirname, '../views/checkin_checkout/update_checkin_checkout.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            checkin_checkoutID
                        } = req.query;

                        let infoCheckin_checkout = await CHECKIN_CHECKOUT_MODEL.getInfoById(checkin_checkoutID);
                        if (infoCheckin_checkout.error) {
                            return res.redirect('/something-went-wrong');
                        }

                        let listEmployees = await EMPLOYEE_COLL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean()

                        let listStores = await STORE_COLL.find({
                            _id: infoCheckin_checkout.data.store._id,
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).limit(50).lean()

                        let listShifts = await SHIFT_COLL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean()

                        ChildRouter.renderToView(req, res, {
                            infoCheckin_checkout: infoCheckin_checkout.data || {},
                            listEmployees,
                            listStores,
                            listShifts,
                            CF_ROUTINGS_CHECKIN_CHECKOUT
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            checkin_checkoutID,
                            note,
                            status,
                        } = req.body;

                        const infoAfterUpdate = await CHECKIN_CHECKOUT_MODEL.update({
                            checkin_checkoutID,
                            note,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Checkin_checkout By Id (API)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.UPDATE_CHECKIN_CHECKOUT_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            checkin_checkoutID,
                            employee,
                            store,
                            shift,
                            faceImages,
                            storeImages,
                            location,
                            logs,
                            note,
                            error_log,
                            status,
                            deviceName,
                            deviceID,
                            platform,
                        } = req.body;


                        if (faceImages && faceImages.length) {
                            let listFiles = faceImages.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))
                            listFiles = await Promise.all(listFiles);

                            faceImages = listFiles.map(file => file.data._id);
                        }

                        if (storeImages && storeImages.length) {
                            let listFiles = storeImages.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))
                            listFiles = await Promise.all(listFiles);

                            storeImages = listFiles.map(file => file.data._id);
                        }

                        const infoAfterUpdate = await CHECKIN_CHECKOUT_MODEL.updateNotRequire({
                            checkin_checkoutID,
                            employee,
                            store,
                            shift,
                            faceImages,
                            storeImages,
                            location,
                            logs,
                            note,
                            error_log,
                            status,
                            deviceName,
                            deviceID,
                            platform,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Checkin_checkout By Id (API)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.DELETE_CHECKIN_CHECKOUT_BY_ID]: {
                config: {
                    scopes: ['delete:checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            checkin_checkoutID
                        } = req.params;

                        const infoAfterDelete = await CHECKIN_CHECKOUT_MODEL.deleteById(checkin_checkoutID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Checkin_checkout By List Id (API)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.DELETE_CHECKIN_CHECKOUT_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            checkin_checkoutID
                        } = req.body;

                        const infoAfterDelete = await CHECKIN_CHECKOUT_MODEL.deleteByListId(checkin_checkoutID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Checkin_checkout By Id (API)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.GET_INFO_CHECKIN_CHECKOUT_BY_ID]: {
                config: {
                    scopes: ['read:info_checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            checkin_checkoutID
                        } = req.params;

                        const infoCheckin_checkoutById = await CHECKIN_CHECKOUT_MODEL.getInfoById(checkin_checkoutID);
                        res.json(infoCheckin_checkoutById);
                    }]
                },
            },

            /**
             * Function: Get List Checkin_checkout (API, VIEW)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.GET_LIST_CHECKIN_CHECKOUT]: {
                config: {
                    scopes: ['read:list_checkin_checkout'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách chấm công',
                    code: CF_ROUTINGS_CHECKIN_CHECKOUT.GET_LIST_CHECKIN_CHECKOUT,
                    inc: path.resolve(__dirname, '../views/checkin_checkout/list_checkin_checkouts.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            error_log,
                            status,
                            typeGetList
                        } = req.query;

                        let listCheckin_checkouts = [];
                        if (typeGetList === 'FILTER') {
                            listCheckin_checkouts = await CHECKIN_CHECKOUT_MODEL.getListByFilter({
                                keyword,
                                error_log,
                                status,
                            });
                        } else {
                            listCheckin_checkouts = await CHECKIN_CHECKOUT_MODEL.getList();
                        }
                        
                        ChildRouter.renderToView(req, res, {
                            listCheckin_checkouts: listCheckin_checkouts.data || [],
                            ERROR_LOG_CHECKIN_CHECKOUT_TYPE,
                            STATUS_CHECKIN_CHECKOUT_TYPE,
                            PLATFORM_CHECKIN_CHECKOUT_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Checkin_checkout By Field (API, VIEW)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.GET_LIST_CHECKIN_CHECKOUT_BY_FIELD]: {
                config: {
                    scopes: ['read:list_checkin_checkout'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách chấm công',
                    code: CF_ROUTINGS_CHECKIN_CHECKOUT.GET_LIST_CHECKIN_CHECKOUT_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/checkin_checkout/list_checkin_checkouts.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            error_log,
                            status,
                        } = req.query;

                        let listCheckin_checkouts = await CHECKIN_CHECKOUT_MODEL.getListByFilter({
                            keyword,
                            error_log,
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listCheckin_checkouts: listCheckin_checkouts.data || [],
                            ERROR_LOG_CHECKIN_CHECKOUT_TYPE,
                            STATUS_CHECKIN_CHECKOUT_TYPE,
                            PLATFORM_CHECKIN_CHECKOUT_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Checkin_checkout Server Side (API)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.GET_LIST_CHECKIN_CHECKOUT_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let { region } = req.user;
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order,
                            showIndex,
                            employee,
                            store
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listCheckin_checkoutServerSide = await CHECKIN_CHECKOUT_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir,
                            region,
                            showIndex,
                            employee,
                            store
                        });
                        res.json(listCheckin_checkoutServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Checkin_checkout Excel Server Side (API)
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.GET_LIST_CHECKIN_CHECKOUT_EXCEL]: {
                config: {
                    scopes: ['read:list_checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = CHECKIN_CHECKOUT_MODEL.getConditionArrayFilterExcel(listItemExport)
                       
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Employee Excel Export (API)
             * Date: 22/11/2021
             * Dev: Automatic
             */
             [CF_ROUTINGS_CHECKIN_CHECKOUT.DOWNLOAD_LIST_CHECKIN_CHECKOUT_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'checkin_checkout'
                        });
                       
                        let conditionObj = CHECKIN_CHECKOUT_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listCheckinCheckout = await CHECKIN_CHECKOUT_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listCheckinCheckout)
                    }]
                },
            },

            /**
             * Function: Thêm
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.API_ADD_CHECKIN_CHECKOUT]: {
                config: {
                    scopes: ['create:checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            employee,
                            store,
                            shift,
                            faceImages,
                            storeImages,
                            location,
                            logs,
                            note,
                            error_log,
                            status,
                            deviceName,
                            deviceID,
                            platform,
                        } = req.body;

                        if (faceImages && faceImages.length) {

                            let listFiles = faceImages.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))
                            listFiles = await Promise.all(listFiles);

                            faceImages = listFiles.map(file => file.data._id);
                        }

                        if (storeImages && storeImages.length) {

                            let listFiles = storeImages.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))
                            listFiles = await Promise.all(listFiles);

                            storeImages = listFiles.map(file => file.data._id);
                        }

                        let infoAfterInsert = await CHECKIN_CHECKOUT_MODEL.insertCheckin_checkout({
                            employee,
                            store,
                            shift,
                            faceImages,
                            storeImages,
                            location,
                            logs,
                            note,
                            error_log,
                            status,
                            deviceName,
                            deviceID,
                            platform,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Xóa
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.API_DELETE_CHECKIN_CHECKOUT]: {
                config: {
                    scopes: ['delete:checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            checkin_checkoutID
                        } = req.params;

                        const infoAfterDelete = await CHECKIN_CHECKOUT_MODEL.deleteCheckin_checkout(checkin_checkoutID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Sửa
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.API_UPDATE_CHECKIN_CHECKOUT]: {
                config: {
                    scopes: ['update:checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            checkin_checkoutID
                        } = req.params;
                        let {
                            employee,
                            store,
                            shift,
                            faceImages,
                            storeImages,
                            location,
                            logs,
                            note,
                            error_log,
                            status,
                            deviceName,
                            deviceID,
                            platform,
                        } = req.body;


                        if (faceImages && faceImages.length) {

                            let listFiles = faceImages.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))
                            listFiles = await Promise.all(listFiles);

                            faceImages = listFiles.map(file => file.data._id);
                        }

                        if (storeImages && storeImages.length) {

                            let listFiles = storeImages.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))
                            listFiles = await Promise.all(listFiles);

                            storeImages = listFiles.map(file => file.data._id);
                        }

                        let infoAfterUpdate = await CHECKIN_CHECKOUT_MODEL.updateCheckin_checkout({
                            checkin_checkoutID,
                            employee,
                            store,
                            shift,
                            faceImages,
                            storeImages,
                            location,
                            logs,
                            note,
                            error_log,
                            status,
                            deviceName,
                            deviceID,
                            platform,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.API_GET_INFO_CHECKIN_CHECKOUT]: {
                config: {
                    scopes: ['read:info_checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoCheckin_checkout = await CHECKIN_CHECKOUT_MODEL.getInfoCheckin_checkout({
                            select,
                            filter,
                            explain
                        });
                        res.json(infoCheckin_checkout);
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 31/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.API_GET_LIST_CHECKIN_CHECKOUTS]: {
                config: {
                    scopes: ['read:list_checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listCheckin_checkouts = await CHECKIN_CHECKOUT_MODEL.getListCheckin_checkouts({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listCheckin_checkouts);
                    }]
                },
            },

            /**
             * Function: Checkin
             * Date: 31/10/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_CHECKIN_CHECKOUT.API_ADD_CHECKIN]: {
                config: {
                    scopes: ['create:checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const { _id: userCreate } = req.user;
                        let {
                            employeeID,
                            storeID,
                            shiftID,
                            lng,
                            lat,
                            deviceName,
                            deviceID,
                            platform,
                            timeStartShift,
                        } = req.body;

                        const infoAfterCheckIn = await CHECKIN_CHECKOUT_MODEL.insertCheckIn({
                            employeeID,
                            storeID,
                            shiftID,
                            lng,
                            lat,
                            deviceName,
                            deviceID,
                            platform,
                            timeStartShift,
                            userCreate
                        });
                        res.json(infoAfterCheckIn);
                    }]
                },
            },

            /**
             * Function: Update face Checkin
             * Date: 31/10/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.API_UPDATE_FACE_CHECKIN]: {
                config: {
                    scopes: ['create:checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    put: [ async function(req, res) {
                        let { _id: userUpdate } = req.user;
                        let {
                            checkinID,
                            faceImages,
                        } = req.body;

                        if (faceImages && faceImages.length) {
                            let listFiles = faceImages.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))

                            listFiles = await Promise.all(listFiles);
                            faceImages = listFiles.map(file => file.data._id);
                        }

                        const infoAfterCheckIn = await CHECKIN_CHECKOUT_MODEL.updateFaceCheckIn({
                            checkinID,
                            faceImages,
                            userUpdate
                        });

                        res.json(infoAfterCheckIn);
                    }]
                },
            },

            /**
             * Function: Update store Check in
             * Date: 31/10/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.API_UPDATE_STORE_CHECKIN]: {
                config: {
                    scopes: ['create:checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    put: [ async function(req, res) {
                        let { _id: userUpdate } = req.user;
                        let {
                            checkinID,
                            storeImages,
                        } = req.body;

                        if (storeImages && storeImages.length) {
                            let listFiles = storeImages.map(item => IMAGE_MODEL.insert({
                                name: item.name,
                                path: item.path,
                                size: item.size
                            }))

                            listFiles = await Promise.all(listFiles);
                            storeImages = listFiles.map(file => file.data._id);
                        }

                        const infoAfterCheckIn = await CHECKIN_CHECKOUT_MODEL.updateStoreCheckIn({
                            checkinID,
                            storeImages,
                            userUpdate
                        });

                        res.json(infoAfterCheckIn);
                    }]
                },
            },

            /**
             * Function: Checkout
             * Date: 31/10/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_CHECKIN_CHECKOUT.API_ADD_CHECKOUT]: {
                config: {
                    scopes: ['create:checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const { _id: userUpdate } = req.user;
                        const {
                            checkinID,
                            lng,
                            lat,
                            timeEndShift,
                            timeCheckin,
                        } = req.body;

                        const infoAfterCheckOut = await CHECKIN_CHECKOUT_MODEL.insertCheckout({
                            checkinID,
                            lng,
                            lat,
                            timeEndShift,
                            timeCheckin,
                            userUpdate
                        });
                        res.json(infoAfterCheckOut);
                    }]
                },
            },

            /**
             * Function: Check is Checkin
             * Date: 16/01/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_CHECKIN_CHECKOUT.API_CHECK_CHECKIN]: {
                config: {
                    scopes: ['read:info_checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const { _id: employeeID } = req.user;

                        let response = await EMPLOYEE_MODEL.getInfo__CheckIn_Vs_Employee__ByEmployeeID({ employeeID });

                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Download checkin checkout excel
             * Date: 16/01/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_CHECKIN_CHECKOUT.DOWNLOAD_CHECKIN_CHECKOUT_EXCEL]: {
                config: {
                    scopes: ['read:list_checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { monthAndYear, opt } = req.query;
                        opt = JSON.parse(opt);
                        
                        let response = await CHECKIN_CHECKOUT_MODEL.downloadCheckinCheckoutExcel({ 
                           ...opt, monthAndYear
                        });

                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Download general checkin checkout excel
             * Date: 16/01/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_CHECKIN_CHECKOUT.DOWNLOAD_GENERAL_CHECKIN_CHECKOUT_EXCEL]: {
                config: {
                    scopes: ['read:list_checkin_checkout'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        let { monthAndYear, opt } = req.query;
                        opt = JSON.parse(opt);
                        
                        let response = await CHECKIN_CHECKOUT_MODEL.downloadGeneralCheckinCheckoutExcel({ 
                           ...opt, monthAndYear
                        });

                        res.json(response);
                    }]
                },
            },

        }
    }
};