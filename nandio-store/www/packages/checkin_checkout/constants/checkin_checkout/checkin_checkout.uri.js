const BASE_ROUTE = '/checkin_checkout';
const API_BASE_ROUTE = '/api/checkin_checkout';

const CF_ROUTINGS_CHECKIN_CHECKOUT = {
    ADD_CHECKIN_CHECKOUT: `${BASE_ROUTE}/add-checkin_checkout`,
    UPDATE_CHECKIN_CHECKOUT_BY_ID: `${BASE_ROUTE}/update-checkin_checkout-by-id`,
    DELETE_CHECKIN_CHECKOUT_BY_ID: `${BASE_ROUTE}/delete/:checkin_checkoutID`,

    GET_INFO_CHECKIN_CHECKOUT_BY_ID: `${BASE_ROUTE}/info/:checkin_checkoutID`,
    GET_LIST_CHECKIN_CHECKOUT: `${BASE_ROUTE}/list-checkin_checkout`,
    GET_LIST_CHECKIN_CHECKOUT_BY_FIELD: `${BASE_ROUTE}/list-checkin_checkout/:field/:value`,
    GET_LIST_CHECKIN_CHECKOUT_SERVER_SIDE: `${BASE_ROUTE}/list-checkin_checkout-server-side`,

    UPDATE_CHECKIN_CHECKOUT_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-checkin_checkout-by-id-v2`,
    DELETE_CHECKIN_CHECKOUT_BY_LIST_ID: `${BASE_ROUTE}/delete-checkin_checkout-by-list-id`,
    GET_LIST_CHECKIN_CHECKOUT_EXCEL: `${BASE_ROUTE}/list-checkin_checkout-excel`,
    DOWNLOAD_LIST_CHECKIN_CHECKOUT_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-checkin_checkout-excel-export`,

    API_ADD_CHECKIN_CHECKOUT: `${API_BASE_ROUTE}/add-checkin_checkout`,
    API_DELETE_CHECKIN_CHECKOUT: `${API_BASE_ROUTE}/delete-checkin_checkout/:checkin_checkoutID`,
    API_UPDATE_CHECKIN_CHECKOUT: `${API_BASE_ROUTE}/update-checkin_checkout/:checkin_checkoutID`,
    API_GET_INFO_CHECKIN_CHECKOUT: `${API_BASE_ROUTE}/info-checkin_checkout`,
    API_GET_LIST_CHECKIN_CHECKOUTS: `${API_BASE_ROUTE}/list-checkin_checkouts`,

    API_ADD_CHECKIN: `${API_BASE_ROUTE}/check-in`,
    API_UPDATE_FACE_CHECKIN: `${API_BASE_ROUTE}/update-face-check-in`,
    API_UPDATE_STORE_CHECKIN: `${API_BASE_ROUTE}/update-store-check-in`,
    API_ADD_CHECKOUT: `${API_BASE_ROUTE}/check-out`,

    API_CHECK_CHECKIN: `${API_BASE_ROUTE}/check-checkin`,

    DOWNLOAD_CHECKIN_CHECKOUT_EXCEL: `${API_BASE_ROUTE}/download-checkin_checkout-excel`,
    DOWNLOAD_GENERAL_CHECKIN_CHECKOUT_EXCEL: `${API_BASE_ROUTE}/download-general-checkin_checkout-excel`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_CHECKIN_CHECKOUT = CF_ROUTINGS_CHECKIN_CHECKOUT;