exports.ERROR_LOCATION          = 'ERROR_LOCATION';
exports.ERROR_TIME              = 'ERROR_TIME';
exports.ERROR_FACE_RECOGNITION  = 'ERROR_FACE_RECOGNITION';
exports.ERROR_STORE             = 'ERROR_STORE';

/**
 * TIME RANGE CHECK IN - OUT
 * UNIT: minutes
 */
exports.TIME_RANGE_CHECKIN_CHECKOUT = {
    EARLIEST_CHECKIN_TIME: 120, // Thời gian checkin sớm nhất: trước 2h so với time In chuẩn
    LATEST_CHECKIN_TIME: -180, // Thời gian checkin muộn nhất: 3h so với time In chuẩn
    LATEST_CHECKOUT_TIME: -180, // Muộn nhất: sau 3h so với time out chuẩn
    LATEST_CHECKOUT_HOURS: 3, // Muộn nhất: sau 3h so với time out chuẩn
    WORK_TIME_GREATER_THAN: -210, // Đảm bảo thời gian làm việc > 3,5 giờ
}

/**
 * Lỗi ghi nhận 
 * ERROR_LOCATION: Lỗi vị trí > 800m
 * ERROR_FACE_RECOGNITION: Lỗi nhận diện khuôn mặt
 * ERROR_STORE: Lỗi hình ảnh cửa hàng
 */
exports.ERROR_LOG_CHECKIN_CHECKOUT_TYPE = {

    "ERROR_LOCATION": {
        value: "Lỗi vị trí > 800m",
        color: "#be091b"
    },

    "ERROR_FACE_RECOGNITION": {
        value: "Lỗi nhận diện khuôn mặt",
        color: "#c80909"
    },

    "ERROR_STORE": {
        value: "Lỗi hình ảnh cửa hàng",
        color: "#c40808"
    },

};

/**
 * Trạng thái 
 * 1: Chưa hoàn thành checkin
 * 2: Đã hoàn thành checkin
 * 3: Đã hoàn thành checkout
 */
exports.STATUS_CHECKIN_CHECKOUT_TYPE = {

    "1": {
        value: "Chưa hoàn thành checkin",
        color: "#d90808"
    },

    "2": {
        value: "Đã hoàn thành checkin",
        color: "#68df07"
    },

    "3": {
        value: "Đã hoàn thành checkout",
        color: "#01bec1"
    },

};

/**
 * Nền tảng 
 * ANDROID: ANDROID
 * IOS: IOS
 */
exports.PLATFORM_CHECKIN_CHECKOUT_TYPE = {

    "ANDROID": {
        value: "ANDROID",
        color: "#0b51b7"
    },

    "IOS": {
        value: "IOS",
        color: "#d63031"
    },

};

/**
 * Trạng thái 
 * 1: Chưa hoàn thành checkin (pending)
 * 2: Đã hoàn thành checkin
 * 3: Đã hoàn thành checkout
 * 4: Checkout thất bại
 */
 exports.STATUS_CHECKIN_CHECKOUT_V2 = {
    "1": {
        value: "Chưa hoàn thành checkin",
    },
    "2": {
        value: "Đã hoàn thành checkin",
    },

    "3": {
        value: "Đã hoàn thành checkout",
    },

    "4": {
        value: "Checkout thất bại",
    },
};