"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');
const { ERROR_LOCATION, ERROR_FACE_RECOGNITION, ERROR_STORE, ERROR_TIME } = require('../constants/checkin_checkout');

const GeoSchema = new Schema({
    type: {
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        index: "2dsphere"
    }
});


module.exports = BASE_COLL('checkin_checkout', {
    /**
     * Nhân viên
     */
    employee: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },
    /**
     * Cửa hàng
     */
    store: {
        type: Schema.Types.ObjectId,
        ref: 'store',
    },
    /**
     * Vùng
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Ca làm việc
     */
    shift: {
        type: Schema.Types.ObjectId,
        ref: 'shift',
    },
    /**
     * Hình ảnh khuôn mặt
     */
    faceImages: [{
        type: Schema.Types.ObjectId,
        ref: 'image',
    }],
    /**
     * Hình ảnh cửa hàng
     */
    storeImages: [{
        type: Schema.Types.ObjectId,
        ref: 'image',
    }],
    /**
     * Vị trí
     */
    location: GeoSchema,
    /**
     * Lịch sử checkin checkout
     * Type: 1/ checkin 2/ checkout
     */
    logs: [
        { _id: false, type: { type: Number }, time: Date }
    ],
    /**
     * Ghi chú
     */
    note: {
        type: String,
    },
    /**
     * Lỗi ghi nhận 
     * ERROR_LOCATION: Lỗi vị trí > 800m,
     * ERROR_FACE_RECOGNITION: Lỗi nhận diện khuôn mặt,
     * ERROR_STORE: Lỗi hình ảnh cửa hàng
     * ERROR_TIME: Lỗi thời gian
     */
    error_log: {
        type: String,
        enum: [ERROR_LOCATION, ERROR_FACE_RECOGNITION, ERROR_STORE, ERROR_TIME],
    },
    /**
     * Trạng thái 
     * 1: Chưa hoàn thành checkin (pending)
     * 2: Đã hoàn thành checkin
     * 3: Đã hoàn thành checkout
     * 4: Checkout thất bại
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2, 3, 4],
    },
    /**
     * Tên thiết bị
     */
    deviceName: {
        type: String,
    },
    /**
     * ID thiết bị
     */
    deviceID: {
        type: String,
    },
    /**
     * Nền tảng 
     * ANDROID: ANDROID,
     * IOS: IOS
     */
    platform: {
        type: String,
        enum: ["ANDROID", "IOS"],
    },
});