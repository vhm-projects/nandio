"use strict";

/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const ObjectID = require('mongoose').Types.ObjectId;
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const path = require('path');
const fs = require('fs');
const XlsxPopulate = require('xlsx-populate');

/**  
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    checkNumberIsValidWithRange
} = require('../../../utils/utils');
const timeUtils = require('../../../utils/time_utils');
const agenda = require('../../../config/cf_agenda');
const { ERROR_STORE, ERROR_FACE_RECOGNITION, ERROR_LOCATION, ERROR_TIME } = require('../constants/checkin_checkout');
const { TIME_RANGE_CHECKIN_CHECKOUT, STATUS_CHECKIN_CHECKOUT_V2 } = require('../constants/checkin_checkout');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const CHECKIN_CHECKOUT_COLL = require('../databases/checkin_checkout-coll');
const {
    HISTORY_INIT_CUSTOMER_COLL,
    HISTORY_EXCHANGE_GIFT_COLL,
    HISTORY_EXCHANGE_SAMPLING_COLL  
} = require('../../history');

const { STORE_COLL } = require('../../store');
const { SHIFT_COLL } = require('../../core');
const { EMPLOYEE_MODEL } = require('../../employee');
const EMPLOYEE_HR_COLL = require('../../employee/databases/employee_hr-coll');


class Model extends BaseModel {
    constructor() {
        super(CHECKIN_CHECKOUT_COLL);

        const runDefineTask = async () => {
			/**
			 * define all EVENT for COMSUMER
			 */
			agenda.define('update status check in/out - outdated', async (job, done) => {
				console.log(`========================================`);
				console.log(`done update status check in/out - outdated !!!!`);
				console.log({ __JOB: job.attrs });

                const { _id } = job.attrs.data;
                const infoCheckin = await CHECKIN_CHECKOUT_COLL.findOne({ _id, status: 3 });
                console.log({ AGENDA: infoCheckin, _id })

                if(!infoCheckin){
                    await CHECKIN_CHECKOUT_COLL.findByIdAndUpdate(_id, {
                        $set: { status: 4 }
                    });
                }

				await agenda.cancel({ _id: job.attrs._id });
				done();
			});

			await new Promise(resolve => agenda.once('ready', resolve));
			agenda.start();
		}

		runDefineTask().catch(error => {
			console.error({ ERROR_DEFINE_TASK: error });
			process.exit(-1);
		});

    }

    /**
     * Tạo mới checkin_checkout
    * @param {object} employee
    * @param {object} store
    * @param {object} shift
    * @param {array} faceImages
    * @param {array} storeImages
    * @param {string} location
    * @param {array} logs
    * @param {string} note
    * @param {number} error_log
    * @param {number} status
    * @param {string} deviceName
    * @param {string} deviceID
    * @param {string} platform
    * @param {objectId} userCreate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    insert({
        employee,
        store,
        shift,
        faceImages,
        storeImages,
        note,
        status = 1,
        deviceName,
        deviceID,
        platform,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (note.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài ghi chú không được lớn hơn 125 ký tự'
                    });
                }

                if (deviceName.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tên thiết bị không được lớn hơn 125 ký tự'
                    });
                }

                if (deviceID.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài id thiết bị không được lớn hơn 125 ký tự'
                    });
                }

                if (platform.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài nền tảng không được lớn hơn 125 ký tự'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2, 3],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trạng thái không hợp lệ'
                    });
                }

                if (platform && !["ANDROID", "IOS"].includes(platform)) {
                    return resolve({
                        error: true,
                        message: 'nền tảng không hợp lệ'
                    });
                }

                if (!checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                if (!checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (!checkObjectIDs([shift])) {
                    return resolve({
                        error: true,
                        message: 'ca làm việc không hợp lệ'
                    });
                }

                if (faceImages && !checkObjectIDs(faceImages)) {
                    return resolve({
                        error: true,
                        message: 'hình ảnh khuôn mặt không hợp lệ'
                    });
                }

                if (storeImages && !checkObjectIDs(storeImages)) {
                    return resolve({
                        error: true,
                        message: 'hình ảnh cửa hàng không hợp lệ'
                    });
                }

                let dataInsert = {
                    employee,
                    store,
                    shift,
                    faceImages,
                    storeImages,
                    note,
                    status,
                    deviceName,
                    deviceID,
                    platform,
                    userCreate
                };

                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo lịch sử checkin checkout thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Cập nhật checkin_checkout 
     * @param {objectId} checkin_checkoutID
    * @param {object} employee
    * @param {object} store
    * @param {object} shift
    * @param {array} faceImages
    * @param {array} storeImages
    * @param {string} location
    * @param {array} logs
    * @param {string} note
    * @param {number} error_log
    * @param {number} status
    * @param {string} deviceName
    * @param {string} deviceID
    * @param {string} platform

    * @param {objectId} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    update({
        checkin_checkoutID,
        note,
        status,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([checkin_checkoutID])) {
                    return resolve({
                        error: true,
                        message: 'checkin_checkoutID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (note.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài ghi chú không được lớn hơn 125 ký tự'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2, 3],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trạng thái không hợp lệ'
                    });
                }

                const checkExists = await CHECKIN_CHECKOUT_COLL.findById(checkin_checkoutID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'lịch sử checkin checkout không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                dataUpdate.note = note;
                dataUpdate.status = status;

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: checkin_checkoutID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Cập nhật checkin_checkout (không bắt buộc)
     * @param {objectId} checkin_checkoutID
    * @param {object} employee
    * @param {object} store
    * @param {object} shift
    * @param {array} faceImages
    * @param {array} storeImages
    * @param {string} location
    * @param {array} logs
    * @param {string} note
    * @param {number} error_log
    * @param {number} status
    * @param {string} deviceName
    * @param {string} deviceID
    * @param {string} platform

    * @param {objectId} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
        */
    updateNotRequire({
        checkin_checkoutID,
        employee,
        store,
        shift,
        faceImages,
        storeImages,
        location,
        logs,
        note,
        error_log,
        status,
        deviceName,
        deviceID,
        platform,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([checkin_checkoutID])) {
                    return resolve({
                        error: true,
                        message: 'checkin_checkoutID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                if (shift && !checkObjectIDs([shift])) {
                    return resolve({
                        error: true,
                        message: 'ca làm việc không hợp lệ'
                    });
                }

                if (faceImages && !checkObjectIDs(faceImages)) {
                    return resolve({
                        error: true,
                        message: 'hình ảnh khuôn mặt không hợp lệ'
                    });
                }

                if (storeImages && !checkObjectIDs(storeImages)) {
                    return resolve({
                        error: true,
                        message: 'hình ảnh cửa hàng không hợp lệ'
                    });
                }

                if (logs && !logs.length) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập lịch sử checkin checkout cho checkin_checkout'
                    });
                }

                if (error_log && !checkNumberIsValidWithRange({
                        arrValid: [NaN, NaN, NaN],
                        val: error_log
                    })) {
                    return resolve({
                        error: true,
                        message: 'lỗi ghi nhận không hợp lệ'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2, 3],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trạng thái không hợp lệ'
                    });
                }

                if (platform && !["ANDROID", "IOS"].includes(platform)) {
                    return resolve({
                        error: true,
                        message: 'nền tảng không hợp lệ'
                    });
                }

                const checkExists = await CHECKIN_CHECKOUT_COLL.findById(checkin_checkoutID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'lịch sử checkin checkout không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                employee && (dataUpdate.employee = employee);
                store && (dataUpdate.store = store);
                shift && (dataUpdate.shift = shift);
                faceImages && (dataUpdate.faceImages = faceImages);
                storeImages && (dataUpdate.storeImages = storeImages);
                location && (dataUpdate.location = location);
                logs && (dataUpdate.logs = logs);
                note && (dataUpdate.note = note);
                error_log && (dataUpdate.error_log = error_log);
                status && (dataUpdate.status = status);
                deviceName && (dataUpdate.deviceName = deviceName);
                deviceID && (dataUpdate.deviceID = deviceID);
                platform && (dataUpdate.platform = platform);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: checkin_checkoutID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa checkin_checkout 
     * @param {objectId} checkin_checkoutID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(checkin_checkoutID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([checkin_checkoutID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị checkin_checkoutID không hợp lệ'
                    });
                }

                const infoAfterDelete = await this.updateById(checkin_checkoutID, {
                    state: 2
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa checkin_checkout 
     * @param {array} checkin_checkoutID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(checkin_checkoutID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(checkin_checkoutID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị checkin_checkoutID không hợp lệ'
                    });
                }

                const infoAfterDelete = await CHECKIN_CHECKOUT_COLL.updateMany({
                    _id: {
                        $in: checkin_checkoutID
                    }
                }, {
                    state: 2,
                    modifyAt: new Date()
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin checkin_checkout 
     * @param {objectId} checkin_checkoutID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(checkin_checkoutID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([checkin_checkoutID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị checkin_checkoutID không hợp lệ'
                    });
                }

                const infoCheckin_checkout = await CHECKIN_CHECKOUT_COLL.findById(checkin_checkoutID)
                    .populate('employee store shift faceImages storeImages')

                if (!infoCheckin_checkout) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin lịch sử checkin checkout'
                    });
                }

                return resolve({
                    error: false,
                    data: infoCheckin_checkout
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách checkin_checkout 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listCheckin_checkout = await CHECKIN_CHECKOUT_COLL
                    .find({
                        state: 1
                    }).populate('employee store shift faceImages storeImages')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listCheckin_checkout) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách lịch sử checkin checkout'
                    });
                }

                return resolve({
                    error: false,
                    data: listCheckin_checkout
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách checkin_checkout theo bộ lọc
    * @param {string} keyword
    * @enum {number} error_log
    * @enum {number} status

    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: array, message?: string }}
    */
    getListByFilter({
        keyword,
        error_log,
        status,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        location: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        note: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        platform: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                error_log && (conditionObj.error_log = error_log);

                status && (conditionObj.status = status);


                const listCheckin_checkoutByFilter = await CHECKIN_CHECKOUT_COLL
                    .find(conditionObj).populate('employee store shift faceImages storeImages')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listCheckin_checkoutByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách lịch sử checkin checkout"
                    });
                }

                return resolve({
                    error: false,
                    data: listCheckin_checkoutByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách checkin_checkout theo bộ lọc (server side)
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        page,
        limit,
        field,
        dir,
        region,
        employee,
        store,
        showIndex,
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        location: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        note: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        platform: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            }
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = { modifyAt: -1 };

                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                region      && (conditionObj.region = ObjectID(region));
                employee    && (conditionObj['employee._id'] = ObjectID(employee));
                store       && (conditionObj['store._id'] = ObjectID(store));

                const skip = (page - 1) * limit;

                const [{ metadata, data: listCheckin_checkoutByFilter }] = await CHECKIN_CHECKOUT_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'shifts',
                            localField: 'shift',
                            foreignField: '_id',
                            as: 'shift'
                        }
                    },
                    {
                        $unwind: {
                            path: '$shift',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $facet: {
                            metadata: [ { $count: "total" }, { $addFields: { page } } ],
                            data: [ { $sort: sort }, { $skip: +skip }, { $limit: +limit } ]
                        }
                    }
                    // {
                    //     $group: {
                    //         _id: { 
                    //             employee: "$employee",
                    //             store: "$store",
                    //             shift: "$shift",
                    //             createAt: "$createAt",
                    //         },
                    //         lastCheckInOut: { $last: "$$ROOT" }
                    //     }
                    // },
                ]).allowDiskUse(true);

                const totalCheckin_checkout = metadata.length && metadata[0].total;

                if (!listCheckin_checkoutByFilter) {
                    return resolve({
                        recordsTotal: totalCheckin_checkout,
                        recordsFiltered: totalCheckin_checkout,
                        data: []
                    });
                }

                const listCheckin_checkoutDataTable = listCheckin_checkoutByFilter.map((checkin_checkout, index) => {
                    // let checkin_checkout = infoCheckInOut.lastCheckInOut;

                    let status = '';
                    let checkInOut = '';
                    let errorLog = '';
                    let firstColumn = '';

                    switch (checkin_checkout.status) {
                        case 1:
                            status = `
                                <h2>
                                    <span class="badge badge-soft-danger">Chưa hoàn thành check in</span>
                                </h2>
                            `;
                            break;
                        case 2:
                            status = `
                                <h2>
                                    <span class="badge badge-soft-info">Đã check in</span>
                                </h2>
                            `;
                            break;
                        case 3:
                            status = `
                                <h2>
                                    <span class="badge badge-soft-success">Đã check out</span>
                                </h2>
                            `;
                            break;
                        case 4:
                            status = `
                                <h2>
                                    <span class="badge badge-soft-secondary">Hết hạn check out</span>
                                </h2>
                            `;
                            break;
                        default:
                            break;
                    }

                    switch (checkin_checkout.error_log) {
                        case 'ERROR_LOCATION':
                            errorLog = 'Lỗi vị trí > 800m';
                            break;
                        case 'ERROR_FACE_RECOGNITION':
                            errorLog = 'Lỗi nhận diện khuôn mặt';
                            break;
                        case 'ERROR_STORE':
                            errorLog = 'Lỗi hình ảnh cửa hàng';
                            break;
                        case 'ERROR_TIME':
                            if(checkin_checkout.status === 2){
                                errorLog = 'Lỗi thời gian check out';
                            } else{
                                errorLog = 'Lỗi thời gian check in';
                            }
                            break;
                        default:
                            break;
                    }

                    if(checkin_checkout.logs && checkin_checkout.logs.length){
                        checkInOut = `
                            <span class="d-block mb-1">
                                <i> Check in: ${moment(checkin_checkout.logs[0].time).format('HH:mm DD/MM/YYYY')} </i>
                            </span>
                            <span class="d-block">
                                ${checkin_checkout.logs[1] ? `<i>Check out: ${moment(checkin_checkout.logs[1].time).format('HH:mm DD/MM/YYYY')}</i>` : ''}
                            </span>
                        `;
                    }

                    if(showIndex){
                        firstColumn = skip + index + 1;
                    } else{
                        firstColumn = `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${checkin_checkout._id}" type="checkbox" class="check-record check-record-${checkin_checkout._id}" _index ="${index + 1}"><label for="${checkin_checkout._id}"></label></div></td>`;
                    }

                    return {
                        index: firstColumn,
                        createAt: `<a href="/checkin_checkout/update-checkin_checkout-by-id?checkin_checkoutID=${checkin_checkout._id}">${moment(checkin_checkout.createAt).format('HH:mm DD/MM/YYYY')}</a>`,
                        employee: `
                            <span class="d-block">
                                ${checkin_checkout.employee ? checkin_checkout.employee.fullname : ''}
                            </span>
                            <span class="d-block" style="font-size:10px">
                                Mã NV: ${checkin_checkout.employee ? checkin_checkout.employee.code : ''}
                            </span>
                        `,
                        store: `
                            <span class="d-block">
                                ${checkin_checkout.store ? checkin_checkout.store.name : ''}
                            </span>
                            <span class="d-block" style="font-size:10px">
                                Mã CH: ${checkin_checkout.store ? checkin_checkout.store.code : ''}
                            </span>
                        `,
                        shift: checkin_checkout.shift ? checkin_checkout.shift.name : '',
                        status,
                        checkin_checkout: checkInOut,
                        // error: errorLog
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalCheckin_checkout,
                    recordsFiltered: totalCheckin_checkout,
                    data: listCheckin_checkoutDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc checkin_checkout
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc checkin_checkout
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {
                // arrayFilter.map(item => {
                //     console.log(item);
                // });
                // arrayItemCustomerChoice.map(item => {
                //     console.log(item);
                // });
                const listCheckin_checkoutByFilter = await CHECKIN_CHECKOUT_COLL.aggregate(arrayFilter)
                
                if (!listCheckin_checkoutByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listCheckin_checkoutByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listCheckin_checkoutByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');
                                // console.log({
                                //     variable
                                // });
                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    // console.log({
                                    //     objDataOfVariable
                                    // });
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                    // console.log({
                                    //     value
                                    // });
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }
                               
                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('DD/MM/YYYY');
                                }
                                
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                          
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('DD/MM/YYYY'));
                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc checkin_checkout
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            dataEnum: item.dataEnum,
                            nameCollChoice: item.nameCollChoice,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                dataEnum: item.dataEnum,
                                nameCollChoice: item.nameCollChoice,
                            }
                        ];
                    }
                } else {
                    
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    nameCollChoice: item.nameCollChoice,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!elem.populate || !elem.populate.length) {
                                    arrPopulate[indexV3] = {
                                        ...arrPopulate[indexV3],
                                        populate: [{
                                            name: nameFieldRef,
                                            coll: item.coll,
                                            select: item.name + " ",
                                            refFrom: item.refFrom,
                                        }]
                                    }
                                    // console.log({
                                    //     ___check: arrPopulate[indexV3]
                                    // });
                                } else {
                                    // console.log({
                                    //     _aaaaaa: arrPopulate[indexV3]
                                    // });
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        // console.log({
                                        //     populate,
                                        //     coll: item.coll
                                        // });
                                        if (!populate.name.includes(item.coll)) {
                                            // markPopulate
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                            // arrPopulate.map(item => {
                            //     console.log({
                            //         item,
                            //         populate: item.populate
                            //     });
                            // })
                            
                        });
                    }
                }
            }
        });
        // console.log({
        //     arrPopulate
        // });
        // arrPopulate.map(item => {
        //     console.log({
        //         item,
        //         populate: item.populate
        //     });
        // })
        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {

            if (filter.length > 1) {
                condition === 'OR' && (conditionObj.$or = []);
                condition === 'AND' && (conditionObj.$and = []);

                filter.map(filterObj => {
                    const getConditionByFilter = this.getConditionObj(filterObj);

                    if (condition === 'OR') {
                        conditionObj.$or.push(getConditionByFilter);
                    } else {
                        conditionObj.$and.push(getConditionByFilter);
                    }
                })
            } else {
                conditionObj = {
                    ...conditionObj,
                    ...this.getConditionObj(filter[0])
                };
            }
        }

        
        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let lookup = [{
                        $lookup: {
                            from: pluralize.plural(item.coll),
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: pluralize.plural(populate.coll),
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Thêm
    * @param {string} location
	* @param {string} note
	* @param {string} deviceName
	* @param {string} deviceID
	* @param {string} platform
    * @param {objectId} userCreate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    insertCheckin_checkout({
        employee,
        store,
        shift,
        faceImages,
        storeImages,
        location,
        logs,
        note,
        error_log,
        status,
        deviceName,
        deviceID,
        platform,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([userCreate]))
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ',
                        status: 400
                    });



                if (employee && !checkObjectIDs(employee)) {
                    return resolve({
                        error: true,
                        message: 'Nhân viên không hợp lệ',
                        status: 400
                    });
                }

                if (store && !checkObjectIDs(store)) {
                    return resolve({
                        error: true,
                        message: 'Cửa hàng không hợp lệ',
                        status: 400
                    });
                }

                if (shift && !checkObjectIDs(shift)) {
                    return resolve({
                        error: true,
                        message: 'Ca làm việc không hợp lệ',
                        status: 400
                    });
                }

                if (faceImages && !checkObjectIDs(faceImages)) {
                    return resolve({
                        error: true,
                        message: 'Hình ảnh khuôn mặt không hợp lệ',
                        status: 400
                    });
                }

                if (storeImages && !checkObjectIDs(storeImages)) {
                    return resolve({
                        error: true,
                        message: 'Hình ảnh cửa hàng không hợp lệ',
                        status: 400
                    });
                }

                if (logs && !logs.length) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập Lịch sử checkin checkout cho checkin_checkout',
                        status: 400
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2, 3],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'Trạng thái không hợp lệ',
                        status: 400
                    });
                }

                if (platform && !["ANDROID", "IOS"].includes(platform)) {
                    return resolve({
                        error: true,
                        message: 'Nền tảng không hợp lệ',
                        status: 400
                    });
                }

                let dataInsert = {
                    employee,
                    store,
                    shift,
                    faceImages,
                    storeImages,
                    location,
                    logs,
                    note,
                    error_log,
                    status,
                    deviceName,
                    deviceID,
                    platform,
                    userCreate
                };
                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);


                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo CHECKIN_CHECKOUT thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa
     * @param {objectId} checkin_checkoutID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteCheckin_checkout(checkin_checkoutID) {
        return new Promise(async resolve => {
            try {
                let ids = [checkin_checkoutID];

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị checkin_checkoutID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.updateWhereClause({
                    _id: {
                        $in: ids
                    }
                }, {
                    state: 2
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
    * Sửa
    * @param {string} location
	* @param {string} note
	* @param {string} deviceName
	* @param {string} deviceID
	* @param {string} platform
    * @param {objectId} checkin_checkoutID
    * @param {objectId} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    updateCheckin_checkout({
        checkin_checkoutID,
        employee,
        store,
        shift,
        faceImages,
        storeImages,
        location,
        logs,
        note,
        error_log,
        status,
        deviceName,
        deviceID,
        platform,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([checkin_checkoutID]))
                    return resolve({
                        error: true,
                        message: 'checkin_checkoutID không hợp lệ',
                        status: 400
                    });

                if (!checkObjectIDs([userUpdate]))
                    return resolve({
                        error: true,
                        message: 'ID user cập nhật không hợp lệ',
                        status: 400
                    });


                if (employee && !checkObjectIDs(employee)) {
                    return resolve({
                        error: true,
                        message: 'Nhân viên không hợp lệ',
                        status: 400
                    });
                }

                if (store && !checkObjectIDs(store)) {
                    return resolve({
                        error: true,
                        message: 'Cửa hàng không hợp lệ',
                        status: 400
                    });
                }

                if (shift && !checkObjectIDs(shift)) {
                    return resolve({
                        error: true,
                        message: 'Ca làm việc không hợp lệ',
                        status: 400
                    });
                }

                if (faceImages && !checkObjectIDs(faceImages)) {
                    return resolve({
                        error: true,
                        message: 'Hình ảnh khuôn mặt không hợp lệ',
                        status: 400
                    });
                }

                if (storeImages && !checkObjectIDs(storeImages)) {
                    return resolve({
                        error: true,
                        message: 'Hình ảnh cửa hàng không hợp lệ',
                        status: 400
                    });
                }

                if (logs && !logs.length) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập Lịch sử checkin checkout cho checkin_checkout',
                        status: 400
                    });
                }

                if (error_log && !checkNumberIsValidWithRange({
                        arrValid: [NaN, NaN, NaN],
                        val: error_log
                    })) {
                    return resolve({
                        error: true,
                        message: 'Lỗi ghi nhận không hợp lệ',
                        status: 400
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2, 3],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'Trạng thái không hợp lệ',
                        status: 400
                    });
                }

                if (platform && !["ANDROID", "IOS"].includes(platform)) {
                    return resolve({
                        error: true,
                        message: 'Nền tảng không hợp lệ',
                        status: 400
                    });
                }

                const checkExists = await CHECKIN_CHECKOUT_COLL.findById(checkin_checkoutID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'checkin_checkout không tồn tại',
                        status: 404
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                employee && (dataUpdate.employee = employee);
                store && (dataUpdate.store = store);
                shift && (dataUpdate.shift = shift);
                faceImages && (dataUpdate.faceImages = faceImages);
                storeImages && (dataUpdate.storeImages = storeImages);
                location && (dataUpdate.location = location);
                logs && (dataUpdate.logs = logs);
                note && (dataUpdate.note = note);
                error_log && (dataUpdate.error_log = error_log);
                status && (dataUpdate.status = status);
                deviceName && (dataUpdate.deviceName = deviceName);
                deviceID && (dataUpdate.deviceID = deviceID);
                platform && (dataUpdate.platform = platform);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: checkin_checkoutID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Chi tiết
     * @param {JSON string} filter
     * @param {string} select
     * @param {string} explain
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoCheckin_checkout({
        select,
        filter = {},
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['employee', 'store', 'shift', 'faceImages', 'storeImages'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let {
                    checkinID,
                    employeeID,
                    storeID,
                    shiftID,
                    error_log,
                    status,
                } = filter;
                let conditionObj = {};

                checkinID && (conditionObj._id = checkinID);
                employeeID && (conditionObj.employee = employeeID);
                storeID && (conditionObj.store = storeID);
                shiftID && (conditionObj.shift = shiftID);
                error_log && (conditionObj.error_log = error_log);
                status && (conditionObj.status = status);

                let infoCheckin_checkout = await CHECKIN_CHECKOUT_COLL
                    .findOne(conditionObj)
                    .select(fieldsSelected)
                    .lean();

                if (!infoCheckin_checkout) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin checkin_checkout',
                        status: 400
                    });
                }

                const { _id, store, employee } = infoCheckin_checkout;

                let totalNewCustomer = await HISTORY_INIT_CUSTOMER_COLL.countDocuments({
                    checkin: _id,
                    store,
                    employee
                });

                let totalPointExchange = await HISTORY_EXCHANGE_GIFT_COLL.aggregate([
                    {
                        $match: {
                            checkin: ObjectID(_id),
                            store: ObjectID(store),
                            employee: ObjectID(employee)
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            total: { $sum: '$point' }
                        }
                    }
                ]);

                let totalGiftExchange = await HISTORY_EXCHANGE_GIFT_COLL.aggregate([
                    {
                        $match: {
                            checkin: ObjectID(_id),
                            store: ObjectID(store),
                            employee: ObjectID(employee)
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            total: { $sum: '$amount' }
                        }
                    }
                ]);

                let totalSamplingUsed = await HISTORY_EXCHANGE_SAMPLING_COLL.aggregate([
                    {
                        $match: {
                            checkin: ObjectID(_id),
                            store: ObjectID(store),
                            employee: ObjectID(employee)
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            total: { $sum: '$amount' }
                        }
                    }
                ]);

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoCheckin_checkout = await CHECKIN_CHECKOUT_COLL.populate(infoCheckin_checkout, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoCheckin_checkout = await CHECKIN_CHECKOUT_COLL.populate(infoCheckin_checkout, `${ref}.${field}`);
                        }
                    }
                }

                return resolve({
                    error: false,
                    data: {
                        infoCheckin_checkout,
                        totalNewCustomer,
                        totalPointExchange: totalPointExchange.length ? totalPointExchange[0].total : 0,
                        totalGiftExchange: totalGiftExchange.length ? totalGiftExchange[0].total : 0,
                        totalSamplingUsed: totalSamplingUsed.length ? totalSamplingUsed[0].total : 0
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Danh sách
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListCheckin_checkouts({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { state: 1 };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['employee', 'store', 'shift', 'faceImages', 'storeImages', 'error_log', 'status', 'fromDate', 'toDate'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    employee,
                    store,
                    shift,
                    faceImages,
                    storeImages,
                    error_log,
                    fromDate,
                    toDate,
                    status,
                    isCheckInOut,
                } = filter;

                employee && (conditionObj.employee = employee);
                store && (conditionObj.store = store);
                shift && (conditionObj.shift = shift);

                if(fromDate && toDate){
                    fromDate = timeUtils.parseTimeUTC(fromDate, true);
                    toDate = timeUtils.parseTimeUTC(toDate, true);

                    console.table({ fromDate, toDate });

                    let _fromDate = moment(fromDate).startOf('day').format('Y-MM-DD H:m:sZ');
                    let _toDate = moment(toDate).endOf('day').format('Y-MM-DD H:m:sZ');

                    console.table({ _fromDate, _toDate });

                    conditionObj['logs.type'] = 1;
                    conditionObj['logs.time'] = {
                        $gte: _fromDate,
                        $lte: _toDate,
                    }

                    console.log({ condLogs: conditionObj })
                }

                if (faceImages && faceImages.length) {
                    conditionObj.faceImages = {
                        $in: faceImages
                    };
                }

                if (storeImages && storeImages.length) {
                    conditionObj.storeImages = {
                        $in: storeImages
                    };
                }

                error_log && (conditionObj.error_log = error_log);
                status && (conditionObj.status = status);

                if(isCheckInOut){
                    conditionObj.status = { $in: [2,3] };
                }

                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['employee', 'store', 'shift', 'faceImages', 'storeImages'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listCheckin_checkouts = await CHECKIN_CHECKOUT_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listCheckin_checkouts = await CHECKIN_CHECKOUT_COLL.populate(listCheckin_checkouts, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listCheckin_checkouts = await CHECKIN_CHECKOUT_COLL.populate(listCheckin_checkouts, `${ref}.${field}`);
                        }
                    }
                }

                if (!listCheckin_checkouts) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách checkin_checkout',
                        status: 400
                    });
                }

                let totalRecord = await CHECKIN_CHECKOUT_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listCheckin_checkouts,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
    * Tạo mới check in
    * @param {objectID} employee
    * @param {objectID} store
    * @param {objectID} shift
    * @param {float} lng
    * @param {float} lat
    * @param {string} note
    * @param {string} deviceName
    * @param {string} deviceID
    * @param {string} platform
    * @param {objectID} userCreate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    insertCheckIn({ 
        employeeID, storeID, shiftID, lng, lat, timeStartShift,
        deviceName, deviceID, platform, userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ',
                        status: 400
                    });
                }

                if(!lng || !lat){
                    return resolve({
                        error: true,
                        message: 'Vui lòng cho phép truy cập vị trí hiện tại',
                        status: 400
                    });
                }

                if (!checkObjectIDs([employeeID])) {
                    return resolve({
                        error: true,
                        message: 'nhân viên không hợp lệ',
                        status: 400
                    });
                }

                if (!checkObjectIDs([storeID])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ',
                        status: 400
                    });
                }

                if (!checkObjectIDs([shiftID])) {
                    return resolve({
                        error: true,
                        message: 'Ca làm việc không hợp lệ',
                        status: 400
                    });
                }

                let infoShift = await SHIFT_COLL.findById(shiftID);
                if(!infoShift){
                    return resolve({
                        error: true,
                        message: 'Không tìm thấy ca làm việc',
                        status: 400
                    });
                }

                let infoStore = await STORE_COLL.findById(storeID);
                if(!infoStore){
                    return resolve({
                        error: true,
                        message: 'Không tìm thấy cửa hàng',
                        status: 400
                    });
                }

                let dataInsert = {
                    employee: employeeID,
                    store: storeID,
                    shift: shiftID,
                    area: infoStore.area,
                    region: infoStore.region,
                    deviceName, 
                    deviceID,
                    platform,
                    userCreate
                };

                let description = '';
                let { endTime, startTime } = infoShift;
                let currentTime = timeUtils.parseTimeUTC(new Date(), true);
                let startTimeShift = timeUtils.parseTimeUTC(startTime, true);

                if(timeStartShift){
                    timeStartShift = timeUtils.parseTimeUTC(timeStartShift, true);
                }

                let diffTime = timeUtils.calculateExpireTime(currentTime, timeStartShift || startTimeShift);

                const storesIgnore = ['G00TESTING-0', 'G00TESTING-1', 'G00TESTING-2', 'G00TESTING-3', 'G00TESTING-4', 'G00TESTING-5', 'G00TESTING-6', 'G00TESTING-7', 'G00TESTING-8', 'G00TESTING-9'];

                if(!storesIgnore.includes(infoStore.code)){
                    let checkDistanceStore = await STORE_COLL.aggregate([
                        {
                            $geoNear: {
                                near: { type: "Point", coordinates: [parseFloat(lng), parseFloat(lat)] },
                                maxDistance: 800,
                                minDistance: 0,
                                distanceMultiplier: 0.001, // Returns km
                                distanceField: "dist.calculated",
                                includeLocs: "dist.location", // Returns distance
                                spherical: true
                            }
                        },
                        {
                            $match: {
                                _id: ObjectID(storeID),
                                status: 1,
                                state: 1
                            }
                        }
                    ]);
    
                    if(!checkDistanceStore.length){
                        dataInsert.error_log = ERROR_LOCATION;
                        description = 'Vị trí hiện tại không nằm trong tọa độ định vị cho phép của cửa hàng (800m)';
                    }
                }

                dataInsert.location = {
                    type: "Point",
                    coordinates: [parseFloat(lng), parseFloat(lat)]
                }

                let startHours = timeUtils.parseTimeUTC(startTime).subtract(2, 'hours').format("HH:mm A");
                let endHours = timeUtils.parseTimeUTC(startTime).add(3, 'hours').format("HH:mm A");

                console.log({
                    diffTime,
                    currentTime,
                    timeStartShift,
                    startTimeShift,
                    startHours,
                    endHours,
                })

                // Thời gian checkin sớm nhất: trước 2h so với time In chuẩn
                if(diffTime > TIME_RANGE_CHECKIN_CHECKOUT.EARLIEST_CHECKIN_TIME){
                    dataInsert.error_log = ERROR_TIME;
                    description = `Thời gian checkin không hợp lệ (Thời gian hợp lệ Từ ${startHours} đến ${endHours})`;
                }

                // Thời gian checkin muộn nhất: 3h so với time In chuẩn
                if(diffTime < TIME_RANGE_CHECKIN_CHECKOUT.LATEST_CHECKIN_TIME){
                    dataInsert.error_log = ERROR_TIME;
                    description = `Thời gian checkin không hợp lệ (Thời gian hợp lệ Từ ${startHours} đến ${endHours})`;
                }

                let hours = timeUtils.parseTimeUTC(endTime).add(TIME_RANGE_CHECKIN_CHECKOUT.LATEST_CHECKOUT_HOURS, 'hours').format("HH");
                let minutes = timeUtils.parseTimeUTC(endTime).format("mm");
                let currentTimeScheduler = timeUtils.parseTimeUTC();
                let timeScheduler = timeUtils.parseTimeUTC();

                timeScheduler.set({ hours, minutes });
                timeScheduler.toISOString();

                let compareCurrentTime = timeUtils.calculateExpire(currentTimeScheduler, timeScheduler);

                console.log({ 
                    hours, minutes,
                    compareCurrentTime,
                    currentTimeScheduler,
                    timeScheduler
                });

                // if(compareCurrentTime < 0) {
                //     return resolve({
                //         error: true,
                //         status: 400,
                //         code: dataInsert.error_log,
                //         message: 'Check in thất bại (quá hạn thời gian check in)'
                //     });
                // }

                let infoAfterInsert = await this.insertData(cleanObject(dataInsert));

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        status: 422,
                        code: dataInsert.error_log,
                        message: 'Tạo lịch sử check in thất bại',
                    });
                }

                // agenda.schedule(timeScheduler, 'update status check in/out - outdated', infoAfterInsert);
                // console.log(`agenda: update status check in/out - outdated added - ${timeScheduler}`);

                return resolve({
                    error: dataInsert.error_log ? true : false,
                    status: dataInsert.error_log ? 400 : 200,
                    code: dataInsert.error_log,
                    message: description,
                    data: infoAfterInsert,
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
    * Update face check in
    * @param {objectID} checkinID
    * @param {arrayID} faceImage
    * @param {objectID} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    updateFaceCheckIn({ checkinID, faceImages, userUpdate }){
        return new Promise(async resolve => {
            try {
                let dataUpdate = { userUpdate };

                if (!checkObjectIDs([checkinID])) {
                    return resolve({
                        error: true,
                        message: 'ID check in không hợp lệ',
                        status: 400
                    });
                }

                let infoCheckIn = await CHECKIN_CHECKOUT_COLL.findById(checkinID);

                if(!infoCheckIn){
                    return resolve({
                        error: true,
                        message: 'Bạn chưa check in',
                        status: 403
                    });
                }

                if (!faceImages || !faceImages.length || !checkObjectIDs(faceImages)) {
                    const infoAfterUpdate = await CHECKIN_CHECKOUT_COLL.findByIdAndUpdate(infoCheckIn._id, { 
                        error_log: ERROR_FACE_RECOGNITION
                    }, { new: true });

                    return resolve({
                        error: true,
                        code: ERROR_FACE_RECOGNITION,
                        message: 'Hình ảnh khuôn mặt không hợp lệ',
                        status: 400,
                        data: infoAfterUpdate,
                    });
                } else{
                    dataUpdate.faceImages = faceImages;
                }

                if(infoCheckIn.error_log === ERROR_FACE_RECOGNITION){
                    dataUpdate.error_log = null;
                }

                let infoAfterUpdate = await CHECKIN_CHECKOUT_COLL.findByIdAndUpdate(infoCheckIn._id, dataUpdate, { new: true });

                if(!infoAfterUpdate){
                    const infoAfterUpdate =await CHECKIN_CHECKOUT_COLL.findByIdAndUpdate(infoCheckIn._id, { 
                        error_log: ERROR_FACE_RECOGNITION 
                    }, { new: true });

                    return resolve({
                        error: true,
                        code: ERROR_FACE_RECOGNITION,
                        message: 'Không thể cập nhật hình ảnh khuôn mặt',
                        status: 422,
                        data: infoAfterUpdate,
                    });
                }

                resolve({ error: false, data: infoAfterUpdate, status: 200 });
            } catch (error) {
                resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    /**
    * Update face check in
    * @param {objectID} checkinID
    * @param {arrayID} storeImage
    * @param {objectID} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    updateStoreCheckIn({ checkinID, storeImages, userUpdate }){
        return new Promise(async resolve => {
            try {
                let dataUpdate = { 
                    userUpdate, 
                    status: 2,
                    error_log: null,
                    $push: {
                        logs: {
                            type: 1,
                            time: timeUtils.getCurrentTime()
                        }
                    }
                };

                if (!checkObjectIDs([checkinID])) {
                    return resolve({
                        error: true,
                        message: 'ID check in không hợp lệ',
                        status: 400
                    });
                }

                let infoCheckIn = await CHECKIN_CHECKOUT_COLL.findById(checkinID);

                if(!infoCheckIn){
                    return resolve({
                        error: true,
                        message: 'Bạn chưa check in',
                        status: 403
                    });
                }

                if(infoCheckIn.status >= 2){
                    return resolve({
                        error: true,
                        message: 'Bạn đã check in thành công',
                        status: 400
                    });
                }

                if (!storeImages || !storeImages.length || !checkObjectIDs(storeImages)) {
                    const infoAfterUpdate = await CHECKIN_CHECKOUT_COLL.findByIdAndUpdate(infoCheckIn._id, { 
                        error_log: ERROR_STORE 
                    }, { new: true });

                    return resolve({
                        error: true,
                        code: ERROR_STORE,
                        message: 'Hình ảnh cửa hàng không hợp lệ',
                        status: 400,
                        data: infoAfterUpdate
                    });
                } else{
                    dataUpdate.storeImages = storeImages;
                }

                let infoAfterUpdate = await CHECKIN_CHECKOUT_COLL.findOneAndUpdate({
                    _id: infoCheckIn._id,
                    logs: { $exists: true, $eq: [] }
                }, dataUpdate, { new: true });

                if(!infoAfterUpdate){
                    const infoAfterUpdate = await CHECKIN_CHECKOUT_COLL.findByIdAndUpdate(infoCheckIn._id, { 
                        error_log: ERROR_STORE, status: 1
                    });

                    return resolve({
                        error: true,
                        code: ERROR_STORE,
                        message: 'Cập nhật hình ảnh cửa hàng thất bại',
                        status: 422,
                        data: infoAfterUpdate
                    });
                }

                resolve({ error: false, data: infoAfterUpdate, status: 200 });
            } catch (error) {
                resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    /**
    * Check out
    * @param {objectID} checkinID
    * @param {float} lng
    * @param {float} lat
    * @param {objectID} userUpdate
    * @this {BaseModel}
    * @extends {BaseModel}
    * @returns {{ error: boolean, data?: object, message?: string }}
    */
    insertCheckout({ checkinID, lng, lat, timeEndShift, timeCheckin, userUpdate }) {
        return new Promise(async resolve => {
            try {
                if(!lng || !lat){
                    return resolve({
                        error: true,
                        message: 'Vui lòng cho phép truy cập vị trí hiện tại',
                        status: 400
                    });
                }

                if (!checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ',
                        status: 400
                    });
                }

                if (!checkObjectIDs([checkinID])) {
                    return resolve({
                        error: true,
                        message: 'ID check in không hợp lệ',
                        status: 400
                    });
                }

                let infoCheckIn = await CHECKIN_CHECKOUT_COLL.findById(checkinID).lean();
                if(!infoCheckIn){
                    return resolve({
                        error: true,
                        message: 'Check in không tồn tại',
                        status: 400
                    });
                } else{

                    let message = '';
                    switch (infoCheckIn.status) {
                        case 1:
                            message = 'Bạn chưa hoàn thành check in';
                            break;
                        case 3:
                            message = 'Bạn đã hoàn thành check out';
                            break;
                        case 4:
                            message = 'Check out đã quá hạn';
                            break;
                        default:
                            break;
                    }

                    if(message){
                        return resolve({
                            error: true,
                            message,
                            status: 400
                        });
                    }
                }

                let infoShift = await SHIFT_COLL.findById(infoCheckIn.shift);
                if(!infoShift){
                    return resolve({
                        error: true,
                        message: 'Không tìm thấy ca làm việc',
                        status: 400
                    });
                }

                let currentTime = timeUtils.parseTimeUTC(new Date(), true);
                let checkinTime = timeUtils.parseTimeUTC(infoCheckIn.logs[0].time, true);
                let shiftEndTime = timeUtils.parseTimeUTC(infoShift.endTime, true);

                if(timeEndShift){
                    timeEndShift = timeUtils.parseTimeUTC(timeEndShift, true);
                }

                if(timeCheckin){
                    timeCheckin = timeUtils.parseTimeUTC(timeCheckin, true);
                }

                console.log({ timeEndShift, timeCheckin })

                let checkTimeEndShift = timeUtils.calculateExpireTime(currentTime, timeEndShift || shiftEndTime);
                let totalTimeWork = timeUtils.calculateExpireTime(currentTime, timeCheckin || checkinTime);

                let startHours = timeUtils.parseTimeUTC(checkinTime).add(3.5, 'hours').format("HH:mm A");
                let endHours = timeUtils.parseTimeUTC(shiftEndTime).add(3, 'hours').format("HH:mm A");

                console.log({
                    totalTimeWork,
                    checkTimeEndShift,
                    currentTime,
                    checkinTime,
                    startHours,
                    endHours
                })

                // Muộn nhất: sau 3h so với time out chuẩn
                if(checkTimeEndShift < TIME_RANGE_CHECKIN_CHECKOUT.LATEST_CHECKOUT_TIME){
                    const infoAfterUpdate = await CHECKIN_CHECKOUT_COLL.findByIdAndUpdate(infoCheckIn._id, {
                        error_log: ERROR_TIME
                    }, { new: true });

                    return resolve({
                        error: true,
                        code: ERROR_TIME,
                        message: `Thời gian checkout không hợp lệ (thời gian hợp lệ từ ${startHours} đến ${endHours})`,
                        status: 400,
                        data: infoAfterUpdate
                    })
                }

                // Đảm bảo thời gian làm việc > 3,5 giờ
                if(totalTimeWork > TIME_RANGE_CHECKIN_CHECKOUT.WORK_TIME_GREATER_THAN){
                    const infoAfterUpdate = await CHECKIN_CHECKOUT_COLL.findByIdAndUpdate(infoCheckIn._id, {
                        error_log: ERROR_TIME
                    }, { new: true });

                    return resolve({
                        error: true,
                        code: ERROR_TIME,
                        message: 'Đảm bảo thời gian làm việc > 3,5 giờ',
                        status: 400,
                        data: infoAfterUpdate
                    })
                }

                let infoStore = await STORE_COLL.findById(infoCheckIn.store).select('code').lean();

                if(!['G5807436', 'G4700638', 'G5810768'].includes(infoStore.code)){
                    let checkDistanceStore = await STORE_COLL.aggregate([
                        {
                            $geoNear: {
                                near: { type: "Point", coordinates: [parseFloat(lng), parseFloat(lat)] },
                                maxDistance: 10000,
                                minDistance: 0,
                                distanceMultiplier: 0.001, // Returns km
                                distanceField: "dist.calculated",
                                includeLocs: "dist.location", // Returns distance
                                spherical: true
                            }
                        },
                        {
                            $match: {
                                _id: ObjectID(infoCheckIn.store), 
                                status: 1
                            }
                        }
                    ]);
    
                    if(!checkDistanceStore.length){
                        const infoAfterUpdate = await CHECKIN_CHECKOUT_COLL.findByIdAndUpdate(infoCheckIn._id, {
                            error_log: ERROR_LOCATION
                        }, { new: true });
    
                        return resolve({
                            error: true,
                            code: ERROR_LOCATION,
                            message: 'Vị trí hiện tại không nằm trong tọa độ định vị cho phép của cửa hàng (800m)',
                            status: 400,
                            data: infoAfterUpdate
                        });
                    }
                }

                let infoAfterUpdate = await CHECKIN_CHECKOUT_COLL.findByIdAndUpdate(infoCheckIn._id, {
                    $addToSet: {
                        logs: {
                            type: 2,
                            time: timeUtils.getCurrentTime()
                        }
                    },
                    status: 3,
                    error_log: null,
                    userUpdate
                }, { new: true });

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Check out thất bại',
                        status: 422
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    downloadCheckinCheckoutExcel({ 
        keyword,
        filter,
        condition,
        monthAndYear,
    }){
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1,
                    $or: [],
                    $and: []
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        location: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        note: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        platform: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);
                                console.log({
                                    conditionFieldRef
                                });
                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj.$and.push(conditionFieldRef);
                                    // conditionObj = {
                                    //     ...conditionObj,
                                    //     ...conditionFieldRef
                                    // };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);
                            
                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj.$and.push(conditionByFilter);
                                    // conditionObj = {
                                    //     ...conditionObj,
                                    //     ...conditionByFilter
                                    // };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            }
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }
                
                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }
                if (conditionObj.$and && !conditionObj.$and.length) {
                    delete conditionObj.$and;
                }

                // Nối chuỗi để thành ngày 1 của tháng mình chọn
                // let dateFromat = monthAndYear+`-01`;
                // const date     = new Date(dateFromat), y = date.getFullYear(), m = date.getMonth();
                // const firstDay = new Date(y, m, 1);
                // const lastDay  = new Date(y, m + 1, 0);
                let _fromDate   = moment(new Date(monthAndYear)).startOf('month').format();
                let _toDate     = moment(new Date(monthAndYear)).endOf('month').format();
                console.log({
                    _fromDate, _toDate,
                    monthAndYear
                })

                const listCheckin_checkoutByFilter = await CHECKIN_CHECKOUT_COLL.aggregate([
                    {
                        $match: {
                            ...conditionObj,
                            $or: [
                                { status: 2 }, // checkin thành côgng
                                { status: 3 }, // checkout thành công
                            ],
                            createAt: {
                                $gte: new Date(_fromDate),
                                $lte: new Date(_toDate)
                            },
                        }
                    },
                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'employee.distributor',
                            foreignField: '_id',
                            as: 'employee.distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee.distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'detail_type_employees',
                            localField: 'employee.detail_type_employee',
                            foreignField: '_id',
                            as: 'employee.detail_type_employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee.detail_type_employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'shifts',
                            localField: 'shift',
                            foreignField: '_id',
                            as: 'shift'
                        }
                    },
                    {
                        $unwind: {
                            path: '$shift',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                   
                    {
                        $sort: {
                            modifyAt: -1
                        }
                    }
                ]).allowDiskUse(true);;
                // console.log('listCheckin_checkoutByFilter: ', listCheckin_checkoutByFilter);
                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);

                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        workbook.sheet("Report").row(1).cell(1).value("Miền");
                        workbook.sheet("Report").row(1).cell(2).value("Vùng");
                        workbook.sheet("Report").row(1).cell(3).value("Mã Nhà Phân Phối");
                        workbook.sheet("Report").row(1).cell(4).value("Tên Nhà Phân Phối");
                        workbook.sheet("Report").row(1).cell(5).value("Mã Nhân viên NA/PG");
                        workbook.sheet("Report").row(1).cell(6).value("Mã nhân viên HR");
                        workbook.sheet("Report").row(1).cell(7).value("Tên Nhân Viên HR");
                        workbook.sheet("Report").row(1).cell(8).value("Loại NA");
                        workbook.sheet("Report").row(1).cell(9).value("Mã Cửa hàng");
                        workbook.sheet("Report").row(1).cell(10).value("Tên cửa hàng");
                        workbook.sheet("Report").row(1).cell(11).value("Ngày làm việc");
                        workbook.sheet("Report").row(1).cell(12).value("Ca làm việc");
                        workbook.sheet("Report").row(1).cell(13).value("Thời gian Checkin");
                        workbook.sheet("Report").row(1).cell(14).value("Thời gian Checkout");
                        workbook.sheet("Report").row(1).cell(15).value("Tổng thời gian làm việc");
                        workbook.sheet("Report").row(1).cell(16).value("Trạng thái chấm công");
                        workbook.sheet("Report").row(1).cell(17).value("Duyệt ca lỗi");
                        workbook.sheet("Report").row(1).cell(18).value("Đăng ký Phép");
                        workbook.sheet("Report").row(1).cell(19).value("Ghi chú");

                        // listCheckin_checkoutByFilter && listCheckin_checkoutByFilter.length && listCheckin_checkoutByFilter.map((item, index) => {
                        if (listCheckin_checkoutByFilter && listCheckin_checkoutByFilter.length) {
                            let index = 0;
                            for (const item of listCheckin_checkoutByFilter) {
                                workbook.sheet("Report").row(index + 2).cell(1).value(item.region && item.region.name);
                                workbook.sheet("Report").row(index + 2).cell(2).value(item.area   && item.area.name);
                                workbook.sheet("Report").row(index + 2).cell(3).value(item.employee && item.employee.distributor && item.employee.distributor.code);
                                workbook.sheet("Report").row(index + 2).cell(4).value(item.employee && item.employee.distributor && item.employee.distributor.name);
                                workbook.sheet("Report").row(index + 2).cell(5).value(item.employee && item.employee.code);

                                let createAtStartOfDate = moment(item.createAt).startOf('day').format();
                                let createAtEndOfDate = moment(item.createAt).endOf('day').format();
                                let infoEmployeeHr = await EMPLOYEE_HR_COLL.findOne({
                                    employee: item.employee._id,
                                    from: {
                                        $lte: createAtEndOfDate
                                    },
                                    to: {
                                        $gte: createAtStartOfDate
                                    }
                                }).select("code fullname");
                                workbook.sheet("Report").row(index + 2).cell(6).value(infoEmployeeHr && infoEmployeeHr.code);
                                workbook.sheet("Report").row(index + 2).cell(7).value(infoEmployeeHr && infoEmployeeHr.fullname);
                                workbook.sheet("Report").row(index + 2).cell(8).value(item.employee && item.employee.detail_type_employee && item.employee.detail_type_employee.name);
                                workbook.sheet("Report").row(index + 2).cell(9).value(item.store    && item.store.code);
                                workbook.sheet("Report").row(index + 2).cell(10).value(item.store   && item.store.name);
                                workbook.sheet("Report").row(index + 2).cell(11).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));
                                workbook.sheet("Report").row(index + 2).cell(12).value(item.shift && item.shift.name);
                                
                                let time_checkin;
                                let total_hour;
                                item.logs && item.logs.length && item.logs.map(log => {
                                    const CHECK_IN  = 1;
                                    const CHECK_OUT = 2;
                                    if (log.type == CHECK_IN) {
                                        time_checkin = log.time;
                                        workbook.sheet("Report").row(index + 2).cell(13).value(moment(log.time).format('HH:mm DD/MM/YYYY'));
                                    }
                                    if (log.type == CHECK_OUT) {
                                        if (time_checkin) {
                                            total_hour = Math.abs(log.time - time_checkin) / 36e5;
                                            total_hour = Number(total_hour.toFixed(2));
                                        }
                                        workbook.sheet("Report").row(index + 2).cell(14).value(moment(log.time).format('HH:mm DD/MM/YYYY'));
                                    }
                                });
                            
                                workbook.sheet("Report").row(index + 2).cell(15).value(total_hour);
                                workbook.sheet("Report").row(index + 2).cell(16).value(item.status && STATUS_CHECKIN_CHECKOUT_V2[item.status].value);
                                workbook.sheet("Report").row(index + 2).cell(17).value("");
                                workbook.sheet("Report").row(index + 2).cell(18).value("");
                                workbook.sheet("Report").row(index + 2).cell(19).value("");
                                index ++;
                            }

                            workbook.toFileAsync(pathWriteFile)
                                .then(_ => {
                                    // Download file from server
                                    return resolve({
                                        error: false,
                                        data: "/files/upload_excel_temp/" + fileNameRandom,
                                        path: fileNameRandom,
                                        status: 200
                                    });
                                });
                        }else{
                            resolve({ error: true, message:"Không tìm thấy dữ liệu" });
                        }
                    });
            } catch (error) {
                resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    downloadGeneralCheckinCheckoutExcel({ 
        keyword,
        filter,
        condition,
        monthAndYear
    }){
        return new Promise(async resolve => {
            try {

                let conditionObj = {
                    state: 1,
                    $or: [],
                    $and: []
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        location: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        note: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        deviceID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        platform: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }

                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);
                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj.$and.push(conditionFieldRef);
                                    // conditionObj = {
                                    //     ...conditionObj,
                                    //     ...conditionFieldRef
                                    // };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);
                            
                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj.$and.push(conditionByFilter);
                                    // conditionObj = {
                                    //     ...conditionObj,
                                    //     ...conditionByFilter
                                    // };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            }
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }

                // return console.log({ conditionObj });

                // return console.log({   
                //     keyword,
                //     filter,
                //     condition,
                // })

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }
                if (conditionObj.$and && !conditionObj.$and.length) {
                    delete conditionObj.$and;
                }
                
                // Nối chuỗi để thành ngày 1 của tháng mình chọn
                // let dateFromat = monthAndYear+`-01`;
                // const date     = new Date(dateFromat), y = date.getFullYear(), m = date.getMonth();
                // const firstDay = new Date(y, m, 1);
                // const lastDay  = new Date(y, m + 1, 0);
                let _fromDate   = moment(new Date(monthAndYear)).startOf('month').format();
                let _toDate     = moment(new Date(monthAndYear)).endOf('month').format();
                const listCheckin_checkoutByFilter = await CHECKIN_CHECKOUT_COLL.aggregate([
                    {
                        $match: {
                            createAt: {
                                $gte: new Date(_fromDate),
                                $lte: new Date(_toDate)
                            },
                            status: 3
                        }
                    },
                    {
                        $sort: {
                            createAt: 1
                        }
                    },
                    {
                        $group: {
                            _id: {
                                employee: '$employee', // distributorádasd
                            },
                            listData: {
                                $push: "$$ROOT"
                                // $addToSet: "$$CURRENT"
                            },
                        }      
                    },
                    {
                        $lookup: {
                            from: 'employees',
                            localField: '_id.employee',
                            foreignField: '_id',
                            as: '_id.employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$_id.employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'regions',
                            localField: '_id.employee.region',
                            foreignField: '_id',
                            as: '_id.employee.region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$_id.employee.region',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'areas',
                            localField: '_id.employee.area',
                            foreignField: '_id',
                            as: '_id.employee.area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$_id.employee.area',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'distributors',
                            localField: '_id.employee.distributor',
                            foreignField: '_id',
                            as: '_id.employee.distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$_id.employee.distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'detail_type_employees',
                            localField: '_id.employee.detail_type_employee',
                            foreignField: '_id',
                            as: '_id.employee.detail_type_employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$_id.employee.detail_type_employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'stores',
                            localField: '_id.store',
                            foreignField: '_id',
                            as: '_id.store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$_id.store',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'shifts',
                            localField: '_id.shift',
                            foreignField: '_id',
                            as: '_id.shift'
                        }
                    },
                    {
                        $unwind: {
                            path: '$_id.shift',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    // {
                    //     $match: conditionObj
                    // },
                ]);

                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Template_report_checkin.xlsx')))
                    .then(async workbook => {
                        const dateNow = new Date(monthAndYear);
                        let month = dateNow.getMonth() + 1;
                        let year  = dateNow.getFullYear();
                        let daysInMonth = (month, year) => {
                            return new Date(year, month, 0).getDate();
                        }
                        
                        let totalDayOfMonth = daysInMonth(month, year);
                        for (let index = 1; index <= totalDayOfMonth; index++) {
                            workbook.sheet("Report").row(3).cell(index + 11).value(`${index}/${month}`);
                        }

                        workbook.sheet("Report").row(3).cell(totalDayOfMonth + 12).value("Số ngày công theo kế hoạch");
                        workbook.sheet("Report").row(3).cell(totalDayOfMonth + 13).value("Số ngày công đạt chuẩn");
                        workbook.sheet("Report").row(3).cell(totalDayOfMonth + 14).value("Số ca lỗi DOE duyệt");
                        workbook.sheet("Report").row(3).cell(totalDayOfMonth + 15).value("Số ngày phép");
                        workbook.sheet("Report").row(3).cell(totalDayOfMonth + 16).value("Tổng ngày công có lương");
                        workbook.sheet("Report").row(3).cell(totalDayOfMonth + 17).value("Ghi chú");
                        
                        if (listCheckin_checkoutByFilter && listCheckin_checkoutByFilter.length) {
                            let index = 0;
                            for (const item of listCheckin_checkoutByFilter) {
                                if(item._id.employee.state == 1 && item._id.employee.status == 1 ){
                                    let createAtStartOfDate = moment(item._id.createAt).startOf('month').format();
                                    let createAtEndOfDate = moment(item._id.createAt).endOf('month').format();
                                    
                                    let listEmployeeHr = await EMPLOYEE_HR_COLL.find({
                                        employee: item._id.employee._id,
                                        from: {
                                            $lte: createAtEndOfDate
                                        },
                                        to: {
                                            $gte: createAtStartOfDate
                                        }
                                    }).select("code fullname from to");

                                    if(listEmployeeHr.length > 0){
                                        listEmployeeHr.forEach(infoEmployeeHr => {
                                            workbook.sheet("Report").row(index + 4).cell(1).value(index + 1);
                                            workbook.sheet("Report").row(index + 4).cell(2).value(item._id.employee.region && item._id.employee.region.code);
                                            workbook.sheet("Report").row(index + 4).cell(3).value(item._id.employee.area   && item._id.employee.area.code);
                                            workbook.sheet("Report").row(index + 4).cell(4).value(item._id.employee && item._id.employee.distributor && item._id.employee.distributor.code);
                                            workbook.sheet("Report").row(index + 4).cell(5).value(item._id.employee && item._id.employee.distributor && item._id.employee.distributor.name);
                                            workbook.sheet("Report").row(index + 4).cell(6).value(item._id.employee && item._id.employee.code);
                                        
                                            workbook.sheet("Report").row(index + 4).cell(7).value(infoEmployeeHr && infoEmployeeHr.code);
                                            workbook.sheet("Report").row(index + 4).cell(8).value(infoEmployeeHr && infoEmployeeHr.fullname);
                                            workbook.sheet("Report").row(index + 4).cell(9).value(infoEmployeeHr && infoEmployeeHr.from && moment(infoEmployeeHr.from).format('HH:mm DD/MM/YYYY'));
                                            workbook.sheet("Report").row(index + 4).cell(10).value(infoEmployeeHr && infoEmployeeHr.to && moment(infoEmployeeHr.to).format('HH:mm DD/MM/YYYY'));
                                            workbook.sheet("Report").row(index + 4).cell(11).value(item._id.employee && item._id.employee.detail_type_employee && item._id.employee.detail_type_employee.name);

                                            for (let day = 1; day <= totalDayOfMonth; day++) {
                                                let totalCheckinDone = 0;
                                                for (let checkin_done of item.listData) {
                                                    let createAtOfCheckInDone = checkin_done.createAt;
                                                    let dayOfCreateAtOfCheckInDone = createAtOfCheckInDone.getDate();

                                                    let checkinMappingWithFromToOfHR = (_fromDateOfHR, _toDateOfHR) => {
                                                        let condition1 = timeUtils.compareTwoTime(createAtOfCheckInDone, _fromDateOfHR) != 2;
                                                        let condition2 = timeUtils.compareTwoTime(createAtOfCheckInDone, _toDateOfHR) != 1;
                                                        return condition1 && condition2;
                                                    }

                                                    if (dayOfCreateAtOfCheckInDone == day && checkinMappingWithFromToOfHR(infoEmployeeHr.from, infoEmployeeHr.to)) {
                                                        totalCheckinDone += 0.5;
                                                        workbook.sheet("Report").row(index + 4).cell(day + 11).value(totalCheckinDone);
                                                    }
                                                }
                                            }
                                            index ++;
                                        })
                                    }else{
                                        // Trường hợp lấy nhân viên không có HR
                                        workbook.sheet("Report").row(index + 4).cell(1).value(index + 1);
                                        workbook.sheet("Report").row(index + 4).cell(2).value(item._id.employee.region && item._id.employee.region.code);
                                        workbook.sheet("Report").row(index + 4).cell(3).value(item._id.employee.area   && item._id.employee.area.code);
                                        workbook.sheet("Report").row(index + 4).cell(4).value(item._id.employee && item._id.employee.distributor && item._id.employee.distributor.code);
                                        workbook.sheet("Report").row(index + 4).cell(5).value(item._id.employee && item._id.employee.distributor && item._id.employee.distributor.name);
                                        workbook.sheet("Report").row(index + 4).cell(6).value(item._id.employee && item._id.employee.code);
                                        workbook.sheet("Report").row(index + 4).cell(11).value(item._id.employee && item._id.employee.detail_type_employee && item._id.employee.detail_type_employee.name);

                                        for (let day = 1; day <= totalDayOfMonth; day++) {
                                            let totalCheckinDone = 0;
                                            for (let checkin_done of item.listData) {
                                                let createAtOfCheckInDone = checkin_done.createAt;
                                                let dayOfCreateAtOfCheckInDone = createAtOfCheckInDone.getDate();
                                                
                                                // let checkinMappingWithFromToOfHR = (_fromDateOfHR, _toDateOfHR) => {
                                                //     let condition1 = timeUtils.compareTwoTime(createAtOfCheckInDone, _fromDateOfHR) != 2;
                                                //     let condition2 = timeUtils.compareTwoTime(createAtOfCheckInDone, _toDateOfHR) != 1;
                                                //     return condition1 && condition2;
                                                // }
                                                
                                                if (dayOfCreateAtOfCheckInDone == day) {
                                                    totalCheckinDone += 0.5;
                                                    workbook.sheet("Report").row(index + 4).cell(day + 11).value(totalCheckinDone);
                                                }
                                            }
                                        }
                                        index ++;
                                    }}
                            }

                            let fileNameRandom = "Report_checkin_"+moment(new Date()).format('LT') + "_" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                            let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                            workbook.toFileAsync(pathWriteFile)
                                .then(_ => {
                                    // Download file from server
                                    return resolve({
                                        error: false,
                                        data: "/files/upload_excel_temp/" + fileNameRandom,
                                        path: fileNameRandom,
                                        status: 200
                                    });
                                });
                        }else{
                            resolve({ error: true, message:"Không tìm thấy dữ liệu" });
                        }
                    });
            } catch (error) {
                resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }


}

exports.MODEL = new Model;
