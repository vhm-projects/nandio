"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { uploadSingle }                      = require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_CATEGORY } 			    = require('../constants/category.uri');

/**
 * MODELS
 */
const CATEGORY_MODEL   = require('../models/category').MODEL;
const { IMAGE_MODEL }  = require('../../image');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ====================== ************************ ================================
             * =====================      QUẢN LÝ DANH MỤC     ================================
             * ====================== ************************ ================================
             */

            /**
             * Function: Tạo Category (View, API)
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_CATEGORY.ADD_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    title: 'Add category',
                    type: 'view',
                    code: CF_ROUTINGS_CATEGORY.ADD_CATEGORY,
                    inc: path.resolve(__dirname, '../views/add_category.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function(req, res) {
                        ChildRouter.renderToView(req, res);
					}],
                    post: [ uploadSingle, async function (req, res) {
                        const { title, description, status, pathImage, pathGallerys } = JSON.parse(req.body.data);

                        let image = null;
                        if(req.file){
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size,
                                name: req.fileName,
                                path: pathImage[0]
                            });
                            image = infoImageAfterInsert.data._id;
                        }

                        const infoCategoryInsert = await CATEGORY_MODEL.insert({ title, description, status, image, pathGallerys });
                        res.json(infoCategoryInsert);
                    }]
                },
            },

            /**
             * Function: Xóa Category
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_CATEGORY.REMOVE_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { categoryID } = req.params;
                        const infoCategoryRemove = await CATEGORY_MODEL.remove({ categoryID });

                        res.json(infoCategoryRemove);
                    }]
                },
            },

            /**
             * Function: Cập nhật Category (View API)
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_CATEGORY.UPDATE_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    title: 'Update category',
                    type: 'view',
                    code: CF_ROUTINGS_CATEGORY.UPDATE_CATEGORY,
                    inc: path.resolve(__dirname, '../views/update_category.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function(req, res) {
                        let { categoryID } = req.query;
                        let infoCategoryUpdate = await CATEGORY_MODEL.getInfo({ categoryID });

                        ChildRouter.renderToView(req, res, { infoCategoryUpdate: infoCategoryUpdate.data });
					}],
                    post: [ uploadSingle, async function (req, res) {
                        const { categoryID } = req.query;
                        const { title, description, status, pathImage, pathGallerysUpdate, imgGalleryOldIsDeleted, arrObjectImageCTA } = JSON.parse(req.body.data);

                        if(arrObjectImageCTA && arrObjectImageCTA.length){
                            await IMAGE_MODEL.updateCTA({ arrObjectImageCTA });
                        }

                        let image = null;
                        if(req.file){
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size, 
                                name: req.fileName, 
                                path: pathImage[0]
                            });
                            image = infoImageAfterInsert.data._id;
                        }

                        const infoCategoryUpdate = await CATEGORY_MODEL.update({ categoryID, title, description, status, image, pathGallerysUpdate, imgGalleryOldIsDeleted });

                        res.json(infoCategoryUpdate);
                    }]
                },
            },

            /**
             * Function: Xem chi tiết Category
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_CATEGORY.DETAIL_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { categoryID } = req.params;

                        const infoDetailCategory = await CATEGORY_MODEL.getInfo({ categoryID });

                        res.json(infoDetailCategory);
                    }]
                },
            },

            /**
             * Function: Xem chi tiết Category (View)
             * Date: 20/06/2021
             * Dev: VyPQ
             */
             [CF_ROUTINGS_CATEGORY.INFO_DETAIL_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
                    view: 'index.ejs',
                    inc: path.resolve(__dirname, '../views/info_detail_category.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        const { categoryID } = req.params;

                        const infoDetailCategory = await CATEGORY_MODEL.getInfo({ categoryID });
                        
                        ChildRouter.renderToView(req, res, { infoDetailCategory: infoDetailCategory.data });
                    }]
                },
            },

            /**
             * Function: Xem danh sách Category
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_CATEGORY.LIST_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    title: "List Category - NANDIO",
                    code: CF_ROUTINGS_CATEGORY.LIST_CATEGORY,
                    inc: path.resolve(__dirname, '../views/list_category'),
                    view: 'index.ejs',
                    type: 'view'
                },
                methods: {
                    get: [ async function (req, res) {
                        const listCategory = await CATEGORY_MODEL.getList();

                        ChildRouter.renderToView(req, res, { listCategory: listCategory.data });
                    }]
                },
            },


            /**
             * Function: Danh sách danh mục enduser
             * Date: 20/06/2021
             * Dev: depv
             */
            [CF_ROUTINGS_CATEGORY.LIST_CATEGORY_FOR_ENDUSER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { limit, page } = req.query;
                        let listCategory = await CATEGORY_MODEL.getListForEnduser({ limit, page });
                        res.json(listCategory);
                    }]
                },
            },

            [CF_ROUTINGS_CATEGORY.UPDATE_CATEGORY_STATUS]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        const { stt, categoryID } = req.body;

                        const infoDetailBrand = await CATEGORY_MODEL.updateSTT({ categoryID, stt });

                        res.json(infoDetailBrand);
                    }]
                },
            },

        }
    }
};
