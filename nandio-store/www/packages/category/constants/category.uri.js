const BASE_ROUTE = '/category';

const CF_ROUTINGS_CATEGORY = {
	ADD_CATEGORY: `${BASE_ROUTE}/add-category`,
	REMOVE_CATEGORY: `${BASE_ROUTE}/remove-category/:categoryID`,
	UPDATE_CATEGORY: `${BASE_ROUTE}/update-category`,
	UPDATE_CATEGORY_STATUS: `${BASE_ROUTE}/update-category-status`,
	DETAIL_CATEGORY: `${BASE_ROUTE}/detail-category/:categoryID`,
	INFO_DETAIL_CATEGORY: `${BASE_ROUTE}/info-detail-category/:categoryID`,
	LIST_CATEGORY: `${BASE_ROUTE}/list-category`,
	LIST_CATEGORY_FOR_ENDUSER: `${BASE_ROUTE}/list-category-for-enduser`,
    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_CATEGORY = CF_ROUTINGS_CATEGORY;
