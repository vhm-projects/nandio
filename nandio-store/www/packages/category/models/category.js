"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { checkObjectIDs }            = require("../../../utils/utils");
/**
 * MODELS
 */
 const IMAGE_MODEL                   = require('../../image/models/image').MODEL;

/**
 * COLLECTIONS
 */
const CATEGORY_COLL  					= require('../databases/category-coll');


class Model extends BaseModel {
    constructor() {
        super(CATEGORY_COLL);
    }

	insert({ title, description, image, status, pathGallerys }) {
        return new Promise(async resolve => {
            try {
                if(!title || !description)
                    return resolve({ error: true, message: 'params_invalid' });

                // let isExistTitle = await CATEGORY_COLL.findOne({ title });
                // if(isExistTitle)
                //     return resolve({ error: true, message: 'title_existed' });

                let statusValid = [0, 1];
                if(!statusValid.includes(Number(status))) {
                    return resolve({ error: true, message: 'status_invalid' });
                }

                let dataInsert = { title, description, status, image };

                let infoCategoryInsert = await this.insertData(dataInsert);
                if(!infoCategoryInsert)
                    return resolve({ error: true, message: 'Thêm danh mục thất bại, hoặc tên danh mục đã tồn tại' });

                let arrGallery = [];
                if(pathGallerys && pathGallerys.length > 0) {
                    for(let imgGallery of pathGallerys) {
                        let infoImageGalleryAfterInsert = await IMAGE_MODEL.insert({
                            name: imgGallery,
                            path: imgGallery
                        });
                        arrGallery.push(infoImageGalleryAfterInsert.data._id);
                    }

                    if(checkObjectIDs(arrGallery)){
                        let infoCategoryUpdate = await CATEGORY_COLL.findByIdAndUpdate(infoCategoryInsert._id, {
                            $set: { gallerys: arrGallery }
                        }, { new: true });

                        if(!infoCategoryUpdate)
                            return resolve({ error: true, message: 'cannot_update_gallery_big_sale' });
                    }
                }

                return resolve({ error: false, data: infoCategoryInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    remove({ categoryID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoCategoryRemove = await CATEGORY_COLL.findByIdAndUpdate({ _id: categoryID }, {
                    status: 2
                });
                if(!infoCategoryRemove)
                    return resolve({ error: false, message: 'cannot_remove_category' });

                return resolve({ error: false, data: infoCategoryRemove });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ categoryID, title, description, status, image, pathGallerysUpdate, imgGalleryOldIsDeleted }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(categoryID) || (image && !ObjectID.isValid(image)))
                    return resolve({ error: true, message: 'params_invalid' });

                let isExistCategory = await CATEGORY_COLL.findById({ _id: categoryID });
                if(!isExistCategory)
                    return resolve({ error: true, message: 'category_does_not_exist' });

                // let isExistTitle = await CATEGORY_COLL.findOne({ _id: { $nin: categoryID }, title });
                // if(isExistTitle)
                //     return resolve({ error: true, message: 'title_existed' });

                let statusValid = [0, 1];
                if(status && !statusValid.includes(Number(status))) 
                    return resolve({ error: true, message: 'status_invalid' });

                let dataUpdate = { modifyAt: new Date() };

                title        && (dataUpdate.title = title);
                description  && (dataUpdate.description = description);
                status       && (dataUpdate.status = status);
                image        && (dataUpdate.image = image);   

                let infoCategoryAfterUpdate = await CATEGORY_COLL.findByIdAndUpdate({ _id: categoryID }, {
                    $set: dataUpdate
                }, { new: true });
                if(!infoCategoryAfterUpdate)
                    return resolve({ error: true, message: 'update_category_failed' });

                //Xóa khỏi hình ảnh cũ trong gallery cập nhật
                if(imgGalleryOldIsDeleted && imgGalleryOldIsDeleted.length){
                    for (let image of imgGalleryOldIsDeleted) {
                        infoCategoryAfterUpdate = await CATEGORY_COLL.findByIdAndUpdate(categoryID, {
                            $pull: { gallerys: image.id }
                        }, { new: true });
                    }
                }

                //Thêm các hình ảnh mới cập nhật
                if(pathGallerysUpdate && pathGallerysUpdate.length){
                    for(let image of pathGallerysUpdate) {
                        let infoImageAfterInsert = await IMAGE_MODEL.insert({
                            name: image,
                            path: image
                        });

                        // sau khi thêm hình ảnh thì thêm nó vào gallers của big sale đó luôn
                        infoCategoryAfterUpdate = await CATEGORY_COLL.findByIdAndUpdate(categoryID, {
                            $addToSet: { gallerys: infoImageAfterInsert.data._id }
                        }, { new: true });
                    }
                }

                return resolve({ error: false, data: infoCategoryAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateSTT({ categoryID, stt }) {
        return new Promise(async resolve => {
            try {
                
                let dataUpdate = {  modifyAt: new Date() }
                if(!checkObjectIDs(categoryID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }

                if (stt || stt != 0) {
                    dataUpdate.stt      = stt;
                }
                
                let infoAfterUpdate = await CATEGORY_COLL.findByIdAndUpdate(categoryID, {
                    ...dataUpdate
                }, {
                    new: true
                });
                if (!infoAfterUpdate) {
                    return resolve({ error: true, message: "cannot_update" });
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ categoryID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoCategory = await CATEGORY_COLL.findById({ _id: categoryID })
                    .populate('image')
                    .populate({
                        path: 'gallerys',
                        options: { sort: { order: 1 } },
                    })
                if(!infoCategory)
                    return resolve({ error: false, message: 'cannot_get_info_category' });

                return resolve({ error: false, data: infoCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList() {
        return new Promise(async resolve => {
            try {
                let listCategory = await CATEGORY_COLL.find({ status: { $in: [0, 1] }})
                    .populate('image gallerys')
                    .sort({ stt: 1, modifyAt: -1 })
                if(!listCategory)
                    return resolve({ error: true, message: 'cannot_get_List_category' });

                return resolve({ error: false, data: listCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }


    getListForEnduser({ limit = 10, page = 1}) {
        return new Promise(async resolve => {
            try {
                limit = Number(limit);
                page  = Number(page);

                let listCategory = await CATEGORY_COLL.find({ status: 1 })
                    .select("title image createAt")
                    .skip((page * limit) - limit)
                    .limit(limit)
                    .populate({
                        path: "image",
                        select: "path"
                    })
                    .populate({
                        path: "gallerys",
                        select: "path typeCTA valueCTA",
                        options: { sort: { order: 1 } },
                    })
                    .sort({ stt: 1, modifyAt: -1 })
                if(!listCategory)
                    return resolve({ error: true, message: 'cannot_get_List_category' });
                let totalItem = await CATEGORY_COLL.count({ status: 1 });
                let pages = Math.ceil(totalItem/limit);

                return resolve({ error: false, data: { listCategory, currentPage: page, totalPage: pages } });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
