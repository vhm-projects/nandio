"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                        = require("moment");
/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { checkObjectIDs }            = require("../../../utils/utils");

/**
 * MODELS
 */
 const IMAGE_MODEL                   = require('../../image/models/image').MODEL;

/**
 * COLLECTIONS
 */
const FLASH_SALE_COLL  				= require('../databases/flash_sale-coll');


class Model extends BaseModel {
    constructor() {
        super(FLASH_SALE_COLL);
    }

    /**
     * Thêm BigSale
     * Depv247
     */
	insert({ name, description, content, image, pathGallerys, typeDiscount, code, products, amountDiscount, startTime, endTime, status, userID }) {
        return new Promise(async resolve => {
            try {
                let typeDiscountByMoney = 1;
                let typeDiscountByPercent = 2;
                let typeDiscountAccept = [ typeDiscountByMoney, typeDiscountByPercent ];

                if(!name || !typeDiscount || !typeDiscountAccept.includes(Number(typeDiscount)))
                    return resolve({ error: true, message: 'params_invalid' });

                let isExistCode = await FLASH_SALE_COLL.findOne({ code });
                if(isExistCode)
                    return resolve({ error: true, message: 'code_existed' });

                /**
                 * CHECK FLASH TẠO CHUNG THỜI ĐIỂM
                 */
                 let isExistFlashSale = await this.getInfoFlashSaleForEnduser();
                if (!isExistFlashSale.error && isExistFlashSale.data) {
                    return resolve({ error: true, message: 'Hiện tại đã có Flash Sale đang chạy' });
                }

                let dataInsert = { name, typeDiscount, code };
                if(description){
                    dataInsert.description = description;
                }

                if(content){
                    dataInsert.content = content;
                }

                if(ObjectID.isValid(image)){
                    dataInsert.image = image;
                }

                if(typeDiscount == typeDiscountByMoney && amountDiscount){
                    amountDiscount = Number(amountDiscount.split(',').join(''));
                    dataInsert.value = amountDiscount;
                }

                if(typeDiscount == typeDiscountByPercent && amountDiscount){
                    amountDiscount = Number(amountDiscount.split(',').join(''));
                    dataInsert.value = amountDiscount;
                }

                if(checkObjectIDs(products)){
                    dataInsert.products = products;
                }

                if(startTime && endTime){
                    dataInsert.startTime = new Date(startTime);
                    dataInsert.endTime = new Date(endTime);
                }

                if(ObjectID.isValid(userID)){
                    dataInsert.userCreate = userID;
                }

                let statusValid = [0, 1];
                if(!statusValid.includes(Number(status)))
                    return resolve({ error: true, message: 'status_invalid' });

                dataInsert.status = Number(status);

                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_flash_sale_failed' });

                let arrGallery = [];
                if(pathGallerys && pathGallerys.length > 0) {
                    for(let imgGallery of pathGallerys) {
                        let infoImageGalleryAfterInsert = await IMAGE_MODEL.insert({
                            name: imgGallery,
                            path: imgGallery,
                            userCreate: userID
                        });
                        arrGallery.push(infoImageGalleryAfterInsert.data._id);
                    }

                    if(checkObjectIDs(arrGallery)){                    
                        let infoFlashSaleUpdate = await FLASH_SALE_COLL.findByIdAndUpdate({ _id: infoAfterInsert._id }, {
                            $set: { gallerys: arrGallery }
                        }, { new: true });
                        if(!infoFlashSaleUpdate)
                            return resolve({ error: true, message: 'cannot_update_gallery_flash_sale' });
                    }
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ flashSaleID, name, description, content, image, typeDiscount, products, amountDiscount, status, linkDiscounts, startTime, endTime, userID, 
        pathGallerysUpdate, imgGalleryOldIsDeleted }) {
        return new Promise(async resolve => {
            try {
                let typeDiscountByMoney = 1;
                let typeDiscountByPercent = 2;
                let typeDiscountAccept = [ typeDiscountByMoney, typeDiscountByPercent ];

                if(!ObjectID.isValid(flashSaleID) || !name || !typeDiscount || !typeDiscountAccept.includes(Number(Number(typeDiscount))))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataUpdate = { name, typeDiscount };
                if(description){
                    dataUpdate.description = description;
                }

                if(content){
                    dataUpdate.content = content;
                }

                if(ObjectID.isValid(image)){
                    dataUpdate.image = image;
                }

                if(typeDiscount == typeDiscountByMoney && amountDiscount){
                    amountDiscount = Number(amountDiscount.split(',').join(''));
                    dataUpdate.value = amountDiscount;
                }

                if(typeDiscount == typeDiscountByPercent && amountDiscount){
                    amountDiscount = Number(amountDiscount.split(',').join(''));
                    dataUpdate.value = amountDiscount;
                }

                if(checkObjectIDs(products)){
                    dataUpdate.products = products;
                }

                if(status){
                    dataUpdate.status = Number(status);
                }
                
                if(linkDiscounts){
                    dataUpdate.linkDiscounts = linkDiscounts;
                }

                if(startTime && endTime){
                    dataUpdate.startTime = startTime;
                    dataUpdate.endTime = endTime;
                }

                if(ObjectID.isValid(userID)){
                    dataUpdate.userUpdate = userID;
                }
                
                let infoAfterUpdate = await FLASH_SALE_COLL.findByIdAndUpdate(flashSaleID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, data: "update_failed" });

                //Xóa khỏi hình ảnh cũ trong gallery cập nhật
                if(imgGalleryOldIsDeleted && imgGalleryOldIsDeleted.length){
                    for (let image of imgGalleryOldIsDeleted) {
                        infoAfterUpdate = await FLASH_SALE_COLL.findByIdAndUpdate(flashSaleID, {
                            $pull: { gallerys: image.id }
                        }, { new: true });
                    }
                }

                //Thêm các hình ảnh mới cập nhật
                if(pathGallerysUpdate && pathGallerysUpdate.length){
                    for(let image of pathGallerysUpdate) {
                        let infoImageAfterInsert = await IMAGE_MODEL.insert({
                            name: image,
                            path: image,
                            userCreate: userID
                        });

                        // sau khi thêm hình ảnh thì thêm nó vào gallers của big sale đó luôn
                        infoAfterUpdate = await FLASH_SALE_COLL.findByIdAndUpdate(flashSaleID, {
                            $addToSet: { gallerys: infoImageAfterInsert.data._id }
                        }, { new: true });
                    }
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ flashSaleID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(flashSaleID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoFlashSale = await FLASH_SALE_COLL.findById(flashSaleID)
                    .populate('image gallerys')
                if(!infoFlashSale)
                    return resolve({ error: false, message: 'cannot_get_info_flash_sale' });

                return resolve({ error: false, data: infoFlashSale });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoFlashSaleForEnduser() {
        return new Promise(async resolve => {
            try {
                let infoFlashSale = await FLASH_SALE_COLL.findOne({
                    // startTime: {
                    //     $gte: new moment(dateCurrent).startOf("day")._d
                    // },
                    endTime: {
                        $gte: new Date()
                    },
                    status: 1
                })
                .select("typeDiscount products endTime value")
                .populate({
                    path: "products",
                    select: "name price status description avatar",
                    populate: {
                        path: "avatar",
                        select: "path",
                    }
                })
                // console.log({ infoFlashSale, abc:  new moment(dateCurrent).startOf("day")._d });
                if(!infoFlashSale)
                    return resolve({ error: false, message: 'cannot_get_info_flash_sale' });

                return resolve({ error: false, data: infoFlashSale });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListAdmin() {
        return new Promise(async resolve => {
            try {
                let listFlashSale = await FLASH_SALE_COLL.find({})
                    .populate('image gallerys')
                if(!listFlashSale)
                    return resolve({ error: true, message: 'cannot_get_list' });

                return resolve({ error: false, data: listFlashSale });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    remove({ flashSaleID }) {
        return new Promise(async resolve => {
            try {
                let infoFlashSaleRemove = await FLASH_SALE_COLL.findByIdAndRemove(flashSaleID);
                if(!infoFlashSaleRemove)
                    return resolve({ error: true, message: 'remove_big_sale_failed' });

                return resolve({ error: false, data: infoFlashSaleRemove });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListFlashSaleActive({ }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                let conditionSearch = {
                    status: STATUS_ACTIVE,
                    endTime: {
                        $gte: new Date()
                    }
                }
                let listFlashSale = await FLASH_SALE_COLL.findOne({ ...conditionSearch } ,{
                    typeDiscount: 1, products: 1, value: 1, name: 1
                });
               
                if(!listFlashSale)
                    return resolve({ error: true, message: "cannot_get" });
                return resolve({ error: false, data: listFlashSale });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;