const BIG_SALE_MODEL    = require('./models/big_sale').MODEL;
const FLASH_SALE_MODEL  = require('./models/flash_sale').MODEL;


const BIG_SALE_COLL     = require('./databases/big_sale-coll');
const FLASH_SALE_COLL   = require('./databases/flash_sale-coll');
const PROMOTION_ROUTES  = require('./apis/promotion');

module.exports = {
    PROMOTION_ROUTES,
    BIG_SALE_COLL,
    FLASH_SALE_COLL,
    BIG_SALE_MODEL,
    FLASH_SALE_MODEL
}