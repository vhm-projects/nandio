"use strict";

const Schema    = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION BIG SALE CỦA HỆ THỐNG
 * DESIGN BY DEPV247
 */
module.exports = BASE_COLL('flash_sale', {
	name: {
		type: String,
		trim: true,
		unique : true
	},
    description: {
		type: String,
		trim: true
	},

	content: {
		type: String,
	},
	//_________Hình ảnh khuyến mãi
	image: {
		type: Schema.Types.ObjectId,
		ref: "image"
	},
	//_________Thời gian bắt đầu khuyến mãi
	startTime: {
		type: Date,
	},

	//_________Thời gian kết thúc khuyến mãi
	endTime: {
		type: Date,
	},

	/**
	 * Loại giảm giá
	 * 1: Giảm giá theo tiền
	 * 2: Giảm giá theo %
	 */
	typeDiscount: {
		type: Number,
		default: 0
	},
	//_________Code khuyến mãi
	code: {
		type: String,
		trim: true,
		unique : true
	},
	//_________Khuyến mãi áp dụng cho sản phẩm nào
	products: [{
		type: Schema.Types.ObjectId,
		ref: 'product'
	}],
	//_________Giảm giá bằng tiền: VD 30.000 vnd(Bỏ thay bằng value)
	amountDiscountByMoney: {
		type: Number
	},

	//_________Giảm giá bằng tiền: VD 50%(Bỏ thay bằng value)
	amountDiscountByPercent: {
		type: Number
	},
	//_________Phần trăm hoặc số tiền sau khi giảm giá
	value: {
		type: Number,
		default: 0
	},
	/**
	 * Trạng thái khuyến mãi
	 * 0: Tắt hoạt động
	 * 1: Còn hoạt động
	 */
	status: {
		type: Number,
		default: 0
	},
	//_________Link tới các trang thương mại điện tử khác
	linkDiscount: {
		type: String
	},
	
	//_________Tổng số lượng click vào khuyến mãi
	amountClick: {
		type: Number
	},
	/**
	 * Mảng hình ảnh của Big sale
	 */
	gallerys: [{
		type: Schema.Types.ObjectId,
		ref: "image"
	}],
	userCreate: {
		type: Schema.Types.ObjectId,
		ref: "user"
	},
	userUpdate: {
		type: Schema.Types.ObjectId,
		ref: "user"
	},
});
