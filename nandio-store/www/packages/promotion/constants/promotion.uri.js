const BASE_ROUTE = '/promotion';
const API_ROUTE = '/api/promotion';
const API_BASE = '/api';

const CF_ROUTINGS_PROMOTION = {

	/**
	 * BIG SALE
	 */
	ADD_BIG_SALE: `${BASE_ROUTE}/add-big-sale`,
	UPDATE_BIG_SALE: `${BASE_ROUTE}/update-big-sale`,
	LIST_BIG_SALE: `${BASE_ROUTE}/list-big-sale`,
	INFO_BIG_SALE: `${BASE_ROUTE}/info-big-sale/:bigSaleID`,
	BIG_SALE: `${BASE_ROUTE}/bigsales`,
	REMOVE_BIG_SALE: `${BASE_ROUTE}/remove-big-sale/:bigSaleID`,
	API_LIST_BIG_SALE: `${API_ROUTE}/list-big-sale`,
	API_LIST_BIG_SALE_EXPIRING: `${API_ROUTE}/list-big-sale-expiring`,
	API_INFO_BIG_SALE: `${API_ROUTE}/info-big-sale/:bigSaleID`,
	API_SEARCH_CODE: `${API_BASE}/search-code/:code`,
	LIST_PRODUCT_OF_ALL_BIG_SALE: `${API_ROUTE}/product-of-all-big-sale`,
	/**
	 * FLASH SALE
	 */
	ADD_FLASH_SALE: `${BASE_ROUTE}/add-flash-sale`,
	UPDATE_FLASH_SALE: `${BASE_ROUTE}/update-flash-sale/:flashSaleID`,
	LIST_FLASH_SALE: `${BASE_ROUTE}/list-flash-sale`,
	INFO_FLASH_SALE: `${BASE_ROUTE}/info-flash-sale/:flashSaleID`,
	INFO_FLASH_SALE_ENDUSER: `${API_ROUTE}/info-flash-sale-for-enduser`,
	FLASH_SALE: `${BASE_ROUTE}/flashsales`,
	REMOVE_FLASH_SALE: `${BASE_ROUTE}/remove-flash-sale/:flashSaleID`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_PROMOTION = CF_ROUTINGS_PROMOTION;
