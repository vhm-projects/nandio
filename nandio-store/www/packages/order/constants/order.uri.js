const BASE_ROUTE = '/order';

const CF_ROUTINGS_ORDER = {
    GET_LIST_ORDER_SERVER_SIDE: `${BASE_ROUTE}/list-order-server-side`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_ORDER = CF_ROUTINGS_ORDER;
