"use strict";

/**
 * EXTERNAL PACKAGE
 */

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const { CF_ROUTINGS_ORDER } 			    = require('../constants/order.uri');

/**
 * MODELS
 */
const ORDER_MODEL 		                    = require('../models/order').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ ORDER ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: Get List order Server Side (API)
             * Date: 19/12/2021
             * Dev: Automatic
             */
             [CF_ROUTINGS_ORDER.GET_LIST_ORDER_SERVER_SIDE]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            start,
                            length,
                            order,
                            customerID
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listOrderServerSide = await ORDER_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            page,
                            limit: length,
                            field,
                            customerID,
                            dir
                        });
                        res.json(listOrderServerSide);
                    }]
                },
            },

        }
    }
};
