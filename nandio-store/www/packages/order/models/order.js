"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      		= require('mongoose').Types.ObjectId;
const moment                      			= require('moment');
const formatCurrency 	                    = require('number-format.js');

/**
 * INTERNAL PACKAGES
 */
const { checkObjectIDs }					= require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel 							= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const ORDER_COLL  							= require('../databases/order-coll');


class Model extends BaseModel {
    constructor() {
        super(ORDER_COLL);
    }

	getListByFilterServerSide({
        keyword,
        filter,
        condition,
        page,
        limit,
        field,
        dir,
        customerID
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    $or: []
                };

                // if (keyword) {
                //     let key = keyword.split(" ");
                //     key = '.*' + key.join(".*") + '.*';

                //     conditionObj.$or = [{
                //         order: {
                //             $regex: key,
                //             $options: 'i'
                //         }
                //     }, {
                //         deviceType: {
                //             $regex: key,
                //             $options: 'i'
                //         }
                //     }, {
                //         deviceID: {
                //             $regex: key,
                //             $options: 'i'
                //         }
                //     }, {
                //         deviceName: {
                //             $regex: key,
                //             $options: 'i'
                //         }
                //     }, {
                //         lat: {
                //             $regex: key,
                //             $options: 'i'
                //         }
                //     }, {
                //         lng: {
                //             $regex: key,
                //             $options: 'i'
                //         }
                //     }]
                // }

                // if (filter && filter.length) {
                //     if (filter.length > 1) {

                //         filter.map(filterObj => {
                //             if (filterObj.type === 'ref') {
                //                 const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                //                 if (condition === 'OR') {
                //                     conditionObj.$or.push(conditionFieldRef);
                //                 } else {
                //                     conditionObj = {
                //                         ...conditionObj,
                //                         ...conditionFieldRef
                //                     };
                //                 }
                //             } else {
                //                 const conditionByFilter = this.getConditionObj(filterObj);

                //                 if (condition === 'OR') {
                //                     conditionObj.$or.push(conditionByFilter);
                //                 } else {
                //                     conditionObj = {
                //                         ...conditionObj,
                //                         ...conditionByFilter
                //                     };
                //                 }
                //             }
                //         });

                //     } else {
                //         let {
                //             type,
                //             ref,
                //             fieldRefName
                //         } = filter[0];

                //         if (type === 'ref') {
                //             conditionObj = {
                //                 ...conditionObj,
                //                 ...this.getConditionObj(ref, fieldRefName)
                //             };
                //         } else {
                //             conditionObj = {
                //                 ...conditionObj,
                //                 ...this.getConditionObj(filter[0])
                //             };
                //         }
                //     }
                // }

                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                if(customerID && checkObjectIDs(customerID)){
                    conditionObj.customer = customerID;
                }

                const skip = (page - 1) * limit;
                const totalOrder = await ORDER_COLL.countDocuments(conditionObj);

                if(customerID && checkObjectIDs(customerID)){
                    delete conditionObj.customer;
                    conditionObj['customer._id'] = ObjectID(customerID);
                }
                
                const listOrderByFilter = await ORDER_COLL.aggregate([
                    {
                        $lookup: {
                            from: 'customers',
                            localField: 'customer',
                            foreignField: '_id',
                            as: 'customer'
                        }
                    },
                    {
                        $unwind: {
                            path: '$customer',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $lookup: {
                            from: 'addresses',
                            localField: 'address',
                            foreignField: '_id',
                            as: 'address'
                        }
                    },
                    {
                        $unwind: {
                            path: '$address',
                            preserveNullAndEmptyArrays: true
                        },
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listOrderByFilter) {
                    return resolve({
                        recordsTotal: totalOrder,
                        recordsFiltered: totalOrder,
                        data: []
                    });
                }

                const listOrderDataTable = listOrderByFilter.map((order, index) => {
					let paymentText = '';
					let statusText = '';
					let statusColor = '';

					switch (order.payment) {
						case 1:
							paymentText = 'Thanh toán qua thẻ ATM';
							break;
						case 2:
							paymentText = 'Thanh toán khi nhận hàng';
							break;
						default:
							paymentText = 'Hình thức nhận hàng không hợp lệ';
							break;
					}

					switch (+order.status) {
						case 0:
							statusText = 'Dang xử lý';
							statusColor = 'badge-danger';
							break;
						case 2:
							statusText = 'Đã nhận hàng';
							statusColor = 'badge-danger';
							break;
						case 3:
							statusText = 'Đang giao';
							statusColor = 'badge-info';
							break;
						case 4:
							statusText = 'Đã giao thành công';
							statusColor = 'badge-success';
							break;
						default:
							statusText = 'Trạng thái không hợp lệ';
							statusColor = 'badge-danger';
							break;
					}

                    return {
                        indexSTT: skip + index + 1,
                        orderID: order.orderID ? order.orderID : '',
                        customer: order.customer ? order.customer.fullname : '',
                        amount: formatCurrency('###,###.', order.amount),
                        deliveryPrice: formatCurrency('###,###.', order.deliveryPrice),
                        deliveryDate: order.deliveryDate ? moment(order.deliveryDate).format('DD/MM/YYYY') : '',
                        address: order.address ? order.address.address : '',
                        payment: paymentText,
                        status: `
							<span class="badge badge-pill ${statusColor}" style="padding: 5px; font-size: 10px;">
								${statusText}
							</span>
						`,
                        createAt: moment(order.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalOrder,
                    recordsFiltered: totalOrder,
                    data: listOrderDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

	getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }


}

exports.MODEL = new Model;
