/**
 * Trạng thái 
 * 1: Hoạt động
 * 2: Không hoạt động
 */
exports.STATUS_AREA_TYPE = {

    "1": {
        value: "Hoạt động",
        color: "#0b51b7"
    },

    "2": {
        value: "Không hoạt động",
        color: "#d63031"
    },

};