const BASE_ROUTE = '/area';
const API_BASE_ROUTE = '/api/area';

const CF_ROUTINGS_AREA = {
    ADD_AREA: `${BASE_ROUTE}/add-area`,
    UPDATE_AREA_BY_ID: `${BASE_ROUTE}/update-area-by-id`,
    DELETE_AREA_BY_ID: `${BASE_ROUTE}/delete/:areaID`,

    GET_INFO_AREA_BY_ID: `${BASE_ROUTE}/info/:areaID`,
    GET_LIST_AREA: `${BASE_ROUTE}/list-area`,
    GET_LIST_AREA_BY_FIELD: `${BASE_ROUTE}/list-area/:field/:value`,
    GET_LIST_AREA_SERVER_SIDE: `${BASE_ROUTE}/list-area-server-side`,

    UPDATE_AREA_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-area-by-id-v2`,
    DELETE_AREA_BY_LIST_ID: `${BASE_ROUTE}/delete-area-by-list-id`,
    GET_LIST_AREA_EXCEL: `${BASE_ROUTE}/list-area-excel`,

    API_GET_LIST_AREAS: `${API_BASE_ROUTE}/list-areas`,
    API_GET_INFO_AREA: `${API_BASE_ROUTE}/info-area/:areaID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_AREA = CF_ROUTINGS_AREA;