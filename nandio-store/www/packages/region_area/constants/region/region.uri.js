const BASE_ROUTE = '/region';
const API_BASE_ROUTE = '/api/region';

const CF_ROUTINGS_REGION = {
    ADD_REGION: `${BASE_ROUTE}/add-region`,
    UPDATE_REGION_BY_ID: `${BASE_ROUTE}/update-region-by-id`,
    DELETE_REGION_BY_ID: `${BASE_ROUTE}/delete/:regionID`,

    GET_INFO_REGION_BY_ID: `${BASE_ROUTE}/info/:regionID`,
    GET_LIST_REGION: `${BASE_ROUTE}/list-region`,
    GET_LIST_REGION_BY_FIELD: `${BASE_ROUTE}/list-region/:field/:value`,
    GET_LIST_REGION_SERVER_SIDE: `${BASE_ROUTE}/list-region-server-side`,

    UPDATE_REGION_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-region-by-id-v2`,
    DELETE_REGION_BY_LIST_ID: `${BASE_ROUTE}/delete-region-by-list-id`,
    GET_LIST_REGION_EXCEL: `${BASE_ROUTE}/list-region-excel`,

    API_GET_LIST_REGIONS: `${API_BASE_ROUTE}/list-regions`,
    API_GET_INFO_REGION: `${API_BASE_ROUTE}/info-region/:regionID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_REGION = CF_ROUTINGS_REGION;