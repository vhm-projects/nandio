"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('area', {
    idSql: Number,
    id: {
        type: String,
        unique: true,
        trim: true
    },
    idRegion: {
        type: String,
        trim: true
    },
    /**
     * Tên vùng
     */
    name: {
        type: String,
        required: true,
    },
    /**
     * Mã vùng
     */
    code: {
        type: String,
        required: true,
    },
    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
    /**
     * Thứ tự
     */
    order: {
        type: Number,
        default: 1,
    },
});