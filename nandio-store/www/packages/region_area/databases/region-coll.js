"use strict";

const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('region', {
    idSql: Number,
    id: {
        type: String,
        unique: true,
        trim: true
    },
    /**
     * Tên miền
     */
    name: {
        type: String,
        required: true,
    },
    /**
     * Mã miền
     */
    code: {
        type: String,
        required: true,
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        enum: [1, 2],
    },
    /**
     * Thứ tự
     */
    order: {
        type: Number,
        default: 1,
    },
});