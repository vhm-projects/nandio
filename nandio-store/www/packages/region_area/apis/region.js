"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const beautifyer = require('js-beautify').js_beautify;
const fs = require('fs');
const moment = require('moment');
const logger = require('../../../config/logger/winston.config');
const chalk = require('chalk');
const log = console.log;
/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_REGION_TYPE,
} = require('../constants/region');
const {
    CF_ROUTINGS_REGION
} = require('../constants/region/region.uri');

/**
 * MODELS
 */
const REGION_MODEL = require('../models/region').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ REGION  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Region (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_REGION.ADD_REGION]: {
                config: {
                    scopes: ['create:region'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Region',
                    code: CF_ROUTINGS_REGION.ADD_REGION,
                    inc: path.resolve(__dirname, '../views/region/add_region.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_REGION
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            name,
                            code,
                        } = req.body;


                        let infoAfterInsert = await REGION_MODEL.insert({
                            name,
                            code,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Region By Id (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_REGION.UPDATE_REGION_BY_ID]: {
                config: {
                    scopes: ['update:region'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Region',
                    code: CF_ROUTINGS_REGION.UPDATE_REGION_BY_ID,
                    inc: path.resolve(__dirname, '../views/region/update_region.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            regionID
                        } = req.query;

                        let infoRegion = await REGION_MODEL.getInfoById(regionID);
                        if (infoRegion.error) {
                            return res.redirect('/something-went-wrong');
                        }



                        ChildRouter.renderToView(req, res, {
                            infoRegion: infoRegion.data || {},


                            CF_ROUTINGS_REGION
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            regionID,
                            name,
                            code,
                            status,
                            order,
                        } = req.body;


                        const infoAfterUpdate = await REGION_MODEL.update({
                            regionID,
                            name,
                            code,
                            status,
                            order,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Region By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_REGION.UPDATE_REGION_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:region'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            regionID,
                            name,
                            code,
                            status,
                            order,
                        } = req.body;


                        const infoAfterUpdate = await REGION_MODEL.updateNotRequire({
                            regionID,
                            name,
                            code,
                            status,
                            order,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Region By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_REGION.DELETE_REGION_BY_ID]: {
                config: {
                    scopes: ['delete:region'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            regionID
                        } = req.params;

                        const infoAfterDelete = await REGION_MODEL.deleteById(regionID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Region By List Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_REGION.DELETE_REGION_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:region'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            regionID
                        } = req.body;

                        const infoAfterDelete = await REGION_MODEL.deleteByListId(regionID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Region By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_REGION.GET_INFO_REGION_BY_ID]: {
                config: {
                    scopes: ['read:info_region'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            regionID
                        } = req.params;

                        const infoRegionById = await REGION_MODEL.getInfoById(regionID);
                        res.json(infoRegionById);
                    }]
                },
            },

            /**
             * Function: Get List Region (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_REGION.GET_LIST_REGION]: {
                config: {
                    scopes: ['read:list_region'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách miền',
                    code: CF_ROUTINGS_REGION.GET_LIST_REGION,
                    inc: path.resolve(__dirname, '../views/region/list_regions.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            status,
                            typeGetList
                        } = req.query;

                        let listRegions = [];
                        if (typeGetList === 'FILTER') {
                            listRegions = await REGION_MODEL.getListByFilter({
                                keyword,
                                status,
                            });
                        } else {
                            listRegions = await REGION_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listRegions: listRegions.data || [],
                            STATUS_REGION_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Region By Field (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_REGION.GET_LIST_REGION_BY_FIELD]: {
                config: {
                    scopes: ['read:list_region'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Region by field isStatus',
                    code: CF_ROUTINGS_REGION.GET_LIST_REGION_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/region/list_regions.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            status,
                            type
                        } = req.query;

                        let listRegions = await REGION_MODEL.getListByFilter({
                            keyword,
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listRegions: listRegions.data || [],
                            STATUS_REGION_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Region Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_REGION.GET_LIST_REGION_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_region'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listRegionServerSide = await REGION_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listRegionServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Region Excel Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_REGION.GET_LIST_REGION_EXCEL]: {
                config: {
                    scopes: ['read:list_region'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            filter,
                            condition,
                            nameOfParentColl,
                            objFilterStatic,
                            order,
                            keyword
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let conditionObj = REGION_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword)
                        let listRegion = await REGION_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice
                        })

                        res.json(listRegion)
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_REGION.API_GET_LIST_REGIONS]: {
                config: {
                    scopes: ['read:list_region'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listRegions = await REGION_MODEL.getListRegions({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listRegions);
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_REGION.API_GET_INFO_REGION]: {
                config: {
                    scopes: ['read:info_region'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            regionID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoRegion = await REGION_MODEL.getInfoRegion({
                            regionID,
                            select,
                            explain
                        });
                        res.json(infoRegion);
                    }]
                },
            },

        }
    }
};