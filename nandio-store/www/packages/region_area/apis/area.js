"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_AREA_TYPE,
} = require('../constants/area');
const {
    CF_ROUTINGS_AREA
} = require('../constants/area/area.uri');

/**
 * MODELS
 */
const AREA_MODEL = require('../models/area').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */
const REGION_COLL = require('../databases/region-coll');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ AREA  ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: Insert Area (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_AREA.ADD_AREA]: {
                config: {
                    scopes: ['create:area'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Area',
                    code: CF_ROUTINGS_AREA.ADD_AREA,
                    inc: path.resolve(__dirname, '../views/area/add_area.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let listRegions = await REGION_COLL.find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                order: 1,
                                modifyAt: -1
                            })
                            .lean()

                        ChildRouter.renderToView(req, res, {
                            listRegions,
                            CF_ROUTINGS_AREA
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            name,
                            code,
                            region,
                        } = req.body;

                        let infoAfterInsert = await AREA_MODEL.insert({
                            name,
                            code,
                            region,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Area By Id (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_AREA.UPDATE_AREA_BY_ID]: {
                config: {
                    scopes: ['update:area'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Area',
                    code: CF_ROUTINGS_AREA.UPDATE_AREA_BY_ID,
                    inc: path.resolve(__dirname, '../views/area/update_area.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            areaID
                        } = req.query;

                        let infoArea = await AREA_MODEL.getInfoById(areaID);
                        if (infoArea.error) {
                            return res.redirect('/something-went-wrong');
                        }

                        let listRegions = await REGION_COLL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean()

                        ChildRouter.renderToView(req, res, {
                            infoArea: infoArea.data || {},
                            listRegions,
                            CF_ROUTINGS_AREA
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            areaID,
                            name,
                            code,
                            region,
                            status,
                            order,
                        } = req.body;

                        const infoAfterUpdate = await AREA_MODEL.update({
                            areaID,
                            name,
                            code,
                            region,
                            status,
                            order,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Area By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_AREA.UPDATE_AREA_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:area'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            areaID,
                            name,
                            code,
                            region,
                            status,
                            order,
                        } = req.body;

                        const infoAfterUpdate = await AREA_MODEL.updateNotRequire({
                            areaID,
                            name,
                            code,
                            region,
                            status,
                            order,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Area By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_AREA.DELETE_AREA_BY_ID]: {
                config: {
                    scopes: ['delete:area'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            areaID
                        } = req.params;

                        const infoAfterDelete = await AREA_MODEL.deleteById(areaID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Area By List Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_AREA.DELETE_AREA_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:area'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            areaID
                        } = req.body;

                        const infoAfterDelete = await AREA_MODEL.deleteByListId(areaID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Area By Id (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_AREA.GET_INFO_AREA_BY_ID]: {
                config: {
                    scopes: ['read:info_area'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            areaID
                        } = req.params;

                        const infoAreaById = await AREA_MODEL.getInfoById(areaID);
                        res.json(infoAreaById);
                    }]
                },
            },

            /**
             * Function: Get List Area (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_AREA.GET_LIST_AREA]: {
                config: {
                    scopes: ['read:list_area'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách vùng',
                    code: CF_ROUTINGS_AREA.GET_LIST_AREA,
                    inc: path.resolve(__dirname, '../views/area/list_areas.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            status,
                            typeGetList
                        } = req.query;

                        let listAreas = [];
                        if (typeGetList === 'FILTER') {
                            listAreas = await AREA_MODEL.getListByFilter({
                                keyword,
                                status,
                            });
                        } else {
                            listAreas = await AREA_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listAreas: listAreas.data || [],
                            STATUS_AREA_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Area By Field (API, VIEW)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_AREA.GET_LIST_AREA_BY_FIELD]: {
                config: {
                    scopes: ['read:list_area'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Area by field isStatus',
                    code: CF_ROUTINGS_AREA.GET_LIST_AREA_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/area/list_areas.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            status,
                            type
                        } = req.query;

                        let listAreas = await AREA_MODEL.getListByFilter({
                            keyword,
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listAreas: listAreas.data || [],
                            STATUS_AREA_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Area Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_AREA.GET_LIST_AREA_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_area'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listAreaServerSide = await AREA_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listAreaServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Area Excel Server Side (API)
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_AREA.GET_LIST_AREA_EXCEL]: {
                config: {
                    scopes: ['read:list_area'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            filter,
                            condition,
                            nameOfParentColl,
                            objFilterStatic,
                            order,
                            keyword
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let conditionObj = AREA_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword)
                        let listArea = await AREA_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });
                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice
                        })

                        res.json(listArea)
                    }]
                },
            },

            /**
             * Function: Danh sách
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_AREA.API_GET_LIST_AREAS]: {
                config: {
                    scopes: ['read:list_area'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listAreas = await AREA_MODEL.getListAreas({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listAreas);
                    }]
                },
            },

            /**
             * Function: Chi tiết
             * Date: 28/10/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_AREA.API_GET_INFO_AREA]: {
                config: {
                    scopes: ['read:info_area'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            areaID
                        } = req.params;
                        const {
                            select,
                            explain
                        } = req.query;

                        const infoArea = await AREA_MODEL.getInfoArea({
                            areaID,
                            select,
                            explain
                        });
                        res.json(infoArea);
                    }]
                },
            },

        }
    }
};