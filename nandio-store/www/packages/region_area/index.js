const REGION_COLL = require('./databases/region-coll');
const REGION_MODEL = require('./models/region').MODEL;
const REGION_ROUTES = require('./apis/region');

const AREA_COLL  = require('./databases/area-coll');
const AREA_MODEL  = require('./models/area').MODEL;
const AREA_ROUTES  = require('./apis/area');
    
// MARK REQUIRE

module.exports = {
    REGION_COLL,
    REGION_MODEL,
    REGION_ROUTES,

    AREA_COLL,
    AREA_MODEL,
    AREA_ROUTES,
    
    // MARK EXPORT
}