"use strict";

const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('manage_money', {
    /**
     * Số tiền chương trình 1
     */
    moneyOne: {
        type: Number,
        required: true,
    },
    /**
     * Số tiền chương trình 2
     */
    moneySecond: {
        type: Number,
        required: true,
    },
});