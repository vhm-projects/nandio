/**
 * EXTERNAL PACKAGE
 */

/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs, cleanObject }   = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel                         = require('../../../models/intalize/base_model');

/**
 * COLLECTION
 */
const MANAGE_MONEY__COLL                = require('../databases/manage_money-coll');

class Model extends BaseModel {
    constructor() {
        super(MANAGE_MONEY__COLL)
    }

   /**
     * Tạo mới hoặc cập nhật money
     * @param {object} moneyOne
     * @param {object} moneySecond
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    insertOrUpdate({ moneyOne, moneySecond, userCreate }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ',
                        status: 400
                    });
                }

                if (!moneyOne || !moneySecond) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập số tiền 1 và 2',
                        status: 400
                    });
                }

                const infoAfterUpdate = await MANAGE_MONEY__COLL.findOneAndUpdate({
                    $or: [
                        { moneyOne: { $exists: true } },
                        { moneySecond: { $exists: true } }
                    ]
                }, {
                    $set: {
                        moneyOne,
                        moneySecond
                    },
                    $setOnInsert: {
                        userCreate
                    }
                }, { upsert: true, new: true })

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật tiền thất bại',
                        status: 422
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy thông tin money
     * @param {objectId} moneyID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(moneyID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([moneyID])) {
                    return resolve({
                        error: true,
                        message: 'moneyID không hợp lệ',
                        status: 400
                    });
                }

                const infoMoney = await MANAGE_MONEY__COLL.findById(moneyID).lean();

                if (!infoMoney) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin tiền',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoMoney,
                    status: 200
                });
            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }
}

module.exports.MODEL = new Model;