const MANAGE_MONEY_COLL = require('./databases/manage_money-coll');
const MANAGE_MONEY_MODEL = require('./models/manage_money').MODEL;
const MANAGE_MONEY_ROUTES = require('./apis/manage_money');
// MARK REQUIRE

module.exports = {
    MANAGE_MONEY_COLL,
    MANAGE_MONEY_MODEL,
    MANAGE_MONEY_ROUTES,
    // MARK EXPORT
}