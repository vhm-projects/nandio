/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const { CF_ROUTINGS_MANAGE_MONEY } = require('../constants/manage_money/manage_money.uri');

/**
 * MODELS
 */
const MANAGE_MONEY__COLL = require('../databases/manage_money-coll');
const { MODEL: MANAGE_MONEY__MODEL } = require('../models/manage_money');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ MONEY ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: Insert Money (API, VIEW)
             * Date: 14/04/2022
             * Dev: MinhVH
             */
            [CF_ROUTINGS_MANAGE_MONEY.ADD_OR_UPDATE_MANAGE_MONEY]: {
                config: {
                    scopes: ['create:manage_money'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm tiền chương trình',
                    code: CF_ROUTINGS_MANAGE_MONEY.ADD_OR_UPDATE_MANAGE_MONEY,
                    inc: path.resolve(__dirname, '../views/manage_money/add_update_manage_money.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        const moneyInfo = await MANAGE_MONEY__COLL.findOne();
                        ChildRouter.renderToView(req, res, {
                            moneyInfo,
                            CF_ROUTINGS_MANAGE_MONEY
                        });
                    }],
                    post: [async function(req, res) {
                        const userCreate = req.user && req.user._id;
                        const { moneyOne, moneySecond } = req.body;

                        const response = await MANAGE_MONEY__MODEL.insertOrUpdate({
                            moneyOne, moneySecond, userCreate
                        });
                        res.status(response.status).json(response);
                    }]
                },
            },

            /**
             * Function: Get Info money (API)
             * Date: 14/04/2022
             * Dev: MinhVH
             */
            [CF_ROUTINGS_MANAGE_MONEY.GET_INFO_MANAGE_MONEY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(_, res) {
                        const response = await MANAGE_MONEY__COLL.findOne();
                        if (!response) 
                            return res.status(200).json({ data: null, error: true, message: 'cannot_get_info_campaign' });
                        return res.status(200).json({ data: response, error: false });
                    }]
                },
            },

        }
    }
};