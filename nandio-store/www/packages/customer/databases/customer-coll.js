"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('customer', {

    /**
     * Mã khách hàng
     */
    code: {
        type: String,
        unique: true,
        required: true,
    },
    /**
     * Họ và tên
     */
    fullname: {
        type: String,
        required: true,
    },
    /**
     * Email
     */
    email: {
        type: String,
        unique: true,
    },
    /**
     * Số điện thoại
     */
    phone: {
        type: String,
        unique: true,
    },
    /**
     * Ngày sinh
     */
    birthday: {
        type: Date,
    },
    /**
     * Mật khẩu
     */
    password: {
        type: String,
    },
    /**
     * Điểm xếp hạng khách hàng
     */
    pointRanking: {
        type: Number,
    },
    /**
     * Điểm hiện tại
     */
    point: {
        type: Number,
    },
    /**
     * Ảnh đại diện
     */
    avatar: {
        type: Schema.Types.Mixed,
    },
    /**
     * Code thay đổi email
     */
    codeChangeEmail: {
        type: String,
    },
    /**
     * Picture FIREBASE GỬI VỀ
     */
    picture: {
        type: String,
    },
    /**
     * id FIREBASE GỬI VỀ
     */
    id: {
        type: String,
    },
    /**
     * birthday FIREBASE GỬI VỀ
     */
    birthday1: {
        type: String,
    },
    /**
     * token FIREBASE GỬI VỀ
     */
    token: {
        type: String,
    },
    /**
     * Loại khách hàng
     */
    type: {
        type: String,
    },
    /**
     * googleUID
     */
    googleUID: {
        type: String,
    },
    /**
     * facebookUID
     */
    facebookUID: {
        type: String,
    },
    /**
     * appleUID
     */
    appleUID: {
        type: String,
    },
    /**
     * Giới tính 
     * 0: Nữ,
     * 1: Nam,
     * 2: Khác
     */
    gender: {
        type: Number,
        default: 2,
        enum: [0, 1, 2],
    },
    /**
     * Địa chỉ
     */
    address: {
        type: String,
    },
    /**
     * Bên AMS đã đụng chạm dữ liệu
     */
    intervention: {
        type: Boolean,
    },
    /**
     * Miền
     */
    region: {
        type: Schema.Types.ObjectId,
        ref: 'region',
    },
    /**
     * Vùng
     */
    area: {
        type: Schema.Types.ObjectId,
        ref: 'area',
    },
    /**
     * Nhà phân phối
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    /**
     * Cửa hàng
     */
    store: {
        type: Schema.Types.ObjectId,
        ref: 'store',
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        enum: [1, 2],
    },
    /**
     * Tỉnh/Thành phố
     */
    city: {
        type: String,
    },
    /**
     * Tỉnh/Thành phố Text
     */
    cityName: {
        type: String,
    },
    /**
     * Quận/Huyện
     */
    district: {
        type: String,
    },
    /**
     * Quận/Huyện Text
     */
    districtName: {
        type: String,
    },
    /**
     * Xã/Phường
     */
    ward: {
        type: String,
    },
    /**
     * Xã/Phường Text
     */
    wardName: {
        type: String,
    },
});