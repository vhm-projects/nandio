const BASE_ROUTE = '/customer';
const API_BASE_ROUTE = '/api/customer';

const CF_ROUTINGS_CUSTOMER = {
    ADD_CUSTOMER: `${BASE_ROUTE}/add-customer`,
    UPDATE_CUSTOMER_BY_ID: `${BASE_ROUTE}/update-customer-by-id`,
    DELETE_CUSTOMER_BY_ID: `${BASE_ROUTE}/delete/:customerID`,

    GET_INFO_CUSTOMER_BY_ID: `${BASE_ROUTE}/info/:customerID`,
    GET_LIST_CUSTOMER: `${BASE_ROUTE}/list-customer`,
    GET_LIST_CUSTOMER_BY_FIELD: `${BASE_ROUTE}/list-customer/:field/:value`,
    GET_LIST_CUSTOMER_SERVER_SIDE: `${BASE_ROUTE}/list-customer-server-side`,

    UPDATE_CUSTOMER_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-customer-by-id-v2`,
    DELETE_CUSTOMER_BY_LIST_ID: `${BASE_ROUTE}/delete-customer-by-list-id`,
    
    // EXPORT EXCEL
    GET_LIST_CUSTOMER_EXCEL: `${BASE_ROUTE}/list-customer-excel`,
    DOWNLOAD_LIST_CUSTOMER_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-customer-excel-export`,

    // IMPORT EXCEL
    GET_LIST_CUSTOMER_IMPORT: `${BASE_ROUTE}/list-customer-import`,
    SETTING_FILE_CUSTOMER_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-customer-import-setting`,
    DOWNLOAD_FILE_CUSTOMER_EXCEL_IMPORT: `${BASE_ROUTE}/list-customer-import-dowload`,
    CREATE_CUSTOMER_IMPORT_EXCEL: `${BASE_ROUTE}/create-customer-import-excel`,

    API_GET_LIST_CUSTOMERS: `${API_BASE_ROUTE}/list-customers`,
    API_GET_INFO_CUSTOMER: `${API_BASE_ROUTE}/info-customer/:customerID`,

    // OTP
    SEND_OTP_REGISTER_PHONE:   `${API_BASE_ROUTE}/send-otp/:phone`,
    VERIFY_OTP_REGISTER_PHONE: `${API_BASE_ROUTE}/verify-otp/:phone`,
    SEND_OTP_FORGOT_PHONE:     `${API_BASE_ROUTE}/send-otp-forgot/:phone`,
    VERIFY_OTP_FORGOT_PHONE:   `${API_BASE_ROUTE}/verify-otp-forgot/:phone`,
    SEND_OTP_LOGIN_PHONE:      `${API_BASE_ROUTE}/send-otp-login/:phone`,
    VERIFY_OTP_LOGIN_PHONE:    `${API_BASE_ROUTE}/verify-otp-login/:phone`,
    SIGNIN_CUSTOMER_WITHOUT_OTP:    `${API_BASE_ROUTE}/signin-customer-without-otp/:phone`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_CUSTOMER = CF_ROUTINGS_CUSTOMER;