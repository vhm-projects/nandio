/**
 * Giới tính 
 * 0: Nữ
 * 1: Nam
 * 2: Khác
 */
exports.GENDER_CUSTOMER_TYPE = {

    "0": {
        value: "Nữ",
        color: "#0b51b7"
    },

    "1": {
        value: "Nam",
        color: "#d63031"
    },

    "2": {
        value: "Khác",
        color: "#0b51b7"
    },

};

/**
 * Trạng thái 
 * 1: Hoạt động
 * 2: Không hoạt động
 */
exports.STATUS_CUSTOMER_TYPE = {

    "1": {
        value: "Hoạt động",
        color: "#0b51b7"
    },

    "2": {
        value: "Không hoạt động",
        color: "#d63031"
    },

};