"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    GENDER_CUSTOMER_TYPE,
    STATUS_CUSTOMER_TYPE,
} = require('../constants/customer');
const {
    CF_ROUTINGS_CUSTOMER
} = require('../constants/customer/customer.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const CUSTOMER_MODEL    = require('../models/customer').MODEL;
const IMAGE_MODEL       = require('../../image/models/image').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const {
    DISTRIBUTOR_COLL
} = require('../../distributor');

const {
    STORE_COLL
} = require('../../store');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ CUSTOMER  ===============================
             * =============================== ************* ===============================
             */


            /**
             * Function: Get List area By parent (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            '/customer/list-area-by-parent': {
                config: {
                    scopes: ['read:list_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            region
                        } = req.query;

                        const listAreaByParent = await AREA_COLL
                            .find({
                                region
                            })
                            .lean();
                        res.json(listAreaByParent);
                    }]
                },
            },

            /**
             * Function: Get List distributor By parent (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            '/customer/list-distributor-by-parent': {
                config: {
                    scopes: ['read:list_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            area
                        } = req.query;

                        const listDistributorByParent = await DISTRIBUTOR_COLL
                            .find({
                                area
                            })
                            .lean();
                        res.json(listDistributorByParent);
                    }]
                },
            },

            /**
             * Function: Get List store By parent (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            '/customer/list-store-by-parent': {
                config: {
                    scopes: ['read:list_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            distributor
                        } = req.query;

                        const listStoreByParent = await STORE_COLL
                            .find({
                                distributor
                            })
                            .lean();
                        res.json(listStoreByParent);
                    }]
                },
            },



            /**
             * Function: Insert Customer (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.ADD_CUSTOMER]: {
                config: {
                    scopes: ['create:customer'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Customer',
                    code: CF_ROUTINGS_CUSTOMER.ADD_CUSTOMER,
                    inc: path.resolve(__dirname, '../views/customer/add_customer.ejs')
                },
                methods: {
                    get: [async function(req, res) {


                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_CUSTOMER
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            code,
                            fullname,
                            email,
                            phone,
                            birthday,
                            password,
                            confirmPassword,
                            pointRanking,
                            point,
                            avatar,
                            gender,
                            address,
                            distributor,
                            store,
                            city,
                            cityName,
                            district,
                            districtName,
                            ward,
                            wardName,
                        } = req.body;


                        if (avatar) {
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                name: avatar.name,
                                path: avatar.path,
                                type: avatar.type,
                                size: avatar.size
                            });
                            avatar = infoImageAfterInsert.data._id;
                        }

                        let infoAfterInsert = await CUSTOMER_MODEL.insert({
                            code,
                            fullname,
                            email,
                            phone,
                            birthday,
                            password,
                            confirmPassword,
                            pointRanking,
                            point,
                            avatar,
                            gender,
                            address,
                            distributor,
                            store,
                            city,
                            cityName,
                            district,
                            districtName,
                            ward,
                            wardName,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Customer By Id (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.UPDATE_CUSTOMER_BY_ID]: {
                config: {
                    scopes: ['update:customer'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Customer',
                    code: CF_ROUTINGS_CUSTOMER.UPDATE_CUSTOMER_BY_ID,
                    inc: path.resolve(__dirname, '../views/customer/update_customer.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            customerID
                        } = req.query;

                        let infoCustomer = await CUSTOMER_MODEL.getInfoById(customerID);
                        if (infoCustomer.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionDistributor = {
                            state: 1,
                            status: 1
                        };
                        if (infoCustomer.data.distributor) {

                            conditionDistributor._id = infoCustomer.data.distributor._id;

                        }

                        let listDistributors = await DISTRIBUTOR_COLL
                            .find(conditionDistributor)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let conditionStore = {
                            state: 1,
                            status: 1
                        };
                        if (infoCustomer.data.store) {

                            conditionStore._id = infoCustomer.data.store._id;

                        }

                        let listStores = await STORE_COLL
                            .find(conditionStore)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoCustomer: infoCustomer.data || {},

                            listDistributors,
                            listStores,
                            CF_ROUTINGS_CUSTOMER
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            customerID,
                            code,
                            fullname,
                            email,
                            phone,
                            birthday,
                            password,
                            confirmPassword,
                            pointRanking,
                            point,
                            avatar,
                            gender,
                            address,
                            distributor,
                            store,
                            city,
                            cityName,
                            district,
                            districtName,
                            ward,
                            wardName,
                        } = req.body;


                        if (avatar) {
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                name: avatar.name,
                                path: avatar.path,
                                type: avatar.type,
                                size: avatar.size
                            });
                            avatar = infoImageAfterInsert.data._id;
                        }

                        const infoAfterUpdate = await CUSTOMER_MODEL.update({
                            customerID,
                            code,
                            fullname,
                            email,
                            phone,
                            birthday,
                            password,
                            confirmPassword,
                            pointRanking,
                            point,
                            avatar,
                            gender,
                            address,
                            distributor,
                            store,
                            city,
                            cityName,
                            district,
                            districtName,
                            ward,
                            wardName,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Customer By Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.UPDATE_CUSTOMER_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            customerID,
                            code,
                            fullname,
                            email,
                            phone,
                            birthday,
                            password,
                            pointRanking,
                            point,
                            avatar,
                            gender,
                            address,
                            distributor,
                            store,
                            city,
                            cityName,
                            district,
                            districtName,
                            ward,
                            wardName,
                            status
                        } = req.body;


                        if (avatar) {
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                name: avatar.name,
                                path: avatar.path,
                                type: avatar.type,
                                size: avatar.size
                            });
                            avatar = infoImageAfterInsert.data._id;
                        }

                        const infoAfterUpdate = await CUSTOMER_MODEL.updateNotRequire({
                            customerID,
                            code,
                            fullname,
                            email,
                            phone,
                            birthday,
                            password,
                            pointRanking,
                            point,
                            avatar,
                            gender,
                            address,
                            distributor,
                            store,
                            city,
                            cityName,
                            district,
                            districtName,
                            ward,
                            wardName,
                            userUpdate,
                            status
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Customer By Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.DELETE_CUSTOMER_BY_ID]: {
                config: {
                    scopes: ['delete:customer'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            customerID
                        } = req.params;

                        const infoAfterDelete = await CUSTOMER_MODEL.deleteById(customerID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Customer By List Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.DELETE_CUSTOMER_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            customerID
                        } = req.body;

                        const infoAfterDelete = await CUSTOMER_MODEL.deleteByListId(customerID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Customer By Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.GET_INFO_CUSTOMER_BY_ID]: {
                config: {
                    scopes: ['read:info_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            customerID
                        } = req.params;

                        const infoCustomerById = await CUSTOMER_MODEL.getInfoById(customerID);
                        res.json(infoCustomerById);
                    }]
                },
            },

            /**
             * Function: Get List Customer (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.GET_LIST_CUSTOMER]: {
                config: {
                    scopes: ['read:list_customer'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách khách hàng',
                    code: CF_ROUTINGS_CUSTOMER.GET_LIST_CUSTOMER,
                    inc: path.resolve(__dirname, '../views/customer/list_customers.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            birthdayDateRange,
                            gender,
                            status,
                            typeGetList
                        } = req.query;

                        let listCustomers = [];
                        if (typeGetList === 'FILTER') {
                            listCustomers = await CUSTOMER_MODEL.getListByFilter({
                                keyword,
                                birthdayDateRange,
                                gender,
                                status,
                            });
                        } else {
                            listCustomers = await CUSTOMER_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listCustomers: listCustomers.data || [],
                            GENDER_CUSTOMER_TYPE,
                            STATUS_CUSTOMER_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Customer By Field (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.GET_LIST_CUSTOMER_BY_FIELD]: {
                config: {
                    scopes: ['read:list_customer'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Customer by field isStatus',
                    code: CF_ROUTINGS_CUSTOMER.GET_LIST_CUSTOMER_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/customer/list_customers.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            birthdayDateRange,
                            gender,
                            status,
                            type
                        } = req.query;

                        let listCustomers = await CUSTOMER_MODEL.getListByFilter({
                            keyword,
                            birthdayDateRange,
                            gender,
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listCustomers: listCustomers.data || [],
                            GENDER_CUSTOMER_TYPE,
                            STATUS_CUSTOMER_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Customer Server Side (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.GET_LIST_CUSTOMER_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listCustomerServerSide = await CUSTOMER_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listCustomerServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Customer Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.GET_LIST_CUSTOMER_IMPORT]: {
                config: {
                    scopes: ['read:list_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listCustomerImport = await CUSTOMER_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listCustomerImport);
                    }]
                },
            },

            /**
             * Function: Get List Customer Excel Server Side (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.GET_LIST_CUSTOMER_EXCEL]: {
                config: {
                    scopes: ['read:list_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = CUSTOMER_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Customer Excel Export (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.DOWNLOAD_LIST_CUSTOMER_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'customer'
                        });

                        let conditionObj = CUSTOMER_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listCustomer = await CUSTOMER_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listCustomer)
                    }]
                },
            },

            /**
             * Function: Setting Customer Excel Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.SETTING_FILE_CUSTOMER_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_customer'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = CUSTOMER_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Customer Excel Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.DOWNLOAD_FILE_CUSTOMER_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'customer'
                        });

                        let listCustomerImport = await CUSTOMER_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listCustomerImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listCustomerImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Customer Excel Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.CREATE_CUSTOMER_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:customer'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'customer'
                        });

                        let infoCustomerAfterImport = await CUSTOMER_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'customer',
                        });

                        res.json(infoCustomerAfterImport);
                    }]
                },
            },

            /**
             * Function: API Get info Customer
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.API_GET_LIST_CUSTOMERS]: {
                config: {
                    scopes: ['read:list_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listCustomers = await CUSTOMER_MODEL.getListCustomers({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listCustomers);
                    }]
                },
            },

            /**
             * Function: API Get info Customer
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.API_GET_INFO_CUSTOMER]: {
                config: {
                    scopes: ['read:info_customer'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            customerID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoCustomer = await CUSTOMER_MODEL.getInfoCustomer({
                            customerID,
                            select,
                            filter,
                            explain
                        });
                        res.json(infoCustomer);
                    }]
                },
            },

            /**
             * Function: GỬI OTP ĐĂNG KÝ KHÁCH HÀNG
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.SEND_OTP_REGISTER_PHONE]: {
                config: {
                    scopes: ['create:customer'],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
						const { phone } = req.params;
                        const { employeeID } = req.query;

                        console.log({
                            phone, employeeID
                        })
                        const infoSendOTP = await CUSTOMER_MODEL.sendOTP__PHONE({ 
							phone, employeeID
						});
                        res.json(infoSendOTP);
                    }]
                },
            },

            /**
             * Function: VERIFY OTP ĐĂNG KÝ KHÁCH HÀNG
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.VERIFY_OTP_REGISTER_PHONE]: {
                config: {
                    scopes: ['read:info_customer'],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
						const { phone }  = req.params;
                        const { code, fullname, employeeID, deviceID, deviceName, deviceType, lat, lng } = req.body;

                        const infoData = await CUSTOMER_MODEL.verifyRegisterOTP__PHONE({ 
							code, phone, fullname, employeeID, deviceID, deviceName, deviceType, lat, lng
						});
                        res.json(infoData);
                    }]
                },
            },

            /**
             * Function: SEND OTP QUÊN MẬT KHẨU KHÁCH HÀNG
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.SEND_OTP_FORGOT_PHONE]: {
                config: {
                    scopes: ['create:customer'],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
						const { phone } = req.params;
                        const infoSendOTP = await CUSTOMER_MODEL.sendOTPForgot__PHONE({ phone });
                        res.json(infoSendOTP);
                    }]
                },
            },

            /**
             * Function: VERIFY OTP QUÊN MẬT KHẨU KHÁCH HÀNG
             * Date: 02/12/2021
             * Dev: Automatic
             */
			 [CF_ROUTINGS_CUSTOMER.VERIFY_OTP_FORGOT_PHONE]: {
                config: {
                    scopes: ['read:info_customer'],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
						const { phone }  = req.params;
                        const { code }   = req.query;

                        const infoData = await CUSTOMER_MODEL.verifyForgotOTP__PHONE({ 
							code, phone
						});
                        res.json(infoData);
                    }]
                },
            },

            /**
             * Function: SEND OTP ĐĂNG NHẬP KHÁCH HÀNG
             * Date: 02/12/2021
             * Dev: Automatic
             */
             [CF_ROUTINGS_CUSTOMER.SEND_OTP_LOGIN_PHONE]: {
                config: {
                    scopes: ['create:customer'],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
						const { phone } = req.params;
                        const infoSendOTP = await CUSTOMER_MODEL.sendOTPLogin__PHONE({ phone })
                        res.json(infoSendOTP);
                    }]
                },
            },

            /**
             * Function: VERIFY OTP ĐĂNG NHẬP KHÁCH HÀNG
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CUSTOMER.VERIFY_OTP_LOGIN_PHONE]: {
                config: {
                    scopes: ['read:info_customer'],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
						const { phone }  = req.params;
                        const { code }   = req.query;

                        const infoData = await CUSTOMER_MODEL.verifyLoginOTP__PHONE({ 
							code, phone
						});
                        res.json(infoData);
                    }]
                },
            },
            [CF_ROUTINGS_CUSTOMER.SIGNIN_CUSTOMER_WITHOUT_OTP]: {
                config: {
                    scopes: ['read:info_customer'],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
						const { phone }  = req.params;

                        const infoData = await CUSTOMER_MODEL.verifyOTPFakeSMS({ 
                            phone
						});
                        res.json(infoData);
                    }]
                },
            },
        }
    }
};