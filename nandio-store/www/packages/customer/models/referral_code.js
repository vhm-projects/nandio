"use strict";
let request = require('request');
/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
/**
 * INTERNAL PACKAGES
 */
const { checkObjectIDs }           = require('../../../utils/utils');
/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const REFERRAL_CODE_COLL  				    = require('../databases/referral_code-coll');
const EMPLOYEE_COLL  				        = require('../../employee/databases/employee-coll');

class Model extends BaseModel {
    constructor() {
        super(REFERRAL_CODE_COLL);
    }
    
    getReferralCodeOfEmployee({ employeeID }) {
        return new Promise(async resolve => {
            try {
                const infoEmployee = await EMPLOYEE_COLL.findById(employeeID);
                if(!infoEmployee)
                    return resolve({ error: true, message: 'Không tìm thấy nhân viên' });

                const { phone } = infoEmployee;

                let conditionObj = {
                    phone
                }

                let listCustomerRegister__AMS = await REFERRAL_CODE_COLL.aggregate([
                    {
                        $match: {
                            ...conditionObj
                        }
                    },
                    {
                        $lookup: {
                            from: 'customers',
                            localField: 'customer',
                            foreignField: '_id',
                            as: 'customer'
                        }
                    }, 
                    {
                        $unwind: "$customer"
                    },
                    {
                        $match: {
                            "customer._id": {
                                $lte: ObjectID("61e44c8a964d9e0625edc32f")
                            },
                        }
                    }
                ]);

                let listCustomerRegister__QSHOP = await REFERRAL_CODE_COLL.aggregate([
                    {
                        $match: {
                            ...conditionObj
                        }
                    },
                    {
                        $lookup: {
                            from: 'customers',
                            localField: 'customer',
                            foreignField: '_id',
                            as: 'customer'
                        }
                    }, 
                    {
                        $unwind: "$customer"
                    },
                    {
                        $match: {
                            "customer._id": {
                                $gt: ObjectID("61e44c8a964d9e0625edc32f")
                            },
                        }
                    }
                ])

                return resolve({ error: false, data: { listCustomerRegister__AMS, listCustomerRegister__QSHOP} });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
