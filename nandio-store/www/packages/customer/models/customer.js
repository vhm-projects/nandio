/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const path = require('path');
const fs = require('fs');
const {
    hash,
    hashSync,
    compare
} = require('bcryptjs');
const XlsxPopulate = require('xlsx-populate');
const mongodb = require("mongodb");
const {
    MongoClient
} = mongodb;


/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    renderOptionFilter,
    colName,
    checkEmail,
    checkPhoneNumber,
    isEmptyObj,
    checkNumberIsValidWithRange,
    validPhone
} = require('../../../utils/utils');
const {
    isTrue
} = require('../../../tools/module/check');
const {
    randomStringFixLength,
    randomStringOnlyNumber
} = require('../../../utils/string_utils');

const {
    GENDER_CUSTOMER_TYPE,
    STATUS_CUSTOMER_TYPE,
} = require('../constants/customer');


/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const URL_DATABASE = process.env.URL_DATABASE || 'mongodb://localhost:27017';
const NAME_DATABASE = process.env.NAME_DATABASE || 'ldk_tools_op';

/**
 * COLLECTIONS
 */
const CUSTOMER_COLL = require('../databases/customer-coll');
const {
    REGION_COLL
} = require('../../region_area');

const {
    AREA_COLL
} = require('../../region_area');

const {
    DISTRIBUTOR_COLL
} = require('../../distributor');

const {
    STORE_COLL
} = require('../../store');

const OTP_MODEL         = require('../../otp/models/otp').MODEL;
// const { EMPLOYEE_MODEL }                      = require('../../employee');

class Model extends BaseModel {
    constructor() {
        super(CUSTOMER_COLL);
    }

    /**
         * Tạo mới customer
		* @param {string} code
		* @param {string} fullname
		* @param {string} email
		* @param {string} phone
		* @param {date} birthday
		* @param {string} password
		* @param {number} pointRanking
		* @param {number} point
		* @param {object} avatar
		* @param {number} gender
		* @param {string} address
		* @param {object} distributor
		* @param {object} store
		* @param {string} city
		* @param {string} cityName
		* @param {string} district
		* @param {string} districtName
		* @param {string} ward
		* @param {string} wardName

         * @param {objectId} userCreate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        code,
        fullname,
        email,
        phone,
        birthday,
        password,
        confirmPassword,
        pointRanking,
        point,
        avatar,
        gender = 2,
        address,
        distributor,
        store,
        city,
        cityName,
        district,
        districtName,
        ward,
        wardName,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (code.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mã khách hàng không được lớn hơn 125 ký tự'
                    });
                }

                if (!code) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mã khách hàng'
                    });
                }

                if (fullname.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài họ và tên không được lớn hơn 125 ký tự'
                    });
                }

                if (!fullname) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập họ và tên'
                    });
                }

                if (email.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài email không được lớn hơn 125 ký tự'
                    });
                }

                if (phone.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài số điện thoại không được lớn hơn 125 ký tự'
                    });
                }

                if (password.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mật khẩu không được lớn hơn 125 ký tự'
                    });
                }

                if (!password) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mật khẩu'
                    });
                }

                if(password !== confirmPassword){
                    return resolve({
                        error: true,
                        message: 'Mật khẩu không trùng khớp'
                    });
                }

                if (avatar && isEmptyObj(avatar)) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập ảnh đại diện'
                    });
                }

                if (address.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài địa chỉ không được lớn hơn 125 ký tự'
                    });
                }

                if (city.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tỉnh/thành phố không được lớn hơn 125 ký tự'
                    });
                }

                if (!city) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tỉnh/thành phố'
                    });
                }

                if (cityName.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tỉnh/thành phố text không được lớn hơn 125 ký tự'
                    });
                }

                if (!cityName) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tỉnh/thành phố text'
                    });
                }

                if (district.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài quận/huyện không được lớn hơn 125 ký tự'
                    });
                }

                if (!district) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập quận/huyện'
                    });
                }

                if (districtName.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài quận/huyện text không được lớn hơn 125 ký tự'
                    });
                }

                if (!districtName) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập quận/huyện text'
                    });
                }

                if (ward.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài xã/phường không được lớn hơn 125 ký tự'
                    });
                }

                if (!ward) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập xã/phường'
                    });
                }

                if (wardName.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài xã/phường text không được lớn hơn 125 ký tự'
                    });
                }

                if (!wardName) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập xã/phường text'
                    });
                }


                if (gender && !checkNumberIsValidWithRange({
                        arrValid: [0, 1, 2],
                        val: gender
                    })) {
                    return resolve({
                        error: true,
                        message: 'giới tính không hợp lệ'
                    });
                }


                if (email && !checkEmail(email)) {
                    return resolve({
                        error: true,
                        message: 'Email không hợp lệ'
                    });
                }


                if (phone && !checkPhoneNumber(phone)) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại không đúng định dạng'
                    });
                }


                if (distributor && !checkObjectIDs([distributor])) {
                    return resolve({
                        error: true,
                        message: 'nhà phân phối không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }


                const checkCodeExits = await CUSTOMER_COLL.findOne({
                    code
                });
                if (checkCodeExits) {
                    return resolve({
                        error: true,
                        message: 'Mã Khách Hàng đã tồn tại'
                    });
                }

                const checkEmailExits = await CUSTOMER_COLL.findOne({
                    email
                });
                if (checkEmailExits) {
                    return resolve({
                        error: true,
                        message: 'Email đã tồn tại'
                    });
                }

                const checkPhoneExits = await CUSTOMER_COLL.findOne({
                    phone
                });

                if (phone && checkPhoneExits) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại đã tồn tại'
                    });
                }

                let hashPassword = await hash(password, 8);
				if (!hashPassword){
                    return resolve({ error: true, message: 'Không thể hash password' });
                }

                let dataInsert = {
                    code,
                    fullname,
                    email,
                    phone,
                    password: hashPassword,
                    pointRanking,
                    point,
                    avatar,
                    gender,
                    address,
                    distributor,
                    store,
                    city,
                    cityName,
                    district,
                    districtName,
                    ward,
                    wardName,
                    userCreate
                };
                birthday && (dataInsert.birthday = new Date(birthday));

                if (distributor) {
                    const distributorInfo = await DISTRIBUTOR_COLL.findById(distributor);
                    dataInsert.region = distributorInfo.region;
                    dataInsert.area = distributorInfo.area;
                    dataInsert.status = distributorInfo.status;
                }

                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo khách hàng thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật customer 
         * @param {objectId} customerID
		* @param {string} code
		* @param {string} fullname
		* @param {string} email
		* @param {string} phone
		* @param {date} birthday
		* @param {string} password
		* @param {number} pointRanking
		* @param {number} point
		* @param {object} avatar
		* @param {number} gender
		* @param {string} address
		* @param {object} distributor
		* @param {object} store
		* @param {string} city
		* @param {string} cityName
		* @param {string} district
		* @param {string} districtName
		* @param {string} ward
		* @param {string} wardName

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        customerID,
        code,
        fullname,
        email,
        phone,
        birthday,
        password,
        confirmPassword,
        pointRanking,
        point,
        avatar,
        gender,
        address,
        distributor,
        store,
        city,
        cityName,
        district,
        districtName,
        ward,
        wardName,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([customerID])) {
                    return resolve({
                        error: true,
                        message: 'customerID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (code.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài mã khách hàng không được lớn hơn 125 ký tự'
                    });
                }

                if (!code) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập mã khách hàng cho customer'
                    });
                }

                if (fullname.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài họ và tên không được lớn hơn 125 ký tự'
                    });
                }

                if (!fullname) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập họ và tên cho customer'
                    });
                }

                if (email.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài email không được lớn hơn 125 ký tự'
                    });
                }

                if (phone.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài số điện thoại không được lớn hơn 125 ký tự'
                    });
                }

                if (password) {
                    if(password.length > 125){
                        return resolve({
                            error: true,
                            message: 'Độ dài mật khẩu không được lớn hơn 125 ký tự'
                        });
                    }

                    if(password !== confirmPassword){
                        return resolve({
                            error: true,
                            message: 'Mật khẩu không trùng khớp'
                        });
                    }
                }

                if (avatar && isEmptyObj(avatar)) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập ảnh đại diện cho customer'
                    });
                }

                if (address.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài địa chỉ không được lớn hơn 125 ký tự'
                    });
                }

                if (city.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tỉnh/thành phố không được lớn hơn 125 ký tự'
                    });
                }

                if (!city) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tỉnh/thành phố cho customer'
                    });
                }

                if (cityName.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài tỉnh/thành phố text không được lớn hơn 125 ký tự'
                    });
                }

                if (!cityName) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập tỉnh/thành phố text cho customer'
                    });
                }

                if (district.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài quận/huyện không được lớn hơn 125 ký tự'
                    });
                }

                if (!district) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập quận/huyện cho customer'
                    });
                }

                if (districtName.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài quận/huyện text không được lớn hơn 125 ký tự'
                    });
                }

                if (!districtName) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập quận/huyện text cho customer'
                    });
                }

                if (ward.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài xã/phường không được lớn hơn 125 ký tự'
                    });
                }

                if (!ward) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập xã/phường cho customer'
                    });
                }

                if (wardName.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài xã/phường text không được lớn hơn 125 ký tự'
                    });
                }

                if (!wardName) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập xã/phường text cho customer'
                    });
                }


                if (gender && !checkNumberIsValidWithRange({
                        arrValid: [0, 1, 2],
                        val: gender
                    })) {
                    return resolve({
                        error: true,
                        message: 'giới tính không hợp lệ'
                    });
                }


                if (email && !checkEmail(email)) {
                    return resolve({
                        error: true,
                        message: 'Email không hợp lệ'
                    });
                }


                if (phone && !checkPhoneNumber(phone)) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại không đúng định dạng'
                    });
                }


                if (distributor && !checkObjectIDs([distributor])) {
                    return resolve({
                        error: true,
                        message: 'nhà phân phối không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                const checkExists = await CUSTOMER_COLL.findById(customerID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'khách hàng không tồn tại'
                    });
                }

                const checkCodeExits = await CUSTOMER_COLL.findOne({
                    code
                });
                if (checkCodeExits && checkExists.code !== code) {
                    return resolve({
                        error: true,
                        message: 'mã khách hàng đã tồn tại'
                    });
                }

                const checkEmailExits = await CUSTOMER_COLL.findOne({
                    email
                });
                if (checkEmailExits && checkExists.email !== email) {
                    return resolve({
                        error: true,
                        message: 'email đã tồn tại'
                    });
                }

                const checkPhoneExits = await CUSTOMER_COLL.findOne({
                    phone
                });
                if (checkPhoneExits && checkExists.phone !== phone) {
                    return resolve({
                        error: true,
                        message: 'số điện thoại đã tồn tại'
                    });
                }


                let dataUpdate = {
                    userUpdate
                };
                code && (dataUpdate.code = code);
                fullname && (dataUpdate.fullname = fullname);
                dataUpdate.email = email;
                dataUpdate.phone = phone;
                birthday && (dataUpdate.birthday = new Date(birthday));
                password && (dataUpdate.password = hashSync(password, 8));
                dataUpdate.pointRanking = pointRanking;
                dataUpdate.point = point;
                dataUpdate.avatar = avatar;
                dataUpdate.gender = gender;
                dataUpdate.address = address;
                distributor && (dataUpdate.distributor = distributor);
                store && (dataUpdate.store = store);
                city && (dataUpdate.city = city);
                cityName && (dataUpdate.cityName = cityName);
                district && (dataUpdate.district = district);
                districtName && (dataUpdate.districtName = districtName);
                ward && (dataUpdate.ward = ward);
                wardName && (dataUpdate.wardName = wardName);

                if (distributor) {
                    const distributorInfo = await DISTRIBUTOR_COLL.findById(distributor);
                    dataUpdate.region = distributorInfo.region;
                    dataUpdate.area = distributorInfo.area;
                    dataUpdate.status = distributorInfo.status;
                }

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: customerID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật customer (không bắt buộc)
         * @param {objectId} customerID
		* @param {string} code
		* @param {string} fullname
		* @param {string} email
		* @param {string} phone
		* @param {date} birthday
		* @param {string} password
		* @param {number} pointRanking
		* @param {number} point
		* @param {object} avatar
		* @param {number} gender
		* @param {string} address
		* @param {object} distributor
		* @param {object} store
		* @param {string} city
		* @param {string} cityName
		* @param {string} district
		* @param {string} districtName
		* @param {string} ward
		* @param {string} wardName

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        customerID,
        code,
        fullname,
        email,
        phone,
        birthday,
        password,
        pointRanking,
        point,
        avatar,
        gender,
        address,
        distributor,
        store,
        city,
        cityName,
        district,
        districtName,
        ward,
        wardName,
        userUpdate,
        status
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([customerID])) {
                    return resolve({
                        error: true,
                        message: 'customerID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (email && !checkEmail(email)) {
                    return resolve({
                        error: true,
                        message: 'Email không hợp lệ'
                    });
                }

                if (phone && !checkPhoneNumber(phone)) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại không đúng định dạng'
                    });
                }

                if (avatar && isEmptyObj(avatar)) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập ảnh đại diện cho customer'
                    });
                }

                if (gender && !checkNumberIsValidWithRange({
                        arrValid: [0, 1, 2],
                        val: gender
                    })) {
                    return resolve({
                        error: true,
                        message: 'giới tính không hợp lệ'
                    });
                }

                if (distributor && !checkObjectIDs([distributor])) {
                    return resolve({
                        error: true,
                        message: 'nhà phân phối không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'cửa hàng không hợp lệ'
                    });
                }

                const checkExists = await CUSTOMER_COLL.findById(customerID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'khách hàng không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                code && (dataUpdate.code = code);
                fullname && (dataUpdate.fullname = fullname);
                email && (dataUpdate.email = email);
                phone && (dataUpdate.phone = phone);
                birthday && (dataUpdate.birthday = new Date(birthday));
                password && (dataUpdate.password = password);
                pointRanking && (dataUpdate.pointRanking = pointRanking);
                point && (dataUpdate.point = point);
                avatar && (dataUpdate.avatar = avatar);
                gender && (dataUpdate.gender = gender);
                address && (dataUpdate.address = address);
                distributor && (dataUpdate.distributor = distributor);
                store && (dataUpdate.store = store);
                city && (dataUpdate.city = city);
                cityName && (dataUpdate.cityName = cityName);
                district && (dataUpdate.district = district);
                districtName && (dataUpdate.districtName = districtName);
                ward && (dataUpdate.ward = ward);
                wardName && (dataUpdate.wardName = wardName);
                status && (dataUpdate.status = status);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: customerID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }
                
                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa customer 
     * @param {objectId} customerID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(customerID) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 2
                };
                if (!checkObjectIDs([customerID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị customerID không hợp lệ'
                    });
                }


                const infoAfterDelete = await this.updateById(customerID, conditionObj);

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa customer 
     * @param {array} customerID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(customerID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(customerID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị customerID không hợp lệ'
                    });
                }

                const infoAfterDelete = await CUSTOMER_COLL.deleteMany({
                    _id: {
                        $in: customerID
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin customer 
     * @param {objectId} customerID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(customerID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([customerID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị customerID không hợp lệ'
                    });
                }

                const infoCustomer = await CUSTOMER_COLL.findById(customerID)
                    .populate('region area distributor store')

                if (!infoCustomer) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin khách hàng'
                    });
                }

                return resolve({
                    error: false,
                    data: infoCustomer
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách customer 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listCustomer = await CUSTOMER_COLL
                    .find({
                        state: 1
                    }).populate('region area distributor store')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listCustomer) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách khách hàng'
                    });
                }

                return resolve({
                    error: false,
                    data: listCustomer
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách customer theo bộ lọc
		* @param {string} keyword
		* @param {date} birthdayDateRange
		* @param {number} pointRanking
		* @param {number} point
		* @enum {number} gender
		* @enum {number} status

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        keyword,
        birthdayDateRange,
        gender,
        status,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        code: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullname: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        email: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        phone: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        password: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        codeChangeEmail: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        picture: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        id: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        birthday1: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        token: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        type: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        googleUID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        facebookUID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        appleUID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        address: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        city: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        cityName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        district: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        districtName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        ward: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        wardName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                if (birthdayDateRange) {
                    let [fromDate, toDate] = birthdayDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.birthday = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                gender && (conditionObj.gender = gender);

                status && (conditionObj.status = status);


                const listCustomerByFilter = await CUSTOMER_COLL
                    .find(conditionObj).populate('region area distributor store')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listCustomerByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách khách hàng"
                    });
                }

                return resolve({
                    error: false,
                    data: listCustomerByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách customer theo bộ lọc (server side)
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        code: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullname: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        email: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        phone: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        password: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        codeChangeEmail: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        picture: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        id: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        birthday1: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        token: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        type: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        googleUID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        facebookUID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        appleUID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        address: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        city: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        cityName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        district: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        districtName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        ward: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        wardName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }



                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trạng thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }


                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalCustomer = await CUSTOMER_COLL.countDocuments(conditionObj);

                const listCustomerByFilter = await CUSTOMER_COLL.aggregate([
                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },

                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                  
                ])

                if (!listCustomerByFilter) {
                    return resolve({
                        recordsTotal: totalCustomer,
                        recordsFiltered: totalCustomer,
                        data: []
                    });
                }

                const listCustomerDataTable = listCustomerByFilter.map((customer, index) => {

                    let status = '';
                    if (customer.status == 1) {
                        status = 'checked';
                    }
                    
                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${customer._id}" type="checkbox" class="check-record check-record-${customer._id}" _index ="${index + 1}"><label for="${customer._id}"></label></div></td>`,
                        indexSTT: skip + index + 1,
                        code: `<a href="/customer/update-customer-by-id?customerID=${customer._id}">${customer.code && customer.code.length > 50 ? customer.code.substr(0,50) + "..." : customer.code} </a>`,
                        fullname: `${customer.fullname && customer.fullname.length > 50 ? customer.fullname.substr(0,50) + "..." : customer.fullname}`,
                        email: `${customer.email && customer.email.length > 50 ? customer.email.substr(0,50) + "..." : customer.email}`,
                        phone: `${customer.phone && customer.phone.length > 50 ? customer.phone.substr(0,50) + "..." : customer.phone}`,
                        birthday: moment(customer.birthday).format("L"),
                        point: `${customer && customer.point || 0}`,
                        address: `${customer.address && customer.address.length > 50 ? customer.address.substr(0,50) + "..." : customer.address || ""}`,
                        region: customer.region ? customer.region.name : '',
                        area: customer.area ? customer.area.name : '',
                        distributor: customer.distributor ? customer.distributor.name : '',
                        store: customer.store ? customer.store.name : '',
                        status: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-status" _customerID="${customer._id}" type="checkbox" id="${customer._id}" ${status} style="width: 40px;height: 20px;"></div></td>`,
                        createAt: moment(customer.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalCustomer,
                    recordsFiltered: totalCustomer,
                    data: listCustomerDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách customer theo bộ lọc import
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterImport({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        code: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullname: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        email: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        phone: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        password: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        codeChangeEmail: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        picture: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        id: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        birthday1: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        token: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        type: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        googleUID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        facebookUID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        appleUID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        address: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        city: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        cityName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        district: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        districtName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        ward: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        wardName: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }


                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trạng thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }


                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalCustomer = await CUSTOMER_COLL.countDocuments(conditionObj);

                const listCustomerByFilter = await CUSTOMER_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'regions',
                            localField: 'region',
                            foreignField: '_id',
                            as: 'region'
                        }
                    },
                    {
                        $unwind: {
                            path: '$region',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'areas',
                            localField: 'area',
                            foreignField: '_id',
                            as: 'area'
                        }
                    },
                    {
                        $unwind: {
                            path: '$area',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listCustomerByFilter) {
                    return resolve({
                        recordsTotal: totalCustomer,
                        recordsFiltered: totalCustomer,
                        data: []
                    });
                }

                return resolve({
                    error: false,
                    recordsTotal: totalCustomer,
                    recordsFiltered: totalCustomer,
                    data: listCustomerByFilter || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc customer
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc customer
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listCustomerByFilter = await CUSTOMER_COLL.aggregate(arrayFilter)

                if (!listCustomerByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listCustomerByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listCustomerByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('HH:mm DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Tải file import excel mẫu customer
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    fileImportExcelPreview({
        arrayItemCustomerChoice
    }) {
        return new Promise(async resolve => {
            try {
                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition; // BỘ LỌC && LOẠI IMPORT
                let {
                    listFieldPrimaryKey
                } = condition; // DANH SÁCH PRIMARY KEY

                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        let index = 0;
                        arrayItemCustomerChoice.map((elem) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            if (elem.dataDynamic && elem.dataDynamic.length) {
                                listFieldPrimaryKey = listFieldPrimaryKey.filter(key => key != elem.nameFieldRef); // LỌC PRIMARY KEY MÀ KHÔNG PHẢI REF

                                elem.dataDynamic.map(item => {
                                    workbook.sheet("Report").row(1).cell(index + 1).value(item);
                                    index++;
                                })
                            } else {
                                workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                                index++;
                            }
                        });

                        if (isTrue(condition.checkDownloadDataOld)) { // KIỂM TRA CÓ ĐÍNH ĐÈM DỮ LIỆU CŨ THEO ĐIỀU KIỆN
                            let listItemImport = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].listItemImport; // LIST FIELD ĐÃ ĐƯỢC CẤU HÌNH
                            let {
                                arrayFilter
                            } = this.getConditionArrayFilterExcel(listItemImport, condition.conditionDeleteImport.filter, condition.conditionDeleteImport.condition); // LẤY RA ARRAY ARREGATE

                            let groupByCustomer = {};
                            listFieldPrimaryKey.map(key => {
                                groupByCustomer = {
                                    ...groupByCustomer,
                                    [key]: '$' + key
                                }
                            });

                            arrayFilter = [
                                ...arrayFilter,
                                {
                                    $group: {
                                        _id: {
                                            groupByCustomer
                                        },
                                        listData: {
                                            $addToSet: "$$CURRENT"
                                        },
                                    }
                                }
                            ];

                            const listCustomerByFilter = await CUSTOMER_COLL.aggregate(arrayFilter);

                            listCustomerByFilter && listCustomerByFilter.length && listCustomerByFilter.map((item, indexCustomer) => {
                                let indexValue = 0;
                                arrayItemCustomerChoice.map((elem, indexChoice) => {
                                    let variable = elem.name.split('.');

                                    if (elem.dataDynamic && elem.dataDynamic.length) { // KIỂM TRA FIELD CÓ CHỌN DYNAMIC
                                        item.listData && item.listData.length && item.listData.map(value => { // ARRAY DATA DYNAMIC
                                            if (item.listData.length > elem.dataDynamic.length) { // KIỂM TRA ĐỘ DÀI CỦA DATA SO VỚI SỐ CỘT
                                                // TODO: XỬ LÝ NHIỀU DYNAMIC

                                            } else {
                                                elem.dataDynamic.map(dynamic => {
                                                    let valueOfField;
                                                    if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                        let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                        if (objDataOfVariable) {
                                                            valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                        }
                                                    } else {
                                                        valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                                    }

                                                    if (valueOfField == dynamic) { // CHECK NẾU VALUE === CỘT
                                                        let valueImportDynamic = value[elem.variableChoice] ? value[elem.variableChoice] : '';

                                                        // INSERT DỮ LIỆU VÀO BẢNG VỚI FIELD ĐƯỢC CHỌN THEO DẠNG DYNAMIC
                                                        workbook.sheet("Report").row(indexCustomer + 2).cell(indexValue + 1).value(valueImportDynamic);
                                                        indexValue++;
                                                    } else {
                                                        if (elem.dataDynamic && item.listData && elem.dataDynamic.length > item.listData.length) {
                                                            indexValue++;
                                                        }
                                                    }
                                                })
                                            }
                                        })
                                    } else { // DẠNG STATIC
                                        let valueCustomer;
                                        if (variable.length > 1) {
                                            let objDataOfVariable = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                            if (objDataOfVariable) {
                                                valueCustomer = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                            }
                                        } else {
                                            valueCustomer = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                        }

                                        workbook.sheet("Report").row(indexCustomer + 2).cell(indexValue + 1).value(valueCustomer);
                                        indexValue++;
                                    }
                                });
                            });
                        }

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    pathWriteFile,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Upload File Excel Import Lưu Dữ Liệu customer
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    importExcel({
        nameCollParent,
        arrayItemCustomerChoice,
        file,
    }) {
        return new Promise(async resolve => {
            try {

                XlsxPopulate.fromFileAsync(file.path)
                    .then(async workbook => {
                        let listData = [];
                        let index = 2;
                        let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition;

                        const client = await MongoClient.connect(URL_DATABASE);
                        const db = client.db(NAME_DATABASE)

                        for (; true;) {
                            if (arrayItemCustomerChoice && arrayItemCustomerChoice.length) {
                                let conditionObj = {};

                                let totalLength = 0;
                                arrayItemCustomerChoice.map((item, index) => {
                                    if (item.dataDynamic && item.dataDynamic.length) {
                                        totalLength += item.dataDynamic.length;
                                    } else {
                                        totalLength++;
                                    }
                                });

                                let indexOfListField = 0;
                                let checkIsRequire = false;
                                let arrayConditionObjDynamic = [];

                                for (let i = 0; i < totalLength; i++) {
                                    if (arrayItemCustomerChoice[indexOfListField].dataDynamic && arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                        let indexOfDynamic = 1;
                                        for (let valueDynamic of arrayItemCustomerChoice[indexOfListField].dataDynamic) {
                                            let letter = colName(i);
                                            let indexOfCeil = letter.toUpperCase() + index;

                                            let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                            if (variable) {

                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: valueDynamic.trim()
                                                });

                                                let conditionOfOneValueDynamic = {
                                                    [arrayItemCustomerChoice[indexOfListField].variableChoice]: variable
                                                }
                                                if (docs) {
                                                    conditionOfOneValueDynamic = {
                                                        ...conditionOfOneValueDynamic,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    }
                                                }

                                                arrayConditionObjDynamic = [
                                                    ...arrayConditionObjDynamic,
                                                    conditionOfOneValueDynamic
                                                ];
                                            }

                                            if (indexOfDynamic < arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                                i++;
                                            }
                                            indexOfDynamic++;
                                        }
                                    } else {
                                        let letter = colName(i);
                                        let indexOfCeil = letter.toUpperCase() + index;
                                        let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                        if (arrayItemCustomerChoice[indexOfListField].isRequire && !variable) {
                                            checkIsRequire = true;
                                            break;
                                        }
                                        if (arrayItemCustomerChoice[indexOfListField].ref && arrayItemCustomerChoice[indexOfListField].ref != nameCollParent) {
                                            if (arrayItemCustomerChoice[indexOfListField].isRequire) {
                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: variable.trim()
                                                })
                                                // .sort({
                                                //     _id: -1
                                                // })
                                                // .limit(100)
                                                // .toArray();
                                                if (docs) {
                                                    conditionObj = {
                                                        ...conditionObj,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                    };

                                                    if (arrayItemCustomerChoice[indexOfListField].mappingRef && arrayItemCustomerChoice[indexOfListField].mappingRef.length) {
                                                        arrayItemCustomerChoice[indexOfListField].mappingRef.map(mapping => {
                                                            conditionObj = {
                                                                ...conditionObj,
                                                                [mapping]: docs[mapping]
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                [arrayItemCustomerChoice[indexOfListField].name]: variable
                                            };
                                        }
                                    }

                                    indexOfListField++;
                                }
                                if (checkIsRequire) {
                                    break;
                                }

                                conditionObj = {
                                    ...conditionObj,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                }

                                let arrayCondditionObj = []
                                if (arrayConditionObjDynamic && arrayConditionObjDynamic.length) {
                                    arrayConditionObjDynamic.map(item => {
                                        arrayCondditionObj = [
                                            ...arrayCondditionObj,
                                            {
                                                ...conditionObj,
                                                ...item
                                            }
                                        ]
                                    });
                                } else {
                                    arrayCondditionObj = [
                                        ...arrayCondditionObj,
                                        conditionObj
                                    ]
                                }
                                listData = [
                                    ...listData,
                                    ...arrayCondditionObj
                                ];

                                index++;
                            }
                        }

                        await fs.unlinkSync(file.path);

                        if (listData.length) {
                            await this.changeDataImport({
                                condition,
                                listCustomer: listData
                            });
                        } else {
                            return resolve({
                                error: true,
                                message: 'Import thất bại'
                            });
                        }

                        return resolve({
                            error: false,
                            message: 'Import thành công'
                        });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lưu Dữ Liệu Theo Lựa Chọn customer
     * @param {object} listCustomer
     * @param {object} condition
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    changeDataImport({
        condition,
        listCustomer
    }) {
        return new Promise(async resolve => {
            try {
                if (isTrue(condition.delete)) { // XÓA DATA CŨ
                    if (isTrue(condition.deleteAll)) { // XÓA TẤT CẢ DỮ LIỆU
                        console.log("====================XÓA TẤT CẢ DỮ LIỆU====================");
                        await CUSTOMER_COLL.deleteMany({});
                        let listDataAfterInsert = await CUSTOMER_COLL.insertMany(listCustomer);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    } else { // XÓA VỚI ĐIỀU KIỆN
                        console.log("====================XÓA VỚI ĐIỀU KIỆN====================");

                        /**
                         * ===========================================================================
                         * =========================XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===========================================================================
                         */
                        let {
                            filter,
                            condition: conditionMultiple,
                        } = condition.conditionDeleteImport;

                        if (!filter || !filter.length) {
                            return resolve({
                                error: true,
                                message: 'Filter do not exist'
                            });
                        }

                        let conditionObj = {
                            state: 1,
                            $or: []
                        };

                        if (filter && filter.length) {
                            if (filter.length > 1) {

                                filter.map(filterObj => {
                                    if (filterObj.type === 'ref') {
                                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionFieldRef);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionFieldRef
                                            };
                                        }
                                    } else {
                                        const conditionByFilter = this.getConditionObj(filterObj);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionByFilter);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionByFilter
                                            };
                                        }
                                    }
                                });

                            } else {
                                let {
                                    type,
                                    ref,
                                    fieldRefName
                                } = filter[0];

                                if (type === 'ref') {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(ref, fieldRefName)
                                    };
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(filter[0])
                                    };
                                }
                            }
                        }

                        if (conditionObj.$or && !conditionObj.$or.length) {
                            delete conditionObj.$or;
                        }

                        let listAfterDelete = await CUSTOMER_COLL.deleteMany({
                            ...conditionObj
                        });
                        /**
                         * ===============================================================================
                         * =========================END XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===============================================================================
                         */

                        if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                            for (let item of listCustomer) {
                                let listConditionFindOneUpdate = {};
                                let {
                                    listFieldPrimaryKey
                                } = condition;
                                listFieldPrimaryKey.map(elem => {
                                    listConditionFindOneUpdate = {
                                        ...listConditionFindOneUpdate,
                                        [elem]: item[elem]
                                    }
                                });

                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    state: 1
                                }

                                let checkExist = await CUSTOMER_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                    $set: item
                                }, {
                                    upsert: true
                                });
                            }
                            return resolve({
                                error: false,
                                message: 'Insert success'
                            });

                        } else { // INSERT CÁI MỚI
                            console.log("====================INSERT CÁI MỚI 2====================");
                            let listDataAfterInsert = await CUSTOMER_COLL.insertMany(listCustomer);
                            return resolve({
                                error: false,
                                message: 'Insert success',
                                data: listDataAfterInsert
                            });
                        }
                    }
                } else { // KHÔNG XÓA DATA CŨ
                    if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                        console.log("====================KIỂM TRA TỒN TẠI VÀ UPDATE====================");
                        for (let item of listCustomer) {
                            let listConditionFindOneUpdate = {};
                            let {
                                listFieldPrimaryKey
                            } = condition;
                            listFieldPrimaryKey.map(elem => {
                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    [elem]: item[elem]
                                }
                            });

                            listConditionFindOneUpdate = {
                                ...listConditionFindOneUpdate,
                                state: 1
                            }

                            let checkExist = await CUSTOMER_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                $set: item
                            }, {
                                upsert: true
                            });
                        }
                        return resolve({
                            error: false,
                            message: 'Insert success'
                        });
                    } else { // INSERT CÁI MỚI
                        console.log("====================INSERT CÁI MỚI====================");
                        let listDataAfterInsert = await CUSTOMER_COLL.insertMany(listCustomer);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    }
                }

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc customer
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
            $or: []
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            let mappingRef = [];
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                    mappingRef = element.mappingRef;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            fieldID: item.fieldID,
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            ref: item.coll,
                            variable: item.name,
                            nameFieldRef,
                            dataEnum: item.dataEnum,
                            isRequire: item.isRequire,
                            dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                            variableChoice: item.variableChoice,
                            nameCollChoice: item.nameCollChoice,
                            mappingRef: mappingRef,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                fieldID: item.fieldID,
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                ref: item.coll,
                                variable: item.name,
                                nameFieldRef,
                                dataEnum: item.dataEnum,
                                isRequire: item.isRequire,
                                dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                variableChoice: item.variableChoice,
                                nameCollChoice: item.nameCollChoice,
                                mappingRef: mappingRef,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    dataEnum: item.dataEnum,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {
            if (filter.length > 1) {

                filter.map(filterObj => {
                    if (filterObj.type === 'ref') {
                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionFieldRef);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionFieldRef
                            };
                        }
                    } else {
                        const conditionByFilter = this.getConditionObj(filterObj);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionByFilter);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionByFilter
                            };
                        }
                    }
                });

            } else {
                let {
                    type,
                    ref,
                    fieldRefName
                } = filter[0];

                if (type === 'ref') {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(ref, fieldRefName)
                    };
                } else {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(filter[0])
                    };
                }
            }
        }

        if (conditionObj.$or && !conditionObj.$or.length) {
            delete conditionObj.$or;
        }


        if (!isEmptyObject(objFilterStatic)) {

            if (objFilterStatic.status) {
                if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                    return resolve({
                        error: true,
                        message: "trạng thái không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    status: Number(objFilterStatic.status)
                }
            }

        }


        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let collName = pluralize.plural(item.coll);
                let checkPluralColl = collName[collName.length - 1];

                if (checkPluralColl.toLowerCase() != 's') {
                    collName += 's';
                }

                let lookup = [{
                        $lookup: {
                            from: collName,
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        let collNamePopulate = pluralize.plural(populate.coll);
                        let checkPluralColl = collNamePopulate[collNamePopulate.length - 1];

                        if (checkPluralColl.toLowerCase() != 's') {
                            collNamePopulate += 's';
                        }

                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: collNamePopulate,
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Lấy danh sách customer
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListCustomers({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                } else {

                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['birthdayDateRange', 'gender', 'region', 'area', 'distributor', 'store', 'status'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    birthdayDateRange,
                    gender,
                    region,
                    area,
                    distributor,
                    store,
                    status,
                } = filter;

                if (birthdayDateRange) {
                    let [fromDate, toDate] = birthdayDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.birthday = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                gender && (conditionObj.gender = gender);

                region && (conditionObj.region = region);

                area && (conditionObj.area = area);

                distributor && (conditionObj.distributor = distributor);

                store && (conditionObj.store = store);

                status && (conditionObj.status = status);


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['region', 'area', 'distributor', 'store'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listCustomers = await CUSTOMER_COLL
                    .find(conditionObj, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listCustomers = await CUSTOMER_COLL.populate(listCustomers, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listCustomers = await CUSTOMER_COLL.populate(listCustomers, `${ref}.${field}`);
                        }
                    }
                }

                if (!listCustomers) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách customer',
                        status: 400
                    });
                }

                let totalRecord = await CUSTOMER_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listCustomers,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy thông tin customer
     * @param {objectId} customerID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoCustomer({
        customerID,
        select,
        filter = {},
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];
                let conditionObj = {
                    state: 1
                };

                if (!checkObjectIDs([customerID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị customerID không hợp lệ',
                        status: 400
                    });
                }

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                Object.keys(filter).map(key => {
                    if (![].includes(key)) {
                        delete filter[key];
                    }
                });

                let {} = filter;


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (![].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoCustomer = await CUSTOMER_COLL
                    .findOne({
                        _id: customerID,
                        ...conditionObj
                    })
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoCustomer = await CUSTOMER_COLL.populate(infoCustomer, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoCustomer = await CUSTOMER_COLL.populate(infoCustomer, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoCustomer) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin customer',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoCustomer,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

     /**
     * Gửi OTP đăng ký tài khoản
     * @param {number} phone
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    sendOTP__PHONE({ phone, employeeID }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                /**
                 * NẾU có mail: gửi OTP qua mail
                 * NẾU ko mail: gửi OTP qua SĐT
                 */
                if (!validPhone(phone))
                    return resolve({ error: true, message: 'SĐT không hợp lệ' });

                if (!checkObjectIDs([employeeID])) {
                    return resolve({
                        error: true,
                        message: 'ID Nhân viên không hơp lệ',
                        status: 400
                    });
                }
                // if (email && !validEmail(email))
                //     return resolve({ error: true, message: 'Mail không hợp lệ' });

                let isExistPhone = await CUSTOMER_COLL.findOne({ phone, status: STATUS_ACTIVE });
                if (isExistPhone)
                    return resolve({ error: true, message: 'SĐT đã đăng ký trước đó' });
              
                /**
                 * ===================================================================
                 *  1. KIỂM TRA EMPLOYEE CÓ ĐANG CHECKIN HAY KHÔNG => LẤY RA THỒNG TIN CHECKIN VÀ EMPLOYEE
                 * ===================================================================
                 */
                let info__CheckInVsEmployee__ByEmployeeID = await EMPLOYEE_MODEL.getInfo__CheckIn_Vs_Employee__ByEmployeeID({ employeeID });
                if (info__CheckInVsEmployee__ByEmployeeID.error) {
                    return resolve(info__CheckInVsEmployee__ByEmployeeID);
                }

                // let isExistEmailInCitizenVerify = await CITIZEN_VERIFY_COLL.findOne({ email, status: this.STATUS_ACTIVE });
                // if (isExistEmailInCitizenVerify)
                    // return resolve({ error: true, message: 'Mail đã đăng ký trước đó' });
                
                /**
                 * kiểm tra otp trước đó đã cách 2 phút chưa?
                 */
                let infoOTPAfterInsert = await OTP_MODEL.insertOTP__PHONE({ phone, type: OTP_MODEL.TYPE_REGISTER });
                if(infoOTPAfterInsert.error)
                    return resolve(infoOTPAfterInsert);
                // if (email) {
                //     objForInsert = {
                //         ...objForInsert, 
                //         email: email.toLowerCase()
                //     }
                //     //* Gửi MAIL
                //     let infoSendOtpViaMail = await OTP_2_MODEL.sendOTPV2({ email, phone });
                //     if(infoSendOtpViaMail.error)
                //         return resolve(infoSendOtpViaMail)
                // } else {
                //     //* Gửi OTP
                //     // let infoSendOtpViaSMS = await OTP_MODEL.sendOTP({ phone });
                //     // if(infoSendOtpViaSMS.error)
                //     //     return resolve(infoSendOtpViaSMS)

                //     let infoSendOtpViaSMS = await OTP_MODEL.sendOTP_VNPT({ phone });
                //     if(infoSendOtpViaSMS.error)
                //         return resolve(infoSendOtpViaSMS)
                //     console.log({ infoSendOtpViaSMS })
                // }
               
                // let infoAfterUpdate = await CITIZEN_VERIFY_COLL.findByIdAndUpdate(citizenVerifyID, {
                //     ...objForInsert
                // }, { new: true });
				
                // if (!infoOTPAfterInsert)
                //     return resolve({ error: true, message: 'cannot_update_info' });

                //TODO: Thêm function gửi OTP 
                return resolve({ error: false, data: infoOTPAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }
    /**
     * Verify OTP đăng ký tài khoản
     * @param {number} phone
     * @param {number} code
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    verifyRegisterOTP__PHONE({ phone, code, fullname, employeeID, deviceID, deviceName, deviceType, lat, lng }) { // xác nhận mail
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                if (!validPhone(phone))
                    return resolve({ error: true, message: 'SĐT không hợp lệ', status: 400 });
                
                if (!fullname) {
                    return resolve({ error: true, message: 'Tên khách hàng không hợp lệ', status: 400 });
                }
                   
                if (!code || Number.isNaN(Number(code)) || Number(code) < 0)
                    return resolve({ error: true, message: 'Mã OTP không hợp lệ', status: 400 });
                
                 /**
                 *  (*)REQUIRE: KIỂM TRA CÁC THUỘC TÍNH BUỘC PHẢI CÓ
                 */
                if (!deviceID || !deviceName || !deviceType || !lat || !lng) {
                    return resolve({ error: true, message: 'Bạn phải gửi đầy đủ thông tin về ID, Tên Thiết Bị, Loại Thiết Bị, Lat và Lng', status: 400 });
                }

                if (!["ANDROID", "IOS"].includes(deviceType))
                    return resolve({ error: true, message: 'Loại Thiết Bị không hợp lệ', status: 400 });

                let isExistPhone = await CUSTOMER_COLL.findOne({ phone, status: STATUS_ACTIVE });
                if (isExistPhone)
                    return resolve({ error: true, message: 'SĐT đã đăng ký trước đó', status: 400 });

                /**
                 * ===================================================================
                 *  1. KIỂM TRA EMPLOYEE CÓ ĐANG CHECKIN HAY KHÔNG => LẤY RA THỒNG TIN CHECKIN VÀ EMPLOYEE
                 * ===================================================================
                 */
                let info__CheckInVsEmployee__ByEmployeeID = await EMPLOYEE_MODEL.getInfo__CheckIn_Vs_Employee__ByEmployeeID({ employeeID });
                if (info__CheckInVsEmployee__ByEmployeeID.error) {
                    return resolve(info__CheckInVsEmployee__ByEmployeeID);
                }

                let infoAfterCheck = await OTP_MODEL.verifyOTP__PHONE({ phone, code, type: OTP_MODEL.TYPE_REGISTER })
                
				if (infoAfterCheck.error)
                    return resolve(infoAfterCheck);
                
                let dataInsert = {
                    phone,
                    fullname,
                    email: `user_default${randomStringOnlyNumber(10)}@gmail.com`,
                    code: randomStringOnlyNumber(10),
                    status: STATUS_ACTIVE
                };

                let infoAfterInsert = await this.insertData(dataInsert);
                if (!infoAfterInsert) {
                    return resolve({ error: true, message: 'Không thể đăng ký Khách hàng', status: 400 });
                }
  
                let { data: { infoCheckInOut, infoEmployee } } = info__CheckInVsEmployee__ByEmployeeID;
                let conditionObj__HistoryInitCustomer = {
                    customer: infoAfterInsert._id,
                    deviceID, deviceName, deviceType, lat, lng,
                    checkin:     infoCheckInOut._id,
                    store:       infoCheckInOut.store,
                    employee:    infoEmployee._id,
                    region:      infoEmployee.region,
                    area:        infoEmployee.area,
                    distributor: infoEmployee.distributor,
                    createAt: new Date(),
                    modifyAt: new Date(),
                }
                // let infoHistoryInitCustomer = await HISTORY_INIT_CUSTOMER_COLL.create(conditionObj__HistoryInitCustomer);
                // console.log({
                //     infoHistoryInitCustomer
                // });
                console.log({
                    conditionObj__HistoryInitCustomer
                });
                let infoHistoryInitCustomer = await HISTORY_INIT_CUSTOMER_MODEL.insert(conditionObj__HistoryInitCustomer);
                console.log({
                    infoHistoryInitCustomer
                });
                
                if (!infoHistoryInitCustomer) {
                    return resolve({ error: true, message: 'Không thể thêm lịch sử đăng ký khách hàng', status: 400 });
                }
                
				return resolve({ error: false, data: { infoCustomer: infoAfterInsert, infoOtpAfterVerify: infoAfterCheck } })

            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    /**
     * Gửi OTP quên mật khẩu KHÁCH HÀNG
     * @param {number} phone
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    sendOTPForgot__PHONE({ phone }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                /**
                 * NẾU có mail: gửi OTP qua mail
                 * NẾU ko mail: gửi OTP qua SĐT
                 */
                if (!validPhone(phone))
                    return resolve({ error: true, message: 'SĐT không hợp lệ' });

                // if (email && !validEmail(email))
                //     return resolve({ error: true, message: 'Mail không hợp lệ' });

                let isExistPhone = await CUSTOMER_COLL.findOne({ phone, status: STATUS_ACTIVE });
                if (!isExistPhone)
                    return resolve({ error: true, message: 'SĐT chưa được đăng ký' });
              
                let infoOTPAfterInsert = await OTP_MODEL.insertOTP__PHONE({ phone, type: OTP_MODEL.TYPE_FORGET_PASS });
                if(infoOTPAfterInsert.error)
                    return resolve(infoOTPAfterInsert)
                
                return resolve({ error: false, data: infoOTPAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    /**
     * Verify OTP quên mật khẩu KHÁCH HÀNG
     * @param {number} phone
     * @param {number} code
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    verifyForgotOTP__PHONE({ phone, code }) { // xác nhận mail
        return new Promise(async resolve => {
            try {
                if (!validPhone(phone))
                    return resolve({ error: true, message: 'SĐT không hợp lệ' });
                   
                if (!code || Number.isNaN(Number(code)) || Number(code) < 0)
                    return resolve({ error: true, message: 'Mã OTP không hợp lệ' });
                    
                /**
                 *  vì flow hiện tại của gửi OTP khác
                 *  - SMS: otp đang lưu bên authy
                 *  - Mail: otp lưu trên hệ thống (selfhost) 
                 * --> cách kiểm tra khác nhau
                 */
                let infoAfterCheck = await OTP_MODEL.verifyOTP__PHONE({ phone, code, type: OTP_MODEL.TYPE_FORGET_PASS })
                if (infoAfterCheck.error)
                    return resolve(infoAfterCheck);
                
				return resolve({ error: false, data: infoAfterCheck })

            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    /**
     * Gửi OTP đăng nhập KHÁCH HÀNG
     * @param {number} phone
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
     sendOTPLogin__PHONE({ phone }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                /**
                 * NẾU có mail: gửi OTP qua mail
                 * NẾU ko mail: gửi OTP qua SĐT
                 */
                if (!validPhone(phone))
                    return resolve({ error: true, message: 'SĐT không hợp lệ' });

                // if (email && !validEmail(email))
                //     return resolve({ error: true, message: 'Mail không hợp lệ' });

                let isExistPhone = await CUSTOMER_COLL.findOne({ phone, status: STATUS_ACTIVE });
                if (!isExistPhone)
                    return resolve({ error: true, message: 'SĐT chưa được đăng ký' });
              
                let infoOTPAfterInsert = await OTP_MODEL.insertOTP__PHONE({ phone, type: OTP_MODEL.TYPE_LOGIN });
                if(infoOTPAfterInsert.error)
                    return resolve(infoOTPAfterInsert)
                
                return resolve({ error: false, data: infoOTPAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    /**
     * Verify OTP đăng nhập KHÁCH HÀNG
     * @param {number} phone
     * @param {number} code
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
     verifyLoginOTP__PHONE({ phone, code }) { // xác nhận mail
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                if (!validPhone(phone))
                    return resolve({ error: true, message: 'SĐT không hợp lệ' });
                   
                if (!code || Number.isNaN(Number(code)) || Number(code) < 0)
                    return resolve({ error: true, message: 'Mã OTP không hợp lệ' });
                    
                let isExistPhone = await CUSTOMER_COLL.findOne({ phone, status: STATUS_ACTIVE });
                if (!isExistPhone)
                    return resolve({ error: true, message: 'SĐT chưa được đăng ký' });

                let infoAfterCheck = await OTP_MODEL.verifyOTP__PHONE({ phone, code, type: OTP_MODEL.TYPE_LOGIN })
                if (infoAfterCheck.error)
                    return resolve(infoAfterCheck);
                
				return resolve({ error: false, data: isExistPhone });

            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    /**
     * FUNC 'Đăng Nhập tại QStore', version trước thì cần đăng nhập gửi OTP -> verify
     * -> Hiện tại thì không cần verify nữa, nên đăng nhập số điện thoại là được luôn
     * => verifyOTPFakeSMS là func bỏ qua bước sendOTP, verifyOTP mà vẫn đăng nhập được
     */
    verifyOTPFakeSMS({ phone }) { 
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                if (!validPhone(phone))
                    return resolve({ error: true, message: 'SĐT không hợp lệ' });
                    
                let isExistPhone = await CUSTOMER_COLL.findOne({ phone, status: STATUS_ACTIVE });
                if (!isExistPhone)
                    return resolve({ error: true, message: 'SĐT chưa được đăng ký' });

				return resolve({ error: false, data: isExistPhone });

            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }
}

exports.MODEL = new Model;

/**
 * MODELS
 */
var { MODEL: EMPLOYEE_MODEL }			        = require('../../employee/models/employee');
var { MODEL: HISTORY_INIT_CUSTOMER_MODEL }		= require('../../history/models/history_init_customer');
// var { HISTORY_INIT_CUSTOMER_COLL }      = require('../../history');