const CUSTOMER_COLL = require('./databases/customer-coll');
const CUSTOMER_MODEL = require('./models/customer').MODEL;
const CUSTOMER_ROUTES = require('./apis/customer');
// MARK REQUIRE

module.exports = {
    CUSTOMER_COLL,
    CUSTOMER_MODEL,
    CUSTOMER_ROUTES,
    // MARK EXPORT
}