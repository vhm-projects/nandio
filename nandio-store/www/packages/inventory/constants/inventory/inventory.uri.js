const BASE_ROUTE = '/inventory';
const API_BASE_ROUTE = '/api/inventory';

const CF_ROUTINGS_INVENTORY = {
    ADD_INVENTORY: `${BASE_ROUTE}/add-inventory`,
    UPDATE_INVENTORY_BY_ID: `${BASE_ROUTE}/update-inventory-by-id`,
    DELETE_INVENTORY_BY_ID: `${BASE_ROUTE}/delete/:inventoryID`,

    GET_INFO_INVENTORY_BY_ID: `${BASE_ROUTE}/info/:inventoryID`,
    GET_LIST_INVENTORY: `${BASE_ROUTE}/list-inventory`,
    GET_LIST_INVENTORY_BY_FIELD: `${BASE_ROUTE}/list-inventory/:field/:value`,
    GET_LIST_INVENTORY_SERVER_SIDE: `${BASE_ROUTE}/list-inventory-server-side`,

    UPDATE_INVENTORY_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-inventory-by-id-v2`,
    DELETE_INVENTORY_BY_LIST_ID: `${BASE_ROUTE}/delete-inventory-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_INVENTORY_EXCEL: `${BASE_ROUTE}/list-inventory-excel`,
    DOWNLOAD_LIST_INVENTORY_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-inventory-excel-export`,

    // IMPORT EXCEL
    GET_LIST_INVENTORY_IMPORT: `${BASE_ROUTE}/list-inventory-import`,
    SETTING_FILE_INVENTORY_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-inventory-import-setting`,
    DOWNLOAD_FILE_INVENTORY_EXCEL_IMPORT: `${BASE_ROUTE}/list-inventory-import-dowload`,
    CREATE_INVENTORY_IMPORT_EXCEL: `${BASE_ROUTE}/create-inventory-import-excel`,

    API_GET_INFO_INVENTORY: `${API_BASE_ROUTE}/info-inventory/:inventoryID`,
    API_GET_LIST_INVENTORYS: `${API_BASE_ROUTE}/list-inventorys`,
    API_ADD_INVENTORY: `${API_BASE_ROUTE}/add-inventory`,
    API_UPDATE_INVENTORY: `${API_BASE_ROUTE}/update-inventory/:inventoryID`,
    API_DELETE_INVENTORY: `${API_BASE_ROUTE}/delete-inventory/:inventoryID`,
    API_DELETE_INVENTORYS: `${API_BASE_ROUTE}/delete-inventorys/:inventorysID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_INVENTORY = CF_ROUTINGS_INVENTORY;