const INVENTORY_COLL = require('./databases/inventory-coll');
const INVENTORY_MODEL = require('./models/inventory').MODEL;
const INVENTORY_ROUTES = require('./apis/inventory');
// MARK REQUIRE

module.exports = {
    INVENTORY_COLL,
    INVENTORY_MODEL,
    INVENTORY_ROUTES,
    // MARK EXPORT
}