"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('inventory', {

    /**
     * Nhà phân phối
     */
    distributor: {
        type: Schema.Types.ObjectId,
        ref: 'distributor',
    },
    /**
     * Sản phẩm
     */
    sku: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },
    /**
     * Số lượng
     */
    amount: {
        type: Number,
        required: true,
    },
    /**
     * Trạng thái  
     * SUCCESS: Thành công
     */
    statusImport: {
        type: String,
        default: "SUCCESS",
        enum: ["SUCCESS"],
    },
    /**
     * Thời gian nhập
     */
    dateImport: {
        type: Date,
        required: true,
    },
});