/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUSIMPORT_INVENTORY_TYPE,
} = require('../constants/inventory');
const {
    CF_ROUTINGS_INVENTORY
} = require('../constants/inventory/inventory.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */

const {
    MODEL: INVENTORY_MODEL
} = require('../models/inventory');
const {
    MODEL: MANAGE_COLL_MODEL
} = require('../../../models/manage_coll');

const {
    MODEL: DISTRIBUTOR_MODEL
} = require('../../distributor/models/distributor');

const {
    MODEL: PRODUCT_MODEL
} = require('../../product/models/product');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ INVENTORY  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Inventory (API, VIEW)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.ADD_INVENTORY]: {
                config: {
                    scopes: ['create:inventory'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Inventory',
                    code: CF_ROUTINGS_INVENTORY.ADD_INVENTORY,
                    inc: path.resolve(__dirname, '../views/inventory/add_inventory.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listDistributors = await DISTRIBUTOR_MODEL.find({
                            state: 1,
                            status: 1
                        }).sort({
                            modifyAt: -1
                        }).lean();


                        ChildRouter.renderToView(req, res, {
                            listDistributors,
                            CF_ROUTINGS_INVENTORY
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            distributor,
                            sku,
                            amount,
                            dateImport,
                        } = req.body;


                        let response = await INVENTORY_MODEL.insert({
                            distributor,
                            sku,
                            amount,
                            dateImport,
                            userCreate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update Inventory By Id (API, VIEW)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.UPDATE_INVENTORY_BY_ID]: {
                config: {
                    scopes: ['update:inventory'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Inventory',
                    code: CF_ROUTINGS_INVENTORY.UPDATE_INVENTORY_BY_ID,
                    inc: path.resolve(__dirname, '../views/inventory/update_inventory.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            inventoryID
                        } = req.query;

                        let infoInventory = await INVENTORY_MODEL.getInfoById(inventoryID);
                        if (infoInventory.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let listDistributors = await DISTRIBUTOR_MODEL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();

                        let conditionSku = {
                            state: 1,
                            status: 1
                        };
                        if (infoInventory && infoInventory.data && infoInventory.data.sku) {

                            conditionSku._id = infoInventory.data.sku._id;

                        }

                        let listSkus = await PRODUCT_MODEL
                            .find(conditionSku)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoInventory: infoInventory.data || {},

                            listDistributors,
                            listSkus,
                            CF_ROUTINGS_INVENTORY
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            inventoryID,
                            distributor,
                            sku,
                            amount,
                            dateImport,
                        } = req.body;


                        const response = await INVENTORY_MODEL.update({
                            inventoryID,
                            distributor,
                            sku,
                            amount,
                            dateImport,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Update not require Inventory By Id (API)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.UPDATE_INVENTORY_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:inventory'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            inventoryID,
                            distributor,
                            sku,
                            amount,
                            dateImport,
                        } = req.body;


                        const response = await INVENTORY_MODEL.updateNotRequire({
                            inventoryID,
                            distributor,
                            sku,
                            amount,
                            dateImport,
                            userUpdate
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Inventory By Id (API)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.DELETE_INVENTORY_BY_ID]: {
                config: {
                    scopes: ['delete:inventory'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            inventoryID
                        } = req.params;

                        const response = await INVENTORY_MODEL.deleteById(inventoryID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Delete Inventory By List Id (API)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.DELETE_INVENTORY_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:inventory'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            inventoryID
                        } = req.body;

                        const response = await INVENTORY_MODEL.deleteByListId(inventoryID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get Info Inventory By Id (API)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.GET_INFO_INVENTORY_BY_ID]: {
                config: {
                    scopes: ['read:info_inventory'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            inventoryID
                        } = req.params;

                        const response = await INVENTORY_MODEL.getInfoById(inventoryID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Inventory (API, VIEW)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.GET_LIST_INVENTORY]: {
                config: {
                    scopes: ['read:list_inventory'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Inventory',
                    code: CF_ROUTINGS_INVENTORY.GET_LIST_INVENTORY,
                    inc: path.resolve(__dirname, '../views/inventory/list_inventorys.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            amountFromNumber,
                            amountToNumber,
                            keyword,
                            dateImportDateRange,
                            typeGetList
                        } = req.query;

                        let listInventorys = [];
                        if (typeGetList === 'FILTER') {
                            listInventorys = await INVENTORY_MODEL.getListByFilter({
                                amountFromNumber,
                                amountToNumber,
                                keyword,
                                dateImportDateRange,
                            });
                        } else {
                            listInventorys = await INVENTORY_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listInventorys: listInventorys.data || [],
                            STATUSIMPORT_INVENTORY_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Inventory By Field (API, VIEW)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.GET_LIST_INVENTORY_BY_FIELD]: {
                config: {
                    scopes: ['read:list_inventory'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Inventory by field isStatus',
                    code: CF_ROUTINGS_INVENTORY.GET_LIST_INVENTORY_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/inventory/list_inventorys.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            amountFromNumber,
                            amountToNumber,
                            keyword,
                            dateImportDateRange,
                            type
                        } = req.query;

                        let listInventorys = await INVENTORY_MODEL.getListByFilter({
                            amountFromNumber,
                            amountToNumber,
                            keyword,
                            dateImportDateRange,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listInventorys: listInventorys.data || [],
                            STATUSIMPORT_INVENTORY_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Inventory Server Side (API)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.GET_LIST_INVENTORY_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_inventory'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const response = await INVENTORY_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Inventory Import (API)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.GET_LIST_INVENTORY_IMPORT]: {
                config: {
                    scopes: ['read:list_inventory'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const response = await INVENTORY_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Get List Inventory Excel Server Side (API)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.GET_LIST_INVENTORY_EXCEL]: {
                config: {
                    scopes: ['read:list_inventory'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = INVENTORY_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let response = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Inventory Excel Export (API)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.DOWNLOAD_LIST_INVENTORY_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_inventory'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'inventory'
                        });

                        let conditionObj = INVENTORY_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let response = await INVENTORY_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Setting Inventory Excel Import (API)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.SETTING_FILE_INVENTORY_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_inventory'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = INVENTORY_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let response = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(response)
                    }]
                },
            },

            /**
             * Function: Download Inventory Excel Import (API)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.DOWNLOAD_FILE_INVENTORY_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_inventory'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'inventory'
                        });

                        let listInventoryImport = await INVENTORY_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listInventoryImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listInventoryImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Inventory Excel Import (API)
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.CREATE_INVENTORY_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:inventory'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'inventory'
                        });

                        let response = await INVENTORY_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'inventory',
                        });

                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Get info Inventory
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.API_GET_INFO_INVENTORY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        const {
                            inventoryID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const response = await INVENTORY_MODEL.getInfoInventory({
                            inventoryID,
                            select,
                            filter,
                            explain,
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Get list Inventory
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.API_GET_LIST_INVENTORYS]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listInventorys = await INVENTORY_MODEL.getListInventorys({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                        });
                        res.json(listInventorys);
                    }]
                },
            },

            /**
             * Function: API Insert Inventory
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.API_ADD_INVENTORY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {
                            distributor,
                            sku,
                            amount,
                            statusImport,
                            dateImport,
                        } = req.body;


                        const response = await INVENTORY_MODEL.insertInventory({
                            distributor,
                            sku,
                            amount,
                            statusImport,
                            dateImport,
                            authorID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Update Inventory
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.API_UPDATE_INVENTORY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let authorID = req.user && req.user._id;
                        let {
                            inventoryID
                        } = req.params;
                        let {
                            distributor,
                            sku,
                            amount,
                            statusImport,
                            dateImport,
                        } = req.body;


                        let response = await INVENTORY_MODEL.updateInventory({
                            inventoryID,
                            distributor,
                            sku,
                            amount,
                            statusImport,
                            dateImport,
                            authorID
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Delete Inventory
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.API_DELETE_INVENTORY]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {

                        const {
                            inventoryID
                        } = req.params;

                        const response = await INVENTORY_MODEL.deleteInventory(inventoryID);
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: API Delete Inventory
             * Date: 12/01/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_INVENTORY.API_DELETE_INVENTORYS]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {

                        const {
                            inventorysID
                        } = req.params;

                        const response = await INVENTORY_MODEL.deleteInventorys(inventorysID);
                        res.json(response);
                    }]
                },
            },

        }
    }
};