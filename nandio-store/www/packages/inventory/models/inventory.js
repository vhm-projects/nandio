/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const path = require('path');
const fs = require('fs');
const ISOdate = require('isodate');
const ObjectID = require("mongoose").Types.ObjectId;
const XlsxPopulate = require('xlsx-populate');
const {
    MongoClient
} = require('mongodb');
const formatCurrency = require('number-format.js');

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    colName,
} = require('../../../utils/utils');
const {
    isTrue
} = require('../../../tools/module/check');

const {
    STATUSIMPORT_INVENTORY_TYPE,
} = require('../constants/inventory');


/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const URL_DATABASE = process.env.URL_DATABASE || 'mongodb://localhost:27017';
const NAME_DATABASE = process.env.NAME_DATABASE || 'ldk_tools_op';

class Model extends BaseModel {
    constructor() {
        super(require('../databases/inventory-coll'))
    }

    /**
         * Tạo mới inventory
		* @param {object} distributor
		* @param {object} sku
		* @param {number} amount
		* @param {date} dateImport

         * @param {objectId} author
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        distributor,
        sku,
        amount,
        dateImport,
        author
    }) {
        return new Promise(async resolve => {
            try {
                if (author && !checkObjectIDs([author])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ',
                        status: 400
                    });
                }

                if (!amount) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập số lượng',
                        status: 400
                    });
                }

                if (!dateImport) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập thời gian nhập',
                        status: 400
                    });
                }






                if (!checkObjectIDs([distributor])) {
                    return resolve({
                        error: true,
                        message: 'nhà phân phối không hợp lệ',
                        status: 400
                    });
                }

                if (!checkObjectIDs([sku])) {
                    return resolve({
                        error: true,
                        message: 'sản phẩm không hợp lệ',
                        status: 400
                    });
                }




                let dataInsert = {
                    distributor,
                    sku,
                    amount,
                    dateImport: new Date(dateImport),
                    author
                };


                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo tồn đầu thất bại',
                        status: 422
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
         * Cập nhật inventory 
         * @param {objectId} inventoryID
		* @param {object} distributor
		* @param {object} sku
		* @param {number} amount
		* @param {date} dateImport

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        inventoryID,
        distributor,
        sku,
        amount,
        dateImport,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([inventoryID])) {
                    return resolve({
                        error: true,
                        message: 'inventoryID không hợp lệ',
                        status: 400
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ',
                        status: 400
                    });
                }

                if (!amount) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập số lượng',
                        status: 400
                    });
                }

                if (!dateImport) {
                    return resolve({
                        error: true,
                        message: 'Bạn cần nhập thời gian nhập',
                        status: 400
                    });
                }






                if (!checkObjectIDs([distributor])) {
                    return resolve({
                        error: true,
                        message: 'nhà phân phối không hợp lệ',
                        status: 400
                    });
                }

                if (!checkObjectIDs([sku])) {
                    return resolve({
                        error: true,
                        message: 'sản phẩm không hợp lệ',
                        status: 400
                    });
                }

                const checkExists = await INVENTORY_COLL.findById(inventoryID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'tồn đầu không tồn tại',
                        status: 403
                    });
                }



                let dataUpdate = {
                    userUpdate
                };
                distributor && (dataUpdate.distributor = distributor);
                sku && (dataUpdate.sku = sku);
                amount && (dataUpdate.amount = amount);
                dateImport && (dataUpdate.dateImport = new Date(dateImport));

                let infoAfterUpdate = await this.findOneAndUpdate({
                    _id: inventoryID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại',
                        status: 422
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
         * Cập nhật inventory (không bắt buộc)
         * @param {objectId} inventoryID
		* @param {object} distributor
		* @param {object} sku
		* @param {number} amount
		* @param {date} dateImport

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        inventoryID,
        distributor,
        sku,
        amount,
        dateImport,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([inventoryID])) {
                    return resolve({
                        error: true,
                        message: 'inventoryID không hợp lệ',
                        status: 400
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ',
                        status: 400
                    });
                }

                if (distributor && !checkObjectIDs([distributor])) {
                    return resolve({
                        error: true,
                        message: 'nhà phân phối không hợp lệ',
                        status: 400
                    });
                }

                if (sku && !checkObjectIDs([sku])) {
                    return resolve({
                        error: true,
                        message: 'sản phẩm không hợp lệ',
                        status: 400
                    });
                }

                const checkExists = await INVENTORY_COLL.findById(inventoryID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'tồn đầu không tồn tại',
                        status: 400
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                distributor && (dataUpdate.distributor = distributor);
                sku && (dataUpdate.sku = sku);
                amount && (dataUpdate.amount = amount);
                dateImport && (dataUpdate.dateImport = new Date(dateImport));
                let infoAfterUpdate = await this.findOneAndUpdate({
                    _id: inventoryID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại',
                        status: 422
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa inventory 
     * @param {objectId} inventoryID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(inventoryID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([inventoryID])) {
                    return resolve({
                        error: true,
                        message: 'inventoryID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.findByIdAndDelete(inventoryID);

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa inventory 
     * @param {array} inventoryID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(inventoryID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(inventoryID)) {
                    return resolve({
                        error: true,
                        message: 'inventoryID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await INVENTORY_COLL.deleteMany({
                    _id: {
                        $in: inventoryID
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy thông tin inventory 
     * @param {objectId} inventoryID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(inventoryID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([inventoryID])) {
                    return resolve({
                        error: true,
                        message: 'inventoryID không hợp lệ',
                        status: 400
                    });
                }

                const infoInventory = await INVENTORY_COLL.findById(inventoryID)
                    .populate('distributor sku')

                if (!infoInventory) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin tồn đầu',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoInventory,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách inventory 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listInventory = await INVENTORY_COLL
                    .find({
                        state: 1
                    }).populate('distributor sku')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listInventory) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách tồn đầu',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: listInventory,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
         * Lấy danh sách inventory theo bộ lọc
		* @param {number} amountFromNumber
		* @param {number} amountToNumber
		* @param {string} keyword
		* @param {date} dateImportDateRange

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        amountFromNumber,
        amountToNumber,
        keyword,
        dateImportDateRange,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        statusImport: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                if (amountFromNumber && amountToNumber) {
                    conditionObj.amount = {
                        $gte: amountFromNumber,
                        $lt: amountToNumber,
                    };
                }

                if (dateImportDateRange) {
                    let [fromDate, toDate] = dateImportDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.dateImport = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }


                const listInventoryByFilter = await INVENTORY_COLL
                    .find(conditionObj).populate('distributor sku')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listInventoryByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách tồn đầu",
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: listInventoryByFilter,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách inventory theo bộ lọc (server side)
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        statusImport: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }




                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalInventory = await INVENTORY_COLL.countDocuments(conditionObj);

                const listInventoryByFilter = await INVENTORY_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'products',
                            localField: 'sku',
                            foreignField: '_id',
                            as: 'sku'
                        }
                    },
                    {
                        $unwind: {
                            path: '$sku',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listInventoryByFilter) {
                    return resolve({
                        recordsTotal: totalInventory,
                        recordsFiltered: totalInventory,
                        data: []
                    });
                }

                const listInventoryDataTable = listInventoryByFilter.map((inventory, index) => {

                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${inventory._id}" type="checkbox" class="check-record check-record-${inventory._id}" _index ="${index + 1}"><label for="${inventory._id}"></label></div></td>`,
                        indexSTT: skip + index + 1,
                        distributor: inventory.distributor ?
                            inventory.distributor.name :
                            '<span class="badge bg-danger"> <span class="badge bg-danger"> không tồn tại hoặc đã xoá </span> </span>',
                        sku: inventory.sku ?
                            inventory.sku.name :
                            '<span class="badge bg-danger"> <span class="badge bg-danger"> không tồn tại hoặc đã xoá </span> </span>',
                        amount: formatCurrency('###,###.', inventory.amount),
                        statusImport: `<span class="badge" style="background-color: ${STATUSIMPORT_INVENTORY_TYPE[inventory.statusImport].color}">${STATUSIMPORT_INVENTORY_TYPE[inventory.statusImport].value} </span>`,
                        dateImport: moment(inventory.dateImport).format("L"),

                        createAt: moment(inventory.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalInventory,
                    recordsFiltered: totalInventory,
                    data: listInventoryDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách inventory theo bộ lọc import
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterImport({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        statusImport: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }



                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalInventory = await INVENTORY_COLL.countDocuments(conditionObj);

                const listInventoryByFilter = await INVENTORY_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'distributors',
                            localField: 'distributor',
                            foreignField: '_id',
                            as: 'distributor'
                        }
                    },
                    {
                        $unwind: {
                            path: '$distributor',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'products',
                            localField: 'sku',
                            foreignField: '_id',
                            as: 'sku'
                        }
                    },
                    {
                        $unwind: {
                            path: '$sku',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listInventoryByFilter) {
                    return resolve({
                        recordsTotal: totalInventory,
                        recordsFiltered: totalInventory,
                        data: []
                    });
                }

                return resolve({
                    error: false,
                    recordsTotal: totalInventory,
                    recordsFiltered: totalInventory,
                    data: listInventoryByFilter || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc inventory
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc inventory
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listInventoryByFilter = await INVENTORY_COLL.aggregate(arrayFilter)

                if (!listInventoryByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listInventoryByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listInventoryByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('HH:mm DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Tải file import excel mẫu inventory
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    fileImportExcelPreview({
        arrayItemCustomerChoice
    }) {
        return new Promise(async resolve => {
            try {
                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition; // BỘ LỌC && LOẠI IMPORT
                let {
                    listFieldPrimaryKey
                } = condition; // DANH SÁCH PRIMARY KEY

                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        let index = 0;
                        arrayItemCustomerChoice.map((elem) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            if (elem.dataDynamic && elem.dataDynamic.length) {
                                listFieldPrimaryKey = listFieldPrimaryKey.filter(key => key != elem.nameFieldRef); // LỌC PRIMARY KEY MÀ KHÔNG PHẢI REF

                                elem.dataDynamic.map(item => {
                                    workbook.sheet("Report").row(1).cell(index + 1).value(item);
                                    index++;
                                })
                            } else {
                                workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                                index++;
                            }
                        });

                        if (isTrue(condition.checkDownloadDataOld)) { // KIỂM TRA CÓ ĐÍNH ĐÈM DỮ LIỆU CŨ THEO ĐIỀU KIỆN
                            let listItemImport = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].listItemImport; // LIST FIELD ĐÃ ĐƯỢC CẤU HÌNH
                            let {
                                arrayFilter
                            } = this.getConditionArrayFilterExcel(listItemImport, condition.conditionDeleteImport.filter, condition.conditionDeleteImport.condition); // LẤY RA ARRAY ARREGATE

                            let groupByInventory = {};
                            listFieldPrimaryKey.map(key => {
                                groupByInventory = {
                                    ...groupByInventory,
                                    [key]: '$' + key
                                }
                            });

                            arrayFilter = [
                                ...arrayFilter,
                                {
                                    $group: {
                                        _id: {
                                            groupByInventory
                                        },
                                        listData: {
                                            $addToSet: "$$CURRENT"
                                        },
                                    }
                                }
                            ];

                            const listInventoryByFilter = await INVENTORY_COLL.aggregate(arrayFilter);

                            listInventoryByFilter && listInventoryByFilter.length && listInventoryByFilter.map((item, indexInventory) => {
                                let indexValue = 0;
                                arrayItemCustomerChoice.map((elem, indexChoice) => {
                                    let variable = elem.name.split('.');

                                    if (elem.dataDynamic && elem.dataDynamic.length) { // KIỂM TRA FIELD CÓ CHỌN DYNAMIC
                                        item.listData && item.listData.length && item.listData.map(value => { // ARRAY DATA DYNAMIC
                                            if (item.listData.length > elem.dataDynamic.length) { // KIỂM TRA ĐỘ DÀI CỦA DATA SO VỚI SỐ CỘT
                                                // TODO: XỬ LÝ NHIỀU DYNAMIC

                                            } else {
                                                elem.dataDynamic.map(dynamic => {
                                                    let valueOfField;
                                                    if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                        let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                        if (objDataOfVariable) {
                                                            valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                        }
                                                    } else {
                                                        valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                                    }

                                                    if (valueOfField == dynamic) { // CHECK NẾU VALUE === CỘT
                                                        let valueImportDynamic = value[elem.variableChoice] ? value[elem.variableChoice] : '';

                                                        // INSERT DỮ LIỆU VÀO BẢNG VỚI FIELD ĐƯỢC CHỌN THEO DẠNG DYNAMIC
                                                        workbook.sheet("Report").row(indexInventory + 2).cell(indexValue + 1).value(valueImportDynamic);
                                                        indexValue++;
                                                    } else {
                                                        if (elem.dataDynamic && item.listData && elem.dataDynamic.length > item.listData.length) {
                                                            indexValue++;
                                                        }
                                                    }
                                                })
                                            }
                                        })
                                    } else { // DẠNG STATIC
                                        let valueInventory;
                                        if (variable.length > 1) {
                                            let objDataOfVariable = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                            if (objDataOfVariable) {
                                                valueInventory = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                            }
                                        } else {
                                            valueInventory = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                        }

                                        workbook.sheet("Report").row(indexInventory + 2).cell(indexValue + 1).value(valueInventory);
                                        indexValue++;
                                    }
                                });
                            });
                        }

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    pathWriteFile,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Upload File Excel Import Lưu Dữ Liệu inventory
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    importExcel({
        nameCollParent,
        arrayItemCustomerChoice,
        file,
    }) {
        return new Promise(async resolve => {
            try {

                XlsxPopulate.fromFileAsync(file.path)
                    .then(async workbook => {
                        let listData = [];
                        let index = 2;
                        let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition;

                        const client = await MongoClient.connect(URL_DATABASE);
                        const db = client.db(NAME_DATABASE)

                        for (; true;) {
                            if (arrayItemCustomerChoice && arrayItemCustomerChoice.length) {
                                let conditionObj = {};

                                let totalLength = 0;
                                arrayItemCustomerChoice.map((item, index) => {
                                    if (item.dataDynamic && item.dataDynamic.length) {
                                        totalLength += item.dataDynamic.length;
                                    } else {
                                        totalLength++;
                                    }
                                });

                                let indexOfListField = 0;
                                let checkIsRequire = false;
                                let arrayConditionObjDynamic = [];

                                for (let i = 0; i < totalLength; i++) {
                                    if (arrayItemCustomerChoice[indexOfListField].dataDynamic && arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                        let indexOfDynamic = 1;
                                        for (let valueDynamic of arrayItemCustomerChoice[indexOfListField].dataDynamic) {
                                            let letter = colName(i);
                                            let indexOfCeil = letter.toUpperCase() + index;

                                            let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                            if (variable) {

                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: valueDynamic.trim()
                                                });

                                                let conditionOfOneValueDynamic = {
                                                    [arrayItemCustomerChoice[indexOfListField].variableChoice]: variable
                                                }
                                                if (docs) {
                                                    conditionOfOneValueDynamic = {
                                                        ...conditionOfOneValueDynamic,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    }
                                                }

                                                arrayConditionObjDynamic = [
                                                    ...arrayConditionObjDynamic,
                                                    conditionOfOneValueDynamic
                                                ];
                                            }

                                            if (indexOfDynamic < arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                                i++;
                                            }
                                            indexOfDynamic++;
                                        }
                                    } else {
                                        let letter = colName(i);
                                        let indexOfCeil = letter.toUpperCase() + index;
                                        let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                        if (arrayItemCustomerChoice[indexOfListField].isRequire && !variable) {
                                            checkIsRequire = true;
                                            break;
                                        }
                                        if (arrayItemCustomerChoice[indexOfListField].ref && arrayItemCustomerChoice[indexOfListField].ref != nameCollParent) {
                                            if (arrayItemCustomerChoice[indexOfListField].isRequire) {
                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                if(typeof variable !== 'number'){
                                                    variable = variable.trim();
                                                }

                                                if(arrayItemCustomerChoice[indexOfListField].type === 'text') {
                                                    variable = variable.toString();
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: variable
                                                })
   
                                                if (docs) {
                                                    conditionObj = {
                                                        ...conditionObj,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                    };

                                                    if (arrayItemCustomerChoice[indexOfListField].mappingRef && arrayItemCustomerChoice[indexOfListField].mappingRef.length) {
                                                        arrayItemCustomerChoice[indexOfListField].mappingRef.map(mapping => {
                                                            conditionObj = {
                                                                ...conditionObj,
                                                                [mapping]: docs[mapping]
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                [arrayItemCustomerChoice[indexOfListField].name]: variable
                                            };
                                        }
                                    }

                                    indexOfListField++;
                                }
                                if (checkIsRequire) {
                                    break;
                                }

                                conditionObj = {
                                    ...conditionObj,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                }

                                let arrayCondditionObj = []
                                if (arrayConditionObjDynamic && arrayConditionObjDynamic.length) {
                                    arrayConditionObjDynamic.map(item => {
                                        arrayCondditionObj = [
                                            ...arrayCondditionObj,
                                            {
                                                ...conditionObj,
                                                ...item
                                            }
                                        ]
                                    });
                                } else {
                                    arrayCondditionObj = [
                                        ...arrayCondditionObj,
                                        conditionObj
                                    ]
                                }
                                listData = [
                                    ...listData,
                                    ...arrayCondditionObj
                                ];

                                index++;
                            }
                        }

                        await fs.unlinkSync(file.path);

                        console.log({ listData })

                        if (listData.length) {
                            await this.changeDataImport({
                                condition,
                                listInventory: listData
                            });
                        } else {
                            return resolve({
                                error: true,
                                message: 'Import thất bại'
                            });
                        }

                        await this.updateInventoryBeginMonth(listData);

                        return resolve({
                            error: false,
                            message: 'Import thành công'
                        });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    updateInventoryBeginMonth(listData = []) {
        return new Promise(async resolve => {
            try {
                let yearCurrent     = moment().get('years');
                let monthCurrent    = moment().get('months') + 1; // Starting 1
                let bulkUpdateOps   = [];

                for (const inventory of listData) {

                    const infoDistributor = await DISTRIBUTOR_COLL
                        .findById(inventory.distributor)
                        .lean();

                    const infoEmployee = await EMPLOYEE_COLL
                        .findOne({ distributor: inventory.distributor })
                        .select('_id')
                        .lean();

                    const infoInventoryBeginMonth = await INVENTORY_BEGIN_MONTH_COLL
                        .findOne({
                            distributor: inventory.distributor,
                            sku: inventory.sku,
                            month: monthCurrent,
                            year: yearCurrent
                        })
                        .lean();

                    console.log({ infoInventoryBeginMonth })

                    if(infoInventoryBeginMonth){
                        bulkUpdateOps[bulkUpdateOps.length] = {
                            updateOne: {
                                filter: {
                                    _id: ObjectID(infoInventoryBeginMonth._id)
                                },
                                update: {
                                    $inc: {
                                        amount: inventory.amount
                                    },
                                    $set: {
                                        modifyAt: inventory.modifyAt
                                    }
                                }
                            }
                        }
                    } else {
                        bulkUpdateOps[bulkUpdateOps.length] = {
                            insertOne: {
                                document: {
                                    title: moment().format('DD-MM-YYYY HH:mm:ss'),
                                    month: monthCurrent,
                                    year: yearCurrent,
                                    employee: infoEmployee._id,
                                    region: infoDistributor.region,
                                    area: infoDistributor.area,
                                    sku: inventory.sku,
                                    distributor: inventory.distributor,
                                    amount: inventory.amount,
                                    createAt: ISOdate(new Date()),
                                    modifyAt: ISOdate(new Date())
                                }
                            }
                        }
                        // END ELSE IF
                    }
                }

                console.log({ bulkUpdateOps_LENGTH: bulkUpdateOps.length })
                const infoAfterBulkWrite = await INVENTORY_BEGIN_MONTH_COLL.bulkWrite(bulkUpdateOps);

                return resolve({ error: false, data: infoAfterBulkWrite, status: 200 });
            } catch (error) {
                return resolve({ error: true, message: error.message, status: 500 });
            }
        })
    }

    /**
     * Lưu Dữ Liệu Theo Lựa Chọn inventory
     * @param {object} listInventory
     * @param {object} condition
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    changeDataImport({
        condition,
        listInventory
    }) {
        return new Promise(async resolve => {
            try {
                if (isTrue(condition.delete)) { // XÓA DATA CŨ
                    if (isTrue(condition.deleteAll)) { // XÓA TẤT CẢ DỮ LIỆU
                        console.log("====================XÓA TẤT CẢ DỮ LIỆU====================");
                        await INVENTORY_COLL.deleteMany({});
                        let listDataAfterInsert = await INVENTORY_COLL.insertMany(listInventory);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    } else { // XÓA VỚI ĐIỀU KIỆN
                        console.log("====================XÓA VỚI ĐIỀU KIỆN====================");

                        /**
                         * ===========================================================================
                         * =========================XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===========================================================================
                         */
                        let {
                            filter,
                            condition: conditionMultiple,
                        } = condition.conditionDeleteImport;

                        if (!filter || !filter.length) {
                            return resolve({
                                error: true,
                                message: 'Filter do not exist'
                            });
                        }

                        let conditionObj = {
                            state: 1,
                            $or: []
                        };

                        if (filter && filter.length) {
                            if (filter.length > 1) {

                                filter.map(filterObj => {
                                    if (filterObj.type === 'ref') {
                                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionFieldRef);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionFieldRef
                                            };
                                        }
                                    } else {
                                        const conditionByFilter = this.getConditionObj(filterObj);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionByFilter);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionByFilter
                                            };
                                        }
                                    }
                                });

                            } else {
                                let {
                                    type,
                                    ref,
                                    fieldRefName
                                } = filter[0];

                                if (type === 'ref') {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(ref, fieldRefName)
                                    };
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(filter[0])
                                    };
                                }
                            }
                        }

                        if (conditionObj.$or && !conditionObj.$or.length) {
                            delete conditionObj.$or;
                        }

                        let listAfterDelete = await INVENTORY_COLL.deleteMany({
                            ...conditionObj
                        });
                        /**
                         * ===============================================================================
                         * =========================END XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===============================================================================
                         */

                        if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                            for (let item of listInventory) {
                                let listConditionFindOneUpdate = {};
                                let {
                                    listFieldPrimaryKey
                                } = condition;
                                listFieldPrimaryKey.map(elem => {
                                    listConditionFindOneUpdate = {
                                        ...listConditionFindOneUpdate,
                                        [elem]: item[elem]
                                    }
                                });

                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    state: 1
                                }

                                let checkExist = await INVENTORY_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                    $set: item
                                }, {
                                    upsert: true
                                });
                            }
                            return resolve({
                                error: false,
                                message: 'Insert success'
                            });

                        } else { // INSERT CÁI MỚI
                            console.log("====================INSERT CÁI MỚI 2====================");
                            let listDataAfterInsert = await INVENTORY_COLL.insertMany(listInventory);
                            return resolve({
                                error: false,
                                message: 'Insert success',
                                data: listDataAfterInsert
                            });
                        }
                    }
                } else { // KHÔNG XÓA DATA CŨ
                    if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                        console.log("====================KIỂM TRA TỒN TẠI VÀ UPDATE====================");
                        for (let item of listInventory) {
                            let listConditionFindOneUpdate = {};
                            let {
                                listFieldPrimaryKey
                            } = condition;
                            listFieldPrimaryKey.map(elem => {
                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    [elem]: item[elem]
                                }
                            });

                            listConditionFindOneUpdate = {
                                ...listConditionFindOneUpdate,
                                state: 1
                            }

                            await INVENTORY_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                $set: item
                            }, {
                                upsert: true
                            });
                        }
                        return resolve({
                            error: false,
                            message: 'Insert success'
                        });
                    } else { // INSERT CÁI MỚI
                        console.log("====================INSERT CÁI MỚI====================");
                        let listDataAfterInsert = await INVENTORY_COLL.insertMany(listInventory);

                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    }
                }

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc inventory
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
            $or: []
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            let mappingRef = [];
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                    mappingRef = element.mappingRef;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            fieldID: item.fieldID,
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            ref: item.coll,
                            variable: item.name,
                            nameFieldRef,
                            dataEnum: item.dataEnum,
                            isRequire: item.isRequire,
                            dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                            variableChoice: item.variableChoice,
                            nameCollChoice: item.nameCollChoice,
                            mappingRef: mappingRef,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                fieldID: item.fieldID,
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                ref: item.coll,
                                variable: item.name,
                                nameFieldRef,
                                dataEnum: item.dataEnum,
                                isRequire: item.isRequire,
                                dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                variableChoice: item.variableChoice,
                                nameCollChoice: item.nameCollChoice,
                                mappingRef: mappingRef,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    dataEnum: item.dataEnum,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {
            if (filter.length > 1) {

                filter.map(filterObj => {
                    if (filterObj.type === 'ref') {
                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionFieldRef);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionFieldRef
                            };
                        }
                    } else {
                        const conditionByFilter = this.getConditionObj(filterObj);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionByFilter);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionByFilter
                            };
                        }
                    }
                });

            } else {
                let {
                    type,
                    ref,
                    fieldRefName
                } = filter[0];

                if (type === 'ref') {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(ref, fieldRefName)
                    };
                } else {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(filter[0])
                    };
                }
            }
        }

        if (conditionObj.$or && !conditionObj.$or.length) {
            delete conditionObj.$or;
        }



        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let collName = pluralize.plural(item.coll);
                let checkPluralColl = collName[collName.length - 1];

                if (checkPluralColl.toLowerCase() != 's') {
                    collName += 's';
                }

                let lookup = [{
                        $lookup: {
                            from: collName,
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        let collNamePopulate = pluralize.plural(populate.coll);
                        let checkPluralColl = collNamePopulate[collNamePopulate.length - 1];

                        if (checkPluralColl.toLowerCase() != 's') {
                            collNamePopulate += 's';
                        }

                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: collNamePopulate,
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Lấy thông tin inventory
     * @param {objectId} inventoryID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoInventory({
        inventoryID,
        select,
        filter = {},
        explain
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];
                let conditionObj = {
                    state: 1
                };

                if (!checkObjectIDs([inventoryID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị inventoryID không hợp lệ',
                        status: 400
                    });
                }

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                Object.keys(filter).map(key => {
                    if (![].includes(key)) {
                        delete filter[key];
                    }
                });

                let {} = filter;


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['distributor', 'sku'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoInventory = await INVENTORY_COLL
                    .findOne({
                        _id: inventoryID,
                        ...conditionObj
                    })
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoInventory = await INVENTORY_COLL.populate(infoInventory, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoInventory = await INVENTORY_COLL.populate(infoInventory, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoInventory) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin inventory',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoInventory,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách inventory
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListInventorys({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                } else {

                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['distributor', 'sku', 'amountFromNumber', 'amountToNumber', 'dateImportDateRange'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    distributor,
                    sku,
                    amountFromNumber,
                    amountToNumber,
                    dateImportDateRange,
                } = filter;

                distributor && (conditionObj.distributor = distributor);

                sku && (conditionObj.sku = sku);

                if (amountFromNumber && amountToNumber) {
                    conditionObj.amount = {
                        $gte: amountFromNumber,
                        $lte: amountToNumber,
                    };
                }

                if (dateImportDateRange) {
                    let [fromDate, toDate] = dateImportDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.dateImport = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['distributor', 'sku'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listInventorys = await INVENTORY_COLL
                    .find({
                        ...conditionObj
                    }, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listInventorys = await INVENTORY_COLL.populate(listInventorys, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listInventorys = await INVENTORY_COLL.populate(listInventorys, `${ref}.${field}`);
                        }
                    }
                }

                if (!listInventorys) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách inventory',
                        status: 400
                    });
                }

                let totalRecord = await INVENTORY_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listInventorys,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Tạo inventory
     	* @param {string} statusImport

     * @param {objectId} authorID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    insertInventory({
        distributor,
        sku,
        amount,
        statusImport,
        dateImport,
        authorID
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([authorID]))
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ',
                        status: 400
                    });



                if (distributor && !checkObjectIDs(distributor)) {
                    return resolve({
                        error: true,
                        message: 'Nhà phân phối không hợp lệ',
                        status: 400
                    });
                }

                if (sku && !checkObjectIDs(sku)) {
                    return resolve({
                        error: true,
                        message: 'Sản phẩm không hợp lệ',
                        status: 400
                    });
                }

                if (statusImport && !["SUCCESS"].includes(statusImport)) {
                    return resolve({
                        error: true,
                        message: 'Trạng thái  không hợp lệ',
                        status: 400
                    });
                }

                let dataInsert = {
                    distributor,
                    sku,
                    amount,
                    statusImport,
                    dateImport,
                    author: authorID
                };
                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);


                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo INVENTORY thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Cập nhật inventory
     	* @param {string} statusImport

     * @param {objectId} inventoryID
     * @param {objectId} authorID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    updateInventory({
        inventoryID,
        distributor,
        sku,
        amount,
        statusImport,
        dateImport,
        authorID
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([inventoryID]))
                    return resolve({
                        error: true,
                        message: 'inventoryID không hợp lệ',
                        status: 400
                    });

                if (!checkObjectIDs([authorID]))
                    return resolve({
                        error: true,
                        message: 'ID user cập nhật không hợp lệ',
                        status: 400
                    });


                if (distributor && !checkObjectIDs(distributor)) {
                    return resolve({
                        error: true,
                        message: 'Nhà phân phối không hợp lệ',
                        status: 400
                    });
                }

                if (sku && !checkObjectIDs(sku)) {
                    return resolve({
                        error: true,
                        message: 'Sản phẩm không hợp lệ',
                        status: 400
                    });
                }

                if (statusImport && !["SUCCESS"].includes(statusImport)) {
                    return resolve({
                        error: true,
                        message: 'Trạng thái  không hợp lệ',
                        status: 400
                    });
                }

                const checkExists = await INVENTORY_COLL.findOne({
                    _id: inventoryID,
                    state: 1
                });
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'inventory không tồn tại',
                        status: 404
                    });
                }

                let dataUpdate = {
                    userUpdate: authorID
                };
                distributor && (dataUpdate.distributor = distributor);
                sku && (dataUpdate.sku = sku);
                amount && (dataUpdate.amount = amount);
                statusImport && (dataUpdate.statusImport = statusImport);
                dateImport && (dataUpdate.dateImport = new Date(dateImport));
                let infoAfterUpdate = await this.findOneAndUpdate({
                    _id: inventoryID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa inventory
     * @param {objectId} inventoryID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteInventory(inventoryID) {
        return new Promise(async resolve => {
            try {
                let ids = [inventoryID];

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị inventoryID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.deleteMany({
                    _id: {
                        $in: ids
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa inventory
     * @param {objectId} inventoryID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteInventorys(inventorysID) {
        return new Promise(async resolve => {
            try {
                let ids = inventorysID.split(',');

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị inventorysID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.deleteMany({
                    _id: {
                        $in: ids
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

}

module.exports.MODEL = new Model;

/**
 * COLLECTIONS
 */
var INVENTORY_COLL = require('../databases/inventory-coll');
var INVENTORY_BEGIN_MONTH_COLL = require('../../inventory_begin_month/databases/inventory_begin_month-coll');
var DISTRIBUTOR_COLL = require('../../distributor/databases/distributor-coll');
var EMPLOYEE_COLL = require('../../employee/databases/employee-coll');
