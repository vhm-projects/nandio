"use strict";
let request = require('request');
/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const { CLIENT_NO_FIBO, CLIENT_PASS_FIBO, SENDER_NAME_FIBO, FIBO_DOMAIN }                   = require('../../../config/cf_fibo');
/**
 * INTERNAL PACKAGES
 */
 const { checkObjectIDs }           = require('../../../utils/utils');
 const { randomStringOnlyNumber }   = require('../../../utils/string_utils');
 const { addMinuteToDate, betweenTwoDateResultMinute, betweenTwoDateResultSeconds }          
                                    = require('../../../utils/time_utils');

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');

const { sendOTP: sendOTPViamail }                           = require('../../../mailer/module/mail_user')
/**
 * COLLECTIONS
 */
const OTP_COLL  				    = require('../databases/otp-coll');

class Model extends BaseModel {
    constructor() {
        super(OTP_COLL);
        this.TYPE_REGISTER      = 1;
        this.TYPE_LOGIN         = 2;
        this.TYPE_FORGET_PASS   = 3;

        this.STATUS_WAITING_VERIFY  = 0;
        this.STATUS_VERIFIED        = 1;
        this.STATUS_CANCEL_VERIFY   = 2;
    }

    insertOTP__PHONE({ phone, type }) {      // khi người dùng nhập OTP
        return new Promise(async resolve => {
            try {
                let listOTPLatestOFUser = await OTP_COLL.find({ phone, status: this.STATUS_WAITING_VERIFY }).sort({ createAt: -1 }).limit(1);
            
                if (listOTPLatestOFUser && listOTPLatestOFUser.length > 0) { // đang có otp đang ở chế độ chờ verify
                    let itemLatest = listOTPLatestOFUser[0]
                    // kiểm tra đã hết 2 phút -> cho phép gửi tiếp
                    const MINUTE_FOR_COMPARE = 2; //chu kỳ là 2 phút gửi 1 lần
                    let currentTime = Date.now();
                    let minuteHasSub = betweenTwoDateResultMinute(itemLatest.createAt, new Date(currentTime));
                    let secondsHasSub = betweenTwoDateResultSeconds(itemLatest.createAt, new Date(currentTime));
                    if (minuteHasSub < MINUTE_FOR_COMPARE) {
                        return resolve({ error: true, message: `OTP được gửi ${MINUTE_FOR_COMPARE} phút/1 lần, vui lòng chờ ${ 2*60 - secondsHasSub} giây còn lại`, minute_waited: minuteHasSub })
                    }
                }

                // cập nhật tât cả dữ liệu cũ là 2- đã hết hạn
                let infoAllRecordAfterUpdate = await OTP_COLL.updateMany({
                    phone
                }, {
                    $set: {
                        status: this.STATUS_CANCEL_VERIFY
                    }
                });

                const AMOUNT_CHARATOR = 6;
                //TODO: Mở ra khi đã có gửi OTP

                let codeGenerate = randomStringOnlyNumber(AMOUNT_CHARATOR);
                // let codeGenerate = 123456;

                const TIME_FOR_BORN_CODE = 5; // 5 phút chỉ 1 code được tạo ra (CHỜ quá trình người dân có thể đăng ký thông tin bên zalo)
                let now = Date.now();
                let expiredTime = addMinuteToDate(now, TIME_FOR_BORN_CODE);
                // console.log({
                //     now, expiredTime
                // })
                let objectForInsert = {
                    phone, type, code: codeGenerate, expiredTime, phone
                }

                let infoOTPFibo = await this.sendOTP__FIBO({ code: codeGenerate, phone });
                if (infoOTPFibo.statusCode == 200) {
                    let infoAfterInsert = await this.insertData(objectForInsert);
                    if(!infoAfterInsert)
                        return resolve({ error: true, message: 'cannot_insert' });

                    return resolve({ error: false, data: infoAfterInsert });
                } else {
                    return resolve({ error: true, message: 'Xảy ra lỗi trong quá trình gửi mã OTP' });
                }

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    sendOTP__FIBO({ code, phone }) {
        return new Promise(async resolve => {
            try {
                let message     = `Ma xac thuc OTP cua Quy khach tai ung dung Yen Sao Thien Viet la ${code}. Ma OTP co hieu luc trong 2 phut. Vui long khong cung cap OTP cho bat ky ai.`;
                let smsGUID     = 0;
                let serviceType = 0;

                var options = {
                    'method': 'GET',
                    'url': `${FIBO_DOMAIN}/SendMT/service.asmx/SendMaskedSMS?clientNo=${CLIENT_NO_FIBO}&clientPass=${CLIENT_PASS_FIBO}&senderName=${SENDER_NAME_FIBO}&phoneNumber=${phone}&smsMessage=${message}&smsGUID=${smsGUID}&serviceType=${serviceType}`,
                    'headers': 
                        {}
                    };
                await request(options, function (error, response) {
                    if (error) throw new Error(error);
                    return resolve(response);
                });

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
   
    sendOTP__EMAIL({ email, phone }) {
        return new Promise(async resolve => {
            try {
                 /**
                 * 1/kiểm tra user đã đăng ký trên web chưa, hiệu lực OTP còn hay không
                 * 2/ TODO Sơn/SON xử lý kiểm tra 2 phút chỉ gửi 1 lần (sau 2 phút mới tiếp tục cho gửi)
                 */
                let listOTPLatestOFUser = await OTP_COLL.find({ phone }).sort({ createAt: -1 }).limit(1);
                if (!listOTPLatestOFUser || listOTPLatestOFUser.length == 0) 
                    return resolve({ error: true, message: 'cannot_get_otp_record' });
                let itemLatest = listOTPLatestOFUser[0]
                let { code } = itemLatest;
                sendOTPViamail(email, code)
                    
                return resolve({ error: false, message: 'send_success' })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    verifyOTP__PHONE({ phone, code, type }) {
        return new Promise(async resolve => {
            try {
                let isExistItemWaitingForVerify = await OTP_COLL.findOne({ phone, status: this.STATUS_WAITING_VERIFY, type });
                if (!isExistItemWaitingForVerify)
                    return resolve({ error: true, message: 'Mã sai hoặc đã được sử dụng, vui lòng chọn mã khác' }); //trường hợp này là chưa gửi OTP

                if (isExistItemWaitingForVerify.code != code)
                    return resolve({ error: true, message: 'Mã xác nhận không chính xác' });
                
                let infoAfterUpdateOTPUsed = await OTP_COLL.findByIdAndUpdate(isExistItemWaitingForVerify._id, {
                    status: this.STATUS_VERIFIED,
                    modifyAt: Date.now()
                }, {
                    new: true
                })
                return resolve({ error: false, message: 'Xác thực thành công', data: {
                    infoAfterUpdateOTPUsed
                } });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
