"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../database/intalize/base-coll');

/**
 * COLLECTION QÙA TẶNG CỦA HỆ THỐNG 
 */
module.exports = BASE_COLL("reward", {
    /**
     * Hình ảnh quà tặng
     */
    image: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    }, 
    // Tên sản phẩm => Thay vì lấy trong gift => Áp dụng cho CRM STORE
    title: {
        type: String,
    },
    //_________Điểm quy đổi
    point: Number,

    //_________Thời gian bắt đầu
    startTime: Date,

    //_________Thời gian kết thúc
    endTime: Date,
    //_________Nội dung quà tặng
    content: {
        type: String,
    },
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Thương hiệu
    brand: {
        type:  Schema.Types.ObjectId,
        ref : 'brand'
    },
    //_________Số lượng đổi quà
    amount: Number,
    /**
     * 1: Tạo mới quà
     * 2: Tạo quà từ sản phẩm
     */
    type: {
        type: Number,
        default: 1
    },
    gift: {
        kind: String,
        item: { type: Schema.Types.ObjectId, refPath: 'gift.kind' }
    },
    /**
     * Trạng thái hoạt động
     * 1: Hoạt động
     * 2: Đã xóa
     */
    state: {
        type: Number,
        default: 1
    },
    /**
     * Lấy theo dữ liệu tool
     * 1: Hoạt động
     */
    status: {
        type: Number,
        default: 1
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
});
