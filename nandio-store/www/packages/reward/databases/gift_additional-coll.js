"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION QÙA TẶNG CỦA HỆ THỐNG
 */
module.exports = BASE_COLL("gift_additional", {
      
    // Tiêu đề quà tặng
    name: {
        type: String,
    },
    //_________Nội dung quà tặng
    content: {
        type: String,
    },
    /**
     * Trọng lượng (gram)
     */
    weight: {
        type: Number,
        default: 0
    },
    /**
     * Độ dài (cm)
     */
    length: {
        type: Number,
        default: 0
    },
    /**
     * Độ rộng (cm)
     */
    width: {
        type: Number,
        default: 0
    },
    /**
     * Chiều cao (cm)
     */
    height: {
        type: Number,
        default: 0
    },
    //_________Thương hiệu
    brand: {
        type:  Schema.Types.ObjectId,
        ref : 'brand'
    },
    //_________Hình ảnh
    avatar: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    },
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref: 'user'
    },
});
