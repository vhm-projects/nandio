"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path						                    = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_REWARD } 						= require('../constants/reward.uri');
const { IMAGE_MODEL  }                              = require("../../image");
/**
 * MODELS
 */
const REWARD_MODEL                                   = require('../models/reward').MODEL;
const { REWARD_ROUTES } = require('..');
const { request } = require('http');
const BRAND_MODEL                                    = require("../../brand/models/brand").MODEL;
const { PRODUCT_COLL } = require('../../product');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }
    registerRouting() {
        return {
            /**
             * Function: Danh sách quà tặng api cung cấp cho bên store gọi qua
             * Date: 01/04/2021
            */
            [CF_ROUTINGS_REWARD.LIST_REWARD_ENDUSER_FOR_STORE]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { limit, page, keyword } = req.query;
                        let listReward = await REWARD_MODEL.getListForEnduser({ limit: +limit, page: +page, keyword });
                        res.json(listReward)
                    }]
                },
            },
    
            /**
             * Function: Thông tin quà tặng
             * Date: 01/04/2021
            */
            [CF_ROUTINGS_REWARD.INFO_REWARD]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async  function (req, res) {
                        let  { rewardID } = req.params;
                        let infoReward  = await REWARD_MODEL.getInfoReward({ rewardID });
                        return res.json(infoReward);
                    }]
                },
            },
        }
    }
};
