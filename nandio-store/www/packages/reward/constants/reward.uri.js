const BASE_ROUTE = '/reward';
const BASE_ROUTE_ADMIN = '/admin';

const CF_ROUTINGS_REWARD = {
    ADD_REWARD: `${BASE_ROUTE}/add-reward`,
    LIST_REWARD_ADMIN: `${BASE_ROUTE_ADMIN}/list-reward`,
    LIST_REWARD_ENDUSER: `${BASE_ROUTE}/list-reward`,
    LIST_REWARD_ENDUSER_FOR_STORE: `${BASE_ROUTE}/list-reward-for-store`,
    INFO_REWARD: `${BASE_ROUTE}/info-reward/:rewardID`,
    UPDATE_STAGE_REWARD: `${BASE_ROUTE}/update-stage-reward`,
    UPDATE_REWARD: `${BASE_ROUTE}/update-reward`,
    REMOVE_REWARD: `${BASE_ROUTE}/remove-reward/:rewardID`,
    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_REWARD = CF_ROUTINGS_REWARD;
