const REWARD_MODEL 		= require('./models/reward').MODEL;
const REWARD_COLL  		= require('./databases/reward-coll');
const REWARD_ROUTES       = require('./apis/reward');

module.exports = {
    REWARD_ROUTES,
    REWARD_COLL,
    REWARD_MODEL
}
