"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('campaign', {

    /**
     * Tên chiến dịch
     */
    name: {
        type: String,
        required: true,
    },
    /**
     * Từ ngày
     */
    fromDate: {
        type: Date,
        required: true,
    },
    /**
     * Đến ngày
     */
    toDate: {
        type: Date,
        required: true,
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
});