"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('criteria', {

    /**
     * Tên
     */
    name: {
        type: String,
        required: true,
    },
    /**
     * Mô tả
     */
    description: {
        type: String,
    },
    /**
     * Trạng thái 
     * 1: Hoạt động,
     * 2: Không hoạt động
     */
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
});