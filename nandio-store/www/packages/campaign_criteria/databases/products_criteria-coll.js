"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('products_criteria', {

    /**
     * Sản phẩm
     */
    product: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },
    /**
     * Tiêu chí
     */
    criteria: {
        type: Schema.Types.ObjectId,
        ref: 'criteria',
    },
    /**
     * Trạng thái 
     * 1: Có,
     * 2: Không
     */
    status: {
        type: Number,
        default: 2,
        enum: [1, 2],
    },
});