const CRITERIA_COLL = require('./databases/criteria-coll');
const CRITERIA_MODEL = require('./models/criteria').MODEL;
const CRITERIA_ROUTES = require('./apis/criteria');

const CAMPAIGN_COLL  = require('./databases/campaign-coll');
const CAMPAIGN_MODEL  = require('./models/campaign').MODEL;
const CAMPAIGN_ROUTES  = require('./apis/campaign');
    

const PRODUCTS_CRITERIA_COLL  = require('./databases/products_criteria-coll');
const PRODUCTS_CRITERIA_MODEL  = require('./models/products_criteria').MODEL;
const PRODUCTS_CRITERIA_ROUTES  = require('./apis/products_criteria');
    
// MARK REQUIRE

module.exports = {
    CRITERIA_COLL,
    CRITERIA_MODEL,
    CRITERIA_ROUTES,

    CAMPAIGN_COLL,
    CAMPAIGN_MODEL,
    CAMPAIGN_ROUTES,
    

    PRODUCTS_CRITERIA_COLL,
    PRODUCTS_CRITERIA_MODEL,
    PRODUCTS_CRITERIA_ROUTES,
    
    // MARK EXPORT
}