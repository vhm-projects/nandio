/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_CRITERIA_TYPE,
} = require('../constants/criteria');
const {
    CF_ROUTINGS_CRITERIA
} = require('../constants/criteria/criteria.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const CRITERIA_MODEL = require('../models/criteria').MODEL;
const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ CRITERIA  ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: Insert Criteria (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.ADD_CRITERIA]: {
                config: {
                    scopes: ['create:criteria'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Criteria',
                    code: CF_ROUTINGS_CRITERIA.ADD_CRITERIA,
                    inc: path.resolve(__dirname, '../views/criteria/add_criteria.ejs')
                },
                methods: {
                    get: [async function(req, res) {


                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_CRITERIA
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            name,
                            description,
                        } = req.body;


                        let infoAfterInsert = await CRITERIA_MODEL.insert({
                            name,
                            description,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Criteria By Id (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.UPDATE_CRITERIA_BY_ID]: {
                config: {
                    scopes: ['update:criteria'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Criteria',
                    code: CF_ROUTINGS_CRITERIA.UPDATE_CRITERIA_BY_ID,
                    inc: path.resolve(__dirname, '../views/criteria/update_criteria.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            criteriaID
                        } = req.query;

                        let infoCriteria = await CRITERIA_MODEL.getInfoById(criteriaID);
                        if (infoCriteria.error) {
                            return res.redirect('/something-went-wrong');
                        }



                        ChildRouter.renderToView(req, res, {
                            infoCriteria: infoCriteria.data || {},


                            CF_ROUTINGS_CRITERIA
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            criteriaID,
                            name,
                            description,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await CRITERIA_MODEL.update({
                            criteriaID,
                            name,
                            description,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Criteria By Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.UPDATE_CRITERIA_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            criteriaID,
                            name,
                            description,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await CRITERIA_MODEL.updateNotRequire({
                            criteriaID,
                            name,
                            description,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Criteria By Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.DELETE_CRITERIA_BY_ID]: {
                config: {
                    scopes: ['delete:criteria'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            criteriaID
                        } = req.params;

                        const infoAfterDelete = await CRITERIA_MODEL.deleteById(criteriaID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Criteria By List Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.DELETE_CRITERIA_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            criteriaID
                        } = req.body;

                        const infoAfterDelete = await CRITERIA_MODEL.deleteByListId(criteriaID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Criteria By Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.GET_INFO_CRITERIA_BY_ID]: {
                config: {
                    scopes: ['read:info_criteria'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            criteriaID
                        } = req.params;

                        const infoCriteriaById = await CRITERIA_MODEL.getInfoById(criteriaID);
                        res.json(infoCriteriaById);
                    }]
                },
            },

            /**
             * Function: Get List Criteria (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.GET_LIST_CRITERIA]: {
                config: {
                    scopes: ['read:list_criteria'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách tiêu chí',
                    code: CF_ROUTINGS_CRITERIA.GET_LIST_CRITERIA,
                    inc: path.resolve(__dirname, '../views/criteria/list_criterias.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            status,
                            typeGetList
                        } = req.query;

                        let listCriterias = [];
                        if (typeGetList === 'FILTER') {
                            listCriterias = await CRITERIA_MODEL.getListByFilter({
                                keyword,
                                status,
                            });
                        } else {
                            listCriterias = await CRITERIA_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listCriterias: listCriterias.data || [],
                            STATUS_CRITERIA_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Criteria By Field (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.GET_LIST_CRITERIA_BY_FIELD]: {
                config: {
                    scopes: ['read:list_criteria'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Criteria by field isStatus',
                    code: CF_ROUTINGS_CRITERIA.GET_LIST_CRITERIA_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/criteria/list_criterias.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            status,
                            type
                        } = req.query;

                        let listCriterias = await CRITERIA_MODEL.getListByFilter({
                            keyword,
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listCriterias: listCriterias.data || [],
                            STATUS_CRITERIA_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Criteria Server Side (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.GET_LIST_CRITERIA_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listCriteriaServerSide = await CRITERIA_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listCriteriaServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Criteria Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.GET_LIST_CRITERIA_IMPORT]: {
                config: {
                    scopes: ['read:list_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listCriteriaImport = await CRITERIA_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listCriteriaImport);
                    }]
                },
            },

            /**
             * Function: Get List Criteria Excel Server Side (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.GET_LIST_CRITERIA_EXCEL]: {
                config: {
                    scopes: ['read:list_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = CRITERIA_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Criteria Excel Export (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.DOWNLOAD_LIST_CRITERIA_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'criteria'
                        });

                        let conditionObj = CRITERIA_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listCriteria = await CRITERIA_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listCriteria)
                    }]
                },
            },

            /**
             * Function: Setting Criteria Excel Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.SETTING_FILE_CRITERIA_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = CRITERIA_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Criteria Excel Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.DOWNLOAD_FILE_CRITERIA_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_criteria'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'criteria'
                        });

                        let listCriteriaImport = await CRITERIA_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listCriteriaImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listCriteriaImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Criteria Excel Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.CREATE_CRITERIA_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:criteria'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'criteria'
                        });

                        let infoCriteriaAfterImport = await CRITERIA_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'criteria',
                        });

                        res.json(infoCriteriaAfterImport);
                    }]
                },
            },

            /**
             * Function: API Insert Criteria
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.API_ADD_CRITERIA]: {
                config: {
                    scopes: ['create:criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            name,
                            description,
                            status,
                        } = req.body;


                        let infoAfterInsert = await CRITERIA_MODEL.insertCriteria({
                            name,
                            description,
                            status,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: API Get info Criteria
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.API_GET_INFO_CRITERIA]: {
                config: {
                    scopes: ['read:info_criteria'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            criteriaID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoCriteria = await CRITERIA_MODEL.getInfoCriteria({
                            criteriaID,
                            select,
                            filter,
                            explain
                        });
                        res.json(infoCriteria);
                    }]
                },
            },

            /**
             * Function: API Get info Criteria
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CRITERIA.API_GET_LIST_CRITERIAS]: {
                config: {
                    scopes: ['read:list_criteria'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listCriterias = await CRITERIA_MODEL.getListCriterias({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        });
                        res.json(listCriterias);
                    }]
                },
            },

        }
    }
};