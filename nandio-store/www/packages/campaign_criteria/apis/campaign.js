"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const beautifyer = require('js-beautify').js_beautify;
const fs = require('fs');
const moment = require('moment');
const logger = require('../../../config/logger/winston.config');
const chalk = require('chalk');
const log = console.log;

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_CAMPAIGN_TYPE,
} = require('../constants/campaign');
const {
    CF_ROUTINGS_CAMPAIGN
} = require('../constants/campaign/campaign.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const CAMPAIGN_MODEL = require('../models/campaign').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ CAMPAIGN  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Campaign (API, VIEW)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.ADD_CAMPAIGN]: {
                config: {
                    scopes: ['create:campaign'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Campaign',
                    code: CF_ROUTINGS_CAMPAIGN.ADD_CAMPAIGN,
                    inc: path.resolve(__dirname, '../views/campaign/add_campaign.ejs')
                },
                methods: {
                    get: [async function(req, res) {


                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_CAMPAIGN
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            name,
                            fromDate,
                            toDate,
                        } = req.body;


                        let infoAfterInsert = await CAMPAIGN_MODEL.insert({
                            name,
                            fromDate,
                            toDate,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Campaign By Id (API, VIEW)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.UPDATE_CAMPAIGN_BY_ID]: {
                config: {
                    scopes: ['update:campaign'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Campaign',
                    code: CF_ROUTINGS_CAMPAIGN.UPDATE_CAMPAIGN_BY_ID,
                    inc: path.resolve(__dirname, '../views/campaign/update_campaign.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            campaignID
                        } = req.query;

                        let infoCampaign = await CAMPAIGN_MODEL.getInfoById(campaignID);
                        if (infoCampaign.error) {
                            return res.redirect('/something-went-wrong');
                        }



                        ChildRouter.renderToView(req, res, {
                            infoCampaign: infoCampaign.data || {},


                            CF_ROUTINGS_CAMPAIGN
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            campaignID,
                            name,
                            fromDate,
                            toDate,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await CAMPAIGN_MODEL.update({
                            campaignID,
                            name,
                            fromDate,
                            toDate,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Campaign By Id (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.UPDATE_CAMPAIGN_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:campaign'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            campaignID,
                            name,
                            fromDate,
                            toDate,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await CAMPAIGN_MODEL.updateNotRequire({
                            campaignID,
                            name,
                            fromDate,
                            toDate,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Campaign By Id (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.DELETE_CAMPAIGN_BY_ID]: {
                config: {
                    scopes: ['delete:campaign'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            campaignID
                        } = req.params;

                        const infoAfterDelete = await CAMPAIGN_MODEL.deleteById(campaignID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Campaign By List Id (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.DELETE_CAMPAIGN_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:campaign'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            campaignID
                        } = req.body;

                        const infoAfterDelete = await CAMPAIGN_MODEL.deleteByListId(campaignID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Campaign By Id (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.GET_INFO_CAMPAIGN_BY_ID]: {
                config: {
                    scopes: ['read:info_campaign'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            campaignID
                        } = req.params;

                        const infoCampaignById = await CAMPAIGN_MODEL.getInfoById(campaignID);
                        res.json(infoCampaignById);
                    }]
                },
            },

            /**
             * Function: Get List Campaign (API, VIEW)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.GET_LIST_CAMPAIGN]: {
                config: {
                    scopes: ['read:list_campaign'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách chiến dịch',
                    code: CF_ROUTINGS_CAMPAIGN.GET_LIST_CAMPAIGN,
                    inc: path.resolve(__dirname, '../views/campaign/list_campaigns.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            fromDateDateRange,
                            toDateDateRange,
                            status,
                            typeGetList
                        } = req.query;

                        let listCampaigns = [];
                        if (typeGetList === 'FILTER') {
                            listCampaigns = await CAMPAIGN_MODEL.getListByFilter({
                                keyword,
                                fromDateDateRange,
                                toDateDateRange,
                                status,
                            });
                        } else {
                            listCampaigns = await CAMPAIGN_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listCampaigns: listCampaigns.data || [],
                            STATUS_CAMPAIGN_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Campaign By Field (API, VIEW)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.GET_LIST_CAMPAIGN_BY_FIELD]: {
                config: {
                    scopes: ['read:list_campaign'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Campaign by field isStatus',
                    code: CF_ROUTINGS_CAMPAIGN.GET_LIST_CAMPAIGN_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/campaign/list_campaigns.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            fromDateDateRange,
                            toDateDateRange,
                            status,
                            type
                        } = req.query;

                        let listCampaigns = await CAMPAIGN_MODEL.getListByFilter({
                            keyword,
                            fromDateDateRange,
                            toDateDateRange,
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listCampaigns: listCampaigns.data || [],
                            STATUS_CAMPAIGN_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Campaign Server Side (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.GET_LIST_CAMPAIGN_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_campaign'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listCampaignServerSide = await CAMPAIGN_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listCampaignServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Campaign Import (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.GET_LIST_CAMPAIGN_IMPORT]: {
                config: {
                    scopes: ['read:list_campaign'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listCampaignImport = await CAMPAIGN_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listCampaignImport);
                    }]
                },
            },

            /**
             * Function: Get List Campaign Excel Server Side (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.GET_LIST_CAMPAIGN_EXCEL]: {
                config: {
                    scopes: ['read:list_campaign'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = CAMPAIGN_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Campaign Excel Export (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.DOWNLOAD_LIST_CAMPAIGN_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_campaign'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'limit_kpi_config'
                        });

                        let conditionObj = CAMPAIGN_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listCampaign = await CAMPAIGN_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listCampaign)
                    }]
                },
            },

            /**
             * Function: Setting Campaign Excel Import (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.SETTING_FILE_CAMPAIGN_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_campaign'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = CAMPAIGN_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Campaign Excel Import (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.DOWNLOAD_FILE_CAMPAIGN_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_campaign'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'campaign'
                        });

                        let listCampaignImport = await CAMPAIGN_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listCampaignImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listCampaignImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Campaign Excel Import (API)
             * Date: 01/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_CAMPAIGN.CREATE_CAMPAIGN_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:campaign'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'campaign'
                        });

                        let infoCampaignAfterImport = await CAMPAIGN_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'campaign',
                        });

                        res.json(infoCampaignAfterImport);
                    }]
                },
            },

        }
    }
};