"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const beautifyer = require('js-beautify').js_beautify;
const fs = require('fs');
const moment = require('moment');
const logger = require('../../../config/logger/winston.config');
const chalk = require('chalk');
const log = console.log;

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_PRODUCTS_CRITERIA_TYPE,
} = require('../constants/products_criteria');
const {
    CF_ROUTINGS_PRODUCTS_CRITERIA
} = require('../constants/products_criteria/products_criteria.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const PRODUCTS_CRITERIA_MODEL = require('../models/products_criteria').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const {
    PRODUCT_COLL
} = require('../../product');

const CRITERIA_COLL = require('../databases/criteria-coll');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ PRODUCTS_CRITERIA  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Products_criteria (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.ADD_PRODUCTS_CRITERIA]: {
                config: {
                    scopes: ['create:products_criteria'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Products_criteria',
                    code: CF_ROUTINGS_PRODUCTS_CRITERIA.ADD_PRODUCTS_CRITERIA,
                    inc: path.resolve(__dirname, '../views/products_criteria/add_products_criteria.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listCriterias = await CRITERIA_COLL.find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1
                            }).lean()

                        ChildRouter.renderToView(req, res, {
                            listCriterias,
                            CF_ROUTINGS_PRODUCTS_CRITERIA
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            product,
                            criteria,
                            status,
                        } = req.body;


                        let infoAfterInsert = await PRODUCTS_CRITERIA_MODEL.insert({
                            product,
                            criteria,
                            status,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Products_criteria By Id (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.UPDATE_PRODUCTS_CRITERIA_BY_ID]: {
                config: {
                    scopes: ['update:products_criteria'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Products_criteria',
                    code: CF_ROUTINGS_PRODUCTS_CRITERIA.UPDATE_PRODUCTS_CRITERIA_BY_ID,
                    inc: path.resolve(__dirname, '../views/products_criteria/update_products_criteria.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            products_criteriaID
                        } = req.query;

                        let infoProducts_criteria = await PRODUCTS_CRITERIA_MODEL.getInfoById(products_criteriaID);
                        if (infoProducts_criteria.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let conditionProduct = {
                            state: 1,
                            status: 1
                        };
                        if (infoProducts_criteria.data.product) {

                            conditionProduct._id = infoProducts_criteria.data.product._id;

                        }

                        let listProducts = await PRODUCT_COLL
                            .find(conditionProduct)
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .limit(1)
                            .lean();

                        let listCriterias = await CRITERIA_COLL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoProducts_criteria: infoProducts_criteria.data || {},

                            listProducts,
                            listCriterias,
                            CF_ROUTINGS_PRODUCTS_CRITERIA
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            products_criteriaID,
                            product,
                            criteria,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await PRODUCTS_CRITERIA_MODEL.update({
                            products_criteriaID,
                            product,
                            criteria,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Products_criteria By Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.UPDATE_PRODUCTS_CRITERIA_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:products_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            products_criteriaID,
                            product,
                            criteria,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await PRODUCTS_CRITERIA_MODEL.updateNotRequire({
                            products_criteriaID,
                            product,
                            criteria,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Products_criteria By Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.DELETE_PRODUCTS_CRITERIA_BY_ID]: {
                config: {
                    scopes: ['delete:products_criteria'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            products_criteriaID
                        } = req.params;

                        const infoAfterDelete = await PRODUCTS_CRITERIA_MODEL.deleteById(products_criteriaID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Products_criteria By List Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.DELETE_PRODUCTS_CRITERIA_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:products_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            products_criteriaID
                        } = req.body;

                        const infoAfterDelete = await PRODUCTS_CRITERIA_MODEL.deleteByListId(products_criteriaID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Products_criteria By Id (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.GET_INFO_PRODUCTS_CRITERIA_BY_ID]: {
                config: {
                    scopes: ['read:info_products_criteria'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            products_criteriaID
                        } = req.params;

                        const infoProducts_criteriaById = await PRODUCTS_CRITERIA_MODEL.getInfoById(products_criteriaID);
                        res.json(infoProducts_criteriaById);
                    }]
                },
            },

            /**
             * Function: Get List Products_criteria (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.GET_LIST_PRODUCTS_CRITERIA]: {
                config: {
                    scopes: ['read:list_products_criteria'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách tiêu chí sản phẩm',
                    code: CF_ROUTINGS_PRODUCTS_CRITERIA.GET_LIST_PRODUCTS_CRITERIA,
                    inc: path.resolve(__dirname, '../views/products_criteria/list_products_criterias.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            status,
                            typeGetList
                        } = req.query;

                        let listProducts_criterias = [];
                        if (typeGetList === 'FILTER') {
                            listProducts_criterias = await PRODUCTS_CRITERIA_MODEL.getListByFilter({
                                status,
                            });
                        } else {
                            listProducts_criterias = await PRODUCTS_CRITERIA_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listProducts_criterias: listProducts_criterias.data || [],
                            STATUS_PRODUCTS_CRITERIA_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Products_criteria By Field (API, VIEW)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.GET_LIST_PRODUCTS_CRITERIA_BY_FIELD]: {
                config: {
                    scopes: ['read:list_products_criteria'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Products_criteria by field isStatus',
                    code: CF_ROUTINGS_PRODUCTS_CRITERIA.GET_LIST_PRODUCTS_CRITERIA_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/products_criteria/list_products_criterias.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            status,
                            type
                        } = req.query;

                        let listProducts_criterias = await PRODUCTS_CRITERIA_MODEL.getListByFilter({
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listProducts_criterias: listProducts_criterias.data || [],
                            STATUS_PRODUCTS_CRITERIA_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Products_criteria Server Side (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.GET_LIST_PRODUCTS_CRITERIA_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_products_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listProducts_criteriaServerSide = await PRODUCTS_CRITERIA_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listProducts_criteriaServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Products_criteria Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.GET_LIST_PRODUCTS_CRITERIA_IMPORT]: {
                config: {
                    scopes: ['read:list_products_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listProducts_criteriaImport = await PRODUCTS_CRITERIA_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listProducts_criteriaImport);
                    }]
                },
            },

            /**
             * Function: Get List Products_criteria Excel Server Side (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.GET_LIST_PRODUCTS_CRITERIA_EXCEL]: {
                config: {
                    scopes: ['read:list_products_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = PRODUCTS_CRITERIA_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Products_criteria Excel Export (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.DOWNLOAD_LIST_PRODUCTS_CRITERIA_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_products_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'products_criteria'
                        });

                        let conditionObj = PRODUCTS_CRITERIA_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listProducts_criteria = await PRODUCTS_CRITERIA_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listProducts_criteria)
                    }]
                },
            },

            /**
             * Function: Setting Products_criteria Excel Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.SETTING_FILE_PRODUCTS_CRITERIA_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_products_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = PRODUCTS_CRITERIA_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Products_criteria Excel Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.DOWNLOAD_FILE_PRODUCTS_CRITERIA_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_products_criteria'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'products_criteria'
                        });

                        let listProducts_criteriaImport = await PRODUCTS_CRITERIA_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listProducts_criteriaImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listProducts_criteriaImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Products_criteria Excel Import (API)
             * Date: 02/12/2021
             * Dev: Automatic
             */
            [CF_ROUTINGS_PRODUCTS_CRITERIA.CREATE_PRODUCTS_CRITERIA_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:products_criteria'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'products_criteria'
                        });

                        let infoProducts_criteriaAfterImport = await PRODUCTS_CRITERIA_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'products_criteria',
                        });

                        res.json(infoProducts_criteriaAfterImport);
                    }]
                },
            },

        }
    }
};