const BASE_ROUTE = '/campaign';
const API_BASE_ROUTE = '/api/campaign';

const CF_ROUTINGS_CAMPAIGN = {
    ADD_CAMPAIGN: `${BASE_ROUTE}/add-campaign`,
    UPDATE_CAMPAIGN_BY_ID: `${BASE_ROUTE}/update-campaign-by-id`,
    DELETE_CAMPAIGN_BY_ID: `${BASE_ROUTE}/delete/:campaignID`,

    GET_INFO_CAMPAIGN_BY_ID: `${BASE_ROUTE}/info/:campaignID`,
    GET_LIST_CAMPAIGN: `${BASE_ROUTE}/list-campaign`,
    GET_LIST_CAMPAIGN_BY_FIELD: `${BASE_ROUTE}/list-campaign/:field/:value`,
    GET_LIST_CAMPAIGN_SERVER_SIDE: `${BASE_ROUTE}/list-campaign-server-side`,

    UPDATE_CAMPAIGN_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-campaign-by-id-v2`,
    DELETE_CAMPAIGN_BY_LIST_ID: `${BASE_ROUTE}/delete-campaign-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_CAMPAIGN_EXCEL: `${BASE_ROUTE}/list-campaign-excel`,
    DOWNLOAD_LIST_CAMPAIGN_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-campaign-excel-export`,

    // IMPORT EXCEL
    GET_LIST_CAMPAIGN_IMPORT: `${BASE_ROUTE}/list-campaign-import`,
    SETTING_FILE_CAMPAIGN_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-campaign-import-setting`,
    DOWNLOAD_FILE_CAMPAIGN_EXCEL_IMPORT: `${BASE_ROUTE}/list-campaign-import-dowload`,
    CREATE_CAMPAIGN_IMPORT_EXCEL: `${BASE_ROUTE}/create-campaign-import-excel`,



    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_CAMPAIGN = CF_ROUTINGS_CAMPAIGN;