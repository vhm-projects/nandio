const BASE_ROUTE = '/criteria';
const API_BASE_ROUTE = '/api/criteria';

const CF_ROUTINGS_CRITERIA = {
    ADD_CRITERIA: `${BASE_ROUTE}/add-criteria`,
    UPDATE_CRITERIA_BY_ID: `${BASE_ROUTE}/update-criteria-by-id`,
    DELETE_CRITERIA_BY_ID: `${BASE_ROUTE}/delete/:criteriaID`,

    GET_INFO_CRITERIA_BY_ID: `${BASE_ROUTE}/info/:criteriaID`,
    GET_LIST_CRITERIA: `${BASE_ROUTE}/list-criteria`,
    GET_LIST_CRITERIA_BY_FIELD: `${BASE_ROUTE}/list-criteria/:field/:value`,
    GET_LIST_CRITERIA_SERVER_SIDE: `${BASE_ROUTE}/list-criteria-server-side`,

    UPDATE_CRITERIA_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-criteria-by-id-v2`,
    DELETE_CRITERIA_BY_LIST_ID: `${BASE_ROUTE}/delete-criteria-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_CRITERIA_EXCEL: `${BASE_ROUTE}/list-criteria-excel`,
    DOWNLOAD_LIST_CRITERIA_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-criteria-excel-export`,

    // IMPORT EXCEL
    GET_LIST_CRITERIA_IMPORT: `${BASE_ROUTE}/list-criteria-import`,
    SETTING_FILE_CRITERIA_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-criteria-import-setting`,
    DOWNLOAD_FILE_CRITERIA_EXCEL_IMPORT: `${BASE_ROUTE}/list-criteria-import-dowload`,
    CREATE_CRITERIA_IMPORT_EXCEL: `${BASE_ROUTE}/create-criteria-import-excel`,

    API_ADD_CRITERIA: `${API_BASE_ROUTE}/add-criteria`,
    API_GET_INFO_CRITERIA: `${API_BASE_ROUTE}/info-criteria/:criteriaID`,
    API_GET_LIST_CRITERIAS: `${API_BASE_ROUTE}/list-criterias`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_CRITERIA = CF_ROUTINGS_CRITERIA;