/**
 * Trạng thái 
 * 1: Có
 * 2: Không
 */
exports.STATUS_PRODUCTS_CRITERIA_TYPE = {

    "1": {
        value: "Có",
        color: "#0b51b7"
    },

    "2": {
        value: "Không",
        color: "#d63031"
    },

};