const BASE_ROUTE = '/products_criteria';
const API_BASE_ROUTE = '/api/products_criteria';

const CF_ROUTINGS_PRODUCTS_CRITERIA = {
    ADD_PRODUCTS_CRITERIA: `${BASE_ROUTE}/add-products_criteria`,
    UPDATE_PRODUCTS_CRITERIA_BY_ID: `${BASE_ROUTE}/update-products_criteria-by-id`,
    DELETE_PRODUCTS_CRITERIA_BY_ID: `${BASE_ROUTE}/delete/:products_criteriaID`,

    GET_INFO_PRODUCTS_CRITERIA_BY_ID: `${BASE_ROUTE}/info/:products_criteriaID`,
    GET_LIST_PRODUCTS_CRITERIA: `${BASE_ROUTE}/list-products_criteria`,
    GET_LIST_PRODUCTS_CRITERIA_BY_FIELD: `${BASE_ROUTE}/list-products_criteria/:field/:value`,
    GET_LIST_PRODUCTS_CRITERIA_SERVER_SIDE: `${BASE_ROUTE}/list-products_criteria-server-side`,

    UPDATE_PRODUCTS_CRITERIA_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-products_criteria-by-id-v2`,
    DELETE_PRODUCTS_CRITERIA_BY_LIST_ID: `${BASE_ROUTE}/delete-products_criteria-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_PRODUCTS_CRITERIA_EXCEL: `${BASE_ROUTE}/list-products_criteria-excel`,
    DOWNLOAD_LIST_PRODUCTS_CRITERIA_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-products_criteria-excel-export`,

    // IMPORT EXCEL
    GET_LIST_PRODUCTS_CRITERIA_IMPORT: `${BASE_ROUTE}/list-products_criteria-import`,
    SETTING_FILE_PRODUCTS_CRITERIA_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-products_criteria-import-setting`,
    DOWNLOAD_FILE_PRODUCTS_CRITERIA_EXCEL_IMPORT: `${BASE_ROUTE}/list-products_criteria-import-dowload`,
    CREATE_PRODUCTS_CRITERIA_IMPORT_EXCEL: `${BASE_ROUTE}/create-products_criteria-import-excel`,



    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_PRODUCTS_CRITERIA = CF_ROUTINGS_PRODUCTS_CRITERIA;