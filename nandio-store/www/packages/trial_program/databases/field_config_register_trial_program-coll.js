"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('field_config_register_trial_program', {
    trialProgram: {
        type: Schema.Types.ObjectId,
        ref: 'trial_program',
    },
    title: {
        type: String,
    },
    type: {
        type: Number,
    },
    values: {
        type: String,
    },
    status: {
        type: Number,
    },
});