"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('trial_program', {
    title: {
        type: String,
    },
    description: {
        type: String,
    },
    content: {
        type: String,
    },
    startTime: {
        type: Date,
    },
    endTime: {
        type: Date,
    },
    image: {
        type: Schema.Types.ObjectId,
        ref: 'image',
    },
    banner: {
        type: Schema.Types.ObjectId,
        ref: 'banner',
    },
    status: {
        type: Number,
        default: 1,
        enum: [1, 0, 2],
    },
    views: {
        type: Number,
    },
    registers: {
        type: Number,
    },
    products: [{
        type: Schema.Types.ObjectId,
        ref: 'product',
    }],
    userCreate: {
        type: Schema.Types.ObjectId,
        ref: 'user',
    },
});