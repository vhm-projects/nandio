"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('user_fields_register_trial_program', {
    field: {
        type: Schema.Types.ObjectId,
        ref: 'field_config_register_trial_program',
    },
    value: {
        type: String,
    },
});