"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('trial_program_history_contact', {

    /**
     * Trạng Thái 
     * 1: Liên Hệ Thất Bại,
     * 2: Liên Hệ Đặt Lịch Thành Công,
     * 3: Yêu Cầu Huỷ Đơn,
     * 4: Đổi Quà Thất Bại (hết hàng),
     * 5: Đổi Quà Thành Công
     */
    status: {
        type: Number,
        enum: [1, 2, 3, 4, 5],
    },
    /**
     * Thời Gian Hẹn
     */
    reminderTime: {
        type: Date,
    },
    /**
     * Note
     */
    note: {
        type: String,
    },
    /**
     * Tên Chương Trình
     */
    trialProgram: {
        type: Schema.Types.ObjectId,
        ref: 'trial_program',
    },
    /**
     * phone
     */
    phone: {
        type: String,
    },
    /**
     * user_register_trial_program
     */
    user_register_trial_program: {
        type: Schema.Types.ObjectId,
        ref: 'user_register_trial_program',
    },

    // lưu thông tin sản phẩm đơn hàng xuât (chỉ với status = 5 [xuất đơn thành công])
    // vì mỗi record chỉ lưu 1 sản phẩm -> 1 đơn xuất ra n record này nên cần lưu thông tin này để sau hiển thị theo từng đơn
    // dataOrderExport: { product, amount }
    dataOrderExport: {
        type: Object
    }
});