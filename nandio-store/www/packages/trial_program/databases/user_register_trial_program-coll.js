"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

module.exports = BASE_COLL('user_register_trial_program', {

    /**
     * Chương Trình
     */
    trialProgram: {
        type: Schema.Types.ObjectId,
        ref: 'trial_program',
    },
    /**
     * Sản Phẩm
     */
    // product: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'product',
    // },
    products: [
		{
			type: Schema.Types.ObjectId,
			ref: 'product'
		}
	],
    /**
     * Trạng Thái
     */
    status: {
        type: Number,
        default: 0,
    },
    /**
     * deviceID
     */
    deviceID: {
        type: String,
        trim: true,
    },
    fullname: {
        type: String,
    },
    email: {
        type: String,
    },
    phone: {
        type: String,
    },
    address: {
        type: String,
    },
    cityNumber: {
        type: String,
    },
    cityText: {
        type: String,
    },
    districtNumber: {
        type: String,
    },
    districtText: {
        type: String,
    },
    wardNumber: {
        type: String,
    },
    wardText: {
        type: String,
    },
    fullAddress: {
        type: String,
    },
    note: {
        type: String,
    },
    fields: [{
        type: Schema.Types.ObjectId,
        ref: 'user_fields_register_trial_program',
    }],
    store: {
        type: Schema.Types.ObjectId,
        ref: 'store',
    },
    employee: {
        type: Schema.Types.ObjectId,
        ref: 'employee',
    },

    /**
     * bổ sung trường mới so với QSHOP
     */
    // trạng thái cuối cùng của đơn này (khi NA cập nhật)
    lastest_trial_program_history_contact: {
        type: Schema.Types.ObjectId,
        ref: 'trial_program_history_contact',
    },
    // là dữ liệu ánh xạ status từ trial_program_history_contact để dễ truy vấn từ coll này
    lastest_status_contact: {
        type: Number
    },
    // là dữ liệu ánh xạ của centralize_order -> mục đích hiển thị nhanh
    centralizeOrderID: String,
    // Số lượng/sản phẩm
	amountPerProduct: {
		type: Number,
		default: 0
	},
});