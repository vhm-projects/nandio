/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const beautifyer = require('js-beautify').js_beautify;
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_TRIAL_PROGRAM_TYPE,
} = require('../constants/trial_program');
const {
    CF_ROUTINGS_TRIAL_PROGRAM
} = require('../constants/trial_program/trial_program.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const TRIAL_PROGRAM_MODEL = require('../models/trial_program').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ TRIAL_PROGRAM  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Trial_program (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.ADD_TRIAL_PROGRAM]: {
                config: {
                    scopes: ['create:trial_program'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Trial_program',
                    code: CF_ROUTINGS_TRIAL_PROGRAM.ADD_TRIAL_PROGRAM,
                    inc: path.resolve(__dirname, '../views/trial_program/add_trial_program.ejs')
                },
                methods: {
                    get: [async function(req, res) {


                        ChildRouter.renderToView(req, res, {

                            CF_ROUTINGS_TRIAL_PROGRAM
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            title,
                            description,
                            content,
                            startTime,
                            endTime,
                            status,
                            views,
                            registers,
                        } = req.body;


                        let infoAfterInsert = await TRIAL_PROGRAM_MODEL.insert({
                            title,
                            description,
                            content,
                            startTime,
                            endTime,
                            status,
                            views,
                            registers,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Trial_program By Id (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.UPDATE_TRIAL_PROGRAM_BY_ID]: {
                config: {
                    scopes: ['update:trial_program'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Trial_program',
                    code: CF_ROUTINGS_TRIAL_PROGRAM.UPDATE_TRIAL_PROGRAM_BY_ID,
                    inc: path.resolve(__dirname, '../views/trial_program/update_trial_program.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            trial_programID
                        } = req.query;

                        let infoTrial_program = await TRIAL_PROGRAM_MODEL.getInfoById(trial_programID);
                        if (infoTrial_program.error) {
                            return res.redirect('/something-went-wrong');
                        }



                        ChildRouter.renderToView(req, res, {
                            infoTrial_program: infoTrial_program.data || {},


                            CF_ROUTINGS_TRIAL_PROGRAM
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            trial_programID,
                            title,
                            description,
                            content,
                            startTime,
                            endTime,
                            status,
                            views,
                            registers,
                        } = req.body;


                        const infoAfterUpdate = await TRIAL_PROGRAM_MODEL.update({
                            trial_programID,
                            title,
                            description,
                            content,
                            startTime,
                            endTime,
                            status,
                            views,
                            registers,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Trial_program By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.UPDATE_TRIAL_PROGRAM_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            trial_programID,
                            title,
                            description,
                            content,
                            startTime,
                            endTime,
                            status,
                            views,
                            registers,
                        } = req.body;


                        const infoAfterUpdate = await TRIAL_PROGRAM_MODEL.updateNotRequire({
                            trial_programID,
                            title,
                            description,
                            content,
                            startTime,
                            endTime,
                            status,
                            views,
                            registers,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Trial_program By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.DELETE_TRIAL_PROGRAM_BY_ID]: {
                config: {
                    scopes: ['delete:trial_program'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            trial_programID
                        } = req.params;

                        const infoAfterDelete = await TRIAL_PROGRAM_MODEL.deleteById(trial_programID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Trial_program By List Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.DELETE_TRIAL_PROGRAM_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            trial_programID
                        } = req.body;

                        const infoAfterDelete = await TRIAL_PROGRAM_MODEL.deleteByListId(trial_programID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Trial_program By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.GET_INFO_TRIAL_PROGRAM_BY_ID]: {
                config: {
                    scopes: ['read:info_trial_program'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            trial_programID
                        } = req.params;

                        const infoTrial_programById = await TRIAL_PROGRAM_MODEL.getInfoById(trial_programID);
                        res.json(infoTrial_programById);
                    }]
                },
            },

            /**
             * Function: Get List Trial_program (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.GET_LIST_TRIAL_PROGRAM]: {
                config: {
                    scopes: ['read:list_trial_program'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Trial_program',
                    code: CF_ROUTINGS_TRIAL_PROGRAM.GET_LIST_TRIAL_PROGRAM,
                    inc: path.resolve(__dirname, '../views/trial_program/list_trial_programs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            startTimeDateRange,
                            endTimeDateRange,
                            status,
                            typeGetList
                        } = req.query;

                        let listTrial_programs = [];
                        if (typeGetList === 'FILTER') {
                            listTrial_programs = await TRIAL_PROGRAM_MODEL.getListByFilter({
                                keyword,
                                startTimeDateRange,
                                endTimeDateRange,
                                status,
                            });
                        } else {
                            listTrial_programs = await TRIAL_PROGRAM_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listTrial_programs: listTrial_programs.data || [],
                            STATUS_TRIAL_PROGRAM_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Trial_program By Field (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.GET_LIST_TRIAL_PROGRAM_BY_FIELD]: {
                config: {
                    scopes: ['read:list_trial_program'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Trial_program by field isStatus',
                    code: CF_ROUTINGS_TRIAL_PROGRAM.GET_LIST_TRIAL_PROGRAM_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/trial_program/list_trial_programs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            startTimeDateRange,
                            endTimeDateRange,
                            status,
                            type
                        } = req.query;

                        let listTrial_programs = await TRIAL_PROGRAM_MODEL.getListByFilter({
                            keyword,
                            startTimeDateRange,
                            endTimeDateRange,
                            status,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listTrial_programs: listTrial_programs.data || [],
                            STATUS_TRIAL_PROGRAM_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Trial_program Server Side (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.GET_LIST_TRIAL_PROGRAM_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listTrial_programServerSide = await TRIAL_PROGRAM_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listTrial_programServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Trial_program Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.GET_LIST_TRIAL_PROGRAM_IMPORT]: {
                config: {
                    scopes: ['read:list_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listTrial_programImport = await TRIAL_PROGRAM_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listTrial_programImport);
                    }]
                },
            },

            /**
             * Function: Get List Trial_program Excel Server Side (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.GET_LIST_TRIAL_PROGRAM_EXCEL]: {
                config: {
                    scopes: ['read:list_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = TRIAL_PROGRAM_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Trial_program Excel Export (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.DOWNLOAD_LIST_TRIAL_PROGRAM_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'trial_program'
                        });

                        let conditionObj = TRIAL_PROGRAM_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listTrial_program = await TRIAL_PROGRAM_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listTrial_program)
                    }]
                },
            },

            /**
             * Function: Setting Trial_program Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.SETTING_FILE_TRIAL_PROGRAM_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = TRIAL_PROGRAM_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Trial_program Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.DOWNLOAD_FILE_TRIAL_PROGRAM_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_trial_program'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'trial_program'
                        });

                        let listTrial_programImport = await TRIAL_PROGRAM_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listTrial_programImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listTrial_programImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Trial_program Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.CREATE_TRIAL_PROGRAM_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'trial_program'
                        });

                        let infoTrial_programAfterImport = await TRIAL_PROGRAM_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'trial_program',
                        });

                        res.json(infoTrial_programAfterImport);
                    }]
                },
            },

            /**
             * Function: API Get info Trial_program
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.API_GET_INFO_TRIAL_PROGRAM_OF_ME]: {
                config: {
                    scopes: ['read:info_trial_program'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {
                            trial_programID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoTrial_program = await TRIAL_PROGRAM_MODEL.getInfoTrial_program({
                            trial_programID,
                            select,
                            filter,
                            explain,
                            authorID
                        });
                        res.json(infoTrial_program);
                    }]
                },
            },

            /**
             * Function: API Get list Trial_program
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.API_GET_LIST_TRIAL_PROGRAMS_OF_ME]: {
                config: {
                    scopes: ['read:list_trial_program'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listTrial_programs = await TRIAL_PROGRAM_MODEL.getListTrial_programs({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                            authorID
                        });
                        res.json(listTrial_programs);
                    }]
                },
            },

            /**
             * Function: API Insert Trial_program
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.API_ADD_TRIAL_PROGRAM_OF_ME]: {
                config: {
                    scopes: ['create:trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            title,
                            description,
                            content,
                            startTime,
                            endTime,
                            image,
                            banner,
                            status,
                            views,
                            registers,
                            products,
                        } = req.body;


                        if (image) {
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                name: image.name,
                                path: image.path,
                                type: image.type,
                                size: image.size
                            });
                            image = infoImageAfterInsert.data._id;
                        }

                        if (banner) {
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                name: banner.name,
                                path: banner.path,
                                type: banner.type,
                                size: banner.size
                            });
                            banner = infoImageAfterInsert.data._id;
                        }

                        let infoAfterInsert = await TRIAL_PROGRAM_MODEL.insertTrial_program({
                            title,
                            description,
                            content,
                            startTime,
                            endTime,
                            image,
                            banner,
                            status,
                            views,
                            registers,
                            products,
                            userCreate,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: API Update Trial_program
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.API_UPDATE_TRIAL_PROGRAM_OF_ME]: {
                config: {
                    scopes: ['update:trial_program'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            trial_programID
                        } = req.params;
                        let {
                            title,
                            description,
                            content,
                            startTime,
                            endTime,
                            image,
                            banner,
                            status,
                            views,
                            registers,
                            products,
                            userCreate,
                        } = req.body;


                        if (image) {
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                name: image.name,
                                path: image.path,
                                type: image.type,
                                size: image.size
                            });
                            image = infoImageAfterInsert.data._id;
                        }

                        if (banner) {
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                name: banner.name,
                                path: banner.path,
                                type: banner.type,
                                size: banner.size
                            });
                            banner = infoImageAfterInsert.data._id;
                        }

                        let infoAfterUpdate = await TRIAL_PROGRAM_MODEL.updateTrial_program({
                            trial_programID,
                            title,
                            description,
                            content,
                            startTime,
                            endTime,
                            image,
                            banner,
                            status,
                            views,
                            registers,
                            products,
                            userCreate,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: API Delete Trial_program
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.API_DELETE_TRIAL_PROGRAM_OF_ME]: {
                config: {
                    scopes: ['delete:trial_program'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {
                            trial_programID
                        } = req.params;

                        const infoAfterDelete = await TRIAL_PROGRAM_MODEL.deleteTrial_program(trial_programID, authorID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: API Delete Trial_program
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.API_DELETE_TRIAL_PROGRAMS_OF_ME]: {
                config: {
                    scopes: ['delete:trial_program'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {
                            trial_programsID
                        } = req.params;

                        const infoAfterDelete = await TRIAL_PROGRAM_MODEL.deleteTrial_programs(trial_programsID, authorID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Trial_program By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
             [CF_ROUTINGS_TRIAL_PROGRAM.API_PUSH_NOTI_WHEN_USER_REGISTER_TRAIL_PROGRAM]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            employeeID
                        } = req.params;
                        const { 
                            fullname, phone,
                            userRegisterTrialProgramID,
                        } = req.body;

                        TRIAL_PROGRAM_MODEL.pushNotiWhenUserRegisterTrialProgram({
                            employeeID, 
                            fullname, phone,
                            userRegisterTrialProgramID,
                        });
                        res.json({ error: false, message: 'success' });
                    }]
                },
            },

        }
    }
};