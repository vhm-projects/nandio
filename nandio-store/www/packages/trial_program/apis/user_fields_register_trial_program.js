/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const beautifyer = require('js-beautify').js_beautify;
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {} = require('../constants/user_fields_register_trial_program');
const {
    CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM
} = require('../constants/user_fields_register_trial_program/user_fields_register_trial_program.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL = require('../models/user_fields_register_trial_program').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const {
    FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_COLL
} = require('../');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ USER_FIELDS_REGISTER_TRIAL_PROGRAM  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert User_fields_register_trial_program (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.ADD_USER_FIELDS_REGISTER_TRIAL_PROGRAM]: {
                config: {
                    scopes: ['create:user_fields_register_trial_program'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm User_fields_register_trial_program',
                    code: CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.ADD_USER_FIELDS_REGISTER_TRIAL_PROGRAM,
                    inc: path.resolve(__dirname, '../views/user_fields_register_trial_program/add_user_fields_register_trial_program.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listFields = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_COLL.find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1
                            }).lean()

                        ChildRouter.renderToView(req, res, {
                            listFields,
                            CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            field,
                            value,
                        } = req.body;


                        let infoAfterInsert = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.insert({
                            field,
                            value,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update User_fields_register_trial_program By Id (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.UPDATE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_BY_ID]: {
                config: {
                    scopes: ['update:user_fields_register_trial_program'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật User_fields_register_trial_program',
                    code: CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.UPDATE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_BY_ID,
                    inc: path.resolve(__dirname, '../views/user_fields_register_trial_program/update_user_fields_register_trial_program.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            user_fields_register_trial_programID
                        } = req.query;

                        let infoUser_fields_register_trial_program = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.getInfoById(user_fields_register_trial_programID);
                        if (infoUser_fields_register_trial_program.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let listFields = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_COLL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoUser_fields_register_trial_program: infoUser_fields_register_trial_program.data || {},

                            listFields,
                            CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            user_fields_register_trial_programID,
                            field,
                            value,
                        } = req.body;


                        const infoAfterUpdate = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.update({
                            user_fields_register_trial_programID,
                            field,
                            value,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require User_fields_register_trial_program By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.UPDATE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:user_fields_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            user_fields_register_trial_programID,
                            field,
                            value,
                        } = req.body;


                        const infoAfterUpdate = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.updateNotRequire({
                            user_fields_register_trial_programID,
                            field,
                            value,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete User_fields_register_trial_program By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.DELETE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_BY_ID]: {
                config: {
                    scopes: ['delete:user_fields_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            user_fields_register_trial_programID
                        } = req.params;

                        const infoAfterDelete = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.deleteById(user_fields_register_trial_programID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete User_fields_register_trial_program By List Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.DELETE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:user_fields_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            user_fields_register_trial_programID
                        } = req.body;

                        const infoAfterDelete = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.deleteByListId(user_fields_register_trial_programID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info User_fields_register_trial_program By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.GET_INFO_USER_FIELDS_REGISTER_TRIAL_PROGRAM_BY_ID]: {
                config: {
                    scopes: ['read:info_user_fields_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            user_fields_register_trial_programID
                        } = req.params;

                        const infoUser_fields_register_trial_programById = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.getInfoById(user_fields_register_trial_programID);
                        res.json(infoUser_fields_register_trial_programById);
                    }]
                },
            },

            /**
             * Function: Get List User_fields_register_trial_program (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.GET_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM]: {
                config: {
                    scopes: ['read:list_user_fields_register_trial_program'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách User_fields_register_trial_program',
                    code: CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.GET_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM,
                    inc: path.resolve(__dirname, '../views/user_fields_register_trial_program/list_user_fields_register_trial_programs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            typeGetList
                        } = req.query;

                        let listUser_fields_register_trial_programs = [];
                        if (typeGetList === 'FILTER') {
                            listUser_fields_register_trial_programs = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.getListByFilter({
                                keyword,
                            });
                        } else {
                            listUser_fields_register_trial_programs = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listUser_fields_register_trial_programs: listUser_fields_register_trial_programs.data || [],

                        });
                    }]
                },
            },

            /**
             * Function: Get List User_fields_register_trial_program By Field (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.GET_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM_BY_FIELD]: {
                config: {
                    scopes: ['read:list_user_fields_register_trial_program'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách User_fields_register_trial_program by field isStatus',
                    code: CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.GET_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/user_fields_register_trial_program/list_user_fields_register_trial_programs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            type
                        } = req.query;

                        let listUser_fields_register_trial_programs = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.getListByFilter({
                            keyword,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listUser_fields_register_trial_programs: listUser_fields_register_trial_programs.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List User_fields_register_trial_program Server Side (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.GET_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_user_fields_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listUser_fields_register_trial_programServerSide = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listUser_fields_register_trial_programServerSide);
                    }]
                },
            },

            /**
             * Function: Get List User_fields_register_trial_program Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.GET_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM_IMPORT]: {
                config: {
                    scopes: ['read:list_user_fields_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listUser_fields_register_trial_programImport = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listUser_fields_register_trial_programImport);
                    }]
                },
            },

            /**
             * Function: Get List User_fields_register_trial_program Excel Server Side (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.GET_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM_EXCEL]: {
                config: {
                    scopes: ['read:list_user_fields_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download User_fields_register_trial_program Excel Export (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.DOWNLOAD_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_user_fields_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'user_fields_register_trial_program'
                        });

                        let conditionObj = USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listUser_fields_register_trial_program = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listUser_fields_register_trial_program)
                    }]
                },
            },

            /**
             * Function: Setting User_fields_register_trial_program Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.SETTING_FILE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_user_fields_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download User_fields_register_trial_program Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.DOWNLOAD_FILE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_user_fields_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'user_fields_register_trial_program'
                        });

                        let listUser_fields_register_trial_programImport = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listUser_fields_register_trial_programImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listUser_fields_register_trial_programImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload User_fields_register_trial_program Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM.CREATE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:user_fields_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'user_fields_register_trial_program'
                        });

                        let infoUser_fields_register_trial_programAfterImport = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'user_fields_register_trial_program',
                        });

                        res.json(infoUser_fields_register_trial_programAfterImport);
                    }]
                },
            },

        }
    }
};