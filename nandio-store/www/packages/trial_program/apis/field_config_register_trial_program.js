/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const beautifyer = require('js-beautify').js_beautify;
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {} = require('../constants/field_config_register_trial_program');
const {
    CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM
} = require('../constants/field_config_register_trial_program/field_config_register_trial_program.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL = require('../models/field_config_register_trial_program').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const TRIAL_PROGRAM_COLL = require('../databases/trial_program-coll');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ FIELD_CONFIG_REGISTER_TRIAL_PROGRAM  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Field_config_register_trial_program (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.ADD_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM]: {
                config: {
                    scopes: ['create:field_config_register_trial_program'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Field_config_register_trial_program',
                    code: CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.ADD_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM,
                    inc: path.resolve(__dirname, '../views/field_config_register_trial_program/add_field_config_register_trial_program.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listTrialPrograms = await TRIAL_PROGRAM_COLL.find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1
                            }).lean()

                        ChildRouter.renderToView(req, res, {
                            listTrialPrograms,
                            CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            trialProgram,
                            title,
                            type,
                            values,
                            status,
                        } = req.body;


                        let infoAfterInsert = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.insert({
                            trialProgram,
                            title,
                            type,
                            values,
                            status,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Field_config_register_trial_program By Id (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.UPDATE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_BY_ID]: {
                config: {
                    scopes: ['update:field_config_register_trial_program'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Field_config_register_trial_program',
                    code: CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.UPDATE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_BY_ID,
                    inc: path.resolve(__dirname, '../views/field_config_register_trial_program/update_field_config_register_trial_program.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field_config_register_trial_programID
                        } = req.query;

                        let infoField_config_register_trial_program = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getInfoById(field_config_register_trial_programID);
                        if (infoField_config_register_trial_program.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let listTrialPrograms = await TRIAL_PROGRAM_COLL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoField_config_register_trial_program: infoField_config_register_trial_program.data || {},

                            listTrialPrograms,
                            CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            field_config_register_trial_programID,
                            trialProgram,
                            title,
                            type,
                            values,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.update({
                            field_config_register_trial_programID,
                            trialProgram,
                            title,
                            type,
                            values,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Field_config_register_trial_program By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.UPDATE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:field_config_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            field_config_register_trial_programID,
                            trialProgram,
                            title,
                            type,
                            values,
                            status,
                        } = req.body;


                        const infoAfterUpdate = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.updateNotRequire({
                            field_config_register_trial_programID,
                            trialProgram,
                            title,
                            type,
                            values,
                            status,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Field_config_register_trial_program By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.DELETE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_BY_ID]: {
                config: {
                    scopes: ['delete:field_config_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            field_config_register_trial_programID
                        } = req.params;

                        const infoAfterDelete = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.deleteById(field_config_register_trial_programID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Field_config_register_trial_program By List Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.DELETE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:field_config_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            field_config_register_trial_programID
                        } = req.body;

                        const infoAfterDelete = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.deleteByListId(field_config_register_trial_programID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Field_config_register_trial_program By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.GET_INFO_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_BY_ID]: {
                config: {
                    scopes: ['read:info_field_config_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            field_config_register_trial_programID
                        } = req.params;

                        const infoField_config_register_trial_programById = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getInfoById(field_config_register_trial_programID);
                        res.json(infoField_config_register_trial_programById);
                    }]
                },
            },

            /**
             * Function: Get List Field_config_register_trial_program (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.GET_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM]: {
                config: {
                    scopes: ['read:list_field_config_register_trial_program'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Field_config_register_trial_program',
                    code: CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.GET_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM,
                    inc: path.resolve(__dirname, '../views/field_config_register_trial_program/list_field_config_register_trial_programs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            keyword,
                            typeGetList
                        } = req.query;

                        let listField_config_register_trial_programs = [];
                        if (typeGetList === 'FILTER') {
                            listField_config_register_trial_programs = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getListByFilter({
                                keyword,
                            });
                        } else {
                            listField_config_register_trial_programs = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listField_config_register_trial_programs: listField_config_register_trial_programs.data || [],

                        });
                    }]
                },
            },

            /**
             * Function: Get List Field_config_register_trial_program By Field (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.GET_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_BY_FIELD]: {
                config: {
                    scopes: ['read:list_field_config_register_trial_program'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Field_config_register_trial_program by field isStatus',
                    code: CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.GET_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/field_config_register_trial_program/list_field_config_register_trial_programs.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            keyword,
                            type
                        } = req.query;

                        let listField_config_register_trial_programs = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getListByFilter({
                            keyword,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listField_config_register_trial_programs: listField_config_register_trial_programs.data || [],

                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Field_config_register_trial_program Server Side (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.GET_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_field_config_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listField_config_register_trial_programServerSide = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listField_config_register_trial_programServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Field_config_register_trial_program Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.GET_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_IMPORT]: {
                config: {
                    scopes: ['read:list_field_config_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listField_config_register_trial_programImport = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listField_config_register_trial_programImport);
                    }]
                },
            },

            /**
             * Function: Get List Field_config_register_trial_program Excel Server Side (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.GET_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_EXCEL]: {
                config: {
                    scopes: ['read:list_field_config_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Field_config_register_trial_program Excel Export (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.DOWNLOAD_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_field_config_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'field_config_register_trial_program'
                        });

                        let conditionObj = FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listField_config_register_trial_program = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listField_config_register_trial_program)
                    }]
                },
            },

            /**
             * Function: Setting Field_config_register_trial_program Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.SETTING_FILE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_field_config_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Field_config_register_trial_program Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.DOWNLOAD_FILE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_field_config_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'field_config_register_trial_program'
                        });

                        let listField_config_register_trial_programImport = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listField_config_register_trial_programImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listField_config_register_trial_programImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Field_config_register_trial_program Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM.CREATE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:field_config_register_trial_program'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'field_config_register_trial_program'
                        });

                        let infoField_config_register_trial_programAfterImport = await FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'field_config_register_trial_program',
                        });

                        res.json(infoField_config_register_trial_programAfterImport);
                    }]
                },
            },

        }
    }
};