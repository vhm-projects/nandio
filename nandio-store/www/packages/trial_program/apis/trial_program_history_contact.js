/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');
const beautifyer = require('js-beautify').js_beautify;
const moment = require('moment');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter = require('../../../routing/child_routing');
const {
    STATUS_TRIAL_PROGRAM_HISTORY_CONTACT_TYPE,
} = require('../constants/trial_program_history_contact');
const {
    CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT
} = require('../constants/trial_program_history_contact/trial_program_history_contact.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');

/**
 * MODELS
 */
const TRIAL_PROGRAM_HISTORY_CONTACT_MODEL = require('../models/trial_program_history_contact').MODEL;

const MANAGE_COLL_MODEL = require('../../../models/manage_coll').MODEL;

/**
 * COLLECTIONS
 */

const TRIAL_PROGRAM_COLL = require('../databases/trial_program-coll');

const USER_REGISTER_TRIAL_PROGRAM_COLL = require('../databases/user_register_trial_program-coll');



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ TRIAL_PROGRAM_HISTORY_CONTACT  ===============================
             * =============================== ************* ===============================
             */




            /**
             * Function: Insert Trial_program_history_contact (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.ADD_TRIAL_PROGRAM_HISTORY_CONTACT]: {
                config: {
                    scopes: ['create:trial_program_history_contact'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Thêm Trial_program_history_contact',
                    code: CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.ADD_TRIAL_PROGRAM_HISTORY_CONTACT,
                    inc: path.resolve(__dirname, '../views/trial_program_history_contact/add_trial_program_history_contact.ejs')
                },
                methods: {
                    get: [async function(req, res) {

                        let listUser_register_trial_programs = await USER_REGISTER_TRIAL_PROGRAM_COLL.find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1
                            }).lean()

                        ChildRouter.renderToView(req, res, {
                            listUser_register_trial_programs,
                            CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT
                        });
                    }],
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            status,
                            reminderTime,
                            note,
                            user_register_trial_program,
                        } = req.body;


                        let infoAfterInsert = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.insert({
                            status,
                            reminderTime,
                            note,
                            user_register_trial_program,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Update Trial_program_history_contact By Id (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.UPDATE_TRIAL_PROGRAM_HISTORY_CONTACT_BY_ID]: {
                config: {
                    scopes: ['update:trial_program_history_contact'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Cập nhật Trial_program_history_contact',
                    code: CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.UPDATE_TRIAL_PROGRAM_HISTORY_CONTACT_BY_ID,
                    inc: path.resolve(__dirname, '../views/trial_program_history_contact/update_trial_program_history_contact.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            trial_program_history_contactID
                        } = req.query;

                        let infoTrial_program_history_contact = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getInfoById(trial_program_history_contactID);
                        if (infoTrial_program_history_contact.error) {
                            return res.redirect('/something-went-wrong');
                        }


                        let listUser_register_trial_programs = await USER_REGISTER_TRIAL_PROGRAM_COLL
                            .find({
                                state: 1,
                                status: 1
                            })
                            .sort({
                                modifyAt: -1,
                                createAt: -1,
                                _id: -1
                            })
                            .lean();


                        ChildRouter.renderToView(req, res, {
                            infoTrial_program_history_contact: infoTrial_program_history_contact.data || {},

                            listUser_register_trial_programs,
                            CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT
                        });
                    }],
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            trial_program_history_contactID,
                            status,
                            reminderTime,
                            note,
                            user_register_trial_program,
                        } = req.body;


                        const infoAfterUpdate = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.update({
                            trial_program_history_contactID,
                            status,
                            reminderTime,
                            note,
                            user_register_trial_program,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Update not require Trial_program_history_contact By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.UPDATE_TRIAL_PROGRAM_HISTORY_CONTACT_NOT_REQUIRE_BY_ID]: {
                config: {
                    scopes: ['update:trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            trial_program_history_contactID,
                            status,
                            reminderTime,
                            note,
                            user_register_trial_program,
                        } = req.body;


                        const infoAfterUpdate = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.updateNotRequire({
                            trial_program_history_contactID,
                            status,
                            reminderTime,
                            note,
                            user_register_trial_program,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete Trial_program_history_contact By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.DELETE_TRIAL_PROGRAM_HISTORY_CONTACT_BY_ID]: {
                config: {
                    scopes: ['delete:trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const {
                            trial_program_history_contactID
                        } = req.params;

                        const infoAfterDelete = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.deleteById(trial_program_history_contactID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Delete Trial_program_history_contact By List Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.DELETE_TRIAL_PROGRAM_HISTORY_CONTACT_BY_LIST_ID]: {
                config: {
                    scopes: ['delete:trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            trial_program_history_contactID
                        } = req.body;

                        const infoAfterDelete = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.deleteByListId(trial_program_history_contactID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Get Info Trial_program_history_contact By Id (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.GET_INFO_TRIAL_PROGRAM_HISTORY_CONTACT_BY_ID]: {
                config: {
                    scopes: ['read:info_trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const {
                            trial_program_history_contactID
                        } = req.params;

                        const infoTrial_program_history_contactById = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getInfoById(trial_program_history_contactID);
                        res.json(infoTrial_program_history_contactById);
                    }]
                },
            },

            /**
             * Function: Get List Trial_program_history_contact (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACT]: {
                config: {
                    scopes: ['read:list_trial_program_history_contact'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Trial_program_history_contact',
                    code: CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACT,
                    inc: path.resolve(__dirname, '../views/trial_program_history_contact/list_trial_program_history_contacts.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            status,
                            reminderTimeDateRange,
                            keyword,
                            typeGetList
                        } = req.query;

                        let listTrial_program_history_contacts = [];
                        if (typeGetList === 'FILTER') {
                            listTrial_program_history_contacts = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getListByFilter({
                                status,
                                reminderTimeDateRange,
                                keyword,
                            });
                        } else {
                            listTrial_program_history_contacts = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getList();
                        }

                        ChildRouter.renderToView(req, res, {
                            listTrial_program_history_contacts: listTrial_program_history_contacts.data || [],
                            STATUS_TRIAL_PROGRAM_HISTORY_CONTACT_TYPE,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Trial_program_history_contact By Field (API, VIEW)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACT_BY_FIELD]: {
                config: {
                    scopes: ['read:list_trial_program_history_contact'],
                    type: 'view',
                    view: 'index.ejs',
                    title: 'Danh sách Trial_program_history_contact by field isStatus',
                    code: CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACT_BY_FIELD,
                    inc: path.resolve(__dirname, '../views/trial_program_history_contact/list_trial_program_history_contacts.ejs')
                },
                methods: {
                    get: [async function(req, res) {
                        let {
                            field,
                            value
                        } = req.params;
                        let {
                            status,
                            reminderTimeDateRange,
                            keyword,
                            type
                        } = req.query;

                        let listTrial_program_history_contacts = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getListByFilter({
                            status,
                            reminderTimeDateRange,
                            keyword,
                            [field]: value,
                        });

                        ChildRouter.renderToView(req, res, {
                            listTrial_program_history_contacts: listTrial_program_history_contacts.data || [],
                            STATUS_TRIAL_PROGRAM_HISTORY_CONTACT_TYPE,
                            [field]: value,
                        });
                    }]
                },
            },

            /**
             * Function: Get List Trial_program_history_contact Server Side (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACT_SERVER_SIDE]: {
                config: {
                    scopes: ['read:list_trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length,
                            order
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        const listTrial_program_history_contactServerSide = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getListByFilterServerSide({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length,
                            field,
                            dir
                        });
                        res.json(listTrial_program_history_contactServerSide);
                    }]
                },
            },

            /**
             * Function: Get List Trial_program_history_contact Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACT_IMPORT]: {
                config: {
                    scopes: ['read:list_trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            start,
                            length
                        } = req.body;
                        const page = Number(start) / Number(length) + 1;

                        const listTrial_program_history_contactImport = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getListByFilterImport({
                            keyword,
                            filter,
                            condition,
                            objFilterStatic,
                            page,
                            limit: length
                        });
                        res.json(listTrial_program_history_contactImport);
                    }]
                },
            },

            /**
             * Function: Get List Trial_program_history_contact Excel Server Side (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACT_EXCEL]: {
                config: {
                    scopes: ['read:list_trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl,
                        } = req.body;

                        let conditionObj = TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getConditionArrayFilterExcel(listItemExport)

                        let historyExportColl = await MANAGE_COLL_MODEL.insertHistoryExport({
                            coll: conditionObj.refParent,
                            list_type_coll: conditionObj.arrayFieldIDChoice,
                            listItemExport,
                            chooseCSV,
                            nameOfParentColl
                        })

                        res.json(historyExportColl)
                    }]
                },
            },

            /**
             * Function: Download Trial_program_history_contact Excel Export (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.DOWNLOAD_LIST_TRIAL_PROGRAM_HISTORY_CONTACT_EXCEL_EXPORT]: {
                config: {
                    scopes: ['read:list_trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let {
                            filter,
                            condition,
                            objFilterStatic,
                            order,
                            keyword,
                            arrayItemChecked
                        } = req.body;

                        let field, dir;
                        if (order && order.length) {
                            field = req.body.columns[order[0].column].data;
                            dir = order[0].dir;
                        }

                        let {
                            listCollChoice: {
                                listItemExport,
                                chooseCSV,
                                nameOfParentColl
                            }
                        } = await MANAGE_COLL_MODEL.getInfo({
                            name: 'trial_program_history_contact'
                        });

                        let conditionObj = TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked)
                        let listTrial_program_history_contact = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getListByFilterExcel({
                            arrayFilter: conditionObj.arrayFilter,
                            arrayItemCustomerChoice: conditionObj.arrayItemCustomerChoice,
                            chooseCSV,
                            nameOfParentColl
                        });

                        res.json(listTrial_program_history_contact)
                    }]
                },
            },

            /**
             * Function: Setting Trial_program_history_contact Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.SETTING_FILE_TRIAL_PROGRAM_HISTORY_CONTACT_EXCEL_IMPORT_PREVIEW]: {
                config: {
                    scopes: ['read:list_trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        const {
                            listItemImport,
                            condition
                        } = req.body;

                        let conditionObj = TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getConditionArrayFilterExcel(listItemImport);

                        let historyImportColl = await MANAGE_COLL_MODEL.insertHistoryImport({
                            coll: conditionObj.refParent,
                            arrayFieldChoice: conditionObj.arrayItemCustomerChoice,
                            listItemImport,
                            condition
                        });
                        res.json(historyImportColl)
                    }]
                },
            },

            /**
             * Function: Download Trial_program_history_contact Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.DOWNLOAD_FILE_TRIAL_PROGRAM_HISTORY_CONTACT_EXCEL_IMPORT]: {
                config: {
                    scopes: ['read:list_trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'trial_program_history_contact'
                        });

                        let listTrial_program_history_contactImport = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.fileImportExcelPreview({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice
                        });

                        res.download(listTrial_program_history_contactImport.pathWriteFile, function(err) {
                            if (err) {
                                console.log(err);
                            } else {
                                // Remove file on server
                                fs.unlinkSync(listTrial_program_history_contactImport.pathWriteFile);
                            }
                        });
                    }]
                },
            },

            /**
             * Function: Upload Trial_program_history_contact Excel Import (API)
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.CREATE_TRIAL_PROGRAM_HISTORY_CONTACT_IMPORT_EXCEL]: {
                config: {
                    scopes: ['create:trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    post: [uploadSingle, async function(req, res) {

                        let listFieldHistoryImportColl = await MANAGE_COLL_MODEL.getInfoImport({
                            name: 'trial_program_history_contact'
                        });

                        let infoTrial_program_history_contactAfterImport = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.importExcel({
                            arrayItemCustomerChoice: listFieldHistoryImportColl.listCollChoice,
                            file: req.file,
                            nameCollParent: 'trial_program_history_contact',
                        });

                        res.json(infoTrial_program_history_contactAfterImport);
                    }]
                },
            },

            /**
             * Function: API Get info Trial_program_history_contact
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.API_GET_INFO_TRIAL_PROGRAM_HISTORY_CONTACT_OF_ME]: {
                config: {
                    scopes: ['read:info_trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {
                            trial_program_history_contactID
                        } = req.params;
                        const {
                            select,
                            filter,
                            explain
                        } = req.query;

                        const infoTrial_program_history_contact = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getInfoTrial_program_history_contact({
                            trial_program_history_contactID,
                            select,
                            filter,
                            explain,
                            authorID
                        });
                        res.json(infoTrial_program_history_contact);
                    }]
                },
            },

            /**
             * Function: API Get list Trial_program_history_contact
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.API_GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACTS_OF_ME]: {
                config: {
                    scopes: ['public'],
                    type: 'json',
                },
                methods: {
                    get: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page
                        } = req.query;

                        const listTrial_program_history_contacts = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getListTrial_program_history_contacts({
                            select,
                            filter,
                            explain,
                            sort,
                            search,
                            limit,
                            page,
                            authorID
                        });
                        res.json(listTrial_program_history_contacts);
                    }]
                },
            },

            /**
             * Function: API Insert Trial_program_history_contact
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.API_ADD_TRIAL_PROGRAM_HISTORY_CONTACT_OF_ME]: {
                config: {
                    scopes: ['create:trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    post: [async function(req, res) {
                        let userCreate = req.user && req.user._id;
                        let {
                            status,
                            reminderTime,
                            note,
                            phone,
                            user_register_trial_program,
                        } = req.body;


                        let infoAfterInsert = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.insertTrial_program_history_contact({
                            status,
                            reminderTime,
                            note,
                            phone,
                            user_register_trial_program,
                            userCreate
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: API Update Trial_program_history_contact
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.API_UPDATE_TRIAL_PROGRAM_HISTORY_CONTACT_OF_ME]: {
                config: {
                    scopes: ['update:trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    put: [async function(req, res) {
                        let userUpdate = req.user && req.user._id;
                        let {
                            trial_program_history_contactID
                        } = req.params;
                        let {
                            status,
                            reminderTime,
                            note,
                            trialProgram,
                            phone,
                            user_register_trial_program,
                        } = req.body;


                        let infoAfterUpdate = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.updateTrial_program_history_contact({
                            trial_program_history_contactID,
                            status,
                            reminderTime,
                            note,
                            trialProgram,
                            phone,
                            user_register_trial_program,
                            userUpdate
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: API Delete Trial_program_history_contact
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.API_DELETE_TRIAL_PROGRAM_HISTORY_CONTACT_OF_ME]: {
                config: {
                    scopes: ['delete:trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {
                            trial_program_history_contactID
                        } = req.params;

                        const infoAfterDelete = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.deleteTrial_program_history_contact(trial_program_history_contactID, authorID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: API Delete Trial_program_history_contact
             * Date: 12/05/2022
             * Dev: Automatic
             */
            [CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT.API_DELETE_TRIAL_PROGRAM_HISTORY_CONTACTS_OF_ME]: {
                config: {
                    scopes: ['delete:trial_program_history_contact'],
                    type: 'json',
                },
                methods: {
                    delete: [async function(req, res) {
                        const authorID = req.user && req.user._id;
                        const {
                            trial_program_history_contactsID
                        } = req.params;

                        const infoAfterDelete = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.deleteTrial_program_history_contacts(trial_program_history_contactsID, authorID);
                        res.json(infoAfterDelete);
                    }]
                },
            },

        }
    }
};