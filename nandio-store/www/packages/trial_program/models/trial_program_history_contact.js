/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
moment.locale('vi')
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const path = require('path');
const fs = require('fs');
const {
    hash,
    hashSync,
    compare
} = require('bcryptjs');
const XlsxPopulate = require('xlsx-populate');
const mongodb = require("mongodb");
const {
    MongoClient
} = mongodb;


/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    renderOptionFilter,
    colName,
    checkNumberIsValidWithRange,
    checkPhoneNumber
} = require('../../../utils/utils');
const {
    isTrue
} = require('../../../tools/module/check');
const {
    randomStringFixLength
} = require('../../../utils/string_utils');
const {
    checkDateValid
} = require('../../../utils/time_utils');
const {
    STATUS_TRIAL_PROGRAM_HISTORY_CONTACT_TYPE,
} = require('../constants/trial_program_history_contact');


/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const URL_DATABASE = process.env.URL_DATABASE || 'mongodb://localhost:27017';
const NAME_DATABASE = process.env.NAME_DATABASE || 'ldk_tools_op';
const request                      	= require('request');
const NANDIO_SHOP_DOMAIN            = process.env.NANDIO_SHOP_DOMAIN || 'http://localhost:5002';


class Model extends BaseModel {
    constructor() {
        super(require('../databases/trial_program_history_contact-coll'));
        this.STATUS_SETUP_MEETING_SUCCESS = 2; //Liên Hệ Đặt Lịch Thành Công
        this.STATUS_HISTORY_CONTACT_ORDER_DELIVERY_SUCCESS = 5; //đổi quà thành công (NA --giao hàng--> Customer)
        this.STATUS_HISTORY_CONTACT_ORDER_CANCEL = 3; //đơn hàng đổi quà
    }

    // call API QSHOP push noti khi xuất đơn ESAMPLING
    _callAPIQSHOP_Push_Noti_When_Export_ESAMPLING_QSHOP({ 
        userRegisterTrialProgramID,
        description
    }) {
        return new Promise(async resolve => {
            try {
                var options = {
                    'method': 'POST',
                    'url': `${NANDIO_SHOP_DOMAIN}/push-notification-fcm`,
                    'headers': {
                        'Content-Type': 'application/x-www-form-urlencoded',
						'user-agent': 'PostmanRuntime/7.28.4',
						'postman_test_sandbox': 'ldk_postman',
                    },
                    form: {
                        screen_key: 'DetailTrialScreen',
                        description,
                        userRegisterTrialProgramID,
                    }
                };
             
                request(options, function (error, response) {
                    if (error) return resolve({ error: true, message: 'cannot_call_push_noti_export_qshop' });
                    return resolve(JSON.parse(response.body));
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    // call API QSHOP khi đơn hàng có trạng thái cuối là huỷ đơn hàng
    _callAPIQShopUpdateOrderCancel({ 
        userRegisterTrialProgramID,
    }) {
        return new Promise(async resolve => {
            try {
                //TODO CALL api qshop để cập nhật
                var options = {
                    'method': 'POST',
                    'url': `${NANDIO_SHOP_DOMAIN}/update-centralize-order-when-order-trial-program-cancel`,
                    'headers': {
                        'Content-Type': 'application/x-www-form-urlencoded',
						'user-agent': 'PostmanRuntime/7.28.4',
						'postman_test_sandbox': 'ldk_postman',
                    },
                    form: {
                        user_register_trial_programID: userRegisterTrialProgramID
                    }
                };
             
                request(options, function (error, response) {
                    if (error) return resolve({ error: true, message: 'cannot_call_push_noti_export_qshop' });
                    return resolve(JSON.parse(response.body));
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
         * Tạo mới trial_program_history_contact
		* @param {number} status
		* @param {date} reminderTime
		* @param {string} note
		* @param {object} user_register_trial_program

         * @param {objectId} userCreate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        status,
        reminderTime,
        note,
        user_register_trial_program,
        userCreate,
        /**
         * thông tin SẢN PHẨM: đơn hàng xuất có format như sau:
         *  { product: '', amount: 123 } // vì xuất thành công chỉ lấy các sản phẩm đủ
         */
        dataOrderExport, 
    }) {
        return new Promise(async resolve => {
            let that = this;
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (note.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài note không được lớn hơn 125 ký tự'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2, 3, 4, 5],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trạng thái không hợp lệ'
                    });
                }

                if (user_register_trial_program && !checkObjectIDs([user_register_trial_program])) {
                    return resolve({
                        error: true,
                        message: 'user_register_trial_program không hợp lệ'
                    });
                }

                let dataInsert = {
                    status,
                    note,
                    user_register_trial_program,
                    userCreate
                };

                  /**
                 * ======================== KHU VỰC CẬP_NHẬT_TRẠNG_THÁI QUA QSHOP (call API qua QSHOP) ==============
                 */
                 /**
                 * chỉ đối với đơn [3: huỷ dơn hàng] -> cần cập nhật centralizeOrder update trạng thái đơn bên QSHOP thành thất bại
                 */
                console.log({
                    ['that.STATUS_HISTORY_CONTACT_ORDER_CANCEL']: that.STATUS_HISTORY_CONTACT_ORDER_CANCEL,
                    status
                })
                  if (+status == that.STATUS_HISTORY_CONTACT_ORDER_CANCEL) {
                    let infoAfterUpdateCentralizeOrderCancel = await that._callAPIQShopUpdateOrderCancel({
                        userRegisterTrialProgramID: user_register_trial_program,
                    });
                    console.log({ infoAfterUpdateCentralizeOrderCancel })
                }

                /**
                 * chỉ đối với đơn [2: Liên Hệ Đặt Lịch Thành Công]-> mới có cập nhật reminderTime
                 */
                if (+status == this.STATUS_SETUP_MEETING_SUCCESS) {
                    if (!checkDateValid(new Date(reminderTime)))
                        return resolve({ error: true, message: 'Vui lòng nhập Thời Gian Đặt Lịch chính xác' })
                    dataInsert.reminderTime = new Date(reminderTime);


                    
                    let infoAfterCallPushNotificationESampling = await that._callAPIQSHOP_Push_Noti_When_Export_ESAMPLING_QSHOP({
                        description: `${moment(reminderTime).format('LT')} ${moment(reminderTime).format('L')} - vui lòng có mặt tại cửa hàng`,
                        userRegisterTrialProgramID: user_register_trial_program,
                    });
                    // console.log({ infoAfterCallPushNotificationESampling })
                }

                /**
                 * đối với đơn hàng [5: Xuất đơn thành công] -> cần lưu các thông tin tại thời điểm xuất
                 * Mục đích: dùng để lưu trữ thông tin sau để hiển thị chính xác cho các đơn hàng đã xuất
                 */
                if (+status == this.STATUS_HISTORY_CONTACT_ORDER_DELIVERY_SUCCESS) {
                    dataInsert = {
                        ...dataInsert,
                        dataOrderExport
                    }
                }

                 /**
                 * ========================(END) KHU VỰC CẬP_NHẬT_TRẠNG_THÁI QUA QSHOP (call API qua QSHOP) ==============
                 */

                if (user_register_trial_program) {
                    const user_register_trial_programInfo = await USER_REGISTER_TRIAL_PROGRAM_COLL.findById(user_register_trial_program);
                    dataInsert.trialProgram = user_register_trial_programInfo.trialProgram;
                    dataInsert.phone = user_register_trial_programInfo.phone;
                }

                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo Lịch sử các lần NA call cho Customer thất bại'
                    });
                }

                let infoOrderAfterUpdate = await USER_REGISTER_TRIAL_PROGRAM_COLL.findByIdAndUpdate(user_register_trial_program, {
                    lastest_trial_program_history_contact: infoAfterInsert._id,
                    lastest_status_contact: status,
                    modifyAt: new Date()
                }, { new: true });
                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật trial_program_history_contact 
         * @param {objectId} trial_program_history_contactID
		* @param {number} status
		* @param {date} reminderTime
		* @param {string} note
		* @param {object} user_register_trial_program

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        trial_program_history_contactID,
        status,
        reminderTime,
        note,
        user_register_trial_program,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([trial_program_history_contactID])) {
                    return resolve({
                        error: true,
                        message: 'trial_program_history_contactID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (note.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài note không được lớn hơn 125 ký tự'
                    });
                }


                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2, 3, 4, 5],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trạng thái không hợp lệ'
                    });
                }




                if (user_register_trial_program && !checkObjectIDs([user_register_trial_program])) {
                    return resolve({
                        error: true,
                        message: 'user_register_trial_program không hợp lệ'
                    });
                }

                const checkExists = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.findById(trial_program_history_contactID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'Lịch sử các lần NA call cho Customer không tồn tại'
                    });
                }


                let dataUpdate = {
                    userUpdate
                };
                dataUpdate.status = status;
                reminderTime && (dataUpdate.reminderTime = new Date(reminderTime));
                dataUpdate.note = note;
                user_register_trial_program && (dataUpdate.user_register_trial_program = user_register_trial_program);

                if (user_register_trial_program) {
                    const user_register_trial_programInfo = await USER_REGISTER_TRIAL_PROGRAM_COLL.findById(user_register_trial_program);
                    dataUpdate.trialProgram = user_register_trial_programInfo.trialProgram;
                    dataUpdate.phone = user_register_trial_programInfo.phone;
                }

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: trial_program_history_contactID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật trial_program_history_contact (không bắt buộc)
         * @param {objectId} trial_program_history_contactID
		* @param {number} status
		* @param {date} reminderTime
		* @param {string} note
		* @param {object} user_register_trial_program

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        trial_program_history_contactID,
        status,
        reminderTime,
        note,
        user_register_trial_program,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([trial_program_history_contactID])) {
                    return resolve({
                        error: true,
                        message: 'trial_program_history_contactID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 2, 3, 4, 5],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'trạng thái không hợp lệ'
                    });
                }

                if (user_register_trial_program && !checkObjectIDs([user_register_trial_program])) {
                    return resolve({
                        error: true,
                        message: 'user_register_trial_program không hợp lệ'
                    });
                }

                const checkExists = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.findById(trial_program_history_contactID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'Lịch sử các lần NA call cho Customer không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                status && (dataUpdate.status = status);
                reminderTime && (dataUpdate.reminderTime = new Date(reminderTime));
                note && (dataUpdate.note = note);
                user_register_trial_program && (dataUpdate.user_register_trial_program = user_register_trial_program);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: trial_program_history_contactID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa trial_program_history_contact 
     * @param {objectId} trial_program_history_contactID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(trial_program_history_contactID) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 2
                };
                if (!checkObjectIDs([trial_program_history_contactID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị trial_program_history_contactID không hợp lệ'
                    });
                }


                const infoAfterDelete = await this.updateById(trial_program_history_contactID, conditionObj);

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa trial_program_history_contact 
     * @param {array} trial_program_history_contactID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(trial_program_history_contactID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(trial_program_history_contactID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị trial_program_history_contactID không hợp lệ'
                    });
                }

                const infoAfterDelete = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.deleteMany({
                    _id: {
                        $in: trial_program_history_contactID
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin trial_program_history_contact 
     * @param {objectId} trial_program_history_contactID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(trial_program_history_contactID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([trial_program_history_contactID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị trial_program_history_contactID không hợp lệ'
                    });
                }

                const infoTrial_program_history_contact = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.findById(trial_program_history_contactID)
                    .populate('trialProgram user_register_trial_program')

                if (!infoTrial_program_history_contact) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin Lịch sử các lần NA call cho Customer'
                    });
                }

                return resolve({
                    error: false,
                    data: infoTrial_program_history_contact
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách trial_program_history_contact 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listTrial_program_history_contact = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL
                    .find({
                        state: 1
                    }).populate('trialProgram user_register_trial_program')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listTrial_program_history_contact) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách Lịch sử các lần NA call cho Customer'
                    });
                }

                return resolve({
                    error: false,
                    data: listTrial_program_history_contact
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    getLatestTrialProgramHistoryContact({ user_register_trial_program }) {
        return new Promise(async resolve => {
            try {
                const listTrial_program_history_contact = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL
                    .find({
                        state: 1,
                        //status: this.STATUS_SETUP_MEETING_SUCCESS,
                        user_register_trial_program: user_register_trial_program
                    })
                    .sort({
                        _id: -1
                    })
                    .lean();
                    
                if (!listTrial_program_history_contact) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy Lịch sử cuối cùng các lần NA call cho Customer'
                    });
                }

                return resolve({
                    error: false,
                    data: listTrial_program_history_contact && listTrial_program_history_contact[0]
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    // time format: yyyy-mm-dd
    getListBookingSuccess(time) {
        return new Promise(async resolve => {
            try {
                let timeCondition = time ? time : moment(Date.now()).format('L');

                let _fromDate = moment(new Date(timeCondition)).startOf('day').format();
                let _toDate = moment(new Date(timeCondition)).endOf('day').format();

                let condition = {
                    status: this.STATUS_SETUP_MEETING_SUCCESS,
                    modifyAt: { 
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                };

                const listTrial_program_history_contact = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.aggregate([
                    {
                        $sort: { 
                            modifyAt: -1,
                        }
                    } , { 
                        $match: condition
                    } , {
                        $group: {
                            _id: '$user_register_trial_program',
                            trial_program_history_contact_latest: { 
                                $first: '$$ROOT' 
                            },
                        }
                    } , {
                        $lookup: {
                            from: 'user_register_trial_programs',
                            localField: 'trial_program_history_contact_latest.user_register_trial_program',
                            foreignField: '_id',
                            as: 'user_register_trial_program'
                        }
                    }, {
                        $unwind: '$user_register_trial_program'
                    }
                ])

                if (!listTrial_program_history_contact) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách Lịch sử các lần NA call cho Customer'
                    });
                }

                return resolve({
                    error: false,
                    data: listTrial_program_history_contact
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách trial_program_history_contact theo bộ lọc
		* @enum {number} status
		* @param {date} reminderTimeDateRange
		* @param {string} keyword

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        status,
        reminderTimeDateRange,
        keyword,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        note: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        phone: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                status && (conditionObj.status = status);

                if (reminderTimeDateRange) {
                    let [fromDate, toDate] = reminderTimeDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.reminderTime = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }


                const listTrial_program_history_contactByFilter = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL
                    .find(conditionObj).populate('trialProgram user_register_trial_program')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listTrial_program_history_contactByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách Lịch sử các lần NA call cho Customer"
                    });
                }

                return resolve({
                    error: false,
                    data: listTrial_program_history_contactByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách trial_program_history_contact theo bộ lọc (server side)
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        note: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        phone: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }



                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trạng thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }


                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalTrial_program_history_contact = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.countDocuments(conditionObj);

                const listTrial_program_history_contactByFilter = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'trial_programs',
                            localField: 'trialProgram',
                            foreignField: '_id',
                            as: 'trialProgram'
                        }
                    },
                    {
                        $unwind: {
                            path: '$trialProgram',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'user_register_trial_programs',
                            localField: 'user_register_trial_program',
                            foreignField: '_id',
                            as: 'user_register_trial_program'
                        }
                    },
                    {
                        $unwind: {
                            path: '$user_register_trial_program',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listTrial_program_history_contactByFilter) {
                    return resolve({
                        recordsTotal: totalTrial_program_history_contact,
                        recordsFiltered: totalTrial_program_history_contact,
                        data: []
                    });
                }

                const listTrial_program_history_contactDataTable = listTrial_program_history_contactByFilter.map((trial_program_history_contact, index) => {

                    let status = '';
                    if (trial_program_history_contact.status == 1) {
                        status = 'checked';
                    }

                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${trial_program_history_contact._id}" type="checkbox" class="check-record check-record-${trial_program_history_contact._id}" _index ="${index + 1}"><label for="${trial_program_history_contact._id}"></label></div></td>`,
                        indexSTT: skip + index + 1,
                        status: ` <td> <div class="form-check form-switch form-switch-success"><input class="form-check-input check-status" _trial_program_history_contactID="${trial_program_history_contact._id}" type="checkbox" id="${trial_program_history_contact._id}" ${status} style="width: 40px;height: 20px;"></div></td>`,
                        reminderTime: `${trial_program_history_contact.reminderTime && trial_program_history_contact.reminderTime.length > 50 ? trial_program_history_contact.reminderTime.substr(0,50) + "..." : trial_program_history_contact.reminderTime}`,
                        trialProgram: trial_program_history_contact.trialProgram ?
                            trial_program_history_contact.trialProgram.title :
                            '<span class="badge bg-danger"> <span class="badge bg-danger"> không tồn tại hoặc đã xoá </span> </span>',
                        phone: `<b>${trial_program_history_contact.phone && trial_program_history_contact.phone.length > 50 ? trial_program_history_contact.phone.substr(0,50) + "..." : trial_program_history_contact.phone}</b>`,

                        createAt: moment(trial_program_history_contact.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalTrial_program_history_contact,
                    recordsFiltered: totalTrial_program_history_contact,
                    data: listTrial_program_history_contactDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách trial_program_history_contact theo bộ lọc import
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterImport({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        note: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        phone: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }


                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trạng thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }


                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalTrial_program_history_contact = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.countDocuments(conditionObj);

                const listTrial_program_history_contactByFilter = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'trial_programs',
                            localField: 'trialProgram',
                            foreignField: '_id',
                            as: 'trialProgram'
                        }
                    },
                    {
                        $unwind: {
                            path: '$trialProgram',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'user_register_trial_programs',
                            localField: 'user_register_trial_program',
                            foreignField: '_id',
                            as: 'user_register_trial_program'
                        }
                    },
                    {
                        $unwind: {
                            path: '$user_register_trial_program',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listTrial_program_history_contactByFilter) {
                    return resolve({
                        recordsTotal: totalTrial_program_history_contact,
                        recordsFiltered: totalTrial_program_history_contact,
                        data: []
                    });
                }

                return resolve({
                    error: false,
                    recordsTotal: totalTrial_program_history_contact,
                    recordsFiltered: totalTrial_program_history_contact,
                    data: listTrial_program_history_contactByFilter || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc trial_program_history_contact
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc trial_program_history_contact
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listTrial_program_history_contactByFilter = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.aggregate(arrayFilter)

                if (!listTrial_program_history_contactByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listTrial_program_history_contactByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listTrial_program_history_contactByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('HH:mm DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Tải file import excel mẫu trial_program_history_contact
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    fileImportExcelPreview({
        arrayItemCustomerChoice
    }) {
        return new Promise(async resolve => {
            try {
                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition; // BỘ LỌC && LOẠI IMPORT
                let {
                    listFieldPrimaryKey
                } = condition; // DANH SÁCH PRIMARY KEY

                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        let index = 0;
                        arrayItemCustomerChoice.map((elem) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            if (elem.dataDynamic && elem.dataDynamic.length) {
                                listFieldPrimaryKey = listFieldPrimaryKey.filter(key => key != elem.nameFieldRef); // LỌC PRIMARY KEY MÀ KHÔNG PHẢI REF

                                elem.dataDynamic.map(item => {
                                    workbook.sheet("Report").row(1).cell(index + 1).value(item);
                                    index++;
                                })
                            } else {
                                workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                                index++;
                            }
                        });

                        if (isTrue(condition.checkDownloadDataOld)) { // KIỂM TRA CÓ ĐÍNH ĐÈM DỮ LIỆU CŨ THEO ĐIỀU KIỆN
                            let listItemImport = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].listItemImport; // LIST FIELD ĐÃ ĐƯỢC CẤU HÌNH
                            let {
                                arrayFilter
                            } = this.getConditionArrayFilterExcel(listItemImport, condition.conditionDeleteImport.filter, condition.conditionDeleteImport.condition); // LẤY RA ARRAY ARREGATE

                            let groupByTrial_program_history_contact = {};
                            listFieldPrimaryKey.map(key => {
                                groupByTrial_program_history_contact = {
                                    ...groupByTrial_program_history_contact,
                                    [key]: '$' + key
                                }
                            });

                            arrayFilter = [
                                ...arrayFilter,
                                {
                                    $group: {
                                        _id: {
                                            groupByTrial_program_history_contact
                                        },
                                        listData: {
                                            $addToSet: "$$CURRENT"
                                        },
                                    }
                                }
                            ];

                            const listTrial_program_history_contactByFilter = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.aggregate(arrayFilter);

                            listTrial_program_history_contactByFilter && listTrial_program_history_contactByFilter.length && listTrial_program_history_contactByFilter.map((item, indexTrial_program_history_contact) => {
                                let indexValue = 0;
                                arrayItemCustomerChoice.map((elem, indexChoice) => {
                                    let variable = elem.name.split('.');

                                    if (elem.dataDynamic && elem.dataDynamic.length) { // KIỂM TRA FIELD CÓ CHỌN DYNAMIC
                                        item.listData && item.listData.length && item.listData.map(value => { // ARRAY DATA DYNAMIC
                                            if (item.listData.length > elem.dataDynamic.length) { // KIỂM TRA ĐỘ DÀI CỦA DATA SO VỚI SỐ CỘT
                                                // TODO: XỬ LÝ NHIỀU DYNAMIC

                                            } else {
                                                elem.dataDynamic.map(dynamic => {
                                                    let valueOfField;
                                                    if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                        let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                        if (objDataOfVariable) {
                                                            valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                        }
                                                    } else {
                                                        valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                                    }

                                                    if (valueOfField == dynamic) { // CHECK NẾU VALUE === CỘT
                                                        let valueImportDynamic = value[elem.variableChoice] ? value[elem.variableChoice] : '';

                                                        // INSERT DỮ LIỆU VÀO BẢNG VỚI FIELD ĐƯỢC CHỌN THEO DẠNG DYNAMIC
                                                        workbook.sheet("Report").row(indexTrial_program_history_contact + 2).cell(indexValue + 1).value(valueImportDynamic);
                                                        indexValue++;
                                                    } else {
                                                        if (elem.dataDynamic && item.listData && elem.dataDynamic.length > item.listData.length) {
                                                            indexValue++;
                                                        }
                                                    }
                                                })
                                            }
                                        })
                                    } else { // DẠNG STATIC
                                        let valueTrial_program_history_contact;
                                        if (variable.length > 1) {
                                            let objDataOfVariable = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                            if (objDataOfVariable) {
                                                valueTrial_program_history_contact = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                            }
                                        } else {
                                            valueTrial_program_history_contact = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                        }

                                        workbook.sheet("Report").row(indexTrial_program_history_contact + 2).cell(indexValue + 1).value(valueTrial_program_history_contact);
                                        indexValue++;
                                    }
                                });
                            });
                        }

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    pathWriteFile,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Upload File Excel Import Lưu Dữ Liệu trial_program_history_contact
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    importExcel({
        nameCollParent,
        arrayItemCustomerChoice,
        file,
    }) {
        return new Promise(async resolve => {
            try {

                XlsxPopulate.fromFileAsync(file.path)
                    .then(async workbook => {
                        let listData = [];
                        let index = 2;
                        let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition;

                        const client = await MongoClient.connect(URL_DATABASE);
                        const db = client.db(NAME_DATABASE)

                        for (; true;) {
                            if (arrayItemCustomerChoice && arrayItemCustomerChoice.length) {
                                let conditionObj = {};

                                let totalLength = 0;
                                arrayItemCustomerChoice.map((item, index) => {
                                    if (item.dataDynamic && item.dataDynamic.length) {
                                        totalLength += item.dataDynamic.length;
                                    } else {
                                        totalLength++;
                                    }
                                });

                                let indexOfListField = 0;
                                let checkIsRequire = false;
                                let arrayConditionObjDynamic = [];

                                for (let i = 0; i < totalLength; i++) {
                                    if (arrayItemCustomerChoice[indexOfListField].dataDynamic && arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                        let indexOfDynamic = 1;
                                        for (let valueDynamic of arrayItemCustomerChoice[indexOfListField].dataDynamic) {
                                            let letter = colName(i);
                                            let indexOfCeil = letter.toUpperCase() + index;

                                            let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                            if (variable) {

                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: valueDynamic.trim()
                                                });

                                                let conditionOfOneValueDynamic = {
                                                    [arrayItemCustomerChoice[indexOfListField].variableChoice]: variable
                                                }
                                                if (docs) {
                                                    conditionOfOneValueDynamic = {
                                                        ...conditionOfOneValueDynamic,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    }
                                                }

                                                arrayConditionObjDynamic = [
                                                    ...arrayConditionObjDynamic,
                                                    conditionOfOneValueDynamic
                                                ];
                                            }

                                            if (indexOfDynamic < arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                                i++;
                                            }
                                            indexOfDynamic++;
                                        }
                                    } else {
                                        let letter = colName(i);
                                        let indexOfCeil = letter.toUpperCase() + index;
                                        let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                        if (arrayItemCustomerChoice[indexOfListField].isRequire && !variable) {
                                            checkIsRequire = true;
                                            break;
                                        }
                                        if (arrayItemCustomerChoice[indexOfListField].ref && arrayItemCustomerChoice[indexOfListField].ref != nameCollParent) {
                                            if (arrayItemCustomerChoice[indexOfListField].isRequire) {
                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: variable.trim()
                                                })
                                                // .sort({
                                                //     _id: -1
                                                // })
                                                // .limit(100)
                                                // .toArray();
                                                if (docs) {
                                                    conditionObj = {
                                                        ...conditionObj,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                    };

                                                    if (arrayItemCustomerChoice[indexOfListField].mappingRef && arrayItemCustomerChoice[indexOfListField].mappingRef.length) {
                                                        arrayItemCustomerChoice[indexOfListField].mappingRef.map(mapping => {
                                                            conditionObj = {
                                                                ...conditionObj,
                                                                [mapping]: docs[mapping]
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                [arrayItemCustomerChoice[indexOfListField].name]: variable
                                            };
                                        }
                                    }

                                    indexOfListField++;
                                }
                                if (checkIsRequire) {
                                    break;
                                }

                                conditionObj = {
                                    ...conditionObj,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                }

                                let arrayCondditionObj = []
                                if (arrayConditionObjDynamic && arrayConditionObjDynamic.length) {
                                    arrayConditionObjDynamic.map(item => {
                                        arrayCondditionObj = [
                                            ...arrayCondditionObj,
                                            {
                                                ...conditionObj,
                                                ...item
                                            }
                                        ]
                                    });
                                } else {
                                    arrayCondditionObj = [
                                        ...arrayCondditionObj,
                                        conditionObj
                                    ]
                                }
                                listData = [
                                    ...listData,
                                    ...arrayCondditionObj
                                ];

                                index++;
                            }
                        }

                        await fs.unlinkSync(file.path);

                        if (listData.length) {
                            await this.changeDataImport({
                                condition,
                                listTrial_program_history_contact: listData
                            });
                        } else {
                            return resolve({
                                error: true,
                                message: 'Import thất bại'
                            });
                        }

                        return resolve({
                            error: false,
                            message: 'Import thành công'
                        });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lưu Dữ Liệu Theo Lựa Chọn trial_program_history_contact
     * @param {object} listTrial_program_history_contact
     * @param {object} condition
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    changeDataImport({
        condition,
        listTrial_program_history_contact
    }) {
        return new Promise(async resolve => {
            try {
                if (isTrue(condition.delete)) { // XÓA DATA CŨ
                    if (isTrue(condition.deleteAll)) { // XÓA TẤT CẢ DỮ LIỆU
                        console.log("====================XÓA TẤT CẢ DỮ LIỆU====================");
                        await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.deleteMany({});
                        let listDataAfterInsert = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.insertMany(listTrial_program_history_contact);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    } else { // XÓA VỚI ĐIỀU KIỆN
                        console.log("====================XÓA VỚI ĐIỀU KIỆN====================");

                        /**
                         * ===========================================================================
                         * =========================XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===========================================================================
                         */
                        let {
                            filter,
                            condition: conditionMultiple,
                        } = condition.conditionDeleteImport;

                        if (!filter || !filter.length) {
                            return resolve({
                                error: true,
                                message: 'Filter do not exist'
                            });
                        }

                        let conditionObj = {
                            state: 1,
                            $or: []
                        };

                        if (filter && filter.length) {
                            if (filter.length > 1) {

                                filter.map(filterObj => {
                                    if (filterObj.type === 'ref') {
                                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionFieldRef);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionFieldRef
                                            };
                                        }
                                    } else {
                                        const conditionByFilter = this.getConditionObj(filterObj);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionByFilter);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionByFilter
                                            };
                                        }
                                    }
                                });

                            } else {
                                let {
                                    type,
                                    ref,
                                    fieldRefName
                                } = filter[0];

                                if (type === 'ref') {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(ref, fieldRefName)
                                    };
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(filter[0])
                                    };
                                }
                            }
                        }

                        if (conditionObj.$or && !conditionObj.$or.length) {
                            delete conditionObj.$or;
                        }

                        let listAfterDelete = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.deleteMany({
                            ...conditionObj
                        });
                        /**
                         * ===============================================================================
                         * =========================END XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===============================================================================
                         */

                        if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                            for (let item of listTrial_program_history_contact) {
                                let listConditionFindOneUpdate = {};
                                let {
                                    listFieldPrimaryKey
                                } = condition;
                                listFieldPrimaryKey.map(elem => {
                                    listConditionFindOneUpdate = {
                                        ...listConditionFindOneUpdate,
                                        [elem]: item[elem]
                                    }
                                });

                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    state: 1
                                }

                                let checkExist = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                    $set: item
                                }, {
                                    upsert: true
                                });
                            }
                            return resolve({
                                error: false,
                                message: 'Insert success'
                            });

                        } else { // INSERT CÁI MỚI
                            console.log("====================INSERT CÁI MỚI 2====================");
                            let listDataAfterInsert = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.insertMany(listTrial_program_history_contact);
                            return resolve({
                                error: false,
                                message: 'Insert success',
                                data: listDataAfterInsert
                            });
                        }
                    }
                } else { // KHÔNG XÓA DATA CŨ
                    if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                        console.log("====================KIỂM TRA TỒN TẠI VÀ UPDATE====================");
                        for (let item of listTrial_program_history_contact) {
                            let listConditionFindOneUpdate = {};
                            let {
                                listFieldPrimaryKey
                            } = condition;
                            listFieldPrimaryKey.map(elem => {
                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    [elem]: item[elem]
                                }
                            });

                            listConditionFindOneUpdate = {
                                ...listConditionFindOneUpdate,
                                state: 1
                            }

                            let checkExist = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                $set: item
                            }, {
                                upsert: true
                            });
                        }
                        return resolve({
                            error: false,
                            message: 'Insert success'
                        });
                    } else { // INSERT CÁI MỚI
                        console.log("====================INSERT CÁI MỚI====================");
                        let listDataAfterInsert = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.insertMany(listTrial_program_history_contact);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    }
                }

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc trial_program_history_contact
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
            $or: []
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            let mappingRef = [];
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                    mappingRef = element.mappingRef;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            fieldID: item.fieldID,
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            ref: item.coll,
                            variable: item.name,
                            nameFieldRef,
                            dataEnum: item.dataEnum,
                            isRequire: item.isRequire,
                            dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                            variableChoice: item.variableChoice,
                            nameCollChoice: item.nameCollChoice,
                            mappingRef: mappingRef,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                fieldID: item.fieldID,
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                ref: item.coll,
                                variable: item.name,
                                nameFieldRef,
                                dataEnum: item.dataEnum,
                                isRequire: item.isRequire,
                                dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                variableChoice: item.variableChoice,
                                nameCollChoice: item.nameCollChoice,
                                mappingRef: mappingRef,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    dataEnum: item.dataEnum,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {
            if (filter.length > 1) {

                filter.map(filterObj => {
                    if (filterObj.type === 'ref') {
                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionFieldRef);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionFieldRef
                            };
                        }
                    } else {
                        const conditionByFilter = this.getConditionObj(filterObj);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionByFilter);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionByFilter
                            };
                        }
                    }
                });

            } else {
                let {
                    type,
                    ref,
                    fieldRefName
                } = filter[0];

                if (type === 'ref') {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(ref, fieldRefName)
                    };
                } else {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(filter[0])
                    };
                }
            }
        }

        if (conditionObj.$or && !conditionObj.$or.length) {
            delete conditionObj.$or;
        }


        if (!isEmptyObject(objFilterStatic)) {

            if (objFilterStatic.status) {
                if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                    return resolve({
                        error: true,
                        message: "trạng thái không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    status: Number(objFilterStatic.status)
                }
            }

        }


        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let collName = pluralize.plural(item.coll);
                let checkPluralColl = collName[collName.length - 1];

                if (checkPluralColl.toLowerCase() != 's') {
                    collName += 's';
                }

                let lookup = [{
                        $lookup: {
                            from: collName,
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        let collNamePopulate = pluralize.plural(populate.coll);
                        let checkPluralColl = collNamePopulate[collNamePopulate.length - 1];

                        if (checkPluralColl.toLowerCase() != 's') {
                            collNamePopulate += 's';
                        }

                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: collNamePopulate,
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Lấy thông tin trial_program_history_contact
     * @param {objectId} trial_program_history_contactID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoTrial_program_history_contact({
        trial_program_history_contactID,
        select,
        filter = {},
        explain,
        authorID
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];
                let conditionObj = {
                    state: 1
                };

                if (!checkObjectIDs([trial_program_history_contactID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị trial_program_history_contactID không hợp lệ',
                        status: 400
                    });
                }

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                Object.keys(filter).map(key => {
                    if (![].includes(key)) {
                        delete filter[key];
                    }
                });

                let {} = filter;


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['trialProgram', 'user_register_trial_program'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoTrial_program_history_contact = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL
                    .findOne({
                        _id: trial_program_history_contactID,
                        ...conditionObj,
                        userCreate: authorID
                    })
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoTrial_program_history_contact = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.populate(infoTrial_program_history_contact, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoTrial_program_history_contact = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.populate(infoTrial_program_history_contact, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoTrial_program_history_contact) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin trial_program_history_contact',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoTrial_program_history_contact,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách trial_program_history_contact
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListTrial_program_history_contacts({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page,
        authorID
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                } else {

                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['status', 'reminderTimeDateRange', 'trialProgram', 'user_register_trial_program'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    status,
                    reminderTimeDateRange,
                    trialProgram,
                    user_register_trial_program,
                } = filter;

                status && (conditionObj.status = status);

                if (reminderTimeDateRange) {
                    let [fromDate, toDate] = reminderTimeDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.reminderTime = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                trialProgram && (conditionObj.trialProgram = trialProgram);

                user_register_trial_program && (conditionObj.user_register_trial_program = user_register_trial_program);


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['trialProgram', 'user_register_trial_program'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listTrial_program_history_contacts = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL
                    .find({
                        ...conditionObj,
                        // userCreate: authorID
                    }, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        let listTrial_program_history_contactsTemp1 = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.populate(listTrial_program_history_contacts, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                        listTrial_program_history_contacts = [...listTrial_program_history_contactsTemp1];
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listTrial_program_history_contactsTemp2 = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.populate(listTrial_program_history_contacts, `${ref}.${field}`);
                            listTrial_program_history_contacts = [...listTrial_program_history_contactsTemp2];
                        }
                    }
                }

                if (!listTrial_program_history_contacts) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách trial_program_history_contact',
                        status: 400
                    });
                }

                let totalRecord = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.countDocuments({
                    ...conditionObj,
                    // userCreate: authorID
                });
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listTrial_program_history_contacts,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
                     * Tạo trial_program_history_contact
                     	* @param {string} note
	* @param {string} phone

                     * @param {objectId} userCreate
                     * @this {BaseModel}
                     * @extends {BaseModel}
                     * @returns {{ error: boolean, data?: object, message?: string }}
                     */
    insertTrial_program_history_contact({
        status,
        reminderTime,
        note,
        trialProgram,
        phone,
        user_register_trial_program,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([userCreate]))
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ',
                        status: 400
                    });



                if (phone && !checkPhoneNumber(phone)) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại không đúng định dạng',
                        status: 400
                    });
                }

                if (user_register_trial_program && !checkObjectIDs(user_register_trial_program)) {
                    return resolve({
                        error: true,
                        message: 'user_register_trial_program không hợp lệ',
                        status: 400
                    });
                }

                let dataInsert = {
                    status,
                    reminderTime,
                    note,
                    trialProgram,
                    phone,
                    user_register_trial_program,
                    userCreate
                };
                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);


                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo TRIAL_PROGRAM_HISTORY_CONTACT thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
                     * Cập nhật trial_program_history_contact
                     	* @param {string} note
	* @param {string} phone

                     * @param {objectId} trial_program_history_contactID
                     * @param {objectId} userUpdate
                     * @this {BaseModel}
                     * @extends {BaseModel}
                     * @returns {{ error: boolean, data?: object, message?: string }}
                     */
    updateTrial_program_history_contact({
        trial_program_history_contactID,
        status,
        reminderTime,
        note,
        trialProgram,
        phone,
        user_register_trial_program,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([trial_program_history_contactID]))
                    return resolve({
                        error: true,
                        message: 'trial_program_history_contactID không hợp lệ',
                        status: 400
                    });

                if (!checkObjectIDs([userUpdate]))
                    return resolve({
                        error: true,
                        message: 'ID user cập nhật không hợp lệ',
                        status: 400
                    });


                if (phone && !checkPhoneNumber(phone)) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại không đúng định dạng',
                        status: 400
                    });
                }

                if (user_register_trial_program && !checkObjectIDs(user_register_trial_program)) {
                    return resolve({
                        error: true,
                        message: 'user_register_trial_program không hợp lệ',
                        status: 400
                    });
                }

                const checkExists = await TRIAL_PROGRAM_HISTORY_CONTACT_COLL.findOne({
                    _id: trial_program_history_contactID,
                    state: 1
                });
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'trial_program_history_contact không tồn tại',
                        status: 404
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                status && (dataUpdate.status = status);
                reminderTime && (dataUpdate.reminderTime = new Date(reminderTime));
                note && (dataUpdate.note = note);
                trialProgram && (dataUpdate.trialProgram = trialProgram);
                phone && (dataUpdate.phone = phone);
                user_register_trial_program && (dataUpdate.user_register_trial_program = user_register_trial_program);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: trial_program_history_contactID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa trial_program_history_contact
     * @param {objectId} trial_program_history_contactID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteTrial_program_history_contact(trial_program_history_contactID, authorID) {
        return new Promise(async resolve => {
            try {
                let ids = [trial_program_history_contactID];

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị trial_program_history_contactID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.removeManyWhere({
                    _id: {
                        $in: ids
                    },
                    userCreate: authorID
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa trial_program_history_contact
     * @param {objectId} trial_program_history_contactID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteTrial_program_history_contacts(trial_program_history_contactsID, authorID) {
        return new Promise(async resolve => {
            try {
                let ids = trial_program_history_contactsID.split(',');

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị trial_program_history_contactsID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.removeManyWhere({
                    _id: {
                        $in: ids
                    },
                    userCreate: authorID
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

}

exports.MODEL = new Model;

/**
 * COLLECTIONS
 */
var TRIAL_PROGRAM_HISTORY_CONTACT_COLL = require('../databases/trial_program_history_contact-coll');

var TRIAL_PROGRAM_COLL = require('../databases/trial_program-coll');

var USER_REGISTER_TRIAL_PROGRAM_COLL = require('../databases/user_register_trial_program-coll');
const { resolveObjectURL } = require('buffer');
