/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const path = require('path');
const fs = require('fs');
const {
    hash,
    hashSync,
    compare
} = require('bcryptjs');
const XlsxPopulate = require('xlsx-populate');
const mongodb = require("mongodb");
const {
    MongoClient
} = mongodb;
let { sendMessageMobile }    = require('../../../fcm/utils');
const agenda				 = require('../../../config/cf_agenda');
let { getTimeBeforeMinutes } = require('../../../utils/time_utils');

/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    renderOptionFilter,
    colName,
    checkNumberIsValidWithRange
} = require('../../../utils/utils');
const {
    isTrue
} = require('../../../tools/module/check');
const {
    randomStringFixLength
} = require('../../../utils/string_utils');

const {
    STATUS_TRIAL_PROGRAM_TYPE,
} = require('../constants/trial_program');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const URL_DATABASE = process.env.URL_DATABASE || 'mongodb://localhost:27017';
const NAME_DATABASE = process.env.NAME_DATABASE || 'ldk_tools_op';
const TRIAL_PROGRAM_HISTORY_CONTACT_MODEL = require('../models/trial_program_history_contact').MODEL;

class Model extends BaseModel {
    constructor() {
        super(TRIAL_PROGRAM_COLL);

        // agenda dùng để push noti trước các buổi hẹn gặp 15 phút
        agenda.define('PUSH_NOTI_REMINDER_BOOKED', async (job, done) => {
            console.log(`===================== START PUSH_NOTI_REMINDER_BOOKED =====================`);

            try {
                let { historyContact } = job.attrs.data;
                let infoLatestTrialProgramHistoryContact = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getLatestTrialProgramHistoryContact({
                    user_register_trial_program: historyContact.user_register_trial_program._id,
                });

                // kiểm tra xem ID của lịch hẹn gặp hiện tại có trùng với ID của record cuối cùng lịch sử hẹn gặp không để PUSH_NOTI
                let idLatestTrialProgramHistoryContact = infoLatestTrialProgramHistoryContact && infoLatestTrialProgramHistoryContact.data && infoLatestTrialProgramHistoryContact.data._id;
                if(idLatestTrialProgramHistoryContact.toString() == historyContact.trial_program_history_contact_latest._id.toString()) {
                    let dataMessage = {
                        title: 'Thông báo lịch hẹn Trial Program', 
                        description: 'Bạn có lịch hẹn xử lý đơn hàng dùng thử với khách hàng hôm nay. Đừng quên bạn nhé', 
                        arrReceiverID: [historyContact.user_register_trial_program.employee]
                      }
                
                    let resultPushNoti = await this.pushNotiTrialProgram(dataMessage);
                    console.log({ resultPushNoti })
                }
                // await agenda.cancel({ name: "PUSH_NOTI_REMINDER_BOOKED" });
    
                done();
            } catch (error) {
                console.log({ error })
            }
        });
    }

    /**
         * Tạo mới trial_program
		* @param {string} title
		* @param {string} description
		* @param {string} content
		* @param {date} startTime
		* @param {date} endTime
		* @param {number} status
		* @param {number} views
		* @param {number} registers

         * @param {objectId} userCreate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        title,
        description,
        content,
        startTime,
        endTime,
        status = 1,
        views,
        registers,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (title.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài title không được lớn hơn 125 ký tự'
                    });
                }

                if (description.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài description không được lớn hơn 125 ký tự'
                    });
                }

                if (content.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài content không được lớn hơn 125 ký tự'
                    });
                }


                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 0, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'status không hợp lệ'
                    });
                }






                let dataInsert = {
                    title,
                    description,
                    content,
                    status,
                    views,
                    registers,
                    userCreate
                };
                startTime && (dataInsert.startTime = new Date(startTime));
                endTime && (dataInsert.endTime = new Date(endTime));

                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo TRAIAL PROGRAM - CHƯƠNG TRÌNH DÙNG THỬ thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật trial_program 
         * @param {objectId} trial_programID
		* @param {string} title
		* @param {string} description
		* @param {string} content
		* @param {date} startTime
		* @param {date} endTime
		* @param {number} status
		* @param {number} views
		* @param {number} registers

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        trial_programID,
        title,
        description,
        content,
        startTime,
        endTime,
        status,
        views,
        registers,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([trial_programID])) {
                    return resolve({
                        error: true,
                        message: 'trial_programID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (title.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài title không được lớn hơn 125 ký tự'
                    });
                }

                if (description.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài description không được lớn hơn 125 ký tự'
                    });
                }

                if (content.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài content không được lớn hơn 125 ký tự'
                    });
                }


                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 0, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'status không hợp lệ'
                    });
                }




                const checkExists = await TRIAL_PROGRAM_COLL.findById(trial_programID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'TRAIAL PROGRAM - CHƯƠNG TRÌNH DÙNG THỬ không tồn tại'
                    });
                }


                let dataUpdate = {
                    userUpdate
                };
                dataUpdate.title = title;
                dataUpdate.description = description;
                dataUpdate.content = content;
                startTime && (dataUpdate.startTime = new Date(startTime));
                endTime && (dataUpdate.endTime = new Date(endTime));
                dataUpdate.status = status;
                dataUpdate.views = views;
                dataUpdate.registers = registers;

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: trial_programID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật trial_program (không bắt buộc)
         * @param {objectId} trial_programID
		* @param {string} title
		* @param {string} description
		* @param {string} content
		* @param {date} startTime
		* @param {date} endTime
		* @param {number} status
		* @param {number} views
		* @param {number} registers

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        trial_programID,
        title,
        description,
        content,
        startTime,
        endTime,
        status,
        views,
        registers,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([trial_programID])) {
                    return resolve({
                        error: true,
                        message: 'trial_programID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 0, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'status không hợp lệ'
                    });
                }

                const checkExists = await TRIAL_PROGRAM_COLL.findById(trial_programID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'TRAIAL PROGRAM - CHƯƠNG TRÌNH DÙNG THỬ không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                title && (dataUpdate.title = title);
                description && (dataUpdate.description = description);
                content && (dataUpdate.content = content);
                startTime && (dataUpdate.startTime = new Date(startTime));
                endTime && (dataUpdate.endTime = new Date(endTime));
                status && (dataUpdate.status = status);
                views && (dataUpdate.views = views);
                registers && (dataUpdate.registers = registers);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: trial_programID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa trial_program 
     * @param {objectId} trial_programID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(trial_programID) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 2
                };
                if (!checkObjectIDs([trial_programID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị trial_programID không hợp lệ'
                    });
                }


                const infoAfterDelete = await this.updateById(trial_programID, conditionObj);

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa trial_program 
     * @param {array} trial_programID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(trial_programID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(trial_programID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị trial_programID không hợp lệ'
                    });
                }

                const infoAfterDelete = await TRIAL_PROGRAM_COLL.deleteMany({
                    _id: {
                        $in: trial_programID
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin trial_program 
     * @param {objectId} trial_programID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(trial_programID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([trial_programID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị trial_programID không hợp lệ'
                    });
                }

                const infoTrial_program = await TRIAL_PROGRAM_COLL.findById(trial_programID)
                    .populate('image banner products userCreate')

                if (!infoTrial_program) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin TRAIAL PROGRAM - CHƯƠNG TRÌNH DÙNG THỬ'
                    });
                }

                return resolve({
                    error: false,
                    data: infoTrial_program
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách trial_program 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listTrial_program = await TRIAL_PROGRAM_COLL
                    .find({
                        state: 1
                    }).populate('image banner products userCreate')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listTrial_program) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách TRAIAL PROGRAM - CHƯƠNG TRÌNH DÙNG THỬ'
                    });
                }

                return resolve({
                    error: false,
                    data: listTrial_program
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách trial_program theo bộ lọc
		* @param {string} keyword
		* @param {date} startTimeDateRange
		* @param {date} endTimeDateRange
		* @enum {number} status
		* @param {number} views
		* @param {number} registers

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        keyword,
        startTimeDateRange,
        endTimeDateRange,
        status,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        title: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        description: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        content: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }


                if (startTimeDateRange) {
                    let [fromDate, toDate] = startTimeDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.startTime = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                if (endTimeDateRange) {
                    let [fromDate, toDate] = endTimeDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.endTime = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                status && (conditionObj.status = status);


                const listTrial_programByFilter = await TRIAL_PROGRAM_COLL
                    .find(conditionObj).populate('image banner products userCreate')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listTrial_programByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách TRAIAL PROGRAM - CHƯƠNG TRÌNH DÙNG THỬ"
                    });
                }

                return resolve({
                    error: false,
                    data: listTrial_programByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách trial_program theo bộ lọc (server side)
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    status: {
                        $ne: 2 // status = 2 là đã xoá
                    },
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        title: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        description: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        content: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }




                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalTrial_program = await TRIAL_PROGRAM_COLL.countDocuments(conditionObj);

                const listTrial_programByFilter = await TRIAL_PROGRAM_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'images',
                            localField: 'image',
                            foreignField: '_id',
                            as: 'image'
                        }
                    },
                    {
                        $unwind: {
                            path: '$image',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'banners',
                            localField: 'banner',
                            foreignField: '_id',
                            as: 'banner'
                        }
                    },
                    {
                        $unwind: {
                            path: '$banner',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'users',
                            localField: 'userCreate',
                            foreignField: '_id',
                            as: 'userCreate'
                        }
                    },
                    {
                        $unwind: {
                            path: '$userCreate',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listTrial_programByFilter) {
                    return resolve({
                        recordsTotal: totalTrial_program,
                        recordsFiltered: totalTrial_program,
                        data: []
                    });
                }

                const listTrial_programDataTable = listTrial_programByFilter.map((trial_program, index) => {

                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${trial_program._id}" type="checkbox" class="check-record check-record-${trial_program._id}" _index ="${index + 1}"><label for="${trial_program._id}"></label></div></td>`,
                        indexSTT: skip + index + 1,
                        title: `<a href="/trial_program/update-trial_program-by-id?trial_programID=${trial_program._id}">${trial_program.title && trial_program.title.length > 50 ? trial_program.title.substr(0,50) + "..." : trial_program.title} </a>`,
                        status: `<span class="badge" style="background-color: ${STATUS_TRIAL_PROGRAM_TYPE[trial_program.status].color}">${STATUS_TRIAL_PROGRAM_TYPE[trial_program.status].value} </span>`,
                        views: `${trial_program.views && trial_program.views.length > 50 ? trial_program.views.substr(0,50) + "..." : trial_program.views}`,
                        registers: `${trial_program.registers && trial_program.registers.length > 50 ? trial_program.registers.substr(0,50) + "..." : trial_program.registers}`,

                        createAt: moment(trial_program.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalTrial_program,
                    recordsFiltered: totalTrial_program,
                    data: listTrial_programDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách trial_program theo bộ lọc import
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterImport({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        title: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        description: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        content: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }



                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalTrial_program = await TRIAL_PROGRAM_COLL.countDocuments(conditionObj);

                const listTrial_programByFilter = await TRIAL_PROGRAM_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'images',
                            localField: 'image',
                            foreignField: '_id',
                            as: 'image'
                        }
                    },
                    {
                        $unwind: {
                            path: '$image',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'banners',
                            localField: 'banner',
                            foreignField: '_id',
                            as: 'banner'
                        }
                    },
                    {
                        $unwind: {
                            path: '$banner',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'users',
                            localField: 'userCreate',
                            foreignField: '_id',
                            as: 'userCreate'
                        }
                    },
                    {
                        $unwind: {
                            path: '$userCreate',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listTrial_programByFilter) {
                    return resolve({
                        recordsTotal: totalTrial_program,
                        recordsFiltered: totalTrial_program,
                        data: []
                    });
                }

                return resolve({
                    error: false,
                    recordsTotal: totalTrial_program,
                    recordsFiltered: totalTrial_program,
                    data: listTrial_programByFilter || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc trial_program
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc trial_program
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listTrial_programByFilter = await TRIAL_PROGRAM_COLL.aggregate(arrayFilter)

                if (!listTrial_programByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listTrial_programByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listTrial_programByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('HH:mm DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Tải file import excel mẫu trial_program
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    fileImportExcelPreview({
        arrayItemCustomerChoice
    }) {
        return new Promise(async resolve => {
            try {
                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition; // BỘ LỌC && LOẠI IMPORT
                let {
                    listFieldPrimaryKey
                } = condition; // DANH SÁCH PRIMARY KEY

                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        let index = 0;
                        arrayItemCustomerChoice.map((elem) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            if (elem.dataDynamic && elem.dataDynamic.length) {
                                listFieldPrimaryKey = listFieldPrimaryKey.filter(key => key != elem.nameFieldRef); // LỌC PRIMARY KEY MÀ KHÔNG PHẢI REF

                                elem.dataDynamic.map(item => {
                                    workbook.sheet("Report").row(1).cell(index + 1).value(item);
                                    index++;
                                })
                            } else {
                                workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                                index++;
                            }
                        });

                        if (isTrue(condition.checkDownloadDataOld)) { // KIỂM TRA CÓ ĐÍNH ĐÈM DỮ LIỆU CŨ THEO ĐIỀU KIỆN
                            let listItemImport = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].listItemImport; // LIST FIELD ĐÃ ĐƯỢC CẤU HÌNH
                            let {
                                arrayFilter
                            } = this.getConditionArrayFilterExcel(listItemImport, condition.conditionDeleteImport.filter, condition.conditionDeleteImport.condition); // LẤY RA ARRAY ARREGATE

                            let groupByTrial_program = {};
                            listFieldPrimaryKey.map(key => {
                                groupByTrial_program = {
                                    ...groupByTrial_program,
                                    [key]: '$' + key
                                }
                            });

                            arrayFilter = [
                                ...arrayFilter,
                                {
                                    $group: {
                                        _id: {
                                            groupByTrial_program
                                        },
                                        listData: {
                                            $addToSet: "$$CURRENT"
                                        },
                                    }
                                }
                            ];

                            const listTrial_programByFilter = await TRIAL_PROGRAM_COLL.aggregate(arrayFilter);

                            listTrial_programByFilter && listTrial_programByFilter.length && listTrial_programByFilter.map((item, indexTrial_program) => {
                                let indexValue = 0;
                                arrayItemCustomerChoice.map((elem, indexChoice) => {
                                    let variable = elem.name.split('.');

                                    if (elem.dataDynamic && elem.dataDynamic.length) { // KIỂM TRA FIELD CÓ CHỌN DYNAMIC
                                        item.listData && item.listData.length && item.listData.map(value => { // ARRAY DATA DYNAMIC
                                            if (item.listData.length > elem.dataDynamic.length) { // KIỂM TRA ĐỘ DÀI CỦA DATA SO VỚI SỐ CỘT
                                                // TODO: XỬ LÝ NHIỀU DYNAMIC

                                            } else {
                                                elem.dataDynamic.map(dynamic => {
                                                    let valueOfField;
                                                    if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                        let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                        if (objDataOfVariable) {
                                                            valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                        }
                                                    } else {
                                                        valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                                    }

                                                    if (valueOfField == dynamic) { // CHECK NẾU VALUE === CỘT
                                                        let valueImportDynamic = value[elem.variableChoice] ? value[elem.variableChoice] : '';

                                                        // INSERT DỮ LIỆU VÀO BẢNG VỚI FIELD ĐƯỢC CHỌN THEO DẠNG DYNAMIC
                                                        workbook.sheet("Report").row(indexTrial_program + 2).cell(indexValue + 1).value(valueImportDynamic);
                                                        indexValue++;
                                                    } else {
                                                        if (elem.dataDynamic && item.listData && elem.dataDynamic.length > item.listData.length) {
                                                            indexValue++;
                                                        }
                                                    }
                                                })
                                            }
                                        })
                                    } else { // DẠNG STATIC
                                        let valueTrial_program;
                                        if (variable.length > 1) {
                                            let objDataOfVariable = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                            if (objDataOfVariable) {
                                                valueTrial_program = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                            }
                                        } else {
                                            valueTrial_program = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                        }

                                        workbook.sheet("Report").row(indexTrial_program + 2).cell(indexValue + 1).value(valueTrial_program);
                                        indexValue++;
                                    }
                                });
                            });
                        }

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    pathWriteFile,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Upload File Excel Import Lưu Dữ Liệu trial_program
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    importExcel({
        nameCollParent,
        arrayItemCustomerChoice,
        file,
    }) {
        return new Promise(async resolve => {
            try {

                XlsxPopulate.fromFileAsync(file.path)
                    .then(async workbook => {
                        let listData = [];
                        let index = 2;
                        let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition;

                        const client = await MongoClient.connect(URL_DATABASE);
                        const db = client.db(NAME_DATABASE)

                        for (; true;) {
                            if (arrayItemCustomerChoice && arrayItemCustomerChoice.length) {
                                let conditionObj = {};

                                let totalLength = 0;
                                arrayItemCustomerChoice.map((item, index) => {
                                    if (item.dataDynamic && item.dataDynamic.length) {
                                        totalLength += item.dataDynamic.length;
                                    } else {
                                        totalLength++;
                                    }
                                });

                                let indexOfListField = 0;
                                let checkIsRequire = false;
                                let arrayConditionObjDynamic = [];

                                for (let i = 0; i < totalLength; i++) {
                                    if (arrayItemCustomerChoice[indexOfListField].dataDynamic && arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                        let indexOfDynamic = 1;
                                        for (let valueDynamic of arrayItemCustomerChoice[indexOfListField].dataDynamic) {
                                            let letter = colName(i);
                                            let indexOfCeil = letter.toUpperCase() + index;

                                            let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                            if (variable) {

                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: valueDynamic.trim()
                                                });

                                                let conditionOfOneValueDynamic = {
                                                    [arrayItemCustomerChoice[indexOfListField].variableChoice]: variable
                                                }
                                                if (docs) {
                                                    conditionOfOneValueDynamic = {
                                                        ...conditionOfOneValueDynamic,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    }
                                                }

                                                arrayConditionObjDynamic = [
                                                    ...arrayConditionObjDynamic,
                                                    conditionOfOneValueDynamic
                                                ];
                                            }

                                            if (indexOfDynamic < arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                                i++;
                                            }
                                            indexOfDynamic++;
                                        }
                                    } else {
                                        let letter = colName(i);
                                        let indexOfCeil = letter.toUpperCase() + index;
                                        let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                        if (arrayItemCustomerChoice[indexOfListField].isRequire && !variable) {
                                            checkIsRequire = true;
                                            break;
                                        }
                                        if (arrayItemCustomerChoice[indexOfListField].ref && arrayItemCustomerChoice[indexOfListField].ref != nameCollParent) {
                                            if (arrayItemCustomerChoice[indexOfListField].isRequire) {
                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: variable.trim()
                                                })
                                                // .sort({
                                                //     _id: -1
                                                // })
                                                // .limit(100)
                                                // .toArray();
                                                if (docs) {
                                                    conditionObj = {
                                                        ...conditionObj,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                    };

                                                    if (arrayItemCustomerChoice[indexOfListField].mappingRef && arrayItemCustomerChoice[indexOfListField].mappingRef.length) {
                                                        arrayItemCustomerChoice[indexOfListField].mappingRef.map(mapping => {
                                                            conditionObj = {
                                                                ...conditionObj,
                                                                [mapping]: docs[mapping]
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                [arrayItemCustomerChoice[indexOfListField].name]: variable
                                            };
                                        }
                                    }

                                    indexOfListField++;
                                }
                                if (checkIsRequire) {
                                    break;
                                }

                                conditionObj = {
                                    ...conditionObj,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                }

                                let arrayCondditionObj = []
                                if (arrayConditionObjDynamic && arrayConditionObjDynamic.length) {
                                    arrayConditionObjDynamic.map(item => {
                                        arrayCondditionObj = [
                                            ...arrayCondditionObj,
                                            {
                                                ...conditionObj,
                                                ...item
                                            }
                                        ]
                                    });
                                } else {
                                    arrayCondditionObj = [
                                        ...arrayCondditionObj,
                                        conditionObj
                                    ]
                                }
                                listData = [
                                    ...listData,
                                    ...arrayCondditionObj
                                ];

                                index++;
                            }
                        }

                        await fs.unlinkSync(file.path);

                        if (listData.length) {
                            await this.changeDataImport({
                                condition,
                                listTrial_program: listData
                            });
                        } else {
                            return resolve({
                                error: true,
                                message: 'Import thất bại'
                            });
                        }

                        return resolve({
                            error: false,
                            message: 'Import thành công'
                        });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lưu Dữ Liệu Theo Lựa Chọn trial_program
     * @param {object} listTrial_program
     * @param {object} condition
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    changeDataImport({
        condition,
        listTrial_program
    }) {
        return new Promise(async resolve => {
            try {
                if (isTrue(condition.delete)) { // XÓA DATA CŨ
                    if (isTrue(condition.deleteAll)) { // XÓA TẤT CẢ DỮ LIỆU
                        console.log("====================XÓA TẤT CẢ DỮ LIỆU====================");
                        await TRIAL_PROGRAM_COLL.deleteMany({});
                        let listDataAfterInsert = await TRIAL_PROGRAM_COLL.insertMany(listTrial_program);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    } else { // XÓA VỚI ĐIỀU KIỆN
                        console.log("====================XÓA VỚI ĐIỀU KIỆN====================");

                        /**
                         * ===========================================================================
                         * =========================XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===========================================================================
                         */
                        let {
                            filter,
                            condition: conditionMultiple,
                        } = condition.conditionDeleteImport;

                        if (!filter || !filter.length) {
                            return resolve({
                                error: true,
                                message: 'Filter do not exist'
                            });
                        }

                        let conditionObj = {
                            state: 1,
                            $or: []
                        };

                        if (filter && filter.length) {
                            if (filter.length > 1) {

                                filter.map(filterObj => {
                                    if (filterObj.type === 'ref') {
                                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionFieldRef);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionFieldRef
                                            };
                                        }
                                    } else {
                                        const conditionByFilter = this.getConditionObj(filterObj);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionByFilter);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionByFilter
                                            };
                                        }
                                    }
                                });

                            } else {
                                let {
                                    type,
                                    ref,
                                    fieldRefName
                                } = filter[0];

                                if (type === 'ref') {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(ref, fieldRefName)
                                    };
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(filter[0])
                                    };
                                }
                            }
                        }

                        if (conditionObj.$or && !conditionObj.$or.length) {
                            delete conditionObj.$or;
                        }

                        let listAfterDelete = await TRIAL_PROGRAM_COLL.deleteMany({
                            ...conditionObj
                        });
                        /**
                         * ===============================================================================
                         * =========================END XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===============================================================================
                         */

                        if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                            for (let item of listTrial_program) {
                                let listConditionFindOneUpdate = {};
                                let {
                                    listFieldPrimaryKey
                                } = condition;
                                listFieldPrimaryKey.map(elem => {
                                    listConditionFindOneUpdate = {
                                        ...listConditionFindOneUpdate,
                                        [elem]: item[elem]
                                    }
                                });

                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    state: 1
                                }

                                let checkExist = await TRIAL_PROGRAM_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                    $set: item
                                }, {
                                    upsert: true
                                });
                            }
                            return resolve({
                                error: false,
                                message: 'Insert success'
                            });

                        } else { // INSERT CÁI MỚI
                            console.log("====================INSERT CÁI MỚI 2====================");
                            let listDataAfterInsert = await TRIAL_PROGRAM_COLL.insertMany(listTrial_program);
                            return resolve({
                                error: false,
                                message: 'Insert success',
                                data: listDataAfterInsert
                            });
                        }
                    }
                } else { // KHÔNG XÓA DATA CŨ
                    if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                        console.log("====================KIỂM TRA TỒN TẠI VÀ UPDATE====================");
                        for (let item of listTrial_program) {
                            let listConditionFindOneUpdate = {};
                            let {
                                listFieldPrimaryKey
                            } = condition;
                            listFieldPrimaryKey.map(elem => {
                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    [elem]: item[elem]
                                }
                            });

                            listConditionFindOneUpdate = {
                                ...listConditionFindOneUpdate,
                                state: 1
                            }

                            let checkExist = await TRIAL_PROGRAM_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                $set: item
                            }, {
                                upsert: true
                            });
                        }
                        return resolve({
                            error: false,
                            message: 'Insert success'
                        });
                    } else { // INSERT CÁI MỚI
                        console.log("====================INSERT CÁI MỚI====================");
                        let listDataAfterInsert = await TRIAL_PROGRAM_COLL.insertMany(listTrial_program);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    }
                }

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc trial_program
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
            $or: []
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            let mappingRef = [];
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                    mappingRef = element.mappingRef;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            fieldID: item.fieldID,
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            ref: item.coll,
                            variable: item.name,
                            nameFieldRef,
                            dataEnum: item.dataEnum,
                            isRequire: item.isRequire,
                            dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                            variableChoice: item.variableChoice,
                            nameCollChoice: item.nameCollChoice,
                            mappingRef: mappingRef,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                fieldID: item.fieldID,
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                ref: item.coll,
                                variable: item.name,
                                nameFieldRef,
                                dataEnum: item.dataEnum,
                                isRequire: item.isRequire,
                                dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                variableChoice: item.variableChoice,
                                nameCollChoice: item.nameCollChoice,
                                mappingRef: mappingRef,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    dataEnum: item.dataEnum,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {
            if (filter.length > 1) {

                filter.map(filterObj => {
                    if (filterObj.type === 'ref') {
                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionFieldRef);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionFieldRef
                            };
                        }
                    } else {
                        const conditionByFilter = this.getConditionObj(filterObj);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionByFilter);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionByFilter
                            };
                        }
                    }
                });

            } else {
                let {
                    type,
                    ref,
                    fieldRefName
                } = filter[0];

                if (type === 'ref') {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(ref, fieldRefName)
                    };
                } else {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(filter[0])
                    };
                }
            }
        }

        if (conditionObj.$or && !conditionObj.$or.length) {
            delete conditionObj.$or;
        }



        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let collName = pluralize.plural(item.coll);
                let checkPluralColl = collName[collName.length - 1];

                if (checkPluralColl.toLowerCase() != 's') {
                    collName += 's';
                }

                let lookup = [{
                        $lookup: {
                            from: collName,
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        let collNamePopulate = pluralize.plural(populate.coll);
                        let checkPluralColl = collNamePopulate[collNamePopulate.length - 1];

                        if (checkPluralColl.toLowerCase() != 's') {
                            collNamePopulate += 's';
                        }

                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: collNamePopulate,
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Lấy thông tin trial_program
     * @param {objectId} trial_programID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoTrial_program({
        trial_programID,
        select,
        filter = {},
        explain,
        authorID
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];
                let conditionObj = {
                    state: 1
                };

                if (!checkObjectIDs([trial_programID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị trial_programID không hợp lệ',
                        status: 400
                    });
                }

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                Object.keys(filter).map(key => {
                    if (![].includes(key)) {
                        delete filter[key];
                    }
                });

                let {} = filter;


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['image', 'banner', 'products', 'userCreate'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoTrial_program = await TRIAL_PROGRAM_COLL
                    .findOne({
                        _id: trial_programID,
                        ...conditionObj,
                        userCreate: authorID
                    })
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoTrial_program = await TRIAL_PROGRAM_COLL.populate(infoTrial_program, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoTrial_program = await TRIAL_PROGRAM_COLL.populate(infoTrial_program, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoTrial_program) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin trial_program',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoTrial_program,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách trial_program
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListTrial_programs({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page,
        authorID
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 1
                };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                } else {

                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['startTimeDateRange', 'endTimeDateRange', 'image', 'banner', 'status', 'products', 'userCreate'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    startTimeDateRange,
                    endTimeDateRange,
                    image,
                    banner,
                    status,
                    products,
                    userCreate,
                } = filter;

                if (startTimeDateRange) {
                    let [fromDate, toDate] = startTimeDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.startTime = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                if (endTimeDateRange) {
                    let [fromDate, toDate] = endTimeDateRange.split('-');
                    let _fromDate = moment(fromDate.trim()).startOf('day').format();
                    let _toDate = moment(toDate.trim()).endOf('day').format();

                    conditionObj.endTime = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }

                image && (conditionObj.image = image);

                banner && (conditionObj.banner = banner);

                status && (conditionObj.status = status);

                if (products && products.length) {
                    conditionObj.products = {
                        $in: products
                    };
                }

                userCreate && (conditionObj.userCreate = userCreate);


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['image', 'banner', 'products', 'userCreate'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listTrial_programs = await TRIAL_PROGRAM_COLL
                    .find({
                        ...conditionObj,
                        userCreate: authorID
                    }, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listTrial_programs = await TRIAL_PROGRAM_COLL.populate(listTrial_programs, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listTrial_programs = await TRIAL_PROGRAM_COLL.populate(listTrial_programs, `${ref}.${field}`);
                        }
                    }
                }

                if (!listTrial_programs) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách trial_program',
                        status: 400
                    });
                }

                let totalRecord = await TRIAL_PROGRAM_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listTrial_programs,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
                     * Tạo trial_program
                     	* @param {string} title
	* @param {string} description
	* @param {string} content

                     * @param {objectId} userCreate
                     * @this {BaseModel}
                     * @extends {BaseModel}
                     * @returns {{ error: boolean, data?: object, message?: string }}
                     */
    insertTrial_program({
        title,
        description,
        content,
        startTime,
        endTime,
        image,
        banner,
        status,
        views,
        registers,
        products,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([userCreate]))
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ',
                        status: 400
                    });



                if (image && !checkObjectIDs(image)) {
                    return resolve({
                        error: true,
                        message: 'image không hợp lệ',
                        status: 400
                    });
                }

                if (banner && !checkObjectIDs(banner)) {
                    return resolve({
                        error: true,
                        message: 'banner không hợp lệ',
                        status: 400
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 0, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'status không hợp lệ',
                        status: 400
                    });
                }

                if (products && !checkObjectIDs(products)) {
                    return resolve({
                        error: true,
                        message: 'products không hợp lệ',
                        status: 400
                    });
                }

                if (userCreate && !checkObjectIDs(userCreate)) {
                    return resolve({
                        error: true,
                        message: 'userCreate không hợp lệ',
                        status: 400
                    });
                }

                let dataInsert = {
                    title,
                    description,
                    content,
                    startTime,
                    endTime,
                    image,
                    banner,
                    status,
                    views,
                    registers,
                    products,
                    userCreate,
                    userCreate
                };
                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);


                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo TRIAL_PROGRAM thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
                     * Cập nhật trial_program
                     	* @param {string} title
	* @param {string} description
	* @param {string} content

                     * @param {objectId} trial_programID
                     * @param {objectId} userUpdate
                     * @this {BaseModel}
                     * @extends {BaseModel}
                     * @returns {{ error: boolean, data?: object, message?: string }}
                     */
    updateTrial_program({
        trial_programID,
        title,
        description,
        content,
        startTime,
        endTime,
        image,
        banner,
        status,
        views,
        registers,
        products,
        userCreate,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([trial_programID]))
                    return resolve({
                        error: true,
                        message: 'trial_programID không hợp lệ',
                        status: 400
                    });

                if (!checkObjectIDs([userUpdate]))
                    return resolve({
                        error: true,
                        message: 'ID user cập nhật không hợp lệ',
                        status: 400
                    });


                if (image && !checkObjectIDs(image)) {
                    return resolve({
                        error: true,
                        message: 'image không hợp lệ',
                        status: 400
                    });
                }

                if (banner && !checkObjectIDs(banner)) {
                    return resolve({
                        error: true,
                        message: 'banner không hợp lệ',
                        status: 400
                    });
                }

                if (status && !checkNumberIsValidWithRange({
                        arrValid: [1, 0, 2],
                        val: status
                    })) {
                    return resolve({
                        error: true,
                        message: 'status không hợp lệ',
                        status: 400
                    });
                }

                if (products && !checkObjectIDs(products)) {
                    return resolve({
                        error: true,
                        message: 'products không hợp lệ',
                        status: 400
                    });
                }

                if (userCreate && !checkObjectIDs(userCreate)) {
                    return resolve({
                        error: true,
                        message: 'userCreate không hợp lệ',
                        status: 400
                    });
                }

                const checkExists = await TRIAL_PROGRAM_COLL.findOne({
                    _id: trial_programID,
                    state: 1
                });
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'trial_program không tồn tại',
                        status: 404
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                title && (dataUpdate.title = title);
                description && (dataUpdate.description = description);
                content && (dataUpdate.content = content);
                startTime && (dataUpdate.startTime = new Date(startTime));
                endTime && (dataUpdate.endTime = new Date(endTime));
                image && (dataUpdate.image = image);
                banner && (dataUpdate.banner = banner);
                status && (dataUpdate.status = status);
                views && (dataUpdate.views = views);
                registers && (dataUpdate.registers = registers);
                products && (dataUpdate.products = products);
                userCreate && (dataUpdate.userCreate = userCreate);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: trial_programID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa trial_program
     * @param {objectId} trial_programID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteTrial_program(trial_programID, authorID) {
        return new Promise(async resolve => {
            try {
                let ids = [trial_programID];

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị trial_programID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.removeManyWhere({
                    _id: {
                        $in: ids
                    },
                    userCreate: authorID
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa trial_program
     * @param {objectId} trial_programID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteTrial_programs(trial_programsID, authorID) {
        return new Promise(async resolve => {
            try {
                let ids = trial_programsID.split(',');

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị trial_programsID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.removeManyWhere({
                    _id: {
                        $in: ids
                    },
                    userCreate: authorID
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    pushNotiWhenUserRegisterTrialProgram({
        employeeID,
        fullname, phone,
        userRegisterTrialProgramID
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([employeeID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị employeeID không hợp lệ'
                    });
                }

                sendMessageMobile({
                    title: `Khách hàng đăng ký thử nghiệm sản phẩm`,
                    description: `Khách hàng ${fullname} - SDT: ${phone}`,
                    arrReceiverID: [employeeID],
                    body: {
                        screen_key: 'TrialProgramDetailRegister',
                        userRegisterTrialProgramID: userRegisterTrialProgramID
                    }
                })
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    pushNotiWhenEmployeeBookingSuccess({
        customerID, userRegisterTrialProgramID
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([customerID, userRegisterTrialProgramID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị ID không hợp lệ'
                    });
                }

                let infoUserRegisterTrialProgram = await USER_REGISTER_TRIAL_PROGRAM_COLL.findOne({ _id: userRegisterTrialProgramID })
                                    .select("trialProgram centralize_order centralizeOrderID customer")
                                    .populate({
                                        path: "trialProgram",
                                        select: "title description content"
                                    })
                if(!infoUserRegisterTrialProgram)
                    return resolve({ error: false, message: 'Không thể lấy thông tin đăng ký chương trình dùng thử' });

                // tạo record notification
                let infoCentralLizeOrder = await CENTRALIZE_ORDER_MODEL.getCentralizeOrderByID({ centralizeOrderID: infoUserRegisterTrialProgram.centralizeOrderID });
                if(infoCentralLizeOrder.error)
                    return resolve({ error: true, message: infoCentralLizeOrder.message });

                const TYPE_NOTI_TRIAL_PROGRAM        = 2;
                let dataInsertNoti = { 
                    title: `Thông báo đơn hàng Trial Program`,
                    description: 'Đặt lịch thành công',
                    content: `Đơn hàng đăng ký dùng thử (Mã đơn: ${infoUserRegisterTrialProgram.centralizeOrderID}) đã được đặt lịch thành công`,
                    type: TYPE_NOTI_TRIAL_PROGRAM,
                    trialProgram: infoUserRegisterTrialProgram.trialProgram && infoUserRegisterTrialProgram.trialProgram._id,
                    centralize_order: infoCentralLizeOrder.data && infoCentralLizeOrder.data._id,
                    receive: infoUserRegisterTrialProgram.customer,
                }

                // trong insertV2 có phần PustNoti
                let infoNotificationAfterInsert = await NOTIFICATION_MODEL.insertV2({ listNotification: [dataInsertNoti] });
                if(infoNotificationAfterInsert.error)
                    return resolve({ error: true, message: 'Xảy ra lỗi trong quá trình tạo thông báo' });

                return resolve({ error: false, message: 'success' });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    pushNotiTrialProgram({
        title, description, arrReceiverID, body
    }) {
        return new Promise(async resolve => {
            try {
                let dataMessage = {
                    title,
                    description,
                    arrReceiverID
                }

                await sendMessageMobile(dataMessage);

                return resolve({
                    error: false,
                    message: 'OK'
                })
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    // function lập lịch push noti 15 phút trước các buổi hẹn gặp
    schedulePushNotiBeforeMeeting({ listTrialProgramHistoryContact }) {
        return new Promise(async resolve => {
            try {
                if(listTrialProgramHistoryContact && listTrialProgramHistoryContact.length) {
                    listTrialProgramHistoryContact.map(historyContact => {
                        let timeReminderBefore15Minute = getTimeBeforeMinutes(historyContact.trial_program_history_contact_latest.reminderTime, 15);
                        agenda.schedule(timeReminderBefore15Minute, 'PUSH_NOTI_REMINDER_BOOKED', { historyContact });
                        agenda.start();
                    })
                }

                return resolve({
                    error: false,
                    message: 'schedule success'
                })
            } catch (error) {
                console.log({ error })
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }
}

exports.MODEL = new Model;

/**
 * COLLECTIONS
 */
var TRIAL_PROGRAM_COLL = require('../databases/trial_program-coll');