/**
 * EXTERNAL PACKAGE
 */
const lodash = require('lodash');
const moment = require('moment');
const pluralize = require('pluralize');
const fastcsv = require('fast-csv');
const path = require('path');
const fs = require('fs');
const {
    hash,
    hashSync,
    compare
} = require('bcryptjs');
const XlsxPopulate = require('xlsx-populate');
const mongodb = require("mongodb");
const {
    MongoClient
} = mongodb;


/**
 * INTERNAL PACKAGE
 */
const {
    checkObjectIDs,
    cleanObject,
    IsJsonString,
    isEmptyObject,
    renderOptionFilter,
    colName,
    checkEmail,
    checkPhoneNumber
} = require('../../../utils/utils');
const {
    isTrue
} = require('../../../tools/module/check');
const {
    randomStringFixLength
} = require('../../../utils/string_utils');

const {} = require('../constants/user_register_trial_program');


/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const HOST_PROD = process.env.HOST_PROD || 'localhost';
const PORT_PROD = process.env.PORT_PROD || '5000';
const domain = HOST_PROD + ":" + PORT_PROD;
const URL_DATABASE = process.env.URL_DATABASE || 'mongodb://localhost:27017';
const NAME_DATABASE = process.env.NAME_DATABASE || 'ldk_tools_op';

class Model extends BaseModel {
    constructor() {
        super(USER_REGISTER_TRIAL_PROGRAM_COLL);
    }

    /**
         * Tạo mới user_register_trial_program
		* @param {object} trialProgram
		* @param {object} product
		* @param {number} status
		* @param {string} deviceID
		* @param {string} fullname
		* @param {string} email
		* @param {string} phone
		* @param {string} address
		* @param {string} cityNumber
		* @param {string} cityText
		* @param {string} districtNumber
		* @param {string} districtText
		* @param {string} wardNumber
		* @param {string} wardText
		* @param {string} fullAddress
		* @param {string} note
		* @param {array} fields
		* @param {object} store
		* @param {object} employee

         * @param {objectId} userCreate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    insert({
        trialProgram,
        product,
        status = 0,
        deviceID,
        fullname,
        email,
        phone,
        address,
        cityNumber,
        cityText,
        districtNumber,
        districtText,
        wardNumber,
        wardText,
        fullAddress,
        note,
        fields,
        store,
        employee,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (userCreate && !checkObjectIDs([userCreate])) {
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ'
                    });
                }

                if (deviceID.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài deviceid không được lớn hơn 125 ký tự'
                    });
                }

                if (fullname.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài fullname không được lớn hơn 125 ký tự'
                    });
                }

                if (email.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài email không được lớn hơn 125 ký tự'
                    });
                }

                if (phone.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài phone không được lớn hơn 125 ký tự'
                    });
                }

                if (address.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài address không được lớn hơn 125 ký tự'
                    });
                }

                if (cityNumber.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài citynumber không được lớn hơn 125 ký tự'
                    });
                }

                if (cityText.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài citytext không được lớn hơn 125 ký tự'
                    });
                }

                if (districtNumber.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài districtnumber không được lớn hơn 125 ký tự'
                    });
                }

                if (districtText.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài districttext không được lớn hơn 125 ký tự'
                    });
                }

                if (wardNumber.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài wardnumber không được lớn hơn 125 ký tự'
                    });
                }

                if (wardText.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài wardtext không được lớn hơn 125 ký tự'
                    });
                }

                if (fullAddress.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài fulladdress không được lớn hơn 125 ký tự'
                    });
                }

                if (note.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài note không được lớn hơn 125 ký tự'
                    });
                }



                if (email && !checkEmail(email)) {
                    return resolve({
                        error: true,
                        message: 'Email không hợp lệ'
                    });
                }


                if (phone && !checkPhoneNumber(phone)) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại không đúng định dạng'
                    });
                }


                if (trialProgram && !checkObjectIDs([trialProgram])) {
                    return resolve({
                        error: true,
                        message: 'chương trình không hợp lệ'
                    });
                }

                if (product && !checkObjectIDs([product])) {
                    return resolve({
                        error: true,
                        message: 'sản phẩm không hợp lệ'
                    });
                }

                if (fields && !checkObjectIDs(fields)) {
                    return resolve({
                        error: true,
                        message: 'fields không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'store không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'employee không hợp lệ'
                    });
                }



                let dataInsert = {
                    trialProgram,
                    product,
                    status,
                    deviceID,
                    fullname,
                    email,
                    phone,
                    address,
                    cityNumber,
                    cityText,
                    districtNumber,
                    districtText,
                    wardNumber,
                    wardText,
                    fullAddress,
                    note,
                    fields,
                    store,
                    employee,
                    userCreate
                };


                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo TRAIAL PROGRAM - SẢN PHẨM DÙNG THỬ thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật user_register_trial_program 
         * @param {objectId} user_register_trial_programID
		* @param {object} trialProgram
		* @param {object} product
		* @param {number} status
		* @param {string} deviceID
		* @param {string} fullname
		* @param {string} email
		* @param {string} phone
		* @param {string} address
		* @param {string} cityNumber
		* @param {string} cityText
		* @param {string} districtNumber
		* @param {string} districtText
		* @param {string} wardNumber
		* @param {string} wardText
		* @param {string} fullAddress
		* @param {string} note
		* @param {array} fields
		* @param {object} store
		* @param {object} employee

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    update({
        user_register_trial_programID,
        trialProgram,
        product,
        status,
        deviceID,
        fullname,
        email,
        phone,
        address,
        cityNumber,
        cityText,
        districtNumber,
        districtText,
        wardNumber,
        wardText,
        fullAddress,
        note,
        fields,
        store,
        employee,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([user_register_trial_programID])) {
                    return resolve({
                        error: true,
                        message: 'user_register_trial_programID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (deviceID.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài deviceid không được lớn hơn 125 ký tự'
                    });
                }

                if (fullname.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài fullname không được lớn hơn 125 ký tự'
                    });
                }

                if (email.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài email không được lớn hơn 125 ký tự'
                    });
                }

                if (phone.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài phone không được lớn hơn 125 ký tự'
                    });
                }

                if (address.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài address không được lớn hơn 125 ký tự'
                    });
                }

                if (cityNumber.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài citynumber không được lớn hơn 125 ký tự'
                    });
                }

                if (cityText.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài citytext không được lớn hơn 125 ký tự'
                    });
                }

                if (districtNumber.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài districtnumber không được lớn hơn 125 ký tự'
                    });
                }

                if (districtText.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài districttext không được lớn hơn 125 ký tự'
                    });
                }

                if (wardNumber.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài wardnumber không được lớn hơn 125 ký tự'
                    });
                }

                if (wardText.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài wardtext không được lớn hơn 125 ký tự'
                    });
                }

                if (fullAddress.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài fulladdress không được lớn hơn 125 ký tự'
                    });
                }

                if (note.length > 125) {
                    return resolve({
                        error: true,
                        message: 'Độ dài note không được lớn hơn 125 ký tự'
                    });
                }



                if (email && !checkEmail(email)) {
                    return resolve({
                        error: true,
                        message: 'Email không hợp lệ'
                    });
                }


                if (phone && !checkPhoneNumber(phone)) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại không đúng định dạng'
                    });
                }


                if (trialProgram && !checkObjectIDs([trialProgram])) {
                    return resolve({
                        error: true,
                        message: 'chương trình không hợp lệ'
                    });
                }

                if (product && !checkObjectIDs([product])) {
                    return resolve({
                        error: true,
                        message: 'sản phẩm không hợp lệ'
                    });
                }

                if (fields && !checkObjectIDs(fields)) {
                    return resolve({
                        error: true,
                        message: 'fields không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'store không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'employee không hợp lệ'
                    });
                }

                const checkExists = await USER_REGISTER_TRIAL_PROGRAM_COLL.findById(user_register_trial_programID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'TRAIAL PROGRAM - SẢN PHẨM DÙNG THỬ không tồn tại'
                    });
                }


                let dataUpdate = {
                    userUpdate
                };
                trialProgram && (dataUpdate.trialProgram = trialProgram);
                product && (dataUpdate.product = product);
                dataUpdate.status = status;
                dataUpdate.deviceID = deviceID;
                dataUpdate.fullname = fullname;
                dataUpdate.email = email;
                dataUpdate.phone = phone;
                dataUpdate.address = address;
                dataUpdate.cityNumber = cityNumber;
                dataUpdate.cityText = cityText;
                dataUpdate.districtNumber = districtNumber;
                dataUpdate.districtText = districtText;
                dataUpdate.wardNumber = wardNumber;
                dataUpdate.wardText = wardText;
                dataUpdate.fullAddress = fullAddress;
                dataUpdate.note = note;
                fields && (dataUpdate.fields = fields);
                store && (dataUpdate.store = store);
                employee && (dataUpdate.employee = employee);

                let infoAfterUpdate = await this.updateWhereClause({
                    _id: user_register_trial_programID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Cập nhật user_register_trial_program (không bắt buộc)
         * @param {objectId} user_register_trial_programID
		* @param {object} trialProgram
		* @param {object} product
		* @param {number} status
		* @param {string} deviceID
		* @param {string} fullname
		* @param {string} email
		* @param {string} phone
		* @param {string} address
		* @param {string} cityNumber
		* @param {string} cityText
		* @param {string} districtNumber
		* @param {string} districtText
		* @param {string} wardNumber
		* @param {string} wardText
		* @param {string} fullAddress
		* @param {string} note
		* @param {array} fields
		* @param {object} store
		* @param {object} employee

         * @param {objectId} userUpdate
         * @this {BaseModel}
         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: object, message?: string }}
         */
    updateNotRequire({
        user_register_trial_programID,
        trialProgram,
        product,
        status,
        deviceID,
        fullname,
        email,
        phone,
        address,
        cityNumber,
        cityText,
        districtNumber,
        districtText,
        wardNumber,
        wardText,
        fullAddress,
        note,
        fields,
        store,
        employee,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([user_register_trial_programID])) {
                    return resolve({
                        error: true,
                        message: 'user_register_trial_programID không hợp lệ'
                    });
                }

                if (userUpdate && !checkObjectIDs([userUpdate])) {
                    return resolve({
                        error: true,
                        message: 'Người cập nhật không hợp lệ'
                    });
                }

                if (trialProgram && !checkObjectIDs([trialProgram])) {
                    return resolve({
                        error: true,
                        message: 'chương trình không hợp lệ'
                    });
                }

                if (product && !checkObjectIDs([product])) {
                    return resolve({
                        error: true,
                        message: 'sản phẩm không hợp lệ'
                    });
                }

                if (email && !checkEmail(email)) {
                    return resolve({
                        error: true,
                        message: 'Email không hợp lệ'
                    });
                }

                if (phone && !checkPhoneNumber(phone)) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại không đúng định dạng'
                    });
                }

                if (fields && !checkObjectIDs(fields)) {
                    return resolve({
                        error: true,
                        message: 'fields không hợp lệ'
                    });
                }

                if (store && !checkObjectIDs([store])) {
                    return resolve({
                        error: true,
                        message: 'store không hợp lệ'
                    });
                }

                if (employee && !checkObjectIDs([employee])) {
                    return resolve({
                        error: true,
                        message: 'employee không hợp lệ'
                    });
                }

                const checkExists = await USER_REGISTER_TRIAL_PROGRAM_COLL.findById(user_register_trial_programID);
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'TRAIAL PROGRAM - SẢN PHẨM DÙNG THỬ không tồn tại'
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                trialProgram && (dataUpdate.trialProgram = trialProgram);
                product && (dataUpdate.product = product);
                status && (dataUpdate.status = status);
                deviceID && (dataUpdate.deviceID = deviceID);
                fullname && (dataUpdate.fullname = fullname);
                email && (dataUpdate.email = email);
                phone && (dataUpdate.phone = phone);
                address && (dataUpdate.address = address);
                cityNumber && (dataUpdate.cityNumber = cityNumber);
                cityText && (dataUpdate.cityText = cityText);
                districtNumber && (dataUpdate.districtNumber = districtNumber);
                districtText && (dataUpdate.districtText = districtText);
                wardNumber && (dataUpdate.wardNumber = wardNumber);
                wardText && (dataUpdate.wardText = wardText);
                fullAddress && (dataUpdate.fullAddress = fullAddress);
                note && (dataUpdate.note = note);
                fields && (dataUpdate.fields = fields);
                store && (dataUpdate.store = store);
                employee && (dataUpdate.employee = employee);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: user_register_trial_programID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại'
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa user_register_trial_program 
     * @param {objectId} user_register_trial_programID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteById(user_register_trial_programID) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    state: 2
                };
                if (!checkObjectIDs([user_register_trial_programID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị user_register_trial_programID không hợp lệ'
                    });
                }


                const infoAfterDelete = await this.updateById(user_register_trial_programID, conditionObj);

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Xóa user_register_trial_program 
     * @param {array} user_register_trial_programID
     * @extends {BaseModel}
     * @returns {{ 
     *      error: boolean, 
     *      message?: string,
     *      data?: { "nMatched": number, "nUpserted": number, "nModified": number }, 
     * }}
     */
    deleteByListId(user_register_trial_programID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(user_register_trial_programID)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị user_register_trial_programID không hợp lệ'
                    });
                }

                const infoAfterDelete = await USER_REGISTER_TRIAL_PROGRAM_COLL.deleteMany({
                    _id: {
                        $in: user_register_trial_programID
                    }
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy thông tin user_register_trial_program 
     * @param {objectId} user_register_trial_programID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoById(user_register_trial_programID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([user_register_trial_programID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị user_register_trial_programID không hợp lệ'
                    });
                }

                const infoUser_register_trial_program = await USER_REGISTER_TRIAL_PROGRAM_COLL.findById(user_register_trial_programID)
                    .populate('trialProgram product fields store employee')

                if (!infoUser_register_trial_program) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin TRAIAL PROGRAM - SẢN PHẨM DÙNG THỬ'
                    });
                }

                return resolve({
                    error: false,
                    data: infoUser_register_trial_program
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách user_register_trial_program 
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: array, message?: string }}
     */
    getList() {
        return new Promise(async resolve => {
            try {
                const listUser_register_trial_program = await USER_REGISTER_TRIAL_PROGRAM_COLL
                    .find({
                    }).populate('trialProgram product fields store employee')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listUser_register_trial_program) {
                    return resolve({
                        error: true,
                        message: 'Không thể lấy danh sách TRAIAL PROGRAM - SẢN PHẨM DÙNG THỬ'
                    });
                }

                return resolve({
                    error: false,
                    data: listUser_register_trial_program
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
         * Lấy danh sách user_register_trial_program theo bộ lọc
		* @param {number} status
		* @param {string} keyword

         * @extends {BaseModel}
         * @returns {{ error: boolean, data?: array, message?: string }}
         */
    getListByFilter({
        keyword,
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                };

                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        deviceID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullname: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        email: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        phone: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        address: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        cityNumber: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        cityText: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        districtNumber: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        districtText: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        wardNumber: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        wardText: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullAddress: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        note: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                const listUser_register_trial_programByFilter = await USER_REGISTER_TRIAL_PROGRAM_COLL
                    .find(conditionObj).populate('trialProgram product fields store employee')
                    .sort({
                        modifyAt: -1
                    })
                    .lean();

                if (!listUser_register_trial_programByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách TRAIAL PROGRAM - SẢN PHẨM DÙNG THỬ"
                    });
                }

                return resolve({
                    error: false,
                    data: listUser_register_trial_programByFilter
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách user_register_trial_program theo bộ lọc (server side)
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterServerSide({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        deviceID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullname: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        email: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        phone: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        address: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        cityNumber: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        cityText: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        districtNumber: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        districtText: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        wardNumber: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        wardText: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullAddress: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        note: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }



                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trạng thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }


                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalUser_register_trial_program = await USER_REGISTER_TRIAL_PROGRAM_COLL.countDocuments(conditionObj);

                const listUser_register_trial_programByFilter = await USER_REGISTER_TRIAL_PROGRAM_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'trial_programs',
                            localField: 'trialProgram',
                            foreignField: '_id',
                            as: 'trialProgram'
                        }
                    },
                    {
                        $unwind: {
                            path: '$trialProgram',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'products',
                            localField: 'product',
                            foreignField: '_id',
                            as: 'product'
                        }
                    },
                    {
                        $unwind: {
                            path: '$product',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listUser_register_trial_programByFilter) {
                    return resolve({
                        recordsTotal: totalUser_register_trial_program,
                        recordsFiltered: totalUser_register_trial_program,
                        data: []
                    });
                }

                const listUser_register_trial_programDataTable = listUser_register_trial_programByFilter.map((user_register_trial_program, index) => {

                    let status = '';
                    if (user_register_trial_program.status == 1) {
                        status = 'checked';
                    }

                    return {
                        index: `<td class="text-center"><div class="checkbox checkbox-success text-center"><input id="${user_register_trial_program._id}" type="checkbox" class="check-record check-record-${user_register_trial_program._id}" _index ="${index + 1}"><label for="${user_register_trial_program._id}"></label></div></td>`,
                        indexSTT: skip + index + 1,
                        trialProgram: user_register_trial_program.trialProgram ?
                            user_register_trial_program.trialProgram.title :
                            '<span class="badge bg-danger"> <span class="badge bg-danger"> không tồn tại hoặc đã xoá </span> </span>',
                        product: user_register_trial_program.product ?
                            user_register_trial_program.product.productCode :
                            '<span class="badge bg-danger"> <span class="badge bg-danger"> không tồn tại hoặc đã xoá </span> </span>',
                        status: `${user_register_trial_program.status && user_register_trial_program.status.length > 50 ? user_register_trial_program.status.substr(0,50) + "..." : user_register_trial_program.status}`,
                        deviceID: `<b>${user_register_trial_program.deviceID && user_register_trial_program.deviceID.length > 50 ? user_register_trial_program.deviceID.substr(0,50) + "..." : user_register_trial_program.deviceID}</b>`,
                        fullname: `${user_register_trial_program.fullname && user_register_trial_program.fullname.length > 50 ? user_register_trial_program.fullname.substr(0,50) + "..." : user_register_trial_program.fullname}`,
                        email: `${user_register_trial_program.email && user_register_trial_program.email.length > 50 ? user_register_trial_program.email.substr(0,50) + "..." : user_register_trial_program.email}`,
                        phone: `${user_register_trial_program.phone && user_register_trial_program.phone.length > 50 ? user_register_trial_program.phone.substr(0,50) + "..." : user_register_trial_program.phone}`,
                        address: `${user_register_trial_program.address && user_register_trial_program.address.length > 50 ? user_register_trial_program.address.substr(0,50) + "..." : user_register_trial_program.address}`,
                        store: user_register_trial_program.store ?
                            user_register_trial_program.store.name :
                            '<span class="badge bg-danger"> <span class="badge bg-danger"> không tồn tại hoặc đã xoá </span> </span>',
                        employee: user_register_trial_program.employee ?
                            user_register_trial_program.employee.code :
                            '<span class="badge bg-danger"> <span class="badge bg-danger"> không tồn tại hoặc đã xoá </span> </span>',

                        createAt: moment(user_register_trial_program.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                });

                return resolve({
                    error: false,
                    recordsTotal: totalUser_register_trial_program,
                    recordsFiltered: totalUser_register_trial_program,
                    data: listUser_register_trial_programDataTable || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy danh sách user_register_trial_program theo bộ lọc import
     
     * @param {string} keyword
     * @param {array} filter
     * @param {string} condition
     
     * @param {number} page
     * @param {number} limit
     * @param {string} field
     * @param {string} dir
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterImport({
        keyword,
        filter,
        condition,
        objFilterStatic,
        page,
        limit,
        field,
        dir
    }) {
        return new Promise(async resolve => {
            try {
                if (isNaN(page)) page = 1;
                if (isNaN(limit)) limit = 25;

                let conditionObj = {
                    state: 1,
                    $or: []
                };


                if (keyword) {
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [{
                        deviceID: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullname: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        email: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        phone: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        address: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        cityNumber: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        cityText: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        districtNumber: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        districtText: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        wardNumber: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        wardText: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        fullAddress: {
                            $regex: key,
                            $options: 'i'
                        }
                    }, {
                        note: {
                            $regex: key,
                            $options: 'i'
                        }
                    }]
                }



                if (filter && filter.length) {
                    if (filter.length > 1) {

                        filter.map(filterObj => {
                            if (filterObj.type === 'ref') {
                                const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionFieldRef);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionFieldRef
                                    };
                                }
                            } else {
                                const conditionByFilter = this.getConditionObj(filterObj);

                                if (condition === 'OR') {
                                    conditionObj.$or.push(conditionByFilter);
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...conditionByFilter
                                    };
                                }
                            }
                        });

                    } else {
                        let {
                            type,
                            ref,
                            fieldRefName
                        } = filter[0];

                        if (type === 'ref') {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(ref, fieldRefName)
                            };
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...this.getConditionObj(filter[0])
                            };
                        }
                    }
                }


                if (!isEmptyObject(objFilterStatic)) {

                    if (objFilterStatic.status) {
                        if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                            return resolve({
                                error: true,
                                message: "trạng thái không hợp lệ"
                            });
                        }
                        conditionObj = {
                            ...conditionObj,
                            status: Number(objFilterStatic.status)
                        }
                    }

                }


                if (conditionObj.$or && !conditionObj.$or.length) {
                    delete conditionObj.$or;
                }

                let sort = {
                    modifyAt: -1
                };
                if (field && dir) {
                    if (dir == 'asc') {
                        sort = {
                            [field]: 1
                        }
                    } else {
                        sort = {
                            [field]: -1
                        }
                    }
                }

                const skip = (page - 1) * limit;
                const totalUser_register_trial_program = await USER_REGISTER_TRIAL_PROGRAM_COLL.countDocuments(conditionObj);

                const listUser_register_trial_programByFilter = await USER_REGISTER_TRIAL_PROGRAM_COLL.aggregate([

                    {
                        $lookup: {
                            from: 'trial_programs',
                            localField: 'trialProgram',
                            foreignField: '_id',
                            as: 'trialProgram'
                        }
                    },
                    {
                        $unwind: {
                            path: '$trialProgram',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'products',
                            localField: 'product',
                            foreignField: '_id',
                            as: 'product'
                        }
                    },
                    {
                        $unwind: {
                            path: '$product',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'stores',
                            localField: 'store',
                            foreignField: '_id',
                            as: 'store'
                        }
                    },
                    {
                        $unwind: {
                            path: '$store',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $lookup: {
                            from: 'employees',
                            localField: 'employee',
                            foreignField: '_id',
                            as: 'employee'
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        },
                    },

                    {
                        $match: conditionObj
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: +skip
                    },
                    {
                        $limit: +limit
                    },
                ])

                if (!listUser_register_trial_programByFilter) {
                    return resolve({
                        recordsTotal: totalUser_register_trial_program,
                        recordsFiltered: totalUser_register_trial_program,
                        data: []
                    });
                }

                return resolve({
                    error: false,
                    recordsTotal: totalUser_register_trial_program,
                    recordsFiltered: totalUser_register_trial_program,
                    data: listUser_register_trial_programByFilter || []
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc user_register_trial_program
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionObj(filter, ref) {
        let {
            type,
            fieldName,
            cond,
            value
        } = filter;
        let conditionObj = {};

        if (ref) {
            fieldName = `${ref}.${fieldName}`;
        }

        switch (cond) {
            case 'equal': {
                if (type === 'date') {
                    let _fromDate = moment(value).startOf('day').format();
                    let _toDate = moment(value).endOf('day').format();

                    conditionObj[fieldName] = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = value;
                }
                break;
            }
            case 'not-equal': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $ne: new Date(value)
                    };
                } else {
                    value = (type === 'number' || type === 'enum') ? +value : value;
                    conditionObj[fieldName] = {
                        $ne: value
                    };
                }
                break;
            }
            case 'greater-than': {
                conditionObj[fieldName] = {
                    $gt: +value
                };
                break;
            }
            case 'less-than': {
                conditionObj[fieldName] = {
                    $lt: +value
                };
                break;
            }
            case 'start-with': {
                conditionObj[fieldName] = {
                    $regex: '^' + value,
                    $options: 'i'
                };
                break;
            }
            case 'end-with': {
                conditionObj[fieldName] = {
                    $regex: value + '$',
                    $options: 'i'
                };
                break;
            }
            case 'is-contains': {
                let key = value.split(" ");
                key = '.*' + key.join(".*") + '.*';

                conditionObj[fieldName] = {
                    $regex: key,
                    $options: 'i'
                };
                break;
            }
            case 'not-contains': {
                conditionObj[fieldName] = {
                    $not: {
                        $regex: value,
                        $options: 'i'
                    }
                };
                break;
            }
            case 'before': {
                conditionObj[fieldName] = {
                    $lt: new Date(value)
                };
                break;
            }
            case 'after': {
                conditionObj[fieldName] = {
                    $gt: new Date(value)
                };
                break;
            }
            case 'today': {
                let _fromDate = moment(new Date()).startOf('day').format();
                let _toDate = moment(new Date()).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'yesterday': {
                let yesterday = moment().add(-1, 'days');
                let _fromDate = moment(yesterday).startOf('day').format();
                let _toDate = moment(yesterday).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-hours': {
                let beforeNHours = moment().add(-value, 'hours');
                let _fromDate = moment(beforeNHours).startOf('day').format();
                let _toDate = moment(beforeNHours).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-days': {
                let beforeNDay = moment().add(-value, 'days');
                let _fromDate = moment(beforeNDay).startOf('day').format();
                let _toDate = moment(beforeNDay).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'before-months': {
                let beforeNMonth = moment().add(-value, 'months');
                let _fromDate = moment(beforeNMonth).startOf('day').format();
                let _toDate = moment(beforeNMonth).endOf('day').format();

                conditionObj[fieldName] = {
                    $gte: new Date(_fromDate),
                    $lte: new Date(_toDate)
                }
                break;
            }
            case 'null': {
                if (type === 'date') {
                    conditionObj[fieldName] = {
                        $exists: false
                    };
                } else {
                    conditionObj[fieldName] = {
                        $eq: ''
                    };
                }
                break;
            }
            default:
                break;
        }

        return conditionObj;
    }

    /**
     * Lấy điều kiện lọc user_register_trial_program
     * @param {array} arrayFilter
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getListByFilterExcel({
        arrayFilter,
        arrayItemCustomerChoice,
        chooseCSV,
        nameOfParentColl
    }) {
        return new Promise(async resolve => {
            try {

                const listUser_register_trial_programByFilter = await USER_REGISTER_TRIAL_PROGRAM_COLL.aggregate(arrayFilter)

                if (!listUser_register_trial_programByFilter) {
                    return resolve({
                        error: true,
                        message: "Không thể lấy danh sách orderv3"
                    });
                }
                const CHOOSE_CSV = 1;
                if (chooseCSV == CHOOSE_CSV) {
                    let nameFileExportCSV = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".csv";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportCSV);

                    let result = this.exportExcelCsv(pathWriteFile, listUser_register_trial_programByFilter, nameFileExportCSV, arrayItemCustomerChoice)
                    return resolve(result);
                } else {
                    let nameFileExportExcel = nameOfParentColl + "-" + moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";

                    let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', nameFileExportExcel);

                    let result = await this.exportExcelDownload(pathWriteFile, listUser_register_trial_programByFilter, nameFileExportExcel, arrayItemCustomerChoice);

                    return resolve(result);
                }

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    exportExcelCsv(pathWriteFile, lisData, fileNameRandom, arrayItemCustomerChoice) {
        let dataInsertCsv = [];
        lisData && lisData.length && lisData.map(item => {
            let objData = {};
            arrayItemCustomerChoice.map(elem => {
                let variable = elem.name.split('.');

                let value;
                if (variable.length > 1) {
                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                    if (objDataOfVariable) {
                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                    }
                } else {
                    value = item[variable[0]] ? item[variable[0]] : '';
                }

                if (elem.type == 'date') { // TYPE: DATE
                    value = moment(value).format('L');
                }

                let nameCollChoice = '';
                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                    nameCollChoice = ' ' + elem.nameCollChoice

                    elem.dataEnum.map(isStatus => {
                        if (isStatus.value == value) {
                            value = isStatus.title;
                        }
                    })
                }

                objData = {
                    ...objData,
                    [elem.note + nameCollChoice]: value
                }
            });

            objData = {
                ...objData,
                ['Ngày tạo']: moment(item.createAt).format('L')
            }

            dataInsertCsv = [
                ...dataInsertCsv,
                objData
            ]
        });

        let ws = fs.createWriteStream(pathWriteFile);
        fastcsv
            .write(dataInsertCsv, {
                headers: true
            })
            .on("finish", function() {
                console.log("Write to CSV successfully!");
            })
            .pipe(ws);

        return {
            error: false,
            data: "/files/upload_excel_temp/" + fileNameRandom,
            domain
        };
    }

    exportExcelDownload(pathWriteFile, listData, fileNameRandom, arrayItemCustomerChoice) {
        return new Promise(async resolve => {
            try {
                let dataInsertCsv = [];
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        arrayItemCustomerChoice.map((elem, index) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                        });
                        workbook.sheet("Report").row(1).cell(arrayItemCustomerChoice.length + 1).value('Ngày tạo');

                        listData && listData.length && listData.map((item, index) => {
                            // let objData = {};
                            arrayItemCustomerChoice.map((elem, indexChoice) => {
                                let variable = elem.name.split('.');

                                let value;
                                if (variable.length > 1) {
                                    let objDataOfVariable = item[variable[0]] ? item[variable[0]] : '';
                                    if (objDataOfVariable) {
                                        value = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                    }
                                } else {
                                    value = item[variable[0]] ? item[variable[0]] : '';
                                }

                                if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                    elem.dataEnum.map(isStatus => {
                                        if (isStatus.value == value) {
                                            value = isStatus.title;
                                        }
                                    })
                                }

                                if (elem.type == 'date') { // TYPE: DATE
                                    value = moment(value).format('HH:mm DD/MM/YYYY');
                                }
                                workbook.sheet("Report").row(index + 2).cell(indexChoice + 1).value(value);
                            });
                            workbook.sheet("Report").row(index + 2).cell(arrayItemCustomerChoice.length + 1).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));

                        });

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Tải file import excel mẫu user_register_trial_program
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    fileImportExcelPreview({
        arrayItemCustomerChoice
    }) {
        return new Promise(async resolve => {
            try {
                let fileNameRandom = moment(new Date()).format('LT') + "-" + moment(new Date()).format('DD-MM-YYYY') + ".xlsx";
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition; // BỘ LỌC && LOẠI IMPORT
                let {
                    listFieldPrimaryKey
                } = condition; // DANH SÁCH PRIMARY KEY

                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                    .then(async workbook => {
                        let index = 0;
                        arrayItemCustomerChoice.map((elem) => {
                            let nameCollChoice = '';
                            if (elem.dataEnum && elem.dataEnum.length) { // TYPE: ISTATUS
                                nameCollChoice = ' ' + elem.nameCollChoice
                            }
                            if (elem.dataDynamic && elem.dataDynamic.length) {
                                listFieldPrimaryKey = listFieldPrimaryKey.filter(key => key != elem.nameFieldRef); // LỌC PRIMARY KEY MÀ KHÔNG PHẢI REF

                                elem.dataDynamic.map(item => {
                                    workbook.sheet("Report").row(1).cell(index + 1).value(item);
                                    index++;
                                })
                            } else {
                                workbook.sheet("Report").row(1).cell(index + 1).value(elem.note + nameCollChoice);
                                index++;
                            }
                        });

                        if (isTrue(condition.checkDownloadDataOld)) { // KIỂM TRA CÓ ĐÍNH ĐÈM DỮ LIỆU CŨ THEO ĐIỀU KIỆN
                            let listItemImport = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].listItemImport; // LIST FIELD ĐÃ ĐƯỢC CẤU HÌNH
                            let {
                                arrayFilter
                            } = this.getConditionArrayFilterExcel(listItemImport, condition.conditionDeleteImport.filter, condition.conditionDeleteImport.condition); // LẤY RA ARRAY ARREGATE

                            let groupByUser_register_trial_program = {};
                            listFieldPrimaryKey.map(key => {
                                groupByUser_register_trial_program = {
                                    ...groupByUser_register_trial_program,
                                    [key]: '$' + key
                                }
                            });

                            arrayFilter = [
                                ...arrayFilter,
                                {
                                    $group: {
                                        _id: {
                                            groupByUser_register_trial_program
                                        },
                                        listData: {
                                            $addToSet: "$$CURRENT"
                                        },
                                    }
                                }
                            ];

                            const listUser_register_trial_programByFilter = await USER_REGISTER_TRIAL_PROGRAM_COLL.aggregate(arrayFilter);

                            listUser_register_trial_programByFilter && listUser_register_trial_programByFilter.length && listUser_register_trial_programByFilter.map((item, indexUser_register_trial_program) => {
                                let indexValue = 0;
                                arrayItemCustomerChoice.map((elem, indexChoice) => {
                                    let variable = elem.name.split('.');

                                    if (elem.dataDynamic && elem.dataDynamic.length) { // KIỂM TRA FIELD CÓ CHỌN DYNAMIC
                                        item.listData && item.listData.length && item.listData.map(value => { // ARRAY DATA DYNAMIC
                                            if (item.listData.length > elem.dataDynamic.length) { // KIỂM TRA ĐỘ DÀI CỦA DATA SO VỚI SỐ CỘT
                                                // TODO: XỬ LÝ NHIỀU DYNAMIC

                                            } else {
                                                elem.dataDynamic.map(dynamic => {
                                                    let valueOfField;
                                                    if (variable.length > 1) { // LẤY RA VALUE CỦA CỦA FIELD CHỌN
                                                        let objDataOfVariable = value[variable[0]] ? value[variable[0]] : '';
                                                        if (objDataOfVariable) {
                                                            valueOfField = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                                        }
                                                    } else {
                                                        valueOfField = value[variable[0]] ? value[variable[0]] : '';
                                                    }

                                                    if (valueOfField == dynamic) { // CHECK NẾU VALUE === CỘT
                                                        let valueImportDynamic = value[elem.variableChoice] ? value[elem.variableChoice] : '';

                                                        // INSERT DỮ LIỆU VÀO BẢNG VỚI FIELD ĐƯỢC CHỌN THEO DẠNG DYNAMIC
                                                        workbook.sheet("Report").row(indexUser_register_trial_program + 2).cell(indexValue + 1).value(valueImportDynamic);
                                                        indexValue++;
                                                    } else {
                                                        if (elem.dataDynamic && item.listData && elem.dataDynamic.length > item.listData.length) {
                                                            indexValue++;
                                                        }
                                                    }
                                                })
                                            }
                                        })
                                    } else { // DẠNG STATIC
                                        let valueUser_register_trial_program;
                                        if (variable.length > 1) {
                                            let objDataOfVariable = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                            if (objDataOfVariable) {
                                                valueUser_register_trial_program = objDataOfVariable[variable[1]] ? objDataOfVariable[variable[1]] : '';
                                            }
                                        } else {
                                            valueUser_register_trial_program = item.listData[0][variable[0]] ? item.listData[0][variable[0]] : '';
                                        }

                                        workbook.sheet("Report").row(indexUser_register_trial_program + 2).cell(indexValue + 1).value(valueUser_register_trial_program);
                                        indexValue++;
                                    }
                                });
                            });
                        }

                        // });
                        workbook.toFileAsync(pathWriteFile)
                            .then(_ => {
                                // Download file from server
                                return resolve({
                                    error: false,
                                    data: "/files/upload_excel_temp/" + fileNameRandom,
                                    path: fileNameRandom,
                                    pathWriteFile,
                                    domain
                                });
                            });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Upload File Excel Import Lưu Dữ Liệu user_register_trial_program
     * @param {object} arrayItemCustomerChoice
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    importExcel({
        nameCollParent,
        arrayItemCustomerChoice,
        file,
    }) {
        return new Promise(async resolve => {
            try {

                XlsxPopulate.fromFileAsync(file.path)
                    .then(async workbook => {
                        let listData = [];
                        let index = 2;
                        let condition = arrayItemCustomerChoice && arrayItemCustomerChoice.length && arrayItemCustomerChoice[0].condition;

                        const client = await MongoClient.connect(URL_DATABASE);
                        const db = client.db(NAME_DATABASE)

                        for (; true;) {
                            if (arrayItemCustomerChoice && arrayItemCustomerChoice.length) {
                                let conditionObj = {};

                                let totalLength = 0;
                                arrayItemCustomerChoice.map((item, index) => {
                                    if (item.dataDynamic && item.dataDynamic.length) {
                                        totalLength += item.dataDynamic.length;
                                    } else {
                                        totalLength++;
                                    }
                                });

                                let indexOfListField = 0;
                                let checkIsRequire = false;
                                let arrayConditionObjDynamic = [];

                                for (let i = 0; i < totalLength; i++) {
                                    if (arrayItemCustomerChoice[indexOfListField].dataDynamic && arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                        let indexOfDynamic = 1;
                                        for (let valueDynamic of arrayItemCustomerChoice[indexOfListField].dataDynamic) {
                                            let letter = colName(i);
                                            let indexOfCeil = letter.toUpperCase() + index;

                                            let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                            if (variable) {

                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: valueDynamic.trim()
                                                });

                                                let conditionOfOneValueDynamic = {
                                                    [arrayItemCustomerChoice[indexOfListField].variableChoice]: variable
                                                }
                                                if (docs) {
                                                    conditionOfOneValueDynamic = {
                                                        ...conditionOfOneValueDynamic,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id,
                                                    }
                                                }

                                                arrayConditionObjDynamic = [
                                                    ...arrayConditionObjDynamic,
                                                    conditionOfOneValueDynamic
                                                ];
                                            }

                                            if (indexOfDynamic < arrayItemCustomerChoice[indexOfListField].dataDynamic.length) {
                                                i++;
                                            }
                                            indexOfDynamic++;
                                        }
                                    } else {
                                        let letter = colName(i);
                                        let indexOfCeil = letter.toUpperCase() + index;
                                        let variable = workbook.sheet(0).cell(indexOfCeil).value();
                                        if (arrayItemCustomerChoice[indexOfListField].isRequire && !variable) {
                                            checkIsRequire = true;
                                            break;
                                        }
                                        if (arrayItemCustomerChoice[indexOfListField].ref && arrayItemCustomerChoice[indexOfListField].ref != nameCollParent) {
                                            if (arrayItemCustomerChoice[indexOfListField].isRequire) {
                                                let collName = pluralize.plural(arrayItemCustomerChoice[indexOfListField].ref);
                                                let checkPluralColl = collName[collName.length - 1];

                                                if (checkPluralColl.toLowerCase() != 's') {
                                                    collName += 's';
                                                }

                                                const docs = await db.collection(collName).findOne({
                                                    [arrayItemCustomerChoice[indexOfListField].variable]: variable.trim()
                                                })
                                                // .sort({
                                                //     _id: -1
                                                // })
                                                // .limit(100)
                                                // .toArray();
                                                if (docs) {
                                                    conditionObj = {
                                                        ...conditionObj,
                                                        [arrayItemCustomerChoice[indexOfListField].nameFieldRef]: docs._id
                                                    };

                                                    if (arrayItemCustomerChoice[indexOfListField].mappingRef && arrayItemCustomerChoice[indexOfListField].mappingRef.length) {
                                                        arrayItemCustomerChoice[indexOfListField].mappingRef.map(mapping => {
                                                            conditionObj = {
                                                                ...conditionObj,
                                                                [mapping]: docs[mapping]
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                [arrayItemCustomerChoice[indexOfListField].name]: variable
                                            };
                                        }
                                    }

                                    indexOfListField++;
                                }
                                if (checkIsRequire) {
                                    break;
                                }

                                conditionObj = {
                                    ...conditionObj,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                }

                                let arrayCondditionObj = []
                                if (arrayConditionObjDynamic && arrayConditionObjDynamic.length) {
                                    arrayConditionObjDynamic.map(item => {
                                        arrayCondditionObj = [
                                            ...arrayCondditionObj,
                                            {
                                                ...conditionObj,
                                                ...item
                                            }
                                        ]
                                    });
                                } else {
                                    arrayCondditionObj = [
                                        ...arrayCondditionObj,
                                        conditionObj
                                    ]
                                }
                                listData = [
                                    ...listData,
                                    ...arrayCondditionObj
                                ];

                                index++;
                            }
                        }

                        await fs.unlinkSync(file.path);

                        if (listData.length) {
                            await this.changeDataImport({
                                condition,
                                listUser_register_trial_program: listData
                            });
                        } else {
                            return resolve({
                                error: true,
                                message: 'Import thất bại'
                            });
                        }

                        return resolve({
                            error: false,
                            message: 'Import thành công'
                        });
                    });

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lưu Dữ Liệu Theo Lựa Chọn user_register_trial_program
     * @param {object} listUser_register_trial_program
     * @param {object} condition
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    changeDataImport({
        condition,
        listUser_register_trial_program
    }) {
        return new Promise(async resolve => {
            try {
                if (isTrue(condition.delete)) { // XÓA DATA CŨ
                    if (isTrue(condition.deleteAll)) { // XÓA TẤT CẢ DỮ LIỆU
                        console.log("====================XÓA TẤT CẢ DỮ LIỆU====================");
                        await USER_REGISTER_TRIAL_PROGRAM_COLL.deleteMany({});
                        let listDataAfterInsert = await USER_REGISTER_TRIAL_PROGRAM_COLL.insertMany(listUser_register_trial_program);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    } else { // XÓA VỚI ĐIỀU KIỆN
                        console.log("====================XÓA VỚI ĐIỀU KIỆN====================");

                        /**
                         * ===========================================================================
                         * =========================XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===========================================================================
                         */
                        let {
                            filter,
                            condition: conditionMultiple,
                        } = condition.conditionDeleteImport;

                        if (!filter || !filter.length) {
                            return resolve({
                                error: true,
                                message: 'Filter do not exist'
                            });
                        }

                        let conditionObj = {
                            state: 1,
                            $or: []
                        };

                        if (filter && filter.length) {
                            if (filter.length > 1) {

                                filter.map(filterObj => {
                                    if (filterObj.type === 'ref') {
                                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionFieldRef);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionFieldRef
                                            };
                                        }
                                    } else {
                                        const conditionByFilter = this.getConditionObj(filterObj);

                                        if (conditionMultiple === 'OR') {
                                            conditionObj.$or.push(conditionByFilter);
                                        } else {
                                            conditionObj = {
                                                ...conditionObj,
                                                ...conditionByFilter
                                            };
                                        }
                                    }
                                });

                            } else {
                                let {
                                    type,
                                    ref,
                                    fieldRefName
                                } = filter[0];

                                if (type === 'ref') {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(ref, fieldRefName)
                                    };
                                } else {
                                    conditionObj = {
                                        ...conditionObj,
                                        ...this.getConditionObj(filter[0])
                                    };
                                }
                            }
                        }

                        if (conditionObj.$or && !conditionObj.$or.length) {
                            delete conditionObj.$or;
                        }

                        let listAfterDelete = await USER_REGISTER_TRIAL_PROGRAM_COLL.deleteMany({
                            ...conditionObj
                        });
                        /**
                         * ===============================================================================
                         * =========================END XÓA DỮ LIỆU VỚI ĐIỀU KIỆN=========================
                         * ===============================================================================
                         */

                        if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                            for (let item of listUser_register_trial_program) {
                                let listConditionFindOneUpdate = {};
                                let {
                                    listFieldPrimaryKey
                                } = condition;
                                listFieldPrimaryKey.map(elem => {
                                    listConditionFindOneUpdate = {
                                        ...listConditionFindOneUpdate,
                                        [elem]: item[elem]
                                    }
                                });

                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    state: 1
                                }

                                let checkExist = await USER_REGISTER_TRIAL_PROGRAM_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                    $set: item
                                }, {
                                    upsert: true
                                });
                            }
                            return resolve({
                                error: false,
                                message: 'Insert success'
                            });

                        } else { // INSERT CÁI MỚI
                            console.log("====================INSERT CÁI MỚI 2====================");
                            let listDataAfterInsert = await USER_REGISTER_TRIAL_PROGRAM_COLL.insertMany(listUser_register_trial_program);
                            return resolve({
                                error: false,
                                message: 'Insert success',
                                data: listDataAfterInsert
                            });
                        }
                    }
                } else { // KHÔNG XÓA DATA CŨ
                    if (condition.typeChangeData == 'update') { // KIỂM TRA TỒN TẠI VÀ UPDATE
                        console.log("====================KIỂM TRA TỒN TẠI VÀ UPDATE====================");
                        for (let item of listUser_register_trial_program) {
                            let listConditionFindOneUpdate = {};
                            let {
                                listFieldPrimaryKey
                            } = condition;
                            listFieldPrimaryKey.map(elem => {
                                listConditionFindOneUpdate = {
                                    ...listConditionFindOneUpdate,
                                    [elem]: item[elem]
                                }
                            });

                            listConditionFindOneUpdate = {
                                ...listConditionFindOneUpdate,
                                state: 1
                            }

                            let checkExist = await USER_REGISTER_TRIAL_PROGRAM_COLL.findOneAndUpdate(listConditionFindOneUpdate, {
                                $set: item
                            }, {
                                upsert: true
                            });
                        }
                        return resolve({
                            error: false,
                            message: 'Insert success'
                        });
                    } else { // INSERT CÁI MỚI
                        console.log("====================INSERT CÁI MỚI====================");
                        let listDataAfterInsert = await USER_REGISTER_TRIAL_PROGRAM_COLL.insertMany(listUser_register_trial_program);
                        return resolve({
                            error: false,
                            message: 'Insert success',
                            data: listDataAfterInsert
                        });
                    }
                }

            } catch (error) {
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    /**
     * Lấy điều kiện lọc user_register_trial_program
     * @param {object} filter
     * @extends {BaseModel}
     * @returns {{ [key]: [value] }}
     */
    getConditionArrayFilterExcel(listItemExport, filter, condition, objFilterStatic, field, dir, keyword, arrayItemChecked) {
        let arrPopulate = [];
        let refParent = '';
        let arrayItemCustomerChoice = [];
        let arrayFieldIDChoice = [];

        let conditionObj = {
            state: 1,
            $or: []
        };

        listItemExport.map((item, index) => {
            arrayFieldIDChoice = [
                ...arrayFieldIDChoice,
                item.fieldID
            ];

            let nameFieldRef = '';
            let mappingRef = [];
            listItemExport.map(element => {
                if (element.ref == item.coll) {
                    nameFieldRef = element.name;
                    mappingRef = element.mappingRef;
                }
            });
            if (!item.refFrom) {
                refParent = item.coll;
                // select += item.name + " ";
                if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                    arrayItemCustomerChoice = [
                        ...arrayItemCustomerChoice,
                        {
                            fieldID: item.fieldID,
                            name: item.name,
                            note: item.note,
                            type: item.typeVar,
                            ref: item.coll,
                            variable: item.name,
                            nameFieldRef,
                            dataEnum: item.dataEnum,
                            isRequire: item.isRequire,
                            dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                            variableChoice: item.variableChoice,
                            nameCollChoice: item.nameCollChoice,
                            mappingRef: mappingRef,
                        }
                    ];
                }
            } else {
                if (!arrPopulate.length) {
                    arrPopulate = [{
                        name: nameFieldRef,
                        coll: item.coll,
                        select: item.name + " ",
                        refFrom: item.refFrom,
                    }];
                    if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                        arrayItemCustomerChoice = [
                            ...arrayItemCustomerChoice,
                            {
                                fieldID: item.fieldID,
                                name: nameFieldRef + "." + item.name,
                                note: item.note,
                                type: item.typeVar,
                                ref: item.coll,
                                variable: item.name,
                                nameFieldRef,
                                dataEnum: item.dataEnum,
                                isRequire: item.isRequire,
                                dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                variableChoice: item.variableChoice,
                                nameCollChoice: item.nameCollChoice,
                                mappingRef: mappingRef,
                            }
                        ];
                    }
                } else {
                    if (item.refFrom == refParent) { // POPULATE CẤP 2
                        let mark = false;
                        arrPopulate.map((elem, indexV2) => {
                            if (elem.coll == item.coll) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                mark = true,
                                    arrPopulate[indexV2].select += item.name + " ";
                            }
                        });
                        if (!mark) { // CHƯA ĐƯỢC THÊM THÌ THÊM VÀO
                            arrPopulate = [
                                ...arrPopulate,
                                {
                                    name: nameFieldRef,
                                    coll: item.coll,
                                    select: item.name + " ",
                                    refFrom: item.refFrom,
                                }
                            ];
                        }
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    dataEnum: item.dataEnum,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                    } else { // POPULATE CẤP 3 TRỞ LÊN
                        if (!item.ref) { // KHÔNG THÊM NHỮNG FIELD REF
                            arrayItemCustomerChoice = [
                                ...arrayItemCustomerChoice,
                                {
                                    fieldID: item.fieldID,
                                    name: nameFieldRef + "." + item.name,
                                    note: item.note,
                                    type: item.typeVar,
                                    dataEnum: item.dataEnum,
                                    ref: item.coll,
                                    variable: item.name,
                                    nameFieldRef,
                                    isRequire: item.isRequire,
                                    dataDynamic: item.dataDynamic ? item.dataDynamic : [],
                                    variableChoice: item.variableChoice,
                                    nameCollChoice: item.nameCollChoice,
                                    mappingRef: mappingRef,
                                }
                            ];
                        }
                        arrPopulate.map((elem, indexV3) => {
                            if (elem.coll == item.refFrom) { // KIỂM TRA XEM POPULATE CẤP 3 ĐÃ ĐƯỢC THÊM VẢO MẢNG
                                if (!arrPopulate[indexV3].populate || !arrPopulate[indexV3].populate.length) {
                                    arrPopulate[indexV3].populate = [{
                                        name: nameFieldRef,
                                        coll: item.coll,
                                        select: item.name + " ",
                                        refFrom: item.refFrom,
                                    }]
                                } else {
                                    let markPopulate = false;
                                    arrPopulate[indexV3].populate.map(populate => {
                                        if (!populate.name.includes(item.coll)) {
                                            markPopulate = true;
                                            arrPopulate[indexV3].populate = [
                                                ...arrPopulate[indexV3].populate,
                                                {
                                                    name: nameFieldRef,
                                                    coll: item.coll,
                                                    select: item.name + " ",
                                                    refFrom: item.refFrom,
                                                }
                                            ]
                                        }
                                    })
                                    if (!markPopulate) {
                                        arrPopulate[indexV3].populate.select += item.name + " ";
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (keyword) {
            let key = keyword.split(" ");
            key = '.*' + key.join(".*") + '.*';

            conditionObj.$or = [{
                name: {
                    $regex: key,
                    $options: 'i'
                }
            }, {
                code: {
                    $regex: key,
                    $options: 'i'
                }
            }]
        }

        if (filter && filter.length) {
            if (filter.length > 1) {

                filter.map(filterObj => {
                    if (filterObj.type === 'ref') {
                        const conditionFieldRef = this.getConditionObj(filterObj.ref, filterObj.fieldRefName);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionFieldRef);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionFieldRef
                            };
                        }
                    } else {
                        const conditionByFilter = this.getConditionObj(filterObj);

                        if (condition === 'OR') {
                            conditionObj.$or.push(conditionByFilter);
                        } else {
                            conditionObj = {
                                ...conditionObj,
                                ...conditionByFilter
                            };
                        }
                    }
                });

            } else {
                let {
                    type,
                    ref,
                    fieldRefName
                } = filter[0];

                if (type === 'ref') {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(ref, fieldRefName)
                    };
                } else {
                    conditionObj = {
                        ...conditionObj,
                        ...this.getConditionObj(filter[0])
                    };
                }
            }
        }

        if (conditionObj.$or && !conditionObj.$or.length) {
            delete conditionObj.$or;
        }


        if (!isEmptyObject(objFilterStatic)) {

            if (objFilterStatic.status) {
                if (![1, 2].includes(Number(objFilterStatic.status)) || Number.isNaN(Number(objFilterStatic.status))) {
                    return resolve({
                        error: true,
                        message: "trạng thái không hợp lệ"
                    });
                }
                conditionObj = {
                    ...conditionObj,
                    status: Number(objFilterStatic.status)
                }
            }

        }


        if (arrayItemChecked && arrayItemChecked.length) {
            conditionObj = {
                ...conditionObj,
                _id: {
                    $in: [...arrayItemChecked.map(item => ObjectID(item))]
                }
            }
        }

        let arrayFilter = [{
            $match: {
                ...conditionObj
            }
        }];

        if (arrPopulate.length) {
            arrPopulate.map(item => {
                let collName = pluralize.plural(item.coll);
                let checkPluralColl = collName[collName.length - 1];

                if (checkPluralColl.toLowerCase() != 's') {
                    collName += 's';
                }

                let lookup = [{
                        $lookup: {
                            from: collName,
                            localField: item.name,
                            foreignField: '_id',
                            as: item.name
                        },
                    },
                    {
                        $unwind: {
                            path: "$" + item.name,
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ];
                if (item.populate && item.populate.length) {
                    item.populate.map(populate => {
                        let collNamePopulate = pluralize.plural(populate.coll);
                        let checkPluralColl = collNamePopulate[collNamePopulate.length - 1];

                        if (checkPluralColl.toLowerCase() != 's') {
                            collNamePopulate += 's';
                        }

                        lookup = [
                            ...lookup,
                            {
                                $lookup: {
                                    from: collNamePopulate,
                                    localField: item.name + "." + populate.name,
                                    foreignField: '_id',
                                    as: populate.name
                                },
                            },
                            {
                                $unwind: {
                                    path: "$" + populate.name,
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ]
                    })
                }
                arrayFilter = [
                    ...arrayFilter,
                    ...lookup
                ]
            });
        }
        let sort = {
            modifyAt: -1
        };

        if (field && dir) {
            if (dir == 'asc') {
                sort = {
                    [field]: 1
                }
            } else {
                sort = {
                    [field]: -1
                }
            }
        }

        arrayFilter = [
            ...arrayFilter,
            {
                $sort: sort
            }
        ]



        return {
            arrayFilter,
            arrayItemCustomerChoice,
            refParent,
            arrayFieldIDChoice
        };
    }

    /**
     * Lấy thông tin user_register_trial_program
     * @param {objectId} user_register_trial_programID
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    getInfoUser_register_trial_program({
        user_register_trial_programID,
        select,
        filter = {},
        explain,
        authorID
    }) {
        return new Promise(async resolve => {
            try {
                let fieldsSelected = '';
                let fieldsReference = [];
                let conditionObj = {
                    employee: authorID
                };

                if (!checkObjectIDs([user_register_trial_programID])) {
                    return resolve({
                        error: true,
                        message: 'Giá trị user_register_trial_programID không hợp lệ',
                        status: 400
                    });
                }

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                Object.keys(filter).map(key => {
                    if (![].includes(key)) {
                        delete filter[key];
                    }
                });

                let {} = filter;


                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['trialProgram', 'products', 'fields', 'store', 'employee', 'lastest_trial_program_history_contact'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let infoUser_register_trial_program = await USER_REGISTER_TRIAL_PROGRAM_COLL
                    .findOne({
                        _id: user_register_trial_programID,
                        ...conditionObj,
                    })
                    .select(fieldsSelected)
                    .lean();

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        infoUser_register_trial_program = await USER_REGISTER_TRIAL_PROGRAM_COLL.populate(infoUser_register_trial_program, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            infoUser_register_trial_program = await USER_REGISTER_TRIAL_PROGRAM_COLL.populate(infoUser_register_trial_program, `${ref}.${field}`);
                        }
                    }
                }

                if (!infoUser_register_trial_program) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý thông tin user_register_trial_program',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoUser_register_trial_program,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Lấy danh sách user_register_trial_program
     * @param {object} filter
     * @param {object} sort
     * @param {string} explain
     * @param {string} select
     * @param {string} search
     * @param {number} limit
     * @param {number} page
     * @extends {BaseModel}
     * @returns {{ 
     *   error: boolean, 
     *   data?: {
     *      records: array,
     *      totalRecord: number,
     *      totalPage: number,
     *      limit: number,
     *      page: number
     *   },
     *   message?: string,
     *   status: number
     * }}
     */
    getListUser_register_trial_programs({
        search,
        select,
        explain,
        filter = {},
        sort = {},
        limit = 25,
        page,
        authorID
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {
                    employee: authorID
                };
                let sortBy = {};
                let objSort = {};
                let fieldsSelected = '';
                let fieldsReference = [];

                limit = isNaN(limit) ? 25 : +limit;
                page = isNaN(page) ? 1 : +page;

                if (filter && typeof filter === 'string') {
                    if (!IsJsonString(filter))
                        return resolve({
                            error: true,
                            message: 'Request params filter invalid',
                            status: 400
                        });

                    filter = JSON.parse(filter);
                }

                if (sort && typeof sort === 'string') {
                    if (!IsJsonString(sort))
                        return resolve({
                            error: true,
                            message: 'Request params sort invalid',
                            status: 400
                        });

                    sort = JSON.parse(sort);
                } else {

                }

                // SEARCH TEXT
                if (search) {
                    conditionObj.$text = {
                        $search: search
                    };
                    objSort.score = {
                        $meta: "textScore"
                    };
                    sortBy.score = {
                        $meta: "textScore"
                    };
                }

                Object.keys(filter).map(key => {
                    if (!['trialProgram', 'product', 'fields', 'store', 'employee', 'status', 'lastest_status_contact'].includes(key)) {
                        delete filter[key];
                    }
                });

                let {
                    trialProgram,
                    product,
                    fields,
                    store,
                    employee,
                    status,
                    lastest_status_contact,
                } = filter;

                trialProgram && (conditionObj.trialProgram = trialProgram);

                product && (conditionObj.product = product);

                if (fields && fields.length) {
                    conditionObj.fields = {
                        $in: fields
                    };
                }

                store && (conditionObj.store = store);

                employee && (conditionObj.employee = employee);
                status && (conditionObj.status = status);

                /**
                 * check trạng thái của NA call khách hàng
                 * 100: là giá trị không tồn tại lastest_status_contact
                 */
                if (lastest_status_contact) {
                    if (+lastest_status_contact == 100) {
                        conditionObj.lastest_status_contact = {
                            $exists: false
                        }
                    } else {
                        conditionObj.lastest_status_contact = lastest_status_contact
                    }
                }
                if (explain) {
                    explain = explain.split(',');

                    explain.map((populate, index) => {
                        let [ref] = populate.split('__');

                        if (!['trialProgram', 'product', 'fields', 'store', 'employee'].includes(ref)) {
                            explain.splice(index, 1);
                        } else {
                            fieldsReference = [...fieldsReference, {
                                path: ref
                            }]
                        }
                    })
                }

                if (select) {
                    select = select.split(',');
                    fieldsReference = [];

                    select.map((fieldSelect, index) => {
                        let refField = fieldSelect.split('__');

                        if (refField.length === 2) {
                            if (explain && !explain.includes(refField[0])) {
                                select.splice(index, 1);
                            } else {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                    select: refField[1]
                                }]
                            }
                        } else {
                            if (explain && explain.includes(refField[0])) {
                                fieldsReference = [...fieldsReference, {
                                    path: refField[0],
                                }]
                            }

                            fieldsSelected += `${fieldSelect} `;
                        }

                    })
                }

                let listUser_register_trial_programs = await USER_REGISTER_TRIAL_PROGRAM_COLL
                    .find({
                        ...conditionObj,
                    }, objSort)
                    .select(fieldsSelected)
                    .limit(limit)
                    .skip((page * limit) - limit)
                    .sort({
                        ...sort,
                        ...sortBy
                    })
                    .lean()

                if (fieldsReference && fieldsReference.length) {
                    fieldsReference = lodash.chain(fieldsReference)
                        .groupBy("path")
                        .map((select, path) => ({
                            path,
                            select: select.map(item => item.select).join(' ')
                        }))
                        .value();

                    for (const refObj of fieldsReference) {
                        listUser_register_trial_programs = await USER_REGISTER_TRIAL_PROGRAM_COLL.populate(listUser_register_trial_programs, {
                            path: refObj.path,
                            select: refObj.select,
                        })
                    }

                    for (const populate of explain) {
                        let [ref, field] = populate.split('__');

                        if (field) {
                            listUser_register_trial_programs = await USER_REGISTER_TRIAL_PROGRAM_COLL.populate(listUser_register_trial_programs, `${ref}.${field}`);
                        }
                    }
                }

                if (!listUser_register_trial_programs) {
                    return resolve({
                        error: true,
                        message: 'Không thế lâý danh sách user_register_trial_program',
                        status: 400
                    });
                }

                let totalRecord = await USER_REGISTER_TRIAL_PROGRAM_COLL.countDocuments(conditionObj);
                let totalPage = Math.ceil(totalRecord / limit);

                return resolve({
                    error: false,
                    data: {
                        records: listUser_register_trial_programs,
                        totalRecord,
                        totalPage,
                        limit,
                        page
                    },
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
                     * Tạo user_register_trial_program
                     	* @param {string} deviceID
	* @param {string} fullname
	* @param {string} email
	* @param {string} phone
	* @param {string} address
	* @param {string} cityNumber
	* @param {string} cityText
	* @param {string} districtNumber
	* @param {string} districtText
	* @param {string} wardNumber
	* @param {string} wardText
	* @param {string} fullAddress
	* @param {string} note

                     * @param {objectId} userCreate
                     * @this {BaseModel}
                     * @extends {BaseModel}
                     * @returns {{ error: boolean, data?: object, message?: string }}
                     */
    insertUser_register_trial_program({
        trialProgram,
        product,
        status,
        deviceID,
        fullname,
        email,
        phone,
        address,
        cityNumber,
        cityText,
        districtNumber,
        districtText,
        wardNumber,
        wardText,
        fullAddress,
        note,
        fields,
        store,
        employee,
        userCreate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([userCreate]))
                    return resolve({
                        error: true,
                        message: 'Người tạo không hợp lệ',
                        status: 400
                    });



                if (phone && !checkPhoneNumber(phone)) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại không đúng định dạng',
                        status: 400
                    });
                }

                if (fields && !checkObjectIDs(fields)) {
                    return resolve({
                        error: true,
                        message: 'fields không hợp lệ',
                        status: 400
                    });
                }

                if (store && !checkObjectIDs(store)) {
                    return resolve({
                        error: true,
                        message: 'store không hợp lệ',
                        status: 400
                    });
                }

                if (employee && !checkObjectIDs(employee)) {
                    return resolve({
                        error: true,
                        message: 'employee không hợp lệ',
                        status: 400
                    });
                }

                let dataInsert = {
                    trialProgram,
                    product,
                    status,
                    deviceID,
                    fullname,
                    email,
                    phone,
                    address,
                    cityNumber,
                    cityText,
                    districtNumber,
                    districtText,
                    wardNumber,
                    wardText,
                    fullAddress,
                    note,
                    fields,
                    store,
                    employee,
                    userCreate
                };
                dataInsert = cleanObject(dataInsert);
                let infoAfterInsert = await this.insertData(dataInsert);


                if (!infoAfterInsert) {
                    return resolve({
                        error: true,
                        message: 'Tạo USER_REGISTER_TRIAL_PROGRAM thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterInsert,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
                     * Cập nhật user_register_trial_program
                     	* @param {string} deviceID
	* @param {string} fullname
	* @param {string} email
	* @param {string} phone
	* @param {string} address
	* @param {string} cityNumber
	* @param {string} cityText
	* @param {string} districtNumber
	* @param {string} districtText
	* @param {string} wardNumber
	* @param {string} wardText
	* @param {string} fullAddress
	* @param {string} note

                     * @param {objectId} user_register_trial_programID
                     * @param {objectId} userUpdate
                     * @this {BaseModel}
                     * @extends {BaseModel}
                     * @returns {{ error: boolean, data?: object, message?: string }}
                     */
    updateUser_register_trial_program({
        user_register_trial_programID,
        trialProgram,
        product,
        status,
        deviceID,
        fullname,
        email,
        phone,
        address,
        cityNumber,
        cityText,
        districtNumber,
        districtText,
        wardNumber,
        wardText,
        fullAddress,
        note,
        fields,
        store,
        employee,
        userUpdate
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([user_register_trial_programID]))
                    return resolve({
                        error: true,
                        message: 'user_register_trial_programID không hợp lệ',
                        status: 400
                    });

                if (!checkObjectIDs([userUpdate]))
                    return resolve({
                        error: true,
                        message: 'ID user cập nhật không hợp lệ',
                        status: 400
                    });


                if (phone && !checkPhoneNumber(phone)) {
                    return resolve({
                        error: true,
                        message: 'Số điện thoại không đúng định dạng',
                        status: 400
                    });
                }

                if (fields && !checkObjectIDs(fields)) {
                    return resolve({
                        error: true,
                        message: 'fields không hợp lệ',
                        status: 400
                    });
                }

                if (store && !checkObjectIDs(store)) {
                    return resolve({
                        error: true,
                        message: 'store không hợp lệ',
                        status: 400
                    });
                }

                if (employee && !checkObjectIDs(employee)) {
                    return resolve({
                        error: true,
                        message: 'employee không hợp lệ',
                        status: 400
                    });
                }

                const checkExists = await USER_REGISTER_TRIAL_PROGRAM_COLL.findOne({
                    _id: user_register_trial_programID,
                    state: 1
                });
                if (!checkExists) {
                    return resolve({
                        error: true,
                        message: 'user_register_trial_program không tồn tại',
                        status: 404
                    });
                }

                let dataUpdate = {
                    userUpdate
                };
                trialProgram && (dataUpdate.trialProgram = trialProgram);
                product && (dataUpdate.product = product);
                status && (dataUpdate.status = status);
                deviceID && (dataUpdate.deviceID = deviceID);
                fullname && (dataUpdate.fullname = fullname);
                email && (dataUpdate.email = email);
                phone && (dataUpdate.phone = phone);
                address && (dataUpdate.address = address);
                cityNumber && (dataUpdate.cityNumber = cityNumber);
                cityText && (dataUpdate.cityText = cityText);
                districtNumber && (dataUpdate.districtNumber = districtNumber);
                districtText && (dataUpdate.districtText = districtText);
                wardNumber && (dataUpdate.wardNumber = wardNumber);
                wardText && (dataUpdate.wardText = wardText);
                fullAddress && (dataUpdate.fullAddress = fullAddress);
                note && (dataUpdate.note = note);
                fields && (dataUpdate.fields = fields);
                store && (dataUpdate.store = store);
                employee && (dataUpdate.employee = employee);
                let infoAfterUpdate = await this.updateWhereClause({
                    _id: user_register_trial_programID
                }, dataUpdate);

                if (!infoAfterUpdate) {
                    return resolve({
                        error: true,
                        message: 'Cập nhật thất bại',
                        status: 400
                    });
                }

                return resolve({
                    error: false,
                    data: infoAfterUpdate,
                    status: 200
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa user_register_trial_program
     * @param {objectId} user_register_trial_programID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteUser_register_trial_program(user_register_trial_programID, authorID) {
        return new Promise(async resolve => {
            try {
                let ids = [user_register_trial_programID];

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị user_register_trial_programID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.removeManyWhere({
                    _id: {
                        $in: ids
                    },
                    userCreate: authorID
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

    /**
     * Xóa user_register_trial_program
     * @param {objectId} user_register_trial_programID
     * @this {BaseModel}
     * @extends {BaseModel}
     * @returns {{ error: boolean, data?: object, message?: string }}
     */
    deleteUser_register_trial_programs(user_register_trial_programsID, authorID) {
        return new Promise(async resolve => {
            try {
                let ids = user_register_trial_programsID.split(',');

                if (!checkObjectIDs(ids)) {
                    return resolve({
                        error: true,
                        message: 'Giá trị user_register_trial_programsID không hợp lệ',
                        status: 400
                    });
                }

                const infoAfterDelete = await this.removeManyWhere({
                    _id: {
                        $in: ids
                    },
                    userCreate: authorID
                });

                return resolve({
                    error: false,
                    data: infoAfterDelete,
                    status: 204
                });
            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message,
                    status: 500
                });
            }
        })
    }

}

exports.MODEL = new Model;

/**
 * COLLECTIONS
 */
var USER_REGISTER_TRIAL_PROGRAM_COLL = require('../databases/user_register_trial_program-coll');

var TRIAL_PROGRAM_COLL = require('../databases/trial_program-coll');

var {
    PRODUCT_COLL
} = require('../../product');

var USER_FIELDS_REGISTER_TRIAL_PROGRAM_COLL = require('../databases/user_fields_register_trial_program-coll');

var {
    STORE_COLL
} = require('../../store');

var {
    EMPLOYEE_COLL
} = require('../../employee');