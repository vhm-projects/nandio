const BASE_ROUTE = '/field_config_register_trial_program';
const API_BASE_ROUTE = '/api/field_config_register_trial_program';

const CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM = {
    ADD_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/add-field_config_register_trial_program`,
    UPDATE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_BY_ID: `${BASE_ROUTE}/update-field_config_register_trial_program-by-id`,
    DELETE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_BY_ID: `${BASE_ROUTE}/delete/:field_config_register_trial_programID`,

    GET_INFO_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_BY_ID: `${BASE_ROUTE}/info/:field_config_register_trial_programID`,
    GET_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/list-field_config_register_trial_program`,
    GET_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_BY_FIELD: `${BASE_ROUTE}/list-field_config_register_trial_program/:field/:value`,
    GET_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_SERVER_SIDE: `${BASE_ROUTE}/list-field_config_register_trial_program-server-side`,

    UPDATE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-field_config_register_trial_program-by-id-v2`,
    DELETE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_BY_LIST_ID: `${BASE_ROUTE}/delete-field_config_register_trial_program-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_EXCEL: `${BASE_ROUTE}/list-field_config_register_trial_program-excel`,
    DOWNLOAD_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-field_config_register_trial_program-excel-export`,

    // IMPORT EXCEL
    GET_LIST_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_IMPORT: `${BASE_ROUTE}/list-field_config_register_trial_program-import`,
    SETTING_FILE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-field_config_register_trial_program-import-setting`,
    DOWNLOAD_FILE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_EXCEL_IMPORT: `${BASE_ROUTE}/list-field_config_register_trial_program-import-dowload`,
    CREATE_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_IMPORT_EXCEL: `${BASE_ROUTE}/create-field_config_register_trial_program-import-excel`,



    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM = CF_ROUTINGS_FIELD_CONFIG_REGISTER_TRIAL_PROGRAM;