/**
 * Trạng Thái 
 * 1: Liên Hệ Thất Bại
 * 2: Liên Hệ Đặt Lịch Thành Công
 * 3: Yêu Cầu Huỷ Đơn
 * 4: Đổi Quà Thất Bại
 * 5: Đổi Quà Thành Công
 */
exports.STATUS_TRIAL_PROGRAM_HISTORY_CONTACT_TYPE = {

    "1": {
        value: "Liên Hệ Thất Bại",
        color: "#ff2600"
    },

    "2": {
        value: "Liên Hệ Đặt Lịch Thành Công",
        color: "#00f900"
    },

    "3": {
        value: "Yêu Cầu Huỷ Đơn",
        color: "#ff9300"
    },

    "4": {
        value: "Đổi Quà Thất Bại",
        color: "#ff2600"
    },

    "5": {
        value: "Đổi Quà Thành Công",
        color: "#0b51b7"
    },

};