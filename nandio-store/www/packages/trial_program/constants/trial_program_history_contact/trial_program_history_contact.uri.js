const BASE_ROUTE = '/trial_program_history_contact';
const API_BASE_ROUTE = '/api/trial_program_history_contact';

const CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT = {
    ADD_TRIAL_PROGRAM_HISTORY_CONTACT: `${BASE_ROUTE}/add-trial_program_history_contact`,
    UPDATE_TRIAL_PROGRAM_HISTORY_CONTACT_BY_ID: `${BASE_ROUTE}/update-trial_program_history_contact-by-id`,
    DELETE_TRIAL_PROGRAM_HISTORY_CONTACT_BY_ID: `${BASE_ROUTE}/delete/:trial_program_history_contactID`,

    GET_INFO_TRIAL_PROGRAM_HISTORY_CONTACT_BY_ID: `${BASE_ROUTE}/info/:trial_program_history_contactID`,
    GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACT: `${BASE_ROUTE}/list-trial_program_history_contact`,
    GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACT_BY_FIELD: `${BASE_ROUTE}/list-trial_program_history_contact/:field/:value`,
    GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACT_SERVER_SIDE: `${BASE_ROUTE}/list-trial_program_history_contact-server-side`,

    UPDATE_TRIAL_PROGRAM_HISTORY_CONTACT_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-trial_program_history_contact-by-id-v2`,
    DELETE_TRIAL_PROGRAM_HISTORY_CONTACT_BY_LIST_ID: `${BASE_ROUTE}/delete-trial_program_history_contact-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACT_EXCEL: `${BASE_ROUTE}/list-trial_program_history_contact-excel`,
    DOWNLOAD_LIST_TRIAL_PROGRAM_HISTORY_CONTACT_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-trial_program_history_contact-excel-export`,

    // IMPORT EXCEL
    GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACT_IMPORT: `${BASE_ROUTE}/list-trial_program_history_contact-import`,
    SETTING_FILE_TRIAL_PROGRAM_HISTORY_CONTACT_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-trial_program_history_contact-import-setting`,
    DOWNLOAD_FILE_TRIAL_PROGRAM_HISTORY_CONTACT_EXCEL_IMPORT: `${BASE_ROUTE}/list-trial_program_history_contact-import-dowload`,
    CREATE_TRIAL_PROGRAM_HISTORY_CONTACT_IMPORT_EXCEL: `${BASE_ROUTE}/create-trial_program_history_contact-import-excel`,

    API_GET_INFO_TRIAL_PROGRAM_HISTORY_CONTACT_OF_ME: `${API_BASE_ROUTE}/info-trial_program_history_contact-of-me/:trial_program_history_contactID`,
    API_GET_LIST_TRIAL_PROGRAM_HISTORY_CONTACTS_OF_ME: `${API_BASE_ROUTE}/list-trial_program_history_contacts-of-me`,
    API_ADD_TRIAL_PROGRAM_HISTORY_CONTACT_OF_ME: `${API_BASE_ROUTE}/add-trial_program_history_contact-of-me`,
    API_UPDATE_TRIAL_PROGRAM_HISTORY_CONTACT_OF_ME: `${API_BASE_ROUTE}/update-trial_program_history_contact-of-me/:trial_program_history_contactID`,
    API_DELETE_TRIAL_PROGRAM_HISTORY_CONTACT_OF_ME: `${API_BASE_ROUTE}/delete-trial_program_history_contact-of-me/:trial_program_history_contactID`,
    API_DELETE_TRIAL_PROGRAM_HISTORY_CONTACTS_OF_ME: `${API_BASE_ROUTE}/delete-trial_program_history_contacts-of-me/:trial_program_history_contactsID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT = CF_ROUTINGS_TRIAL_PROGRAM_HISTORY_CONTACT;