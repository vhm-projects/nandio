const BASE_ROUTE = '/user_fields_register_trial_program';
const API_BASE_ROUTE = '/api/user_fields_register_trial_program';

const CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM = {
    ADD_USER_FIELDS_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/add-user_fields_register_trial_program`,
    UPDATE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_BY_ID: `${BASE_ROUTE}/update-user_fields_register_trial_program-by-id`,
    DELETE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_BY_ID: `${BASE_ROUTE}/delete/:user_fields_register_trial_programID`,

    GET_INFO_USER_FIELDS_REGISTER_TRIAL_PROGRAM_BY_ID: `${BASE_ROUTE}/info/:user_fields_register_trial_programID`,
    GET_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/list-user_fields_register_trial_program`,
    GET_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM_BY_FIELD: `${BASE_ROUTE}/list-user_fields_register_trial_program/:field/:value`,
    GET_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM_SERVER_SIDE: `${BASE_ROUTE}/list-user_fields_register_trial_program-server-side`,

    UPDATE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-user_fields_register_trial_program-by-id-v2`,
    DELETE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_BY_LIST_ID: `${BASE_ROUTE}/delete-user_fields_register_trial_program-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM_EXCEL: `${BASE_ROUTE}/list-user_fields_register_trial_program-excel`,
    DOWNLOAD_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-user_fields_register_trial_program-excel-export`,

    // IMPORT EXCEL
    GET_LIST_USER_FIELDS_REGISTER_TRIAL_PROGRAM_IMPORT: `${BASE_ROUTE}/list-user_fields_register_trial_program-import`,
    SETTING_FILE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-user_fields_register_trial_program-import-setting`,
    DOWNLOAD_FILE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_EXCEL_IMPORT: `${BASE_ROUTE}/list-user_fields_register_trial_program-import-dowload`,
    CREATE_USER_FIELDS_REGISTER_TRIAL_PROGRAM_IMPORT_EXCEL: `${BASE_ROUTE}/create-user_fields_register_trial_program-import-excel`,



    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM = CF_ROUTINGS_USER_FIELDS_REGISTER_TRIAL_PROGRAM;