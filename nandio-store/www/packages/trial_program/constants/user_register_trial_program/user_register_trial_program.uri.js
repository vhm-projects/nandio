const BASE_ROUTE = '/user_register_trial_program';
const API_BASE_ROUTE = '/api/user_register_trial_program';

const CF_ROUTINGS_USER_REGISTER_TRIAL_PROGRAM = {
    ADD_USER_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/add-user_register_trial_program`,
    UPDATE_USER_REGISTER_TRIAL_PROGRAM_BY_ID: `${BASE_ROUTE}/update-user_register_trial_program-by-id`,
    DELETE_USER_REGISTER_TRIAL_PROGRAM_BY_ID: `${BASE_ROUTE}/delete/:user_register_trial_programID`,

    GET_INFO_USER_REGISTER_TRIAL_PROGRAM_BY_ID: `${BASE_ROUTE}/info/:user_register_trial_programID`,
    GET_LIST_USER_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/list-user_register_trial_program`,
    GET_LIST_USER_REGISTER_TRIAL_PROGRAM_BY_FIELD: `${BASE_ROUTE}/list-user_register_trial_program/:field/:value`,
    GET_LIST_USER_REGISTER_TRIAL_PROGRAM_SERVER_SIDE: `${BASE_ROUTE}/list-user_register_trial_program-server-side`,

    UPDATE_USER_REGISTER_TRIAL_PROGRAM_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-user_register_trial_program-by-id-v2`,
    DELETE_USER_REGISTER_TRIAL_PROGRAM_BY_LIST_ID: `${BASE_ROUTE}/delete-user_register_trial_program-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_USER_REGISTER_TRIAL_PROGRAM_EXCEL: `${BASE_ROUTE}/list-user_register_trial_program-excel`,
    DOWNLOAD_LIST_USER_REGISTER_TRIAL_PROGRAM_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-user_register_trial_program-excel-export`,

    // IMPORT EXCEL
    GET_LIST_USER_REGISTER_TRIAL_PROGRAM_IMPORT: `${BASE_ROUTE}/list-user_register_trial_program-import`,
    SETTING_FILE_USER_REGISTER_TRIAL_PROGRAM_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-user_register_trial_program-import-setting`,
    DOWNLOAD_FILE_USER_REGISTER_TRIAL_PROGRAM_EXCEL_IMPORT: `${BASE_ROUTE}/list-user_register_trial_program-import-dowload`,
    CREATE_USER_REGISTER_TRIAL_PROGRAM_IMPORT_EXCEL: `${BASE_ROUTE}/create-user_register_trial_program-import-excel`,

    API_GET_INFO_USER_REGISTER_TRIAL_PROGRAM_OF_ME: `${API_BASE_ROUTE}/info-user_register_trial_program-of-me/:user_register_trial_programID`,
    API_GET_LIST_USER_REGISTER_TRIAL_PROGRAMS_OF_ME: `${API_BASE_ROUTE}/list-user_register_trial_programs-of-me`,
    API_ADD_USER_REGISTER_TRIAL_PROGRAM_OF_ME: `${API_BASE_ROUTE}/add-user_register_trial_program-of-me`,
    API_UPDATE_USER_REGISTER_TRIAL_PROGRAM_OF_ME: `${API_BASE_ROUTE}/update-user_register_trial_program-of-me/:user_register_trial_programID`,
    API_DELETE_USER_REGISTER_TRIAL_PROGRAM_OF_ME: `${API_BASE_ROUTE}/delete-user_register_trial_program-of-me/:user_register_trial_programID`,
    API_DELETE_USER_REGISTER_TRIAL_PROGRAMS_OF_ME: `${API_BASE_ROUTE}/delete-user_register_trial_programs-of-me/:user_register_trial_programsID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_USER_REGISTER_TRIAL_PROGRAM = CF_ROUTINGS_USER_REGISTER_TRIAL_PROGRAM;