const BASE_ROUTE = '/trial_program';
const API_BASE_ROUTE = '/api/trial_program';

const CF_ROUTINGS_TRIAL_PROGRAM = {
    ADD_TRIAL_PROGRAM: `${BASE_ROUTE}/add-trial_program`,
    UPDATE_TRIAL_PROGRAM_BY_ID: `${BASE_ROUTE}/update-trial_program-by-id`,
    DELETE_TRIAL_PROGRAM_BY_ID: `${BASE_ROUTE}/delete/:trial_programID`,

    GET_INFO_TRIAL_PROGRAM_BY_ID: `${BASE_ROUTE}/info/:trial_programID`,
    GET_LIST_TRIAL_PROGRAM: `${BASE_ROUTE}/list-trial_program`,
    GET_LIST_TRIAL_PROGRAM_BY_FIELD: `${BASE_ROUTE}/list-trial_program/:field/:value`,
    GET_LIST_TRIAL_PROGRAM_SERVER_SIDE: `${BASE_ROUTE}/list-trial_program-server-side`,

    UPDATE_TRIAL_PROGRAM_NOT_REQUIRE_BY_ID: `${BASE_ROUTE}/update-trial_program-by-id-v2`,
    DELETE_TRIAL_PROGRAM_BY_LIST_ID: `${BASE_ROUTE}/delete-trial_program-by-list-id`,

    // EXPORT EXCEL
    GET_LIST_TRIAL_PROGRAM_EXCEL: `${BASE_ROUTE}/list-trial_program-excel`,
    DOWNLOAD_LIST_TRIAL_PROGRAM_EXCEL_EXPORT: `${BASE_ROUTE}/dowload-trial_program-excel-export`,

    // IMPORT EXCEL
    GET_LIST_TRIAL_PROGRAM_IMPORT: `${BASE_ROUTE}/list-trial_program-import`,
    SETTING_FILE_TRIAL_PROGRAM_EXCEL_IMPORT_PREVIEW: `${BASE_ROUTE}/list-trial_program-import-setting`,
    DOWNLOAD_FILE_TRIAL_PROGRAM_EXCEL_IMPORT: `${BASE_ROUTE}/list-trial_program-import-dowload`,
    CREATE_TRIAL_PROGRAM_IMPORT_EXCEL: `${BASE_ROUTE}/create-trial_program-import-excel`,

    API_GET_INFO_TRIAL_PROGRAM_OF_ME: `${API_BASE_ROUTE}/info-trial_program-of-me/:trial_programID`,
    API_GET_LIST_TRIAL_PROGRAMS_OF_ME: `${API_BASE_ROUTE}/list-trial_programs-of-me`,
    API_ADD_TRIAL_PROGRAM_OF_ME: `${API_BASE_ROUTE}/add-trial_program-of-me`,
    API_UPDATE_TRIAL_PROGRAM_OF_ME: `${API_BASE_ROUTE}/update-trial_program-of-me/:trial_programID`,
    API_DELETE_TRIAL_PROGRAM_OF_ME: `${API_BASE_ROUTE}/delete-trial_program-of-me/:trial_programID`,
    API_DELETE_TRIAL_PROGRAMS_OF_ME: `${API_BASE_ROUTE}/delete-trial_programs-of-me/:trial_programsID`,

    API_PUSH_NOTI_WHEN_USER_REGISTER_TRAIL_PROGRAM: `${API_BASE_ROUTE}/push-noti-when-user-register-trial-program/:employeeID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_TRIAL_PROGRAM = CF_ROUTINGS_TRIAL_PROGRAM;