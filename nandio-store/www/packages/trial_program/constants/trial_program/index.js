/**
 *  
 * 1: Hoạt động
 * 0: Không hoạt động
 * 2: Đã Xoá
 */
exports.STATUS_TRIAL_PROGRAM_TYPE = {

    "1": {
        value: "Hoạt động",
        color: "#0b51b7"
    },

    "0": {
        value: "Không hoạt động",
        color: "#d63031"
    },

    "2": {
        value: "Đã Xoá",
        color: "#c4bc00"
    },

};