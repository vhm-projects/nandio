const USER_REGISTER_TRIAL_PROGRAM_COLL = require('./databases/user_register_trial_program-coll');
const USER_REGISTER_TRIAL_PROGRAM_MODEL = require('./models/user_register_trial_program').MODEL;
const USER_REGISTER_TRIAL_PROGRAM_ROUTES = require('./apis/user_register_trial_program');

const USER_FIELDS_REGISTER_TRIAL_PROGRAM_COLL  = require('./databases/user_fields_register_trial_program-coll');
const USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL  = require('./models/user_fields_register_trial_program').MODEL;
const USER_FIELDS_REGISTER_TRIAL_PROGRAM_ROUTES  = require('./apis/user_fields_register_trial_program');
    

const FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_COLL  = require('./databases/field_config_register_trial_program-coll');
const FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL  = require('./models/field_config_register_trial_program').MODEL;
const FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_ROUTES  = require('./apis/field_config_register_trial_program');
    

const TRIAL_PROGRAM_HISTORY_CONTACT_COLL  = require('./databases/trial_program_history_contact-coll');
const TRIAL_PROGRAM_HISTORY_CONTACT_MODEL  = require('./models/trial_program_history_contact').MODEL;
const TRIAL_PROGRAM_HISTORY_CONTACT_ROUTES  = require('./apis/trial_program_history_contact');
    
// MARK REQUIRE

module.exports = {
    USER_REGISTER_TRIAL_PROGRAM_COLL,
    USER_REGISTER_TRIAL_PROGRAM_MODEL,
    USER_REGISTER_TRIAL_PROGRAM_ROUTES,

    USER_FIELDS_REGISTER_TRIAL_PROGRAM_COLL,
    USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL,
    USER_FIELDS_REGISTER_TRIAL_PROGRAM_ROUTES,
    

    FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_COLL,
    FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL,
    FIELD_CONFIG_REGISTER_TRIAL_PROGRAM_ROUTES,
    

    TRIAL_PROGRAM_HISTORY_CONTACT_COLL,
    TRIAL_PROGRAM_HISTORY_CONTACT_MODEL,
    TRIAL_PROGRAM_HISTORY_CONTACT_ROUTES,
    
    // MARK EXPORT
}