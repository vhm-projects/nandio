"use strict";

const Schema	= require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION USER CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('user_store', {
	username: {
		type: String,
		trim: true,
		unique: true,
		require: true
	},
	fullname: {
		type: String,
		trim: true,
		unique: true
	},
	email: {
		type: String,
		trim: true,
		unique: true,
		require: true
	},
	password: {
		type: String
	},
	/**
	 * Trạng thái hoạt động.
	 * -1. Khóa
	 * 1. Hoạt động
	 */
	status: {
		type: Number,
		default: 1
	},
	language: {
		type: String,
		default: "vi"
	},
	region: {
		type:  Schema.Types.ObjectId,
		ref : 'region'
	},
	/**
	 * RBAC
	 */
	roles: [{
		type:  Schema.Types.ObjectId,
		ref : 'role_base'
	}],
	/**
	 * Directly permission
	 */
	permissions: [{
		type:  Schema.Types.ObjectId,
		ref : 'api_scope'
	}],
});
