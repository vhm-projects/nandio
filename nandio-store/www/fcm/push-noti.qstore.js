let FCM             = require('fcm-notification');
// let K360_CONFIG_FCM = null, fcm = null;
// let { database_product }      = require('../config/cf_mode');
// if (database_product) {
// 	K360_CONFIG_FCM = require('./config.json'); //prod 
// 	fcm             = new FCM(K360_CONFIG_FCM)
// }
let fcm             = null;
let QSTORE_CONFIG_FCM = require('./qstore_sdk.json'); //prod 
if (process.env.NODE_ENV == 'production') {
	fcm             = new FCM(QSTORE_CONFIG_FCM, 'QSTORE'); //check tại file: qshop/node_modules/fcm-notification/lib/fcm.js để cập nhật thêm name
} else {
	let QSTORE_TEST_FCM = require('./qshop-qstore-staging.json')
	fcm 			= new FCM(QSTORE_TEST_FCM, 'QSTORE')
}

// WORKED
exports.sendMessage = function ({ title, description, arrayRegistrationID, body }) {
	console.log(`run: sendMessage...........`)
	return new Promise(resolve => {
		let tokens 	= Array.isArray(arrayRegistrationID) ? arrayRegistrationID : [arrayRegistrationID]
		let row 	= JSON.stringify(body)
	
		if(!title){
			title = 'QSTORE - CRM YẾN XÀO THIÊN VIỆT'
		}
		let mapMessage = {
			data: {    //This is only optional, you can send any data
				row: row
			},
			notification: {
				title,
				body: description
			}
		};
		fcm.sendToMultipleToken(mapMessage, tokens, function (err, response) {
			if (err) {
				return ({
					error : true,
					message : 'unable_to_send_message'
				});
			} else {
				console.log(response);
				return resolve({
					error: false,
					message: response
				});
			}
		});
	
	})
}
// let tokenNguyen = 'dICILntvRsakamlOZzuyrA:APA91bGClrDjieiWJYJ_49sWyd7fWE6RLg3vTysSiiQJXL1pcoeOFjfq1E49-Hg2mqY2YbrCcC4lSGOanqoZuJqtQwH3pSjiOk2ZyjtxbsikgkmZ6FI6t0H6jjtBijSAStau4DnYViPY';
// let body = { 
// 	screen_key: 'ImageDealScreen',
// 	sender: 'KHANHNEY'
// }
// sendMessage({ title: 'K360 - EVERYONE', description:'HELLO NGUYEN', arrayRegistrationID: tokenNguyen, body })
// 	.then(result => console.log({ result }))
// 	.catch(err => console.log({ err }))