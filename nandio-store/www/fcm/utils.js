let EMPLOYEE_DEVICE_COLL         = require('../packages/employee/databases/employee_device-coll');
let { sendMessage: sendMessageQStore }       = require('./push-noti.qstore');

function sendMessageMobile({ title, description, arrReceiverID, body }){
    ( async() =>{
        for (const receiver of arrReceiverID){
            // tìm registrationID và gửi thông báo đến mobile
            // if(receiver != userID){
                let listEmployeeDevice    = await EMPLOYEE_DEVICE_COLL.find({ employee: receiver });
                if (listEmployeeDevice && listEmployeeDevice.length) {
                    listEmployeeDevice = listEmployeeDevice.filter(item => item.registrationID);
                    let  arrayRegistrationID  = listEmployeeDevice.map(item=> {
                        if (item.registrationID) {
                            return item.registrationID
                        }
                    });
                    sendMessageQStore({ title,  description, arrayRegistrationID, body })
                        .then(resultSendMessage => console.log({ notiCloudMessaging: resultSendMessage }))
                        .catch(err => console.log({ err }))
                }
            // }
        }
    })();
}

exports.sendMessageMobile = sendMessageMobile;
// ----------------PLAYGROUND-------------------//
// let arrReceiverID = ['60ab5729b436165fe06fd141'];
// let title = 'HELLO WORLD - K360';
// let description = 'CHAY NGAY DI';
// let senderID = 'KHANHNEY'
// let body = {
//     screen_key: 'Setting',
//     sender: senderID,
//     transactionID: 'abc'
// };

// sendMessageMobile({ title, description, arrReceiverID, senderID, body })