const fastcsv = require("fast-csv");
const fs = require('fs');

const REGION_COLL = require('../www/packages/region_area/databases/region-coll');
const AREA_COLL = require('../www/packages/region_area/databases/area-coll');
const DISTRIBUTOR_COLL = require('../www/packages/distributor/databases/distributor-coll');

let stream = fs.createReadStream('./import_ams/shop.csv');

let csvData = [];
let numberRow = 0;

let csvStream = fastcsv
  .parse()
  .on("data", function(data) {
    numberRow++;
    console.log(`numberRow: ${numberRow}`);

    let [id, code, name, parent,,,,,,,,,idSql] = data;

    csvData[csvData.length] = {
        id, code, name, parent, idSql
    }
  })
  .on("end", async function() {
    console.log(`-----------------------------`)
    console.log({ LENGTH: csvData.length })
    console.log(`-----------------------------`)

    // ------------ IMPORT DATA REGION 
    for (const data of csvData) {
      const { id, code, name, idSql } = data;
      await REGION_COLL.findOneAndUpdate({ code: id }, { id, idSql, code, name });
    }

    // ------------ IMPORT DATA AREA 
    for (const data of csvData) {
      const { id, code, name, parent, idSql } = data;

      const infoRegion = await REGION_COLL.findOne({ id: parent }).select('_id').lean();
      const infoArea = await AREA_COLL.findOne({ code: id }).select('_id').lean();

      if(infoRegion && infoArea){
        await AREA_COLL.findByIdAndUpdate(infoArea._id, { 
          id, idSql, code, name, idRegion: parent, parent: infoRegion._id
        });
      }
    }

    // ------------ IMPORT DATA DISTRIBUTOR 
    for (const data of csvData) {
      const { id, code, name, parent, idSql } = data;

      const infoArea = await AREA_COLL.findOne({ id: parent }).select('_id parent').lean();
      const infoDistributor = await DISTRIBUTOR_COLL.findOne({ code: id }).select('_id').lean();

      if(infoArea && infoDistributor){
        await DISTRIBUTOR_COLL.findByIdAndUpdate(infoDistributor._id, { 
          id, idSql, code, name, idArea: parent,
          region: infoArea.parent,
          area: infoArea._id,
        });
      }
    }

  });

stream.pipe(csvStream);
