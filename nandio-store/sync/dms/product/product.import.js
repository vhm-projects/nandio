const ObjectID              = require("mongoose").Types.ObjectId;
const MongoClient           = require("mongodb").MongoClient;
const request               = require('request');

const databaseConfig        = require('../../../www/config/cf_database');
const cfMode                = require('../../../www/config/cf_mode');

const PRODUCT_COLL          = require('../../../www/packages/product/databases/product-coll');
const PRODUCT_POINT_COLL    = require('../../../www/packages/product_point/databases/product_point-coll');
const BRAND_COLL            = require('../../../www/packages/brand/databases/brand-coll');

let mongodUrl = "";
if (cfMode.database_product) {
    mongodUrl = `${databaseConfig.product._mongod_user === '' ? 'mongodb://' + databaseConfig.product._mongodb_host + ':' + databaseConfig.product._mongodb_port + '/' + databaseConfig.product._mongod_name :
        'mongodb://' + databaseConfig.product._mongod_user + ':' + databaseConfig.product._mongodb_pass + '@' + databaseConfig.product._mongodb_host + ':' + databaseConfig.product._mongodb_port + '/' + databaseConfig.product._mongod_name}`;
 }
else {
    mongodUrl = `${databaseConfig.development._mongod_user === '' ? 'mongodb://' + databaseConfig.development._mongodb_host + ':' + databaseConfig.development._mongodb_port + '/' + databaseConfig.development._mongod_name :
            'mongodb://' + databaseConfig.development._mongod_user + ':' + databaseConfig.development._mongodb_pass + '@' + databaseConfig.development._mongodb_host + ':' + databaseConfig.development._mongodb_port + '/' + databaseConfig.development._mongod_name}`;
}

const mongoClient = new MongoClient(mongodUrl);

const checkDateValid = (d) => {
    if (Object.prototype.toString.call(d) === "[object Date]") {
        // it is a date
        if (isNaN(d.getTime())) {  // d.valueOf() could also work
            // date is not valid
            return false;
        } else {
            // date is valid
            return true;
        }
    } else {
        // not a date
        return false;
    }
}


function run1rd(dateP1 = '01/01/2000') {
    return new Promise(async (resolve, reject) => {
        const options = {
            'method': 'POST',
            'url': 'http://dms1.yensaothienviet.vn:8580/webresources/ios',
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Cookie': 'PHPSESSID=79183vm5mj4bb5igphlj0183g1'
            },
            body: `{\n    "MESSAGE":"DMS.PKG_SYNC_ASM.product_list_crm",\n    "PARAMS": [\n        {"p1":"${dateP1}"}\n    ]\n}`
        };

        request(options, async function (error, response) {
            if (error) throw new Error(error);

            try {
                let dataResp = JSON.parse(response.body);
                let listProduct = dataResp.DATA[0].RESULT_DATA;

                let dataAfterConvert = listProduct.map(product => {
                    return {
                        id: product[0], //PRODUCT_ID
                        code: product[1], //PRODUCT_CODE
                        name: product[2], //PRODUCT_NAME
                        brandCode: product[3], //BRAND_CODE
                        brandName: product[4], //BRAND_NAME
                        variant: product[5], //VARIANT
                        convfact: product[6], //CONVFACT
                        uom: product[7], //UOM
                        description: product[8], //DESCRIPTION
                        price: product[9], //PRICE
                        priceP1: product[10], //PRICE_P1
                        priceP2: product[11], //PRICE_P2
                        updateDate: product[12], //UPDATE_DATE
                    }
                })

                await mongoClient.connect();
                let db = mongoClient.db('nandio_staging');
                let PRODUCT_IMPORT_COLL = db.collection('product_import');

                await PRODUCT_IMPORT_COLL.deleteMany({});
                let infoAfterInsert = await PRODUCT_IMPORT_COLL.insertMany(dataAfterConvert);
                console.log(`Inserted: ${infoAfterInsert.insertedCount} rows`);

                resolve(infoAfterInsert);
            } catch (error) {
                reject(error);
            }
        });
    })
}

function run2rd() {
    return new Promise(async (resolve, reject) => {
        try {
            let listProductPoints = await PRODUCT_POINT_COLL.find({}).sort({ _id: -1 }).lean();
            let index = 0;
            for (const productPoint of listProductPoints) {
                console.log(`productPoint index with -_id: ${index}, _id: ${productPoint && productPoint._id}`)
                index++;

                const { product_id, point } = productPoint;

                if (product_id && product_id.length) {
                    await PRODUCT_COLL.findOneAndUpdate({ idSQL: product_id }, {
                        point: point
                    });
                }
            }

            resolve(index);
        } catch (error) {
            reject(error);
        }
    })
}

function run3rd() {
    return new Promise(async (resolve, reject) => {
        try {
            await mongoClient.connect();
            const db = mongoClient.db('nandio_staging');
            const PRODUCT_IMPORT_COLL = db.collection('product_import');

            let listProducts = await PRODUCT_IMPORT_COLL.find({}).sort({ _id: -1 }).toArray();
            let dataInsertAll = [];
            let index = 0;

            for (const product of listProducts) {
                console.log(`product index with -_id: ${index}, _id: ${product && product._id}`)
                index++;

                const {
                    id,
                    code,
                    name,
                    brandCode,
                    description,
                    price,
                    priceP1,
                    priceP2,
                    updateDate,
                } = product;

                if (id && id.length && code && code.length) {
                    let itemForInsert = {
                        idSQL: id,
                        productCode: code,
                        name,
                        description,
                        status: 2, // mặc định: tắt hoạt động
                    }
                    if (brandCode) {
                        let infoBrand = await BRAND_COLL.findOne({ brandCode: brandCode }).select('_id').lean();
                        if (infoBrand) {
                            itemForInsert.brand = ObjectID(infoBrand._id);
                        }
                    }

                    if (price && !Number.isNaN(price)) {
                        itemForInsert.price = price;
                    }

                    if (priceP1 && !Number.isNaN(priceP1)) {
                        itemForInsert.priceP1 = priceP1;
                    }

                    if (priceP2 && !Number.isNaN(priceP2)) {
                        itemForInsert.priceP2 = priceP2;
                    }

                    let dateInsert;
                    if (checkDateValid(new Date(updateDate))) {
                        dateInsert = new Date(updateDate);
                    } else {
                        dateInsert = new Date(Date.now());
                    }

                    itemForInsert.createAt = dateInsert;
                    itemForInsert.modifyAt = dateInsert;

                    console.log({ itemForInsert })

                    dataInsertAll[dataInsertAll.length] = itemForInsert;
                }
            }

            const infoAfterInsert = await PRODUCT_COLL.insertMany(dataInsertAll);
            console.log(`Inserted: ${infoAfterInsert.insertedCount} rows`);
            console.log({ PRODUCT_FINAL_LENGTH: dataInsertAll.length })

            resolve(infoAfterInsert);
        } catch (error) {
            reject(error);
        }
    })
}

function run4rd() {
    return new Promise(async resolve => {
        let listProduct = await PRODUCT_COLL.find({}).sort({ _id: -1 }).lean();
        let index = 0;

        for (const product of listProduct) {
            console.log(`product index with -_id: ${index}, _id: ${product && product._id}`)
            index++;

            let { productCode } = product;
            let listProductExists = await PRODUCT_COLL.find({ productCode }).lean();
            if (listProductExists && listProductExists.length === 1) {
                console.log(`---ko có trùng----`)
                // ko có trùng
                continue;
            } else { // có trùng
                console.log(`---có trùng----`)
                let [fistItemProduct, ...listProductDuplicateForRemove] = listProductExists;
                if (!fistItemProduct) return;

                // xoá từ thằng thứ 2 -> n-1
                if (listProductDuplicateForRemove && listProductDuplicateForRemove.length) {
                    let listProductDuplicateForRemoveIDs = [...listProductDuplicateForRemove.map(item => ObjectID(item._id))];
                    let infoAfterRemove = await PRODUCT_COLL.deleteMany({
                        _id: {
                            $in: listProductDuplicateForRemoveIDs
                        }
                    });
                    console.log({ infoAfterRemove })
                } else {
                    console.log(`--error---01---`)
                }
            }
        }

        resolve();
    })
}

// (async () => {
//     try {
//         await run1rd();
//         await run2rd();
//         await run3rd();
//         await run4rd();
//         console.log('Sync product done!!');
//     } catch (error) {
//         console.error(error);
//     }
// })()

module.exports = async date => {
    try {
        await run1rd(date);
        await run2rd();
        await run3rd();
        await run4rd();
        console.info('Sync product done!!', date);
    } catch (error) {
        console.error(error);
    }
}
