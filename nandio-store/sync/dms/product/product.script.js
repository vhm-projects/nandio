var request = require('request');
var mongodb = require('mongodb');
let url = "mongodb://localhost:27017";

var options = {
  'method': 'POST',
  'url': 'http://dms1.yensaothienviet.vn:8580/webresources/ios',
  'headers': {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Cookie': 'PHPSESSID=79183vm5mj4bb5igphlj0183g1'
  },
  body: '{\n    "MESSAGE":"DMS.PKG_SYNC_ASM.product_list_crm",\n    "PARAMS": [\n        {"p1":"01/01/2000"}\n    ]\n}'

};
request(options, function (error, response) {
  if (error) throw new Error(error);
  let dataResp = JSON.parse(response.body);
  let RESULT_DATA     = dataResp.DATA[0].RESULT_DATA;

  let RESULT_DATAAfterConvert = RESULT_DATA.map(product => {
    return {
        id: product[0], //PRODUCT_ID
        code: product[1], //PRODUCT_CODE
        name: product[2], //PRODUCT_NAME
        brandCode: product[3], //BRAND_CODE
        brandName: product[4], //BRAND_NAME
        variant: product[5], //VARIANT
        convfact: product[6], //CONVFACT
        uom: product[7], //UOM
        description: product[8], //DESCRIPTION
        price: product[9], //PRICE
        priceP1: product[10], //PRICE_P1
        priceP2: product[11], //PRICE_P2
        updateDate: product[12], //UPDATE_DATE
    }
  })
  mongodb.connect(
    url,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // console.log({ err, client });
      if (err) throw err;
      client
        .db("nandio")
        .collection("product_import")
        .insertMany(RESULT_DATAAfterConvert, (err, res) => {
          if (err) throw err;

          console.log(`Inserted: ${res.insertedCount} rows`);
          client.close();
        });
    }
  );
});
