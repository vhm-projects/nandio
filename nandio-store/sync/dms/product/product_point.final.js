// chạy script này để cập nhật product_point vào field point (product-coll)
let PRODUCT_COLL               = require('../../../www/packages/product/databases/product-coll');
let PRODUCT_POINT_COLL         = require('../../../www/packages/product_point/databases/product_point-coll');
const mongodb                   = require("mongodb").MongoClient;
const path                      = require("path");
const fs                        = require("fs");
const ObjectID                        = require("mongoose").Types.ObjectId;

// const { provinces }                                 = require('../../../www/packages/common/constants/wards');
let url = "mongodb://localhost:27017";

async function run() {
    let listProductPoints            = await PRODUCT_POINT_COLL.find({}).sort({ _id: -1 }).lean();
    let index = 0;
    for (const productPoint of listProductPoints) {
        console.log(`productPoint index with -_id: ${index}, _id: ${productPoint && productPoint._id}`)
        index++;

        const {
            product_id,
            point
        } = productPoint;

        if (product_id && product_id.length) {
            let infoProductAfterUpdatePoint = await PRODUCT_COLL.findOneAndUpdate({ idSQL: product_id }, {
                point: point
            }, { new: true });
        }
    }
}

run()
    .then(listProductConverted => {
       
    })
    .catch(error => console.log({ error }))