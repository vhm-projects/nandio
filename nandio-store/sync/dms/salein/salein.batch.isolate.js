let {
    checkObjectIDs,
    cleanObject,
} = require('../../../www/utils/utils');
const timeUtils = require('../../../www/utils/time_utils');

let STORE_COLL                  = require('../../../www/packages/store/databases/store-coll');
let EMPLOYEE_COLL               = require('../../../www/packages/employee/databases/employee-coll');
let KPI_SALEIN_ACTUALLY_COLL    = require('../../../www/packages/kpi/databases/kpi_salein_actually-coll');
 
 /**
 * Tạo mới kpi_salein_actually
* @param {object} store
* @param {object} employee
* @param {string} SALE_ORDER_ID
* @param {string} ORDER_NUMBER
* @param {date} ORDER_DATE
* @param {object} product
* @param {number} QUANTITY
* @param {number} IS_FREE_ITEM
* @param {objectId} author
* @this {BaseModel}
* @extends {BaseModel}
* @returns {{ error: boolean, data?: object, message?: string }}
*/
exports.insert = ({
    store,
    employee,
    SALE_ORDER_ID,
    SALE_ORDER_DETAIL_ID,
    ORDER_NUMBER,
    ORDER_DATE,
    product,
    QUANTITY,
    IS_FREE_ITEM,
    author
}) => {
    return new Promise(async resolve => {
        try {
            if (author && !checkObjectIDs([author])) {
                return resolve({
                    error: true,
                    message: 'Người tạo không hợp lệ',
                    status: 400
                });
            }

            if (SALE_ORDER_ID.length > 125) {
                return resolve({
                    error: true,
                    message: 'Độ dài sale_order_id không được lớn hơn 125 ký tự',
                    status: 400
                });
            }

            if (ORDER_NUMBER.length > 125) {
                return resolve({
                    error: true,
                    message: 'Độ dài order_number không được lớn hơn 125 ký tự',
                    status: 400
                });
            }

            if (store && !checkObjectIDs([store])) {
                return resolve({
                    error: true,
                    message: 'cửa hàng không hợp lệ',
                    status: 400
                });
            }

            if (employee && !checkObjectIDs([employee])) {
                return resolve({
                    error: true,
                    message: 'nhân viên không hợp lệ',
                    status: 400
                });
            }

            if (product && !checkObjectIDs([product])) {
                return resolve({
                    error: true,
                    message: 'sản phẩm không hợp lệ',
                    status: 400
                });
            }


            // let isExistOrderDuplicate = await KPI_SALEIN_ACTUALLY_COLL.findOne({ 
            //     store, //mỗi nhân viên là 1 cửa hàng (nên lấy theo employee hay store là như nhau)
            //     SALE_ORDER_ID, // mỗi đơn hàng có 1 id
            //     product, // 1 đơn hàng từ DMS nhập sẽ có n sản phẩm -> 1 SALE_ORDER_ID có thể lập lại n lần tương ứng sô lượng sản phẩm
            //     IS_FREE_ITEM, // có 2 loại: 1 hoặc 0

            //     QUANTITY, // hiện tại đang có record trùng: SALE_ORDER_ID và product -> cần thêm QUANTITY
            // });
            let isExistOrderDuplicate = await KPI_SALEIN_ACTUALLY_COLL.findOne({ 
                SALE_ORDER_DETAIL_ID
            });

            // console.log({ isExistOrderDuplicate: `SALE_ORDER_DETAIL_ID: ${SALE_ORDER_DETAIL_ID} exists` })
            // if(ORDER_NUMBER == 'IN012657') {
            //     console.log({ 
            //         store, //mỗi nhân viên là 1 cửa hàng (nên lấy theo employee hay store là như nhau)
            //         SALE_ORDER_ID, // mỗi đơn hàng có 1 id
            //         product, // 1 đơn hàng từ DMS nhập sẽ có n sản phẩm -> 1 SALE_ORDER_ID có thể lập lại n lần tương ứng sô lượng sản phẩm
            //         IS_FREE_ITEM, // có 2 loại: 1 hoặc 0

            //         QUANTITY, // hiện tại đang có record trùng: SALE_ORDER_ID và product -> cần thêm QUANTITY })
            //         isExistOrderDuplicate
            //     })
            // }
            if (isExistOrderDuplicate) {
                return resolve({ error: true, message: `SALE_ORDER_DETAIL_ID: ${SALE_ORDER_DETAIL_ID} exists` })
            }

            let dataInsert = {
                store,
                employee,
                SALE_ORDER_ID,
                SALE_ORDER_DETAIL_ID,
                ORDER_NUMBER,
                product,
                QUANTITY,
                IS_FREE_ITEM,
                author
            };         
            
            ORDER_DATE && (dataInsert.ORDER_DATE = new Date(ORDER_DATE));

            if (store) {
                const storeInfo = await STORE_COLL.findById(store);
                dataInsert.region = storeInfo.region;
                dataInsert.area = storeInfo.area;
                dataInsert.distributor = storeInfo.distributor;
            } else if (employee) {
                const employeeInfo = await EMPLOYEE_COLL.findById(employee);
               
                dataInsert.region = employeeInfo.region;
                dataInsert.area = employeeInfo.area;
                dataInsert.distributor = employeeInfo.distributor;
            }

            dataInsert = cleanObject(dataInsert);

            // let infoAfterInsert = await this.insertData(dataInsert);
            dataInsert.modifyAt = timeUtils.getCurrentTime();
            dataInsert.createAt = timeUtils.getCurrentTime();
            if (ORDER_NUMBER == 'IN011583') {
                console.log({
                    dataInsert
                })
            }   
            let infoAfterInsert = await KPI_SALEIN_ACTUALLY_COLL.create(dataInsert)
            if (ORDER_NUMBER == 'IN011583') {
                console.log({
                    infoAfterInsert
                })
            }   
            if (!infoAfterInsert) {
                return resolve({
                    error: true,
                    message: 'Tạo KPI Salein Thực Đạt thất bại'
                });
            }

            return resolve({
                error: false,
                data: infoAfterInsert,
                status: 200
            });
        } catch (error) {
            console.error(error);
            return resolve({
                error: true,
                message: error.message,
                status: 500
            });
        }
    })
}