const ObjectID              = require("mongoose").Types.ObjectId;
const request               = require('request');
const moment                = require('moment');

let STORE_COLL                  = require('../../../www/packages/store/databases/store-coll');
let EMPLOYEE_STORE_COLL         = require('../../../www/packages/employee_store/databases/employee_store-coll');
let CALENDAR_TASK_NA_COLL       = require('../../../www/packages/calendar_task/databases/calendar_task_na-coll');
let EMPLOYEE_COLL               = require('../../../www/packages/employee/databases/employee-coll');
let PRODUCT_COLL                = require('../../../www/packages/product/databases/product-coll');
let { insert: KPI_SALEIN_ACTUALLY_MODEL__insert }   = require('./salein.batch.isolate')

var dateCurrent = new Date();
var dateYesterday = new Date();
dateYesterday.setDate(dateYesterday.getDate() - 5); // thay vì lấy từ ngày hôm qua, nên lấy 5 ngày gần nhất cho chính xác
let fromDateYYYYMMDD    = moment(dateYesterday).format("YYYY-MM-DD");
let toDateYYYYMMDD      = moment(dateCurrent).format("YYYY-MM-DD");


// dự kiến sẽ chạy từ ngày 01/03/2022
/**
 * run1rd: function lấy dữ liệu từ API
 * @param {*} dateP1 thời gian từ
 * @param {*} dateP2 thời gian tới
 * @returns 
 */
function run1rd(dateP1 = '2022-05-01', dateP2 = '2022-05-31') {
    return new Promise(async (resolve, reject) => {
        const options = {
            'method': 'POST',
            'url': 'http://dms1.yensaothienviet.vn:8580/webresources/ios',
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded'
            },        
            body: `{\n    "MESSAGE":"DMS.pkg_sync_asm.ams_salein_store", \n    "PARAMS": [\n        {"p1":"${dateP1}", "p2":"${dateP2}"}\n    ]\n}`,
        };

        request(options, async function (error, response) {
            if (error) throw new Error(error);

            try {
                let dataResp = JSON.parse(response.body);
                let listOrders = dataResp.DATA[0].RESULT_DATA;

                let dataAfterConvert = listOrders.map(order => {
                    
                    return {
                        SHOP_CODE: order[0], //SHOP_CODE: Mã NPP
                        SHOP_NAME: order[1], //SHOP_NAME (BỎ)
                        STORE_CODE: order[2], //STORE_CODE: Mã Cửa Hàng
                        USERNAME: order[3], //USERNAME: mã nhân viên
                        SALE_ORDER_DETAIL_ID: order[4], //SALE_ORDER_DETAIL_ID: MÃ ĐƠN ĐỂ KIỂM TRA TỒN TẠI HAY CHƯA
                        SALE_ORDER_ID: order[5], //SALE_ORDER_ID
                        ORDER_NUMBER: order[6], //ORDER_NUMBER
                        ORDER_DATE: order[7], //ORDER_DATE
                        PRODUCT_ID: order[8], //PRODUCT_ID (BỎ)
                        PRODUCT_CODE: order[9], //PRODUCT_CODE: Mã Sản Phẩm đầu 1
                        PRODUCT_NAME: order[10], //PRODUCT_NAME (BỎ)
                        QUANTITY: order[11], //QUANTITY (Số Lượng)
                        IS_FREE_ITEM: order[12], //IS_FREE_ITEM
                    }
                })
                // console.log("🚀 ~ file: salein.batch.js ~ line 50 ~ dataAfterConvert", dataAfterConvert)

                // await mongoClient.connect();
                // let db = mongoClient.db('nandio_staging');
                // let PRODUCT_IMPORT_COLL = db.collection('product_import');

                // await PRODUCT_IMPORT_COLL.deleteMany({});
                // let infoAfterInsert = await PRODUCT_IMPORT_COLL.insertMany(dataAfterConvert);
                // console.log(`Inserted: ${infoAfterInsert.insertedCount} rows`);

                resolve(dataAfterConvert);
            } catch (error) {
                reject(error);
            }
        });
    })
}

function run2rd({ listOrders }) {
    console.log({
        run2rd_listOrders: listOrders.length
    })
    return new Promise(async resolve => {
        try {
            const AUTHOR_DEFAULT = '617a535ec5cd05506cc4d54c';
            // danh sách convert với key là schema của kpi_salein_actually-coll
            let listOrdersConvertToSchema = [];
            for await (let order of listOrders) {
                if (order && order.STORE_CODE) {
                    
                    /**
                     * Case sẽ chia ra như sau:
                     *  (?) Có Store ???
                     *   -> Có:
                     *         + (?) IS_FREE_ITEM(có phải là hàng tặng)
                     *              -> Có: insert
                     *              -> Không: insert
                     *   -> Không: Tăng tồn kho từ Mã Nhân Viên
                     */

                    // vì có store -> store sẽ có format (G17DAMIL_G1700472)
                        // G17DAMIL: mã npp
                        // G1700472: mã cửa hàng
                    let [distributorCode, storeCodeSplit] = order.STORE_CODE && order.STORE_CODE.split('_');;
                    let storeInfo = await STORE_COLL.findOne({ code: storeCodeSplit });
                    if (storeInfo) {
                        //? từ mã cửa hàng -> tìm mã nhân viên làm việc tại tháng đó (cần hỏi lại A Tuấn phần này)
                        // let employeeStoreInfo = await EMPLOYEE_STORE_COLL.findOne({
                        //     store: storeInfo._id,
                        //     // có thể lấy thêm create trong khoảng tháng hiện tại
                        // }); //function này đã predecated được thay thế bởi CALENDAR_TASK_NA_COLL

                        // chỉ lấy lịch làm việc trong tháng của cửa hàng
                        let dateCurrentGetCalendarInMonth = new Date();
                        let month = dateCurrentGetCalendarInMonth.getMonth() + 1;
                        let year  = dateCurrentGetCalendarInMonth.getFullYear();


                        let employeeStoreInfo = await CALENDAR_TASK_NA_COLL.findOne({
                            store: storeInfo._id,
                            
                            month,
                            year,
                        });

                        if (!employeeStoreInfo) {
                            // console.log(`STORE: ${storeInfo._id} không có lịch làm việc trong tháng`)
                        }

                        let productInfo       = await PRODUCT_COLL.findOne({
                            productCode: order.PRODUCT_CODE
                        })

                        if (!productInfo) {
                            // console.log(`PRODUCT: ${order.PRODUCT_CODE} KHÔNG TỒN TẠI`)
                        }
                        /**
                         * nếu product có, và employeeStoreInfo ko tồn tại -> nhân viên này không có lịch làm việc trong tháng hiện tại
                         */
                        if (employeeStoreInfo && productInfo) {
                            listOrdersConvertToSchema = [...listOrdersConvertToSchema,
                                KPI_SALEIN_ACTUALLY_MODEL__insert({
                                    store: storeInfo._id,
                                    employee: employeeStoreInfo.employee,
                                    SALE_ORDER_ID: order.SALE_ORDER_ID,
                                    SALE_ORDER_DETAIL_ID: order.SALE_ORDER_DETAIL_ID,
                                    ORDER_NUMBER: order.ORDER_NUMBER,
                                    ORDER_DATE: order.ORDER_DATE,
                                    product: productInfo._id,
                                    QUANTITY: order.QUANTITY,
                                    IS_FREE_ITEM: order.IS_FREE_ITEM,
                                    author: AUTHOR_DEFAULT
                                })
                            ]
                        }
                        
                    } else {
                        console.log({ store_not_exist_in_db_and_not_write_db: storeCodeSplit })
                    }
                   
                } else {
                    /**
                     * danh sách riêng cho việc tăng_tồn_kho cho nhân viên
                     *  trường hợp này sẽ có EMPLOYEE_CODE nhưng ko có STORE
                     */

                    /**
                     * order.USERNAME là mã nhân viên với format: 'NA_58BTHANHNA1', 'G31NTRAG_NATV2'
                     *  TH1: có chứa 'NA_' -> thì cần tách
                     *  TH2: 'G31NTRAG_NATV2' -> ko chứa 'NA' trước mã nhân viên -> ko cần tách
                     * 
                     *  -> 'NA' là loại nhân viên
                     *  -> '58BTHANHNA1' là mã nhân viên
                     */
                    let codeEmployee = '';
                    if (order.USERNAME && order.USERNAME.indexOf('NA_') == 0)
                        codeEmployee = order.USERNAME && order.USERNAME.split('_')[1];
                    else 
                        codeEmployee = order.USERNAME;
                    
                    let employeeInfo = await EMPLOYEE_COLL.findOne({
                        //code: codeEmployee
                        $or: [
                            {  code: codeEmployee.toLowerCase() },
                            {  code: codeEmployee.toUpperCase() },
                        ]
                    });

                    let productInfo       = await PRODUCT_COLL.findOne({
                        productCode: order.PRODUCT_CODE
                    })

                    if (employeeInfo && productInfo) {
                        listOrdersConvertToSchema = [...listOrdersConvertToSchema,
                            KPI_SALEIN_ACTUALLY_MODEL__insert({
                                // store: '..', store trong trường hợp TĂNG TỒN KHO này ko tồn tại
                                employee: employeeInfo._id,
                                SALE_ORDER_ID: order.SALE_ORDER_ID,
                                SALE_ORDER_DETAIL_ID: order.SALE_ORDER_DETAIL_ID,
                                ORDER_NUMBER: order.ORDER_NUMBER,
                                ORDER_DATE: order.ORDER_DATE,
                                product: productInfo._id,
                                QUANTITY: order.QUANTITY,
                                IS_FREE_ITEM: order.IS_FREE_ITEM,
                                author: AUTHOR_DEFAULT
                            })
                        ]
                    } else {
                        console.log({ employee_not_exist_in_db_and_not_write_db: order.USERNAME })
                    }
                }
            }
            console.log({ listOrdersConvertToSchema: listOrdersConvertToSchema.length })
            let listOrdersConvertToSchemaResult = listOrdersConvertToSchema && listOrdersConvertToSchema.length && 
                await Promise.all(listOrdersConvertToSchema);

            console.log({ listOrdersConvertToSchemaResult: listOrdersConvertToSchemaResult.length })
//		process.exit(0);
        } catch (error) {
            console.log({ error_run2rd: error })
            return resolve({ error: true, message: error.message })
//		process.exit(0);
        }
    })
}

exports.RUN_SYNC_SALEIN_DMS = () => {
    console.log('fromDateYYYYMMDD: ', fromDateYYYYMMDD);
    console.log('toDateYYYYMMDD: ', toDateYYYYMMDD);
    run1rd(fromDateYYYYMMDD, toDateYYYYMMDD)
        .then(listOrders => {
            run2rd({ listOrders })
                .then(result2 => {
                    console.log({ result2 })
                    // process.exit(0);
                })
        })
        .catch(err => {
            console.log({ err })
            // process.exit(1);
        })
}
