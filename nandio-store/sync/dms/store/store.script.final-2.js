// sẽ là bảng tiếp theo để chuyển danh sách các store trùng để push vào brand
let STORE_COLL              = require('../../../www/packages/store/databases/store-coll');
let BRAND_COLL              = require('../../../www/packages/brand/databases/brand-coll');
const mongodb                   = require("mongodb").MongoClient;
const path                      = require("path");
const fs                        = require("fs");
const ObjectID                        = require("mongoose").Types.ObjectId;

let url = "mongodb://localhost:27017";

async function run() {
    try {
        let listStore            = await STORE_COLL.find({}).sort({ _id: -1 }).lean();
        let index = 0;
        for (const store of listStore) {
            console.log(`store index with -_id: ${index}, _id: ${store && store._id}`)
            index++;
            let { storeCode } = store;
            let listStoreExists = await STORE_COLL.find({ storeCode }).lean();
            if (listStoreExists && listStoreExists.length == 1) {
                console.log(`---ko có trùng----`)
                //ko có trùng
                // return;
                continue;

            } else {// có trùng
                console.log(`---có trùng----`)
                // listStoreExists && listStoreExists.length > 1
                let [fistItemStore, ...listStoreDuplicateForRemove] = listStoreExists;
                if (!fistItemStore) return;

                // tìm danh sách brands của những thằng trùng
                let listBrandAfterMerge = [];
                listStoreExists.forEach(item => {
                    if (item.brands)
                        listBrandAfterMerge = [...listBrandAfterMerge, ...item.brands]
                });

                // xoá từ thằng thứ 2 -> n-1
                if (listStoreDuplicateForRemove && listStoreDuplicateForRemove.length) {
                    let listStoreDuplicateForRemoveIDs = [...listStoreDuplicateForRemove.map(item => ObjectID(item._id))];
                    let infoAfterRemove = await STORE_COLL.deleteMany({
                        _id: {
                            $in: listStoreDuplicateForRemoveIDs
                        }
                    });
                    console.log({ infoAfterRemove })
                } else {
                    console.log(`--error---01---`)
                }

                let infoAfterPushBrandIntoStoreExists = await STORE_COLL.findOneAndUpdate({ _id: fistItemStore._id }, {
                    brands: listBrandAfterMerge
                }, { new: true });
                console.log({
                    infoAfterPushBrandIntoStoreExists, 
                    listBrandAfterMerge
                })
            }
        }
    } catch (error) {
        console.log({ error })
    }
}

run()
    .then(listStoreConverted => {
        // mongodb.connect(
        //     url,
        //     { useNewUrlParser: true, useUnifiedTopology: true },
        //     (err, client) => {
        //       console.log({ err, client });
        //       if (err) throw err;
        //       client
        //         .db("nandio")
        //         .collection("store_final")
        //         .insertMany(listStoreConverted, (err, res) => {
        //           if (err) throw err;
        
        //           console.log(`Inserted: ${res.insertedCount} rows`);
        //           client.close();
        //         });
        //     }
        // );
    })
    .catch(error => console.log({ error }))