let STORE_COLL              = require('../../../www/packages/store/databases/store-coll');
let STORE_TEMP_COLL         = require('../../../www/packages/store/databases/store_temp-coll');
let BRAND_COLL              = require('../../../www/packages/brand/databases/brand-coll');
const mongodb               = require("mongodb").MongoClient;
const path                  = require("path");
const fs                    = require("fs");
const ObjectID              = require("mongoose").Types.ObjectId;

const { provinces }         = require('../../../www/packages/common/constants/provinces');
const { districts }         = require('../../../www/packages/common/constants/districts');
let url                     = "mongodb://localhost:27017";


let filterObject = (obj, filter, filterValue, filterCuster) => 
    !filterCuster ? 
    Object.keys(obj).reduce((acc, val) =>
        (obj[val][filter] === filterValue ? {
            ...acc,
            [val]: obj[val]  
        } : acc
    ), {}) 
        :
    Object.keys(obj).reduce((acc, val) =>
        (obj[val][filter] === filterValue || obj[val][filterCuster]  === filterValue ? {
            ...acc,
            [val]: obj[val]  
        } : acc
    ), {}) 


const getObjItemByProvinceName = ({ provinceName }) => {
    let filterObject = (obj, filter, filterValue) => 
        Object.keys(obj).reduce((acc, val) =>
        (obj[val][filter] === filterValue ? {
            ...acc,
            [val]: obj[val]  
        } : acc
    ), {});
    if (provinceName && provinceName.length) {
        if (provinceName.includes('TP')) {
            provinceName = provinceName.replace('TP', '')
            provinceName = provinceName.trim();
        }
        let infoProvince = filterObject(provinces, 'name', provinceName.toString())
        return Object.values(infoProvince)[0];
    }
} 

const getObjItemByDistrictName = ({ province, districtName }) => {
        // let { province } = req.params;
       
        if (province && !Number.isNaN(Number(province))) {
            let listDistricts   = filterObject(districts, 'parent_code', province.toString());

            let itemDistrict    = filterObject(listDistricts, 'name', districtName, 'name_with_type') 
            if (!itemDistrict) {
                console.log(`-----------------------itemDistrict--------------------------`)
            }   
                            //    filterObject(listDistricts, 'name_with_type', districtName);
          
            // console.log({
            //     // listDistricts,
            //     districtName,
            //     getObjItemByDistrictName: itemDistrict
            // });
            return Object.values(itemDistrict)[0];
        } else {
            console.log(`--district: province null`)
        }

}

const getObjItemByWardName = ({ district, wardName }) => {
    return new Promise(async resolve => {
        try {
            if (!district || !wardName)
                return resolve({ error: true, message: "invalid_param" });

            let listWards = [];
            let  filePath = path.resolve(__dirname, `../../../www/packages/common/constants/wards/${district}.json`);
            await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                if (district == '088') {
                    console.log({ err, data })
                }
                if (!err) {
                    if (!data)
                        return resolve({ error: true, message: "no_data_district" });

                    listWards = JSON.parse(data);

                    if (listWards && wardName) {
                        let itemWard = filterObject(listWards, 'name', wardName, 'name_with_type');
                        if (itemWard) {
                            return resolve({
                                error: false,
                                data: Object.values(itemWard)[0]
                            })
                        } else {
                            return resolve({ error: true, message: "cannot_get_itemWard" });
                        }
                    } else {
                        return resolve({ error: true, message: "cannot_get_listWards_or_wardName" });
                    }
                } else {
                    return resolve({ error: true, message: "district_not_exist" });
                }
            });
        } catch (error) {
            return { error: true, message: error.message };
        }
    })
}

async function run() {
    let listStore = await STORE_TEMP_COLL.find({}).sort({ _id: -1 }).lean();
    console.log({ listStore: listStore.length })
    let infoAferUpdateAll = [];
    let index = 0;
    for (const store of listStore) {
        console.log(`store index with -_id: ${index}, _id: ${store && store._id}`);
        console.log(`-------------------------------------------------------------`);
        index++;
        let { code, name, address, province, district, precinct, lat, lng, phone, brandCode } = store;
        let isExistStore = await STORE_COLL.findOne({ storeCode: code }).lean();
        if (isExistStore) {
            let infoAfterPushBrandIntoStoreExists = await STORE_COLL.findOneAndUpdate({ storeCode: code }, {
                $addToSet: {
                    brands: brandCode
                }
            })
            if (!infoAfterPushBrandIntoStoreExists)
                console.log({ infoAfterPushBrandIntoStoreExists })
            return;
        }

        if (code && code.length) {
            let itemForInsert = {
                storeCode: code,
                name,
                phone,
                address
            }
            let cityForInsert = '',
                districtForInsert = '',
                wardForInsert = '';

            if (brandCode) {
                let infoBrand = await BRAND_COLL.findOne({ brandCode: brandCode }).lean();
                if (infoBrand) {
                    itemForInsert = {
                        ...itemForInsert,
                        brands: [ObjectID(infoBrand._id)]
                    }
                }
            }

            if (province) {
                /**
                 * có 2 trường hợp đặt biệt 
                 *     bên trái là nội dung trong file import - bên phải là nội dung dữ liệu tỉnh thành
                 *    Hòa Bình -> Hoà Bình
                      Thừa Thiên - Huế ->Thừa Thiên Huế
                 */
                if (province == 'Hòa Bình') {
                    province = 'Hoà Bình'
                }
                if (province == 'Thừa Thiên - Huế') {
                    province = 'Thừa Thiên Huế'
                }
                if (province == 'TIỀN GIANG') {
                    province = 'Tiền Giang'
                }

                let itemProvince = getObjItemByProvinceName({provinceName: province})
               
                if (itemProvince) {
                    let { code: provinceCode } = itemProvince;
                    // itemForInsert = {
                    //     ...itemForInsert,
                    //     city: provinceCode
                    // }
                    cityForInsert = provinceCode;

                   
                    if (district) { 
                        if (district.includes('Huyện') || district.includes('huyen')) {
                            district = district.replace('Huyện', '')
                            district = district.trim();
                        }
                        if (district.includes('TP')) {
                            district = district.replace('TP.', '')
                            district = district.trim();
                        }

                        let itemDistrict = getObjItemByDistrictName({
                            province: provinceCode,
                            districtName: district
                        })
                      
                        if (itemDistrict) {
                            let { code: districtCode } = itemDistrict;
                            // itemForInsert = {
                            //     ...itemForInsert,
                            //     district: districtCode
                            // }
                           
                            
                         
                            
                            districtForInsert = districtCode;

                            if (precinct) {
                                let infoWard = await getObjItemByWardName({
                                    district: districtCode,
                                    wardName: precinct
                                })
                                if (!infoWard.error) {
                                    let dataOfWard = infoWard.data;
                                    if (dataOfWard) {
                                        let { code: wardCode } = dataOfWard
                                        // console.log({ district,  districtCode, precinct, wardCode });

                                        if (wardCode) {
                                            // itemForInsert = {
                                            //     ...itemForInsert,
                                            //     ward: wardCode
                                            // }
                                            wardForInsert = wardCode;
                                            // console.log({
                                            //     // itemDistrict, 
                                            //     district, districtCode,
                                            //     precinct, wardCode
                                            // });
                                        }
                                    }
                                } else {
                                    console.log(`code: ${code}, district: ${district}, precinct: ${precinct}`)
                                }
                            }
                        } 
                        else {
                            console.log(`----------KHÔNG TÌM THẤY DISTRICT--------`)
                            console.log({ itemDistrict, provinceCode, district })
                        }
                    }

                }
            }

            if (lat && lng) {
                itemForInsert = {
                    ...itemForInsert,
                    location: {
                        type: "point",
                        coordinates: [Number(lng), Number(lat)]
                    }
                }
            }

            infoAferUpdateAll = [
                ...infoAferUpdateAll, {
                    ...itemForInsert,
                    city: cityForInsert, 
                    district: districtForInsert, 
                    ward: wardForInsert
                }
            ]
            // itemForInsert = {};
            if (province == 'TP Hồ Chí Minh') {
                console.log({ district, districtForInsert })
                console.log({
                    infoAferUpdateAll: infoAferUpdateAll[infoAferUpdateAll.length - 1]
                });
            }
           
        }
    }

    return infoAferUpdateAll;
}

run()
    .then(listStoreConverted => {
        mongodb.connect(
            url,
            { useNewUrlParser: true, useUnifiedTopology: true },
            (err, client) => {
              console.log({ err, client });
              if (err) throw err;
              client
                .db("nandio_convert")
                .collection("store_final")
                .insertMany(listStoreConverted, (err, res) => {
                  if (err) throw err;
        
                  console.log(`Inserted: ${res.insertedCount} rows`);
                  client.close();
                });
            }
        );
    })
    .catch(error => console.log({ error }))