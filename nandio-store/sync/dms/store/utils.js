const { provinces }         = require('../../../www/packages/common/constants/provinces');
const { districts }         = require('../../../www/packages/common/constants/districts');

exports.filterObject = (obj, filter, filterValue, filterCuster) => 
    !filterCuster ? 
    Object.keys(obj).reduce((acc, val) =>
        (obj[val][filter] === filterValue ? {
            ...acc,
            [val]: obj[val]  
        } : acc
    ), {}) 
        :
    Object.keys(obj).reduce((acc, val) =>
        (obj[val][filter] || obj[val][filterCuster] === filterValue ? {
            ...acc,
            [val]: obj[val]  
        } : acc
    ), {}) 

exports.getObjItemByProvinceName = ({ provinceName }) => {
    let filterObject = (obj, filter, filterValue) => 
        Object.keys(obj).reduce((acc, val) =>
        (obj[val][filter] === filterValue ? {
            ...acc,
            [val]: obj[val]  
        } : acc
    ), {});
    if (provinceName && provinceName.length) {
        if (provinceName.includes('TP')) {
            provinceName = provinceName.replace('TP', '')
            provinceName = provinceName.trim();
        }
        let infoProvince = filterObject(provinces, 'name', provinceName.toString())
        return Object.values(infoProvince)[0];
    }
}

exports.getObjItemByDistrictName = ({ province, districtName }) => {
    if (province && !Number.isNaN(Number(province))) {
        let listDistricts = filterObject(districts, 'parent_code', province.toString());

        let itemDistrict = filterObject(listDistricts, 'name', districtName, 'name_with_type') 
        if (!itemDistrict) {
            console.log(`-----------------------itemDistrict--------------------------`)
        }   

        return Object.values(itemDistrict)[0];
    } else {
        console.log(`--district: province null`)
    }
}
