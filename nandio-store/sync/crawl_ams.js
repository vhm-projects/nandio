const puppeteer = require('puppeteer');
const async = require('async');
const mongodb = require("mongodb").MongoClient;

let URL_MONGODB = "mongodb://localhost:27017";

(async () => {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.setViewport({ width: 1366, height: 768 });
    await page.goto('http://ams1.yensaothienviet.vn:8680/thsams/web/index.php?r=site%2Flogin', { 
      waitUntil: 'networkidle2'
    });

    // Fill input username
    await page.waitForSelector('#loginform-username');
    await page.$eval('#loginform-username', el => el.value = 'admin');

    // Fill input password
    await page.waitForSelector('#loginform-password');
    await page.$eval('#loginform-password', el => el.value = 'nadio@123');

    // Click button submit
    await page.$eval('button[type="submit"][name="login-button"]', form => form.click() );

    await page.waitForSelector('#mien-id');

    // Crawl data from input select
    const dataCrawler = await page.evaluate(() => {
      const selectRegions = document.querySelectorAll('#mien-id option');
      const selectAreas = document.querySelectorAll('#vung-id option');
      const selectNPPs = document.querySelectorAll('#shop-id option');

      let regions = [];
      let areas = [];
      let npps = [];

      selectRegions.forEach(option => regions[regions.length] = {
        name: option.textContent,
        code: option.value,
        state: 1,
        status: 1
      })

      selectAreas.forEach(option => areas[areas.length] = {
        name: option.textContent,
        code: option.value,
        state: 1,
        status: 1
      })

      selectNPPs.forEach(option => npps[npps.length] = {
        name: option.textContent,
        code: option.value,
        state: 1,
        status: 1
      })

      regions.shift();
      areas.shift();
      npps.shift();

      return {
        regions,
        areas,
        npps,
      };
    });

    // console.log('Data:', dataCrawler);

    mongodb.connect(
      URL_MONGODB,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err, client) => {
        if (err) throw err;

        const { regions, areas, npps } = dataCrawler;

        const db = client.db('nandio_staging');
        const regionCollection = db.collection('regions');
        const areaCollection = db.collection('areas');
        const nppCollection = db.collection('distributors');

        async.parallel([
          callback => {
            regionCollection.drop();
            regionCollection.insertMany(regions, callback);
          },
          callback => {
            areaCollection.drop();
            areaCollection.insertMany(areas, callback);
          },
          callback => {
            nppCollection.drop();
            nppCollection.insertMany(npps, callback);
          }
        ], () => {
            console.log('Hey, done!!');
        });

      }
    );

  await browser.close();
})();