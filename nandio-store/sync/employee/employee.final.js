const mongodb                   = require("mongodb").MongoClient;
const path                      = require("path");
const ObjectID                  = require("mongoose").Types.ObjectId;
const DISTRIBUTOR_COLL = require('../../www/packages/distributor/databases/distributor-coll');

let url = "mongodb://localhost:27017";
async function run() {
    let client = await mongodb.connect(url);
    let db     = client.db("nandio_staging_import_employee_pg");

    let cursor = await db.collection("employee_temp").find();
    let data = await cursor.toArray();

    let listEmployee = []
    if (data && data.length) {
        for (let employee of data) {
            let NPP = employee.NPP;
            if (employee.NPP.includes(" ")) {
                let splitNPP = employee.NPP.split(" ");
                NPP = splitNPP[0];
            }

            let infoDistributor = await DISTRIBUTOR_COLL.findOne({
                code: NPP
            });

            if (infoDistributor) {
                listEmployee = [
                    ...listEmployee,
                    {
                        "status" : 1,
                        "roles" : [
                            ObjectID("61799ef6ccd7cf418c13caa3")
                        ],
                        "state" : 1,
                        "code" : employee.MaNV,
                        "fullname" : employee.MaNV,
                        "username" : employee.MaNV,
                        "email" : `${employee.MaNV}@gmail.com`,
                        "password" : "$2a$12$IexifCxvkUrZdE21gi4P6OsWVevkPCZh2MxSZPV1G/fFT5Fpli2OG", //12345,
                        "region" : infoDistributor.region,
                        "distributor": infoDistributor._id,
                        "area" : infoDistributor.area,
                        "type_employee" : ObjectID("616da3cbee78f90f7c04f156"),
                        "run_script_13_03_2022": true
                    },
                ];
            } else {
                console.log({
                    employee
                });
            }
        }
    }

    return listEmployee;
}

run()
    .then(listEmployee => {
        console.log({ listEmployee: listEmployee.length })
        mongodb.connect(
            url,
            { useNewUrlParser: true, useUnifiedTopology: true },
            (err, client) => {
              console.log({ err, client });
              if (err) throw err;
              client
                .db("nandio_staging_import_employee_pg")
                .collection("employees")
                .insertMany(listEmployee, (err, res) => {
                  if (err) throw err;
        
                  console.log(`Inserted: ${res.insertedCount} rows`);
                  client.close();
                });
            }
        );
    })
    .catch(error => console.log({ error }))