const fastcsv = require("fast-csv");
const fs = require('fs');
const mongodb = require("mongodb").MongoClient;
const ObjectID = require('mongoose').Types.ObjectId;
let stream = fs.createReadStream('./form_tạo_user.csv');

const REGION_COLL = require('../../www/packages/region_area/databases/region-coll');
const DISTRIBUTOR_COLL = require('../../www/packages/distributor/databases/distributor-coll');
const EMPLOYEE_COLL = require('../../www/packages/employee/databases/employee-coll');

let numberRow = 0;
let url = "mongodb://localhost:27017";
let csvData = [];
let csvStream = fastcsv
  .parse()
  .on("data", async function(data) {
    console.log(`numberRow: ${numberRow}`);
    numberRow++;
    
    // let infoCustomer = {};

    let MaNPP = data[0]; // MÃ ĐẦU 2
    let MaNV = data[2]; // MÃ QR
    console.log({
        MaNPP
    });
    if (MaNPP && MaNV) {
        csvData = [
            ...csvData,
            {
                NPP: MaNPP,
                MaNV: MaNV
            }
            // {
            //     "status" : 1,
            //     "roles" : [
            //         ObjectID("61799ef6ccd7cf418c13caa3")
            //     ],
            //     "state" : 1,
            //     "code" : MaNV,
            //     "fullname" : MaNV,
            //     "username" : MaNV,
            //     "email" : `${MaNV}@gmail.com`,
            //     "password" : "", //12345,
            //     "region" : infoDistributor.region,
            //     "distributor": infoDistributor._id,
            //     "area" : infoDistributor.area,
            //     "type_employee" : ObjectID("616da3cbee78f90f7c04f156"),
            //     "run_script_13_03_2022": true
            // },
        ];
    }
  })
  .on("end", async function() {
    // remove the first line: header
    // csvData.shift();
    console.log(`-----------------------------`)
    console.log({ LENGTH: csvData.length })
    console.log(`-----------------------------`)

    mongodb.connect(
      url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err, client) => {
        console.log({ err, client });
        if (err) throw err;
        client
          .db("nandio_staging_import_employee_pg")
          .collection("employee_temp")
          .insertMany(csvData, (err, res) => {
            if (err) throw err;

            console.log(`Inserted: ${res.insertedCount} rows`);
            client.close();
          });
      }
    );
  });

stream.pipe(csvStream);