const moment = require('moment');
async function runConvertLastStockOfMonthToFirstStockOfCurrentMonth() {
	const EMPLOYEE_COLL      			  = require('../../www/packages/employee/databases/employee-coll');
	const LIMIT_KPI_CONFIG_MODEL  		  = require('../../www/packages/config/models/limit_kpi_config').MODEL;
	const INVENTORY_BEGIN_MONTH_COLL      = require('../../www/packages/inventory_begin_month/databases/inventory_begin_month-coll');

	let yearCurrent     = moment().get('years');
	let monthCurrent    = moment().get('months') + 1; // Starting 1
	let yearPrevious   	= moment().subtract(1, 'year').get('years');
	let monthPrevious   = moment().subtract(1, 'month').get('months') + 1; // Starting 1

	// Nếu tháng hiện tại là tháng đầu tiên -> set năm hiện tại = năm trước 
	if(monthCurrent === 1) {
		yearCurrent = yearPrevious;
	}

	console.log({ yearCurrent, monthCurrent, yearPrevious, monthPrevious })
	/**
	 * b1: lấy danh sách employee của hệ thống
	 */
	let listEmployee = await EMPLOYEE_COLL.find({ state: 1 });
	console.log({ listEmployee: listEmployee.length });
	
	/**
	 * b1.2: tránh dữ liệu lập lại nếu chạy lại -> cần xoá tất cả dữ liệu trong tháng,năm đó -> chạy lại insert mới
	 */
	let infoAfterRemoveAll = await INVENTORY_BEGIN_MONTH_COLL.deleteMany({
		month: monthCurrent,
		year: yearCurrent
	})
	// console.log('infoAfterRemoveAll: ', infoAfterRemoveAll);
	/**
	 * b2:for từng employee và call func lấy tồn kho tháng trước
	 */
	for (let [index, infoEmployee] of listEmployee.entries()) {
		let { _id: employeeID } = infoEmployee;
		let listSKUWithInventoryResp = await LIMIT_KPI_CONFIG_MODEL.getListSKUWithInventory({ employeeID, date: `${monthPrevious}/${yearCurrent}` });
		if (listSKUWithInventoryResp.error) {
			return console.log({
				employeeID,
				listSKUWithInventoryResp
			})
		} 
		let listSKUWithInventory = listSKUWithInventoryResp.data && listSKUWithInventoryResp.data.listSKUWithInventory;
		if (!listSKUWithInventory) {
			return console.log({
				employeeID,
				listSKUWithInventoryResp
			})
		}
		/**
		 * b3: tạo promise.all danh sách record cho nhân viên theo từng SKU
		 */
		let listSKUWithInventoryPreviosBeforeInsert = listSKUWithInventory.map(itemSKUInventoryPreviousMonth => {
			return {
				title: `Tồn đầu tháng: ${monthCurrent}, năm: ${yearCurrent}`,
				month: monthCurrent,
				year: yearCurrent,
				sku: itemSKUInventoryPreviousMonth.sku,
				amount: itemSKUInventoryPreviousMonth.amount || 0,
				employee: employeeID
			}
		})
		let infoAfterInsertMany = await INVENTORY_BEGIN_MONTH_COLL.insertMany(listSKUWithInventoryPreviosBeforeInsert);
		console.log(`infoAfterInsertMany - i=${index}: ${employeeID} -> total: ${infoAfterInsertMany.length} record`);
	}
}

(async () => {
    try {
        let result__runConvertLastStockOfMonthToFirstStockOfCurrentMonth = await runConvertLastStockOfMonthToFirstStockOfCurrentMonth();
		console.log({ result__runConvertLastStockOfMonthToFirstStockOfCurrentMonth })
    } catch (error) {
        console.log({ error })
    }
})();