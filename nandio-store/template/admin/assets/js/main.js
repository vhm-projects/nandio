function validPhoneNumber(phone) {
    var vnf_regex = /((09|03|07|08|05)+([0-9]{8}))/g;
    if (phone !== '') {
        if (vnf_regex.test(phone) == false)
            return false;
        return true;
    } else {
        return false;
    }
}

function validFullname(fullname) {
    let regName = /^[a-zA-Z]+ [a-zA-Z]+$/;
    if(!regName.test(fullname)){
        return false;
    } else{
        return true;
    }
}

function validUserName(username) {
    let usernameRegex = /^[a-zA-Z0-9]+$/;
    let validUsername = username.match(usernameRegex);
    return validUsername != null;
};

/**
 * param: email
 * return bolean
 */
function validEmail(email){
    const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(String(email).toLowerCase());
}

function base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
       var ascii = binaryString.charCodeAt(i);
       bytes[i] = ascii;
    }
    return bytes;
}

function saveByteArray(fileName, byte) {
    var blob = new Blob([byte], { type: "application/pdf" });
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = fileName;
    link.click();
};

function s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
}

function createAndDownloadBlobFile(body, filename, extension = 'pdf') {
    const blob = new Blob([body]);
    // const fileName = `${filename}.${extension}`;
    if (navigator.msSaveBlob) {
      // IE 10+
      navigator.msSaveBlob(blob, filename);
    } else {
      const link = document.createElement('a');
      // Browsers that support HTML5 download attribute
      if (link.download !== undefined) {
        const url = URL.createObjectURL(blob);
        link.setAttribute('href', url);
        link.setAttribute('download', filename);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
}

function createDownloadBufferFile(buffer, fileName, type = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
    let bytes = new Uint8Array(buffer.length);

    for (let i = 0; i < bytes.length; i++) {
        bytes[i] = buffer[i];
    }

    const blob = new Blob([bytes], { type });
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');

    link.href = url;
    link.download = fileName;
    link.click();
    link.remove();
}

$(document).ready(function () {
    $('body').addClass('loaded');
    $('#global-loader').fadeOut();

    Fancybox.bind(".fancybox", {
        on: {
            ready: (fancybox) => {
                console.log(`fancybox #${fancybox.id} is ready!`);
            }
        }
    });
});
