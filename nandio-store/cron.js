const { execFile } = require('child_process');
const cron = require('cron');
// let { RUN_SYNC_SALEIN_DMS } = require('./sync/dms/salein/salein.batch')
// Models
const TRIAL_PROGRAM_HISTORY_CONTACT_MODEL = require('./www/packages/trial_program/models/trial_program_history_contact').MODEL;
const TRIAL_PROGRAM_MODEL                 = require('./www/packages/trial_program/models/trial_program').MODEL;

/**
 * DANH SÁCH CÁC TÍNH NĂNG cần chạy CRON
 * 	1/ Backup 1 ngày 2 lần: 12h15, 0h15
 *  2/ Chuyển tồn cuối tháng trước -> tồn đầu tháng hiện tại: 00:01 ngày đầu trong tháng
 *  3/ Chạy đơn Salein mỗi ngày (DMS SALEIN -> kpi_salein_actually-coll)
 */

// chức năng BACKUP DB -> S3 (cli s3)
function initCron(time, cb = null) {
    return new cron.CronJob({
        cronTime: time,
        start: true,
        timeZone: 'Asia/Ho_Chi_Minh',
        onTick: function() {
          console.log('Cron job runing...', __dirname);

          if (cb instanceof Function) {
            return cb();
          }

          execFile(__dirname + '/backup/backup_db_to_s3.sh', (error, stdout, stderr) => {
              if (error) {
                return console.error(`error: ${error.message}`);
              }

              if (stderr) {
                return console.error(`stderr:\n${stderr}`);
              }

              console.log(`stdout:\n${stdout}`);
          });
        }
    })
}
function initCronWithoutBackupDB(time, cb = null) {
 
  return new cron.CronJob(time, async function() {
        console.log('Cron job runing without db...', __dirname);

        if (cb instanceof Function) {
          return await cb();
        }
      }
  )
}
/**
 * nhóm chức năng chạy đầu tháng
 */
function runSync() {
//   const syncStore = require('./sync/dms/store/store.import');
//   const syncProduct = require('./sync/dms/product/product.import');
//   const datePrevious = moment().subtract(1, 'months').format('DD/MM/YYYY');

//   // Import store && product
//   syncStore(datePrevious);
//   syncProduct(datePrevious);

  // Convert tồn cuối -> tồn đầu
  require('./sync/inventory_begin_month_convert');
}

/**
 * chức năng đồng bộ đơn hàng DMS
 * 1/ 12h15
 * 2/ 0h15
 */
// function runDMSOrderSalein(){
// 	require('./sync/dms/salein/salein.batch')
// }

// param time with format: 'yyyy-mm-dd'
async function getOrderBookingSuccessInDay() {
  console.log('=============================================================================');
  console.log('============== START CRON JOB PUSH NOTI CALENDAR MEETING TODAY ==============');
  console.log('=============================================================================');

  // let time = '2022-05-26';
  let listTrialProgramHistoryContact = await TRIAL_PROGRAM_HISTORY_CONTACT_MODEL.getListBookingSuccess();
  if(listTrialProgramHistoryContact.data.length) {
    // lấy ra danh sách ID của nhân viên có lịch hẹn với khách hàng hôm nay
    let listEmployeeReceivceID = listTrialProgramHistoryContact.data.map(trialProgramHistoryContact => trialProgramHistoryContact.user_register_trial_program.employee);
    
    let dataMessage = {
      title: 'Thông báo lịch hẹn Trial Program', 
      description: 'Bạn có lịch hẹn xử lý đơn hàng dùng thử với khách hàng hôm nay. Đừng quên bạn nhé', 
      arrReceiverID: listEmployeeReceivceID
    }

    let resultAfterSendMessage = await TRIAL_PROGRAM_MODEL.pushNotiTrialProgram(dataMessage);
    console.log({ resultAfterSendMessage })

    // Lập lịch push thông báo trước 15 phút các buổi hẹn gặp
    let resultSchedule = await TRIAL_PROGRAM_MODEL.schedulePushNotiBeforeMeeting({ 
      listTrialProgramHistoryContact: listTrialProgramHistoryContact.data 
    });
  } else {
    console.log('============================ NO APPOINTMENT TODAY ===========================');
  }

  console.log('=============================================================================');
  console.log('=============== STOP CRON JOB PUSH NOTI CALENDAR MEETING TODAY ==============');
  console.log('=============================================================================');
}

initCron('00 04 * * *', getOrderBookingSuccessInDay).start();

initCron('00 01 00 1 * 0', runSync).start(); // 00:01 ngày đầu trong tháng
initCron('00 15 12 * * 0-6').start(); // Chạy Jobs vào 12h15 trưa
initCron('00 15 00 * * 0-6').start(); // Chạy Jobs vào 00h15 hằng đêm

// initCronWithoutBackupDB('0 * * * *', RUN_SYNC_SALEIN_DMS).start(); // Chạy Jobs vào mỗi giờ
