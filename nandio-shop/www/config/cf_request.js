"use strict";

const express = require('express');
const helmet = require('helmet');
const morganMiddleware = require('./logger/morganMiddleware');

module.exports = function (app) {
    app.use(express.json({ limit: '50mb' }));
    app.use(express.urlencoded({ limit: '50mb', extended: true }));
    app.use(helmet.hidePoweredBy());
    app.use(helmet.xssFilter());
	app.use(morganMiddleware);
};
