const Agenda = require('agenda');
const { MONGODB_URL } = require('../database/intalize/db_connect');

const connectionOpts = {
	db: {
		address: MONGODB_URL, 
		collection: process.env.AGENDA_COLLECTION || "nandio_staging",
		options: { useNewUrlParser: true }	
	},
};

const agenda = new Agenda(connectionOpts);

module.exports = agenda;