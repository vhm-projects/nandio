// KIỂU TÀI KHOẢN
exports.ADMIN_LEVEL = [
    { value: 0, text: 'Editor' },
    { value: 1, text: 'Admin' },
]

// TRẠNG THÁI
exports.ADMIN_STATUS = [
    { value: 0, text: 'Khóa' },
    { value: 1, text: 'Hoạt động' },
]

// KIỂU BÀI VIẾT
exports.POST_TYPE = [
    { value: 0, text: 'Product' },
    { value: 1, text: 'News' },
]

/**
 * MIME Types image
 */
 exports.MIME_TYPES_IMAGE = [ 
	'image/jpeg', 
	'image/pjpeg', 
	'image/png', 
	'image/svg+xml'
];

/**
 * Định nghĩa file collection
 * BIG SALE
 */
exports.DESCRIPTION_BIG_SALE_COLL = {
    name: "Tên khuyến mãi BigSale",
    description: "Mô tả khuyến mãi",
    image: "Hình ảnh khuyến mãi",
    typeDiscount: "Loại giảm giá [1, 2] => ['Giảm theo tiền', 'Giảm theo %']",
    code: "Mã code khuyến mãi",
    maxUsage: "Số lượng mã được sử dụng trong chương trình khuyến mãi này",
    products: "Mảng sản phẩm được sử dụng khuyến mãi",
    amountDiscountByMoney: "Nếu typeDiscount = 1 thì sử dụng trường này",
    amountDiscountByPercent: "Nếu typeDiscount = 2 thì sử dụng trường này",
    status: "[0, 1] => [hết hạn, còn hạng]",
    linkDiscounts: "Là một mảng link được link tới những trang thương mại điện tử khác",
    userCreate: "User tạo khuyến mãi",
    userUpdate: "User update khuyến mãi",
    createAt: "Ngày tạo khuyến mãi"
};

// GIỚI TÍNH
exports.GENDER_TYPE = [
    { value: 0, text: 'Nữ' },
    { value: 1, text: 'Nam' },
    { value: 2, text: 'Khác' },
]

// LOẠI TÀI KHOẢN
exports.CUSTOMER_ACCOUNT_TYPE = [
    { value: 0, text: 'Bình thường' },
    { value: 1, text: 'Google' },
    { value: 2, text: 'Facebook' },
    { value: 3, text: 'Apple' },
]

// LOẠI LỊCH SỬ
exports.HISTORY_POINT_TYPE = [
    { value: 1, text: 'Tích điểm', badge: 'badge badge-light' },
    { value: 2, text: 'Đổi điểm', badge: 'badge badge-primary'  },
    { value: 3, text: 'Đăng ký tài khoản Nhận điểm', badge: 'badge badge-success' },
    { value: 4, text: 'Nhận điểm chuyển tới', badge: 'badge badge-info' },
    { value: 5, text: 'Chuyển điểm', badge: 'badge badge-warning' },
    { value: 6, text: 'Member get member (mã giới thiệu)', badge: 'badge badge-danger' },
    { value: 7, text: 'GLOQ nạp điểm', badge: 'badge badge-dark' },
]

// PHƯƠNG THỨC THANH TOÁN
exports.PAYMENT_TYPE = [
    { value: 1, text: 'Thanh toán qua online', description: "Thanh toán online." },
    { value: 2, text: 'Thanh toán khi nhận hàng', description: 'Khi các bạn cần sự an toàn. Chúng tôi hỗ trợ thanh toán khi nhận được sản phẩm' },
    // { value: 2, text: 'Chuyển khoản ngân hàng' },
]

// TRẠNG THÁI ĐƠN HÀNG
exports.ORDER_STATUS_TYPE = [                   
    { value: 0, text: 'Đang xử lý', color: 'danger' },
    { value: 1, text: 'Đã nhận hàng', color: 'info' },
    { value: 2, text: 'Đang giao', color: 'info' },
    { value: 3, text: 'Đã giao thành công', color: 'success' },
]

exports.TYPE_NOTIFICATION = [     
    // { value: 1, text: 'Thông báo trạng thái đơn hàng' },
    { value: 2, text: 'Thông báo Trial Program' },
    { value: 3, text: 'Thông báo tin tức mới' },
    { value: 4, text: 'Thông báo khuyến mãi' },
    // { value: 5, text: 'Thông báo biến động số dư', color: 'success' },
]

exports.PRODUCT_STATUS = [                   
    { value: 0, text: 'Không hoạt động' },
    { value: 1, text: 'Hoạt động' },
]

exports.REGISTER_TRIAL_PROGRAM_STATUS = [                   
    { value: 0, text: 'Đã Nhận', color: "#E64237" },
    { value: 1, text: 'Đã Xem', color: "#2596be" },
    { value: 2, text: 'Đang Chuyển Hàng', color: "#758c94" },
    { value: 3, text: 'Khách không nhận', color: "#472c81" },
    { value: 4, text: 'Khách đã nhận', color: "#2c8144" },
]

exports.TYPE_CENTRALIZE_ORDER = [                   
    { value: 1, text: 'Đơn hàng' },
    { value: 2, text: 'Hàng dùng thử' },
    { value: 3, text: 'Quà tặng' },
]


exports.STATUS_ORDER = [
    { value: 1, text: 'Chờ xác nhận',        badge: 'badge-light',           icon: 'dripicons-stopwatch', color: '#ffb822' },
    { value: 2, text: 'Đã tiếp nhận',        badge: 'badge-primary',   icon: 'dripicons-archive',   color: '#8aadf6' },
    { value: 3, text: 'Đã xác nhận',         badge: 'badge-success',           icon: 'dripicons-paperclip', color: '#1761fd' },
    { value: 4, text: 'Đã tạo đơn giao vận', badge: 'badge-info',         icon: 'dripicons-plus',      color: '#9ba7ca' },
    { value: 5, text: 'Lưu kho',             badge: 'badge-warning', icon: 'dripicons-store',     color: '#b4b7be' },
    { value: 6, text: 'Đang giao',           badge: 'badge-light',   icon: 'dripicons-basket',    color: '#ffc54a' },
    { value: 7, text: 'Đã giao thành công',  badge: 'badge-dark',              icon: 'dripicons-checkmark', color: '#12a4ed' },
    { value: 8, text: 'Đã hủy',              badge: 'badge-danger',            icon: 'dripicons-wrong',     color: '#f5325c' },
    { value: 9, text: 'Trả hàng',            badge: 'badge-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
]

exports.STATUS_SHIPPING = [
    { value: 'ready_to_pick', text: 'Đơn hàng vận chuyển vừa được tạo',                     badge: 'bg-warning',           icon: 'dripicons-stopwatch', color: '#ffb822' },
    { value: 'picking',       text: 'Shipper đến lấy hàng',                                 badge: 'badge-soft-primary',   icon: 'dripicons-archive',   color: '#8aadf6' },
    { value: 'cancel',        text: 'Đơn hàng vận chuyển đã bị hủy',                        badge: 'bg-primary',           icon: 'dripicons-paperclip', color: '#1761fd' },
    { value: 'money_collect_picking', text: 'Người giao hàng đang tương tác với người bán', badge: 'bg-secondary',         icon: 'dripicons-plus',      color: '#9ba7ca' },
    { value: 'picked',                text: 'Người giao hàng được chọn hàng',               badge: 'badge-soft-secondary', icon: 'dripicons-store',     color: '#b4b7be' },
    { value: 'storing',       text: 'Hàng đã được chuyển đến trung tâm phân loại GHN',      badge: 'badge-soft-warning',   icon: 'dripicons-basket',    color: '#ffc54a' },
    { value: 'transporting',  text: 'Hàng đang được luân chuyển',                           badge: 'bg-info',              icon: 'dripicons-checkmark', color: '#12a4ed' },
    { value: 'sorting',       text: 'Hàng đang được phân loại (tại kho phân loại)',         badge: 'bg-danger',            icon: 'dripicons-wrong',     color: '#f5325c' },
    { value: 'delivering',    text: 'Người giao hàng đang giao hàng cho khách hàng',        badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'money_collect_delivering', text: 'Người giao hàng đang tương tác với người mua', badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'delivered',     text: 'Hàng đã được giao cho khách hàng',                     badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'delivery_fail', text: 'Hàng hóa chưa được giao cho khách hàng',               badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'waiting_to_return', text: 'Hàng đang chờ giao (có thể giao trong vòng 24 / 48h)', badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'return',        text: 'Hàng đang chờ trả lại cho người bán / người bán sau 3 lần giao hàng không thành công', badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'return_transporting', text: 'Hàng đang được luân chuyển',                     badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'return_sorting',text: 'Hàng đang được phân loại (tại kho phân loại)',         badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'returning',     text: 'Người gửi hàng đang trả lại cho người bán',            badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'return_fail',   text: 'Trả hàng thất bại',                                    badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'returned',      text: 'Hàng hóa đã được trả lại cho người bán / người bán',            badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'exception',     text: 'Xử lý ngoại lệ hàng hóa (các trường hợp làm trái quy trình)',            badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'damage',       text: 'Hàng hóa bị hư hỏng',                                   badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
    { value: 'lost',         text: 'Hàng hóa bị mất',                                       badge: 'badge-soft-danger',    icon: 'dripicons-reply-all', color: '#ff0239' },
]
