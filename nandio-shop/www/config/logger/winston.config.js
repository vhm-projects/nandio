const winston 	= require('winston');
require('winston-daily-rotate-file');

const levels = {
	error: 0,
	warn: 1,
	info: 2,
	http: 3,
	debug: 4,
}

const level = () => {
	const env = process.env.NODE_ENV || 'development'
	const isDevelopment = env === 'development'
	return isDevelopment ? 'debug' : 'warn'
}

const colors = {
	error: 'red',
	warn: 'yellow',
	info: 'green',
	http: 'magenta',
	debug: 'white',
}

winston.addColors(colors)

const format = winston.format.combine(
	winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
	winston.format.colorize({ all: true }),
	winston.format.prettyPrint(),
	winston.format.printf(
		(info) => `${info.timestamp} ${info.level}: ${info.message}`,
	),
)

const transports = [
	new winston.transports.Console(),
	new winston.transports.File({
		filename: 'logs/error.log',
		level: 'error',
		maxsize: 5242880,
	}),
	new winston.transports.File({ 
		filename: 'logs/all.log',
		maxsize: 5242880,
	}),
	new winston.transports.DailyRotateFile({
		filename: `logs/%DATE%.log`, // path.join(__dirname, '..', 'logs', `%DATE%.log`),
		datePattern: 'YYYY-MM-DD',
		zippedArchive: true,
		maxSize: '3m',
		maxFiles: '14d'
	})
]

const Logger = winston.createLogger({
	level: level(),
	levels,
	format,
	transports,
})

module.exports = Logger;