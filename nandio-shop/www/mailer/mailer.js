"use strict";

let mailer 			= require("nodemailer");
let cfMailer 		= require('../config/cf_mailer');
let { checkEmail } 	= require('../utils/utils');

module.exports = function (to, subject, content, callback, cc, bbc, attachment) {
	if(checkEmail(to)){
		let smtpTransport = mailer.createTransport("SMTP", {
			service: cfMailer.service,
			auth: {
				user: cfMailer.email,
				pass: cfMailer.password,
			}
		});
	
		let mail = {
			from: 'EXT TRADE',
			to: to,
			subject: subject,
			html: content
		};

		if (cc && cc.length) {
			mail = {
				...mail,
				cc
			}
		}

		if (bbc && bbc.length) {
			mail = {
				...mail,
				bbc
			}
		}

		if (attachment && attachment.length) {
			mail = {
				...mail,
				attachments: attachment
			}
		}

		console.log({
			mail
		});
		smtpTransport.sendMail(mail, function (error, response) {
			if (error) {
				if (callback == null || typeof callback == "undefined") {
				} else {
					console.log({
						error
					});
					callback({error: true, message: "send mail error!"});
				}
			} else {
				if (callback == null || typeof callback == "undefined") {
				} else {
					callback({error: false, message: "send mail success!"});
				}
			}
	
			smtpTransport.close();
		});
	} else{
		console.error('Email invalid');
	}
};