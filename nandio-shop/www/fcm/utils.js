let USER_COLL         = require('../packages/users/databases/user-coll');
let CUSTOMER_DEVICE_COLL     = require('../packages/customer/databases/user_device-coll');
let { sendMessage }   = require('./push-noti.cloudmessaging');

/**
 * FUNCTION gửi noti từ cloud messaging
 * @param {*} arrReceiver danh sách userID (danh sách người nhận noti)
 * @param {*} message 
 * @param {*} body 
 * @param {*} senderID người gửi
 */
function sendMessageMobile({ title, description: message, arrReceiverID, body, senderID: userID }){
    ( async() =>{
        for (const receiver of arrReceiverID){
        // tìm registrationID và gửi thông báo đến mobile
            if(receiver != userID){
                let infoUser    = await USER_COLL.findById(receiver);
                let devices     = infoUser.devices;
                if(devices && devices.length >0){
                    let  arrayRegistrationID  = devices.map(item=> {
                        return item.registrationID
                    })
                    sendMessage({ title,  description: message, arrayRegistrationID, body })
                        .then(resultSendMessage => console.log({ notiCloudMessaging: resultSendMessage }))
                        .catch(err => console.log({ err }))
                }
            }
        }
    })();
}

function sendMessageMobile__Customer({ title, description, arrReceiverID, body, senderID: userID }){
    ( async() =>{
        console.log({
            title, description, arrReceiverID, body,
        });
        for (const receiver of arrReceiverID){
        // tìm registrationID và gửi thông báo đến mobile
            // if(receiver != userID){
                let listCustomerDevice    = await CUSTOMER_DEVICE_COLL.find({ customer: receiver });
               
                if (listCustomerDevice && listCustomerDevice.length) {
                    listCustomerDevice = listCustomerDevice.filter(item => item.registrationID);
                    let  arrayRegistrationID  = listCustomerDevice.map(item=> {
                        if (item.registrationID) {
                            return item.registrationID
                        }
                    });
                    // console.log({
                    //     arrayRegistrationID,
                    //     message,
                    //     title,
                    //     body
                    // });
                    sendMessage({ title,  description, arrayRegistrationID, body })
                        .then(resultSendMessage => console.log({ notiCloudMessaging: resultSendMessage }))
                        .catch(err => console.log({ err }))
                }
                
            // }
        }
    })();
}

exports.sendMessageMobile = sendMessageMobile;
exports.sendMessageMobile__Customer = sendMessageMobile__Customer;
// ----------------PLAYGROUND-------------------//
// let arrReceiverID = ['60ab5729b436165fe06fd141'];
// let title = 'HELLO WORLD - K360';
// let description = 'CHAY NGAY DI';
// let senderID = 'KHANHNEY'
// let body = {
//     screen_key: 'Setting',
//     sender: senderID,
//     transactionID: 'abc'
// };

// sendMessageMobile({ title, description, arrReceiverID, senderID, body })