"use strict";

const express 			= require('express');
const moment 			= require('moment');
const lodash			= require('lodash');
const url 				= require('url');
const formatCurrency 	= require('number-format.js');

/**
 *	INTERNAL PACKAGE
 */
require('../../app');
const { CF_ROUTINGS_COMMON } 	 				= require('../packages/common/constants/common.uri');
const { CF_ROUTINGS_USER } 	 					= require('../packages/users/constants/user.uri');
const { CF_ROUTINGS_CUSTOMER } 					= require('../packages/customer/constants/customer.uri');
const { CF_ROUTINGS_CATEGORY } 					= require('../packages/category/constants/category.uri');
const { CF_ROUTINGS_BRAND }    					= require('../packages/brand/constants/brand.uri');
const { CF_ROUTINGS_BLOG } 	 					= require('../packages/blog/constants/blog.uri');
const { CF_ROUTINGS_PROMOTION }					= require('../packages/promotion/constants/promotion.uri');
const { CF_ROUTINGS_PRODUCT } 					= require('../packages/product/constants/product.uri');
const { CF_ROUTINGS_SEGMENT }  					= require('../packages/segment/constants/segment.uri');
const { CF_ROUTINGS_ORDER } 					= require('../packages/order/constants/order.uri');
const { CF_ROUTINGS_TRANSACTION } 				= require('../packages/transaction/constant/transaction.uri');
const { CF_ROUTINGS_HISTORY_POINT, CF_ROUTINGS_DELIVERY_POINT_CONFIG } 			= require('../packages/history_point/constants/history_point.uri');
const { CF_ROUTINGS_SLIDE } 			        = require('../packages/slide/constants/slide.uri');
const { CF_ROUTINGS_STORE } 			        = require('../packages/store/constants/store.uri');
const { CF_ROUTINGS_RANKING } 			        = require('../packages/ranking/constants/ranking.uri');
const { CF_ROUTINGS_REWARD } 			        = require('../packages/reward/constants/reward.uri');
const { CF_ROUTINGS_PRODUCT_QR } 			    = require('../packages/product_qr/constants/product_qr.uri');
const { CF_ROUTINGS_PRODUCT_POINT } 			= require('../packages/product_point/constants/product_point.uri');
const { CF_ROUTINGS_TRIAL_PROGRAM } 			= require('../packages/trial_program/constants/trial_program.uri');
const { CF_ROUTINGS_SKU_2 } 			        = require('../packages/sku-2/constants/sku_2.uri');
const { CF_ROUTINGS_PARCEL } 			        = require('../packages/sku-2/constants/parcel.uri');
const { CF_ROUTINGS_VERSION } 			        = require('../packages/version/constants/version.uri');
const { CF_ROUTINGS_NOTIFICATION } 			    = require('../packages/notification/constants/notification.uri');

const { 
	GENDER_TYPE, CUSTOMER_ACCOUNT_TYPE, HISTORY_POINT_TYPE, PAYMENT_TYPE, ORDER_STATUS_TYPE
} = require('../config/cf_constants');

const { loadPathImage, getExtension, checkValidStartTimeVsEndTime } 			= require('../utils/utils');


class ChildRouter{
    constructor(basePath) {
        this.basePath = basePath;
        this.registerRouting;
    }

    registerRouting() {
    }
    exportModule() {
        let router = express.Router();
        
        for (let basePath in this.registerRouting()) {
            const item = this.registerRouting()[basePath];
    
            if (typeof item.methods.post !== 'undefined' && item.methods.post !== null) {
                if (item.methods.post.length === 1) {
                    router.post(basePath, item.methods.post[0]);
                } else if (item.methods.post.length === 2) {
                    router.post(basePath, item.methods.post[0], item.methods.post[1]);
                }
            }

            if (typeof item.methods.get !== 'undefined' && item.methods.get !== null) {
                if (item.methods.get.length === 1) {
                    router.get(basePath, item.methods.get[0]);
                } else if (item.methods.get.length === 2) {
                    router.get(basePath, item.methods.get[0], item.methods.get[1]);
                }
            }

            if (typeof item.methods.put !== 'undefined' && item.methods.put !== null) {
                if (item.methods.put.length === 1) {
                    router.put(basePath, item.methods.put[0]);
                } else if (item.methods.put.length === 2) {
                    router.put(basePath, item.methods.put[0], item.methods.put[1]);
                }
            }

            if (typeof item.methods.delete !== 'undefined' && item.methods.delete !== null) {
                if (item.methods.delete.length === 1) {
                    router.delete(basePath, item.methods.delete[0]);
                } else if (item.methods.delete.length === 2) {
                    router.delete(basePath, item.methods.delete[0], item.methods.delete[1]);
                }
            }

        }
        return router;
    }

    /**
     * 
     * @param {*} msg 
     * @param {*} res 
     * @param {*} data 
     * @param {*} code
     * @param {*} status  tham số nhận biết lỗi
     */
    static responseError(msg, res, data, code, status) {
        if (code) {
            res.status(code);
        }
        return res.json({error: true, message: msg, data: data, status: status});
    }

    static response(response, data) {
        return response.json(data);
    }


    static responseSuccess(msg, res, data, code) {
        if (code) {
            res.status(code);
        }

        return res.json({error: false, message: msg, data: data});
    }

    static pageNotFoundJson(res) {
        res.status(404);
        return res.json({"Error": "Page not found!"});
    }

     /**
     * 
     * @param {*} req 
     * @param {*} res 
     * @param {*} data 
     * @param {*} title 
     */
    static renderToView(req, res, data, title) {
        data = typeof data === 'undefined' || data === null ? {} : data;

        if (title) {
            res.bindingRole.config.title = title;
        }
        
        data.render = res.bindingRole.config;
        data.url = url.format({
            protocol: req.protocol,
            host: req.get('host'),
            pathname: req.originalUrl
        });
		data.hrefCurrent = req.originalUrl;

		//_________Thư viện thời gian
		data.moment = moment;

		//_________Thư viện tiện ích
		data.lodash = lodash;

		//_________Thư viện số
        data.formatCurrency = formatCurrency;

		if(req.user){
			data.infoUser = req.user;
		}

		// PREFIX URL
        data.CF_ROUTINGS_COMMON   	 = CF_ROUTINGS_COMMON;
		data.CF_ROUTINGS_USER	  	 = CF_ROUTINGS_USER; 
		data.CF_ROUTINGS_CUSTOMER 	 = CF_ROUTINGS_CUSTOMER;
		data.CF_ROUTINGS_CATEGORY 	 = CF_ROUTINGS_CATEGORY; 
		data.CF_ROUTINGS_BRAND    	 = CF_ROUTINGS_BRAND; 
		data.CF_ROUTINGS_BLOG 	  	 = CF_ROUTINGS_BLOG;
        data.CF_ROUTINGS_PROMOTION	 = CF_ROUTINGS_PROMOTION;
        data.CF_ROUTINGS_PRODUCT     = CF_ROUTINGS_PRODUCT;
		data.CF_ROUTINGS_SEGMENT	 = CF_ROUTINGS_SEGMENT;
		data.CF_ROUTINGS_ORDER		 = CF_ROUTINGS_ORDER;
		data.CF_ROUTINGS_TRANSACTION = CF_ROUTINGS_TRANSACTION;
		data.CF_ROUTINGS_HISTORY_POINT = CF_ROUTINGS_HISTORY_POINT;
		data.CF_ROUTINGS_SLIDE      = CF_ROUTINGS_SLIDE;
		data.CF_ROUTINGS_STORE      = CF_ROUTINGS_STORE;
		data.CF_ROUTINGS_RANKING     = CF_ROUTINGS_RANKING;
		data.CF_ROUTINGS_REWARD      = CF_ROUTINGS_REWARD;
		data.CF_ROUTINGS_PRODUCT_QR  = CF_ROUTINGS_PRODUCT_QR;
		data.CF_ROUTINGS_PRODUCT_POINT  = CF_ROUTINGS_PRODUCT_POINT;
		data.CF_ROUTINGS_TRIAL_PROGRAM  = CF_ROUTINGS_TRIAL_PROGRAM;
		data.CF_ROUTINGS_SKU_2       = CF_ROUTINGS_SKU_2;
		data.CF_ROUTINGS_PARCEL      = CF_ROUTINGS_PARCEL;
		data.CF_ROUTINGS_VERSION      = CF_ROUTINGS_VERSION;
		data.CF_ROUTINGS_DELIVERY_POINT_CONFIG      = CF_ROUTINGS_DELIVERY_POINT_CONFIG;
		data.CF_ROUTINGS_NOTIFICATION      = CF_ROUTINGS_NOTIFICATION;
        
		// CONSTANTS
		data.GENDER_TYPE 			= GENDER_TYPE;
		data.CUSTOMER_ACCOUNT_TYPE 	= CUSTOMER_ACCOUNT_TYPE;
		data.HISTORY_POINT_TYPE		= HISTORY_POINT_TYPE;
		data.PAYMENT_TYPE			= PAYMENT_TYPE;
		data.ORDER_STATUS_TYPE		= ORDER_STATUS_TYPE;
		data.AWS_S3_URI 			= process.env.AWS_S3_URI;

        let langDemo = 'en';
        data.langKey = langDemo;

		data.loadPathImage  = loadPathImage;
		data.getExtension 	= getExtension;
        data.checkValidStartTimeVsEndTime = checkValidStartTimeVsEndTime;

        // =============BIND ALL DATA TO VIEW=============//
        return res.render(res.bindingRole.config.view, data);
    }

    static renderToPath(req, res, path, data) {
        data = data == null ? {} : data;
        data.render = res.bindingRole.config;
        return res.render(path, data);
    }

    static renderToViewWithOption(req, res, pathRender, data) {
        return res.render(pathRender, {data});
    }

    static redirect(res, path) {
        return res.redirect(path);
    }
}

module.exports = ChildRouter;