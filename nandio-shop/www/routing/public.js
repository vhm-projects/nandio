"use strict";

const mailjet = require('node-mailjet');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('./child_routing');
const roles                                 = require('../config/cf_role');

/**
 * MODELS
 */

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {

            /**
             * Function: Demo send mailjet (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            '/send-mailjet': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const mailjetConnect = mailjet.connect(process.env.MJ_APIKEY_PUBLIC, process.env.MJ_APIKEY_PRIVATE);

                        const request = mailjetConnect
                            .post("send", {'version': 'v3.1'})
                            .request({
                                "Messages": [
                                    {
                                        "From": {
                                            "Email": "pilot@mailjet.com",
                                            "Name": "Employee Funny!!"
                                        },
                                        "To": [
                                            {
                                                "Email": "taolaolataom@gmail.com",
                                                "Name": "Customer Happy!"
                                            }
                                        ],
                                        "Subject": "Your email flight plan!",
                                        "TextPart": "Dear passenger 1, welcome to Mailjet! May the delivery force be with you!",
                                        "HTMLPart": "<h3>Dear passenger 1, welcome to <a href=\"https://www.mailjet.com/\">Mailjet</a>!</h3><br />May the delivery force be with you!",
                                        "CustomCampaign": "SendAPI_campaign",
                                        "DeduplicateCampaign": true
                                    }
                            ]
                        });

                        request
                            .then((result) => {
                                console.log({ BODY: result.body })
                                res.json(result.body)
                            })
                            .catch((err) => {
                                console.log({ ERROR: err, CODE: err.statusCode })
                                res.json(err)
                            })
                    }]
                },
            },

            

        }
    }
};
