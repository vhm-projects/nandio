const BASE_ROUTE = '/product';
const BASE_ROUTE_API = '/api/product';

const CF_ROUTINGS_PRODUCT = {
    // USER PERMISSION
	ADD_PRODUCT:        `${BASE_ROUTE}/add-product`,
	LIST_PRODUCT:       `${BASE_ROUTE}/list-product`,
	INFO_PRODUCT:       `${BASE_ROUTE}/info-product/:productID`,
    UPDATE_PRODUCT:     `${BASE_ROUTE}/update-product/:productID`,
    UPDATE_PRODUCT_API: `/api${BASE_ROUTE}/update-product/:productID`,
    UPDATE_PRODUCT_STATUS_API: `/api${BASE_ROUTE}/update-product-status`,
    UPDATE_PRODUCT_SHOW_QR_API: `/api${BASE_ROUTE}/update-product-show-qr`,
    UPDATE_PRODUCT_IS_TRIAL_PROGRAM_API: `/api${BASE_ROUTE}/update-product-is-trial-program`,
    UPDATE_PRODUCT_IS_GIFT_API: `/api${BASE_ROUTE}/update-product-is-gift`,
    UPDATE_PRODUCT_STT_API: `/api${BASE_ROUTE}/update-product-stt`,
    DELETE_PRODUCT:     `${BASE_ROUTE}/delete-product/:productID`,

	UPDATE_ORDER_IMAGE_PRODUCT: `${BASE_ROUTE}/update-order-image-product`,

	LIST_PRODUCT_PAGINATION:            `${BASE_ROUTE_API}/list-product-pagination`,

    /**============================API MOBILE ======================== */
	LIST_PRODUCT_CATEGORY_API:          `${BASE_ROUTE_API}/list-product-category/:categoryID`,
	LIST_PRODUCT_BRAND_API:             `${BASE_ROUTE_API}/list-product-brand/:brandID`,
	LIST_PRODUCT_SEGMENT_API:           `${BASE_ROUTE_API}/list-product-segment/:segmentID`,
	INFO_PRODUCT_API:                   `${BASE_ROUTE_API}/info-product/:productID`,
	LIST_PRODUCT_FILTER:                `${BASE_ROUTE_API}/list-product`,
	LIKE_PRODUCT_API:                   `${BASE_ROUTE_API}/like-product/:productID`,
	LIST_PRODUCT_USER_SEGMENT:          `${BASE_ROUTE_API}/list-product-user-segment`,
	LIST_PRODUCT_USER_LIKE_PRODUCT:     `${BASE_ROUTE_API}/list-product-user-like-product`,
	LIST_PRODUCT_NEW_API:               `${BASE_ROUTE_API}/list-product-new`,
	LIST_PRODUCT_SEGMENT_CATEGORY_API:  `${BASE_ROUTE_API}/list-product-segment-category`,
	LIST_PRODUCT_SEGMENTS_CATEGORY_API: `${BASE_ROUTE_API}/list-product-segments-category`,
	LIST_PRODUCT_FILTER_KEYWORD:        `${BASE_ROUTE_API}/list-product-filter`,

	// LIST_PRODUCT_SEGMENT_CATEGORY_API: `${BASE_ROUTE_API}/list-product-segment-category`,
	LIST_PRODUCT_SEGMENT_USER_CATEGORY_API: `${BASE_ROUTE_API}/list-product-user-segment-category`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_PRODUCT = CF_ROUTINGS_PRODUCT;
