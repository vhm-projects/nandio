"use strict";
const path = require('path');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { CF_ROUTINGS_PRODUCT } 				= require('../constants/product.uri');
const { PRODUCT_STATUS } 				= require('../../../config/cf_constants');

/**
 * MODELS
 */
const PRODUCT_MODEL        = require('../models/product').MODEL;
const { IMAGE_MODEL }      = require('../../image');
const { CATEGORY_MODEL }   = require('../../category');
const { BRAND_MODEL }      = require('../../brand');
const { SEGMENT_MODEL }    = require('../../segment');
const SKU_2_MODEL          = require('../../sku-2/models/sku_2').MODEL;

/**
 * COLLECTIONS
 */
const PRODUCT_COLL = require('../databases/product-coll');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ==========================      QUẢN LÝ PRODUCT     ================================
             * ========================== ************************ ================================
             */

			/**
             * Function: TẠO SẢN PHẨM API
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.ADD_PRODUCT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_PRODUCT.ADD_PRODUCT,
                    inc : path.resolve(__dirname, '../views/add_product.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [async (req, res) => {
                        /**
                         *  1// LẤY DANH MỤC CỦA SẢN PHẨM
                         */
                        let listCategory = await CATEGORY_MODEL.getList();

                        /**
                         *  2// LẤY THƯƠNG HIỆU
                         */
                        let listBrand    = await BRAND_MODEL.getList();

                         /**
                         *  3// LẤY SEGMENT 
                         */
                        let listSegment  = await SEGMENT_MODEL.getList({});

                        ChildRouter.renderToView(req, res, { 
                           listCategory: listCategory.data, 
                           listBrand:    listBrand.data,
                           listSegment:  listSegment.data 
                        });
                    }],
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                
                        const { 
                            name, description, content, price, category, brand, SKU, avatar, images, segments,
                            sourceMaterial,
                            importedBy,
                            madeBy,
                            distributor,
                            certifiedImages
                        } = req.body;

                        if(!avatar) {
                            return res.json({ error: true, message: "image_invalid" });
                        }
                     
                        const infoAfterInsertAccount = await PRODUCT_MODEL.insert({ 
                            name, description, content, price, category, brand, SKU, avatar, images: images, userID, segments,
                            sourceMaterial,
                            madeBy,
                            importedBy,
                            distributor,
                            certifiedImages
                        });

                        res.json(infoAfterInsertAccount);
                    }]
                },
            },

            /**
             * Function: LIST SẢN PHẨM 
             * Dev: SONLP
             */
            [CF_ROUTINGS_PRODUCT.LIST_PRODUCT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_PRODUCT.LIST_PRODUCT,
                    inc : path.resolve(__dirname, '../views/list_product.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { category, brand, price, fromDay, toDay, status } = req.query;
                        /**
                         *  1// LẤY DANH MỤC CỦA SẢN PHẨM
                         */
                        let listCategory = await CATEGORY_MODEL.getList();

                        /**
                         *  2// LẤY THƯƠNG HIỆU
                         */
                        let listBrand    = await BRAND_MODEL.getList();

                        // let listProduct = await PRODUCT_MODEL.filter({ category, brand, price, fromDay, toDay });

                        ChildRouter.renderToView(req, res, { 
                            // listProduct:  listProduct.data, 
                            listCategory: listCategory.data,
                            listBrand:    listBrand.data,
                            categoryFilter: category,
                            brandFilter: brand,
                            price,
                            fromDay,
                            toDay,
                            status,
                            PRODUCT_STATUS
                         });
                    }]
                },
            },

            /**
             * Function: INFO SẢN PHẨM
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.INFO_PRODUCT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_PRODUCT.INFO_PRODUCT,
                    inc : path.resolve(__dirname, '../views/info_product.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { productID }   = req.params;

                        let infoProduct = await PRODUCT_MODEL.getInfoV2({ productID });
                        ChildRouter.renderToView(req, res, { 
                            infoProduct: infoProduct.data, 
                        });
                    }]
                },
            },

            /**
             * Function: DELETE SẢN PHẨM API
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.DELETE_PRODUCT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { productID }   = req.params;

                        let infoProduct = await PRODUCT_MODEL.delete({ productID });

                        return res.json(infoProduct)
                    }]
                },
            },
            /**
             * Function: TẠO SẢN PHẨM API
             * Dev: SONLP
             */
            [CF_ROUTINGS_PRODUCT.UPDATE_PRODUCT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_PRODUCT.UPDATE_PRODUCT,
                    inc : path.resolve(__dirname, '../views/update_product.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [async (req, res) => {
                        let { productID }   = req.params;

                        let infoProduct = await PRODUCT_MODEL.getInfo({ productID });
                        if (infoProduct.error) {
                            res.json(infoProduct)
                        }
                        
                        /**
                         *  1// LẤY DANH MỤC CỦA SẢN PHẨM
                         */
                        let listCategory = await CATEGORY_MODEL.getList();

                        /**
                         *  2// LẤY THƯƠNG HIỆU
                         */
                        let listBrand    = await BRAND_MODEL.getList();

                         /**
                         *  3// LẤY SEGMENT 
                         */
                        let listSegment  = await SEGMENT_MODEL.getList({});

                        let listSku2     = await SKU_2_MODEL.getList({  });

                        ChildRouter.renderToView(req, res, { 
                           listCategory: listCategory.data, 
                           listBrand:    listBrand.data,
                           listSegment:  listSegment.data,
                           infoProduct:  infoProduct.data.infoProduct,
                           listSku2:     listSku2.data
                        });
                    }]
                },
            },

            [CF_ROUTINGS_PRODUCT.UPDATE_PRODUCT_API]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { productID }   = req.params;
                        const { name, description, content, status, price, category, 
                            brand, SKU, SKUV2, avatar, images, segment, imageDelete, avatarDelete,
                            sourceMaterial,
                            importedBy,
                            madeBy,
                            distributor,
                            listCertifiedImagesNew,
                            listCertifiedImagesDelete,
                            weight,
                            length,
                            width,
                            height,
                        } = req.body;

                        const infoAfterInsertAccount = await PRODUCT_MODEL.update({ 
                            productID, name, description, content, status, price, category, brand, SKU, SKUV2, avatar, images: images, userID, segments: segment, imageDelete, avatarDelete,
                            sourceMaterial,
                            madeBy,
                            importedBy,
                            distributor,
                            listCertifiedImagesNew,
                            listCertifiedImagesDelete,
                            weight,
                            length,
                            width,
                            height,
                        });

                        res.json(infoAfterInsertAccount);
                    }]
                },
            },

            [CF_ROUTINGS_PRODUCT.UPDATE_PRODUCT_STATUS_API]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { status, productID } = req.body;

                        const infoAfterInsertAccount = await PRODUCT_MODEL.updateStatus({ 
                            productID, status
                        });

                        res.json(infoAfterInsertAccount);
                    }]
                },
            },

            [CF_ROUTINGS_PRODUCT.UPDATE_PRODUCT_SHOW_QR_API]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { show_qr, productID, SKUV2 } = req.body;

                        const infoAfterUpdate = await PRODUCT_MODEL.updateShowQR({ productID, show_qr, SKUV2 });

                        res.json(infoAfterUpdate);
                    }]
                },
            },

            [CF_ROUTINGS_PRODUCT.UPDATE_PRODUCT_IS_TRIAL_PROGRAM_API]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { isTrialProgram, productID } = req.body;
                        const infoAfterInsertAccount = await PRODUCT_MODEL.updateIsTrialProgram({ 
                            productID, isTrialProgram
                        });
                        res.json(infoAfterInsertAccount);
                    }]
                },
            },
            
            [CF_ROUTINGS_PRODUCT.UPDATE_PRODUCT_IS_GIFT_API]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { isGift, productID } = req.body;
                        const infoAfterInsertAccount = await PRODUCT_MODEL.updateIsGift({ 
                            productID, isGift
                        });
                        res.json(infoAfterInsertAccount);
                    }]
                },
            },

            [CF_ROUTINGS_PRODUCT.UPDATE_PRODUCT_STT_API]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { stt, productID } = req.body;

                        const infoData = await PRODUCT_MODEL.updateSTT({ 
                            productID, stt
                        });

                        res.json(infoData);
                    }]
                },
            },

            /**<================================ API MOBILE =================================> */
            /**
             * Function: LIST SẢN PHẨM VỚI CATEGORYID API
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_CATEGORY_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        // let { _id: userID } = req.user;
                        let { categoryID }     = req.params;
                        let { page, limit } = req.query;

                        let listProduct = await PRODUCT_MODEL.getListProductCategory({ category: categoryID, page, limit });

                        res.json(listProduct)
                    }]
                },
            },

            [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_PAGINATION]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        // let { _id: userID } = req.user;
                        // let { categoryID }     = req.params;
                        // let { page, limit } = req.query;
                        let { category, brand, price, fromDay, toDay, status, start, length, search } = req.body;
                        let page = Number(start)/Number(length) + 1;
                        // console.log({
                        //     category, brand, price, fromDay, toDay, start, length, page, search
                        // });
                       
                        let listProduct = await PRODUCT_MODEL.filterV2({ 
                            category, 
                            brand, 
                            price, 
                            fromDay, 
                            toDay, 
                            status,
                            page: Number(page), 
                            limit: Number(length), 
                            keyword: search.value 
                        });
                       
                        // let listProduct = await PRODUCT_MODEL.getListProductCategory({ category: categoryID, page, limit });
                        res.json(listProduct)
                    }]
                },
            },

            /**
             * Function: LIST SẢN PHẨM VỚI BRANDID API
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_BRAND_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        // let { _id: userID } = req.user;
                        let { brandID }     = req.params;
                        let { page, limit } = req.query;

                        let listProduct = await PRODUCT_MODEL.getListProductBrand({ brand: brandID, page, limit });

                        res.json(listProduct)
                    }]
                },
            },

            /**
             * Function: LIST SẢN PHẨM VỚI SEGMENT API
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_SEGMENT_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        // let { _id: userID } = req.user;
                        let { segmentID }     = req.params;
                        let { page, limit, categoryID } = req.query;

                        let listProduct = await PRODUCT_MODEL.getListProductSegment({ segment: segmentID, page, limit, categoryID });

                        res.json(listProduct)
                    }]
                },
            },
            

            /**
             * Function: INFO SẢN PHẨM (API)
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.INFO_PRODUCT_API]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.customer;
                        let { productID }   = req.params;
                        let infoProduct = await PRODUCT_MODEL.getInfoProduct({ productID, userID });
                        res.json(infoProduct);
                    }]
                },
            },

            /**
             * Function: TÌM KIẾM SẢN PHẨM THEO TÊN CÓ PHÂN TRANG
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_FILTER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.customer;
                        let { keyword, page, limit, categoryID, brandID, type, segmentID, like, typeRequest } = req.query;

                        if(typeRequest){
                            let conditionObj = { status: 1 };

                            if(keyword){
                                let key = keyword.split(" ");
                                key = '.*' + key.join(".*") + '.*';

                                conditionObj.$or = [
                                    { productCode: new RegExp(key, 'i') },
                                    { name: new RegExp(key, 'i') },
                                ]
                            }

                            let listProducts = await PRODUCT_COLL
                                .find(conditionObj)
                                .select('_id stt productCode name modifyAt createAt')
                                .sort({ stt: 1, modifyAt: -1, createAt: -1 })
                                .lean();

                            return res.json(listProducts);
                        }
                        
                        let listProducts = await PRODUCT_MODEL.checkTypeAPI({ keyword, page, limit, categoryID, brandID, type, segmentID, like, userID });

                        res.json(listProducts)
                    }]
                },
            },

            /**
             * Function: TÌM KIẾM SẢN PHẨM THEO TÊN CÓ PHÂN TRANG
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_FILTER_KEYWORD]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        // let { _id: userID } = req.user;
                        let { keyword, page, limit, categoryID, brandID } = req.query;
                        
                        // let listProduct = await PRODUCT_MODEL.checkTypeAPI({ userID, keyword, page, limit, categoryID, brandID, type });
                        let listProduct = await PRODUCT_MODEL.filterByKeyword({ keyword, page, limit, categoryID, brandID });

                        res.json(listProduct)
                    }]
                },
            },

            /**
             * Function: YÊU THÍCH SẢN PHẨM
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.LIKE_PRODUCT_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID }          = req.customer;
                        let { productID }            = req.params;

                        let listProduct = await PRODUCT_MODEL.likeProduct({ productID, userID });

                        res.json(listProduct)
                    }]
                },
            },

            /**
             * Function: DANH SÁCH SẢN PHẨM YÊU THÍCH USER SEGMENT 
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_USER_SEGMENT]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.customer;
                        let { page, limit } = req.query;

                        let listProduct = await PRODUCT_MODEL.getListProductCategory({ userID, page, limit });

                        res.json(listProduct)
                    }]
                },
            },

            /**
             * Function: DANH SÁCH SẢN PHẨM YÊU THÍCH USER LIKE PRODUCT
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_USER_LIKE_PRODUCT]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json', 
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.customer;
                        let { page, limit, categoryID, brandID } = req.query;

                        let listProduct = await PRODUCT_MODEL.getListProductUserLikeProduct({ userID, page, limit, categoryID, brandID });

                        res.json(listProduct)
                    }]
                },
            },

            /**
             * Function: DANH SÁCH SẢN PHẨM SẮP XẾP THEO THỜI GIAN
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_NEW_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        // let { _id: userID } = req.customer;
                        let { page, limit, createAt } = req.query;

                        let listProduct = await PRODUCT_MODEL.getListCreateAt({ createAt, page, limit });

                        res.json(listProduct)
                    }]
                },
            },

            /**
             * Function: DANH SÁCH SẢN PHẨM CỦA SEGMENT CỦA CATEGORY
             * Dev: SONLP
             */
            // [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_SEGMENT_CATEGORY_API]: {
            //     config: {
            //         auth: [ roles.role.customer.bin ],
            //         type: 'json',
            //     },
            //     methods: {
            //         get: [ async function (req, res) {
            //             // let { _id: userID } = req.customer;
            //             let { page, limit, categoryID } = req.query;

            //             let listProduct = await PRODUCT_MODEL.getListProductSegmentCategory({ categoryID, page, limit });

            //             res.json(listProduct)
            //         }]
            //     },
            // },

            /**
             * Function: DANH SÁCH SẢN PHẨM CỦA SEGMENT CỦA CATEGORY
             * Dev: SONLP
             */
            [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_SEGMENT_CATEGORY_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        // let { _id: userID } = req.customer;
                        let { page, limit, categoryID } = req.query;

                        let listProduct = await PRODUCT_MODEL.getListProductSegmentCategoryScreenS10({ categoryID, page, limit });

                        res.json(listProduct)
                    }]
                },
            },


            /**
             * Function: DANH SÁCH SẢN PHẨM CỦA CÁC SEGMENT CỦA CATEGORY S08
             * Dev: SONLP
             */
            [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_SEGMENTS_CATEGORY_API]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        // let { _id: userID } = req.customer;
                        let { page, limit, keyword, categoryID, brandID } = req.query;
                        let listProduct = await PRODUCT_MODEL.getListProductSegmentsCategoryScreenS07({ keyword, categoryID, brandID, page, limit });
                        res.json(listProduct)
                    }]
                },
            },

            /**
             * Function: DANH SÁCH SẢN PHẨM CỦA SEGMENT CỦA CATEGORY VÀ USER
             * Dev: SONLP
             */
             [CF_ROUTINGS_PRODUCT.LIST_PRODUCT_SEGMENT_USER_CATEGORY_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.customer;
                        let { page, limit, keyword, categoryID, brandID } = req.query;

                        let listProduct = await PRODUCT_MODEL.getListProductSegmentUserCategory({ userID, keyword, categoryID, brandID, page, limit });

                        res.json(listProduct)
                    }]
                },
            },

            /**
             * Update order
             * Dattv
             */
             [CF_ROUTINGS_PRODUCT.UPDATE_ORDER_IMAGE_PRODUCT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { arrayObject } = req.body;
                        let infoResult = await IMAGE_MODEL.updateOrder({ arrayObject });
                        res.json(infoResult);
                    }]
                },
            },
        }
    }
};
