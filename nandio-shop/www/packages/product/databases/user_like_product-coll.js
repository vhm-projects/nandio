"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../database/intalize/base-coll');

/**
 * COLLECTION user_like_product CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('user_like_product', {
	product: {
		type: Schema.Types.ObjectId,
		ref: 'product',
	},
    /**
	 * Segment
	 */
	user: {
		type: Schema.Types.ObjectId,
		ref: 'customer',
	},
	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},
});
