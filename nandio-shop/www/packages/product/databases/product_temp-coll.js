"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

// sử dụng tạm cho việc import dữ liệu
/**
 * SẢN PHẨM
 */
module.exports = BASE_COLL('product_import', {
    id: String,
    code: String,
    name: String,
    brandCode: String,
    brandName: String,
    variant: String,
    convfact: String,
    uom: String,
    description: String,
    price: String,
    priceP1: String,
    priceP2: String,
    updateDate: String,
});
