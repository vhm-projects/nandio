"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../database/intalize/base-coll');

/**
 * COLLECTION user_segment CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('user_segment', {
	/**
	 * Segment
	 */
	segment: {
		type: Schema.Types.ObjectId,
		ref: 'segment',
	},
	user: {
		type: Schema.Types.ObjectId,
		ref: 'customer',
	},
	/**
	 * USER_SEGMENT
	 * 1. Do Customer Like Product
	 * 0. Do Admin Tạo
	 */
	type: {
		type: Number,
		default: 0
	},
	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},
});
