"use strict";

const Schema    = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * SẢN PHẨM
 */
module.exports = BASE_COLL('product', {
	// start (import mysql)
	idSQL: {
		type: String,
	},
	productCode: {
		type: String,
	},
	convfact: {
		type: Number,
	},
	uom: {
		type: String,
	},
	priceP1: {
		type: Number,
	},
	priceP2: {
		type: Number,
	},
	// end (import mysql)
	name: {
		type: String,
	}, 
	description: {
		type: String,
	},

	content: {
		type: String,
	},
	/**
	 * Giá sản phẩm
	 */
	price: {
		type: Number,
	},
	point: {
		type: Number,
	},
	/**
	 * Điểm (Lịch sử thay đổi của điểm)
	 */
	// Tạo collect mới point_product_history
	// points: [{
	// 	point: Number,
	// 	timeUpdate: {
	// 		type: Date,
	// 		default: new Date()
	// 	}
	// }],
	/**
	 * Danh mục sản phẩm
	 */
	category: {
		type: Schema.Types.ObjectId,
		ref: 'category',
	},
	
	sku_2: {
		type: Schema.Types.ObjectId,
		ref: 'sku_2',
	},
	/**
	 * Thương hiệu
	 */
	brand: {
		type: Schema.Types.ObjectId,
		ref: 'brand',
	},
	/**
	 * Thương hiệu
	 */
	segments: [{
		type: Schema.Types.ObjectId,
		ref: 'segment',
	}],
	/**
	 * Mã QR
	 */
	qr_code: {
		type: Schema.Types.ObjectId,
		ref: 'qr_code',
	},
	// STT HIỂN THỊ
	stt : {
		type: Number,
	},
	
	SKU: {
		type: String,
	},
	// SKUV2: {
	// 	type: String,
	// },
	/**
	 * Trạng thái hoạt động.
	 * 1. Quét
	 * 2. Không quét
	 */
	show_qr: {
		type: Number,
		default: 2
	},
	/**
	 * Hình ảnh đại diện
	 */
	avatar: {
		type: Schema.Types.ObjectId,
		ref: 'image',
	},
	/**
	 * Hình ảnh 
	 */
	images: [{
		type: Schema.Types.ObjectId,
		ref: 'image',
	}],
	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},

	/**
	 * DELETE
	 * 1. Hoạt động 
	 * 2: Xoá
	 */
	state: {
		type: Number,
		default: 1
	},
	/**
	 * Sản phẩm thử nghiệm
	 * 1. Sản phẩm thử nghiệm
	 * 2. Sản phẩm bình thường
	 */
	isTrialProgram: {
		type: Number,
		default: 2
	},
	
	/**
	 * Sản phẩm quà tặng
	 * 1. Sản phẩm quà tặng
	 * 2. Sản phẩm bình thường
	 */
	isGift: {
		type: Number,
		default: 2
	},

	/**
	 * Khối lượng tịnh
	 */
	netWeight: {
		type: Number,
		default: 0
	},
	/**
	 * Thông tin website
	 */
	website: String,
	/**
	 * Nguồn nguyên liệu
	 */
	sourceMaterial: String,
	/**
	 * Sản xuất bởi
	 */
	madeBy: String,
	/**
	 * Nhập khẩu bởi
	 */
	importedBy: String,
	/**
	 * Phân phối bởi
	 */
	distributor: String,
	/**
	 * Ngày sản xuất
	 */
	manufactureDate: Date,
	/**
	* Ngày hết hạn
	*/
	expiredTime: Date,
	/**
	 * Hình ảnh chứng nhận
	 */
	certifiedImages: [
		{
			type: Schema.Types.ObjectId,
			ref: 'image',
		}
	],

	/**
	 * Trọng lượng (gram)
	 */
	weight: {
		type: Number,
		default: 0
    },
	/**
	 * Độ dài (cm)
	 */
	length: {
		type: Number,
		default: 0
    },
	/**
	 * Độ rộng (cm)
	 */
	width: {
		type: Number,
		default: 0
    },
	/**
	 * Chiều cao (cm)
	 */
	height: {
		type: Number,
		default: 0
    },

});
