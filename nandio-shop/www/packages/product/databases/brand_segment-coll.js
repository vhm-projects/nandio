"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../database/intalize/base-coll');

/**
 * COLLECTION brand_segment CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('brand_segment', {
	/**
	 * Sản phẩm
	 */
	 product: {
		type: Schema.Types.ObjectId,
		ref: 'product',
	},
	/**
	 * Thương hiệu
	 */
	brand: {
		type: Schema.Types.ObjectId,
		ref: 'brand',
	},
    /**
	 * Segment
	 */
	segment: {
		type: Schema.Types.ObjectId,
		ref: 'segment',
	},
	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},
});
