"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../database/intalize/base-coll');

/**
 * COLLECTION category_brand CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('category_brand', {
	
	/**
	 * Sản phẩm
	 */
	 product: {
		type: Schema.Types.ObjectId,
		ref: 'product',
	},
	/**
	 * Danh mục sản phẩm
	 */
	category: {
		type: Schema.Types.ObjectId,
		ref: 'category',
	},
	/**
	 * Thương hiệu
	 */
	brand: {
		type: Schema.Types.ObjectId,
		ref: 'brand',
	},
	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},
});
