"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../database/intalize/base-coll');

/**
 * COLLECTION category_segment CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('category_segment', {
	/**
	 * Sản phẩm
	 */
	 product: {
		type: Schema.Types.ObjectId,
		ref: 'product',
	},
	/**
	 * Danh mục sản phẩm
	 */
    category: {
		type: Schema.Types.ObjectId,
		ref: 'category',
	},
    /**
	 * Segment
	 */
	segment: {
		type: Schema.Types.ObjectId,
		ref: 'segment',
	},
	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},
});
