"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGES
 */
const cfJWS                         = require('../../../config/cf_jws');

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const {checkObjectIDs} 				= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const BRAND_SEGMENT_COLL  			= require('../databases/brand_segment-coll');


class Model extends BaseModel {
    constructor() {
        super(BRAND_SEGMENT_COLL);
    }

    /**
     * FUNCTION: INSERT BRAND SEGMENT
     * AUTHOR: SONLP
     * DATE: 21/6/2021
     */
	insert({  brand, segment, product }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                if(!checkObjectIDs(brand) || !checkObjectIDs(segment[0]) || !checkObjectIDs(product)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }

                let checkExisted = await BRAND_SEGMENT_COLL.find({ brand, product });
                if (checkExisted.length) {
                    let arrBrandSegmentID = checkExisted.map( segment => segment.segment.toString() );
                    segment = segment.filter( segmentChild => !arrBrandSegmentID.includes(segmentChild.toString()) );
                }

                if (!segment.length) {
                    return resolve({ error: true, message: 'brand_segment_exist' });
                }

                let listAfterInsert = segment.map( segment => {
                    return this.insertData({ segment, brand, product });
                });

                let resultOfList    = await Promise.all(listAfterInsert);
                
                if(!resultOfList)
                    return resolve({ error: true, message: 'create_brand_segment_failed' });
                return resolve({ error: false, data: resultOfList });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListSegmentOfBrand({ brandID }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                const LIMIT_2_SEGMENT = 2;
                let condition = { };
                if(brandID && !checkObjectIDs(brandID))
                    return resolve({ error: true, message: 'id_invalid' });
                if (brandID) {
                    condition.brand = brandID
                } 
               
                let listSegmentOfCategory = await BRAND_SEGMENT_COLL.find({ ...condition },{
                    segment: 1, brand: 1
                })
                    .sort({ createAt: -1 })
                    // .limit(LIMIT_2_SEGMENT)
                    .populate({
                        path: 'segment',
                        match: { status: 1 },
                        select: '_id name'
                    })
                    .populate({
                        path: 'brand',
                        match: { status: 1 },
                        select: '_id name'
                    });
                
                if (!listSegmentOfCategory) {
                    return resolve({ error: true, message: 'brand_segment_not_exist' });
                }
                
                return resolve({ error: false, data: listSegmentOfCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({  brand, segment, product }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                if(!checkObjectIDs(brand) || !checkObjectIDs(segment[0]) || !checkObjectIDs(product)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }

                let listAfterInsert = await BRAND_SEGMENT_COLL.deleteMany({ 
                    brand, 
                    product, 
                    segment: {
                        $in: segment
                    }
                })
                if(!listAfterInsert)
                    return resolve({ error: true, message: 'delete_brand_segment_failed' });
                return resolve({ error: false, data: listAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
