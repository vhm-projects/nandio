"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGES
 */
const cfJWS                         = require('../../../config/cf_jws');

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const {checkObjectIDs} 				= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const USER_SEGMENT_COLL  			= require('../databases/user_segment-coll');


class Model extends BaseModel {
    constructor() {
        super(USER_SEGMENT_COLL);
    }

    /**
     * FUNCTION: INSERT SEGMENT USER
     * AUTHOR: SONLP
     * DATE: 21/6/2021
     */
	insert({ segment, user, type = 0 }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                if(!segment.length)
                    return resolve({ error: true, message: 'segment_invalid' });
                if(!checkObjectIDs(user) || !checkObjectIDs(segment[0]))
                    return resolve({ error: true, message: 'id_invalid' });
                let checkExisted = await USER_SEGMENT_COLL.find({ user });
                if (checkExisted.length) {
                    let arrUserSegmentID = checkExisted.map( segment => segment.segment.toString() );
                    segment = segment.filter( segmentChild => !arrUserSegmentID.includes(segmentChild.toString()) );
                }
 
                if (!segment.length) {
                    return resolve({ error: true, message: 'user_segment_exist' });
                }

                let listAfterInsert = segment.map( segment => {
                    return this.insertData({ segment, user, type });
                });

                let resultOfList    = await Promise.all(listAfterInsert);
                if(!resultOfList)
                    return resolve({ error: true, message: 'create_user_segment_failed' });
                return resolve({ error: false, data: resultOfList });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: LẤY DANH SÁCH SEGMENT CỦA USER
     * AUTHOR: SONLP
     */
     getListSegmentOfUser({ userID }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                if(!checkObjectIDs(userID))
                    return resolve({ error: true, message: 'id_invalid' });
                let listSegmentOfUser = await USER_SEGMENT_COLL.find({ user: userID }).populate({
                    path: 'segment',
                    match: { status: 1 },
                    // options: { sort: { order: 1 } },
                    select: 'name'
                });
               
                if (!listSegmentOfUser) {
                    return resolve({ error: true, message: 'user_segment_not_exist' });
                }
                
                return resolve({ error: false, data: listSegmentOfUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    deleteSegmentOfUser({ userID, segment, type }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
               
                if(!checkObjectIDs(userID) || !checkObjectIDs(segment))
                    return resolve({ error: true, message: 'id_invalid' });
                
                let infoDelete = await USER_SEGMENT_COLL.deleteMany({ user: userID, segment: { $in: segment }, type });
                
                if (!infoDelete) {
                    return resolve({ error: true, message: 'cannot_delete' });
                }
                
                return resolve({ error: false, data: infoDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
