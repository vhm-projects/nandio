"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGES
 */
const cfJWS                         = require('../../../config/cf_jws');

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const {checkObjectIDs} 				= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const USER_LIKE_PRODUCT_COLL  			= require('../databases/user_like_product-coll');


class Model extends BaseModel {
    constructor() {
        super(USER_LIKE_PRODUCT_COLL);
    }

    /**
     * FUNCTION: INSERT PRODUCT USERs
     * AUTHOR: SONLP
     * DATE: 21/6/2021
     */
	insert({ product, user }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                if(!checkObjectIDs(product) || !checkObjectIDs(user))
                    return resolve({ error: true, message: 'id_invalid' });

                let checkExisted = await USER_LIKE_PRODUCT_COLL.find({ product, user });

                if (checkExisted.length) {
                    let infoUpdate = await USER_LIKE_PRODUCT_COLL.findOneAndDelete({ product, user });

                    return resolve({ error: false, data: infoUpdate, message: 'update' });
                    // return resolve({ error: false, message: 'user_liked_product' }); 
                }

                let infoAfterInsert = await this.insertData({ product, user });
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'create_user_liked_product_failed' });
                return resolve({ error: false, data: infoAfterInsert, message: 'insert' });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductOfUser({ userID }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                if(!checkObjectIDs(userID))
                    return resolve({ error: true, message: 'id_invalid' });

                let listProduct = await USER_LIKE_PRODUCT_COLL.find({ user: userID });

                if (!listProduct) {
                    return resolve({ error: false, message: 'user_like_product_not_exist' }); 
                }

                return resolve({ error: false, data: listProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoLikeByProductID({ product, user }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                if(!checkObjectIDs(product))
                    return resolve({ error: true, message: 'id_invalid' });

                let infoProduct = await USER_LIKE_PRODUCT_COLL.findOne({ product, user });

                if (!infoProduct) {
                    return resolve({ error: false, data: 'not_like' }); 
                }

                return resolve({ error: false, data: 'like' });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
