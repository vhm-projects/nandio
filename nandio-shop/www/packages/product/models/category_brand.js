"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGES
 */
const cfJWS                         = require('../../../config/cf_jws');

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const {checkObjectIDs} 				= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const CATEGORY_BRAND_COLL  			= require('../databases/category_brand');


class Model extends BaseModel {
    constructor() {
        super(CATEGORY_BRAND_COLL);
    }

    /**
     * FUNCTION: INSERT CATEGORY BRAND
     * AUTHOR: SONLP
     * DATE: 21/6/2021
     */
	insert({ category, brand, product }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                if(!checkObjectIDs(category) || !checkObjectIDs(brand) || !checkObjectIDs(product))
                    return resolve({ error: true, message: 'id_invalid' });

                let checkExisted = await CATEGORY_BRAND_COLL.find({ brand, product });
                if (checkExisted.length) {
                    for (let brand of checkExisted) {
                        if (category.toString() == brand.category.toString()) {
                            return resolve({ error: true, message: 'category_brand_existed' }); 
                        }
                    }
                }

                let infoAfterInsert = await this.insertData({ category, brand, product });
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'create_category_brand_failed' });
                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListCategory({ brandID }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                let condition = {};
                if(brandID && !checkObjectIDs(brandID))
                    return resolve({ error: true, message: 'id_invalid' });
                brandID && (condition.brand = brandID) 
                
                let listCategoryOfBrand = await CATEGORY_BRAND_COLL.find({ ...condition },{
                    category: 1, brand: 1
                });

                if (!listCategoryOfBrand) {
                    return resolve({ error: true, message: 'category_brand_not_exist' });
                }
                
                return resolve({ error: false, data: listCategoryOfBrand });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ category, brand, product }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                if(!checkObjectIDs(category) || !checkObjectIDs(brand) || !checkObjectIDs(product))
                    return resolve({ error: true, message: 'id_invalid' });

                let infoAfterInsert = await CATEGORY_BRAND_COLL.deleteMany({ category, brand, product });
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'delete_category_brand_failed' });
                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
