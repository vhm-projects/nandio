"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGES
 */
const cfJWS                         = require('../../../config/cf_jws');

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const {checkObjectIDs} 				= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const CATEGORY_SEGMENT_COLL  			= require('../databases/category_segment-coll');


class Model extends BaseModel {
    constructor() {
        super(CATEGORY_SEGMENT_COLL);
    }

    /**
     * FUNCTION: INSERT CATEGORY SEGMENT
     * AUTHOR: SONLP
     * DATE: 21/6/2021
     */
	insert({ category, segment, product }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                if(!checkObjectIDs(category) || !checkObjectIDs(segment[0]) || !checkObjectIDs(product))
                    return resolve({ error: true, message: 'id_invalid' });

                let checkExisted = await CATEGORY_SEGMENT_COLL.find({ category, product });
                if (checkExisted.length) {
                    let arrCategorySegmentID = checkExisted.map( segment => segment.segment.toString() );
                    segment = segment.filter( segmentChild => !arrCategorySegmentID.includes(segmentChild.toString()) );
                }
 
                if (!segment.length) {
                    return resolve({ error: true, message: 'category_segment_exist' });
                }

                let listAfterInsert = segment.map( segment => {
                    return this.insertData({ segment, category, product });
                });

                let resultOfList    = await Promise.all(listAfterInsert);
                
                if(!resultOfList)
                    return resolve({ error: true, message: 'create_category_segment_failed' });
                return resolve({ error: false, data: resultOfList });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: LẤY DANH SÁCH SEGMENT
     * AUTHOR: SONLP
     */
     getListSegmentOfCategory({ categoryID, segmentID }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                let condition = {};
                if(categoryID && !checkObjectIDs(categoryID))
                    return resolve({ error: true, message: 'id_invalid' });

                categoryID && (condition.category = categoryID) 

                let listSegmentOfCategory = await CATEGORY_SEGMENT_COLL.find({ ...condition },{
                    segment: 1, category: 1
                })
                    .populate({
                        path: 'segment',
                        match: { status: 1 },
                        select: '_id name'
                    })
                    .populate({
                        path: 'category',
                        match: { status: 1 },
                        select: '_id title'
                    })
                    .sort({ createAt: -1 });
                
                if (!listSegmentOfCategory) {
                    return resolve({ error: true, message: 'category_segment_not_exist' });
                }
                
                return resolve({ error: false, data: listSegmentOfCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: LẤY DANH SÁCH SEGMENT CỦA USER
     * AUTHOR: SONLP
    */
    getListSegmentOfCategorySort2SegmentScreenS08({ categoryID }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                const LIMIT_2_SEGMENT = 2;
                let condition = {};
                if(categoryID && !checkObjectIDs(categoryID))
                    return resolve({ error: true, message: 'id_invalid' });
                categoryID && (condition.category = categoryID) 
               
                let listSegmentOfCategory = await CATEGORY_SEGMENT_COLL.find({ ...condition },{
                    segment: 1, category: 1
                })
                    .sort({ createAt: -1 })
                    // .limit(LIMIT_2_SEGMENT)
                    .populate({
                        path: 'segment',
                        match: { status: 1 },
                        select: '_id name'
                    })
                    .populate({
                        path: 'category',
                        match: { status: 1 },
                        select: '_id title'
                    });
                
                if (!listSegmentOfCategory) {
                    return resolve({ error: true, message: 'category_segment_not_exist' });
                }
                
                return resolve({ error: false, data: listSegmentOfCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ category, segment, product }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                if(!checkObjectIDs(category) || !checkObjectIDs(segment[0]) || !checkObjectIDs(product))
                    return resolve({ error: true, message: 'id_invalid' });

                let listAfterInsert = await CATEGORY_SEGMENT_COLL.deleteMany({ 
                    category, 
                    segment: {
                        $in: segment
                    }, 
                    product 
                }) 
                
                if(!listAfterInsert)
                    return resolve({ error: true, message: 'delete_category_segment_failed' });
                return resolve({ error: false, data: listAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
}

exports.MODEL = new Model;
