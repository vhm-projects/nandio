"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      		= require('mongoose').Types.ObjectId;
const formatCurrency 						= require('number-format.js');

/**
 * INTERNAL PACKAGES
 */
const cfJWS                         		= require('../../../config/cf_jws');


/**
 * BASES
 */
const BaseModel 							= require('../../../models/intalize/base_model');
const { checkObjectIDs, loadPathImage } 	= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const PRODUCT_COLL  				= require('../databases/product-coll');
const CATEGORY_BRAND_MODEL  		= require('./category_brand').MODEL;
const BRAND_SEGMENT_MODEL  		    = require('./brand_segment').MODEL;
const CATEGORY_SEGMENT_MODEL  		= require('./category_segment').MODEL;
const USER_LIKE_PRODUCT_MODEL  		= require('./user_like_product').MODEL;
const USER_SEGMENT_MODEL  		    = require('./user_segment').MODEL;
const { QRCODE_MODEL }              = require('../../qrcode');
const { IMAGE_MODEL }               = require('../../image');
const { CATEGORY_MODEL }            = require('../../category');
const { BRAND_MODEL }               = require('../../brand');
const BIG_SALE_MODEL                = require('../../promotion/models/big_sale').MODEL;
const FLASH_SALE_MODEL              = require('../../promotion/models/flash_sale').MODEL;
const SEGMENT_MODEL  		        = require('../../segment/models/segment').MODEL;


class Model extends BaseModel {
    constructor() {
        super(PRODUCT_COLL);
    }

    /**
     * FUNCTION: INSERT PRODUCT
     * AUTHOR: SONLP
     * DATE: 21/6/2021
     */
	insert({ name, description, content, price, category, brand, SKU, avatar, images, userID, segments, sourceMaterial,
        importedBy,
        madeBy,
        distributor,
        certifiedImages
     }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
              
                if(!checkObjectIDs(category) || !checkObjectIDs(brand) || !checkObjectIDs(...segments))
                    return resolve({ error: true, message: 'id_invalid' });
                /**
                 * CHECK TÊN CÓ TỒN TẠI
                 */
                if(!name) {
                    return resolve({ error: true, message: 'name_invalid' });
                }
                /**
                 * CHECK HÌNH ẢNH AVATAR
                 */
                 if(!avatar) {
                    return resolve({ error: true, message: 'avatar_invalid' });
                }
                /**     
                 * CHECK GIÁ CÓ TỒN TẠI
                 */
                if(!price || !Number(price)) {
                    return resolve({ error: true, message: 'price_invalid' });
                }

                let dataInsert = {
                    name, price, category, brand, segments, content
                }
                
                if(content) {
                    dataInsert.content = content;
                }

                if(description) {
                    dataInsert.description = description;
                }

                if(sourceMaterial){
                    dataInsert.sourceMaterial = sourceMaterial;
                }

                if(importedBy){
                    dataInsert.importedBy = importedBy;
                }

                if(madeBy){
                    dataInsert.madeBy = madeBy;
                }

                if(distributor){
                    dataInsert.distributor = distributor;
                }

                if(SKU) {
                    let checkSKU = await PRODUCT_COLL.find({ SKU: SKU });
                    if (checkSKU && checkSKU.length) {
                        return resolve({ error: true, message: 'SKU_existed' });
                    }
                    dataInsert.SKU = SKU;
                }
                 /**
                 * Insert Hình Ảnh
                 */
                let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                    name: avatar.name, 
                    path: avatar.pathAvatar,
                    size: avatar.size,
                    userCreate: userID 
                });
                
                if (images && images.length) {
                    let listGallery          = images.map( image => {
                        return IMAGE_MODEL.insert({ 
                            name: image.name,
                            path: image.pathAvatar,
                            size: image.size, 
                            userCreate: userID 
                        });
                    });
                    let resultPromiseAll = await Promise.all(listGallery);
                    let listGalleryID    = resultPromiseAll.map( gallery => gallery.data._id );

                    dataInsert.images = listGalleryID;
                }

                if (certifiedImages && certifiedImages.length) {
                    let listCertifiedImages          = certifiedImages.map( image => {
                        return IMAGE_MODEL.insert({ 
                            name: image.name,
                            path: image.pathAvatar,
                            size: image.size, 
                            userCreate: userID 
                        });
                    });
                    let resultPromiseAll = await Promise.all(listCertifiedImages);
                    let listCertifiedImagesID    = resultPromiseAll.map( certifiedImages => certifiedImages.data._id );

                    dataInsert.certifiedImages = listCertifiedImagesID;
                }
                
                dataInsert = {
                    ...dataInsert,
                    avatar: infoImageAfterInsert.data._id,
                }
               
                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'create_product_failed' });

                /**
                 * UPDATE QR_CODE SẢN PHẨM
                 */
                let nameFile          = `` + infoAfterInsert._id +".jpg";
                let infoQRAfterInsert = await QRCODE_MODEL.insert({ id: infoAfterInsert._id, key: nameFile, userID });

                let infoAfterUpdate   = await PRODUCT_COLL.findByIdAndUpdate(infoAfterInsert._id, {
                    qr_code: infoQRAfterInsert.data._id
                });
                /** 
                 * 1//  INSERT BRAND_CATEGORY  
                 */
                let infoCategoryBrandAfterInsert = await CATEGORY_BRAND_MODEL.insert({ category, brand, product: infoAfterInsert._id });
                /**
                 * 2// INSERT BRAND_SEGMENT
                 */
                let infoBrandSegmentAfterInsert = await BRAND_SEGMENT_MODEL.insert({ brand, segment: segments, product: infoAfterInsert._id });
                /**
                 * 3// INSERT CATEGORY_SEGMENT
                 */
                let infoCategorySegmentAfterInsert = await CATEGORY_SEGMENT_MODEL.insert({ category, segment: segments, product: infoAfterInsert._id });
                
                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ 
        productID, name, description, content, status, price, category, 
        brand, SKU, SKUV2, avatar, images, userID, segments, imageDelete, 
        avatarDelete, sourceMaterial,
        madeBy,
        importedBy,
        distributor,
        listCertifiedImagesNew,
        listCertifiedImagesDelete,
        weight,
        length,
        width,
        height,
     }) {
        return new Promise(async resolve => {
            try {
                
                let dataUpdate = { modifyAt: new Date() }
                if(!checkObjectIDs(productID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }

                let checkImageExit = await PRODUCT_COLL.findById(productID);

                if (!avatar && (!checkImageExit.avatar || !checkObjectIDs(checkImageExit.avatar))) {
                    return resolve({ error: true, message: 'Mời bạn chọn hình ảnh đại diện' });
                }

                name            && (dataUpdate.name        = name);
                price           && (dataUpdate.price       = price);
                SKU             && (dataUpdate.SKU         = SKU);
                SKUV2           && (dataUpdate.sku_2       = SKUV2);
                if (status || status == 0) {
                    dataUpdate.status      = status
                }
                if (brand) {
                    /**
                     * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                     */
                    if(!checkObjectIDs(brand)) {
                        return resolve({ error: true, message: 'id_invalid' });
                    } else {
                        dataUpdate.brand       = brand
                    }
                }

                if (category) {
                    /**
                     * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                     */
                    if(!checkObjectIDs(category)) {
                        return resolve({ error: true, message: 'id_invalid' });
                    } else {
                        dataUpdate.category       = category
                    }
                }

                if (segments && segments.length) {
                    /**
                     * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                     */
                    if(!checkObjectIDs(segments[0])) {
                        return resolve({ error: true, message: 'id_invalid' });
                    } else {
                        dataUpdate.segments       = segments
                    }
                }
                
                if (description) {
                    dataUpdate.description = description;
                } else {
                    dataUpdate.description = "";
                }

                if (content) {
                    dataUpdate.content = content;
                } else {
                    dataUpdate.content = "";
                }

                if (sourceMaterial) {
                    dataUpdate.sourceMaterial = sourceMaterial;
                } else {
                    dataUpdate.sourceMaterial = "";
                }

                if (importedBy) {
                    dataUpdate.importedBy = importedBy;
                } else {
                    dataUpdate.importedBy = "";
                }
                if (madeBy) {
                    dataUpdate.madeBy = madeBy;
                } else {
                    dataUpdate.madeBy = "";
                }

                if (distributor) {
                    dataUpdate.distributor = distributor;
                } else {
                    dataUpdate.distributor = "";
                }

                if(avatar) {
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                        name: avatar.name, 
                        path: avatar.pathAvatar,
                        size: avatar.size,
                        userCreate: userID 
                    });
                    dataUpdate.avatar = infoImageAfterInsert.data._id;
                }
            
                if (images && images.length) {
                    let listGallery          = images.map( image => {
                        return IMAGE_MODEL.insert({ 
                            name: image.name,
                            path: image.pathAvatar,
                            size: image.size, 
                            userCreate: userID 
                        });
                    });
                    let resultPromiseAll = await Promise.all(listGallery);
                    let listGalleryID    = resultPromiseAll.map( gallery => gallery.data._id );

                    dataUpdate.$addToSet = {
                        images: listGalleryID
                    };
                }

                if (listCertifiedImagesNew && listCertifiedImagesNew.length) {
                    let listCertifiedImages = listCertifiedImagesNew.map( image => {
                        return IMAGE_MODEL.insert({ 
                            name: image.name,
                            path: image.pathAvatar,
                            size: image.size, 
                            userCreate: userID 
                        });
                    });
                    let resultPromiseAll = await Promise.all(listCertifiedImages);
                    let listCertifiedImagesID    = resultPromiseAll.map( certifiedImages => certifiedImages.data._id );

                    dataUpdate.$addToSet = {
                        certifiedImages: listCertifiedImagesID
                    };
                }

                if (imageDelete && imageDelete.length) {
                    let infoProductAfterDeleteImage = await PRODUCT_COLL.findByIdAndUpdate(productID, {
                        $pullAll: {
                            images: [...imageDelete.map(image => ObjectID(image))]
                        }
                    });
                }
               
                if (listCertifiedImagesDelete && listCertifiedImagesDelete.length) {
                    let infoProductAfterDeleteCertifiedImages = await PRODUCT_COLL.findByIdAndUpdate(productID, {
                        $pullAll: {
                            certifiedImages: [...listCertifiedImagesDelete.map(image => ObjectID(image))] 
                        }
                    }, {
                        new: true
                    });
                }

                if (weight) {
                    if (Number.isNaN(Number(weight)) || Number(weight) < 0) {
                        return resolve({ error: true, message: 'Trọng lượng không hợp lệ' });
                    }
                    dataUpdate.weight = weight;
                }

                if (length) {
                    if (Number.isNaN(Number(length)) || Number(length) < 0) {
                        return resolve({ error: true, message: 'Trọng lượng không hợp lệ' });
                    }
                    dataUpdate.length = length;
                }

                if (width) {
                    if (Number.isNaN(Number(width)) || Number(width) < 0) {
                        return resolve({ error: true, message: 'Trọng lượng không hợp lệ' });
                    }
                    dataUpdate.width = width;
                }

                if (height) {
                    if (Number.isNaN(Number(height)) || Number(height) < 0) {
                        return resolve({ error: true, message: 'Trọng lượng không hợp lệ' });
                    }
                    dataUpdate.height = height;
                }
                console.log({
                    dataUpdate
                });
                let infoProductAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(productID, {
                    ...dataUpdate
                }, {
                    new: true
                });
                
                if (!infoProductAfterUpdate) {
                    return resolve({ error: true, message: "cannot_update_product" });
                }
                /**
                 * XÓA HÌNH ẢNH
                 */
                if (avatarDelete) {
                    let infoAvatarDelete = await IMAGE_MODEL.delete({ imageID: avatarDelete });
                }

                if (imageDelete && imageDelete.length) {
                    let listGalleryDelete = imageDelete.map( imageDelete => {
                        return IMAGE_MODEL.delete({ imageID: imageDelete });
                    });
                    let resultOfDelete    = await Promise.all(listGalleryDelete);
                }

                /** 
                 * 1//  DELETE BRAND_CATEGORY  
                 */
                let infoCategoryBrandAfterDelete = CATEGORY_BRAND_MODEL.delete({ category: checkImageExit.category, brand: checkImageExit.brand, product: productID });
                /**
                  * 2// DELETE BRAND_SEGMENT
                  */
                let infoBrandSegmentAfterDelete = BRAND_SEGMENT_MODEL.delete({ brand: checkImageExit.brand, segment: checkImageExit.segments, product: productID });
                 /**
                  * 3// DELETE CATEGORY_SEGMENT
                  */
                let infoCategorySegmentAfterDelete = CATEGORY_SEGMENT_MODEL.delete({ category: checkImageExit.category, segment: checkImageExit.segments, product: productID });
                let result1 = await Promise.all([ infoCategoryBrandAfterDelete, infoBrandSegmentAfterDelete, infoCategorySegmentAfterDelete ])
                /** 
                 * 1//  INSERT BRAND_CATEGORY  
                 */
                 let infoCategoryBrandAfterInsert =  CATEGORY_BRAND_MODEL.insert({ category, brand, product: productID });
                 /**
                  * 2// INSERT BRAND_SEGMENT
                  */
                 let infoBrandSegmentAfterInsert =  BRAND_SEGMENT_MODEL.insert({ brand, segment: segments, product: productID });
                 /**
                  * 3// INSERT CATEGORY_SEGMENT
                  */
                 let infoCategorySegmentAfterInsert =  CATEGORY_SEGMENT_MODEL.insert({ category, segment: segments, product: productID });
                let result2 = await Promise.all([ infoCategoryBrandAfterInsert, infoBrandSegmentAfterInsert, infoCategorySegmentAfterInsert])

                return resolve({ error: false, data: infoProductAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    updateIsTrialProgram({ productID, isTrialProgram }) {
        return new Promise(async resolve => {
            try {
                
                let dataUpdate = { modifyAt: new Date() }
                if(!checkObjectIDs(productID))
                    return resolve({ error: true, message: 'id_invalid' });
                
                dataUpdate.isTrialProgram = isTrialProgram;

                let infoProductAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(productID, {
                    ...dataUpdate
                }, { new: true });

                if (!infoProductAfterUpdate)
                    return resolve({ error: true, message: "cannot_update_product" });
                
                return resolve({ error: false, data: infoProductAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateIsGift({ productID, isGift }) {
        return new Promise(async resolve => {
            try {
                
                let dataUpdate = { modifyAt: new Date() }
                if(!checkObjectIDs(productID))
                    return resolve({ error: true, message: 'id_invalid' });
                
                dataUpdate.isGift = isGift;

                let infoProductAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(productID, {
                    ...dataUpdate
                }, { new: true });

                if (!infoProductAfterUpdate)
                    return resolve({ error: true, message: "cannot_update_product" });
                
                return resolve({ error: false, data: infoProductAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateStatus({ productID, status }) {
        return new Promise(async resolve => {
            try {
                
                let dataUpdate = { modifyAt: new Date() }
                if(!checkObjectIDs(productID))
                    return resolve({ error: true, message: 'id_invalid' });
                
                dataUpdate.status = status;

                let infoProductAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(productID, {
                    ...dataUpdate
                }, { new: true });

                if (!infoProductAfterUpdate)
                    return resolve({ error: true, message: "cannot_update_product" });
                
                return resolve({ error: false, data: infoProductAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateShowQR({ productID, show_qr, SKUV2 }) {
        return new Promise(async resolve => {
            try {
                
                let dataUpdate = { modifyAt: new Date() }
                if(!checkObjectIDs(productID))
                    return resolve({ error: true, message: 'id_invalid' });
                if (!show_qr) {
                    return resolve({ error: true, message: 'Trạng thái quét QR không hợp lệ' });
                }
                dataUpdate.show_qr = show_qr;

                let infoProductAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(productID, {
                    ...dataUpdate
                }, { new: true });

                if (!infoProductAfterUpdate)
                    return resolve({ error: true, message: "cannot_update_product" });
                
                const QR_SHOW = 1;
                if (SKUV2 && show_qr == QR_SHOW) {
                    let listProduct = await PRODUCT_COLL.find({ sku_2: SKUV2, _id: {
                        $nin: [productID]
                    }});
                   
                    if (listProduct && listProduct.length) {
                        let listProductIDUnshowQr = listProduct.map(item => item._id);
                        let dataUpdateMany = await PRODUCT_COLL.updateMany({
                            _id: listProductIDUnshowQr
                        },{
                            show_qr: 2
                        });
                    }
                }

                return resolve({ error: false, data: infoProductAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoProductOfSku2({ sku_2 }) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(sku_2))
                    return resolve({ error: true, message: 'id_invalid' });
                
                const SHOW_QR = 1;
                let infoProduct = await PRODUCT_COLL.findOne({
                    sku_2,
                    show_qr: SHOW_QR
                })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    .populate({
                        path: "images",
                        select: "_id path"
                    })
                    .populate({
                        path: "certifiedImages",
                        select: "_id path"
                    })
                    .populate({
                        path: 'brand',
                        select: 'name linkWebsite image',
                        populate: {
                            path: 'image',
                            select: 'path'
                        }
                    })
                    .populate({
                        path: 'category',
                        select: 'title'
                    });

                if (!infoProduct)
                    return resolve({ error: true, message: "cannot_get_info" });

                return resolve({ error: false, data: infoProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoProductOfSku1({ sku_1 }) {
        return new Promise(async resolve => {
            try {
                if(!sku_1)
                    return resolve({ error: true, message: 'Mã SKU không tồn tại' });
                
                let infoProduct = await PRODUCT_COLL.findOne({
                    SKU: sku_1,
                })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    .populate({
                        path: "images",
                        select: "_id path"
                    })
                    .populate({
                        path: "certifiedImages",
                        select: "_id path"
                    })
                    .populate({
                        path: 'brand',
                        select: 'name linkWebsite image',
                        populate: {
                            path: 'image',
                            select: 'path'
                        }
                    })
                    .populate({
                        path: 'category',
                        select: 'title'
                    });

                if (!infoProduct)
                    return resolve({ error: true, message: "cannot_get_info" });

                return resolve({ error: false, data: infoProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateSTT({ productID, stt }) {
        return new Promise(async resolve => {
            try {
                
                let dataUpdate = { modifyAt: new Date() }
                if(!checkObjectIDs(productID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }

                if (stt || stt != 0) {
                    dataUpdate.stt      = stt;
                }
                
                let infoProductAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(productID, {
                    ...dataUpdate
                }, {
                    new: true
                });
                if (!infoProductAfterUpdate) {
                    return resolve({ error: true, message: "cannot_update_product" });
                }

                return resolve({ error: false, data: infoProductAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updatePoint({ idSQL, point }) {
        return new Promise(async resolve => {
            try {
                if(!idSQL || isNaN(+point))
                    return resolve({ error: true, message: "params_invalid" });
                    
                let infoProductAfterUpdate = await PRODUCT_COLL.findOneAndUpdate({ idSQL }, { point }, { new: true });
                if(!infoProductAfterUpdate)
                    return resolve({ error: true, message: "cannot_update" });
                // Tiến hành cập nhật lịch sử sửa điểm cho sản phẩm history_point_product
                return resolve({ error: false, data: "update_point_product_success" });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getList({ status }){
        return new Promise(async resolve => {
            try {
                let listProduct = await PRODUCT_COLL.find({ status })
                    .sort({ stt: 1, modifyAt: -1 })
                    .lean();
                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });

                return resolve({ error: false, data: listProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    filter({ status, category, brand, price, fromDay, toDay, limit = 20, page = 1, keyword }){
        return new Promise(async resolve => {
            try {
                page = page ? page : 1;

                let dataFind = {};
                if (category) {
                    if(!checkObjectIDs(category))
                        return resolve({ error: true, message: 'id_invalid' });
                    dataFind.category = category;
                }

                if (brand) {
                    if(!checkObjectIDs(brand))
                        return resolve({ error: true, message: 'id_invalid' });
                    dataFind.brand = brand;
                }

                if (price) {
                    if(!Number(price))
                        return resolve({ error: true, message: 'price_invalid' });
                    dataFind.price = price;
                }

                if (fromDay) {
                    dataFind.createAt = {
                        $gte: fromDay
                    }
                }

                if (toDay) {
                    dataFind.createAt = {
                        ...dataFind.createAt,
                        $lte: toDay
                    }
                }

                let listProduct = await PRODUCT_COLL.find({ ...dataFind })
                    .limit(200)
                    .skip((page - 1) * limit)
                    .populate({
                        path: "category",
                        select: "_id title"
                    })
                    .populate({
                        path: "brand",
                        select: "_id name"
                    })
                    .populate({
                        path: "segments",
                        select: "_id name"
                    })
                    .populate({
                        path: "avatar",
                        select: "_id name path"
                    })
                    .sort({ stt: 1, modifyAt: -1 })
                    .lean();
                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });

                return resolve({ error: false, data: listProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    filterV2({ status, category, brand, price, fromDay, toDay, limit = 20, page = 1, keyword }){
        return new Promise(async resolve => {
            try {
                page = page ? page : 1;
                let dataFind = {};

                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    dataFind.$or = [
                        { name: new RegExp(key, 'i') },
                    ]
                }

                if (category) {
                    if(!checkObjectIDs(category))
                        return resolve({ error: true, message: 'id_invalid' });
                    dataFind.category = category;
                }

                if (brand) {
                    if(!checkObjectIDs(brand))
                        return resolve({ error: true, message: 'id_invalid' });
                    dataFind.brand = brand;
                }

                if (price) {
                    if(!Number(price))
                        return resolve({ error: true, message: 'price_invalid' });
                    dataFind.price = price;
                }

                if (fromDay) {
                    dataFind.createAt = {
                        $gte: fromDay
                    }
                }

                if (toDay) {
                    dataFind.createAt = {
                        ...dataFind.createAt,
                        $lte: toDay
                    }
                }

                if (status || status == 0) {
                    dataFind = {
                        ...dataFind,
                        status: status
                    }
                }
                
                let skip = (page - 1) * limit;
                let listProduct = await PRODUCT_COLL.find({ ...dataFind }, { 
                    name: 1, price: 1, description: 1, SKU: 1, SKUV2: 1, show_qr: 1, 
                    category: 1, brand: 1, segments: 1, avatar: 1, status: 1, stt: 1,
                    isTrialProgram: 1, isGift: 1
                })
                    .limit(limit * 1)
                    .skip(skip)
                    .populate({
                        path: "category",
                        select: "_id title"
                    })
                    .populate({
                        path: "avatar",
                        select: "path"
                    })
                    .populate({
                        path: "brand",
                        select: "_id name"
                    })
                    .populate({
                        path: "segments",
                        select: "_id name"
                    })
                    .populate({
                        path: "sku_2",
                        select: "_id name"
                    })
                    .sort({ stt: 1, modifyAt: -1 })
                    .lean();
                
                let count = await PRODUCT_COLL.count({ ...dataFind });
                let arrProduct = [];
                listProduct && listProduct.length && listProduct.forEach((item, index) => {
                    let indexChange = skip + index + 1;
                    let stringSegment = ``;
                    item.segments && item.segments.length && item.segments.forEach( segment => {
                        stringSegment += `
                            <h5 title="${segment.name}">
                                <span class="badge badge-pill badge-success" style="padding: 10px; font-size: 10px;">
                                    ${segment && segment.name && segment.name.length > 15 ? segment.name.substr(0, 15).concat('...') : segment.name}
                                </span>
                            </h5>
                        `;
                    });

                    let isTrialProgram = '';
                    if (item.isTrialProgram == 1) { 
                        isTrialProgram = `
                            <div style="float: left;">
                                <input class="isTrialProgram" type="checkbox" id="isTrialProgram${item._id}" _productID="${item._id}" switch="none" checked="checked"> 
                                <label for="isTrialProgram${item._id}" data-on-label="On" data-off-label="Off">
                            </div>
                        `;
                    } else {
                        isTrialProgram = ` 
                            <div style="float: left;">
                                <input type="checkbox"class="isTrialProgram" id="isTrialProgram${item._id}"  _productID="${item._id}" switch="none"> 
                                <label for="isTrialProgram${item._id}" data-on-label="On" data-off-label="Off">
                            </div>
                        `;
                    }

                    let isGift = '';
                    if (item.isGift == 1) { 
                        isGift = `
                            <div style="float: left;">
                                <input class="isGift" type="checkbox" id="isGift${item._id}" _productID="${item._id}" switch="none" checked="checked"> 
                                <label for="isGift${item._id}" data-on-label="On" data-off-label="Off">
                            </div>
                        `;
                    } else {
                        isGift = ` 
                            <div style="float: left;">
                                <input type="checkbox"class="isGift" id="isGift${item._id}"  _productID="${item._id}" switch="none"> 
                                <label for="isGift${item._id}" data-on-label="On" data-off-label="Off">
                            </div>
                        `;
                    }

                    let status = '';
                    if (item.status == 1) { 
                        status = `
                            <div style="float: left;">
                                <input class="status" type="checkbox" id="status${item._id}" _productID="${item._id}"  switch="none" checked="checked"> 
                                <label  for="status${item._id}" data-on-label="On" data-off-label="Off">
                            </div>
                        `;
                    } else {
                        status = ` 
                            <div style="float: left;">
                                <input type="checkbox"class="status" id="status${item._id}" _productID="${item._id}" switch="none"> 
                                <label for="status${item._id}" data-on-label="On" data-off-label="Off">
                            </div>
                        `;
                    }

                    let show_qr = '';
                    let SKU_V2 = item.sku_2 && item.sku_2._id || '';
                    if (item.show_qr && item.show_qr == 1) { 
                        show_qr = `
                            <div style="float: left;">
                                <input class="show-qr show-qr-${SKU_V2}" type="checkbox" _SKUV2= "${SKU_V2}" id="show_qr${item._id}" _productID="${item._id}"  switch="none" checked="checked"> 
                                <label  for="show_qr${item._id}" data-on-label="On" data-off-label="Off">
                            </div>
                        `;
                    } else {
                        show_qr = ` 
                            <div style="float: left;">
                                <input type="checkbox"class="show-qr show-qr-${SKU_V2}" _SKUV2= "${SKU_V2}" id="show_qr${item._id}" _productID="${item._id}" switch="none"> 
                                <label for="show_qr${item._id}" data-on-label="On" data-off-label="Off">
                            </div>
                        `;
                    }

                    let stt = `<input type="number" id="txtSTT" _productID ='${item._id}' class="form-control" value="${ (item && item.stt && item.stt != 5000) ?  item.stt : '' }" required="" placeholder="VD: 1">`
                  
                    let obj = {
                        index:  indexChange,
                        image:  `
                            <img src="${item.avatar ? loadPathImage(item.avatar.path) : '/template/admin/img/50x50.png'}" width="50px" height="50px">
                        `,
                        name:   `
                            <a href="/product/info-product/${item._id}">${item.name}</a>
                        `,
                        stt,
                        price:  ` ${ item.price && formatCurrency('###,###.', item.price, '' ) || 0} đ`,
                        sku:      item.SKU && item.SKU || '',
                        skuv2:    item.sku_2 && item.sku_2.name || '',
                        category:    item.category && item.category.title || '',
                        brand:       item.brand    && item.brand.name     || '',
                        segment:     stringSegment,
                        isTrialProgram,
                        isGift,
                        show_qr,
                        status,
                        action:`
                            <div class="btn-group mb-2">
                                <button type="button" class="btn btn-secondary btn-sm waves-effect waves-light">
                                    Thao Tác
                                </button>
                                <button type="button" class="btn btn-secondary btn-sm waves-effect waves-light dropdown-toggle-split dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">...</span>
                                </button>
                                <div class="dropdown-menu" x-placement="right-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(143px, 0px, 0px);">
                                    <a class="dropdown-item" href="/product/update-product/${item._id}" target="blank">
                                        <i class="fa fa-edit"></i>
                                        Cập nhật Thông Tin
                                    </a>
                                    <a class="dropdown-item btnUpdatePointProduct" href="javascript: void(0)>" product_id="${item.idSQL && item.idSQL}">
                                        <i class="fa fa-edit"></i>
                                        Cật nhật điểm
                                    </a>
                                </div>
                            </div>
                        `,
                    };
                   
                    arrProduct = [
                        ...arrProduct,
                        obj
                    ]
                })
                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });

                return resolve({ error: false, data: arrProduct, recordsTotal: count, recordsFiltered: count });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    filterByKeyword({ keyword, page = 1, limit = 10, categoryID, brandID }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                let dataFind = { status: STATUS_ACTIVE };
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    dataFind.$or = [
                        { name: new RegExp(key, 'i') },
                    ]
                }

                if(categoryID && !checkObjectIDs(categoryID))
                    return resolve({ error: true, message: 'id_invalid' });
                categoryID && (dataFind.category = categoryID)

                if(brandID && !checkObjectIDs(brandID))
                    return resolve({ error: true, message: 'id_invalid' });
                brandID && (dataFind.brand = brandID)

                let listSegmentActive = await SEGMENT_MODEL.getListActiveV2({});
                let listSegmentID = listSegmentActive.data.map( item => item._id );
                dataFind = {
                    ...dataFind,
                    segments: {
                        $in: listSegmentID
                    }
                }

                let listProduct = await PRODUCT_COLL.find({ ...dataFind }, {
                    name: 1, price: 1, avatar: 1, description: 1
                })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ stt: -1, modifyAt: -1 })
                    .populate({
                        path: "avatar",
                        select: "path"
                    })
                    .lean();
            
                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });

                let totalItem = await PRODUCT_COLL.count(dataFind);
                let pages = Math.ceil(totalItem/limit);
                
                // Lấy danh sách bigsale và flashsale
                let listBigsaleAndFlashsale = await this.getListBigSaleAndInfoFlashSale();
                let { listBigSale, infoFlashSale } = listBigsaleAndFlashsale.data;

                let data = {
                    listProduct,
                    listBigSale,
                    infoFlashSale
                }
    
                return resolve({ error: false, data: data, page, pages });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkTypeAPI({ userID, keyword, page = 1, limit = 10, categoryID, brandID, type, segmentID, like }) {
        return new Promise(async resolve => {
            try {
                
                //=============================CHI TIẾT DANH MỤC=============================
                const TYPE_DETAIL_CATEGORY = 1;
                const TYPE_LIST_SEGMENT    = 2;
                let data = {};

                /**
                 * ===========================DANH SÁCH CỦA BRAND================================
                */
                let listBrand = await BRAND_MODEL.getListAll({  });
                
                /**
                 * ===========================DANH SÁCH CỦA CATEGORY================================
                 */
                let listCategory = await CATEGORY_MODEL.getListAll({ brandID });
                if (listCategory.error) {
                    return resolve(listCategory)
                }

                /**
                 * ===========================DANH SÁCH BIGSALE================================
                 */
                let listBigSale = await BIG_SALE_MODEL.getListBigSaleActive({ });
                
                /**
                 * ===========================DANH SÁCH FLASH SALE================================
                 */
                let listFlashSale = await FLASH_SALE_MODEL.getListFlashSaleActive({ });

                /**
                 * =============================LOẠI API=============================
                 * TYPE == TYPE_DETAIL_CATEGORY (CHI TIẾT DANH MỤC)
                 * TYPE == TYPE_LIST_SEGMENT    (CHI TIẾT SEGMENT)
                 */

                /**
                 * =======================================================================================================
                 * ===================================MÀN HÌNH CHI TIẾT DANH MỤC S08 =====================================
                 * =======================================================================================================
                 */
                if (type == TYPE_DETAIL_CATEGORY) {
                    /**
                     * ===========================DANH SÁCH SẢN PHẨM THEO SEGMENT CỦA CATEGORY================================
                     */
                    let listProductCategory  = await this.getListProductSegmentsCategoryScreenS08({ keyword, page, limit, categoryID, brandID });
                    if (listProductCategory.error) {
                        return resolve(listProductCategory)
                    }
                    /**
                     * ===========================DANH SÁCH SẢN PHẨM GỢI Ý CỦA BẠN================================
                     */
                    if (userID) {
                        let listProductRecommend = await this.getListProductSegmentUserCategory({ userID, keyword, categoryID, brandID, page, limit });
                        if (listProductRecommend.error) {
                            return resolve(listProductRecommend)
                        }
                        data = {
                            ...data,
                            listProductRecommend: listProductRecommend.data
                        }
                    } 

                    let listGalleryCategory = [];
                    let listGalleryBrand    = [];

                    if (categoryID) {
                        let listGalleryOfCategory = await CATEGORY_MODEL.getInfo({ categoryID });
                        listGalleryCategory = [
                            ...listGalleryCategory,
                            ...listGalleryOfCategory.data.gallerys.map(element => ({
                                path: element.path,
                                typeCTA: element.typeCTA,
                                valueCTA: element.valueCTA,
                            }))
                        ];

                    } else {
                        /**
                        * ===========================DANH SÁCH HÌNH ẢNH CỦA BRAND================================
                        */
                        let listBrandGallery = await BRAND_MODEL.getListAll({ brandID });
                        listBrandGallery.data.map( item => {
                            listGalleryBrand = listGalleryBrand.concat(item.gallerys.map(element => (
                                { 
                                    path: element.path,
                                    typeCTA: element.typeCTA,
                                    valueCTA: element.valueCTA,
                                }
                            )));
                        });
                       
                        if (!brandID) {
                            /**
                             * ===========================DANH SÁCH HÌNH ẢNH CỦA CATEGORY================================
                             */
                            listCategory.data.map( item => {               
                                listGalleryCategory = listGalleryCategory.concat(item.gallerys.map(element => (
                                    { 
                                        path: element.path,
                                        typeCTA: element.typeCTA,
                                        valueCTA: element.valueCTA,
                                    }
                                )));
                            });
                        } else {
                             /**
                             * ===========================DANH SÁCH HÌNH ẢNH CỦA CATEGORY VỚI BRAND================================
                             */
                            let listGalleryOfCategory = await CATEGORY_MODEL.getListAll({ brandID });
                            listGalleryOfCategory.data.map( item => {               
                                listGalleryCategory = listGalleryCategory.concat(item.gallerys.map(element => (
                                    { 
                                        path: element.path,
                                        typeCTA: element.typeCTA,
                                        valueCTA: element.valueCTA,
                                    }
                                )));
                            });
                        }
                    }
                    // console.log({
                    //     listGalleryCategory,
                    //     listGalleryBrand
                    // });

                    data = {
                        ...data,
                        listProductCategory: listProductCategory.data,
                        listGallery: [
                            ...listGalleryCategory,
                            ...listGalleryBrand,
                        ],
                    }
                } 

                /**
                 * =======================================================================================================
                 * ===================================MÀN HÌNH CHI TIẾT SEGMENT S10=======================================
                 * =======================================================================================================
                 */
                if (type == TYPE_LIST_SEGMENT) {
                    let listProductSegment;
                    if (segmentID) {
                        listProductSegment = await this.getListProductHaveSegmentIDScreenS10({ keyword, categoryID, brandID, page, limit, segmentID });
                    } else {
                        listProductSegment = await this.getListProductSegmentCategoryScreenS10({ keyword, categoryID, brandID, page, limit });
                    }
                    // DANH SÁCH SEGMENT 
                    let listSegment      = await SEGMENT_MODEL.getListActive({ segmentID });
                  
                    data = {
                        ...data,
                        listProductSegment: listProductSegment.data.listProduct,
                        segmentChoice:      listProductSegment.data.segmentChoice,
                        listSegment: listSegment.data
                    }
                }

                data = {
                    ...data,
                    listCategory: listCategory.data,
                    listBrand: listBrand.data,
                    listBigSale:  listBigSale.data,
                    infoFlashSale:  listFlashSale.data,
                }
                return resolve({
                    error: false,
                    data: data
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductCategory({ category, page = 1, limit = 10 }){
        return new Promise(async resolve => {
            try {
                // const limit = 10;
                const STATUS_ACTIVE = 1;
                let dataFind = { status: STATUS_ACTIVE };

                if (category) {
                    if(!checkObjectIDs(category))
                        return resolve({ error: true, message: 'id_invalid' });
                    dataFind.category = ObjectID(category);
                }
                let listProduct = await PRODUCT_COLL.find({ ...dataFind }, {
                    name: 1, price: 1, category: 1, avatar: 1
                })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ stt: 1, modifyAt: -1 })
                    // .populate({
                    //     path: "category",
                    //     select: "_id title"
                    // })
                    // .populate({
                    //     path: "brand",
                    //     select: "_id name"
                    // })
                    // .populate({
                    //     path: "segments",
                    //     select: "_id name"
                    // })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    // .populate({
                    //     path: "image",
                    //     select: "_id path"
                    // })
                    .lean();
                
                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });
                
                let totalItem = await PRODUCT_COLL.count(dataFind);
                let pages = Math.ceil(totalItem/limit);
                    
                return resolve({ error: false, data: listProduct, page, pages });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductBrand({ brand, page = 1, limit = 10 }){
        return new Promise(async resolve => {
            try {
                // const limit = 10;
                const STATUS_ACTIVE = 1;
                let dataFind = { status: STATUS_ACTIVE };

                if (brand) {
                    if(!checkObjectIDs(brand))
                        return resolve({ error: true, message: 'id_invalid' });
                    dataFind.brand = brand;
                }

                let listProduct = await PRODUCT_COLL.find({ ...dataFind })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ stt: 1, modifyAt: -1 })
                    .populate({
                        path: "category",
                        select: "_id title"
                    })
                    .populate({
                        path: "brand",
                        select: "_id name"
                    })
                    .populate({
                        path: "segments",
                        select: "_id name"
                    })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    .populate({
                        path: "image",
                        select: "_id path"
                    })
                    .lean();
                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });

                return resolve({ error: false, data: listProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductSegment({ segment, page = 1, limit = 10 }) {
        return new Promise(async resolve => {
            try {
                // const limit = 10;
                const STATUS_ACTIVE = 1;
                let dataFind = { status: STATUS_ACTIVE };
                
                if (segment) {
                    if(!checkObjectIDs(segment))
                        return resolve({ error: true, message: 'id_invalid' });
                    dataFind.segments = segment;
                }

                let listProduct = await PRODUCT_COLL.find({ ...dataFind })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ stt: 1, modifyAt: -1 })
                    .populate({
                        path: "category",
                        select: "_id title"
                    })
                    .populate({
                        path: "brand",
                        select: "_id name"
                    })
                    .populate({
                        path: "segments",
                        select: "_id name"
                    })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    .populate({
                        path: "image",
                        select: "_id path"
                    })
                    .lean();
                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });

                return resolve({ error: false, data: listProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductUserSegment({ userID, page = 1, limit = 10 }){
        return new Promise(async resolve => {
            try {
                // const limit = 10;
                if(!checkObjectIDs(userID))
                    return resolve({ error: true, message: 'id_invalid' });
                const STATUS_ACTIVE = 1;
                let dataFind = { status: STATUS_ACTIVE };

                /**
                 * 1// LẤY DANH SÁCH SEGMENT CỦA USER ádasdádadasd
                 */
                let listSegmentOfUser = await USER_SEGMENT_MODEL.getListSegmentOfUser({ userID })            

                let listSegmentID = [];
                if (listSegmentOfUser.error == false && listSegmentOfUser.data.length) {
                    listSegmentID = listSegmentOfUser.data.map( segment=> {
                        if (segment.segment) {
                            return segment.segment._id.toString()
                        }
                    });
                    listSegmentID = listSegmentID.filter(item => item);
                    // listSegmentID = [...new Set(listSegmentID)];
                    // listSegmentID = listSegmentOfUser.data.map( segment=> segment.segments);;
                }

                dataFind = {
                    ...dataFind,
                    segments: {
                        $in: listSegmentID
                    }
                }
                /**
                 * 2// TÌM KIẾM DANH SÁCH SẢN PHẨM CỦA SEGMENT
                 */
                let listProduct = await PRODUCT_COLL.find({ ...dataFind })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ stt: 1, modifyAt: -1 })
                    .populate({
                        path: "category",
                        select: "_id title"
                    })
                    .populate({
                        path: "brand",
                        select: "_id name"
                    })
                    .populate({
                        path: "segments",
                        select: "_id name"
                    })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    .populate({
                        path: "image",
                        select: "_id path"
                    })
                    .lean();
                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });

                return resolve({ error: false, data: listProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductUserLikeProduct({ categoryID, brandID, userID, page = 1, limit = 10 }){
        return new Promise(async resolve => {
            try {
                // const limit = 10;
                const STATUS_ACTIVE = 1;
                let dataFind = { status: STATUS_ACTIVE };
                
                if(!checkObjectIDs(userID))
                    return resolve({ error: true, message: 'id_invalid' });
                    
                if(categoryID && !checkObjectIDs(categoryID))
                    return resolve({ error: true, message: 'id_invalid' });
                categoryID && (dataFind.category = categoryID);
                
                if(brandID && !checkObjectIDs(brandID))
                    return resolve({ error: true, message: 'id_invalid' });
                brandID && (dataFind.brand = brandID);
                
                /**
                 * 1// LẤY DANH SÁCH PRODUCT CỦA USER LIKE
                 */
                let listProductLikedByUser = await USER_LIKE_PRODUCT_MODEL.getListProductOfUser({ userID });  
    
                let listProductID = [];
                if (listProductLikedByUser.error == false && listProductLikedByUser.data.length) {
                    listProductID = listProductLikedByUser.data.map( product=> product.product);
                    dataFind = {
                        ...dataFind,
                        _id: {
                            $in: listProductID
                        }
                    }
                } else {
                    dataFind = {
                        ...dataFind,
                        _id: {
                            $in: []
                        }
                    }
                }

                let listSegmentActive = await SEGMENT_MODEL.getListActiveV2({  });
                let listSegmentID = listSegmentActive.data.map( item => item._id );
                dataFind = {
                    ...dataFind,
                    segments: {
                        $in: listSegmentID
                    }
                }
               
                /**
                 * 2// TÌM KIẾM DANH SÁCH SẢN PHẨM CỦA TRONG ARRAY SẢN PHẨM (listProductID)
                 */
                let listProduct = await PRODUCT_COLL.find({ ...dataFind }, {
                    name: 1, price: 1, avatar: 1
                })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ stt: 1, modifyAt: -1 })
                    .populate({
                        path: "avatar",
                        select: "path"
                    })
                    .lean();
              
                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });

                // Lấy danh sách bigsale và flashsale
                let listBigsaleAndFlashsale = await this.getListBigSaleAndInfoFlashSale();
                let { listBigSale, infoFlashSale } = listBigsaleAndFlashsale.data;

                let data = {
                    listProduct,
                    listBigSale,
                    infoFlashSale
                }

                let totalItem = await PRODUCT_COLL.count(dataFind);
                let pages = Math.ceil(totalItem/limit);

                return resolve({ error: false, data: data, page: Number(page), pages });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListCreateAt({ createAt, page = 1, limit = 10 }){
        return new Promise(async resolve => {
            try {
                // const limit = 10;
                const SORT_DESC = 1; 
                const SORT_ASC  = -1; 
                const SORT_VALUE_VALID = [ SORT_DESC, SORT_ASC]

                if (!createAt || Number.isNaN(Number(createAt)) || !SORT_VALUE_VALID.includes(Number(createAt))) {
                    return resolve({ error: true, message: "createAt_invalid" });
                }
                let dataFind = { createAt: createAt }
                let listSegmentActive = await SEGMENT_MODEL.getListActiveV2({  });
                let listSegmentID = listSegmentActive.data.map( item => item._id );
                dataFind = {
                    ...dataFind,
                    segments: {
                        $in: listSegmentID
                    }
                }
                /**
                 * 2// TÌM KIẾM DANH SÁCH SẢN PHẨM CỦA TRONG ARRAY SẢN PHẨM (listProductID)
                 */
                let listProduct = await PRODUCT_COLL.find({  })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ ...dataFind })
                    .populate({
                        path: "category",
                        select: "_id title"
                    })
                    .populate({
                        path: "brand",
                        select: "_id name"
                    })
                    .populate({
                        path: "segments",
                        select: "_id name"
                    })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    .populate({
                        path: "image",
                        select: "_id path"
                    })
                    .lean();

                    if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });

                return resolve({ error: false, data: listProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductSegmentCategory({ categoryID, page = 1, limit = 10 }) {
        return new Promise(async resolve => {
            try {
                // const limit = 10;

                if(categoryID && !checkObjectIDs(categoryID))
                    return resolve({ error: true, message: 'id_invalid' });
                const STATUS_ACTIVE = 1;
                let dataFind = { status: STATUS_ACTIVE, category: categoryID };
                categoryID && (dataFind.category = categoryID)
                /**
                 * 1// TÌM KIẾM SEGMENT CỦA CATEGORY
                 */
                if (categoryID) {
                    let listSegmentOfCategory = await CATEGORY_SEGMENT_MODEL.getListSegmentOfCategory({ categoryID })            
                    let listSegmentID = [];
                    if (listSegmentOfCategory.error == false && listSegmentOfCategory.data.length) {
                        listSegmentID = listSegmentOfCategory.data.map( segment=> segment.segments._id);;
                    }
                    listSegmentID = listSegmentID.filter( item => item);
                    listSegmentID = [...new Set(listSegmentID)];

                    dataFind = {
                        ...dataFind,
                        segments: {
                            $in: listSegmentID
                        }
                    }
                }
                let listSegmentActive = await SEGMENT_MODEL.getListActiveV2({  });
                let listSegmentID = listSegmentActive.data.map( item => item._id );
                dataFind = {
                    ...dataFind,
                    segments: {
                        $in: listSegmentID
                    }
                }
                
                let listProduct = await PRODUCT_COLL.find({ ...dataFind })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ stt: 1, modifyAt: -1 })
                    .populate({
                        path: "category",
                        select: "_id title"
                    })
                    .populate({
                        path: "brand",
                        select: "_id name"
                    })
                    .populate({
                        path: "segments",
                        select: "_id name"
                    })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    .populate({
                        path: "image",
                        select: "_id path"
                    })
                    .lean();

                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });

                return resolve({ error: false, data: listProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductSegmentCategoryScreenS10({ keyword, categoryID, brandID, page = 1, limit = 10 }) {
        return new Promise(async resolve => {
            try {
                if(categoryID && !checkObjectIDs(categoryID))
                    return resolve({ error: true, message: 'id_invalid' });
                const STATUS_ACTIVE = 1;
                let dataFind = { status: STATUS_ACTIVE };
                categoryID && (dataFind.category = categoryID)

                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    dataFind.$or = [
                        { name: new RegExp(key, 'i') },
                    ]
                }

                if(brandID && !checkObjectIDs(brandID))
                    return resolve({ error: true, message: 'id_invalid' });
                brandID && (dataFind.brand = brandID)
                
                /**
                 * 1// TÌM KIẾM SEGMENT CỦA CATEGORY
                 */

                let listSegmentOfCategory = await CATEGORY_SEGMENT_MODEL.getListSegmentOfCategory({ categoryID });            
                let listSegment = [];
                // if (listSegmentOfCategory.error == false && listSegmentOfCategory.data.length) {
                //     listSegmentID = listSegmentOfCategory.data.map( segment=> segment.segment);;
                // }
                let segmentChoice = {};
                if (!listSegmentOfCategory.error && listSegmentOfCategory.data.length) {
                    // let listSegmentIDOfCategory = listSegmentOfCategory.data.map(item => {
                    //     if (item.category && item.segment) {
                    //         return item.segment._id.toString()
                    //     }
                    // });
                    listSegment =  listSegmentOfCategory.data.filter( item => item.category && item.segment && item.segment._id.toString() != listSegmentOfCategory.data[0].segment._id.toString());
                    listSegment = [...new Set(listSegment)];
                    
                    /**
                     * ===================LẤY SEGMENT CỦA CATEGORY MỚI NHẤT===================
                     */
                    segmentChoice = listSegmentOfCategory.data[0];
                    dataFind = {
                        ...dataFind,
                        segments: {
                            $in: [listSegmentOfCategory.data[0].segment._id]
                        }
                    }
                }
               
                
                let listProduct = await PRODUCT_COLL.find({ ...dataFind }, {
                    name: 1, price: 1, category: 1, segments: 1, avatar: 1
                })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ stt: 1, modifyAt: -1 })
                    .populate({
                        path: "category",
                        select: "_id title"
                    })
                    // .populate({
                    //     path: "brand",
                    //     select: "_id name"
                    // })
                    .populate({
                        path: "segments",
                        select: "_id name"
                    })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    // .populate({
                    //     path: "image",
                    //     select: "_id path"
                    // })
                    .lean();
                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });
                    
                let data = {
                    listProduct,
                    // listSegment: listSegment,
                    segmentChoice
                }

                let totalItem = await PRODUCT_COLL.count(dataFind);
                let pages = Math.ceil(totalItem/limit);

                return resolve({ error: false, data: data, pageCurrent: +page, totalPage: +pages });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductHaveSegmentIDScreenS10({ keyword, categoryID, brandID, segmentID, page = 1, limit = 10 }) {
        return new Promise(async resolve => {
            try {
                if(categoryID && !checkObjectIDs(categoryID))
                    return resolve({ error: true, message: 'id_invalid' });
                const STATUS_ACTIVE = 1;
                let dataFind = { status: STATUS_ACTIVE };
                categoryID && (dataFind.category = categoryID)

                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    dataFind.$or = [
                        { name: new RegExp(key, 'i') },
                    ]
                }

                if(brandID && !checkObjectIDs(brandID))
                    return resolve({ error: true, message: 'id_invalid' });
                brandID && (dataFind.brand = brandID)
                
                if(segmentID) {
                    if (!checkObjectIDs(segmentID)) {
                        return resolve({ error: true, message: 'id_invalid' });
                    }
                    segmentID && (dataFind.segments = { $in: [ segmentID ] });
                } else {
                    // let listSegmentActive = await SEGMENT_MODEL.getListActiveV2({  });
                    // console.log({
                    //     listSegmentActive
                    // });
                }
                
                let listProduct = await PRODUCT_COLL.find({ ...dataFind }, {
                    name: 1, price: 1, category: 1, segments: 1, avatar: 1
                })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ stt: 1, modifyAt: -1 })
                    .populate({
                        path: "category",
                        select: "_id title"
                    })
                    .populate({
                        path: "segments",
                        select: "_id name"
                    })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    .lean();
                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });
                
                let data = {
                    listProduct,
                }
               
                if (segmentID) {
                    let segmentChoice      = await SEGMENT_MODEL.getInfoSegment({ segmentID });
                    data = {
                        ...data,
                        segmentChoice: segmentChoice.data
                    }
                }
                
                let totalItem = await PRODUCT_COLL.count(dataFind);
                let pages = Math.ceil(totalItem/limit);

                return resolve({ error: false, data: data, pageCurrent: +page, totalPage: +pages });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    /**
     * Danh sách bigsale
     * Danh sách flashale
     * Depv
     */
    getListBigSaleAndInfoFlashSale() {
        return new Promise(async resolve => {
            try {
                 /**
                 * ===========================DANH SÁCH BIGSALE================================
                 */
                let listBigSale = await BIG_SALE_MODEL.getListBigSaleActive({ });
            
                /**
                 * ===========================DANH SÁCH FLASH SALE================================
                 */
                let listFlashSale = await FLASH_SALE_MODEL.getListFlashSaleActive({ });
                let data = {
                    listBigSale: listBigSale.data,
                    infoFlashSale: listFlashSale.data,
                }
                return resolve({ error: false, data });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductSegmentsCategoryScreenS07({ keyword, categoryID, brandID, page = 1, limit = 10 }) {
        return new Promise(async resolve => {
            try {
                // const limit = 10;

                if(categoryID && !checkObjectIDs(categoryID))
                    return resolve({ error: true, message: 'id_invalid' });
                const STATUS_ACTIVE = 1;
                let dataFind = { status: STATUS_ACTIVE };
                categoryID && (dataFind.category = categoryID)
                
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    dataFind.$or = [
                        { name: new RegExp(key, 'i') },
                    ]
                }

                if(brandID && !checkObjectIDs(brandID))
                    return resolve({ error: true, message: 'id_invalid' });
                brandID && (dataFind.brand = brandID)
                /**
                 * 1// TÌM KIẾM SEGMENT CỦA CATEGORY
                 */
                let listSegmentOfCategory   = await CATEGORY_SEGMENT_MODEL.getListSegmentOfCategorySort2SegmentScreenS08({ categoryID });
                let listSegmentIDOfCategory = listSegmentOfCategory.data.map(item => {
                    if (item.category && item.segment) {
                        return item.segment._id.toString()
                    }
                });
                listSegmentIDOfCategory = listSegmentIDOfCategory.filter( item => item);
                listSegmentIDOfCategory = [...new Set(listSegmentIDOfCategory)];
                // GỌI VỀ SEGMENT ĐỂ SORT
                let listSegmentSort = await SEGMENT_MODEL.getListSegmentActive({ listSegmentID: listSegmentIDOfCategory })
                listSegmentIDOfCategory = listSegmentSort.data.map(item => item._id);

                let listSegment = [];
               
                if (listSegmentIDOfCategory && listSegmentIDOfCategory.length && listSegmentIDOfCategory[0]) {
                    /**
                     * ===================LẤY SEGMENT CỦA CATEGORY MỚI NHẤT===================
                     */
                    // console.log({
                    //     dataFind
                    // });
                    for (let segment of listSegmentIDOfCategory) {
                        dataFind = {
                            ...dataFind,
                            segments: {
                                $in: [segment]
                            }
                        }
                        
                        let listProduct = await PRODUCT_COLL.find({ ...dataFind }, {
                            name: 1, price: 1, category: 1, avatar: 1
                        })
                            .limit(limit * 1)
                            .skip((page - 1) * limit)
                            .sort({ stt: 1, modifyAt: -1 })
                            .populate({
                                path: "avatar",
                                select: "_id path"
                            })
                            .lean();
                        if(!listProduct)
                            return resolve({ error: true, message: "not_found_products_list" });

                        let infoSegment = await SEGMENT_MODEL.getInfoV2({ segmentID: segment });
                        
                        listSegment = [
                            ...listSegment,
                            {
                                listProduct,
                                segment: {
                                    segment: infoSegment.data
                                }
                            }
                        ]
                    }
                }
                // Lấy danh sách bigsale và flashsale
                let listBigsaleAndFlashsale = await this.getListBigSaleAndInfoFlashSale();
                let { listBigSale, infoFlashSale } = listBigsaleAndFlashsale.data;

                let data = {
                    listSegment,
                    listBigSale,
                    infoFlashSale
                }
                return resolve({ error: false, data: data });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductSegmentsCategoryScreenS08({ keyword, categoryID, brandID, page = 1, limit = 10 }) {
        return new Promise(async resolve => {
            try {
                // const limit = 10;

                if(categoryID && !checkObjectIDs(categoryID))
                    return resolve({ error: true, message: 'id_invalid' });
                const STATUS_ACTIVE = 1;
                let dataFind = { status: STATUS_ACTIVE };
                categoryID && (dataFind.category = categoryID)

                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    dataFind.$or = [
                        { name: new RegExp(key, 'i') },
                    ]
                }

                /**
                 * ================FILTER LIKE=================
                 */

                if(brandID && !checkObjectIDs(brandID))
                    return resolve({ error: true, message: 'id_invalid' });
                brandID && (dataFind.brand = brandID)
                /**
                 * 1// TÌM KIẾM SEGMENT CỦA CATEGORY
                 */
                let listSegmentOfCategory   = await CATEGORY_SEGMENT_MODEL.getListSegmentOfCategorySort2SegmentScreenS08({ categoryID });  
                let listSegmentIDOfCategory = listSegmentOfCategory.data.map(item => {
                    if (item.category && item.segment) {
                        return item.segment._id.toString()
                    }
                });
                listSegmentIDOfCategory = listSegmentIDOfCategory.filter( item => item);
                listSegmentIDOfCategory     = [...new Set(listSegmentIDOfCategory)];

                let listSegment = [];

                /**
                 * 2// TÌM KIẾM SEGMENT CỦA BRANDID
                 */
                let listSegmentOfBrand      = await BRAND_SEGMENT_MODEL.getListSegmentOfBrand({ brandID });            
                let listSegmentIDOfBrand    = listSegmentOfBrand.data.map(item => {
                    if (item.brand && item.segment) {
                        return item.segment._id.toString();
                    }
                });
                listSegmentIDOfBrand = listSegmentIDOfBrand.filter(item => item);
                listSegmentIDOfBrand     = [...new Set(listSegmentIDOfBrand)];

                let listSegmentOfCategoryVsBrand = listSegmentIDOfCategory.filter(item => 
                    listSegmentIDOfBrand.includes(item)
                );

                // GỌI VỀ SEGMENT ĐỂ SORT
                let listSegmentSort = await SEGMENT_MODEL.getListSegmentActive({ listSegmentID: listSegmentOfCategoryVsBrand })
                listSegmentOfCategoryVsBrand = listSegmentSort.data.map(item => item._id);
                
                if (listSegmentOfCategoryVsBrand && listSegmentOfCategoryVsBrand.length && listSegmentOfCategoryVsBrand[0]) {
                    // listSegmentOfCategoryVsBrand = listSegmentOfCategoryVsBrand.splice(0, 2);
                    /**
                     * ===================LẤY SEGMENT CỦA CATEGORY MỚI NHẤT===================
                     */
                   
                    for (let segment of listSegmentOfCategoryVsBrand) {
                        dataFind = {
                            ...dataFind,
                            segments: {
                                $in: [segment]
                            }
                        }
                      
                        let listProduct = await PRODUCT_COLL.find({ ...dataFind }, {
                            name: 1, price: 1, category: 1, avatar: 1
                        })
                            .limit(limit * 1)
                            .skip((page - 1) * limit)
                            .sort({ stt: 1, modifyAt: -1 })
                            .populate({
                                path: "avatar",
                                select: "_id path"
                            })
                            .lean();
                        if(!listProduct)
                            return resolve({ error: true, message: "not_found_products_list" });
                        
                        let infoSegment = await SEGMENT_MODEL.getInfoV2({ segmentID: segment });
                        listSegment = [
                            ...listSegment,
                            {
                                listProduct,
                                segment: {
                                    segment: infoSegment.data
                                }
                            }
                        ]
                    }
                }
                // Lấy danh sách bigsale và flashsale
                // let listBigsaleAndFlashsale = await this.getListBigSaleAndInfoFlashSale();
                // let { listBigSale, infoFlashSale } = listBigsaleAndFlashsale.data;

                // let data = {
                //     listSegment,
                //     listBigSale,
                //     infoFlashSale
                // }
                return resolve({ error: false, data: listSegment });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductSegmentUserCategory({ userID, keyword, categoryID, brandID, page = 1, limit = 10 }) {
        return new Promise(async resolve => {
            try {
                // const limit = 10;

                if(categoryID && !checkObjectIDs(categoryID, userID))
                    return resolve({ error: true, message: 'id_invalid' });
                const STATUS_ACTIVE = 1;
                let dataFind = { status: STATUS_ACTIVE};
                categoryID && (dataFind.category = categoryID);

                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    dataFind.$or = [
                        { name: new RegExp(key, 'i') },
                    ]
                }

                if(brandID && !checkObjectIDs(brandID))
                    return resolve({ error: true, message: 'id_invalid' });
                brandID && (dataFind.brand = brandID)
                /**
                 * 1// TÌM KIẾM SEGMENT CỦA CATEGORY
                 */
                // let listSegmentOfCategory = await CATEGORY_SEGMENT_MODEL.getListSegmentOfCategory({ categoryID })            
                // let listSegmentID = [];
                // if (listSegmentOfCategory.error == false && listSegmentOfCategory.data.length) {
                //     listSegmentID = listSegmentOfCategory.data.map( segment=> segment.segments.toString());;
                // }
                /**
                 * 2// TÌM KIẾM SEGMENT CỦA USER
                 */
                 let listSegmentOfUser = await USER_SEGMENT_MODEL.getListSegmentOfUser({ userID });   
                      
                 if (listSegmentOfUser.error == false && listSegmentOfUser.data.length ) {
                    
                    let listSegmentID = [...listSegmentOfUser.data.map( segment=> {
                        if (segment.segment) {
                            return segment.segment._id.toString()
                        }
                    })];
                    listSegmentID = listSegmentID.filter(item => item);
                    // let listSegmentSort = await SEGMENT_MODEL.getListSegmentActive({ listSegmentID: listSegmentID })
                    // if (!listSegmentSort.error && listSegmentSort.data.length) {
                    //     listSegmentID = listSegmentSort.data.map(item => item._id);
                    // }
                    // console.log({
                    //     listSegmentID
                    // });
                    // let listSegmentIDAfterSet = [...new Set(listSegmentID)]
                    dataFind = {
                        ...dataFind,
                        segments: {
                            $in: listSegmentID
                        }
                    }
                } else {
                    dataFind = {
                        _id: []
                    }
                }
                let listProduct = await PRODUCT_COLL.find({ ...dataFind }, {
                    name: 1, price: 1, avatar: 1, segments: 1
                })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ stt: 1, modifyAt: -1 })
                    // .populate({
                    //     path: "category",
                    //     select: "_id title"
                    // })
                    // .populate({
                    //     path: "brand",
                    //     select: "_id name"
                    // })
                    .populate({
                        path: "segments",
                        select: "_id name"
                    })
                    .populate({
                        path: "avatar",
                        select: "_id path"
                    })
                    // .populate({
                    //     path: "image",
                    //     select: "_id path"
                    // })
                    .lean();

                if(!listProduct)
                    return resolve({ error: true, message: "not_found_products_list" });

                let totalItem = await PRODUCT_COLL.count(dataFind);
                let pages = Math.ceil(totalItem/limit);

                return resolve({ error: false, data: listProduct, page, pages });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoV2({ productID }){
        return new Promise(async resolve => {
            try {
                let infoProduct = await PRODUCT_COLL.findById(productID)
                    .populate({
                        path: "category",
                        select: "title"
                    })
                    .populate({
                        path: "brand",
                        select: "name image",
                        populate: {
                            path: "image",
                            select: "path"
                        }
                    })
                    .populate({
                        path: "segments",
                        select: "name"
                    })
                    .populate({
                        path: "avatar",
                        select: "path"
                    })
                    .populate({
                        path: "images certifiedImages",
                        select: "path order"
                    })
                    .populate({
                        path: "qr_code",
                        select: "image",
                        populate: {
                            path: "image",
                            select: "path"
                        }
                    })
                    .lean();
                if(!infoProduct)
                    return resolve({ error: true, message: "not_found_product_info" });
              
                return resolve({ error: false, data: infoProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ productID }){
        return new Promise(async resolve => {
            try {
                let infoProduct = await PRODUCT_COLL.findById(productID)
                    .populate({
                        path: "category",
                        select: "title"
                    })
                    .populate({
                        path: "brand",
                        select: "name image",
                        populate: {
                            path: "image",
                            select: "path"
                        }
                    })
                    .populate({
                        path: "segments",
                        select: "name"
                    })
                    .populate({
                        path: "avatar",
                        select: "path"
                    })
                    .populate({
                        path: "images certifiedImages",
                        select: "path order",
                        options: { sort: { order: 1 } },
                    })
                    .populate({
                        path: "certifiedImages",
                        select: "path",
                        // options: { sort: { order: 1 } },
                    })
                    .populate({
                        path: "qr_code",
                        select: "image",
                        populate: {
                            path: "image",
                            select: "path"
                        }
                    })
                    .lean();
                if(!infoProduct)
                    return resolve({ error: true, message: "not_found_product_info" });

                // Lấy danh sách bigsale và flashsale
                let listBigsaleAndFlashsale = await this.getListBigSaleAndInfoFlashSale();
                let { listBigSale, infoFlashSale } = listBigsaleAndFlashsale.data;
                let data = {
                    infoProduct,
                    listBigSale,
                    infoFlashSale
                }
                return resolve({ error: false, data: data });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoProduct({ productID, userID }){
        return new Promise(async resolve => {
            try {
                let infoProduct = await PRODUCT_COLL.findById(productID)
                    .populate({
                        path: "category",
                        select: "title"
                    })
                    .populate({
                        path: "brand",
                        select: "name image",
                        populate: {
                            path: "image",
                            select: "path"
                        }
                    })
                    .populate({
                        path: "segments",
                        select: "name"
                    })
                    .populate({
                        path: "avatar",
                        select: "path"
                    })
                    .populate({
                        path: "images certifiedImages",
                        select: "path",
                        options: { sort: { order: 1 } },
                    })
                    .populate({
                        path: "qr_code",
                        select: "image",
                        populate: {
                            path: "image",
                            select: "path"
                        }
                    })
                    .lean();
                if(!infoProduct)
                    return resolve({ error: true, message: "not_found_product_info" });

                let listProductCategory = await PRODUCT_COLL.find({
                    category: infoProduct.category,
                    _id: {
                        $nin: [productID]
                    }
                }, {
                    name: 1, price: 1, category: 1
                }).limit(10).populate({
                    path: 'avatar',
                    select: 'path'
                }).sort({ stt: 1, modifyAt: -1 });

                let infoUserLikeProduct = await USER_LIKE_PRODUCT_MODEL.getInfoLikeByProductID({ product: productID, user: userID });
                let infoProductChange = {
                    ...infoProduct,
                    isLike: infoUserLikeProduct.data
                }
               
                // Lấy danh sách bigsale và flashsale
                let listBigsaleAndFlashsale = await this.getListBigSaleAndInfoFlashSale();
                let { listBigSale, infoFlashSale } = listBigsaleAndFlashsale.data;
                let data = {
                    infoProduct: infoProductChange,
                    listBigSale,
                    infoFlashSale,
                    listProductCategory,
                }
                return resolve({ error: false, data: data });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    likeProduct({ productID, userID }){
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK CÁC TRƯỜNG OBJECTID CÓ ĐÚNG KIỂU OBJECTID
                 */
                if(!checkObjectIDs(productID) || !checkObjectIDs(userID))
                    return resolve({ error: true, message: 'id_invalid' });
                

                let infoProduct = await PRODUCT_COLL.findById(productID);

                /**
                 * 1//  INSERT USER_LIKE_PRODUCT
                 */
                let infoUserLikeProduct = await USER_LIKE_PRODUCT_MODEL.insert({ product: productID, user: userID });
                if(infoUserLikeProduct.error)
                    return resolve({ error: true, message: "cannot_insert_user_like_product_info" });
                /**
                 * 2//  INSERT USER_SEGMENT
                 */
                const TYPE_CUSTOMER = 1;
               
                if (infoUserLikeProduct.message == 'insert') {
                    let infoUserSegment = await USER_SEGMENT_MODEL.insert({ user: userID, segment: infoProduct.segments, type: TYPE_CUSTOMER });
                } else if (infoUserLikeProduct.message == 'update') {
                    let infoUserSegment = await USER_SEGMENT_MODEL.deleteSegmentOfUser({ userID, segment: infoProduct.segments, type: TYPE_CUSTOMER });
                }

                return resolve(infoUserLikeProduct);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ productID }) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(productID))
                    return resolve({ error: true, message: "id_invalid" });

                const infoAfterDelete = await PRODUCT_COLL.findByIdAndRemove(productID);

                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "product_is_not_exists" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
