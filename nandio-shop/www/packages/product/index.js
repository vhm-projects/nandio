const PRODUCT_MODEL 		= require('./models/product').MODEL;
const PRODUCT_COLL  		= require('./databases/product-coll');
const PRODUCT_ROUTES       = require('./apis/product');

module.exports = {
    PRODUCT_ROUTES,
    PRODUCT_COLL,
    PRODUCT_MODEL,
}
