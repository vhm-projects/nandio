"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { uploadSingle }                      = require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_BRAND } 			    = require('../constants/brand.uri');

/**
 * MODELS
 */
const BRAND_MODEL     = require('../models/brand').MODEL;
const { IMAGE_MODEL } = require('../../image');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ====================== ************************ ================================
             * ======================    QUẢN LÝ THƯƠNG HIỆU   ================================
             * ====================== ************************ ================================
             */

            /**
             * Function: Tạo Brand (View API)
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_BRAND.ADD_BRAND]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Add brand',
                    type: 'view',
                    code: CF_ROUTINGS_BRAND.ADD_BRAND,
                    inc: path.resolve(__dirname, '../views/add_brand.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function(req, res) {
                        ChildRouter.renderToView(req, res);
					}],
                    post: [ uploadSingle, async function (req, res) {
                        const { name, description, linkWebsite, status, pathImage, pathGallerys } = JSON.parse(req.body.data);
                        
                        let image = null;
                        if(req.file){
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size,
                                name: req.fileName,
                                path: pathImage[0]
                            });
                            image = infoImageAfterInsert.data._id;
                        }

                        const infoBrandInsert = await BRAND_MODEL.insert({ name, description, linkWebsite, status, image, pathGallerys });

                        res.json(infoBrandInsert);
                    }]
                },
            },

            /**
             * Function: Xóa Brand
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_BRAND.REMOVE_BRAND]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { brandID } = req.params;

                        const infoBrandRemove = await BRAND_MODEL.remove({ brandID });

                        res.json(infoBrandRemove);
                    }]
                },
            },

            /**
             * Function: Cập nhật Brand (View API)
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_BRAND.UPDATE_BRAND]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Update brand',
                    type: 'view',
                    code: CF_ROUTINGS_BRAND.UPDATE_BRAND,
                    inc: path.resolve(__dirname, '../views/update_brand.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function(req, res) {
                        let { brandID } = req.query;
                        let infoBrandUpdate = await BRAND_MODEL.getInfo({ brandID });
                        
                        ChildRouter.renderToView(req, res, { infoBrandUpdate: infoBrandUpdate.data });
					}],
                    post: [ uploadSingle, async function (req, res) {
                        const { brandID } = req.query;
                        const { name, description, linkWebsite, status, pathImage, pathGallerysUpdate, imgGalleryOldIsDeleted, arrObjectImageCTA } = JSON.parse(req.body.data);
                        
                        if(arrObjectImageCTA && arrObjectImageCTA.length){
                            await IMAGE_MODEL.updateCTA({ arrObjectImageCTA });
                        }

                        let image = null;
                        if(req.file){
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size,
                                name: req.fileName,
                                path: pathImage[0]
                            });
                            image = infoImageAfterInsert.data._id;
                        }

                        const infoBrandUpdate = await BRAND_MODEL.update({ brandID, name, description, linkWebsite, status, image, pathGallerysUpdate, imgGalleryOldIsDeleted });

                        res.json(infoBrandUpdate);
                    }]
                },
            },

            /**
             * Function: Xem chi tiết Brand
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_BRAND.DETAIL_BRAND]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { brandID } = req.params;

                        const infoDetailBrand = await BRAND_MODEL.getInfo({ brandID });

                        res.json(infoDetailBrand);
                    }]
                },
            },

            /**
             * Function: Xem chi tiết Brand
             * Date: 20/06/2021
             * Dev: VyPQ
             */
             [CF_ROUTINGS_BRAND.UPDATE_BRAND_STATUS]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        const { stt, brandID } = req.body;

                        const infoDetailBrand = await BRAND_MODEL.updateSTT({ brandID, stt });

                        res.json(infoDetailBrand);
                    }]
                },
            },

            /**
             * Function: Xem chi tiết Brand (View)
             * Date: 21/06/2021
             * Dev: VyPQ
             */
             [CF_ROUTINGS_BRAND.INFO_DETAIL_BRAND]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    view: 'index.ejs',
                    inc: path.resolve(__dirname, '../views/info_detail_brand.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        const { brandID } = req.params;

                        const infoDetailBrand = await BRAND_MODEL.getInfo({ brandID });

                        ChildRouter.renderToView(req, res, { infoDetailBrand: infoDetailBrand.data });
                    }]
                },
            },

            /**
             * Function: Xem danh sách Brand
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_BRAND.LIST_BRAND]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: "List Brand - NANDIO",
                    type: 'view',
                    view: 'index.ejs',
                    inc: path.resolve(__dirname, '../views/list_brand'),
					code: CF_ROUTINGS_BRAND.LIST_BRAND  
                },
                methods: {
                    get: [ async function (req, res) {
                        const listBrand = await BRAND_MODEL.getList();
                        ChildRouter.renderToView(req, res, { listBrand: listBrand.data });
                    }]
                },
            },

            /**
             * Function: Danh sách brand cho enduser
             * Date: 13/08/2021
             * Dev: Depv
             */
             [CF_ROUTINGS_BRAND.LIST_BRAND_ENDUSER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { limit, page } = req.query;
                        let STATUS_ACTIVE = 1;
                        let listBrand = await BRAND_MODEL.getListForEnduser({ status: STATUS_ACTIVE, limit, page });
                        res.json(listBrand);
                    }]
                },
            },

        }
    }
};
