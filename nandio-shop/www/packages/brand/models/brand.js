"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { checkObjectIDs }            = require("../../../utils/utils");

/**
 * MODELS
 */
 const IMAGE_MODEL                   = require('../../image/models/image').MODEL;

/**
 * COLLECTIONS
 */
const BRAND_COLL  					= require('../databases/brand-coll');


class Model extends BaseModel {
    constructor() {
        super(BRAND_COLL);
    }

	insert({ name, description, linkWebsite, status, image, pathGallerys }) {
        return new Promise(async resolve => {
            try {
                if(!name || !description)
                    return resolve({ error: true, message: 'params_invalid' });

                // let isExistName = await BRAND_COLL.findOne({ name });
                // if(isExistName)
                //     return resolve({ error: true, message: 'name_existed' });

                let statusValid = [0, 1];
                if(!statusValid.includes(Number(status))) 
                    return resolve({ error: true, message: 'status_invalid' });

                let dataInsert = { name, description, linkWebsite, status, image, brandCode: name };

                let infoBrandInsert = await this.insertData(dataInsert);
                if(!infoBrandInsert)
                    return resolve({ error: true, message: 'insert_brand_failed' });

                let arrGallery = [];
                if(pathGallerys && pathGallerys.length > 0) {
                    for(let imgGallery of pathGallerys) {
                        let infoImageGalleryAfterInsert = await IMAGE_MODEL.insert({
                            name: imgGallery,
                            path: imgGallery
                        });
                        arrGallery.push(infoImageGalleryAfterInsert.data._id);
                    }

                    if(checkObjectIDs(arrGallery)){                    
                        let infoBrandUpdate = await BRAND_COLL.findByIdAndUpdate({ _id: infoBrandInsert._id }, {
                            $set: { gallerys: arrGallery }
                        }, { new: true });
                        if(!infoBrandUpdate)
                            return resolve({ error: true, message: 'cannot_update_gallery_big_sale' });
                    }
                }

                return resolve({ error: false, data: infoBrandInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    remove({ brandID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(brandID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoBrandRemove = await BRAND_COLL.findByIdAndUpdate(brandID, {
                    status: 2
                });
                if(!infoBrandRemove)
                    return resolve({ error: false, message: 'cannot_remove_brand' });

                return resolve({ error: false, data: infoBrandRemove });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ brandID, name, description, linkWebsite, status, image, pathGallerysUpdate, imgGalleryOldIsDeleted }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(brandID))
                    return resolve({ error: true, message: 'params_invalid' });

                let isExistBrand = await BRAND_COLL.findById({ _id: brandID });
                if(!isExistBrand)
                    return resolve({ error: true, message: 'brand_does_not_exist' });

                // let isExistName = await BRAND_COLL.findOne({ _id: { $nin: brandID }, name });
                // if(isExistName)
                //     return resolve({ error: true, message: 'brand_name_existed' });

                let statusValid = [0, 1];
                if(status && !statusValid.includes(Number(status))) 
                    return resolve({ error: true, message: 'status_invalid' });

                let dataUpdate = { modifyAt: new Date() };

                name         && (dataUpdate.name = name);
                description  && (dataUpdate.description = description);
                status       && (dataUpdate.status = status);
                image        && (dataUpdate.image = image);   
                linkWebsite  && (dataUpdate.linkWebsite = linkWebsite);   

                let infoBrandAfterUpdate = await BRAND_COLL.findByIdAndUpdate({ _id: brandID }, {
                    $set: dataUpdate
                }, { new: true });
                if(!infoBrandAfterUpdate)
                    return resolve({ error: true, message: 'update_brand_failed' });

                //Xóa khỏi hình ảnh cũ trong gallery cập nhật
                if(imgGalleryOldIsDeleted && imgGalleryOldIsDeleted.length){
                    for (let image of imgGalleryOldIsDeleted) {
                        infoBrandAfterUpdate = await BRAND_COLL.findByIdAndUpdate(brandID, {
                            $pull: { gallerys: image.id }
                        }, { new: true });
                    }
                }

                //Thêm các hình ảnh mới cập nhật
                if(pathGallerysUpdate && pathGallerysUpdate.length){
                    for(let image of pathGallerysUpdate) {
                        let infoImageAfterInsert = await IMAGE_MODEL.insert({
                            name: image,
                            path: image
                        });

                        // sau khi thêm hình ảnh thì thêm nó vào gallers của big sale đó luôn
                        infoBrandAfterUpdate = await BRAND_COLL.findByIdAndUpdate(brandID, {
                            $addToSet: { gallerys: infoImageAfterInsert.data._id }
                        }, { new: true });
                    }
                }

                return resolve({ error: false, data: infoBrandAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ brandID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(brandID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoBrand = await BRAND_COLL.findById({ _id: brandID })
                    .populate('image')
                    .populate({
                        path: 'gallerys',
                        options: { sort: { order: 1 } },
                    })
                if(!infoBrand)
                    return resolve({ error: false, message: 'cannot_get_info_brand' });

                return resolve({ error: false, data: infoBrand });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateSTT({ brandID, stt }) {
        return new Promise(async resolve => {
            try {
                
                let dataUpdate = {}
                if(!checkObjectIDs(brandID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }

                if (stt || stt != 0) {
                    dataUpdate.stt      = stt;
                }
                
                let infoAfterUpdate = await BRAND_COLL.findByIdAndUpdate(brandID, {
                    ...dataUpdate
                }, {
                    new: true
                });
                if (!infoAfterUpdate) {
                    return resolve({ error: true, message: "cannot_update" });
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList() {
        return new Promise(async resolve => {
            try {
                let listBrand = await BRAND_COLL.find({ status: { $in: [0, 1] } })
                    .populate('image gallerys')
                    .sort({ stt: 1, modifyAt: -1 })
                if(!listBrand)
                    return resolve({ error: true, message: 'cannot_get_list_brand' });

                return resolve({ error: false, data: listBrand });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListAll({ brandID }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                if (brandID && !checkObjectIDs(brandID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }
                let condition = { status: STATUS_ACTIVE };
                brandID && (condition._id = brandID);
                
                let listBrand = await BRAND_COLL.find({ ...condition }, {
                    gallerys: 1, name: 1, image: 1
                })
                    .populate({
                        path: "gallerys",
                        select: "path typeCTA valueCTA"
                    })
                    .populate({
                        path: "image",
                        select: "path"
                    })
                    .sort({ stt: 1, modifyAt: -1 })
                if(!listBrand)
                    return resolve({ error: true, message: 'cannot_get_list_brand' });

                return resolve({ error: false, data: listBrand });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    getListForEnduser({ status, limit = 10, page = 1 }) {
        return new Promise(async resolve => {
            try {
                limit = Number(limit);
                page  = Number(page);
                let listBrand = await BRAND_COLL.find({ status })
                    .select("name image")
                    .populate({
                        path: "image",
                        select: "path"
                    })
                    .skip((page * limit) - limit)
                    .limit(limit)
                    .sort({ stt: 1, modifyAt: -1 })

                if(!listBrand)
                    return resolve({ error: true, message: 'cannot_get_list_brand' });
                let totalItem = await BRAND_COLL.count({ status });
                let pages = Math.ceil(totalItem/limit);
                return resolve({ error: false, data: { listBrand, currentPage: page, totalPage: pages } });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByStatus({ status }) {
        return new Promise(async resolve => {
            try {
                let listBrand = await BRAND_COLL.find({ status })
                    .select("name image")
                    .populate({
                        path: "image",
                        select: "path"
                    })
                    .sort({ stt: 1, modifyAt: -1 })

                if(!listBrand)
                    return resolve({ error: true, message: 'cannot_get_list_brand' });
                return resolve({ error: false, data: listBrand });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
