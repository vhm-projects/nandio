"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION BRAND CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('brand', {
	name: {
		type: String,
		trim: true,
		unique : true
	},
	brandCode: {
		type: String,
		trim: true,
		unique : true
	},
    description: {
		type: String,
		trim: true
	},
	// STT HIỂN THỊ
	stt : {
		type: Number,
	},
	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 * 2: Xóa
	 */
	status: {
		type: Number,
		default: 1
	},
	// hình ảnh demo của brand
	image: {
        type: Schema.Types.ObjectId,
        ref: 'image'
    },
	/**
	 * Mảng hình ảnh của Thương hiệu
	 */
	gallerys: [{
		type: Schema.Types.ObjectId,
		ref: "image"
	}],
	
	/**
	 * Link website
	 */
	linkWebsite: String,
});
