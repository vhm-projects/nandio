const BASE_ROUTE = '/brand';

const CF_ROUTINGS_BRAND = {
	ADD_BRAND: `${BASE_ROUTE}/add-brand`,
	REMOVE_BRAND: `${BASE_ROUTE}/remove-brand/:brandID`,
	UPDATE_BRAND: `${BASE_ROUTE}/update-brand`,
	UPDATE_BRAND_STATUS: `${BASE_ROUTE}/update-brand-status`,
	DETAIL_BRAND: `${BASE_ROUTE}/detail-brand/:brandID`,
	LIST_BRAND: `${BASE_ROUTE}/list-brand`,
	LIST_BRAND_ENDUSER: `${BASE_ROUTE}/list-brand-enduser`,
	INFO_DETAIL_BRAND: `${BASE_ROUTE}/info-detail-brand/:brandID`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_BRAND = CF_ROUTINGS_BRAND;
