
"use strict";

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                          = require('../../../routing/child_routing');
const roles                                = require('../../../config/cf_role');
const { CF_ROUTINGS_QRCODE }               = require('../constants');
const QRCODE_MODEL                         = require('../models/qrcode').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ********** ================================
             * ========================== QUẢN LÝ S3 ================================
             * ========================== ********** ================================
             */

            [CF_ROUTINGS_QRCODE.ADD_QRCODE]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { text } = req.query;
                        let data = await QRCODE_MODEL.insert({ text });

                        res.json(data);
                    }]
                }
            },
        }
    }
};
