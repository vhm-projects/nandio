"use strict";

/**
 * EXTERNAL PACKAGE
 */
const QRCode   = require('qrcode');

/**
 * BASE
 */
const BaseModel 						 = require('../../../models/intalize/base_model');

/**
 * MODELS
 */
const { UPLOAD_FILE_S3 }                 = require('../../upload-s3/utils');
const { IMAGE_MODEL }                    = require('../../image');

/**
 * COLLECTIONS
 */
const QRCODE_COLL                		 = require('../databases/qrcode-coll');


class Model extends BaseModel {
    constructor() {
        super(QRCODE_COLL);
    }

	insert({ id, key, userID }) {
        return new Promise(async resolve => {
            try {
                const generateQR = await QRCode.toDataURL(id.toString());
               
                let uploadFileS3 = await UPLOAD_FILE_S3( generateQR, key );
                let path = `${uploadFileS3.data.Location}`
                let infoImageAfterInsert;
                if ( generateQR ){
                    infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                        name: key,
                        path: path,
                        userCreate: userID
                    });
                }

                let infoAfterInsert = await this.insertData({ product: id, image: infoImageAfterInsert.data._id })
                
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	createQRCodeCustomer({ customerID, key }){
		return new Promise(async resolve => {
			try {
				let generateQR 	 = await QRCode.toDataURL(customerID.toString());
				let uploadFileS3 = await UPLOAD_FILE_S3(generateQR, key);

				let infoImageAfterInsert = null;
                if (generateQR){
                    infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                        name: key,
                        path: uploadFileS3.data.Location
                    });
                }

				let infoAfterInsert = await this.insertData({ 
					customer: customerID, 
					image: infoImageAfterInsert && infoImageAfterInsert.data._id
				})
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert_qrcode' });

                return resolve({ error: false, data: infoAfterInsert });
			} catch (error) {
                return resolve({ error: true, message: error.message });
			}
		})
	}

}

exports.MODEL = new Model;
