"use strict";

const Schema    	= require('mongoose').Schema;
const BASE_COLL  	= require('../../../database/intalize/base-coll');

/**
 * COLLECTION QRCODE CỦA HỆ THỐNG
 */
module.exports = BASE_COLL("qr_code", {
    /**
     * sản phẩm 
     */
    product: {
        type: Schema.Types.ObjectId,
        ref: 'product'
    },
	/**
     * Khách hàng
     */
	 customer: {
        type: Schema.Types.ObjectId,
        ref: 'customer'
    },
    // Hình ảnh qrcode
    image: {
        type: Schema.Types.ObjectId,
        ref: 'image'
    },
    /**
     * Trạng thái hoạt động.
     * 1. Hoạt động
     * 0. Khóa
     */
    status: {
        type: Number,
        default: 1
    },
    userCreate: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
});
