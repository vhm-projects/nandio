"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID 	= require('mongoose').Types.ObjectId;
const moment  	= require("moment");

/**
 * INTERNAL PACKAGE
 */
const { randomStringUpperCaseAndNumber } 	= require('../../../utils/string_utils');
const { vnp_TmnCode, vnp_HashSecret, vnp_Url,
    vnp_ReturnUrl }                         = require('../constant/transaction.uri')
const { checkObjectIDs, sortObject }		= require('../../../utils/utils');

let dateFormat                              = require('dateformat');
let querystring                             = require('qs');
let sha256                                  = require('sha256');
const { networkInterfaces }                 = require('os');

/**
 * LẤY IP ADDRESS
 */
 const nets = networkInterfaces();
 let IPAddress = ''; // Or just '{}', an empty object
 
 for (const name of Object.keys(nets)) {
	 for (const net of nets[name]) {
		 // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
		 if (net.family === 'IPv4' && !net.internal) {
            if (!IPAddress) {
				//  IPAddress[name] = [];
                IPAddress = '';

            }
            IPAddress = net.address;
            // IPAddress[name].push(net.address);
		 }
	 }
 }

/**
 * BASE
 */
const BaseModel 							= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const TRANSACTION_COLL                		= require('../databases/transaction-coll');
const ORDER_COLL                		    = require('../../order/databases/order-coll');
const ORDER_LINE_COLL                	    = require('../../order/databases/order_line-coll');

const HISTORY_POINT_MODEL                	= require('../../history_point/models/history_point').MODEL;
// const HISTORY_POINT_COLL                 	= require('../../history_point/databases/history_point-coll');
const PRODUCT_POINT_COLL                    = require('../../product_point/databases/product_point-coll');
const PRODUCT_COLL                          = require('../../product/databases/product-coll');
const CUSTOMER_COLL                         = require('../../customer/databases/customer-coll');
const CENTRALIZE_ORDER_MODEL                = require('../../order/models/centralize_order').MODEL;
const NOTIFICATION_MODEL                    = require('../../notification/models/notification').MODEL;

class Model extends BaseModel {
    constructor() {
        super(TRANSACTION_COLL);
        this.STATUS_PENDING_PAYMENT = 0;
        this.STATUS_SUCCESS_PAYMENT = 1;
        this.STATUS_FAIL_PAYMENT    = 2;
    }

    checkTransactionIDExists(transactionID){
		return new Promise(resolve => {
			(async function recursiveChecktranTactionID(transactionID){
				let checkExists = await TRANSACTION_COLL.findOne({ transactionID });
				if(checkExists){
					transactionID = Math.floor(Math.random() * 1000000000);
					recursiveChecktranTactionID(transactionID);
				} else{
					resolve(transactionID);
				}
			})(transactionID)
		})
	}

	insert({ 
        orderID, customerID, orderInfo, bankCode, orderType 
        // ipAddr, 
    }){
		return new Promise(async resolve => {
			try {
				if (!ObjectID.isValid(orderID) || !ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoOrder = await ORDER_COLL.findById(orderID)
                    .populate({
                        path: 'orderLines',
                        select: 'product',
                        populate: {
                            path: 'product',
                            select: 'idSQL',
                        }
                    });
                
				// if(!orderInfo || !amount)
				// 	return resolve({ error: true, message: 'order_info_or_amount_is_required' });

                // let { orderLine } = infoOrder;
                // let listOrderLine = await ORDER_LINE_COLL.find({ _id: { $in: orderLine }}).populate("product").lean()
                // let point = 0;
                // listOrderLine.forEach(oderLine => {
                //     point += oderLine.product.point;
                // });
                if (!infoOrder) {
                    return resolve({ error: true, message: "Đơn hàng không tồn tại" });
                }
                let checkExist__Customer = await CUSTOMER_COLL.findById(customerID);
                if (!checkExist__Customer) {
                    return resolve({ error: true, message: 'Khách hàng không tồn tại' });
                }
                let { amount, orderLines } = infoOrder;
                console.log({
                    checkExist__Customer,
                    infoOrder,
                    amount,
                    IPAddress
                });
                // return
				const transactionID = await this.checkTransactionIDExists(Math.floor(Math.random() * 1000000000));
				let infoAfterAdd = await this.insertData({
					transactionID,
					order: orderID, 
					customer: customerID, 
					orderInfo, 
					amount, 
					bankCode, 
					locale: 'vn',
					orderType, 
					ipAddr: IPAddress,
				});

				if(!infoAfterAdd)
                    return resolve({ error: true, message: "cannot_add_transaction" });

                if(orderLines && orderLines.length){
                    let listOrderLineID = orderLines.map(item => item._id);
                    console.log({
                        listOrderLineID
                    });
                    let listOrderLineAfterUpdate = await ORDER_LINE_COLL.updateMany({ _id: { $in: listOrderLineID } }, {
                        $set: { status: 1 }
                    }, { new: true });
                    console.log({
                        listOrderLineAfterUpdate
                    });
                }

                const SHIP_COD = 2;
                if (infoOrder.payment == SHIP_COD) {
                    console.log("----------------SHIP_COD----------------");
                    let infoCentralizeOrder = await CENTRALIZE_ORDER_MODEL.getCentralizeOrderByType({ 
                        type: orderID
                    });
                    console.log({
                        infoCentralizeOrder
                    });
                    console.log("----------------CREATE_NOTIFICATION_TYPE_ORDER----------------");
                    let title = 'Bạn đã tạo đơn hàng thành công';
                    const TYPE_ORDER_NOTIFICATION = 1;
                    let infoNotificationAfterInsert = await NOTIFICATION_MODEL.insertV2({ 
                    	listNotification: [{
                    		title:       title, 
                    		description: `Đơn hàng đã được tạo, Vui lòng chờ xác nhận`, 
                    		type:        TYPE_ORDER_NOTIFICATION, 
                    		receive:     customerID,
                    		centralize_order: infoCentralizeOrder.data._id,
                    		createAt:    new Date(),
                    		modifyAt:    new Date(),
                    	}]
                    });
                }

                return resolve({ error: false, data: infoAfterAdd });
			} catch (error) {
				return resolve({ error: true, messsage: error.message });
			}
		})
	}

    updateStatus({ transactionID, status }) {
        return new Promise(async resolve => {
            try {
                if (!transactionID)
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await TRANSACTION_COLL.findOneAndUpdate({ transactionID }, {
					$set: { status, modifyAt: Date.now() }
				});

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_update_status_transaction" });
                
                // Ghi lịch sử tích điểm
                if (status == this.STATUS_SUCCESS_PAYMENT) {
                    let { order, customer } = infoAfterUpdate;
                    console.log({
                        order, customer
                    });
                    let infoOrder = await ORDER_COLL.findById(order)
                        .populate({
                            path: 'orderLines',
                            select: 'product',
                            populate: {
                                path: 'product',
                                select: 'idSQL',
                            }
                        });
                        
                    if (!infoOrder) {
                        return resolve({ error: true, message: "Đơn hàng không tồn tại" });
                    }

                    let checkExist__Customer = await CUSTOMER_COLL.findById(customer);
                    if (!checkExist__Customer) {
                        return resolve({ error: true, message: 'Khách hàng không tồn tại' });
                    }
                    
                    const typeTichDiem  = 1; 
                    const buyOnlineType = 2;
                    let point = 0;
                    if(infoOrder.orderLines && infoOrder.orderLines.length){
                        for (let orderLine of infoOrder.orderLines) {
                            let infoProductPoint = await PRODUCT_POINT_COLL.findOne({ product_id: orderLine.product.idSQL }).select('point');
                            if (infoProductPoint) {
                                point += (infoProductPoint.point && !Number.isNaN(Number(infoProductPoint.point))) ? Number(infoProductPoint.point) : 0;
                            }
                        }
                    }
                    
                    let conditionObjHistoryPoint = {
                        beforPoint: 0,
                        type: typeTichDiem,
                        transactionID: transactionID,
                        customer: customer,
                        createAt: new Date(),
                        modifyAt: new Date(),
                    }
    
                    // let infoLastRecordHistoryPoint = await HISTORY_POINT_COLL.findOne({
                    //     customer: customer
                    // }).sort({ createAt: -1 });
                    
                    // if (infoLastRecordHistoryPoint) {
                    //     conditionObjHistoryPoint = {
                    //         ...conditionObjHistoryPoint,
                    //         beforPoint: infoLastRecordHistoryPoint.point
                    //     }
                    // }
                    
                    // conditionObjHistoryPoint = {
                    //     ...conditionObjHistoryPoint,
                    //     currentPoint: point,
                    //     point: conditionObjHistoryPoint.beforPoint + point,
                    //     flatform: 'SHOP'
                    // }
                    // console.log({
                    //     conditionObjHistoryPoint
                    // });
                    // let infoHistoryPointAfterInsert = await HISTORY_POINT_COLL.create(conditionObjHistoryPoint);
                    // if (!infoHistoryPointAfterInsert) {
                    //     return resolve({ error: true, message: 'Không thể tạo lịch sử điểm' });
                    // }

                    // INSERT HISTORY POINT 
                    // let infoHistoryPointAfterInsert = await HISTORY_POINT_MODEL.insert({ 
                    //     customerID: customer, 
                    //     type: HISTORY_POINT_MODEL.TYPE_ACCUMULATE_POINT, 
                    //     currentPoint: point 
                    // });

                    // const infoAfterInsert = await HISTORY_POINT_MODEL.insert({ 
                    //     customerID, transactionID: infoAfterAdd._id, type: typeTichDiem, buyType: buyOnlineType
                    // });
                    // console.log({
                    //     infoHistoryPointAfterInsert
                    // });
    
                    /**
                     * ===================================================================
                     *  UPDATE LẠI POINT CỦA KHÁCH HÀNG
                     * ===================================================================
                     */
                    // let infoEmployeeAfterUpdatePoint = await CUSTOMER_COLL.findByIdAndUpdate(customer, {
                    //     pointRanking: checkExist__Customer.pointRanking + point, // CỘNG VÀO POINT TỔNG
                    //     point: checkExist__Customer.point + point, // CỘNG VÀO POINT SỬ DỤNG
                    // });
                    // console.log({
                    //     infoEmployeeAfterUpdatePoint
                    // });

                    let infoCentralizeOrder = await CENTRALIZE_ORDER_MODEL.getCentralizeOrderByType({ 
                        type: order
                    });
                    console.log({
                        infoCentralizeOrder
                    });
                    let title = 'Bạn đã tạo đơn hàng thành công';
                    const TYPE_ORDER_NOTIFICATION = 1;
                    let infoNotificationAfterInsert = await NOTIFICATION_MODEL.insertV2({ 
                    	listNotification: [{
                    		title:       title, 
                    		description: `Đơn hàng đã được tạo, Vui lòng chờ xác nhận`, 
                    		type:        TYPE_ORDER_NOTIFICATION, 
                    		receive:     customer,
                    		centralize_order: infoCentralizeOrder.data._id,
                    		createAt:    new Date(),
                    		modifyAt:    new Date(),
                    	}]
                    });
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ transactionID }) {
        return new Promise(async resolve => {
            try {
                if (!transactionID)
                    return resolve({ error: true, message: 'params_invalid' });

                let infoTransaction = await TRANSACTION_COLL
					.findOne({ transactionID })
					.populate('order customer')
					.lean();

                if(!infoTransaction)
                    return resolve({ error: true, message: "cannot_get_info_transaction" });

                return resolve({ error: false, data: infoTransaction });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByCustomer({ customerID, status, page = 1, limit = 30 }) {
        return new Promise(async resolve => {
            try {
				if (!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });

				let conditionObj = { customer: customerID };
				if(!isNaN(status)){
					conditionObj.status = status;
				}

				limit = +limit;
				page  = +page;

				let listTransactionOfCustomer = await TRANSACTION_COLL
					.find(conditionObj)
					.populate('order customer')
					.sort({ modifyAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
					.lean();

                if(!listTransactionOfCustomer) 
                    return resolve({ error: true, message: "cannot_get_list_transaction_of_customer" });

				let totalTransaction = await TRANSACTION_COLL.countDocuments(conditionObj);

                return resolve({ 
					error: false, 
					data: {
						listTransactionOfCustomer,
						currentPage: page,
						perPage: limit,
						total: totalTransaction
					} 
				});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListByFilterWithPaging({ 
		 keyword, status, customerID, bankCode, fromDate, toDate, fromPrice, toPrice, page = 1, limit = 30 
	}) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {};
				limit = +limit;
				page  = +page;

                if(keyword){
					let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
					key = new RegExp(key, 'i');

					conditionObj.$or = [
						{ 'order.orderID': key },
						{ 'customer.customerID': key },
						{ 'customer.fullname': key },
						{ 'transactionID': key },
						{ 'bankCode': key },
					]
				}

				if(fromPrice){
					conditionObj['order.amount'] = {
						$gte: +fromPrice
					}
				}

				if(toPrice){
					conditionObj['order.amount'] = {
						$lt: +toPrice
					}
				}

				if(fromPrice && toPrice){
					conditionObj['order.amount'] = { 
						$gte: +fromPrice, 
						$lt: +toPrice
					}
				}

				if(fromDate && toDate){
					conditionObj.createAt = {
                        $gte: new Date(fromDate),
                        $lt: new Date(toDate)
                    }
				}

				customerID 	&& (conditionObj['customer._id'] = ObjectID(customerID));
				bankCode 	&& (conditionObj.bankCode 		 = bankCode);
				status 		&& (conditionObj.status 		 = +status);

				let pipeline = [
					{
						$lookup: {
						   from: 'orders',
						   localField: 'order',
						   foreignField: '_id',
						   as: 'order'
						 }
					},
					{
						$lookup: {
						   from: 'customers',
						   localField: 'customer',
						   foreignField: '_id',
						   as: 'customer'
						 }
					},
					{
						$match: conditionObj
					},
					{ 
						$unwind: {
							path: '$order',
							preserveNullAndEmptyArrays: true
						}
					},
					{ 
						$unwind: {
							path: '$customer',
							preserveNullAndEmptyArrays: true
						}
					}
				];

				let listTransaction = await TRANSACTION_COLL.aggregate([
					...pipeline,
					{ $limit: limit },
					{ $skip: (page - 1) * limit },
					{ $sort: { modifyAt: -1 } }
				])

                if(!listTransaction)
                    return resolve({ error: true, message: "cannot_get_list_transaction" });

				let totalTransaction = await TRANSACTION_COLL.aggregate([
					...pipeline,
					{ $count: 'total' },
				]);

                return resolve({ 
					error: false, 
					data: {
						listTransaction,
						currentPage: page,
						perPage: limit,
						total: totalTransaction.length && totalTransaction[0].total
					}
				});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListByFilter({ 
        keyword, status, customerID, bankCode, fromDate, toDate, fromPrice, toPrice, page = 1, limit = 30
    }) {
	   return new Promise(async resolve => {
		   try {
			   let conditionObj = {};

			   if(keyword){
				   let key = keyword.split(" ");
				   key = '.*' + key.join(".*") + '.*';
				   key = new RegExp(key, 'i');

				   conditionObj.$or = [
					   { 'order.orderID': key },
					   { 'customer.customerID': key },
					   { 'customer.fullname': key },
					   { 'transactionID': key },
					   { 'bankCode': key },
				   ]
			   }

			   if(fromPrice){
				   conditionObj['order.amount'] = {
					   $gte: +fromPrice
				   }
			   }

			   if(toPrice){
				   conditionObj['order.amount'] = {
					   $lt: +toPrice
				   }
			   }

			   if(fromPrice && toPrice){
				   conditionObj['order.amount'] = { 
					   $gte: +fromPrice, 
					   $lt: +toPrice
				   }
			   }

			   if(fromDate && toDate){
				   conditionObj.createAt = {
					   $gte: new Date(fromDate),
					   $lt: new Date(toDate)
				   }
			   }

			   customerID 	&& (conditionObj['customer._id'] = ObjectID(customerID));
			   bankCode 	&& (conditionObj.bankCode 		 = bankCode);
			   status 		&& (conditionObj.status 		 = +status);

			   let listTransaction = await TRANSACTION_COLL.aggregate([
					{
						$lookup: {
							from: 'orders',
							localField: 'order',
							foreignField: '_id',
							as: 'order'
						}
					},
					{
						$lookup: {
							from: 'customers',
							localField: 'customer',
							foreignField: '_id',
							as: 'customer'
						}
					},
					{
						$match: conditionObj
					},
					{ 
						$unwind: {
							path: '$order',
							preserveNullAndEmptyArrays: true
						}
					},
					{ 
						$unwind: {
							path: '$customer',
							preserveNullAndEmptyArrays: true
						}
					},
					{ $sort: { modifyAt: -1 } },
                    // { $limit: limit },
					// { $skip: (page - 1) * limit },
			   ])

			   if(!listTransaction)
				   return resolve({ error: true, message: "cannot_get_list_transaction" });

			   return resolve({ error: false, data: listTransaction });
		   } catch (error) {
			   return resolve({ error: true, message: error.message });
		   }
	   })
   }

    // ------------------EXTERNAL: VNPAY--------------//
    // STEP 1: tạo url payemnt (window và href chuyển sang cổng thanh toán)
    createURLPayment({ orderDescription, bankCode, language, ipAddr, customerID, orderID }) {
        return new Promise(async resolve => {
            try {
                

                let tmnCode         = vnp_TmnCode;
                let secretKey       = vnp_HashSecret;
                let vnpUrl          = vnp_Url;
                let returnUrl       = vnp_ReturnUrl;
                let date            = new Date();

                let createDate      = dateFormat(date, 'yyyymmddHHmmss');
                // let orderId         = dateFormat(date, 'HHmmss');
                // let amount       = req.body.amount;
                // let bankCode     = req.body.bankCode;
                let orderInfo       = orderDescription || ' ';
                let orderType       = 'billpayment';
                let locale          = language || 'vn';

                let currCode        = 'VND';
                let vnp_Params      = {};
                vnp_Params['vnp_Version']   = '2';
                vnp_Params['vnp_Command']   = 'pay';
                vnp_Params['vnp_TmnCode']   = tmnCode;
                // vnp_Params['vnp_Merchant'] = ''
                vnp_Params['vnp_Locale']    = locale;
                vnp_Params['vnp_CurrCode']  = currCode;

                 /**
                 * DB TRANSACTION
                 */
                // let USER_FAKE_ID = '5e60f58cf63b8a4e196a1942';
                // let infoTransactionAfterInsert = await PAYMENT__TRANSACTION_MODEL.insert({ orderID: orderId, orderInfo, amount, bankCode, orderType, locale, userID: USER_FAKE_ID, ipAddr });
                let infoTransactionAfterInsert = await this.insert({ 
                    orderID, orderInfo, locale, orderType, ipAddr, customerID
                });
                console.log({
                    infoTransactionAfterInsert
                });
                // vnp_Params['vnp_TxnRef']    = orderID;
                vnp_Params['vnp_TxnRef']    = infoTransactionAfterInsert.data.transactionID;
                vnp_Params['vnp_OrderInfo'] = orderInfo;
                vnp_Params['vnp_OrderType'] = orderType;
                vnp_Params['vnp_Amount']    = infoTransactionAfterInsert.data.amount * 100;
                vnp_Params['vnp_ReturnUrl'] = returnUrl;
                vnp_Params['vnp_IpAddr']    = ipAddr;
                vnp_Params['vnp_CreateDate'] = createDate;
                console.log({
                    vnp_Params
                });
                if(bankCode !== null && bankCode !== '')
                    vnp_Params['vnp_BankCode'] = bankCode;
                vnp_Params                  = sortObject(vnp_Params);
                let signData                = secretKey + querystring.stringify(vnp_Params, { encode: false });
                let secureHash              = sha256(signData);
              
                vnp_Params['vnp_SecureHashType']    =  'SHA256';
                vnp_Params['vnp_SecureHash']        = secureHash;
                vnpUrl += '?' + querystring.stringify(vnp_Params, { encode: true });
                console.log({ vnpUrl })
                
                return resolve({
                    error: false, 
                    data: {
                        url: vnpUrl,
                        infoTransaction: infoTransactionAfterInsert
                    }
                });
            } catch (error) {
                resolve({ error: true, message: error.message })
            }
        })
    }

    // STEP 2.1: url trả về từ browser nếu thanh toán thành công/thất bại
    //http://localhost:5000/api/payment/transactions/url_return
        //?vnp_Amount=5000000&vnp_BankCode=NCB&vnp_BankTranNo=20210617005343&vnp_CardType=ATM&
        //vnp_OrderInfo=Thanh+toan+don+hang+thoi+gian%3A+2021-06-16+23%3A06%3A04&vnp_PayDate=20210617005335&vnp_ResponseCode=00&
        //vnp_TmnCode=1SNJ89L8&vnp_TransactionNo=13525727&vnp_TransactionStatus=00&vnp_TxnRef=000611&vnp_SecureHashType=SHA256&
        //vnp_SecureHash=e917a754f6347b44a43ec94c8c94ee4c2fa1a2397fe711884a828f7493f92234 
    URL_RETURN({ params }) {
        return new Promise(async resolve => {
            try {
                let vnp_Params          = params;
                let secureHash          = vnp_Params['vnp_SecureHash'];
                let transactionStatus   = vnp_Params['vnp_ResponseCode'];

                delete vnp_Params['vnp_SecureHash'];
                delete vnp_Params['vnp_SecureHashType'];
                let transactionID   = vnp_Params['vnp_TxnRef'];
                let rspCode         = vnp_Params['vnp_ResponseCode'];
                vnp_Params          = sortObject(vnp_Params);

                let tmnCode         = vnp_TmnCode;
                let secretKey       = vnp_HashSecret;

                let signData        = secretKey + querystring.stringify(vnp_Params, { encode: false });
                let checkSum        = sha256(signData); 

                const VNPAY_TRANSACTION_WITH_STATUS_SUCCESS = '00';
                console.log({
                    transactionStatus
                });
                if(secureHash === checkSum && transactionStatus == VNPAY_TRANSACTION_WITH_STATUS_SUCCESS){
                    //Kiem tra xem du lieu trong db co hop le hay khong va thong bao ket qua

                    let infoTransactionAfterUpdateStatus = await this.updateStatus({ transactionID, status: this.STATUS_SUCCESS_PAYMENT });
                    // let infoTransactionAfterUpdateStatus = await TRANSACTION_COLL.findOneAndUpdate({
                    //     transactionID,
                    // }, { 
                    //     status: this.STATUS_SUCCESS_PAYMENT,
                    //     modifyAt: Date.now()
                    // }, { new: true });

                    if (!infoTransactionAfterUpdateStatus.error)
                        return resolve(infoTransactionAfterUpdateStatus)

                    return resolve({
                        error: false, 
                        data: {
                            code: transactionStatus,
                            transactionID, rspCode,
                            infoTransaction: infoTransactionAfterUpdateStatus.data
                        }
                    })
                } else{
                    let infoTransactionAfterUpdateStatus = await TRANSACTION_COLL.findOneAndUpdate({
                        transactionID,
                    }, { 
                        status: this.STATUS_FAIL_PAYMENT,
                        modifyAt: Date.now()
                    }, { new: true });

                    let { order, customer } = infoTransactionAfterUpdateStatus;
                    console.log({
                        order, customer
                    });
                    const CANCEL = 8;
                    let infoOrder = await ORDER_COLL.findByIdAndUpdate(order, {
                        status: CANCEL
                    }, {
                        new: true,
                        modifyAt: Date.now()
                    });
                    
                    console.log({
                        infoOrder
                    });

                    let { orderLines }     = infoOrder;
                    const NOT_CREATE_ORDER = 0;
                    let listOrderLineAfterUpdate = await ORDER_LINE_COLL.updateMany({
                        _id: {
                            $in: orderLines
                        }
                    }, {
                        status: NOT_CREATE_ORDER
                    });
                    console.log({
                        listOrderLineAfterUpdate
                    });
                    let infoCentralizeOrder = await CENTRALIZE_ORDER_MODEL.updateByType({
                        type: order,
                        status: CANCEL
                    });
                    console.log({
                        infoCentralizeOrder
                    });
                    return resolve({ 
                        error:  true, message: 'signature_invalid', code: transactionStatus,
                        infoTransaction: infoTransactionAfterUpdateStatus
                    });
                }
            } catch (error) {
                return resolve({ error: true, messsage: error.message })
            }
        })
    }

















	
    /**
     * Top 5 cửa hàng có doanh thu cao nhất
     * Dattv
     */
    //  topAgencyHaveBestSale({ limitNumber, start, end }) {
    //     return new Promise(async resolve => {
    //         try {

    //             let condition = {};

    //             if(start && end){
    //                 let _fromDate   = moment(start).startOf('day').format();
    //                 let _toDate     = moment(end).endOf('day').format();
    //                 condition.createAt = {
    //                     $gte: new Date(_fromDate),
    //                     $lt: new Date(_toDate)
    //                 }
    //             }
                
    //             let listTopAgencyBestSale = await TRANSACTION_COLL.aggregate([
    //                 {
    //                     $match: condition
    //                 },
    //                 {
    //                     $group : { 
    //                         _id : "$agency",
    //                         count: { $sum: 1 },
    //                         total: { $sum: "$totalMoneyNumber" },
    //                         agency : { $first: '$agency' }
    //                     },
    //                 },
    //                 {
    //                     $lookup: {
    //                         from: "agencies",
    //                         localField: "agency",
    //                         foreignField: "_id",
    //                         as: "agency"
    //                     }
    //                 },
    //                 {
    //                     $unwind: "$agency"
    //                 }
    //             ]).sort({ total: -1 }).limit(limitNumber);

    //             if(!listTopAgencyBestSale)
    //                 return resolve({ error: true, message: "cannot_get_info" });
    //             return resolve({ error: false, data: listTopAgencyBestSale });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }

    // /**
    //  * Top 5 cửa hàng có khách hàng cao nhất
    //  * Dattv
    //  */
    //  topAgencyHaveMostCustomer({ limitNumber, start, end }) {
    //     return new Promise(async resolve => {
    //         try {

    //             let condition = {};

    //             if(start && end){
    //                 let _fromDate   = moment(start).startOf('day').format();
    //                 let _toDate     = moment(end).endOf('day').format();
    //                 condition.createAt = {
    //                     $gte: new Date(_fromDate),
    //                     $lt: new Date(_toDate)
    //                 }
    //             }

    //             let listTopAgencyHaveMostCustomer = await CUSTOMER_COLL.aggregate([
    //                 {
    //                     $match: condition
    //                 },
    //                 {
    //                     $group : { 
    //                         _id: "$agency",
    //                         count: { $sum: 1 },
    //                     },
                        
    //                 },
    //                 {
    //                     $lookup: {
    //                         from: "agencies",
    //                         localField: "_id",
    //                         foreignField: "_id",
    //                         as: "agency"
    //                     }
    //                 },
    //                 {
    //                     $unwind: "$agency"
    //                 }
    //             ]).sort({ count: -1 }).limit(limitNumber);

    //             if(!listTopAgencyHaveMostCustomer)
    //                 return resolve({ error: true, message: "cannot_get_info" });
    //             return resolve({ error: false, data: listTopAgencyHaveMostCustomer });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }

    // /**
    //  * Top 5 danh mục sản phẩm được giao dịch nhiều nhất trên hệ thống
    //  * Dattv
    //  */
    //  top5ProductCategoryMostSale({ limit, start, end }) {
    //     return new Promise(async resolve => {
    //         try {

    //             let condition = {};

    //             if(start && end){
    //                 let _fromDate   = moment(start).startOf('day').format();
    //                 let _toDate     = moment(end).endOf('day').format();
    //                 condition.createAt = {
    //                     $gte: new Date(_fromDate),
    //                     $lt: new Date(_toDate)
    //                 }
    //             }
                
    //             let listProductCategory = await PRODUCT_COLL.aggregate([
    //                 {
    //                     $match: condition
    //                 },
    //                 {
    //                     $group : { 
    //                         _id : "$type",
    //                         count: { $sum: 1 },
    //                         type : { $first: '$type' }
    //                     },
    //                 },
    //                 {
    //                     $lookup: {
    //                         from: "product_categories",
    //                         localField: "type",
    //                         foreignField: "_id",
    //                         as: "type"
    //                     }
    //                 },
    //                 {
    //                     $unwind: "$type"
    //                 }
    //             ]).sort({ count: -1 }).limit(limit);

    //             if(!listProductCategory)
    //                 return resolve({ error: true, message: "cannot_get_info" });
    //             return resolve({ error: false, data: listProductCategory });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }

    // /**
    //  * Top khách hàng có chi thu cao nhất
    //  * Dattv
    //  */
    //  topCustomer({ start, end }) {
    //     return new Promise(async resolve => {
    //         try {

    //             let condition = {};

    //             if(start && end){
    //                 let _fromDate   = moment(start).startOf('day').format();
    //                 let _toDate     = moment(end).endOf('day').format();
    //                 condition.createAt = {
    //                     $gte: new Date(_fromDate),
    //                     $lt: new Date(_toDate)
    //                 }
    //             }

    //             let listTopCustomer = await TRANSACTION_COLL.aggregate([
    //                 {
    //                     $match: condition
    //                 },
    //                 {
    //                     $group : { 
    //                         _id : "$customer",
    //                         total: { $sum: "$totalMoneyNumber" },
    //                         customer : { $first: '$customer' }
    //                     },
    //                 },
    //                 {
    //                     $lookup: {
    //                         from: "customers",
    //                         localField: "_id",
    //                         foreignField: "_id",
    //                         as: "customer"
    //                     }
    //                 },
    //                 {
    //                     $unwind: "$customer"
    //                 }
    //             ]).sort({ total: -1 }).limit(5);

    //             //console.log({ listTopCustomer });

    //             if(!listTopCustomer)
    //                 return resolve({ error: true, message: "cannot_get_info" });
    //             return resolve({ error: false, data: listTopCustomer });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }

    // /**
    //  * Chart thể hiển loại hàng kinh doanh biến động theo ngày
    //  * Dattv
    //  */
    //  chartProductCategoryByDay({ typeProductCategory, start, end }) {
    //     return new Promise(async resolve => {
    //         try {

    //             let condition = {};

    //             if(start && end){
    //                 let _fromDate   = moment(start).startOf('day').format();
    //                 let _toDate     = moment(end).endOf('day').format();
    //                 condition.createAt = {
    //                     $gte: new Date(_fromDate),
    //                     $lt: new Date(_toDate)
    //                 }
    //             }
                
    //             let listProductCategory = await PRODUCT_COLL.aggregate([
    //                 //{ $match: { date: { $gte: ISODate("2019-05-01") } } },
    //                 {
    //                     $match: { type: ObjectID(typeProductCategory), ...condition }
    //                 },
    //                 {
    //                     $group : { 
    //                         _id: { 
    //                             dateCreate: {$dateToString: { format: "%Y-%m-%d", date: "$createAt"} } 
    //                         },
    //                         count: { $sum: 1 },
    //                         type : { $first: '$type' },
    //                     },
    //                 },
    //                 {
    //                     $lookup: {
    //                         from: "product_categories",
    //                         localField: "type",
    //                         foreignField: "_id",
    //                         as: "type"
    //                     }
    //                 },
    //                 {
    //                     $unwind: "$type"
    //                 }
    //             ]).sort({ _id: 1 })

    //             // console.log({ listProductCategory });
    //             // console.log({ __: listProductCategory[0]._id });

    //             if(!listProductCategory)
    //                 return resolve({ error: true, message: "cannot_get_info" });
    //             return resolve({ error: false, data: listProductCategory });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }

    // /**
    //  * Chart thể hiển loại hàng kinh doanh biến động theo ngày
    //  * Dattv
    //  */
    //  chartProductCategoryByDayV2({ month, start, end }) {
    //     return new Promise(async resolve => {
    //         try {

    //             let condition = {};

    //             if(start && end){
    //                 let _fromDate   = moment(start).startOf('day').format();
    //                 let _toDate     = moment(end).endOf('day').format();
    //                 condition.createAt = {
    //                     $gte: new Date(_fromDate),
    //                     $lt: new Date(_toDate)
    //                 }
    //             }

    //             let result = [];
    //             let listProductCategory = await PRODUCT_CATEGORY_COLL.find({ parent: null }).lean().select('_id name');
    //             if(listProductCategory && listProductCategory.length){
    //                 for (let catetory of listProductCategory) {
                        
    //                     let amountCategoty = await PRODUCT_COLL.aggregate([
    //                         {
    //                             $match: condition
    //                         },
    //                         {
    //                             $project: {
    //                                 name: 1,
    //                                 type: 1, 
    //                                 createAt: 1, 
    //                                 month: {$month: '$createAt'}
    //                             }
    //                         },
    //                         {
    //                             $match: { month: month, type: ObjectID(catetory._id) }
    //                         },
    //                         {
    //                             $group : { 
    //                                 _id: { $dayOfMonth: "$createAt" },
    //                                 name : { $first: '$name' },
    //                                 count: { $sum: 1 },
    //                                 createAt : { $first: '$createAt' }
    //                             },
    //                         },
    //                     ]).sort({ _id: 1, createAt: 1 });
    //                     result.push({ name: catetory.name, data: amountCategoty });
    //                 }
    //             }

    //             if(!result)
    //                 return resolve({ error: true, message: "cannot_get_info" });
    //             return resolve({ error: false, data: result });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }

    // /**
    //  * Chart thể hiển Đại Lý kinh doanh biến động theo ngày
    //  * Dattv
    //  */
    //  chartAgencyByDay({ agencyID }) {
    //     return new Promise(async resolve => {
    //         try {
                
    //             let listAgencyByDay = await TRANSACTION_COLL.aggregate([
    //                 //{ $match: { date: { $gte: ISODate("2019-05-01") } } },
    //                 {
    //                     $match: { agency: ObjectID(agencyID)}
    //                 },
    //                 {
    //                     $group : { 
    //                         _id: { 
    //                             dateCreate: { $dateToString: { format: "%Y-%m-%d", date: "$createAt"} } 
    //                         },
    //                         total: { $sum: "$totalMoneyNumber" },
    //                         agency : { $first: '$agency' },
    //                     },
    //                 },
    //                 {
    //                     $lookup: {
    //                         from: "agencies",
    //                         localField: "agency",
    //                         foreignField: "_id",
    //                         as: "agency"
    //                     }
    //                 },
    //                 {
    //                     $unwind: "$agency"
    //                 }
    //             ]).sort({ _id: 1 })

    //             // console.log({ listAgencyByDay });
    //             // console.log({ __: listAgencyByDay[0]._id });

    //             if(!listAgencyByDay)
    //                 return resolve({ error: true, message: "cannot_get_info" });
    //             return resolve({ error: false, data: listAgencyByDay });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }

    // /**
    //  * Chart thể hiển Đại Lý kinh doanh biến động theo ngày
    //  * Dattv
    //  */
    //  chartFollowAgencySaleDaily({ month, start, end }) {
    //     return new Promise(async resolve => {
    //         try {

    //             let condition = {};

    //             if(start && end){
    //                 let _fromDate   = moment(start).startOf('day').format();
    //                 let _toDate     = moment(end).endOf('day').format();
    //                 condition.createAt = {
    //                     $gte: new Date(_fromDate),
    //                     $lt: new Date(_toDate)
    //                 }
    //             }
                
    //             let listTransactionEveryDateInMonth = await TRANSACTION_COLL.aggregate([
    //                 {
    //                     $project: {
    //                         _id: 1, 
    //                         totalMoneyNumber: 1, 
    //                         createAt: 1, 
    //                         month: {$month: '$createAt'}}},
    //                 {
    //                     $match: { month: month, ...condition }
    //                 },
    //                 {
    //                     $group : { 
    //                         _id: { $dayOfMonth: "$createAt" },
    //                         count: { $sum: 1 },
    //                         totalMoney: { $sum: "$totalMoneyNumber" },
    //                         createAt : { $first: '$createAt' }
    //                     },
    //                 },
    //             ]).sort({ createAt: 1 })

    //             if(!listTransactionEveryDateInMonth)
    //                 return resolve({ error: true, message: "cannot_get_info" });
    //             return resolve({ error: false, data: listTransactionEveryDateInMonth });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }

    // /**
    //  * Danh sách giao dịch của khách hàng
    //  */
    //  getListByCustomer({ customerID, dayFirst, dayLast, status, limit = 10, page = 1, typeTransaction, fromDay, toDay }) {
    //     return new Promise(async resolve => {
    //         try {
    //             if (!ObjectID.isValid(customerID))
    //                 return resolve({ error: true, message: 'params_invalid' });

    //             let data = {
    //                 customer: customerID
    //             }

    //             let arrTransactionID = [];
    //             let dataFindDateMainTrans = {};
    //             let dataFindDate = {};
    //             // console.log({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay  });
    //             let start;
    //             let end;
    //             if ( fromDay && !toDay ){
    //                 start = new Date(fromDay);
    //                 start.setHours(0,0,0,0);
    //                 // dataFindDateMainTrans.interestStartDate = { $gte: start };
    //                 dataFindDate.createAt = { $gte: start }
    //             }
    //             if ( !fromDay && toDay ){
    //                 end = new Date(toDay);
    //                 end.setHours(23,59,59,999);
    //                 // dataFindDateMainTrans.interestStartDate = { $lte: end };
    //                 dataFindDate.createAt = { $lte: end }
    //             }
    //             if ( fromDay && toDay ){
    //                 start = new Date(fromDay);
    //                 start.setHours(0,0,0,0);
    //                 end = new Date(toDay);
    //                 end.setHours(23,59,59,999);
    //                 // dataFindDateMainTrans.interestStartDate = {     
    //                 //     $gte: start, 
    //                 //     $lte:  end 
    //                 // };
    //                 dataFindDate.createAt = {     
    //                     $gte: start, 
    //                     $lte:  end }
    //             }
    //             if ( typeTransaction ){
    //                 if ( typeTransaction == 1){
    //                     if( dataFindDate.createAt ){
    //                         dataFilter.interestStartDate = dataFindDate.createAt;
    //                     }
    //                     dataFilter.latestInjectTransaction = undefined;
    //                 }else{
    //                     dataFindDate.agency = agencyID;
    //                     dataFindDate.type   = typeTransaction;
    //                     if ( ransom ){
    //                         dataFindDate.ransom = ransom
    //                     }
    //                     let listInjectTransaction = await INJECT_TRANSACTION_COLL.find(dataFindDate);
    //                     // console.log({ listInjectTransaction });
    //                     arrTransactionID = listInjectTransaction.map( item => item._id );
    //                     arrTransactionID = [...new Set(arrTransactionID)];
    //                     // console.log({ arrTransactionID });
    //                     dataFilter.latestInjectTransaction = {
    //                         $in: arrTransactionID
    //                     }
    //                 }
    //             }else{
    //                 if( dataFindDate.createAt ){
    //                     dataFilter.interestStartDate = dataFindDate.createAt;
    //                 }
    //                 // dataFilter.latestInjectTransaction = undefined;
    //             }

    //             if(status) {
    //                 data.status = status
    //             }

    //             if(dayFirst && dayLast) {
    //                 data.transactionDate = {
    //                     $gte: dayFirst, 
    //                     $lte: dayLast 
    //                 }
    //             }
    //             let infoData = await TRANSACTION_COLL.find(data)
    //                 // .limit(limit * 1)
    //                 // .skip((page - 1) * limit)
    //                 .populate({
    //                     path: "agency latestInjectTransaction customerImages KYCImages receiptImages formVerificationImages"
    //                 })
    //                 .populate({
    //                     path : "injectTransaction",
    //                     populate: {
    //                         path: "customerImages KYCImages receiptImages formVerificationImages",
    //                         select: "_id name path"
    //                     }
    //                 })
    //                 .populate({
    //                     path : "products",
    //                     select: "_id name price",
    //                     populate: {
    //                         path: "type",
    //                         select: "_id name"
    //                     }
    //                 })
    //                 .populate({
    //                     path : "qrCode",
    //                     select: "_id image",
    //                     populate: {
    //                         path: "image",
    //                         select: "_id path"
    //                     }
    //                 })
    //                 .sort({ interestStartDate: -1 })
    //             ;
    //             if(!infoData)
    //                 return resolve({ error: true, message: "cannot_get_list" });
    //             return resolve({ error: false, data: infoData });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }

    // // lấy danh sách hết hạn, đến hạn, thanh lý
    // getListByCustomerIDCheckDate({ customerID, typeTransaction, typeStatusTransaction, fromDay, toDay, ransom, status }) {
    //     return new Promise(async resolve => {
    //         try {
    //             if (!ObjectID.isValid(customerID))
    //                 return resolve({ error: true, message: 'params_invalid' });

    //             let dataFilter  = {
    //                 '$match': {
    //                   'customer': new ObjectID(customerID)
    //                 },
    //                 '$project': {}
    //             };
                
    //             let arrTransactionID = [];
    //             let dataFindDateMainTrans = {};
    //             let dataFindDate = {};
    //             // console.log({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay  });
    //             let start;
    //             let end;

    //             // let nowDate = new Date();
    //             // subStractDate({ date1: nowDate,  })

    //             if ( fromDay && !toDay ){
    //                 start = new Date(fromDay);
    //                 start.setHours(0,0,0,0);
    //                 // dataFindDateMainTrans.interestStartDate = { $gte: start };
    //                 dataFindDate.createAt = { $gte: start }
    //             }
    //             if ( !fromDay && toDay ){
    //                 end = new Date(toDay);
    //                 end.setHours(23,59,59,999);
    //                 // dataFindDateMainTrans.interestStartDate = { $lte: end };
    //                 dataFindDate.createAt = { $lte: end }
    //             }
    //             if ( fromDay && toDay ){
    //                 start = new Date(fromDay);
    //                 start.setHours(0,0,0,0);
    //                 end = new Date(toDay);
    //                 end.setHours(23,59,59,999);
    //                 // dataFindDateMainTrans.interestStartDate = {     
    //                 //     $gte: start, 
    //                 //     $lte:  end 
    //                 // };
    //                 dataFindDate.createAt = {     
    //                     $gte: start, 
    //                     $lte:  end }
    //             }
    //             if ( typeTransaction ){
    //                 if ( typeTransaction == 1){
    //                     if( dataFindDate.createAt ){
    //                         // dataFilter.createAt = dataFindDate.createAt;
    //                         dataFilter['$match'].interestStartDate = dataFindDate.createAt;
    //                     }
    //                     dataFilter['$match'].latestInjectTransaction = undefined;
    //                 }else{
    //                     dataFindDate.agency = agencyID;
    //                     dataFindDate.type   = typeTransaction;
    //                     if ( ransom ){
    //                         dataFindDate.ransom = ransom
    //                     }
    //                     let listInjectTransaction = await INJECT_TRANSACTION_COLL.find(dataFindDate);
    //                     // console.log({ listInjectTransaction });
    //                     arrTransactionID = listInjectTransaction.map( item => ObjectID(item._id));
    //                     arrTransactionID = ["$latestInjectTransaction", [...new Set(arrTransactionID)]];
    //                     dataFilter['$project'].checkLastInjectTransaction = {
    //                         $in: arrTransactionID
    //                     }
    //                 }
    //             }else{
    //                 if( dataFindDate.createAt ){
    //                     dataFilter['$match'].interestStartDate = dataFindDate.createAt;
    //                 }
    //             }

    //             // if(typeStatusTransaction ){
    //             //     dataFilter['$match'].status = Number(typeStatusTransaction);
    //             // }
    //             if ( status ){
    //                 dataFilter['$match'].status = Number(status);
    //             }
    //             dataFilter['$project'] = {
    //                 ...dataFilter['$project'],
    //                 'customer': 1, 
    //                 'transactionDate': 1, 
    //                 'expectedDate': 1, 
    //                 'approvalLimit': 1, 
    //                 'approvalNote': 1, 
    //                 'code': 1, 
    //                 'loanAmount': 1, 
    //                 'code': 1, 
    //                 'status': 1, 
    //                 'expireTime': 1, 
    //                 'agency': 1, 
    //                 'latestInjectTransaction': 1,
    //                 'injectTransaction': 1,
    //                 'typeInventory': 1,
    //                 'createAt': 1,
    //                 'interestStartDate': 1, 
    //                 'dateDifference': {
    //                     '$divide': [
    //                     {
    //                         '$subtract': [
    //                             '$expireTime', new Date()
    //                         ]
    //                     }, 1000 * 60 * 60 * 24
    //                     ]
    //                 }
    //             }
    //             // console.log({ dataFilter });
    //             // console.log({latestInjectTransaction:  dataFilter['$project'].latestInjectTransaction });
    //             let listTransaction = await TRANSACTION_COLL.aggregate([
    //                 {
    //                     '$match':{
    //                         ...dataFilter['$match']
    //                     }
    //                 },
    //                 {
    //                     '$project':{
    //                         ...dataFilter['$project']
    //                     }
    //                 },
    //                 {
    //                   '$lookup': {
    //                     'from': 'customers', 
    //                     'localField': 'customer', 
    //                     'foreignField': '_id', 
    //                     'as': 'customer'
    //                   }
    //                 }
    //                 // , {
    //                 //   '$project': {
    //                 //     'customer': 1, 
    //                 //     'loanAmount': 1, 
    //                 //     'code': 1, 
    //                 //     'status': 1, 
    //                 //     'expireTime': 1, 
    //                 //     'interestStartDate': 1, 
    //                 //     'latestInjectTransaction': 1, 
    //                 //     'dateDifference': {
    //                 //       '$divide': [
    //                 //         {
    //                 //           '$subtract': [
    //                 //             '$expireTime', new Date()
    //                 //           ]
    //                 //         }, 1000 * 60 * 60 * 24
    //                 //       ]
    //                 //     }
    //                 //   }
    //                 // }
    //                 , {
    //                   '$lookup': {
    //                     'from': 'inject_transactions', 
    //                     'localField': 'latestInjectTransaction', 
    //                     'foreignField': '_id', 
    //                     'as': 'latestInjectTransaction'
    //                   }
    //                 }, {
    //                     '$lookup': {
    //                       'from': 'agencies', 
    //                       'localField': 'agency', 
    //                       'foreignField': '_id', 
    //                       'as': 'agency'
    //                     }
    //                   }
    //             ])

    //             if(!listTransaction)
    //                 return resolve({ error: true, message: "cannot_get_list" });
    //             return resolve({ error: false, data: listTransaction });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }



}

exports.MODEL = new Model;
