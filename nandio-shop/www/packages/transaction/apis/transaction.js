"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_TRANSACTION }                   = require('../constant/transaction.uri');

/**
 * MODELS
 */
const TRANSACTION_MODEL                             = require('../models/transaction').MODEL;

/**
 * COLLECTIONS
 */
const { CUSTOMER_COLL } = require('../../customer');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() { 
        return {
            /**
             * ========================== ************************ ================================
             * ==========================   QUẢN LÝ GIAO DỊCH     ================================
             * ========================== ************************ ================================
             */

			/**
             * Function: Add Transaction (permission: customer) (API)
			 * Date: 03/07/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_TRANSACTION.ADD_TRANSACTION]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
                        let { _id: customerID } = req.customer;
                        const { 
							orderID, 
                            // orderInfo, amount, bankCode, locale, orderType, ipAddr
						} = req.body;

                        const infoAfterInsert = await TRANSACTION_MODEL.insert({
							orderID, customerID
                            // orderInfo, amount, bankCode, locale, orderType, ipAddr, customerID 
						});
                        res.json(infoAfterInsert)
                    }]
                },
            },

			/**
             * Function: Update Status Transaction (permission: customer) (API)
			 * Date: 03/07/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_TRANSACTION.UPDATE_STATUS_TRANSACTION]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
                        const { transactionID, status } = req.body;
                        console.log({
                            transactionID, status, TRANSACTION_MODEL
                        });
                        const infoAfterUpdate = await TRANSACTION_MODEL.updateStatus({ transactionID, status });
                        res.json(infoAfterUpdate)
                    }]
                },
            },

            /**
             * Function: Info Transaction (permission: customer) (API)
			 * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_TRANSACTION.INFO_TRANSACTION]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
                        const { transactionID } = req.query;

                        const infoTransaction = await TRANSACTION_MODEL.getInfo({ transactionID });
                        res.json(infoTransaction)
                    }]
                },
            },

			/**
             * Function: List Transaction By Customer (permission: customer) (API)
			 * Date: 03/07/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_TRANSACTION.API_LIST_TRANSACTION_BY_CUSTOMER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
                        let { _id: customerID } = req.customer;
                        const { status, page, limit } = req.query;

						const listTransactionByCustomer = await TRANSACTION_MODEL.getListByCustomer({
							customerID, status, page, limit
						});
						res.json(listTransactionByCustomer);
					}]
				}
			},

			/**
             * Function: List Transaction (permission: customer) (API)
			 * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_TRANSACTION.API_LIST_TRANSACTION_FILTER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
						const { 
							keyword, status, customerID, bankCode, 
							fromDate, toDate, fromPrice, toPrice, page, limit 
						} = req.query;

						const listTransactionFilter = await TRANSACTION_MODEL.getListByFilterWithPaging({
							keyword, status, customerID, bankCode, 
							fromDate, toDate, fromPrice, toPrice, page, limit
						});
						res.json(listTransactionFilter);
					}]
				}
			},

			/**
             * Function: List Transaction Filtered (permission: admin) (VIEW)
			 * Date: 08/07/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_TRANSACTION.LIST_TRANSACTION_FILTER]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    view: 'index.ejs',
					title: 'List Transaction - NANDIO',
					code: CF_ROUTINGS_TRANSACTION.LIST_TRANSACTION_FILTER,
					inc: path.resolve(__dirname, '../views/list_transaction.ejs')
                },
                methods: {
                    get: [ async (req, res) => {
                        const { 
							keyword, status, customerID, bankCode, 
							fromDate, toDate, fromPrice, toPrice, limit, page
						} = req.query;

						const listTransactionFilter = await TRANSACTION_MODEL.getListByFilter({
							keyword, status, customerID, bankCode, 
							fromDate, toDate, fromPrice, toPrice, limit, page
						});

						if(listTransactionFilter.error){
							return ChildRouter.renderToView(req, res, {
								listTransaction: [],
								currentPage: 0,
								perPage: 0,
								total: 0
							});
						}

						ChildRouter.renderToView(req, res, {
							listTransaction: listTransactionFilter.data || [],
						})
					}]
				}
			},

            [CF_ROUTINGS_TRANSACTION.WEBHOOK_URL_PAYMENT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
                        let params = req.query;
                        let infoAfter = await TRANSACTION_MODEL.URL_RETURN({
                            params
                        })
                       res.json(infoAfter)
					}]
				}
			},

            [CF_ROUTINGS_TRANSACTION.CREATE_URL_PAYMENT]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
                        let { _id: customerID } = req.customer;

                        let { 
                            orderID, 
                            // customerID, //faker
                        } = req.body;
                        console.log({  orderID, customerID })
                        let ipAddr = req.headers['x-forwarded-for'] 	||
                                        req.connection.remoteAddress 	||
                                        req.socket.remoteAddress 		||
                                        req.connection.socket.remoteAddress;;
                        
                        let infoAfter = await TRANSACTION_MODEL.createURLPayment({
                            customerID, orderID, ipAddr
                        })
                        res.json(infoAfter)
					}]
				}
			},

        }
    }
};
