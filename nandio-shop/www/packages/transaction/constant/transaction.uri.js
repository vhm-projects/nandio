const BASE_ROUTE = '/transaction';

const CF_ROUTINGS_TRANSACTION = {
	ADD_TRANSACTION: `${BASE_ROUTE}/add-transaction`,
    UPDATE_STATUS_TRANSACTION: `${BASE_ROUTE}/update-status-transaction`,
	INFO_TRANSACTION: `${BASE_ROUTE}/info-transaction`,
    
	API_LIST_TRANSACTION_FILTER: `/api${BASE_ROUTE}/list-transaction-filter`,
    API_LIST_TRANSACTION_BY_CUSTOMER: `/api${BASE_ROUTE}/list-transaction-by-customer`,

	LIST_TRANSACTION_FILTER: `${BASE_ROUTE}/list-transaction`,

	CREATE_URL_PAYMENT: `${BASE_ROUTE}/create-url-payment`,
	WEBHOOK_URL_PAYMENT: `${BASE_ROUTE}/webhook-payment-redirect-vnpay`,

	ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_TRANSACTION = CF_ROUTINGS_TRANSACTION;
//file chứa các thông tin cung cấp từ cổng thanh toán (VNPAY)
exports.vnp_TmnCode             = process.env.PAYMENT__VNP_TMNCODE      || 'P1Y1P5Y0';
exports.vnp_HashSecret          = process.env.PAYMENT__VNP_HASHSECRET   || 'KAEBKSKNHRSEORHCEVMUQOPYTTFBDMRA';
exports.vnp_Url                 = process.env.PAYMENT__VNP_URL          || 'http://sandbox.vnpayment.vn/paymentv2/vpcpay.html';
exports.vnp_ReturnUrl           = process.env.PAYMENT__VNP_RETURN_URL   || 'http://3df4-2001-ee0-5274-b4b0-50fd-51d4-9e7e-274d.ngrok.io/api/payment/transactions/url_return';