const TRANSACTION_MODEL 		= require('./models/transaction').MODEL;
const TRANSACTION_COLL  		= require('./databases/transaction-coll');
const TRANSACTION_ROUTES        = require('./apis/transaction');

module.exports = {
    TRANSACTION_ROUTES,
    TRANSACTION_COLL,
    TRANSACTION_MODEL,
}
