"use strict";

const Schema     = require('mongoose').Schema;
const BASE_COLl  = require('../../../database/intalize/base-coll');

/**
 * COLLECTION GIAO DỊCH CỦA HỆ THỐNG
 */
module.exports = BASE_COLl('transaction', {
	transactionID: {
		type: String,
		required: true,
	},
	// Nội dung thanh toán
	orderInfo: {
		type: String,
		trim: true
	},
	// Số tiền thanh toán
	amount: {
		type: Number,
		required: true,
	},
	bankCode: String, // (tuỳ chọn)
	orderType: String, // mã loại hàng hoá thanh toán (tuỳ chọn)
	locale: {
		type: String,
		enum: {
			values: ['vn', 'en'],
			message: '{VALUE} is not supported'
		}
	},
	ipAddr: {
		type: String,
		default: ''
	},
	order: {
		type: Schema.Types.ObjectId,
		ref: 'order'
	},
	customer: {
		type: Schema.Types.ObjectId,
		ref: 'customer'
	},
	/**
	 * 0. chưa thanh toán
	 * 1. đã thanh toán -> //* thành công
	 * 2. đã thanh toán -> //* thất bại -> những statuscode từ vnpay
	 */
	status: {
		type: Number,
		default: 0
	}
});
