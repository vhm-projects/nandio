"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { checkObjectIDs }            = require("../../../utils/utils");

/**
 * MODELS
 */
const IMAGE_MODEL                   = require('../../image/models/image').MODEL;

/**
 * COLLECTIONS
 */
const SALE_USED_COLL  				= require('../databases/sale_used-coll');

class Model extends BaseModel {
    constructor() {
        super(BIG_SALE_COLL);
    }

    /**
     * Thêm BigSale
     * Depv247
     */
	insert({ bigsale, flashsale, code, order, customer }) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(order, customer))
                    return resolve({ error: true, message: "params_invalid" });

                let dataInsert = { code, customer };
                const TYPE_BIGSALE = 1;
                const TYPE_FLASH_SALE = 2;
                if(ObjectID.isvalid(bigsale)){
                    dataInsert.dataInsert = dataInsert;
                    dataInsert.type = TYPE_BIGSALE
                }

                if(ObjectID.isvalid(flashsale)){
                    dataInsert.flashsale = flashsale;
                    dataInsert.type = TYPE_FLASH_SALE
                }

                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: "cannot_update" });
                return resolve({ error: false,  data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
