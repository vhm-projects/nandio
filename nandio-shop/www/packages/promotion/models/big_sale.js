"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                        = require("moment");
/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { checkObjectIDs }            = require("../../../utils/utils");
const { DESCRIPTION_BIG_SALE_COLL}  = require("../../../config/cf_constants");
const { subDate, addDate }                   =  require("../../../../www/utils/time_utils")

/**
 * MODELS
 */
const IMAGE_MODEL                   = require('../../image/models/image').MODEL;

/**
 * COLLECTIONS
 */
const BIG_SALE_COLL  				= require('../databases/big_sale-coll');
const SALE_USED_COLL  				= require('../databases/sale_used-coll');
const PRODUCT_COLL  				= require('../../../packages/product/databases/product-coll');

class Model extends BaseModel {
    constructor() {
        super(BIG_SALE_COLL);
        this.STAGE_ACTIVE = 1
        this.STAGE_INACTIVE = 2
    }

    /**
     * Kiểm tra xem sản phẩm có tồn tại trong bigsale đang hoạt động khác không
     * Depv247
     */
    checkProductIsExistInBigsaleOther({ products, bigSaleID }) {
        return new Promise(async resolve => {
            try {
                // Điều kiện bigsale đang hoạt động
                let condition = {
                    status: 1,
                    stage: this.STAGE_ACTIVE,
                    endTime: {
                        $gte: new Date()
                    }
                }

                if(bigSaleID){
                    condition._id = { $ne: bigSaleID }
                }

                // Check product exist in bigsale
                let listBigSaleActive = await BIG_SALE_COLL.find(condition).select("products");
                let productExist = [];
                listBigSaleActive.forEach(bigsale =>{
                    bigsale.products.forEach(product => {
                        productExist.push(product);
                    })
                });

                let listProductExist = []
                for (const product of productExist) {
                    for (const _product of products) {
                        if(product.toString() == _product.toString()){
                            listProductExist.push(_product);
                        }
                    }
                }
                // Danh sách sản phẩm đã tồn tại trong bigsale
                if(listProductExist.length){
                    let listProductExistMessage = ""
                    for (const product of listProductExist) {
                        let infoProduct = await PRODUCT_COLL.findById(product);
                        listProductExistMessage += `${infoProduct.name}, `
                    }
                    return resolve({ error: true, message: `Sản phẩm ${listProductExistMessage} Đã tồn tại trong 1 bigsale khác`});
                }
                return resolve({ error: false, message: "pass"});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Thêm BigSale
     * Depv247
     */
	insert({ name, description, content, image, typeDiscount, code, maxUsage, products, amountDiscount, status, userID, gallery, endTime }) {
        return new Promise(async resolve => {
            try {
                let typeDiscountByMoney = 1;
                let typeDiscountByPercent = 2;
                let typeDiscountAccept = [ typeDiscountByMoney, typeDiscountByPercent ];
                if(!name || !code || !products || !typeDiscount || !typeDiscountAccept.includes(Number(typeDiscount)))
                    return resolve({ error: true, message: 'params_invalid', description: "name, code, products, typeDiscount => là những trường bắt bộc phải truyền" });

                let isExistCode = await BIG_SALE_COLL.findOne({ code });
                if(isExistCode)
                    return resolve({ error: true, message: 'code_existed' });

                // Kiểm tra sản phẩm có tồn tại trong bigsale khác đang hoạt động không
                let checkProducts = await this.checkProductIsExistInBigsaleOther({ products });
                if(checkProducts.error){
                    return resolve(checkProducts);
                }

                let dataInsert = { name, code, typeDiscount };
                if(description){
                    dataInsert.description = description;
                }

                if(content){
                    dataInsert.content = content;
                }

                if(typeDiscount == typeDiscountByMoney && amountDiscount){
                    amountDiscount = Number(amountDiscount.split(',').join(''));
                    // dataInsert.amountDiscountByMoney = amountDiscount;
                    dataInsert.value = amountDiscount;
                }

                if(typeDiscount == typeDiscountByPercent && amountDiscount){
                    amountDiscount = Number(amountDiscount.split(',').join(''));
                    // dataInsert.amountDiscountByPercent = amountDiscount;
                    dataInsert.value = amountDiscount;
                }

                if(maxUsage){
                    maxUsage = Number(maxUsage.split(',').join(''));
                    dataInsert.maxUsage = maxUsage;
                }

                if(checkObjectIDs(products)){
                    dataInsert.products = products;
                }

                if(ObjectID.isValid(userID)){
                    dataInsert.userCreate = userID;
                }

                let statusValid = [0, 1];
                if(!statusValid.includes(Number(status)))
                    return resolve({ error: true, message: 'status_invalid' });

                dataInsert.status = Number(status);
                if(endTime){
                    dataInsert.endTime = endTime;
                }

                if(image && image.length > 0) {
                    let { name, size, path } = image[0];
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path, userCreate: userID
                    });

                    dataInsert.image = infoImageAfterInsert.data._id;
                }

                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_big_sale_failed' });

                let arrGallery = [];
                if(gallery && gallery.length > 0) {
                    for(let imgGallery of gallery) {
                        let { name, size, path } = imgGallery;
                        let infoImageGalleryAfterInsert = await IMAGE_MODEL.insert({
                            name, size, path, userCreate: userID
                        });
                        arrGallery.push(infoImageGalleryAfterInsert.data._id);
                    }

                    if(checkObjectIDs(arrGallery)){                    
                        let infoBigSaleUpdate = await BIG_SALE_COLL.findByIdAndUpdate({ _id: infoAfterInsert._id }, {
                            $set: { gallerys: arrGallery }
                        }, { new: true });
                        if(!infoBigSaleUpdate)
                            return resolve({ error: true, message: 'cannot_update_gallery_big_sale' });
                    }
                }

                return resolve({ error: false, data: infoAfterInsert, description: DESCRIPTION_BIG_SALE_COLL });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ bigSaleID, name, description, content, image, typeDiscount, code, maxUsage, products, 
    amountDiscount, status, linkDiscounts, userID, galleryUpdate, imgGalleryOldIsDeleted, endTime }) {
        return new Promise(async resolve => {
            try {
                let typeDiscountByMoney = 1;
                let typeDiscountByPercent = 2;
                let typeDiscountAccept = [ typeDiscountByMoney, typeDiscountByPercent ];
                if(!ObjectID.isValid(bigSaleID) || !name || !typeDiscount || !typeDiscountAccept.includes(Number(typeDiscount)))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataUpdate = { name, typeDiscount, endTime };
                if(description){
                    dataUpdate.description = description;
                }

                if(content){
                    dataUpdate.content = content;
                }

                if(image && image.length > 0) {
                    let { name, size, path } = image[0];
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path, userCreate: userID
                    });

                    dataUpdate.image = infoImageAfterInsert.data._id;
                }

                if(typeDiscount == typeDiscountByMoney && amountDiscount){
                    amountDiscount = amountDiscount.split(',').join('');
                    // dataUpdate.amountDiscountByMoney = amountDiscount;
                    dataUpdate.value = amountDiscount;
                }

                if(typeDiscount == typeDiscountByPercent && amountDiscount){
                    amountDiscount = amountDiscount.split(',').join('');
                    // dataUpdate.amountDiscountByPercent = amountDiscount;
                    dataUpdate.value = amountDiscount;
                }

                let isExistCode = await BIG_SALE_COLL.findOne({ _id: { $ne: bigSaleID }, code });
                if(isExistCode)
                    return resolve({ error: true, message: 'code_existed' });

                if(code){
                    dataUpdate.code = code;
                }

                if(maxUsage){
                    maxUsage = maxUsage.split(',').join('');
                    dataUpdate.maxUsage = maxUsage;
                }

                if(checkObjectIDs(products)){
                    dataUpdate.products = products;
                }

                // Kiểm tra sản phẩm có tồn tại trong bigsale khác đang hoạt động không
                let checkProducts = await this.checkProductIsExistInBigsaleOther({ products, bigSaleID });
                if(checkProducts.error){
                    return resolve(checkProducts);
                }

                if(status){
                    dataUpdate.status = status;
                }
                
                if(linkDiscounts){
                    dataUpdate.linkDiscounts = linkDiscounts;
                }

                if(ObjectID.isValid(userID)){
                    dataUpdate.userUpdate = userID;
                }

                let infoAfterUpdate = await BIG_SALE_COLL.findByIdAndUpdate(bigSaleID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, data: "update_failed" });

                //Xóa khỏi hình ảnh cũ trong gallery cập nhật
                if(imgGalleryOldIsDeleted && imgGalleryOldIsDeleted.length){
                    for (let image of imgGalleryOldIsDeleted) {
                        infoAfterUpdate = await BIG_SALE_COLL.findByIdAndUpdate(bigSaleID, {
                            $pull: { gallerys: image.id }
                        }, { new: true });
                    }
                }

                //Thêm các hình ảnh mới cập nhật
                if(galleryUpdate && galleryUpdate.length){
                    for(let image of galleryUpdate) {
                        let { size, name, path } = image;
                        let infoImageAfterInsert = await IMAGE_MODEL.insert({
                            size, name, path, userCreate: userID
                        });

                        // sau khi thêm hình ảnh thì thêm nó vào gallers của big sale đó luôn
                        infoAfterUpdate = await BIG_SALE_COLL.findByIdAndUpdate(bigSaleID, {
                            $addToSet: { gallerys: infoImageAfterInsert.data._id }
                        }, { new: true });
                    }
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateStatus({ bigSaleID, status }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(bigSaleID))
                    return resolve({ error: true, message: "params_invalid" });
                let dataUpdate = {}
                if(status){
                    dataUpdate.status = status;
                }
                let infoAfterUpdate = await BIG_SALE_COLL.findByIdAndUpdate(bigSaleID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, data: "update_failed" });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ bigSaleID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(bigSaleID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoBigSale = await BIG_SALE_COLL.findById(bigSaleID)
                    .select("typeDiscount products name content value image code endTime status")
                    .populate({
                        path: "image",
                        select: "path"
                    })
                    .populate({
                        path: "gallerys",
                        select: "path"
                    })
                    .populate({
                        path: "products",
                        select: "name price",
                        populate: {
                            path: "avatar",
                            select: "path"
                        }
                    })
                if(!infoBigSale)
                    return resolve({ error: false, message: 'cannot_get_info_category' });

                return resolve({ error: false, data: infoBigSale });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    searchCode({ code }) {
        return new Promise(async resolve => {
            try {
                let infoBigSale = await BIG_SALE_COLL.findOne({ 
                        code, status: 1,
                        stage: this.STAGE_ACTIVE 
                    })
                    .select("name image code endTime")
                    .populate({
                        path: "image",
                        select: "path"
                    })

                if(!infoBigSale)
                    return resolve({ error: false, message: 'code_invalid' });

                return resolve({ error: false, data: infoBigSale });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListAdmin() {
        return new Promise(async resolve => {
            try {
                /**
                 * B1 lấy danh sách những bigsale còn hạn với điều kiên
                 *  - status = 1
                 *  - stage: 1
                 *  - endTime > currentTime
                 */
                let listBigSaleActive = await BIG_SALE_COLL.find({
                        status: 1,
                        stage: this.STAGE_ACTIVE,
                        endTime: {
                            $gte: new Date()
                        }
                    })
                    .populate({
                        path: "image",
                        select: "path"
                    })
                    .sort({ createAt: -1 });
                 /**
                 * B1 lấy danh sách những bigsale còn lại ngoại trừ những bigsale còn hạn
                 *  - stage: 1
                 *  - _id: { $nin: arrIDBigsaleActive }
                 */                    
                let arrIDBigsaleActive =  listBigSaleActive.map(item => ObjectID(item._id));
                let listBigSaleInActive = await BIG_SALE_COLL.find({
                        stage: this.STAGE_ACTIVE,
                        _id: { $nin: arrIDBigsaleActive }
                    })
                    .populate({
                        path: "image",
                        select: "path"
                    })
                    .sort({ createAt: -1 });

                if(!listBigSaleActive && !listBigSaleInActive)
                    return resolve({ error: true, message: 'cannot_get_list' });

                return resolve({ error: false, data: { listBigSaleActive, listBigSaleInActive }});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    /**
     * Name: Api danh sách các bigsale phù hợp cho khách hàng
     * Author: Depv
     */
    getListForCustomer({ limit = 10, page = 1}) {
        return new Promise(async resolve => {
            try {
                /**
                 * BA
                 * status = 1
                 * còn mã code // todo
                 * còn hạn
                 */
                let statusActive = 1;
                let conditionSearch =  {
                    status: statusActive,
                    stage: this.STAGE_ACTIVE,
                    endTime: {
                        $gte: new Date()
                    }
                }
                let count = await BIG_SALE_COLL.count(conditionSearch);
                let listBigSale = await BIG_SALE_COLL.find(conditionSearch)
                                    .select("name image endTime products")
                                    .populate({
                                        path: "image",
                                        select: "path"
                                    })
                                    .populate({
                                        path: "products",
                                        select: "name price"
                                    })
                                    .sort({ createAt: -1 })
                                    .limit(limit)
                                    .skip((page - 1) * limit);

                if(!listBigSale)
                    return resolve({ error: true, message: 'cannot_get_list' });

                return resolve({ 
                    error: false, 
                    data: listBigSale, 
                    totalPages: Math.ceil(count/limit), 
					totalResult: +count, 
					currentPage: +page
                });

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

     /**
     * Name: Danh sách các bigsale sắp hết hạng(còn lại 2 ngày)
     * Author: Depv
     */
    getListBigSaleByExpiring({ limit=10, page = 1}) {
        return new Promise(async resolve => {
            try {
                /**
                 * BA
                 * status = 1
                 * còn mã code // todo
                 * còn hạn
                 */
                let today = moment(new Date, 'MM-DD-YYYY').endOf('day')._d;
                let statusActive = 1;
                let todayAdd2 = moment(addDate(2), 'MM-DD-YYYY').endOf('day')._d;
                // Endtime lớn hơn thời gian hiện tại, và bé hơn thời gian hiện tại cộng 2ngày
                let conditionSearch = { status: statusActive, endTime: { $gte: today, $lte: todayAdd2 } };

                let count = await BIG_SALE_COLL.count(conditionSearch);
                let listBigSale = await BIG_SALE_COLL.find(conditionSearch)
                                  .populate('image')
                                  .limit(limit)
                                  .skip((page - 1) * limit);

                if(!listBigSale)
                    return resolve({ error: true, message: 'cannot_get_list' });

                return resolve({ 
                    error: false, 
                    data: listBigSale, 
                    totalPages: Math.ceil(count/limit), 
					totalResult: count, 
					currentPage: +page
                });

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    remove({ bigSaleID }) {
        return new Promise(async resolve => {
            try {
                let infoBigSaleRemove = await BIG_SALE_COLL.findByIdAndUpdate(bigSaleID, { stage: this.STAGE_INACTIVE });
                if(!infoBigSaleRemove)
                    return resolve({ error: true, message: 'remove_big_sale_failed' });

                return resolve({ error: false, data: infoBigSaleRemove });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
     /**
     * Name: Danh sách tất cả các sản phẩm của tất cả bigsale và danh sách tất cả các bigsale
     * Author: Depv
     */
    getListProductOfAllBigsale({ limit= 10, page=1}) {
        return new Promise(async resolve => {
            try {
                /**
                 * BA
                 * Lấy danh sách bigsale đang sale
                 */
                let today = moment(new Date()).endOf('day')._d;
                let conditionSearch = {
                    endTime: {
                        $gte: today
                    }
                }
                let listBigSale = await BIG_SALE_COLL.find(conditionSearch).select("typeDiscount products value gallerys")
                    .populate({
                        path: 'gallerys',
                        select: 'path'
                    });
                let listProductID = [];

                let listGallery = [];
                if (listBigSale && listBigSale.length) {
                    listBigSale.map(item => {
                        if (item.gallerys && item.gallerys.length) {
                            listGallery = [
                                ...listGallery,
                                ...item.gallerys.map(image => image.path)
                            ];
                        }
                    });
                }
                                
                listBigSale.forEach(bigsale => {
                    bigsale.products.forEach(productID => {
                        listProductID.push(ObjectID(productID));
                    });
                });

                let listProduct = await PRODUCT_COLL.find({ _id: { $in: listProductID }})
                                .select("name price description")
                                .populate({
                                    path: "avatar",
                                    select: "path"
                                })
                                .skip((Number(page) * Number(limit)) - Number(limit))
                                .limit(Number(limit))
                                .sort({ createAt: -1 });
                if(!listProduct)
                    return resolve({ error: true, message: "cannot_get" });
                let totalItem = await PRODUCT_COLL.count({ _id: { $in: listProductID }})
                let pages = Math.ceil(totalItem/limit);
                return resolve({ error: false, data: { listProduct, listBigSale, listGallery, totalPage:+pages, currentPage:+page } });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }


    getListBigSaleActive({ }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                const STAGE_ACTIVE  = 1;

                let conditionSearch = {
                    status: STATUS_ACTIVE,
                    stage:  STAGE_ACTIVE,
                    endTime: {
                        $gte: new Date()
                    }
                }
                let listBigSale = await BIG_SALE_COLL.find({ ...conditionSearch }, {
                    name: 1, products: 1, value: 1, typeDiscount: 1, 
                });
                
                if(!listBigSale)
                    return resolve({ error: true, message: "cannot_get" });
                return resolve({ error: false, data: listBigSale });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
