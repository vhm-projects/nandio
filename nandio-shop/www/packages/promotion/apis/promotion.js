"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { uploadSingle }         				= require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_PROMOTION  } 			= require('../constants/promotion.uri');

/**
 * MODELS
 */
const BIG_SALE_MODEL    = require('../models/big_sale').MODEL;
const PRODUCT_MODEL     = require('../../product/models/product').MODEL;
const FLASH_SALE_MODEL  = require('../models/flash_sale').MODEL;
const IMAGE_MODEL       = require('../../image/models/image').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ====================== ************************ ================================
             * =====================     QUẢN LÝ KHUYẾN MÃI   ================================
             * ====================== ************************ ================================
             */
            /**
             * Function: Tạo Big Sale
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.ADD_BIG_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Add Big Sale',
                    type: 'view',
                    code: CF_ROUTINGS_PROMOTION.ADD_BIG_SALE,
                    inc: path.resolve(__dirname, '../views/add_big_sale.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get:  [async function (req, res) {
                        let listProduct = await PRODUCT_MODEL.getList({ status: 1 });
                        ChildRouter.renderToView(req, res, { listProduct: listProduct.data });
                    }],
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { name, description, content, typeDiscount, code, maxUsage, products, amountDiscount, status, image, gallery, endTime } = req.body;

                        let infoAfterInsert = await BIG_SALE_MODEL.insert({ name, description, content, image, typeDiscount, code, maxUsage, products, amountDiscount, status, userID, gallery, endTime });
                        res.json(infoAfterInsert);
                    }]
                },
            },

             /**
             * Function: cập nhật Big Sale 
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.UPDATE_BIG_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Update Big Sale',
                    type: 'view',
                    code: CF_ROUTINGS_PROMOTION.UPDATE_BIG_SALE,
                    inc: path.resolve(__dirname, '../views/update_big_sale.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function(req, res) {
                        let { bigSaleID } = req.query;
                        let infoBigSaleUpdate = await BIG_SALE_MODEL.getInfo({ bigSaleID });
                        let listProduct = await PRODUCT_MODEL.getList({ status: 1 });
 
                        ChildRouter.renderToView(req, res, { 
                            infoBigSaleUpdate: infoBigSaleUpdate.data,
                            listProduct: listProduct.data
                        });
					}],
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { bigSaleID } = req.query;
                        let { name, description, typeDiscount, code, maxUsage, products, content, amountDiscount, status, linkDiscounts, pathImage, galleryUpdate, imgGalleryOldIsDeleted, endTime } = req.body;
                        
                        let infoAfterUpdate = await BIG_SALE_MODEL.update({ bigSaleID, name, description, image: pathImage, typeDiscount, code, maxUsage, products, content, amountDiscount, status, linkDiscounts, userID, galleryUpdate, imgGalleryOldIsDeleted, endTime });
                        res.json(infoAfterUpdate);
                    }]
                },
            },
            
             /**
             * Function: Thông tin big sale
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.INFO_BIG_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_PROMOTION.INFO_BIG_SALE,
                    inc: path.resolve(__dirname, '../views/info_detail_big_sale.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        let { bigSaleID } = req.params;
                        let infoBigSale = await BIG_SALE_MODEL.getInfo({ bigSaleID });
                        ChildRouter.renderToView(req, res, { infoBigSale: infoBigSale.data });
                    }]
                },
            },

             /**
             * Function: Danh sách big sale
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.LIST_BIG_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let listBigSale = await BIG_SALE_MODEL.getListAdmin();
                        res.json(listBigSale);
                    }]
                },
            },

            /**
             * Function: List big sale (View)
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.BIG_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Add brand',
                    type: 'view',
                    code: CF_ROUTINGS_PROMOTION.BIG_SALE,
                    inc: path.resolve(__dirname, '../views/list_big_sale.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function(req, res) {
                        let listData = await BIG_SALE_MODEL.getListAdmin();
                        ChildRouter.renderToView(req, res, { listData: listData.data });
                    }],
                },
            },

            /**
             * Function: Remove BigSale
             * Date: 25/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_PROMOTION.REMOVE_BIG_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Remove big sale',
                    type: 'json'
                },
                methods: {
                    get: [ async function(req, res) {
                        let { bigSaleID } = req.params;
                        let infoBigSaleRemove = await BIG_SALE_MODEL.remove({ bigSaleID });
                        res.json(infoBigSaleRemove);
                    }],
                },
            },

            /**
             * Function: list FLASH SALE VIEW
             * Date: 20/06/2021
             * Dev: Depv
             */
             [CF_ROUTINGS_PROMOTION.FLASH_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Add brand',
                    type: 'view',
                    code: CF_ROUTINGS_PROMOTION.FLASH_SALE,
                    inc: path.resolve(__dirname, '../views/list_flash_sale.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function(req, res) {
                        let listData = await FLASH_SALE_MODEL.getListAdmin();
                        ChildRouter.renderToView(req, res, { listData: listData.data });
                    }],
                },
            },

            /**
             * Function: Tạo FLASH Sale
             * Date: 20/06/2021
             * Dev: Depv247
             */
             [CF_ROUTINGS_PROMOTION.ADD_FLASH_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Add Flash Sale',
                    type: 'view',
                    code: CF_ROUTINGS_PROMOTION.ADD_FLASH_SALE,
                    inc: path.resolve(__dirname, '../views/add_flash_sale.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get:  [async function (req, res) {
                        let listProduct = await PRODUCT_MODEL.getList({ status: 1 });
                        ChildRouter.renderToView(req, res, { listProduct: listProduct.data });
                    }],
                    post: [ uploadSingle, async function (req, res) {
                        let { _id: userID } = req.user;
                        let { name, description, content, pathImage, pathGallerys, typeDiscount, code, products, amountDiscount, startTime, endTime, status } = JSON.parse(req.body.data);
                        let image = null;
                        if(req.file) {
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                size,
                                name: req.filename,
                                path: pathImage[0]
                            });
                            image = infoImageAfterInsert.data._id;
                        }

                        let infoAfterInsert = await FLASH_SALE_MODEL.insert({ name, description, content, image, pathGallerys, typeDiscount, code, products, amountDiscount, startTime, endTime, status, userID });
                        res.json(infoAfterInsert);
                    }]
                },
            },

             /**
             * Function: cập nhật FLASH Sale
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.UPDATE_FLASH_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Update Flash Sale',
                    type: 'view',
                    code: CF_ROUTINGS_PROMOTION.UPDATE_FLASH_SALE,
                    inc: path.resolve(__dirname, '../views/update_flash_sale.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function(req, res) {
                        let { flashSaleID } = req.params;
                        let infoFlashSaleUpdate = await FLASH_SALE_MODEL.getInfo({ flashSaleID });
                        let listProduct = await PRODUCT_MODEL.getList({ status: 1 });

                        ChildRouter.renderToView(req, res, { 
                            infoFlashSaleUpdate: infoFlashSaleUpdate.data,
                            listProduct: listProduct.data
                        });
					}],
                    post: [ uploadSingle, async function (req, res) {
                        let { _id: userID } = req.user;
                        let { flashSaleID } = req.params;
                        let { name, description, content, pathImage, pathGallerysUpdate, imgGalleryOldIsDeleted, typeDiscount, products, amountDiscount, status, linkDiscounts, startTime, endTime } = JSON.parse(req.body.data);
                        
                        let image = null;
                        if(req.file) {
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({
                                size,
                                name: req.fileName,
                                path: pathImage[0]
                            });
                            image = infoImageAfterInsert.data._id;
                        }
                        
                        let infoAfterUpdate = await FLASH_SALE_MODEL.update({ flashSaleID, name, description, content, image, typeDiscount, products, amountDiscount, status, linkDiscounts, startTime, endTime, userID, pathGallerysUpdate, imgGalleryOldIsDeleted });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Remove FlashSale
             * Date: 25/06/2021
             * Dev: VyPQ
             */
             [CF_ROUTINGS_PROMOTION.REMOVE_FLASH_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Remove flash sale',
                    type: 'json'
                },
                methods: {
                    get: [ async function(req, res) {
                        let { flashSaleID } = req.params;
                        let infoFlashSaleRemove = await FLASH_SALE_MODEL.remove({ flashSaleID });
                        res.json(infoFlashSaleRemove);
                    }],
                },
            },
            
             /**
             * Function: Thông tin FLASH sale
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.INFO_FLASH_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_PROMOTION.INFO_FLASH_SALE,
                    inc: path.resolve(__dirname, '../views/info_detail_flash_sale.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        let { flashSaleID } = req.params;
                        let infoFlashSale = await FLASH_SALE_MODEL.getInfo({ flashSaleID });
                        ChildRouter.renderToView(req, res, { infoFlashSale: infoFlashSale.data });
                    }]
                },
            },
            /**
             * Function:Api cho thông tin flash sale
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.INFO_FLASH_SALE_ENDUSER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let infoFlashSale = await FLASH_SALE_MODEL.getInfoFlashSaleForEnduser();
                        res.json(infoFlashSale);
                    }]
                },
            },

            /**
             * Function: Danh sách flash sale
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.LIST_FLASH_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let listBigSale = await FLASH_SALE_MODEL.getListAdmin();
                        res.json(listBigSale);
                    }]
                },
            },

            [CF_ROUTINGS_PROMOTION.API_LIST_FLASH_SALE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let listFlashSale = await FLASH_SALE_MODEL.getListFlashSaleActive({ });
                        console.log({
                            listFlashSale
                        });
                        res.json(listFlashSale);
                    }]
                },
            },

            //======================API MOBILE=====================
            
            /**
             * Function: Danh sách bigSale còn hạn sử dụng
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.API_LIST_BIG_SALE]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { limit, page } = req.query;
                        let listBigSale = await BIG_SALE_MODEL.getListForCustomer({ limit: +limit, page: +page });
                        res.json(listBigSale);
                    }]
                },
            },

            /**
             * Function: Danh sách bigSale sắp đến hạn còn 2 ngày
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.API_LIST_BIG_SALE_EXPIRING]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { limit, page } = req.query;
                        let listBigSale = await BIG_SALE_MODEL.getListBigSaleByExpiring({ limit: +limit, page: +page });
                        res.json(listBigSale);
                    }]
                },
            },

             /**
             * Function: Api thông tin big sale
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.API_INFO_BIG_SALE]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { bigSaleID } = req.params;
                        let infoBigSale = await BIG_SALE_MODEL.getInfo({ bigSaleID });
                        res.json(infoBigSale);
                    }]
                },
            },

            /**
             * Function: Api tìm kiếm thông tin mã ưu đãi(Bigsale)
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.API_SEARCH_CODE]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { code } = req.params;
                        let infoBigSale = await BIG_SALE_MODEL.searchCode({ code });
                        res.json(infoBigSale);
                    }]
                },
            },

              /**
             * Function: Api thông tin big sale
             * Date: 20/06/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_PROMOTION.LIST_PRODUCT_OF_ALL_BIG_SALE]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { limit, page } = req.query;
                        let infoBigSale = await BIG_SALE_MODEL.getListProductOfAllBigsale({ limit, page });
                        res.json(infoBigSale);
                    }]
                },
            },
        }
    }
};
