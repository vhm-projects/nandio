"use strict";

const Schema    = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION BIG SALE CỦA HỆ THỐNG
 * DESIGN BY DEPV247
 */
module.exports = BASE_COLL('big_sale', {
	name: {
		type: String,
		trim: true,
	},
    description: {
		type: String,
		trim: true
	},
	//_________Nội dung khuyến mãi
	content: {
		type: String,
		trim: true
	},
	//_________Hình ảnh khuyến mãi
	image: {
		type: Schema.Types.ObjectId,
		ref: "image"
	},

	/**
	 * Loại giảm giá
	 * 1: Giảm giá theo tiền
	 * 2: Giảm giá theo %
	 */
	typeDiscount: {
		type: Number,
		default: 1
	},
	//_________Code khuyến mãi 
	code: {
		type: String,
		trim: true,
		unique : true
	},
	//_________Số lượng giao dịch tối đa sử dụng khuyến mãi này
	maxUsage: {
		type: Number,
		default: 0
	},
	//_________Khuyến mãi áp dụng cho sản phẩm nào
	products: [{
		type: Schema.Types.ObjectId,
		ref: 'product'
	}],
	//_________Giảm giá bằng tiền: VD 30.000 vnd(Bỏ thay bằng value)
	amountDiscountByMoney: {
		type: Number
	},

	//_________Giảm giá bằng tiền: VD 50%(Bỏ thay bằng value)
	amountDiscountByPercent: {
		type: Number
	},
	
	//_________Thay cho amountDiscountByMoney và amountDiscountByPercent
	value: {
		type: String
	},
	/**
	 * Trạng thái khuyến mãi
	 * 0: Hết hạn
	 * 1: Còn hạn
	 */
	status: {
		type: Number,
		default: 0
	},
	/**
	 * Trạng thái khuyến mãi
	 * 1: Hoạt động
	 * 2: Đã xóa
	 */
	stage: {
		type: Number,
		default: 1
	},
	//_________Thời gian kết thúc khuyến mãi
	endTime: {
		type: Date,
	},     
	//_________Link tới các trang thương mại điện tử khác
	linkDiscounts: [{
		type: Number
	}],
	/**
	 * Mảng hình ảnh của Big sale
	 */
	gallerys: [{
		type: Schema.Types.ObjectId,
		ref: "image"
	}],
	userCreate: {
		type: Schema.Types.ObjectId,
		ref: "user"
	},
	userUpdate: {
		type: Schema.Types.ObjectId,
		ref: "user"
	},
});
