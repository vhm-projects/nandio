"use strict";

const Schema    = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION KHÁCH HÀNG ĐÃ SỬ DỤNG CHƯƠNG TRÌNH KHUYẾN MÃI NÀO
 * DESIGN BY DEPV247
 */
module.exports = BASE_COLL('sale_used', {
	/**
	 * Type = 1 bigsale
	 * Type = 2 flashsale
 	 */
	type: {
		type: Number,
	},
	bigsale: {
		type: Schema.Types.ObjectId,
		ref: "big_sale"
	},
	flashsale: {
		type: Schema.Types.ObjectId,
		ref: "flash_sale"
	},
	// Code của bigsale hoặc flashsale
	code: {
		type: String,
	},
	order: {
		type: Schema.Types.ObjectId,
		ref: "order"
	},
	customer: {
		type: Schema.Types.ObjectId,
		ref: "customer"
	}
});
