const BASE_ROUTE = '/product-point';
const BASE_ROUTE_API = '/api/product-point';

const CF_ROUTINGS_PRODUCT_POINT = {
	API_UPDATE_PRODUCT_POINT:        `${BASE_ROUTE_API}/update-product-point`,
	API_INFO_PRODUCT_POINT:        `${BASE_ROUTE_API}/info-product-point`,
    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_PRODUCT_POINT = CF_ROUTINGS_PRODUCT_POINT;
