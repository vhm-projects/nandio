"use strict";
const path = require('path');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { CF_ROUTINGS_PRODUCT_POINT } 				= require('../constants/product_point.uri');

/**
 * MODELS
 */
const PRODUCT_POINT_MODEL        = require('../models/product_point').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ==========================      QUẢN LÝ PRODUCT     ================================
             * ========================== ************************ ================================
             */
       
            [CF_ROUTINGS_PRODUCT_POINT.API_UPDATE_PRODUCT_POINT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { product_id, point }   = req.body;
                        let infoProductPoint = await PRODUCT_POINT_MODEL.update({ product_id, point });
                        return res.json(infoProductPoint)
                    }]
                },
            },

            [CF_ROUTINGS_PRODUCT_POINT.API_INFO_PRODUCT_POINT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { product_id }   = req.body;
                        let infoProductPoint = await PRODUCT_POINT_MODEL.getInfo({ product_id });
                        return res.json(infoProductPoint)
                    }]
                },
            },
        }
    }
};
