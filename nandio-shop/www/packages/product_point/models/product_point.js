"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const request                       = require('request');

/**
 * INTERNAL PACKAGES
 */
const cfJWS                         = require('../../../config/cf_jws');

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const {checkObjectIDs} 				= require('../../../utils/utils');
/**
 * COLLECTIONS
 */
const PRODUCT_POINT_COLL  				= require('../databases/product_point-coll');
const HISTORY_SYNC_MODEL  				= require('../../history_sync/models/history_sync').MODEL;
const PRODUCT_COLL  				    = require('../../product/databases/product-coll');

class Model extends BaseModel {
    constructor() {
        super(PRODUCT_POINT_COLL);
    }

    update({ product_id, point }) {
        return new Promise(async resolve => {
            try {
                if(!product_id)
                    return resolve({ error: true, message: 'id_invalid' });
                if(isNaN(point))
                    return resolve({ error: true, message: 'point_invalid' });
                let infoAfterUpdate = await PRODUCT_POINT_COLL.findOneAndUpdate({ product_id }, { point }, { upsert: true  });

                let infoAfterUpdatePointProduct = await PRODUCT_COLL.findOneAndUpdate({ idSQL: product_id }, { point }, {new: true });
                console.log({ infoAfterUpdatePointProduct, infoAfterUpdate });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'create_insert' });
                
                // ĐỒNG BỘ DỮ LIỆU BÊN DMS
                var options = {
                    'method': 'POST',
                    'url': `http://dms1.yensaothienviet.vn:8680/thsams/web/index.php?r=api%2Fcauhinhdiemsp&p1=crm&p2=123456&p3=4PBagn81jWU3ouPVt4OeqzDeYrU7iYH9&p4=1&product_id=${product_id}&sl=1&diem=${point}`,
                };
                
                request(options, async function (error, response) {
                    if (error) throw new Error(error);
                    console.log({ response })
                    // if(response.statusCode != 200){
                    //     console.log("Đồng bộ dữ liệu lỗi");
                    //     let infoDataAsyncError = await HISTORY_SYNC_MODEL.insert({ type: 3, product_id, point })
                    // }
                });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    getInfo({ product_id }) {
        return new Promise(async resolve => {
            try {
                // if(!product_id)
                //     return resolve({ error: true, message: 'id_invalid' });
                let infoData = await PRODUCT_POINT_COLL.findOne({ product_id  });
                if(!infoData)
                    return resolve({ error: true, message: 'cannot_get_info' });

                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }
}

exports.MODEL = new Model;
