"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');
/**
 * Điểm sản phẩm
 */
module.exports = BASE_COLL('product_point', {
	id: {
		type: String
	},
	product_id: String,
	quantity: Number,
	amount: Number,
	point: Number,
	create_user: Number,
	create_date: Date,
	from_date: Date,
	to_date: Date,
	rule_type: Number,
	isActive: Number,
	// author: {
	// 	type: Schema.Types.ObjectId,
	// 	ref: "user"
	// }
});
