"use strict";
const { Schema } 	= require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION USER CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('user', {
	username: {
		type: String,
		trim: true,
		unique : true
	}, 
	email: {
		type: String,
		trim: true,
		unique : true
	},
	password: {
		type: String
	},
	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},
	/**
	 * Phân quyền truy cập.
	 * 0. ADMIN
	 * 1. OWNER BRAND
	 * 2. EDITER
	 * 3. GloQ
	 * 4. Manage POINT NANDIO
	 */
	role: {
		type: Number,
		default: 0
	},
	
	// customer: sử dụng như địa chỉ ví để nhận điểm, chuyển điểm
	customer: {
        type:  Schema.Types.ObjectId,
		ref: 'customer' 
	},
});
