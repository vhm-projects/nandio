"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path						                    = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_REWARD } 						= require('../constants/reward.uri');
const { IMAGE_MODEL  }                              = require("../../image");
/**
 * MODELS
 */
const REWARD_MODEL                                   = require('../models/reward').MODEL;
const BRAND_MODEL                                    = require("../../brand/models/brand").MODEL;
const { PRODUCT_COLL } = require('../../product');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }
    registerRouting() {
        return {
            [CF_ROUTINGS_REWARD.ADD_REWARD]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let  { _id: userID }  = req.user;
                        let  { image, title, point, startTime, endTime, content, brand, typeReward, productID, amount, weight, length, width, height } =  req.body
                        let infoData  = await REWARD_MODEL.insert({ image, title, point, startTime, endTime, content, userID, brand, typeReward, productID, amount, weight, length, width, height } );
                        res.json(infoData);
                    }]
                },
            },

            /**
             * Function: Danh sách quà tặng
             * Date: 01/04/2021
            */
            [CF_ROUTINGS_REWARD.LIST_REWARD_ADMIN]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    view: 'index.ejs',
					inc: path.resolve(__dirname, '../views/list_reward.ejs'),
                    title: 'NANDIO-REWARD',
                    type: 'view',
                    code: CF_ROUTINGS_REWARD.LIST_REWARD
                },
                methods: {
                    get: [ async function (req, res) {
                        let listReward  = await REWARD_MODEL.getList();
                        let listBrand   = await  BRAND_MODEL.getListByStatus({ status: 1})
                        let listProduct = await  PRODUCT_COLL.find({ isGift: 1, status: 1 }).select("name");
                        return ChildRouter.renderToView(req, res, {
                            listReward : listReward.data, 
                            listBrand: listBrand.data,
                            listProduct
                        });
                     }]
                },
            },
            
            /**
             * Function: Danh sách quà tặng api
             * Date: 01/04/2021
            */
            [CF_ROUTINGS_REWARD.LIST_REWARD_ENDUSER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { limit, page } = req.query;
                        let listReward  = await REWARD_MODEL.getListForEnduser({ limit: +limit, page: +page });
                        res.json(listReward)
                    }]
                },
            },

            /**
             * Function: Danh sách quà tặng api cung cấp cho bên store gọi qua
             * Date: 01/04/2021
            */
            [CF_ROUTINGS_REWARD.LIST_REWARD_ENDUSER_FOR_STORE]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { limit, page } = req.query;
                        let listReward  = await REWARD_MODEL.getListForEnduser({ limit: +limit, page: +page });
                        res.json(listReward)
                    }]
                },
            },
            
            /**
             * Function: Xóa quà tặng
             * Date: 01/04/2021
             * Dev: VinhNH
            */
            [CF_ROUTINGS_REWARD.UPDATE_STAGE_REWARD]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async  function (req, res) {
                        let { _id: userID } = req.user;
                        let  { rewardID } = req.body;
                        let resultUpdate  = await REWARD_MODEL.removeReward({ rewardID });
                        return res.json(resultUpdate);
                    }]
                },
            },
    
            /**
             * Function: Thông tin quà tặng
             * Date: 01/04/2021
            */
            [CF_ROUTINGS_REWARD.INFO_REWARD]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [async  function (req, res) {
                        let  { rewardID } = req.params;
                        let infoReward  = await REWARD_MODEL.getInfoReward({ rewardID });
                        return res.json(infoReward);
                    }]
                },
            },


            /**
             * Function: Sửa thông tin Slide
             * Date: 01/04/2021
             * Dev: VinhNH
            */
            [CF_ROUTINGS_REWARD.UPDATE_REWARD]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async function (req, res) {
                        let  { _id: userID }        = req.user;
                        let  { rewardID, image, title, point, startTime, endTime, content, brand, typeReward, productID, amount, weight, length, width, height } =  req.body;
                        let resultUpdate  = await REWARD_MODEL.update({  rewardID, image, title, point, startTime, endTime, content, userID, brand, typeReward, productID, amount, weight, length, width, height } );
                        res.json(resultUpdate);
                    }]
                },
            },
        }
    }
};
