"use strict";

/**
 * EXTERNAL PAKCAGE
 */
let fs                                    = require("fs");
let path                                  = require('path');
const ObjectID                            = require('mongoose').Types.ObjectId;
const moment                              = require('moment');

/**
 * BASE
 */
const BaseModel                           = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const REWARD_COLL                         = require('../databases/reward-coll');
// const { IMAGE_COLL }                      = require('../../image');
// const { USER_COLL }                       = require('../../users');
const IMAGE_MODEL                         = require('../../image/models/image').MODEL;
const GIFT_ADDITIONAL_MODEL               = require('../models/gift_additional').MODEL;
const PRODUCT_MODEL                       = require('../../product/models/product').MODEL;

class Model extends BaseModel {
    constructor() {
        super(REWARD_COLL);
    }

    // Thêm tin nhắn mới
    insert({ image, title, point, startTime, endTime, content, userID, brand, typeReward, productID, amount, weight, length, width, height }) { 
        return new Promise(async (resolve) => {    
            try {

                let dataInsert = {
                    title, userCreate: userID
                }

                if(point){
                    dataInsert.point = point;
                }

                if(amount){
                    dataInsert.amount = amount;
                }

                if(startTime){
                    dataInsert.startTime = startTime;
                }

                if(endTime){
                    dataInsert.endTime = endTime;
                }

                if(content){
                    dataInsert.content = content;
                }

                if(brand){
                    dataInsert.brand = brand;
                }

                if(image){
                    let { name, size, path } = image;
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path
                    });
                    dataInsert.avatar = ObjectID(infoImageAfterInsert.data._id);
                }

                /***
                 * typeReward: 1 => Tạo mới quà
                 */
                if(typeReward == 1){
                    dataInsert.title = title;
                    if (!weight) {
                        return resolve({ error: true, message: 'Trọng lượng (gram) không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(weight)) || Number(weight) < 0) {
                            return resolve({ error: true, message: 'Trọng lượng (gram) không hợp lệ' });
                        }
                    }
    
                    if (!length) {
                        return resolve({ error: true, message: 'Độ dài (cm) không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(length)) || Number(length) < 0) {
                            return resolve({ error: true, message: 'Độ dài (cm) không hợp lệ' });
                        }
                    }
    
                    if (!width) {
                        return resolve({ error: true, message: 'Độ rộng (cm) không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(width)) || Number(width) < 0) {
                            return resolve({ error: true, message: 'Độ rộng (cm) không hợp lệ' });
                        }
                    }
    
                    if (!height) {
                        return resolve({ error: true, message: 'Chiều cao (cm) không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(height)) || Number(height) < 0) {
                            return resolve({ error: true, message: 'Chiều cao (cm) không hợp lệ' });
                        }
                    }

                    let infoAdditional = await GIFT_ADDITIONAL_MODEL.insert({ image, name: title, content, brand, userID, weight, length, width, height });
                    dataInsert.gift = {
                        kind: 'gift_additional',
                        item: infoAdditional.data?._id
                    }
                    dataInsert.type = 1;
                }

                /***
                 * typeReward: 2 => Tạo mới quà từ sản phẩm
                 */
                if(typeReward == 2){
                    let infoProduct = await PRODUCT_MODEL.getInfoV2({ productID });
                    if (infoProduct.error) {
                        return resolve(infoProduct)
                    }
                    let { data: { name } } = infoProduct;
                    dataInsert.title = name;

                    dataInsert.gift = {
                        kind: 'product',
                        item: productID
                    }
                    dataInsert.type = 2;
                }
               
                let resultInsert = await this.insertData(dataInsert);
                if(!resultInsert)
                    return resolve({ error: true, message: 'cannot_insert' });
                
                return resolve({ error: false, data: resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    // update mẫu tin (cập nhật trạng thái ngưng hoạt động,...)
    update({ rewardID, image, title, point, startTime, endTime, content, userID, brand, typeReward, productID, amount, weight, length, width, height }) {
        return new Promise(async (resolve) => {
            try {
                if(!rewardID)
                    return resolve({ error: true, message: 'param_invalid' });

                let dataUpdate = {
                    title, userUpdate: userID
                }

                if(point){
                    dataUpdate.point = point;
                }

                if(startTime){
                    dataUpdate.startTime = startTime;
                }

                if(endTime){
                    dataUpdate.endTime = endTime;
                }

                if(content){
                    dataUpdate.content = content;
                }

                if(brand){
                    dataUpdate.brand = brand;
                }

                if(amount){
                    dataUpdate.amount = amount;
                }

                if(image){
                    let { name, size, path } = image;
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path
                    });
                    dataUpdate.image = ObjectID(infoImageAfterInsert.data._id);
                }

                 /***
                 * typeReward: 1 => Tạo mới quà
                 */
                if(typeReward == 1){
                    if (!weight) {
                        return resolve({ error: true, message: 'Trọng lượng (gram) không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(weight)) || Number(weight) < 0) {
                            return resolve({ error: true, message: 'Trọng lượng (gram) không hợp lệ' });
                        }
                    }
    
                    if (!length) {
                        return resolve({ error: true, message: 'Độ dài (cm) không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(length)) || Number(length) < 0) {
                            return resolve({ error: true, message: 'Độ dài (cm) không hợp lệ' });
                        }
                    }
    
                    if (!width) {
                        return resolve({ error: true, message: 'Độ rộng (cm) không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(width)) || Number(width) < 0) {
                            return resolve({ error: true, message: 'Độ rộng (cm) không hợp lệ' });
                        }
                    }
    
                    if (!height) {
                        return resolve({ error: true, message: 'Chiều cao (cm) không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(height)) || Number(height) < 0) {
                            return resolve({ error: true, message: 'Chiều cao (cm) không hợp lệ' });
                        }
                    }

                    let infoReward = await REWARD_COLL.findById(rewardID).populate({
                        path : 'gift.item',
                        select: "brand name avatar",
                        populate: {
                            path: "brand avatar",
                            select: "name path",
                        }
                    });

                    let giftAdditionalID = infoReward.gift?.item?._id;
                    if(giftAdditionalID){
                        let infoAdditional = await GIFT_ADDITIONAL_MODEL.update({ giftAdditionalID, image, name: title, content, brand, userID, weight, length, width, height });
                    }else{
                        let infoAdditional = await GIFT_ADDITIONAL_MODEL.insert({ image, name: title, content, brand, userID, weight, length, width, height });
                        dataUpdate.gift = {
                            kind: 'gift_additional',
                            item: infoAdditional.data?._id
                        }
                    }
                    // dataUpdate.gift = {
                    //     kind: 'gift_additional',
                    //     item: infoAdditional.data?._id
                    // }
                    dataUpdate.type = 1;
                }

                /***
                 * typeReward: 2 => Tạo mới quà từ sản phẩm
                 */
                if(typeReward == 2){
                    let infoProduct = await PRODUCT_MODEL.getInfoV2({ productID });
                    if (infoProduct.error) {
                        return resolve(infoProduct)
                    }
                    let { data: { name } } = infoProduct;
                    dataUpdate.title = name;

                    dataUpdate.gift = {
                        kind: 'product',
                        item: productID
                    }
                    dataUpdate.type = 2;
                }
                console.log({
                    dataUpdate
                });
                let resultUpdate = await REWARD_COLL.findByIdAndUpdate(rewardID, dataUpdate, { new: true })
                if(!resultUpdate)
                    return resolve({ error: true, message: 'cannot_update' });
                
                return resolve({ error: false, data: resultUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    getInfoReward({ rewardID }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {
                let resultFind =  await REWARD_COLL.findById(rewardID)
                .select("title image point startTime endTime content brand type gift amount")
                .populate({
                    path : 'image',
                    select: "path"
                })
                .populate({
                    path : 'gift.item',
                    select: "brand name avatar weight length width height",
                    populate: {
                        path: "brand avatar",
                        select: "name path",
                    }
                })
                if(!resultFind)
                    return resolve({ error: true, message: 'cannot_get' });
                return resolve({ error: false, data: resultFind });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    //Admin lấy tất cả  mẫu tin
    getList(){
        return new Promise(async (resolve) => {
            try {
                let listRewardActive = await REWARD_COLL.find({ 
                    state: 1,
                    startTime: {
                        $lte: moment()
                    },
                    endTime: {
                        $gte: moment().endOf('day').format()
                    }
                })
                .populate({
                    path : 'image',
                    select: "path"
                })
                .populate({
                    path : 'brand',
                    select: "name"
                })
                .populate({
                    path : 'gift.item',
                    select: "brand name avatar",
                    populate: {
                        path: "brand avatar"
                    }
                })
                .sort({
                    point: -1
                })
                console.log({ listRewardActive: listRewardActive[0].gift.item });
                let arrRewardIDActive = listRewardActive.map(reward => reward._id);
                let listRewardInActive = await REWARD_COLL.find({ 
                    state: 1,
                    _id: { $nin: arrRewardIDActive }
                })
                .populate({
                    path : 'image',
                    select: "path"
                })
                .populate({
                    path : 'brand',
                    select: "name"
                })
                .populate({
                    path : 'gift.item',
                    select: "brand name avatar",
                    populate: {
                        path: "brand avatar"
                    }
                })
                .sort({
                    createAt: -1
                })
                if(!listRewardActive)
                    return resolve({ error: true, message: 'cant_update' });
                return resolve({ error: false, data: { listRewardActive, listRewardInActive }});
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListForEnduser({ limit = 4, page = 1 }){
        return new Promise(async (resolve) => {
            try {

                let listReward = await REWARD_COLL.find({ 
                    state: 1,
                    startTime: {
                        $lte: new Date()
                    },
                    endTime: {
                        $gte: new Date()
                    }
                })
                .select("title image point amount startTime endTime brand gift")
                .skip((page * limit) - limit)
                .limit(limit)
                .populate({
                    path : 'image',
                    select: "path"
                })
                .populate({
                    path : 'brand',
                    select: "name"
                })
                .populate({
                    path : 'gift.item',
                    select: "brand name avatar",
                    populate: {
                        path: "brand avatar",
                        select: "name path",
                    }
                })
                .sort({
                    point: -1
                })

                if(!listReward)
                    return resolve({ error: true, message: 'cant_update' });
                let totalItem = await REWARD_COLL.count({
                    state: 1,
                    startTime: {
                        $lte: new Date()
                    },
                    endTime: {
                        $gte: new Date()
                    }
                });
                let pages = Math.ceil(totalItem/limit);
                return resolve({ error: false, data: { listReward, currentPage: +page, totalPage: +pages  }});
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // Thêm tin nhắn mới
    removeReward({ rewardID }) { 
        return new Promise(async (resolve) => {
            try {
                if(!ObjectID.isValid(rewardID))
                    return resolve({ error: true, message: 'param_invalid' });
                
                let resultRemove = await REWARD_COLL.findByIdAndUpdate(rewardID, { state: 2 })
                if(!resultRemove)
                    return resolve({ error: true, message: 'cant_delet' });
                
                return resolve({ error: false, data: resultRemove });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
