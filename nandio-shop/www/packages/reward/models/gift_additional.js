"use strict";

/**
 * EXTERNAL PAKCAGE
 */
let fs                                    = require("fs");
let path                                  = require('path');
const ObjectID                            = require('mongoose').Types.ObjectId;
const moment                              = require('moment');

/**
 * BASE
 */
const BaseModel                           = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const GIFT_ADDITIONAL_COLL                = require('../databases/gift_additional-coll');
const IMAGE_MODEL                         = require('../../image/models/image').MODEL;

class Model extends BaseModel {
    constructor() {
        super(GIFT_ADDITIONAL_COLL);
    }

    // Thêm đổi quà
    insert({ image, name, content, brand, userID, weight, length, width, height }) { 
        return new Promise(async (resolve) => {    
            try {
                if(!name)
                    return resolve({ error: true, message: 'param_invalid' });  
                let dataInsert = {
                    name, userCreate: userID
                }
                if(content){
                    dataInsert.content = content;
                }

                if(brand){
                    dataInsert.brand = brand;
                }

                if (weight) {
                    dataInsert.weight = weight;
                }

                if (length) {
                    dataInsert.length = length;
                }

                if (width) {
                    dataInsert.width = width;
                }

                if (height) {
                    dataInsert.height = height;
                }

                if(image){
                    let { name, size, path } = image;
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path
                    });
                    dataInsert.avatar = ObjectID(infoImageAfterInsert.data._id);
                }

                let resultInsert = await this.insertData(dataInsert);
                if(!resultInsert)
                    return resolve({ error: true, message: 'cannot_insert' });
                
                return resolve({ error: false, data: resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ giftAdditionalID, image, name, content, brand, userID, weight, length, width, height }) { 
        return new Promise(async (resolve) => {    
            try {
                console.log({ giftAdditionalID, image, name, content, brand, userID, weight, length, width, height });
                if(!name)
                    return resolve({ error: true, message: 'param_invalid' });  
                let dataUpdate = {
                    name, userCreate: userID
                }
                if(content){
                    dataUpdate.content = content;
                }

                if(brand){
                    dataUpdate.brand = brand;
                }

                if (weight) {
                    dataUpdate.weight = weight;
                }

                if (length) {
                    dataUpdate.length = length;
                }

                if (width) {
                    dataUpdate.width = width;
                }

                if (height) {
                    dataUpdate.height = height;
                }

                if(image){
                    let { name, size, path } = image;
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path
                    });
                    dataUpdate.avatar = ObjectID(infoImageAfterInsert.data._id);
                }

                let resultInsert = await GIFT_ADDITIONAL_COLL.findByIdAndUpdate(giftAdditionalID, dataUpdate, { new: true } );
                if(!resultInsert)
                    return resolve({ error: true, message: 'cannot_insert' });
                
                return resolve({ error: false, data: resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
