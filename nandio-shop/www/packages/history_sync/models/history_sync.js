"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const request                       = require('request');

/**
 * INTERNAL PACKAGES
 */
const cfJWS                         = require('../../../config/cf_jws');

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const {checkObjectIDs} 				= require('../../../utils/utils');
/**
 * COLLECTIONS
 */
const HISTORY_SYNC  				= require('../databases/history_sync-coll');

class Model extends BaseModel {
    constructor() {
        super(HISTORY_SYNC);
    }

    insert({ type, product_id, point }) {
        return new Promise(async resolve => {
            try {
                let dataInsert = {
                    type
                }

                if(product_id){
                    dataInsert.product_id = product_id
                }

                if(point){
                    dataInsert.point = point;
                }

                let checkExistType3 = await HISTORY_SYNC.findOne({ product_id, type: 3 });
                if(checkExistType3){
                    let dataAfterUpdate = await HISTORY_SYNC.findOneAndUpdate({ product_id, type: 3 }, { point })
                    return resolve({ error: false, data: dataAfterUpdate });
                }
                let infoAfterInsert = await this.insertData(dataInsert)
                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }
}

exports.MODEL = new Model;
