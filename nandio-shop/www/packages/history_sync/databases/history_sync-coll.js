"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');
/**
 * Điểm sản phẩm
 */
module.exports = BASE_COLL('history_sync', {
	/**
	 * 1: Đồng bộ điểm tạo khách hàng
	 * 2: Đồng bộ cập nhật điểm khách hàng 
	 * 3: Đồng bộ cập nhật điểm sản phẩm 
	 */
	type: Number,
	/**
	 * Nếu Type 3 thì lưu product_id => sử dụng để nếu chỉnh sửa nhiều lần thì xóa cái trước
	 */
	product_id: String,

	/**
	 * Nếu Type 1 2 thì lưu phone => sử dụng để nếu chỉnh sửa nhiều lần thì xóa cái trước
	 */
	phone: String,
	point: Number,
});
