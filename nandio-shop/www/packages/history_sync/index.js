const PRODUCT_POINT_MODEL 		= require('./models/product_point').MODEL;
const PRODUCT_POINT_COLL  		= require('./databases/history_sync_error');
const PRODUCT_POINT_ROUTES       = require('./apis/product_point');

module.exports = {
    PRODUCT_POINT_ROUTES,
    PRODUCT_POINT_COLL,
    PRODUCT_POINT_MODEL,
}
