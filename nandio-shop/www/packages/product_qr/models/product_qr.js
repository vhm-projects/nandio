"use strict";

/**
 * EXTERNAL PAKCAGE
 */
let fs                                    = require("fs");
let path                                  = require('path');
const ObjectID                            = require('mongoose').Types.ObjectId;
const { randomNumbers } 	              = require('../../../utils/string_utils');
const { checkObjectIDs } 	              = require('../../../utils/utils');
const moment                              = require('moment');
/**
 * BASE
 */
const BaseModel                           = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const PRODUCT_QR_COLL                         = require('../databases/product_qr-coll');
const PRODUCT_MODEL                           = require('../../product/models/product').MODEL;
const HISTORY_POINT_MODEL                     = require('../../history_point/models/history_point').MODEL;
const HISTORY_POINT_COLL                      = require('../../history_point/databases/history_point-coll');
const CUSTOMER_COLL                           = require('../../customer/databases/customer-coll');

class Model extends BaseModel {
    constructor() {
        super(PRODUCT_QR_COLL);
        this.STATUS_AFTER_SCAN = 1
    }

    checkCodeExists(code){
		return new Promise(resolve => {
			(async function recursiveCheckCode(code){
				let checkExists = await PRODUCT_QR_COLL.findOne({ code });
				if(checkExists){
					code = randomNumbers(10);
					recursiveCheckCode(code);
				} else{
					resolve(code);
				}
			})(code)
		})
	}

    // Thêm tin nhắn mới
    insert({ product }) { 
        return new Promise(async (resolve) => {    
            try {
                if(!ObjectID.isValid(product))
                    return resolve({ error: true, message: 'param_invalid' });  
                let code = await this.checkCodeExists(randomNumbers(10))   
                let dataInsert = {
                    product, code
                }
                let resultInsert = await this.insertData(dataInsert);
                if(!resultInsert)
                    return resolve({ error: true, message: 'cannot_insert' });
                
                return resolve({ error: false, data: resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    // update mẫu tin  (cập nhật trạng thái ngưng hoạt động,...)
    updateWhenScan({ code, customerID }) {
        return new Promise(async (resolve) => {
            try {
                if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'param_invalid' });
                
                let resultUpdate = await PRODUCT_QR_COLL.findOneAndUpdate({ code }, { status: this.STATUS_AFTER_SCAN, customer: customerID }, { new: true })
                if(!resultUpdate)
                    return resolve({ error: true, message: 'cannot_update' });
                
                return resolve({ error: false, data: resultUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    getInfoByCode({ code }) {
        return new Promise(async (resolve) => {
            try {
                let infoData = await PRODUCT_QR_COLL.findOne({ code }).populate({
                    path: "product",
                    populate: {
                        path: "category brand segments avatar images qr_code",
                        select: "title name image path ",
                        populate: {
                            path: "image",
                            select: "path"
                        }
                    }
                }).populate({
                    path: "customer",
                });
                if(!infoData)
                    return resolve({ error: true, message: 'cannot_get_info' });

                // Lấy danh sách bigsale và flashsale
                let listBigsaleAndFlashsale = await PRODUCT_MODEL.getListBigSaleAndInfoFlashSale();
                let { listBigSale, infoFlashSale } = listBigsaleAndFlashsale.data;
                let data = {
                    infoData, listBigSale, infoFlashSale
                }
                return resolve({ error: false, data });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    accumlateHistoryPointByCode({ code, customerID }) {
        return new Promise(async (resolve) => {
            try {
                let infoData = await PRODUCT_QR_COLL.findOne({ 
                    code, 
                    // customer: {
                    //     $exists: false
                    // }
                }).populate({
                    path: 'parcel',
                    select: 'manufactureDate expiryDate point status'
                });
               
                if(!infoData)
                    return resolve({ error: true, message: 'Không tồn tại mã' });

                if (infoData.sku_1) {
                    return resolve({ error: true, message: 'Mã không được tích điểm' });
                }

                if (infoData.customer)
                    return resolve({ error: true, message: 'Sản phẩm đã được tích điểm trước đó' });
              
                let checkExist__Customer = await CUSTOMER_COLL.findById(customerID);
                if (!checkExist__Customer) {
                    return resolve({ error: true, message: 'Khách hàng không tồn tại' });
                }

                let infoProductSku2 = await PRODUCT_MODEL.getInfoProductOfSku2({ sku_2: infoData.sku_2 });
                if (infoProductSku2.error) {
                    return resolve(infoProductSku2);
                }
                
                // Lấy danh sách bigsale và flashsale
                let listBigsaleAndFlashsale = await PRODUCT_MODEL.getListBigSaleAndInfoFlashSale();
                let { listBigSale, infoFlashSale } = listBigsaleAndFlashsale.data;
                let data = {
                    infoProduct: infoProductSku2.data, 
                    listBigSale, infoFlashSale, 
                    infoParcel: infoData.parcel, 
                    infoProductQR: infoData
                }

                
                const TICH_DIEM = 1;
                const point     = infoData.parcel.point ? infoData.parcel.point : 0; // POINT TỪ LÔ

                // LẤY LAST RECORD HISTORY POINT ======
                let infoLastRecordHistoryPoint = await HISTORY_POINT_COLL.findOne({ 
                    customer: customerID
                }).sort({ createAt: -1 });
              
                let conditionObjHistoryPoint = {
                    beforPoint: 0,
                    type: TICH_DIEM,
                    productID: infoProductSku2.data._id,
                    customerID: customerID,
                    createAt: new Date(),
                    modifyAt: new Date(),
                }

                if (infoLastRecordHistoryPoint) {
                    conditionObjHistoryPoint = {
                        ...conditionObjHistoryPoint,
                        beforPoint: infoLastRecordHistoryPoint.point
                    }
                }

                conditionObjHistoryPoint = {
                    ...conditionObjHistoryPoint,
                    currentPoint: point,
                    point: conditionObjHistoryPoint.beforPoint + point,
                    flatform: 'SHOP'
                }
                
                // let infoHistoryPointAfterInsert = await HISTORY_POINT_COLL.create(conditionObjHistoryPoint);
                // if (!infoHistoryPointAfterInsert) {
                //     return resolve({ error: true, message: 'Không thể tạo lịch sử điểm' });
                // }
                const TRANSFER_POINT = 5;
                let infoHistoryPointAfterInsert = await HISTORY_POINT_MODEL.createTransferPointBy__NandioAdmin({ 
                    productID:       infoProductSku2.data._id,
                    customerID:      customerID, 
                    typeNandioAdmin: TRANSFER_POINT, 
                    type:            TICH_DIEM, 
                    currentPoint:    point
                });
                console.log({
                    infoHistoryPointAfterInsert
                });
                if (infoHistoryPointAfterInsert.error) {
                    return resolve(infoHistoryPointAfterInsert);
                }

                const STATUS_SHOWED_QR = 1;
                let infoAfterUpdate = await PRODUCT_QR_COLL.findByIdAndUpdate(infoData._id, {
                    status: STATUS_SHOWED_QR,
                    customer: customerID
                });


                /**
                 * ===================================================================
                 *  UPDATE LẠI POINT CỦA KHÁCH HÀNG
                 * ===================================================================
                 */
                // let infoEmployeeAfterUpdatePoint = await CUSTOMER_COLL.findByIdAndUpdate(customerID, {
                //     pointRanking: checkExist__Customer.pointRanking + point, // CỘNG VÀO POINT TỔNG
                //     point: checkExist__Customer.point + point, // CỘNG VÀO POINT SỬ DỤNG
                // });
                
                // if (!infoEmployeeAfterUpdatePoint) {
                //     return resolve({ error: true, message: 'Không thể cập nhật điểm của Khách hàng' });
                // }


                return resolve({ error: false, data });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoByCode_v3({ code, customerID }) {
        return new Promise(async (resolve) => {
            try {
                let infoData = await PRODUCT_QR_COLL.findOne({ 
                    code, 
                    // customer: {
                    //     $exists: false
                    // }
                }).populate({
                    path: 'parcel',
                    select: 'manufactureDate expiryDate point'
                });
                if(!infoData)
                    return resolve({ error: true, message: 'Không tồn tại mã' });
              
                // if (infoData.customer)
                //     return resolve({ error: true, message: 'Sản phẩm đã được tích điểm trước đó' });
                let data = {};
                if (infoData.sku_2) { // SKU ĐẦU 2
                    let infoProductSku2 = await PRODUCT_MODEL.getInfoProductOfSku2({ sku_2: infoData.sku_2 });
                    if (infoProductSku2.error) {
                        return resolve(infoProductSku2);
                    }
                    data = {
                        infoProduct: infoProductSku2.data, 
                    }
                } else { // SKU ĐẦU 1
                    let infoProductSku1 = await PRODUCT_MODEL.getInfoProductOfSku1({ sku_1: infoData.sku_1 });
                    if (infoProductSku1.error) {
                        return resolve(infoProductSku1);
                    }
                    data = {
                        infoProduct: infoProductSku1.data, 
                    }
                }

                // Lấy danh sách bigsale và flashsale ádasd
                let listBigsaleAndFlashsale = await PRODUCT_MODEL.getListBigSaleAndInfoFlashSale();
                let { listBigSale, infoFlashSale } = listBigsaleAndFlashsale.data;
                data = {
                    ...data,
                    listBigSale, infoFlashSale, 
                    infoParcel: infoData.parcel, 
                    infoProductQR: infoData
                }

                return resolve({ error: false, data });
                
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListHistoryPointOfCustomer__ApiStore({ customerID, page = 1, limit = 3 }) {
        return new Promise(async resolve => {
            try {
				limit = isNaN(limit) ? 3 : +limit;
                page = isNaN(page) ? 1 : +page;

				if (!checkObjectIDs(customerID)) 
					return resolve({ error: true, message: "ID Khách hàng không hợp lệ" });

                let infoHistoryPointOfOrder = await HISTORY_POINT_COLL
                    .find({ customer: customerID})
                    .limit(+limit)
                    .skip((page - 1) * limit)
                    .sort({ modifyAt: -1 })
                    .select("product customer beforPoint point type createAt")
                    .populate({
                        path: 'product',
                        select: 'SKU name'
                    })
                    .lean();

                if(!infoHistoryPointOfOrder)
                    return resolve({ error: true, message: "Không thể lấy danh sách lịch sử điểm" }); 

				return resolve({ 
					error: false, 
					data: infoHistoryPointOfOrder
				});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    //Admin lấy tất cả  mẫu tin
    getListByAdmin({ product, status, customer }){  
        return new Promise(async (resolve) => {
            try {
                let dataSearch = {}
                if(ObjectID.isValid(product)){
                    dataSearch.product = product;
                }

                if(ObjectID.isValid(customer)){
                    dataSearch.customer = customer;
                }

                if(status){
                    dataSearch.status = status;
                }

                let listData = await PRODUCT_QR_COLL.find(dataSearch)
                .populate({
                    path : 'sku_2 customer'
                })
                if(!listData)
                    return resolve({ error: true, message: 'cant_update' });
                return resolve({ error: false, data: listData});
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByPagination({ product, status, customer, 
        limit,
        keyword,
        page, 
    }){  
        return new Promise(async (resolve) => {
            try {
                let dataSearch = {};
                let skip = (page - 1) * limit;

                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    dataSearch.$or = [
                        { code: new RegExp(key, 'i') },
                    ];

                    // conditionObj.$or = [
                    //     { quantities_parcel: new RegExp(key, 'i') },
                    // ];
                }

                if(ObjectID.isValid(product)){
                    dataSearch.product = product;
                }

                if(ObjectID.isValid(customer)){
                    dataSearch.customer = customer;
                }

                if(status){
                    dataSearch.status = status;
                }
               
                let count = await PRODUCT_QR_COLL.count(dataSearch);
                let listData = await PRODUCT_QR_COLL.find(dataSearch)
                    .limit(limit * 1)
                    .skip(skip)
                    .populate({
                        path : 'sku_2 customer parcel'
                    })
                    
                if(!listData)
                    return resolve({ error: true, message: 'cant_update' });
               
                let arrayProductQR = [];
                listData && listData.length  && listData.map((item, index) => {
               
                    let indexChange       = skip + index + 1;
                    let code  = item.code && item.code || '';
                    let sku_2 = item.sku_2 && item.sku_2.name || '';
                    let sku_1 = item.sku_1 && item.sku_1 || '';
                    let parcel = item.parcel && item.parcel.quantities_parcel || '';
                    let customer = item.customer && item.customer.fullname || '';
                    // let createAt = moment(item.createAt).format('LT') + " " + moment(item.createAt).format('L');
                    
                    let badge = 'badge-success';
                    let nameStatus = 'Chưa quét'
                    if (item.status) {
                        badge = 'badge-danger';
                        nameStatus = 'Đã quét';
                    }
                    let status = `
                        <span class="badge badge-pill ${badge}" style="padding: 5px; font-size: 10px;">
                            ${nameStatus}
                        </span>
                    `;
                    arrayProductQR = [
                        ...arrayProductQR,
                        {
                            index: indexChange,
                            code,
                            sku_2,
                            sku_1,
                            parcel,
                            customer,
                            status,
                            // createAt
                        }
                    ]
                });
              
                return resolve({ error: false, data: arrayProductQR, recordsTotal: count, recordsFiltered: count });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
