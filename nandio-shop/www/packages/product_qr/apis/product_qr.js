"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path						                    = require('path');
const { request }                                   = require('http');
const TOKEN_STORE                                   = process.env.TOKEN_STORE;
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_PRODUCT_QR } 						= require('../constants/product_qr.uri');
/**
 * MODELS
 */
const PRODUCT_QR_MODEL                                   = require('../models/product_qr').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }
    registerRouting() {
        return {

            /**
             * Function: Danh sách product QR
             * Dev: Depv
             */
            [CF_ROUTINGS_PRODUCT_QR.LIST_PRODUCT_QR]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_PRODUCT_QR.LIST_PRODUCT_QR,
                    inc : path.resolve(__dirname, '../views/list_product_qr.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { product, status, customer } = req.query;
                        // let listProductQR = await PRODUCT_QR_MODEL.getListByAdmin({  product, status, customer });
                        ChildRouter.renderToView(req, res, { 
                            // listProductQR: listProductQR.data
                        });
                    }]
                },
            },

            [CF_ROUTINGS_PRODUCT_QR.LIST_PRODUCT_QR_PAGINATION]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        // let { product, status, customer } = req.query;
                        let { start, length, search } = req.body;
                        let page = Number(start)/Number(length) + 1;

                        let listProductQR = await PRODUCT_QR_MODEL.getListByPagination({  
                            page: Number(page), 
                            limit: Number(length), 
                            keyword: search.value  
                        });
                        res.json(listProductQR);
                    }]
                },
            },

            [CF_ROUTINGS_PRODUCT_QR.ADD_PRODUCT_QR]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let  { productID } =  req.body
                        let infoData  = await PRODUCT_QR_MODEL.insert({ product: productID });
                        res.json(infoData);
                    }]
                },
            },


            [CF_ROUTINGS_PRODUCT_QR.ADD_PRODUCT_QR]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let  { productID } =  req.body
                        let infoData  = await PRODUCT_QR_MODEL.insert({ product: productID });
                        res.json(infoData);
                    }]
                },
            },

            /**
             * Function: TÍch điểm Qr product
             * Date: 01/04/2021
             */
            [CF_ROUTINGS_PRODUCT_QR.ACCUMULATE_PRODUCT_QR]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } =  req.customer;
                        let  { code } =  req.params
                        let infoData  = await PRODUCT_QR_MODEL.accumlateHistoryPointByCode({ code, customerID });
                        res.json(infoData);
                     }]
                },
            },

             /**
             * Function: TÍch điểm Qr product API STORE
             * Date: 01/04/2021
             */
              [CF_ROUTINGS_PRODUCT_QR.API_ACCUMULATE_PRODUCT_QR]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { customerID, TOKEN } = req.query;
                        if (TOKEN != TOKEN_STORE) {
                            res.json({ error: true, message: 'TOKEN không hợp lệ' });
                        }
                        
                        let  { code } =  req.params
                        let infoData  = await PRODUCT_QR_MODEL.accumlateHistoryPointByCode({ code, customerID });
                        res.json(infoData);
                     }]
                },
            },

             /**
             * Function: Thông tin Qr product
             * Date: 01/04/2021
             */
            [CF_ROUTINGS_PRODUCT_QR.DETAIL_PRODUCT_QR]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } =  req.customer;
                        let  { code } =  req.params
                        let infoData  = await PRODUCT_QR_MODEL.getInfoByCode_v3({ code, customerID });
                        res.json(infoData);
                     }]
                },
            },

            
            /**
             * Function: Danh sách đơn hàng (API) => GỌI TỪ NANDIO STORE
             * Date: 03/07/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_PRODUCT_QR.API_LIST_HISTORY_POINT]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
                        const { customerID }  = req.params
						const { TOKEN, page, limit } = req.query;
                       
                        if (TOKEN != TOKEN_STORE) {
						    return res.json({ error: true, message: 'TOKEN không hợp lệ' });
                        }
                        
						const listHistory = await PRODUCT_QR_MODEL.getListHistoryPointOfCustomer__ApiStore({ customerID, page, limit });
						res.json(listHistory);
					}]
                },
            },

            /**
             * Function: Thông tin Qr product API STORE
             * Date: 01/04/2021
             */
             [CF_ROUTINGS_PRODUCT_QR.API_DETAIL_PRODUCT_QR]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { customerID, TOKEN } = req.query;
                        if (TOKEN != TOKEN_STORE) {
                            res.json({ error: true, message: 'TOKEN không hợp lệ' });
                        }
                        let  { code } =  req.params
                        let infoData  = await PRODUCT_QR_MODEL.getInfoByCode_v3({ code, customerID });
                        res.json(infoData);
                     }]
                },
            },

            /**
             * Function: Danh sách quà tặng api
             * Date: 01/04/2021
            */
            [CF_ROUTINGS_PRODUCT_QR.UPDATE_PRODUCT_QR]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let  { code } =  req.params
                        let  { _id: customerID } =  req.customer;
                        let infoData  = await PRODUCT_QR_MODEL.updateWhenScan({ code, customerID });
                        res.json(infoData);
                    }]
                },
            },
        }
    }
};
