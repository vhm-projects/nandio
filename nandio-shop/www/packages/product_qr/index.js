const PRODUCT_QR_MODEL 		= require('./models/product_qr').MODEL;
const PRODUCT_QR_COLL  		= require('./databases/product_qr-coll');
const PRODUCT_QR_ROUTES     = require('./apis/product_qr');

module.exports = {
    PRODUCT_QR_ROUTES,
    PRODUCT_QR_COLL,
    PRODUCT_QR_MODEL
}
