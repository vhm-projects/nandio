const BASE_ROUTE = '/product-qr';
const BASE_ROUTE_ADMIN = '/admin';

const CF_ROUTINGS_PRODUCT_QR = {
    ADD_PRODUCT_QR: `${BASE_ROUTE}/add-product-qr`,
    LIST_PRODUCT_QR: `${BASE_ROUTE_ADMIN}/list-product-qr`,
    LIST_PRODUCT_QR_PAGINATION: `${BASE_ROUTE_ADMIN}/list-product-qr-pagination`,

    ACCUMULATE_PRODUCT_QR:     `${BASE_ROUTE}/info-product-qr/:code`,
    API_ACCUMULATE_PRODUCT_QR: `/api${BASE_ROUTE}/info-product-qr/:code`,

    DETAIL_PRODUCT_QR:     `${BASE_ROUTE}/detail-product-qr/:code`,
    API_DETAIL_PRODUCT_QR: `/api${BASE_ROUTE}/detail-product-qr/:code`,

    UPDATE_PRODUCT_QR: `${BASE_ROUTE}/update-product-qr/:code`,

    API_LIST_HISTORY_POINT: `/api/history-point/list-history-point/:customerID`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_PRODUCT_QR = CF_ROUTINGS_PRODUCT_QR;
