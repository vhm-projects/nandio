"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');
// LÔ HÀNG
module.exports = BASE_COLL("product_qr", {
        /**
         * Điểm
         */
        point: {
            type: Number,
            default: 0
        },
        /**
         * Số lô
         */
        code: String,
        /**
         * Ngày sản xuất
         */
        manufactureDate: Date,
        /**
         * Ngày hết hạn
         */
        expiredTime: Date,
        /**
         * Mã SKU đầu 2
         */
        SKU2: String
});
