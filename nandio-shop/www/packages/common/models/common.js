
 "use strict";

 /**
  * EXTERNAL PACKAGES
  */
const ObjectID         = require('mongoose').Types.ObjectId;
const path             = require('path');
const fs               = require('fs');
const request          = require('request');
const DOMAIN_GHN       = process.env.DOMAIN_GHN || 'https://dev-online-gateway.ghn.vn';
const TOKEN_GHN        = process.env.TOKEN_GHN  || 'a4cc2e63-3191-11ec-b514-aeb9e8b0c5e3';
/**
 * BASE
 */
const { districts } = require('../constants/districts');
const { provinces } = require('../constants/provinces');


class Model {
    listDistrict({ province }) {
        try {
            let listDistricts = [];

            let filterObject = (obj, filter, filterValue) => 
                Object.keys(obj).reduce((acc, val) =>
                (obj[val][filter] === filterValue ? {
                    ...acc,
                    [val]: obj[val]  
                } : acc
            ), {});

            if (province && !Number.isNaN(Number(province))) {
                listDistricts = filterObject(districts, 'parent_code', province.toString())
            }

            return { error: false, data: listDistricts };
        } catch (error) {
            return { error: true, message: error.message };
        }
    }

    listProvinceAll({}) {
        try {
            let listProvince = Object.entries(provinces);
            return { error: false, data: listProvince };
        } catch (error) {
            return { error: true, message: error.message };
        }
    }

    listWard({ district }) {
        return new Promise(async resolve => {
            try {
                let listWards = [];
                let filePath = path.resolve(__dirname, `../constants/wards/${district}.json`);
                await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                    if (!err) {
                        listWards = JSON.parse(data);
                        return resolve({ error: true, data: listWards });
                    } else {
                        return resolve({ error: true, message: "district_not_exist" });
                    }
                });
            } catch (error) {
                return { error: true, message: error.message };
            }
        })
    }

    getInfoProvince({ provinceCode }) {
        try {
            let listProvince    = Object.entries(provinces);
            let provinceArr     = [];

            for (let province of listProvince){
                if (province[1].code == Number(provinceCode)){
                    provinceArr = province;
                    break;
                }
            }

            return { error: false, data: provinceArr };
        } catch (error) {
            return { error: true, message: error.message };
        }
    }

    getInfoDistrict({ districtCode }) {
        try {
            let listDistricts = Object.entries(districts);
            let districtArr = [];
            for (let district of listDistricts){
                if (district[1].code == districtCode){
                    districtArr = district;
                    break;
                }
            }

            return { error: false, data: districtArr };
        } catch (error) {
            return { error: true, message: error.message };
        }
    }

    getInfoWard({ district, wardCode }) {
        return new Promise(async resolve => {
            try {
                let listWards = [];
                let wardArr = [];
                
                let filePath = path.resolve(__dirname, `../constants/wards/${district}.json`);
                await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                    if (!err) {
                        listWards = JSON.parse(data);
                        listWards = Object.entries(listWards);
                        for (let ward of listWards){
                            if ( ward[1].code == wardCode ){
                                wardArr = ward;
                                break;
                            }
                        }
                        return resolve({ error: false, data: wardArr });
                    } else {
                        return resolve({ error: true, message: "not_found_ward" });
                    }
                })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoAddressByDistrictWardCity({ city, district, ward }) {
        return new Promise(async resolve => {
            try {
                const cityInfo     = this.getInfoProvince({ provinceCode: city });
				const districtInfo = this.getInfoDistrict({ districtCode: district });
				const wardInfo     = await this.getInfoWard({ district: district, wardCode: ward });
                if (cityInfo.error) {
                    return resolve(cityInfo);
                }
                if (districtInfo.error) {
                    return resolve(districtInfo);
                }
                if (wardInfo.error) {
                    return resolve(wardInfo);
                }
                
                return resolve({ 
                    error: false, 
                    data: {
                        cityInfo,
                        districtInfo,
                        wardInfo,
                    } 
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    } 

    //========================================================================
    //===========================GIAO HÀNG NHANH==============================
    //========================================================================

    getListProvinceGHN({  }) {
        return new Promise(async resolve => {
            try {
                var options = {
                    'method': 'GET',
                    'url': `${DOMAIN_GHN}/shiip/public-api/master-data/province`,
                    'headers': {
                        'Content-Type': 'application/json',
                        'Token': TOKEN_GHN
                    }
                };
                request(options, function (error, response) {
                    if (error) throw new Error(error);
                    console.log(response.body);
                    return resolve({ error: false, data: response.body });
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoProvinceGHN({ cityName }) {
        return new Promise(async resolve => {
            try {
                var options = {
                    'method': 'GET',
                    'url': `${DOMAIN_GHN}/shiip/public-api/master-data/province`,
                    'headers': {
                        'Content-Type': 'application/json',
                        'Token': TOKEN_GHN
                    }
                };
                request(options, function (error, response) {
                    if (error) throw new Error(error);

                    let infoCity = {};
                    let listCityGHN = JSON.parse(response.body);
                    if (listCityGHN.code == 200) {
                        listCityGHN.data && listCityGHN.data.length && listCityGHN.data.map((item, index) => {
                            if (item.NameExtension.includes(cityName)) {
                                infoCity = item;
                            }
                        });
                    }
                    return resolve({ error: false, data: infoCity });
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoDistrictGHN({ cityID, districtName }) {
        return new Promise(async resolve => {
            try {
                var options = {
                'method': 'GET',
                'url': `${DOMAIN_GHN}/shiip/public-api/master-data/district`,
                'headers': {
                    'token': TOKEN_GHN,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "province_id": cityID
                })

                };
                request(options, function (error, response) {
                    if (error) throw new Error(error);

                    let infoDistrict    = {};
                    let listDistrictGHN = JSON.parse(response.body);
                    if (listDistrictGHN.code == 200) {
                        listDistrictGHN.data && listDistrictGHN.data.length && listDistrictGHN.data.map((item, index) => {
                            if (item.NameExtension.includes(districtName)) {
                                infoDistrict = item;
                            }
                        });
                    }
                    return resolve({ error: false, data: infoDistrict });
                });

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoWardGHN({ districtID, wardName }) {
        return new Promise(async resolve => {
            try {
                var options = {
                'method': 'GET',
                'url': `${DOMAIN_GHN}/shiip/public-api/master-data/ward`,
                'headers': {
                    'token': TOKEN_GHN,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "district_id": districtID
                })

                };
                request(options, function (error, response) {
                    if (error) throw new Error(error);

                    let infoWard    = {};
                    let listWardGHN = JSON.parse(response.body);
                    if (listWardGHN.code == 200) {
                        listWardGHN.data && listWardGHN.data.length && listWardGHN.data.map((item, index) => {
                            console.log({
                                NameExtension: item.NameExtension,
                                wardName
                            });
                            if (item.NameExtension.includes(wardName)) {
                                infoWard = item;
                            }
                        });
                    }
                    return resolve({ error: false, data: infoWard });
                });

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoAddressGHN({ cityName, districtName, wardName }) {
        return new Promise(async resolve => {
            try {
                let infoCity = await this.getInfoProvinceGHN({ cityName: cityName });
                if (infoCity.error) {
                    return resolve(infoCity);
                }
                let infoDistrict = await this.getInfoDistrictGHN({ cityID: infoCity.data.ProvinceID, districtName })
                if (infoDistrict.error) {
                    return resolve(infoDistrict);
                }
               
                let infoWard = await this.getInfoWardGHN({ districtID: infoDistrict.data.DistrictID, wardName })
                if (infoWard.error) {
                    return resolve(infoWard);
                }

                return resolve({ error: false, 
                    data: { 
                        infoCity:     infoCity.data, 
                        infoDistrict: infoDistrict.data, 
                        infoWard:     infoWard.data, 
                    } 
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}
exports.MODEL = new Model;
