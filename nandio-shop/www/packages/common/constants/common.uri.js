const BASE_ROUTE = '/admin';

const CF_ROUTINGS_COMMON = {
    HOME: `${BASE_ROUTE}/home`,
    LOGIN: `/login`,
    LOGOUT: `/logout`,
    
    LIST_PROVINCES: `/list-provinces`,
    LIST_DISTRICTS: `/list-districts/:province`,
    LIST_WARDS: `/list-wards/:district`,

    LIST_PROVINCES_GHN: `/list-provinces-ghn`,
    LIST_DISTRICTS_GHN: `/list-districts-ghn/:province`,
    LIST_WARDS_GHN:     `/list-wards-ghn/:district`,


    LIST_STORE_GHN: `/list-store-ghn`,
    
    WMS_PUSH_QUEUE: `/wms/queue/push`,
  
   
    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_COMMON = CF_ROUTINGS_COMMON;