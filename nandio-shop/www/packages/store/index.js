const STORE_MODEL    = require('./models/store').MODEL;
const STORE_COLL     = require('./databases/store-coll');
const STORE_ROUTES   = require('./apis/store');

module.exports = {
    STORE_ROUTES,
    STORE_COLL,
    STORE_MODEL,
}