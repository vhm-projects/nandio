const BASE_ROUTE = '/store';
const BASE_ROUTE_AMIND = '/admin';
const API = '/api';

const CF_ROUTINGS_STORE = {
	ADD_STORE: `${BASE_ROUTE}/add-store`,
	REMOVE_STORE: `${BASE_ROUTE}/remove-store`,
	UPDATE_STORE: `${BASE_ROUTE}/update-store`,

	LIST_STORE: `${BASE_ROUTE}/list-store`,
	LIST_STORE_API: `${API}/list-store`,
	LIST_STORE_ADMIN: `${BASE_ROUTE_AMIND}/list-store`,
	LIST_STORE_PAGINATION: `${BASE_ROUTE_AMIND}/list-store-pagination`,
	LIST_STORE_FILTER: `${API}/list-store-filter`,
	INFO_STORE: `${BASE_ROUTE}/info-store`,
	INFO_STORE_FOR_ADMIN: `${BASE_ROUTE}/info-store-for-admin`,


	// API AGENCY
	ADD_AGENCY: `/agency/add-agency`,
	LIST_AGENCY: `/agency/list-agency`,
	UPDATE_AGENCY: `/agency/update-agency/:agencyID`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_STORE = CF_ROUTINGS_STORE;
