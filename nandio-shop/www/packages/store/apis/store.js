"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { CF_ROUTINGS_STORE } 			    = require('../constants/store.uri');

/**
 * MODELS
 */
const COMMON_MODEL    = require("../../common/models/common").MODEL;
const STORE_MODEL     = require('../models/store').MODEL;
const BRAND_MODEL     = require("../../brand/models/brand").MODEL;
const AGENCY_MODEL    = require('../models/agency').MODEL;
module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ====================== ************************ ================================
             * ======================    QUẢN LÝ CỬA HÀNG    ================================
             * ====================== ************************ ================================
             */

            /**
             * View: Danh sách cửa hàng admin
             * Date: 20/06/2021
             * Dev: depv
             */
            [CF_ROUTINGS_STORE.LIST_STORE_ADMIN]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'List Store - Nandio',
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_STORE.LIST_STORE_ADMIN,
                    inc: path.resolve(__dirname, '../views/list_store.ejs'),
                },
                methods: {
                    get: [ async function(req, res) {
                        let listStore = await STORE_MODEL.getList({ limit: 1000, page: 1 });
                        let listProvince   = COMMON_MODEL.listProvinceAll({  });
                        let listBrand      = await BRAND_MODEL.getListForEnduser({ status: 1, page: 1, limit: 1000 });
                        ChildRouter.renderToView(req, res, {
                            listStore: listStore.data,
                            listProvince: listProvince.data,
                            listBrand: listBrand.data.listBrand
                        });
					}],
                },
            },

            /**
             * View: Danh sách cửa hàng admin
             * Date: 20/06/2021
             * Dev: depv
             */
             [CF_ROUTINGS_STORE.LIST_STORE_PAGINATION]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function(req, res) {
                        let { start, length, search } = req.body;
                        let page = Number(start)/Number(length) + 1;

                        let listStore = await STORE_MODEL.getListOPagination({ 
                            page: Number(page), 
                            limit: Number(length), 
                            keyword: search.value 
                        });
                        res.json(listStore);
					}],
                },
            },
            /**
             * Function: Tạo Store (VIEW, API)
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_STORE.ADD_STORE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Add Store - Nandio',
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_STORE.ADD_STORE,
                    inc: path.resolve(__dirname, '../views/add_store.ejs'),
                },
                methods: {
                    get: [ async function(req, res) {
                        ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        let { name, avatar, address, city, district, ward, phone, latitude, longitude, openTime, closeTime, brands } = req.body
                        let infoAfterInsert = await STORE_MODEL.insert({ name, avatar, address, city: city, district: district, ward: ward, phone, latitude, longitude, openTime, closeTime, brands });
                        res.json(infoAfterInsert);
                    }]
                },
            },
            /**
             * Function: Cập nhật Store (VIEW, API)
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_STORE.UPDATE_STORE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Update Store - Nandio',
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_STORE.UPDATE_STORE,
                    inc: path.resolve(__dirname, '../views/update_store.ejs')
                },
                methods: {
                    get: [ async function(req, res) {
                        let { storeID } = req.query;

                        let infoStore = await STORE_MODEL.getInfo({ storeID });
                        ChildRouter.renderToView(req, res, { infoStore: infoStore.data });
					}],
                    post: [ async function (req, res) {
                        let { storeID, name, avatar, address, city, district, ward, phone, latitude, longitude, openTime, closeTime, brands } = req.body
                        let infoAfterInsert = await STORE_MODEL.update({ storeID, name, avatar, address, city, district, ward, phone, latitude, longitude, openTime, closeTime, brands });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
             * Function: Xóa Store (API)
             * Date: 20/06/2021
             * Dev: VyPQ
             */
			 [CF_ROUTINGS_STORE.REMOVE_STORE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { storeID } = req.params;

                        const infoAfterDelete = await STORE_MODEL.delete({ storeID });
                        res.json(infoAfterDelete);
                    }]
                },
            },

			/**
             * Function: Danh sách Store (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_STORE.LIST_STORE_API]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
						const { page, limit } = req.query;
                        const listStore = await STORE_MODEL.getList({ page: +page, limit: +limit });
						res.json(listStore);
                    }]
                },
            },

			/**
             * Function: Danh sách Store theo bộ lọc (API)
             * Date: 02/07/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_STORE.LIST_STORE_FILTER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
						const { name, city, district, type, ward, lng, lat, minDistance, maxDistance, brand } = req.query;
                        const listStore = await STORE_MODEL.getListStoreNear({
							name, city, district, type, ward, lng, lat, minDistance: +minDistance, maxDistance: +maxDistance, brand
						});
						res.json(listStore);
                    }]
                },
            },

			/**
             * Function: Xem chi tiết Store (API)
             * Date: 02/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_STORE.INFO_STORE]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { storeID, lat, lng } = req.query;
                        // const infoStore = await STORE_MODEL.getInfo({ storeID });
                        const infoStore = await STORE_MODEL.getInfoStoreForEnduser({ storeID, lng, lat });
                        res.json(infoStore);
                    }]
                },
            },

            /**
             * Function: Xem chi tiết Store (API)
             * Date: 02/07/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_STORE.INFO_STORE_FOR_ADMIN]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { storeID } = req.query;
                        const infoStore = await STORE_MODEL.getInfo({ storeID });
                        res.json(infoStore);
                    }]
                },
            },


            /**
             * ====================== ************************ ================================
             * ======================    QUẢN LÝ CHI NHÁNH     ================================
             * ====================== ************************ ================================
             */

            /**
             * Function: Tạo chi nhánh (API)
             */
            [CF_ROUTINGS_STORE.ADD_AGENCY]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Nandio',
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_STORE.ADD_AGENCY,
                    inc: path.resolve(__dirname, '../views/add_agency.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        ChildRouter.renderToView(req, res, {});
                    }],
                    post: [ async function (req, res) {
                        const { 
                            storeCodeGHN, name, address, phone, email, 
                            city, cityName, district, districtName, ward, wardName, cc, cityNear
                        } = req.body;

                        const infoAgency = await AGENCY_MODEL.insert({ 
                            storeCodeGHN, name, address, phone, email, 
                            city, cityName, district, districtName, ward, wardName, cc, cityNear
                        });

                        res.json(infoAgency);
                    }],
                },
            },

            /**
             * Function: Tạo chi nhánh (API)
             */
             [CF_ROUTINGS_STORE.LIST_AGENCY]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Nandio',
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_STORE.LIST_AGENCY,
                    inc: path.resolve(__dirname, '../views/list_agency.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        let listAgency = await AGENCY_MODEL.getList({  })
                        ChildRouter.renderToView(req, res, { listAgency: listAgency.data || []  });
                    }],
                },
            },

            /**
             * Function: Cập nhật chi nhánh (API)
             */
             [CF_ROUTINGS_STORE.UPDATE_AGENCY]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Nandio',
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_STORE.UPDATE_AGENCY,
                    inc: path.resolve(__dirname, '../views/update_agency.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID } = req.params;
                        let infoAgency = await AGENCY_MODEL.getAgency({ 
                            agencyID
                        });

                        ChildRouter.renderToView(req, res, {
                            infoAgency: infoAgency.data
                        });
                    }],
                    post: [ async function (req, res) {
                        let { agencyID } = req.params;

                        const { 
                            storeCodeGHN, name, address, phone, email, 
                            city, cityName, district, districtName, ward, wardName, cc, cityNear
                        } = req.body;

                        const infoAgency = await AGENCY_MODEL.update({ 
                            agencyID,
                            storeCodeGHN, name, address, phone, email, 
                            city, cityName, district, districtName, ward, wardName, cc, cityNear
                        });

                        res.json(infoAgency);
                    }],
                },
            },

        }
    }
};
