"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');

const GeoSchema = new Schema({
    type: {
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        index: "2dsphere"
    }
});

/**
 * COLLECTION CỬA HÀNG CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('store', {
	storeCode:{
		type: String,
		unique: true
	}, //code (store_temp)
	name: {
		type: String,
		trim: true,
		require: true
	},//name (store_temp)
	// Danh sách thương hiệu
	brands: [{
		type: Schema.Types.ObjectId,
		ref: 'brand'
	}], //brandCode(store_temp)
	
	phone: String, //phone(store_temp)
	address: String,//address(store_temp)

	/**
	 * Thành phốs
	 */
	city: {
		type: String,
	},
	/**
	 * Quận
	 */
	district: {
		type: String,
	},
	/**
	 * Phường
	 */
	ward: {
		type: String,
	},

	//Vị trí
	location: GeoSchema,

	//Giờ mở cửa
	openTime: Date,
	//Giờ đóng cửa
	closeTime: Date,

	//Ảnh đại diện
	avatar: {
		type: Schema.Types.ObjectId,
		ref: 'image'
	},
	
	//Bộ sưu tập
	gallery: [{
		type: Schema.Types.ObjectId,
		ref: 'image'
	}],

	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},
	userCreate: {
		type: Schema.Types.ObjectId,
		ref: 'user'
	},
	userUpdate: {
		type: Schema.Types.ObjectId,
		ref: 'user'
	},

	employees: [{
        type: Schema.Types.ObjectId,
        ref: 'employee',
    }]

});
