"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');


/**
 * COLLECTION CỬA HÀNG CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('agency', {
	storeCodeGHN:{
		type: String,
	},
	name: {
		type: String,
		require: true
	},
	email: {
		type: String,
		require: true
	},

	cc: [{
		type: String,
	}],

	phone: String, 
	address: String,
	/**
	 * Thành phốs
	 */
	city: {
		type: String,
	},
	cityName: {
		type: String,
	},
	/**
	 * Thành phố/Tỉnh lân cận
	 */
	cityNear: [{
		city:     String,
		cityName: String,
	}],
	/**
	 * Quận
	 */
	district: {
		type: String,
	},
	districtName: {
		type: String,
	},
	/**
	 * Phường
	 */
	ward: {
		type: String,
	},
	wardName: {
		type: String,
	},
	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 2. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},
});
