"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');

const GeoSchema = new Schema({
    type: {
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        index: "2dsphere"
    }
});

/**
 * COLLECTION CỬA HÀNG CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('store_import', {
	code: String,
    name: String,
    address: String,
    province: String, // -> objectId city
    district: String, // -> objectId district
    precinct: String, // -> objectId ward
    // -> location
    lat: String, 
    lng: String,

    phone: String,
    brandCode: String, // -> objectId
});