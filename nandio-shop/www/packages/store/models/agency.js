"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { checkObjectIDs, checkPhoneNumber, checkEmail } 			= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const AGENCY_COLL  					= require('../databases/agency-coll');
/**
 * MODEL
 */
const COMMON_MODEL                  = require('../../common/models/common').MODEL;
const GIAO_HANG_NHANH_MODEL  		= require('../../giao_hang_nhanh/models/giao_hang_nhanh').MODEL;

class Model extends BaseModel {
    constructor() {
        super(AGENCY_COLL);
    }

	insert({ 
        storeCodeGHN, name, address, 
        city, cityName, district, districtName, ward, wardName,
        phone, email, cc, cityNear
    }) {
        return new Promise(async resolve => {
            try {
                let dataInsert = { };

                if(!storeCodeGHN)
                    return resolve({ error: true, message: 'Mã cửa hàng Giao hàng nhanh không hợp lệ' });

                if(!name)
                    return resolve({ error: true, message: 'Tên chi nhánh không hợp lệ' });
                
                if(!email)
                    return resolve({ error: true, message: 'Email chi nhánh không hợp lệ' });
                
                if(!checkEmail(email))
                    return resolve({ error: true, message: 'Email chi nhánh không đúng định dạng' });

                if(!phone)
                    return resolve({ error: true, message: 'Số điện thoại chi nhánh không hợp lệ' });

                if(!checkPhoneNumber(phone))
                    return resolve({ error: true, message: 'Số điện thoại chi nhánh không đúng định dạng' });

                if(!address){
                    return resolve({ error: true, message: 'Địa chỉ chi nhánh không hợp lệ' });
                }
                if(!city){
                    return resolve({ error: true, message: 'Tỉnh/Thành phố chi nhánh không hợp lệ' });
                }
                if(!district){
                    return resolve({ error: true, message: 'Quận/Huyện chi nhánh không hợp lệ' });
                }
                if(!ward){
                    return resolve({ error: true, message: 'Xã/Phường chi nhánh không hợp lệ' });
                }

                if (!cc || !cc.length) {
                    return resolve({ error: true, message: 'Email CC không hợp lệ' });
                }

                if (!cityNear || !cityNear.length) {
                    return resolve({ error: true, message: 'Tỉnh/Thành phố chuyển về chi nhánh không hợp lệ' });
                }

                if (cityNear) {
                    let listCity = cityNear.map(item => item.city);
                    console.log({
                        listCity
                    });
                    let listAgency = await AGENCY_COLL.find({ });

                    let error   = false;
                    let message = '';
                    listAgency.map(agency => {
                        agency.cityNear.map(city => {
                            if (listCity.includes(city.city)) {
                                error   = true;
                                message = `${city.cityName} đã tồn tại ở chi nhánh khác`;
                            }
                        });
                    });
                    if (error) {
                        return resolve({ error: true, message: message });
                    }
                }
              
                dataInsert = {
                    storeCodeGHN, name, address, 
                    city, cityName, district, districtName, ward, wardName,
                    phone, email, cc: cc.split(','), cityNear
                };
                console.log({
                    dataInsert
                });
                let infoAgencyInsert = await this.insertData(dataInsert);
                if(!infoAgencyInsert)
                    return resolve({ error: true, message: 'Không thể thêm chi nhánh' });

                return resolve({ error: false, data: infoAgencyInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
	getList({ 
      
    }) {
        return new Promise(async resolve => {
            try {
                let listAgency = await AGENCY_COLL.find({  });

                if(!listAgency)
                    return resolve({ error: true, message: 'Không thể lấy danh sách chi nhánh' });

                return resolve({ error: false, data: listAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ 
        wardName, districtName, cityName
    }) {
        return new Promise(async resolve => {
            try {
                let infoAddressGHN = await GIAO_HANG_NHANH_MODEL.getInfoAddressGHN({ 
                    cityName:     cityName,
                    districtName: districtName,
                    wardName:     wardName,
                });

                // console.log({
                //     infoAddressGHN
                // });
                let infoAgency = await AGENCY_COLL.findOne({ 
                    // city:
                    $or: [
                        {
                            'cityNear.city':  infoAddressGHN.data.infoCity.ProvinceID.toString(),
                        }, {
                            city:     infoAddressGHN.data.infoCity.ProvinceID.toString(),
                            // district: infoAddressGHN.data.infoDistrict.DistrictID.toString(),
                            // ward:     infoAddressGHN.data.infoWard.WardCode.toString(),
                        }
                    ]
                });

                if(!infoAgency)
                    return resolve({ error: true, message: 'Không thể lấy thông tin chi nhánh' });

                return resolve({ error: false, data: { infoAgency, infoAddressGHN } });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getAgency({ 
        agencyID
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(agencyID)) {
                    return resolve({ error: true, message: 'ID Chi nhánh không hợp lệ' });
                }
                let infoAgency = await AGENCY_COLL.findById(agencyID);

                if(!infoAgency)
                    return resolve({ error: true, message: 'Không thể lấy thông tin chi nhánh' });

                return resolve({ error: false, data: infoAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ 
        agencyID, storeCodeGHN, name, address, 
        city, cityName, district, districtName, ward, wardName,
        phone, email, cc, cityNear
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(agencyID)) {
                    return resolve({ error: true, message: 'ID Chi nhánh không hợp lệ' });
                }

                let dataUpdate = { };

                if(!storeCodeGHN)
                    return resolve({ error: true, message: 'Mã cửa hàng Giao hàng nhanh không hợp lệ' });

                if(!name)
                    return resolve({ error: true, message: 'Tên chi nhánh không hợp lệ' });
                
                if(!email)
                    return resolve({ error: true, message: 'Email chi nhánh không hợp lệ' });
                
                if(!checkEmail(email))
                    return resolve({ error: true, message: 'Email chi nhánh không đúng định dạng' });

                if(!phone)
                    return resolve({ error: true, message: 'Số điện thoại chi nhánh không hợp lệ' });

                if(!checkPhoneNumber(phone))
                    return resolve({ error: true, message: 'Số điện thoại chi nhánh không đúng định dạng' });

                if(!address){
                    return resolve({ error: true, message: 'Địa chỉ chi nhánh không hợp lệ' });
                }
                if(!city){
                    return resolve({ error: true, message: 'Tỉnh/Thành phố chi nhánh không hợp lệ' });
                }
                if(!district){
                    return resolve({ error: true, message: 'Quận/Huyện chi nhánh không hợp lệ' });
                }
                if(!ward){
                    return resolve({ error: true, message: 'Xã/Phường chi nhánh không hợp lệ' });
                }

                if (!cc || !cc.length) {
                    return resolve({ error: true, message: 'Email CC không hợp lệ' });
                }

                if (!cityNear || !cityNear.length) {
                    return resolve({ error: true, message: 'Tỉnh/Thành phố chuyển về chi nhánh không hợp lệ' });
                }

                if (cityNear) {
                    let listCity = cityNear.map(item => item.city);
                    console.log({
                        listCity
                    });
                    let listAgency = await AGENCY_COLL.find({ _id: { $nin: agencyID } });

                    let error   = false;
                    let message = '';
                    listAgency.map(agency => {
                        agency.cityNear.map(city => {
                            if (listCity.includes(city.city)) {
                                error   = true;
                                message = `${city.cityName} đã tồn tại ở chi nhánh khác`;
                            }
                        });
                    });
                    if (error) {
                        return resolve({ error: true, message: message });
                    }
                }
              
                dataUpdate = {
                    storeCodeGHN, name, address, 
                    city, cityName, district, districtName, ward, wardName,
                    phone, email, cc: cc.split(','), cityNear
                };
                console.log({
                    dataUpdate
                });
                let infoAgencyAfterUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, dataUpdate, { new: true });
                console.log({
                    infoAgencyAfterUpdate
                });
                if(!infoAgencyAfterUpdate)
                    return resolve({ error: true, message: 'Không thể cập nhật chi nhánh' });

                return resolve({ error: false, data: infoAgencyAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
