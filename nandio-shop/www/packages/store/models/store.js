"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { loadPathImage } 			= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const STORE_COLL  					= require('../databases/store-coll');
/**
 * MODEL
 */
const IMAGE_MODEL                   = require('../../image/models/image').MODEL;
const COMMON_MODEL                  = require('../../common/models/common').MODEL;

class Model extends BaseModel {
    constructor() {
        super(STORE_COLL);
    }

	insert({ name, avatar, address, city, district, ward, phone, latitude, longitude, openTime, closeTime, brands }) {
        return new Promise(async resolve => {
            try {
                if(!name)
                    return resolve({ error: true, message: 'params_invalid' });
                let dataInsert = { name, brands };
                if(address){
                    dataInsert.address = address;
                }
                if(city){
                    dataInsert.city = city;
                }
                if(district){
                    dataInsert.district = district;
                }
                if(ward){
                    dataInsert.ward = ward;
                }
                if(phone){
                    dataInsert.phone = phone;
                }

                if(latitude && longitude){
                    dataInsert.location = {
                        type: "point",
                        coordinates: [longitude, latitude]
                    }
                }

                if(openTime){
                    dataInsert.openTime = openTime;
                }

                if(closeTime){
                    dataInsert.closeTime = closeTime;
                }

                if(avatar){
                    let { name, size, path } = avatar;
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path
                    });
                    dataInsert.avatar = ObjectID(infoImageAfterInsert.data._id);
                }

                let infoStoreInsert = await this.insertData(dataInsert);

                if(!infoStoreInsert)
                    return resolve({ error: true, message: 'insert_brand_failed' });

                return resolve({ error: false, data: infoStoreInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ storeID, name, avatar, address, city, district, ward, phone, latitude, longitude, openTime, closeTime, brands }) {
        return new Promise(async resolve => {
            try {
                if(!name)
                    return resolve({ error: true, message: 'params_invalid' });

                let dataUpdate = { name, brands };
                if(address){
                    dataUpdate.address = address;
                }
                if(city){
                    dataUpdate.city = city;
                }
                if(district){
                    dataUpdate.district = district;
                }
                if(ward){
                    dataUpdate.ward = ward;
                }
                if(phone){
                    dataUpdate.phone = phone;
                }

                if(latitude && longitude){
                    dataUpdate.location = {
                        type: "point",
                        coordinates: [longitude, latitude]
                    }
                }

                if(openTime){
                    dataUpdate.openTime = openTime;
                }

                if(closeTime){
                    dataUpdate.closeTime = closeTime;
                }

                if(avatar){
                    let { name, size, path } = avatar;
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path
                    });
                    dataUpdate.avatar = ObjectID(infoImageAfterInsert.data._id);
                }

                let infoStoreInsert = await STORE_COLL.findByIdAndUpdate(storeID, dataUpdate, {new: true });
                if(!infoStoreInsert)
                    return resolve({ error: true, message: 'insert_brand_failed' });

                return resolve({ error: false, data: infoStoreInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	delete({ storeID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(storeID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterDelete = await STORE_COLL.findByIdAndUpdate(storeID, { status: 0 }, { new: true });
                if(!infoAfterDelete)
                    return resolve({ error: false, message: 'cannot_remove_store' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ storeID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(storeID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoStore = await STORE_COLL.findById(storeID).select("name avatar address city district ward phone location openTime closeTime brands")
                .populate({
                    path: "avatar",
                    select: "path"
                })
                if(!infoStore)
                    return resolve({ error: false, message: 'cannot_get_info_store' });
                let listCity       = COMMON_MODEL.listProvinceAll({});
                let listDistrict   = COMMON_MODEL.listDistrict({ province: infoStore.city });
                let listWard       = await COMMON_MODEL.listWard({ district: infoStore.district });
                return resolve({ error: false, data: { infoStore, listCity, listDistrict, listWard }});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getList({ page = 1, limit = 30 }){
		return new Promise(async resolve => {
			try {
				let listStore = await STORE_COLL
					.find({})
                    .select("name avatar city district ward phone location openTime closeTime brands")
                    .populate({
                        path: "avatar",
                        select: "path"
                    })
					.sort({ modifyAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
					.lean();

                if(!listStore)
                    return resolve({ error: true, message: 'cannot_get_list_store' });

                let totalItem = await STORE_COLL.count();
                let pages = Math.ceil(totalItem/limit);

				return resolve({ 
					error: false, 
					data: {
						listStore,
						currentPage: +page,
						totalPgae: +pages
					}
				});
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

    getListOPagination({ limit = 10, page = 1, keyword }){
		return new Promise(async resolve => {
			try {
                let dataFind = {};

                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    dataFind.$or = [
                        { name: new RegExp(key, 'i') },
                    ]
                }

                let skip = (page - 1) * limit;

				let listStore = await STORE_COLL
					.find({ ...dataFind })
                    .limit(limit * 1)
                    .skip(skip)
                    .select("name avatar city district ward phone location openTime closeTime brands")
                    .populate({
                        path: "avatar",
                        select: "path"
                    })
					.sort({ modifyAt: -1 })
					.lean();

                if(!listStore)
                    return resolve({ error: true, message: 'cannot_get_list_store' });

                let count = await STORE_COLL.count({ ...dataFind });

                let arrStore = [];
                listStore && listStore.length && listStore.forEach((item, index) => {
                    let indexChange = skip + index + 1;
                    let name   = item.name ? item.name : '';
                    let avatar = (item.avatar && item.avatar.path) ? 
                        `
                            <a href="${loadPathImage(item.avatar.path)}" target="_blank">
                                <img class="img-fluid img-thumbnail rounded" src="${loadPathImage(item.avatar.path)}" alt="">
                            </a>
                        ` : '';
                    let phone  = item.phone ? item.phone : '';
                    let action = `
                        <span style="cursor: pointer;" class="btnUpdateStore" _storeID="${item._id}">
                            <i class="fa fa-edit"></i> Chỉnh sửa
                        </span>
                    `;
                    arrStore = [
                        ...arrStore,
                        {
                            indexChange,
                            name,
                            avatar,
                            phone,
                            action,
                        }
                    ]
                })

				return resolve({ 
					error: false, 
					data: arrStore,
                    recordsTotal: count, 
                    recordsFiltered: count
				});
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}


    getListStoreNear({ name, city, district, type, ward, lng, lat, minDistance = 0, maxDistance = 700000, brand }) {
        return new Promise(async resolve => {
            try {
				let conditionObj  = { status: 1 };
				let listStoreNear = [];

                if(name){
                    let keyword = name.split(" ");
                    keyword = '.*' + keyword.join(".*") + '.*';
                    conditionObj.name = new RegExp(keyword, 'i');
                }
				city	 && (conditionObj.city 		= city);
				district && (conditionObj.district 	= district);
				ward 	 && (conditionObj.ward 		= ward);
				brand 	 && (conditionObj.brands 	= { $in: [ObjectID(brand)] });
                if (type && type == 'HAVE_NA_PG') {
                    conditionObj['employees.0'] = {
                        $exists: true
                    }
                }

                if (type && type == 'NOT_HAVE_NA_PG') {
                    conditionObj['employees.0'] = {
                        $exists: false
                    }
                }
               
                // console.log({ lat, lng, minDistance, maxDistance, conditionObj });
				if(!lng || !lat){
                    listStoreNear = await STORE_COLL.find(conditionObj)
                        // .select("address name avatar city district ward phone location openTime closeTime brands")
                        .select("_id location employees")
                        // .limit(50);
                } else{
                    listStoreNear = await STORE_COLL.aggregate([
                        {
                            $geoNear: {
                                near: { type: "Point", coordinates: [parseFloat(lng), parseFloat(lat)] },
                                maxDistance: Number(maxDistance),
                                minDistance: Number(minDistance),
                                distanceMultiplier: 0.001, // Returns km
                                distanceField: "dist.calculated",
                                includeLocs: "dist.location", // Returns distance
                                spherical: true
                            }
                        },
                        { 
                            $match: conditionObj
                        },
                        {
                            $project: { _id: 1, location: 1, employees: 1 } 
                        }
                        // { $limit : 50 }
                    ]);
				}
                console.log({
                    listStoreNear
                });
                if(!listStoreNear)
                    return resolve({ error: true, message: 'not_found_get_list_store_near' });

                let listStoreNearPopulate = await STORE_COLL.populate(listStoreNear, {
                    path: "avatar",
                    select: "path"
                })

                return resolve({ error: false, data: listStoreNearPopulate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }


    getInfoStoreForEnduser({ storeID, lng, lat,  minDistance = 0, maxDistance = 1000000000000 }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(storeID))
                    return resolve({ error: true, message: "params_invalid" })

				let conditionObj  = { _id: ObjectID(storeID), status: 1 };
				let infoStore = [];

                // console.log({ lat, lng, minDistance, maxDistance, conditionObj });
				if(!lng || !lat){
                    infoStore = await STORE_COLL.findOne(conditionObj)
                        // .select("address name avatar city district ward phone location openTime closeTime brands")
                        // .select("_id location")
                        // .limit(50);
                } else{
                    infoStore = await STORE_COLL.aggregate([
                        {
                            $geoNear: {
                                near: { type: "Point", coordinates: [parseFloat(lng), parseFloat(lat)] },
                                maxDistance: Number(maxDistance),
                                minDistance: Number(minDistance),
                                distanceMultiplier: 0.001, // Returns km
                                distanceField: "dist.calculated",
                                includeLocs: "dist.location", // Returns distance
                                spherical: true
                            }
                        },
                        { 
                            $match: conditionObj
                        },
                        // { $limit : 50 }
                    ]);
				}

                if(!infoStore)
                    return resolve({ error: true, message: 'not_found_get_list_store_near' });

                let infoStorePopulate = await STORE_COLL.populate(infoStore, {
                    path: "avatar",
                    select: "path"
                })

                return resolve({ error: false, data: infoStorePopulate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
