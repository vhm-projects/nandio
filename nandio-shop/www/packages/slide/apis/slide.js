"use strict";

/**
 * EXTERNAL PACKAGE
 */

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_SLIDE } 						= require('../constants/slide.uri');
const path						                    = require('path');
const { uploadSingle }                              = require('../../../config/cf_helpers_multer');
const { IMAGE_MODEL  }                              = require("../../image");
/**
 * MODELS
 */
const SLIDE_MODEL                                   = require('../models/slide').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            [CF_ROUTINGS_SLIDE.ADD_SLIDE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ uploadSingle, async function (req, res) {
                        let  { _id: userID }        = req.user;
                        let  { title, description, link, image } =  JSON.parse(req.body.data);
                        //Thêm thành công
                        // console.log({ title, description, link, image });
                        if(image){
                            let imageAfterInsert = await IMAGE_MODEL.insert({ name: image, path: image});
                            if(imageAfterInsert.error)
                                return res.json({ error: true, message: "image_invalid" });
                            
                            let resultAdd  = await SLIDE_MODEL.insert({ image: imageAfterInsert.data._id, title, description, link, userID  } );
                            return res.json(resultAdd);
                        }else{
                            return res.json({error : true});
                        }
                    }]
                },
            },

             /**
             * Function: Danh sách Slide
             * Date: 01/04/2021
             * Dev: VinhNH
            */
            [CF_ROUTINGS_SLIDE.LIST_SLIDE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    view: 'index.ejs',
					inc: path.resolve(__dirname, '../views/list_slide.ejs'),
                    title: 'NANDIO-SLIDE',
                    type: 'view',
                    code: CF_ROUTINGS_SLIDE.LIST_SLIDE
                },
                methods: {
                    get: [ async function (req, res) {
                        let listSlide  = await SLIDE_MODEL.getList({ page : 1 });
                        return ChildRouter.renderToView(req, res, {
                            listSlide : listSlide.data, 
                        });
                     }]
                },
            },
            
            /**
             * Function: Cập nhật trạng thái hoạt động Slide
             * Date: 01/04/2021
             * Dev: VinhNH
            */
            [CF_ROUTINGS_SLIDE.UPDATE_STATUS_SLIDE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async  function (req, res) {
                        let { _id: userID } = req.user;
                        let  { slideID, status } = req.body;
                        let resultUpdate  = await SLIDE_MODEL.update({ slideID, userUpdateID: userID , status  });
                        return res.json(resultUpdate);
                    }]
                },
            },
    
            /**
             * Function: Cập nhật trạng thái hoạt động Slide
             * Date: 01/04/2021
             * Dev: VinhNH
            */
             [CF_ROUTINGS_SLIDE.INFO_SLIDE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [async  function (req, res) {
                        let  { slideID } = req.params;
                        let infoSlide  = await SLIDE_MODEL.getInfoSlide({ slideID });
                        return res.json(infoSlide);
                    }]
                },
            },


            /**
             * Function: Sửa thông tin Slide
             * Date: 01/04/2021
             * Dev: VinhNH
            */
            [CF_ROUTINGS_SLIDE.UPDATE_SLIDE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ uploadSingle, async function (req, res) {
                        let  { _id: userID }        = req.user;
                        let  { slideID, title, description, link, image, imageDelete } =  JSON.parse(req.body.data);

                        //Thêm thành công
                        if(image){         
                            let imageAfterInsert = await IMAGE_MODEL.insert({ name: image, path: image});
                            await IMAGE_MODEL.delete({ imageID: imageDelete });

                            if(imageAfterInsert.error)
                                return res.json({ error: true, message: "image_invalid" });

                            let resultUpdate  = await SLIDE_MODEL.update({ slideID, image: imageAfterInsert.data._id, title, description, link, userUpdateID: userID } );
                           
                            return res.json(resultUpdate);
                        }else{
                            let resultUpdate  = await SLIDE_MODEL.update({ slideID, title, description, link, userUpdateID: userID  } );
                            return res.json(resultUpdate);
                        }
                    }]
                },
            },


             /**
             * Function: Sửa thông tin Slide
             * Date: 01/04/2021
             * Dev: VinhNH
            */
            [CF_ROUTINGS_SLIDE.REMOVE_SLIDE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let  { _id: userID }        = req.user;
                        let  { slideID } =  req.params;
                        //Thêm thành công aaaaaa
                        let resultUpdate  = await SLIDE_MODEL.removeSlide({ slideID, userID });
                        return res.json(resultUpdate);
                    }]
                },
            },
           
        }
    }
};
