"use strict";

/**
 * EXTERNAL PAKCAGE
 */
let fs                                    = require("fs");
let path                                  = require('path');
const ObjectID                            = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel                           = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const SLIDE_COLL                          = require('../databases/slide-coll');
const { IMAGE_COLL }                      = require('../../image');
const { USER_COLL }                       = require('../../users');

class Model extends BaseModel {
    constructor() {
        super(SLIDE_COLL);
    }

    // Thêm tin nhắn mới
    insert({ image, title, description, link, userID }) { 
        return new Promise(async (resolve) => {    
            try {
                if(!ObjectID.isValid(userID) || !title|| !image){
                    return resolve({ error: true, message: 'param_invalid' });  
                }
                let resultInsert = await this.insertData({ image, title, description, link, userCreate: userID });
                if(!resultInsert)
                    return resolve({ error: true, message: 'invalid_object_id2' });
                
                return resolve({ error: false, data : resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    // update mẫu tin  (cập nhật trạng thái ngưng hoạt động,...)
    update({ slideID, image, title, description, link, userUpdateID, status }) {
        return new Promise(async (resolve) => {
            try {
                if( !ObjectID.isValid(userUpdateID)){
                    return resolve({ error: true, message: 'invalid_object_id' });
                }   

                let dataQuery = {};
            
                if(image){
                    dataQuery.image = image;
                    // let imageOld = isExitSlide.image;

                    // if(imageOld){
                    //     let imageOldID = imageOld._id;
                    //     let name = imageOld.name
                    //     await IMAGE_COLL.findByIdAndRemove(imageOldID);

                    //     // Xoá file trong thư Mục
                    //     let  _pathRemove = path.resolve(__dirname, `../../files/db/${name}`);
                    //     await fs.unlink( _pathRemove, (err) => {
                    //         if (err) {
                    //             console.error(err)
                    //             return
                    //         }
                    //     })
                    // }
                }
    
                if(title){
                    dataQuery.title = title
                }
    
                if(description){
                    dataQuery.description = description
                }
    
                if(link){
                    dataQuery.link = link
                }
    
                if(userUpdateID){
                    dataQuery.userUpdate = userUpdateID
                }
                
                if(status || status == 0){
                    dataQuery.status = status
                }

                    // if ( imageDelete ){
                    //     dataQuery.$pull = { image: imageDelete }
                    // }
                let resultUpdate = await SLIDE_COLL.findByIdAndUpdate(slideID, dataQuery );

                if(!resultUpdate){
                    return resolve({ error: true, message: 'cant_update' });
                }
                return resolve({ error: false, data : resultUpdate, message :'update_success' });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // tìm mẫu tin của 1 userID
    getDataWithUserID({ page, userID }){
        return new Promise(async (resolve) => {
            try {
                let perPage = 30;
                if(!page || !Number(page) || page == 'undefined' || page == 'null'){
                    page = 1;
                }
                if(!ObjectID.isValid(userID)){
                    return resolve({ error: true, message: 'invalid_object_id' });
                }
                let resultSearch = await SLIDE_COLL.find({ userID })
                    .skip((page * perPage) - perPage)
                    .limit(perPage)
                    .sort({ createAt: -1 })
                if(!resultSearch){
                    return resolve({ error: true, message: 'cant_update' });
                }
                let count  = await SLIDE_COLL.count();
                return resolve({ error: false, data : resultSearch,page, allPage : count/perPage  });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    getInfoSlide({ slideID }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {
              
                let resultFind =  await SLIDE_COLL.findById(slideID).populate({
                    path : 'image'
                })
                if(!resultFind){
                    return resolve({ error: true, message: 'invalid_object_id2' });
                }
                return resolve({ error: false, data : resultFind });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
    //Admin lấy tất cả  mẫu tin
    getList({ page, adminID }){
        return new Promise(async (resolve) => {
            try {
                let perPage = 30;
                if(!page || !Number(page) || page == 'undefined' || page == 'null'){
                    page = 1;
                }

                // Nếu là user bình thường thì lấy thông tin cơ bản và những chỉ lấy slide đang hoạt động
                let ObjectSearch = {
                }

                //check is Admin  nếu là admin thì lấy tất cả mẫu tin
                if(adminID){
                    if(ObjectID.isValid(userID)){
                       let infoAdmin = await  USER_COLL.findOne({ _id : adminID, status : 1 });
                       if(infoAdmin && infoAdmin.role == 1){
                            ObjectSearch = {};
                       }
                    }
                }

                // 
                let resultSearch = await SLIDE_COLL.find(ObjectSearch)
                .populate({
                    path : 'image'
                })
                    // .skip((page * perPage) - perPage)
                    // .limit(perPage)
                    .sort({ createAt: 1 })
                // console.log({ resultSearch });
                if(!resultSearch){
                    return resolve({ error: true, message: 'cant_update' });
                }
                let count  = await SLIDE_COLL.count();
                return resolve({ error: false, data : resultSearch,page, allPage : count/perPage, perPage  });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // Thêm tin nhắn mới
    removeSlide({ userID, slideID }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {
                if(!ObjectID.isValid(userID) || !ObjectID.isValid(slideID)){
                    return resolve({ error: true, message: 'invalid_object_id' });
                }
                // Kiểm tra quyền 
                // let isExitAdmin =  await USER_COLL.findOne({ _id : userID , status : 0 });
                // if(!isExitAdmin){
                //     return resolve({ error: true, message: 'not_accept_user' });
                // }
                let isExistSlide = await SLIDE_COLL.findById(slideID).populate('image')
                if(!isExistSlide){
                    return resolve({ error: true, message: 'cant_delet' });
                }
                let imageOld = isExistSlide.image;
                if(imageOld){
                    let imageOldID = imageOld._id;
                    let name = imageOld.name
                    let resultRemove = await IMAGE_COLL.findByIdAndRemove(imageOldID);
                    // Xoá file trong thư Mục
                    // let  _pathRemove = path.resolve(__dirname, `../../../../${imageOld.path}`);
                    // await fs.unlink( _pathRemove, (err) => {
                    //     if (err) {
                    //         console.error(err)
                    //         return
                    //     }
                    // })
                }
                let resultRemove = await SLIDE_COLL.findByIdAndRemove(slideID)
                if(!resultRemove){
                    return resolve({ error: true, message: 'cant_delet' });
                }
                return resolve({ error: false, message: 'delet_success', data: resultRemove });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
