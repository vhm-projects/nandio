const SLIDE_MODEL 		= require('./models/slide').MODEL;
const SLIDE_COLL  		= require('./databases/slide-coll');
const SLIDE_ROUTES       = require('./apis/slide');

module.exports = {
    SLIDE_ROUTES,
    SLIDE_COLL,
    SLIDE_MODEL
}
