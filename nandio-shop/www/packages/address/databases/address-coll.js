"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION ĐỊA CHỈ CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('address', {
	customer: {
		type: Schema.Types.ObjectId,
		ref: 'customer'
	},
	fullname: {
		type: String,
		trim: true,
		required: true
	},
	phone: {
		type: String,
		required: true,
	},
	/**
	 * Thành phố
	 */
	city: {
		type: String,
	},
	/**
	 * Quận
	 */
	district: {
		type: String,
	},
	/**
	 * Phường
	 */
	ward: {
		type: String,
	},
	address: {
		type: String
	},

	/**
	 * Loại địa chỉ:
	 * 0: Nhà riêng
	 * 1: Nơi làm việc
	 */
	type: {
		type: Number,
		default: 0
	},
	// Địa chỉ mặc định
	isDefault: {
		type: Boolean,
		default: false
	},
	/**
	 * Trạng thái hoạt động
	 * 1: Hoạt động
	 * 2: Không hoạt động
	 */
	stage: {
		type: Number,
		default: 1
	}
	
});
