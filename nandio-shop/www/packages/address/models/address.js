"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGES
 */
const COMMON_MODEL                  = require('../../common/models/common').MODEL;

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const ADDRESS_COLL  				= require('../databases/address-coll');


class Model extends BaseModel {
    constructor() {
        super(ADDRESS_COLL);
        this.STAGE_ACTIVE = 1;
    }

	insert({ customerID, fullname, phone, city, district, ward, address, type, isDefault }) {
        return new Promise(async resolve => {
            try {
				if(!ObjectID.isValid(customerID))
					return resolve({ error: true, message: 'params_invalid' });

                if(!fullname || !phone)
                    return resolve({ error: true, message: 'params_invalid' });

                if(![0,1].includes(+type)) 
                    return resolve({ error: true, message: 'type_address_invalid' });

                let infoAfterInsert = await this.insertData({
					customer: customerID, fullname, phone, city, district, ward, address, type, isDefault
				});
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_address_failed' });

				if(isDefault){
					await ADDRESS_COLL.updateMany({ 
						_id: { $nin: [infoAfterInsert._id] },
						isDefault: true
					}, { 
						$set: { isDefault: false } 
					});
				}

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	update({ addressID, customerID, fullname, phone, city, district, ward, address, type, isDefault }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(addressID) || !ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });

				if(![0,1].includes(+type)) 
                    return resolve({ error: true, message: 'type_address_invalid' });

                let dataUpdate = { type };
                fullname    && (dataUpdate.fullname = fullname);
                phone  		&& (dataUpdate.phone 	= phone);
                city       	&& (dataUpdate.city 	= city);
                district    && (dataUpdate.district = district);   
                ward    	&& (dataUpdate.ward 	= ward);   
                address    	&& (dataUpdate.address 	= address);
                isDefault   && (dataUpdate.isDefault    = isDefault);

                let infoAfterUpdate = await ADDRESS_COLL.findOneAndUpdate({
					_id: addressID,
					customer: customerID
				}, dataUpdate, { new: true });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'update_address_failed_or_token_invalid' });

                if(isDefault){
                    await ADDRESS_COLL.updateMany({ 
                        _id: { $nin: [infoAfterUpdate._id] },
                        isDefault: true
                    }, { 
                        $set: { isDefault: false } 
                    });
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	updateDefault({ customerID, addressID }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(addressID) || !ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });

				let infoAfterUpdateDefault = await ADDRESS_COLL.findOneAndUpdate({
					_id: addressID,
					customer: customerID
				}, {
					$set: { isDefault: true }
				}, { new: true });

				if(!infoAfterUpdateDefault)
					return resolve({ error: true, message: 'update_default_address_failed' });

				await ADDRESS_COLL.updateMany({ 
					_id: { $nin: [infoAfterUpdateDefault._id] },
					customer: customerID,
					isDefault: true
				}, { 
					$set: { isDefault: false } 
				});

				return resolve({ error: false, data: infoAfterUpdateDefault });
			} catch (error) {
                return resolve({ error: true, message: error.message });
			}
		})
	}

    delete({ addressID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(addressID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterDelete = await ADDRESS_COLL.findByIdAndUpdate(addressID, {
                    stage: 2
                }, { new: true });
                if(!infoAfterDelete)
                    return resolve({ error: true, message: 'cannot_delete_address' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ addressID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(addressID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAddress = await ADDRESS_COLL.findById(addressID).select("type isDefault customer fullname phone city district ward address");
                if(!infoAddress)
                    return resolve({ error: true, message: 'cannot_get_info_address' });

                return resolve({ error: false, data: infoAddress });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListByCustomer({ customerID }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_customerID_invalid' });

				let listAddress = await ADDRESS_COLL
                    .find({ 
                        customer: customerID, 
                        stage: this.STAGE_ACTIVE
                    }, { stage: 0, modifyAt: 0, createAt: 0, __v: 0 })
                    .sort({ createAt: -1 })
                    .lean();

                let listAddressAdditional = [];
                for (const address of listAddress) {
                    const { city, district, ward } = address;

                    const cityInfo = COMMON_MODEL.getInfoProvince({ provinceCode: city });
                    const districtInfo = COMMON_MODEL.getInfoDistrict({ districtCode: district });
                    const wardInfo = await COMMON_MODEL.getInfoWard({ district: district, wardCode: ward });

                    listAddressAdditional[listAddressAdditional.length] = {
                        ...address,
                        cityText: cityInfo.data && cityInfo.data[1]?.name_with_type,
                        districtText: districtInfo.data && districtInfo.data[1]?.name_with_type,
                        wardText: wardInfo.data && wardInfo.data[1]?.name_with_type
                    }
                }

                if(!listAddressAdditional)
                    return resolve({ error: true, message: 'cannot_get_list_address' });

				return resolve({ error: false, data: listAddressAdditional });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

    getInfoAddressDefault({ customerID }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });
				let infoAddess = await ADDRESS_COLL.findOne({ isDefault: true, customer: customerID, stage: this.STAGE_ACTIVE })
                                .select("fullname phone address city district ward")

                if(!infoAddess)
                    return resolve({ error: true, message: 'cannot_get_info_address' });

				return resolve({ error: false, data: infoAddess });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

}

exports.MODEL = new Model;
