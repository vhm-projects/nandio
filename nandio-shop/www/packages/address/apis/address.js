"use strict";

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { CF_ROUTINGS_ADDRESS } 			    = require('../constants/address.uri');

/**
 * MODELS
 */
const ADDRESS_MODEL = require('../models/address').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ====================== ************************ ================================
             * ======================    QUẢN LÝ ĐỊA CHỈ   	 ================================
             * ====================== ************************ ================================
             */

            /**
             * Function: Add address (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ADDRESS.ADD_ADDRESS]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: customerID } = req.customer;
                        const { fullname, phone, city, district, ward, address, type, isDefault } = req.body;
                        const infoBrandInsert = await ADDRESS_MODEL.insert({ 
							customerID, fullname, phone, city, district, ward, address, type, isDefault
						 });
                        res.json(infoBrandInsert);
                    }]
                },
            },

            /**
             * Function: Update address (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ADDRESS.UPDATE_ADDRESS]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: customerID } = req.customer;
                        const { addressID, fullname, phone, city, district, ward, address, type, isDefault } = req.body;

                        const infoAfterUpdate = await ADDRESS_MODEL.update({
							addressID, customerID, fullname, phone, city, district, ward, address, type, isDefault
						});
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
             * Function: Update address to default (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_ADDRESS.UPDATE_DEFAULT_ADDRESS]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { _id: customerID } = req.customer;
                        const { addressID } = req.query;

                        const infoAfterUpdate = await ADDRESS_MODEL.updateDefault({ customerID, addressID });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Delete address (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_ADDRESS.DELETE_ADDRESS]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { addressID } = req.query;
                        const infoAfterDelete = await ADDRESS_MODEL.delete({ addressID });
                        res.json(infoAfterDelete);
                    }]
                },
            },
			
			/**
             * Function: Info address (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ADDRESS.INFO_ADDRESS]: {
				config: {
					auth: [ roles.role.customer.bin ],
					type: 'json',
				},
				methods: {
					get: [ async function (req, res) {
						const { addressID } = req.query;
						const infoAddress = await ADDRESS_MODEL.getInfo({ addressID });
						res.json(infoAddress);
					}]
				},
			},

			/**
             * Function: List address by customer (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ADDRESS.LIST_ADDRESS_BY_CUSTOMER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
						const { _id: customerID } = req.customer;
                        const listAddress = await ADDRESS_MODEL.getListByCustomer({ customerID });
						res.json(listAddress);
                    }]
                },
            },

            

        }
    }
};
