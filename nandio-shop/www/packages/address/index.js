const ADDRESS_MODEL    = require('./models/address').MODEL;
const ADDRESS_COLL     = require('./databases/address-coll');
const ADDRESS_ROUTES   = require('./apis/address');

module.exports = {
    ADDRESS_ROUTES,
    ADDRESS_COLL,
    ADDRESS_MODEL,
}