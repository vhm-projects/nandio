const BASE_ROUTE = '/address';

const CF_ROUTINGS_ADDRESS = {
	ADD_ADDRESS: `${BASE_ROUTE}/add-address`,

	UPDATE_ADDRESS: `${BASE_ROUTE}/update-address`,
	UPDATE_DEFAULT_ADDRESS: `${BASE_ROUTE}/update-default-address`,

	DELETE_ADDRESS: `${BASE_ROUTE}/delete-address`,

	LIST_ADDRESS_BY_CUSTOMER: `${BASE_ROUTE}/list-address-by-customer`,
	INFO_ADDRESS: `${BASE_ROUTE}/info-address`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_ADDRESS = CF_ROUTINGS_ADDRESS;
