"use strict";

const Schema    = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION CATEGORY CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('category', {
	title: {
		type: String,
		trim: true,
		unique : true
	},
    description: {
		type: String,
		trim: true
	},
	// STT HIỂN THỊ
	stt : {
		type: Number,
	},
	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 * 2: Xóa
	 */
	status: {
		type: Number,
		default: 1
	},
    image: {
        type: Schema.Types.ObjectId,
        ref: 'image'
    },
	/**
	 * Mảng hình ảnh của Danh mục
	 */
	gallerys: [{
		type: Schema.Types.ObjectId,
		ref: "image"
	}],
});
