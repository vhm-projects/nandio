const CATEGORY_MODEL    = require('./models/category').MODEL;
const CATEGORY_COLL     = require('./databases/category-coll');
const CATEGORY_ROUTES   = require('./apis/category');

module.exports = {
    CATEGORY_ROUTES,
    CATEGORY_COLL,
    CATEGORY_MODEL,
}