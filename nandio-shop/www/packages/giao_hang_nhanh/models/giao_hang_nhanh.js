
 "use strict";

 /**
  * EXTERNAL PACKAGES
  */
const request          = require('request');
const DOMAIN_GHN       = process.env.DOMAIN_GHN || 'https://dev-online-gateway.ghn.vn';
const TOKEN_GHN        = process.env.TOKEN_GHN  || 'a4cc2e63-3191-11ec-b514-aeb9e8b0c5e3';
const CLIENT_PHONE     = process.env.CLIENT_PHONE  || '0964209752';
/**
 * BASE
 */

class Model {
    getListProvinceGHN({  }) {
        return new Promise(async resolve => {
            try {
                var options = {
                    'method': 'GET',
                    'url': `${DOMAIN_GHN}/shiip/public-api/master-data/province`,
                    'headers': {
                        'Content-Type': 'application/json',
                        'Token': TOKEN_GHN
                    }
                };
                request(options, function (error, response) {
                    if (error) 
                        return resolve({ error: true, data: JSON.parse(error) });
                    return resolve({ error: false, data: JSON.parse(response.body) });
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListDistrictGHNByProvinceID({ provinceID }) {
        return new Promise(async resolve => {
            try {
                var options = {
                    'method': 'GET',
                    'url': `${DOMAIN_GHN}/shiip/public-api/master-data/district`,
                    'headers': {
                        'token': TOKEN_GHN,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "province_id": Number(provinceID)
                    })
                };
                request(options, function (error, response) {
                    if (error) 
                        return resolve({ error: true, data: JSON.parse(error) });
                    return resolve({ error: false, data: JSON.parse(response.body) });
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListWardGHNByDistrictID({ districtID }) {
        return new Promise(async resolve => {
            try {
                var options = {
                    'method': 'GET',
                    'url': `${DOMAIN_GHN}/shiip/public-api/master-data/ward`,
                    'headers': {
                      'token': TOKEN_GHN,
                      'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                      "district_id": Number(districtID)
                    })
                  
                };
                request(options, function (error, response) {
                    if (error) 
                        return resolve({ error: true, data: JSON.parse(error) });
                    return resolve({ error: false, data: JSON.parse(response.body) });
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoProvinceGHN({ cityName }) {
        return new Promise(async resolve => {
            try {
                var options = {
                    'method': 'GET',
                    'url': `${DOMAIN_GHN}/shiip/public-api/master-data/province`,
                    'headers': {
                        'Content-Type': 'application/json',
                        'Token': TOKEN_GHN
                    }
                };
                request(options, function (error, response) {
                    if (error) throw new Error(error);

                    let infoCity = {};
                    let listCityGHN = JSON.parse(response.body);
                    if (listCityGHN.code == 200) {
                        listCityGHN.data && listCityGHN.data.length && listCityGHN.data.map((item, index) => {
                            if (item.NameExtension.includes(cityName)) {
                                infoCity = item;
                            }
                        });
                    }
                    return resolve({ error: false, data: infoCity });
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoDistrictGHN({ cityID, districtName }) {
        return new Promise(async resolve => {
            try {
                var options = {
                'method': 'GET',
                'url': `${DOMAIN_GHN}/shiip/public-api/master-data/district`,
                'headers': {
                    'token': TOKEN_GHN,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "province_id": cityID
                })

                };
                request(options, function (error, response) {
                    if (error) throw new Error(error);

                    let infoDistrict    = {};
                    let listDistrictGHN = JSON.parse(response.body);
                    if (listDistrictGHN.code == 200) {
                        listDistrictGHN.data && listDistrictGHN.data.length && listDistrictGHN.data.map((item, index) => {
                            if ((item.NameExtension && item.NameExtension.includes(districtName)) || item.DistrictName == districtName) {
                                infoDistrict = item;
                            }
                        });
                    }
                    return resolve({ error: false, data: infoDistrict });
                });

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoWardGHN({ districtID, wardName }) {
        return new Promise(async resolve => {
            try {
                var options = {
                    'method': 'GET',
                    'url': `${DOMAIN_GHN}/shiip/public-api/master-data/ward`,
                    'headers': {
                        'token': TOKEN_GHN,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "district_id": districtID
                    })
                };
                request(options, function (error, response) {
                    if (error) throw new Error(error);

                    let infoWard    = {};
                    let listWardGHN = JSON.parse(response.body);
                    if (listWardGHN.code == 200) {
                        listWardGHN.data && listWardGHN.data.length && listWardGHN.data.map((item, index) => {
                            if (item.NameExtension.includes(wardName)) {
                                infoWard = item;
                            }
                        });
                    }
                    return resolve({ error: false, data: infoWard });
                });

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoAddressGHN({ cityName, districtName, wardName }) {
        return new Promise(async resolve => {
            try {
                let infoCity = await this.getInfoProvinceGHN({ cityName: cityName });
                if (infoCity.error) {
                    return resolve(infoCity);
                }
                let infoDistrict = await this.getInfoDistrictGHN({ cityID: infoCity.data.ProvinceID, districtName })
                if (infoDistrict.error) {
                    return resolve(infoDistrict);
                }
               
                let infoWard = await this.getInfoWardGHN({ districtID: infoDistrict.data.DistrictID, wardName })
                if (infoWard.error) {
                    return resolve(infoWard);
                }

                return resolve({ 
                    error: false, 
                    data: { 
                        infoCity:     infoCity.data, 
                        infoDistrict: infoDistrict.data, 
                        infoWard:     infoWard.data, 
                    } 
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListStoreGHN({ limit = 10 }) {
        return new Promise(async resolve => {
            try {
                var options = {
                    'method': 'POST',
                    'url': `${DOMAIN_GHN}/shiip/public-api/v2/shop/all`,
                    'headers': {
                        'Content-Type': 'application/json',
                        'token': TOKEN_GHN
                    },
                    body: JSON.stringify({
                        "offset": 0,
                        "limit": limit,
                        "client_phone": CLIENT_PHONE
                    })
                };
                request(options, function (error, response) {
                    if (error) 
                        return resolve(JSON.parse(error))
                    return resolve(JSON.parse(response.body))
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    calculateFee({ 
        districtStore,
        storeID,
        wardCustomer,
        districtCustomer,
        weight,
        length,
        width,
        height,
    }) {
        return new Promise(async resolve => {
            try {
                // console.log({
                //     districtStore,
                //     storeID,
                //     wardCustomer,
                //     districtCustomer,
                //     weight,
                //     length,
                //     width,
                //     height,
                // });
                var options = {
                    'method': 'POST',
                    'url': `${DOMAIN_GHN}/shiip/public-api/v2/shipping-order/fee`,
                    'headers': {
                        'Content-Type': 'application/json',
                        'ShopId': storeID,
                        'token': TOKEN_GHN
                    },
                    body: JSON.stringify({
                        "from_district_id": Number(districtStore),
                        "service_id": 0,
                        "service_type_id": 2,
                        "to_district_id": Number(districtCustomer),
                        "to_ward_code": wardCustomer,
                        "height": Number(height),
                        "length": Number(length),
                        "weight": Number(weight),
                        "width": Number(width),
                        "insurance_fee": 10000,
                        "coupon": null
                    })
                };

                request(options, function (error, response) {
                    if (error) 
                        return resolve(JSON.parse(error))
                    return resolve(JSON.parse(response.body))
                });

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    createOrderGHN({ 
        payment_type_id,
        clientCode,
        districtStore,
        storeID,
        wardStore,
        wardCustomer,
        districtCustomer,
        weight,
        length,
        width,
        height,
        listProduct,
        totalPrice,
        fullname,
        phone,
        address
     }) {
        return new Promise(async resolve => {
            try {
                var options = {
                    'method': 'POST',
                    'url': `${DOMAIN_GHN}/shiip/public-api/v2/shipping-order/create`,
                    'headers': {
                        'Content-Type': 'application/json',
                        'ShopId': storeID,
                        'token': TOKEN_GHN
                    },
                    body: JSON.stringify({
                        "payment_type_id": Number(payment_type_id),
                        "note": "KHÔNG CHO XEM HÀNG",
                        "required_note": "KHONGCHOXEMHANG",
                        "return_phone": CLIENT_PHONE,
                        "return_address": "Thủ đức",
                        "return_district_id": Number(districtStore),
                        "return_ward_code": wardStore,
                        "client_order_code": clientCode,
                        "to_name": fullname,
                        "to_phone": phone,
                        "to_address": address ? address : '102',
                        "to_ward_code": wardCustomer,
                        "to_district_id": Number(districtCustomer),
                        "cod_amount": totalPrice,
                        "content": "",
                        "weight": Number(weight),
                        "length": Number(length),
                        "width":  Number(width),
                        "height": Number(height),
                        "pick_station_id": 0,
                        "deliver_station_id": null,
                        "insurance_value": totalPrice,
                        "service_id": 53320,
                        "service_type_id": 2,
                        // "order_value": 130000,
                        "coupon": null,
                        "pick_shift": [
                            2
                        ],
                        "items": listProduct
                    })
                };
                request(options, function (error, response) {
                    if (error) {
                        return resolve(JSON.parse(error))
                    }
                    return resolve(JSON.parse(response.body))
                });


            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // IN ĐƠN VẬN
    printfOrderGHN({ 
        code
    }) {
        return new Promise(async resolve => {
            try {
                console.log({
                    code
                });
                var options = {
                    'method': 'POST',
                    'url': `${DOMAIN_GHN}/shiip/public-api/v2/a5/gen-token`,
                    'headers': {
                        'Content-Type': 'application/json',
                        'token': TOKEN_GHN
                    },
                    body: JSON.stringify({
                        "order_codes": [
                            code
                        ]
                    })
                };
                request(options, function (error, response) {
                    if (error)
                        return resolve(JSON.parse(error))
                    let data = JSON.parse(response.body);
                    return resolve({
                        ...data,
                        url: `${DOMAIN_GHN}/a5/public-api/print80x80?token=${data.data.token}`
                    });
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }


}
exports.MODEL = new Model;
