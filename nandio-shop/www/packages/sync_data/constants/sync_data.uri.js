const BASE_ROUTE = '/sync';

const CF_ROUTINGS_SYNC_DATA = {
	REGISTER_CUSTOMER: `${BASE_ROUTE}/register-customer`,
	UPDATE_POINT_CUSTOMER: `${BASE_ROUTE}/update-point-customer`,
	UPDATE_POINT_PRODUCT: `${BASE_ROUTE}/update-point-product`,
	ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_SYNC_DATA = CF_ROUTINGS_SYNC_DATA;
