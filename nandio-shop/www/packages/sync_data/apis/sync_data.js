"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { CF_ROUTINGS_SYNC_DATA } 		    = require('../constants/sync_data.uri.js');

/**
 * MODELS
 */
const CUSTOMER_MODEL 	= require('../../customer/models/customer').MODEL;
const PRODUCT_MODEL 	= require('../../product/models/product').MODEL;
const CUSTOMER_COLL 	= require('../../customer/databases/customer-coll');
// const NON_MEMBER_MODEL	= require('../models/non_member').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ****************** ================================
             * ========================== ĐỒNG BỘ DỮ LIỆU  ================================
             * ========================== ****************** ================================
             */

			/**
             * Function: API đồng bộ dữ liệu khách hàng
             * Date: 05/09/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_SYNC_DATA.REGISTER_CUSTOMER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
                        // key authen BtN0ldfQTfNqShKWzuqcCHnfWpyVFoXjHpkqvGTNYDlNXiFPyXMUHhAO4VJy
                        const { phone, fullname, gender, address, email, key } = req.body;
                        if(key != "BtN0ldfQTfNqShKWzuqcCHnfWpyVFoXjHpkqvGTNYDlNXiFPyXMUHhAO4VJy")
                            return res.json({ error: true, message: "invalid_key" });
                        // console.log({ phone, fullname, gender, address, email });
                        const infoAfterInsertCustomer = await CUSTOMER_MODEL.insert({ 
                            phone, fullname, gender, address, email, gender, address, intervention: true
                        });
                        res.json(infoAfterInsertCustomer);
                    }]
                },
            },

            /**
             * Function: API đồng bộ cập nhật điểm khách hàng
             * Date: 05/09/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_SYNC_DATA.UPDATE_POINT_CUSTOMER]: {
                config: { 
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
                        const { phone, type, value, key } = req.body;
                        if(key != "BtN0ldfQTfNqShKWzuqcCHnfWpyVFoXjHpkqvGTNYDlNXiFPyXMUHhAO4VJy")
                            return res.json({ error: true, message: "invalid_key" });
                        let infoCustomer = await CUSTOMER_COLL.findOne({ phone });

                        // Kiểm tra phone
                        if(!infoCustomer)
                            return res.json({ error: true, message: "phone_not_exist" });
                        let { _id: customerID, point, pointRanking } = infoCustomer;

                        // Kiểm tra value
                        if(isNaN(+value) || !value)
                            return res.json({ error: true, message: "value_invalid" });

                        // Kiểm tra type
                        if(isNaN(+type) || !type || ![1, 2].includes(+type))
                          return res.json({ error: true, message: "type_invalid" });

                        /**
                         * type { 1: "Tích điểm", 2: "Đổi điểm" }
                         */
                        if(type == 1){
                            point += +value;
                            pointRanking += +value;
                        }

                        if(type == 2){
                            if(point < +value)                                
                                return res.json({ error: true, message: "user_not_enough_point" });
                            point -= +value;
                        }
                        // Tiến hành cập nhật điểm
                        const infoAfterInsertCustomer = await CUSTOMER_MODEL.updatePoint({ 
                            customerID, point: +point, pointRanking: +pointRanking, intervention: true
                        });
                        res.json(infoAfterInsertCustomer);
                    }]
                },
            },

            /**
             * Function: API đồng bộ cập nhật điểm sản phẩm
             * Date: 05/09/2021
             * Dev: Depv247
             */
            [CF_ROUTINGS_SYNC_DATA.UPDATE_POINT_PRODUCT]: {
                config: { 
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
                        const { productID, point, key } = req.body;
                        if(key != "BtN0ldfQTfNqShKWzuqcCHnfWpyVFoXjHpkqvGTNYDlNXiFPyXMUHhAO4VJy")
                            return res.json({ error: true, message: "invalid_key" });
                        const infoAfterInsertCustomer = await PRODUCT_MODEL.updatePoint({ idSQL: productID, point  })
                        res.json(infoAfterInsertCustomer);
                    }]
                },
            },
        }
    }
};
