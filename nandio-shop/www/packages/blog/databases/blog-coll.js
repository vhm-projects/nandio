"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../database/intalize/base-coll');

/**
 * COLLECTION BLOG CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('blog', {
	// Tiêu đề bài viết
	title: {
		type: String,
		unique: true,
		require: true
	},

	slug: {
		type: String, 
		unique: true,
		require: true
	},

	// Mô tả ngắn
	description: {
		type: String,
		default: ''
	},

	// Nội dung
	content: {
		type: String,
	},

	// Liên Kết
	link: {
		type: String
	},

	/**
	 * 0: Không hoạt động
	 * 1: Hoạt động
	 */
	status: {
		type: Number,
		default: 1
	},

	// Hình ảnh bài viết
	image: {
		type:  Schema.Types.ObjectId,
		ref : 'image'
	},

	// Gallery bài viết
	images: [{
		type:  Schema.Types.ObjectId,
		ref : 'image'
	}],

	// Comment bài viết
	comments: [{
		type:  Schema.Types.ObjectId,
		ref : 'comment'
	}],

	// Danh mục bài viết
	categoryBlog: {
		type:  Schema.Types.ObjectId,
		ref : 'category_blog'
	},

	// Tagging
	taggings: [{
		type:  Schema.Types.ObjectId,
		ref : 'tagging'
	}],

	// Lượt xem
	views: {
		type: Number,
		default: 0
	},

	//_________Người tạo
	userCreate: {
		type:  Schema.Types.ObjectId,
		ref : 'user'
	},
	
	//_________Người cập nhật
	userUpdate: {
		type:  Schema.Types.ObjectId,
		ref : 'user'
	}
});
