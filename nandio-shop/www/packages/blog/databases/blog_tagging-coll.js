"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../database/intalize/base-coll');

/**
 * COLLECTION BLOG TAGGING CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('blog_tagging', {

	blogID: {
		type:  Schema.Types.ObjectId,
		ref : 'blog'
	},

	taggingID: {
		type:  Schema.Types.ObjectId,
		ref : 'tagging'
	},
	
});
