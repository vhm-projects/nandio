"use strict";

const Schema    = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION COMMENT CỦA HỆ THỐNG
 */
module.exports = BASE_COLL("comment", {
    /**
     * ID khách hàng
     */
    author: {
        type: Schema.Types.ObjectId,
		ref: 'customer',
		require: true
    },
    // ID Bài viết
    post: {
        type: Schema.Types.ObjectId,
        ref: 'blog',
		require: true
    },
	content: {
        type: String,
		require: true
    },
    /**
     * 1: Hiển thị
     * 0: Không hiển thị
     */
    status: {
        type: Number,
        default: 1
    }
});
