"use strict";

const Schema    = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION CATEGORY CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('tagging', {

	title: {
		type: String,
		trim: true,
		unique : true
	},

    description: {
		type: String,
		trim: true
	},

	blogs: [{
		type:  Schema.Types.ObjectId,
		ref : 'blog'
	}],

	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 * 2: Xóa
	 */
	status: {
		type: Number,
		default: 1
	},
	
});
