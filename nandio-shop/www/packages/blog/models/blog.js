"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { convertToSlug } = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const BLOG_COLL = require('../databases/blog-coll');
const { IMAGE_MODEL }     = require('../../image');

class Model extends BaseModel {
    constructor() {
        super(BLOG_COLL);
        this.STATUS_ACTIVE = 1;
    }

	insert({ title, slug, content, description, image, status, category, taggings, userCreate, images }) {
        return new Promise(async resolve => {
            try {

				if(!ObjectID.isValid(userCreate) || !ObjectID.isValid(category))
                    return resolve({ error: true, message: 'param_invalid' });

                if(!title || !slug || !content)
                    return resolve({ error: true, message: 'title_slug_content_is_required' });

				slug = convertToSlug(slug);
                const checkExists = await BLOG_COLL.findOne({
					$or: [
						{ title },
						{ slug }
					]
				});
                if(checkExists)
                    return resolve({ error: true, message: 'title_or_slug_existed' });

                const dataInsert = {
                    title,
                    slug,
					content,
					image,
					description,
                    status,
					categoryBlog: category,
					userCreate
                }

                if(taggings && taggings.length){
                    dataInsert.taggings = taggings
                }

                if (images && images.length) {
                    let listGallery          = images.map( image => {
                        return IMAGE_MODEL.insert({ 
                            name: image.name,
                            path: image.pathAvatar,
                            size: image.size, 
                            userCreate
                        });
                    });
                    let resultPromiseAll = await Promise.all(listGallery);
                    let listGalleryID    = resultPromiseAll.map(gallery => gallery.data._id);

                    dataInsert.images = listGalleryID;
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_post_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ postID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'param_invalid' });

                const infoPost = await BLOG_COLL.findById(postID)
                .populate('image images categoryBlog taggings')
                .populate({
                    path: "categoryBlog",
                    populate: "image"
                })
                .lean();
                if(!infoPost)
                    return resolve({ error: true, message: 'post_is_not_exists' });

                return resolve({ error: false, data: infoPost });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoBySlug({ slug, postID }) {
        return new Promise(async resolve => {
            try {

                const infoPost = await BLOG_COLL.findOne({ _id: { $nin: [postID] }, slug }).lean();
                if(infoPost)
                    return resolve({ error: true, message: 'post_is_existed' });

                return resolve({ error: false, data: infoPost });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ postID, title, category, taggings, slug, description, content, image, status, userUpdate, arrImageGallryNew, arrImageGallryDelete }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userUpdate) || !ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'param_invalid' });

                if(!title || !slug || !content)
                    return resolve({ error: true, message: 'title_slug_content_is_required' });

                const checkExists = await BLOG_COLL.findById(postID);
                if(!checkExists)
                    return resolve({ error: true, message: 'post_is_not_exists' });

                const dataUpdatePost = {
                    title,
                    content,
                    slug: convertToSlug(slug),
                    status,
                    categoryBlog: category,
                    taggings,
                    userUpdate
                };
                description && (dataUpdatePost.description 	= description);
                image    	&& (dataUpdatePost.image 		= image);

                if (arrImageGallryNew && arrImageGallryNew.length) {
                    let listGallery          = arrImageGallryNew.map(image => {
                        return IMAGE_MODEL.insert({ 
                            name: image.name,
                            path: image.pathAvatar,
                            size: image.size, 
                            userCreate: userUpdate 
                        });
                    });
                    let resultPromiseAll = await Promise.all(listGallery);
                    let listGalleryID    = resultPromiseAll.map( gallery => gallery.data._id );

                    dataUpdatePost.$addToSet = {
                        images: listGalleryID
                    };
                }

                if (arrImageGallryDelete.length) {
                    for (const imageID of arrImageGallryDelete) {
                        await BLOG_COLL.findByIdAndUpdate(postID, {
                            $pull: { images: imageID } 
                        }, {new: true});
                    }
                }

                await this.updateWhereClause({ _id: postID }, {
                    ...dataUpdatePost
                });

                return resolve({ error: false, data: dataUpdatePost });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ postID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'param_invalid' });

                const infoAfterDelete = await BLOG_COLL.findByIdAndRemove(postID);
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: 'post_is_not_exists' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ keyword, slug, sort, tab }){
        return new Promise(async resolve => {
            try {
                const condition = {};
				slug && (condition.slug = slug);

                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { title: new RegExp(key, 'i') },
                        { slug: new RegExp(key, 'i') },
                        { description: new RegExp(key, 'i') },
                    ]
                }

				const sortBy = { modifyAt: -1 };
				if(sort && [-1, 1].includes(+sort)){
					sortBy.views = sort;
				}

				// switch (+tab) {
				// 	case 1:
				// 		condition.type = 0;
				// 		break;
				// 	case 2:
				// 		condition.type = 1;
				// 		break;
				// 	default:
				// 		condition.type = { $in: [0,1] };
				// 		break;
				// }

                const listPosts = await BLOG_COLL
					.find(condition)
					.populate('image')
					.sort(sortBy)
					.lean();

                if(!listPosts)
                    return resolve({ error: true, message: 'not_found_posts_list' });

                return resolve({ error: false, data: listPosts });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListForEnduser({ limit = 5, page = 1, categoryID, keyword, tagID }){
        return new Promise(async resolve => {
            try {
                limit = Number(limit);
                page  = Number(page);

                let condition = {};

                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { title: new RegExp(key, 'i') },
                        { description: new RegExp(key, 'i') },
                    ]
                }

                if(categoryID && ObjectID.isValid(categoryID)){
                    condition.categoryBlog = categoryID
                }

                if(tagID && ObjectID.isValid(tagID)){
                    condition.taggings =  { $in: [tagID] };
                }

                const listPosts = await BLOG_COLL
					.find({ status: this.STATUS_ACTIVE, ...condition })
                    .select("title description createAt")
                    .limit(limit)
                    .skip((page * limit) - limit)
					.sort({ modifyAt: -1 })
					.populate({
                        path: "image",
                        select: "path"
                    })

                if(!listPosts)
                    return resolve({ error: true, message: 'not_found_posts_list' });
                let totalItem = await BLOG_COLL.count({ status: this.STATUS_ACTIVE});
                let pages = Math.ceil(totalItem/limit);
                return resolve({ error: false, data: { listPosts, currentPage: page, totalPage: pages }});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getRelatedPost({ postID, type }){
        return new Promise(async resolve => {
            try {
                const listRelatedPosts = await BLOG_COLL.find({
                    $and: [
                        { _id: { $nin: [postID] } },
						type
                    ]
                }).populate('image').limit(3).lean();

                if(!listRelatedPosts)
                    return resolve({ error: true, message: 'not_found_posts_list' });

                return resolve({ error: false, data: listRelatedPosts });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
	getListMostViewed(){
		return new Promise(async resolve => {
			try {
				const listMostViewedPost = await BLOG_COLL.find({})
					.populate('image')
					.sort({ views: -1 })
					.limit(5)
                    .lean();

				if(!listMostViewedPost)
                    return resolve({ error: true, message: 'not_found_posts_list' });

				return resolve({ error: false, data: listMostViewedPost });
			} catch (error) {
                return resolve({ error: true, message: error.message });
			}
		})
	}

    getListMostViewed(){
		return new Promise(async resolve => {
			try {
				const listMostViewedPost = await BLOG_COLL.find({})
					.populate('image')
					.sort({ views: -1 })
					.limit(5)
                    .lean();

				if(!listMostViewedPost)
                    return resolve({ error: true, message: 'not_found_posts_list' });

				return resolve({ error: false, data: listMostViewedPost });
			} catch (error) {
                return resolve({ error: true, message: error.message });
			}
		})
	}

}

exports.MODEL = new Model;
