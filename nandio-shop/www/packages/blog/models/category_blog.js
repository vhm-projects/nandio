"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs } = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const CATEGORY_BLOG_COLL 	= require('../databases/category_blog-coll');


class Model extends BaseModel {
    constructor() {
        super(CATEGORY_BLOG_COLL);
    }

	insert({ title, description, status, image }) {
        return new Promise(async resolve => {
            try {

                if(!title || !ObjectID.isValid(image))
                    return resolve({ error: true, message: 'param_invalid' });

                let infoCategoryBlog = await CATEGORY_BLOG_COLL.findOne({ title });
                if(infoCategoryBlog){
                    return resolve({ error: true, message: 'category_is_existed' });
                }

                const dataInsert = {
					title, description, status: Number(status), image
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	update({ title, description, status, image, categoryBlogID }) {
        return new Promise(async resolve => {
            try {
                if(!title || !ObjectID.isValid(categoryBlogID))
                    return resolve({ error: true, message: 'param_invalid' });

                let infoCategoryBlog = await CATEGORY_BLOG_COLL.findOne({ _id: { $nin: [categoryBlogID] }, title });
                if(infoCategoryBlog){
                    return resolve({ error: true, message: 'category_is_existed' });
                }

                let dataUpdate = {title, description};
                
                if(image)
                    dataUpdate.image = image

                if(status && [0, 1].includes(+status)){
                    dataUpdate.status = status;
                }

                const infoAfterUpdate = await CATEGORY_BLOG_COLL.findByIdAndUpdate(categoryBlogID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfo({ categoryBlogID }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(categoryBlogID))
                    return resolve({ error: true, message: 'param_invalid' });

				const infoCategoryBlog = await CATEGORY_BLOG_COLL
					.findById(categoryBlogID).populate("image")

                if(!infoCategoryBlog)
                    return resolve({ error: true, message: 'failed' });

				return resolve({ error: false, data: infoCategoryBlog });
			} catch (error) {
				return resolve({ error: true, error: message.error });
			}
		})
	}

	getList({ status }) {
        return new Promise(async resolve => {
            try {

                let condition = {};

                if(status){
                    condition.status = status
                }

                const listCategoryBlog = await CATEGORY_BLOG_COLL.find(condition)
                .populate({
                    path: "image",
                    select: "path"
                })

                if(!listCategoryBlog)
                    return resolve({ error: true, message: 'get_list_comment_failed' });

                return resolve({ error: false, data: listCategoryBlog });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ categoryBlogID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(categoryBlogID))
                    return resolve({ error: true, message: 'param_invalid' });
               
                const infoAfterRemove = await CATEGORY_BLOG_COLL.findByIdAndDelete(categoryBlogID);
                if(!infoAfterRemove)
                    return resolve({ error: true, message: 'cannot_remove' });

                return resolve({ error: false, data: infoAfterRemove });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
