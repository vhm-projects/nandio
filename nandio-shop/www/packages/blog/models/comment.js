"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs } = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const COMMENT_COLL 	= require('../databases/comment-coll');
const BLOG_COLL 	= require('../databases/blog-coll');


class Model extends BaseModel {
    constructor() {
        super(COMMENT_COLL);
    }

	insert({ authorID, postID, content }) {
        return new Promise(async resolve => {
            try {
                if(!content || !checkObjectIDs([authorID, postID]))
                    return resolve({ error: true, message: 'param_invalid' });

                const dataInsert = {
                    author: authorID,
                    post: postID,
					content
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_comment_failed' });

				await BLOG_COLL.updateOne({ _id: postID }, {
					$addToSet: { comments: infoAfterInsert._id }
				})

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	updateStatus({ commentID, status }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(commentID))
                    return resolve({ error: true, message: 'param_invalid' });

                let dataUpdate = {};
                if([0, 1].includes(+status)){
                    dataUpdate.status = status;
                }

                const infoAfterUpdate = await COMMENT_COLL.findByIdAndUpdate(commentID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfo({ commentID }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(commentID))
                    return resolve({ error: true, message: 'param_invalid' });

				const infoComment = await COMMENT_COLL
					.findById(commentID)
					.populate('author post')
					.lean();

                if(!infoComment)
                    return resolve({ error: true, message: 'not_found_comment' });

				return resolve({ error: false, data: infoComment });
			} catch (error) {
				return resolve({ error: true, error: message.error });
			}
		})
	}

	getList({ authorID, status }) {
        return new Promise(async resolve => {
            try {
				if(authorID && !ObjectID.isValid(authorID))
                    return resolve({ error: true, message: 'param_invalid' });

                let condition = {};
				authorID && (condition.author = authorID);

                if([0, 1].includes(+status)){
                    condition.status = status;
                }

                const listComment = await COMMENT_COLL
					.find(condition)
					.populate('post author')
					.lean();

                if(!listComment)
                    return resolve({ error: true, message: 'get_list_comment_failed' });

                return resolve({ error: false, data: listComment });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByBlog({ postID , status }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'param_invalid' });

                let condition = {
                    post: postID
                }

                if([0, 1].includes(status)){
                    condition.status = status;
                }

                const listCommentByPost = await COMMENT_COLL.find(condition).lean();
                if(!listCommentByPost)
                    return resolve({ error: true, message: 'get_list_comment_failed' });

                return resolve({ error: false, data: listCommentByPost });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ commentID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(commentID))
                    return resolve({ error: true, message: 'param_invalid' });
               
                const infoAfterRemove = await COMMENT_COLL.findByIdAndDelete(commentID);
                if(!infoAfterRemove)
                    return resolve({ error: true, message: 'cannot_remove' });

				await BLOG_COLL.updateOne({ _id: infoAfterRemove.post }, {
					$pull: { comments: commentID }
				})

                return resolve({ error: false, data: infoAfterRemove });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
