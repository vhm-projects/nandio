"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs } = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const TAGGING_COLL 	= require('../databases/tagging-coll');


class Model extends BaseModel {
    constructor() {
        super(TAGGING_COLL);
    }

	insert({ title, description, status }) {
        return new Promise(async resolve => {
            try {
                if(!title)
                    return resolve({ error: true, message: 'param_invalid' });

                let infoTagging = await TAGGING_COLL.findOne({ title });
                if(infoTagging){
                    return resolve({ error: true, message: 'tagging_is_existed' });
                }
                    
                const dataInsert = {
					title, description, status: Number(status)
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	update({ title, description, status, taggingID }) {
        return new Promise(async resolve => {
            try {
                if(!title || !ObjectID.isValid(taggingID))
                    return resolve({ error: true, message: 'param_invalid' });

                let infoTagging = await TAGGING_COLL.findOne({ _id: { $nin: [taggingID] }, title });
                if(infoTagging){
                    return resolve({ error: true, message: 'tagging_is_existed' });
                }

                let dataUpdate = {
                    title, description, status: Number(status)
                };

                if(status && [0, 1].includes(+status)){
                    dataUpdate.status = status;
                }

                const infoAfterUpdate = await TAGGING_COLL.findByIdAndUpdate(taggingID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfo({ taggingID }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(taggingID))
                    return resolve({ error: true, message: 'param_invalid' });

				const infoTagging = await TAGGING_COLL.findById(taggingID).lean();

                if(!infoTagging)
                    return resolve({ error: true, message: 'failed' });

				return resolve({ error: false, data: infoTagging });
			} catch (error) {
				return resolve({ error: true, error: message.error });
			}
		})
	}

	getList({ status }) {
        return new Promise(async resolve => {
            try {

                let condition = {};

                if(status){
                    condition.status = status
                }

                const listTagging = await TAGGING_COLL.find(condition).lean();

                if(!listTagging)
                    return resolve({ error: true, message: 'get_list_comment_failed' });

                return resolve({ error: false, data: listTagging });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ taggingID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(taggingID))
                    return resolve({ error: true, message: 'param_invalid' });
               
                const infoAfterRemove = await TAGGING_COLL.findByIdAndDelete(taggingID);
                if(!infoAfterRemove)
                    return resolve({ error: true, message: 'cannot_remove' });

                return resolve({ error: false, data: infoAfterRemove });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
