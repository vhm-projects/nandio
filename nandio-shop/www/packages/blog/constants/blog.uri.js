const BASE_ROUTE = '/admin';
const BASE_ROUTE_BLOG = '/blog';
const BASE_ROUTE_CATEGORY_BLOG = '/category-blog';
const BASE_ROUTE_TAGGING = '/tagging';

const CF_ROUTINGS_BLOG = {
    // BLOG
    LIST_POST: `${BASE_ROUTE}/list-post`,
    LIST_POST_FOR_ENDUSER: `${BASE_ROUTE_BLOG}/list-blog`,
    ADD_POST: `${BASE_ROUTE}/add-post`,
    UPDATE_POST: `${BASE_ROUTE}/update-post`,
    DELETE_POST: `${BASE_ROUTE}/delete-post`,
    INFO_POST: `${BASE_ROUTE_BLOG}/info-post`,
    INFO_POST_BY_SLUG: `${BASE_ROUTE}/info-post-by-slug`,

	ADD_COMMENT: `${BASE_ROUTE}/add-comment`,
	INFO_COMMENT: `${BASE_ROUTE}/info-comment`,
	LIST_COMMENT: `${BASE_ROUTE}/list-comments`,
	UPDATE_COMMENT: `${BASE_ROUTE}/update-comment`,
	DELETE_COMMENT: `${BASE_ROUTE}/delete-comment`,

    //Category blog
    ADD_CATEGORY_BLOG: `${BASE_ROUTE_CATEGORY_BLOG}/add-category-blog`,
	INFO_CATEGORY_BLOG: `${BASE_ROUTE_CATEGORY_BLOG}/info-category-blog/:categoryBlogID`,
	LIST_CATEGORY_BLOG: `${BASE_ROUTE_CATEGORY_BLOG}/list-category-blog`,
	LIST_CATEGORY_BLOG_API: `${BASE_ROUTE_CATEGORY_BLOG}/api/list-category-blog`,
	UPDATE_CATEGORY_BLOG: `${BASE_ROUTE_CATEGORY_BLOG}/update-category-blog/:categoryBlogID`,
	DELETE_CATEGORY_BLOG: `${BASE_ROUTE_CATEGORY_BLOG}/delete-category-blog/:categoryBlogID`,

    //Tagging
    ADD_TAGGING: `${BASE_ROUTE_TAGGING}/add-tagging`,
	INFO_TAGGING: `${BASE_ROUTE_TAGGING}/info-tagging/:taggingID`,
	LIST_TAGGING: `${BASE_ROUTE_TAGGING}/list-tagging`,
	LIST_TAGGING_API: `${BASE_ROUTE_TAGGING}/api/list-tagging`,
	UPDATE_TAGGING: `${BASE_ROUTE_TAGGING}/update-tagging/:taggingID`,
	DELETE_TAGGING: `${BASE_ROUTE_TAGGING}/delete-tagging/:taggingID`,

    ORIGIN_APP: BASE_ROUTE,
}

exports.CF_ROUTINGS_BLOG = CF_ROUTINGS_BLOG;
