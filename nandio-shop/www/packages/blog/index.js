const BLOG_MODEL 			= require('./models/blog').MODEL;
const TAGGING_MODEL 		= require('./models/tagging').MODEL;
const CATEGORY_BLOG_MODEL 	= require('./models/category_blog').MODEL;
const BLOG_COLL  			= require('./databases/blog-coll');
const TAGGING_COLL  		= require('./databases/tagging-coll');
const CATEGORY_BLOG_COLL  	= require('./databases/category_blog-coll');
const BLOG_ROUTES       	= require('./apis/blog');

module.exports = {
    BLOG_ROUTES,
    TAGGING_MODEL,
    CATEGORY_BLOG_MODEL,
    BLOG_COLL,
    TAGGING_COLL,
    CATEGORY_BLOG_COLL,
    BLOG_MODEL,
}
