"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                   = require('../../../routing/child_routing');
const roles                         = require('../../../config/cf_role');
const { uploadSingle }              = require('../../../config/cf_helpers_multer');
const { POST_TYPE } 				= require('../../../config/cf_constants');
const { CF_ROUTINGS_BLOG } 			= require('../constants/blog.uri');

/**
 * MODELS
 */
const BLOG_MODEL                = require('../models/blog').MODEL;
const CATEGORY_BLOG_MODEL       = require('../models/category_blog').MODEL;
const TAGGING_MODEL             = require('../models/tagging').MODEL;
const COMMENT_MODEL				= require('../models/comment').MODEL;
const { IMAGE_MODEL }           = require('../../image');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ BLOG  ===============================
             * =============================== ************* ===============================
             */

             /**
             * Function: Tạo post (API, VIEW)
             * Date: 20/06/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.ADD_POST]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'view',
					title: 'Add Post - NANDIO',
					code: CF_ROUTINGS_BLOG.ADD_POST,
					inc: path.resolve(__dirname, '../views/blogs/add_blog.ejs'),
                    view: 'index.ejs'
                },
                methods: {
					get: [ async function(req, res) {
                        let listCategory = await CATEGORY_BLOG_MODEL.getList({status: 1});
                        let listTagging = await TAGGING_MODEL.getList({status: 1});
						ChildRouter.renderToView(req, res, {
                            POST_TYPE,
                            listCategory: listCategory.data,
                            listTagging: listTagging.data,
						});
					}],
					post: [ uploadSingle, async function(req, res){
						let { _id: userCreate } = req.user;
						let { title, category, taggings, slug, description, content, status, images, pathImage } = JSON.parse(req.body.data);

                        let image = null;
                        if(req.file){
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size,
                                name: req.fileName,
                                path: pathImage, // `/files/${req.fileName}`
                            });
                            image = infoImageAfterInsert.data._id;
                        }

						let dataAfterAddPost = await BLOG_MODEL.insert({
							title, category, taggings, slug, description, content, image, status, userCreate, images
						});
						res.json(dataAfterAddPost);
					}]
                },
            },

            /**
             * Function: Cập nhật post (API, VIEW)
             * Date: 20/06/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.UPDATE_POST]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'view',
					title: 'Update Post - NANDIO',
					code: CF_ROUTINGS_BLOG.UPDATE_POST,
					inc: path.resolve(__dirname, '../views/blogs/update_blog.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function(req, res) {
                        let { postID } = req.query;
                        let infoPost   = await BLOG_MODEL.getInfo({ postID });
                        let listCategory = await CATEGORY_BLOG_MODEL.getList({status: 1});
                        let listTagging = await TAGGING_MODEL.getList({status: 1});

                        ChildRouter.renderToView(req, res, {
                            infoPost: infoPost.data,
                            listCategory: listCategory.data,
                            listTagging: listTagging.data,
                        });
					}],
					post: [ uploadSingle, async function(req, res){
						let { _id: userUpdate } = req.user;
						let { 
							postID, title, category, taggings, slug, description, content, status, pathImage, arrImageGallryNew, arrImageGallryOld
						} = JSON.parse(req.body.data);
                        let image = null;
                        if(req.file){
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size, 
                                name: req.fileName, 
                                path: pathImage, // `/files/${req.fileName}`
                            });
                            image = infoImageAfterInsert.data._id;
                        }

						let dataAfterAddPost = await BLOG_MODEL.update({
							postID, title, category, taggings, slug, description, content, image, status, userUpdate, arrImageGallryNew, arrImageGallryDelete: arrImageGallryOld
						});
						res.json(dataAfterAddPost);
					}]
                },
            },

            /**
             * Function: Xóa post (API)
             * Date: 20/06/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.DELETE_POST]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { postID } = req.query;

						let dataAfterRemovePost = await BLOG_MODEL.delete({ postID });
						res.json(dataAfterRemovePost);
					}]
                },
            },

            /**
             * Function: Chi tiết post (API)
             * Date: 20/06/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.INFO_POST]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { postID } = req.query;
						let infoPost = await BLOG_MODEL.getInfo({ postID });
						res.json(infoPost);
					}]
                },
            },

            /**
             * Function: Chi tiết post (API)
             * Date: 20/06/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_BLOG.INFO_POST_BY_SLUG]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { slug, postID } = req.query;
						let infoPost = await BLOG_MODEL.getInfoBySlug({ slug, postID });
						res.json(infoPost);
					}]
                },
            },

            /**
             * Function: LIST POST (API, VIEW)
             * Date: 20/06/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.LIST_POST]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'view',
					title: 'List Posts - NANDIO',
					code: CF_ROUTINGS_BLOG.LIST_POST,
					inc: path.resolve(__dirname, '../views/blogs/list_blog.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { keyword, slug, type, sort, tab } = req.query;
						let listPosts = await BLOG_MODEL.getList({ keyword, slug, sort, tab });
						if(type === 'API'){
							return res.json(listPosts);
						}

						ChildRouter.renderToView(req, res, {
							listPosts: listPosts.data,
							tab
						});
                    }]
                },
            },

            /**
             * Function: LIST POST FOR ENDUSER
             * Date: 20/06/2021
             * Dev: DEPV
             */
            [CF_ROUTINGS_BLOG.LIST_POST_FOR_ENDUSER]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { limit, page, categoryID, tagID, keyword } = req.query;
						let listPosts = await BLOG_MODEL.getListForEnduser({ limit, page, categoryID, keyword, tagID });
                        return res.json(listPosts);
                    }]
                },
            },

			/**
             * =============================== ***************** ===============================
             * =============================== QUẢN LÝ COMMENT  ===============================
             * =============================== ***************** ===============================
             */

			/**
             * Function: Create comment (permission: all) (API)
             * Date: 21/06/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_BLOG.ADD_COMMENT]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						let { authorID, postID, content } = req.body;

						let infoAfterInsert = await COMMENT_MODEL.insert({
							authorID, postID, content 
						});
						res.json(infoAfterInsert);
					}]
                },
            },

			/**
             * Function: Update comment (permission: owner) (API)
             * Date: 21/06/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_BLOG.UPDATE_COMMENT]: {
                config: {
                    auth: [ roles.role.owner.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						let { commentID, status } = req.body;

						let infoAfterUpdate = await COMMENT_MODEL.updateStatus({ commentID, status });
						res.json(infoAfterUpdate);
					}]
                },
            },

			/**
             * Function: Info comment (permission: owner) (API)
             * Date: 21/06/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_BLOG.INFO_COMMENT]: {
                config: {
                    auth: [ roles.role.owner.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { commentID } = req.query;

						let infoComment = await COMMENT_MODEL.getInfo({ commentID });
						res.json(infoComment);
					}]
                },
            },

			/**
             * Function: Delete comment (permission: owner) (API)
             * Date: 21/06/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_BLOG.DELETE_COMMENT]: {
                config: {
                    auth: [ roles.role.owner.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { commentID } = req.query;

						let infoAfterDelete = await COMMENT_MODEL.delete({ commentID });
						res.json(infoAfterDelete);
					}]
                },
            },

			/**
             * Function: List Comment (permission: owner) (API, VIEW)
             * Date: 21/06/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_BLOG.LIST_COMMENT]: {
                config: {
                    auth: [ roles.role.owner.bin ],
					type: 'view',
                    view: 'index.ejs',
					title: 'List Comment - NANDIO',
					code: CF_ROUTINGS_BLOG.LIST_COMMENT,
					inc: path.resolve(__dirname, '../views/comments/list_comments.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        let { authorID, status, type } = req.query;
						let listComment = await COMMENT_MODEL.getList({ authorID, status });

						if(type === 'API'){
							return res.json(listComment);
						}

						ChildRouter.renderToView(req, res, {
							listComment: listComment.data,
						});
                    }]
                },
            },

            /**
             * Function: Xem danh sách Category blog
             * Date: 26/09/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_BLOG.LIST_CATEGORY_BLOG]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: "List Category - NANDIO",
                    code: CF_ROUTINGS_BLOG.LIST_CATEGORY_BLOG,
                    inc: path.resolve(__dirname, '../views/category_blog/list_category_blog'),
                    view: 'index.ejs',
                    type: 'view'
                },
                methods: {
                    get: [ async function (req, res) {
                        const listCategoryBlog = await CATEGORY_BLOG_MODEL.getList({});
                        ChildRouter.renderToView(req, res, { listCategoryBlog: listCategoryBlog.data });
                    }]
                },
            },

            /**
             * Function: Xem danh sách Category blog API
             * Date: 26/09/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_BLOG.LIST_CATEGORY_BLOG_API]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const listCategoryBlog = await CATEGORY_BLOG_MODEL.getList({status: 1});
                       res.json(listCategoryBlog)
                    }]
                },
            },

            /**
             * Function: Tạo Category blog
             * Date: 26/09/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_BLOG.ADD_CATEGORY_BLOG]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function(req, res){
						const { title, description, status, image } = req.body;

                        let imageID = null;
                        if(image){
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size: image.size,
                                name: image.name,
                                path: image.path
                            });
                            
                            imageID = infoImageAfterInsert.data._id;
                        }
                       
						let infoAfterInsert = await CATEGORY_BLOG_MODEL.insert({ title, description, status, image: imageID });
						res.json(infoAfterInsert);
					}]
                },
            },

            /**
             * Function: Xóa Category
             * Date: 26/09/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_BLOG.DELETE_CATEGORY_BLOG]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { categoryBlogID } = req.params;
                        const infoCategoryRemove = await CATEGORY_BLOG_MODEL.delete({ categoryBlogID });
                        res.json(infoCategoryRemove);
                    }]
                },
            },

            /**
             * Function: Info Category
             * Date: 26/09/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_BLOG.INFO_CATEGORY_BLOG]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { categoryBlogID } = req.params;
                        const infoCategory = await CATEGORY_BLOG_MODEL.getInfo({ categoryBlogID });
                        res.json(infoCategory);
                    }]
                },
            },

            /**
             * Function: Tạo Category blog
             * Date: 26/09/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_BLOG.UPDATE_CATEGORY_BLOG]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function(req, res){
                        const { categoryBlogID } = req.params;
						const { title, description, status, image } = req.body;

                        let imageID = null;
                        if(image){
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size: image.size,
                                name: image.name,
                                path: image.path
                            });
    
                            imageID = infoImageAfterInsert.data._id;
                        }
                        
						let infoAfterUpdate = await CATEGORY_BLOG_MODEL.update({ title, description, status, image: imageID, categoryBlogID });
						res.json(infoAfterUpdate);
					}]
                },
            },

            /**
             * Function: Xem danh sách stagging
             * Date: 26/09/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_BLOG.LIST_TAGGING]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: "List Category - NANDIO",
                    code: CF_ROUTINGS_BLOG.LIST_TAGGING,
                    inc: path.resolve(__dirname, '../views/tagging/list_tagging'),
                    view: 'index.ejs',
                    type: 'view'
                },
                methods: {
                    get: [ async function (req, res) {
                        const listTagging = await TAGGING_MODEL.getList({});
                        ChildRouter.renderToView(req, res, { listTagging: listTagging.data });
                    }]
                },
            },

            /**
             * Function: Xem danh sách stagging API
             * Date: 26/09/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_BLOG.LIST_TAGGING_API]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const listTagging = await TAGGING_MODEL.getList({status: 1});
                        res.json(listTagging)
                    }]
                },
            },


            /**
             * Function: Tạo tagging blog
             * Date: 26/09/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_BLOG.ADD_TAGGING]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function(req, res){
						const { title, description, status } = req.body;

						let infoAfterInsert = await TAGGING_MODEL.insert({ title, description, status });
						res.json(infoAfterInsert);
					}]
                },
            },

            /**
             * Function: Xóa Tagging
             * Date: 26/09/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_BLOG.DELETE_TAGGING]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { taggingID } = req.params;
                        const infoTaggingRemove = await TAGGING_MODEL.delete({ taggingID });
                        res.json(infoTaggingRemove);
                    }]
                },
            },

            /**
             * Function: Info Category
             * Date: 26/09/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_BLOG.INFO_TAGGING]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { taggingID } = req.params;
                        const infoTagging = await TAGGING_MODEL.getInfo({ taggingID });
                        res.json(infoTagging);
                    }]
                },
            },

            /**
             * Function: Cập nhật tagging
             * Date: 26/09/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_BLOG.UPDATE_TAGGING]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function(req, res){
                        const { taggingID } = req.params;
						const { title, description, status } = req.body;
						let infoAfterUpdate = await TAGGING_MODEL.update({ title, description, status, taggingID });
						res.json(infoAfterUpdate);
					}]
                },
            },
            

        }
    }
};
