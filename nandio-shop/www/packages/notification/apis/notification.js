"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { CF_ROUTINGS_NOTIFICATION } 		= require('../constants/notification.uri');
const {
    uploadSingle
} = require('../../../config/cf_helpers_multer');
const { TYPE_NOTIFICATION }             = require('../../../config/cf_constants');

/**
 * MODELS
 */
const CONFIG_NOTIFICATION_MODEL                   = require('../models/config_notification').MODEL;
const NOTIFICATION_MODEL                          = require('../models/notification').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ====================== ************************ ================================
             * ======================    NOTIFICATION    ================================
             * ====================== ************************ ================================
             */
            
			/**
             * Function: LIST NOTIFICATION OF DEVICEID
             * Date: 20/06/2021
             */
            [CF_ROUTINGS_NOTIFICATION.LIST_NOTIFICATION_OF_DEVICEID]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { deviceID, page, limit } = req.query;
                        
                        const listNotification = await NOTIFICATION_MODEL.getListNotificationOfDeviceWithPagingnation({ 
                            deviceID, page:+page, limit:+limit,
                        });
                        res.json(listNotification);
                    }]
                },
            },

            /**
             * Function: LIST NOTIFICATION OF DEVICEID
             * Date: 20/06/2021
             */
             [CF_ROUTINGS_NOTIFICATION.API_LIST_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { customer, type, page, limit } = req.query;
                        const listNotification = await NOTIFICATION_MODEL.getListNotification({ 
                            receive: customer, type, page, limit,
                        });
                        res.json(listNotification);
                    }]
                },
            },

            /**
             * Function: LIST NOTIFICATION OF DEVICEID
             * Date: 20/06/2021
             */
             [CF_ROUTINGS_NOTIFICATION.API_LIST_TYPE_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: receive } = req.customer;
                        const { types } = req.body;
                       
                        const listNotification = await NOTIFICATION_MODEL.getAmountNotificationUnSeen({ 
                            receive, types
                        });
                        res.json(listNotification);
                    }]
                },
            },
            
            [CF_ROUTINGS_NOTIFICATION.DEVICEID_SEEN_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { notificationID } = req.params;
                        const infoNotification = await NOTIFICATION_MODEL.updateSeenNotification({ notificationID });
                        res.json(infoNotification);
                    }]
                },
            },

            /**
             * Function: ADD CONFIG NOTIFICATION
             */
             [CF_ROUTINGS_NOTIFICATION.ADD_CONFIG_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    view: 'index.ejs',
					inc: path.resolve(__dirname, '../views/add_config_notification.ejs'),
					code: CF_ROUTINGS_NOTIFICATION.ADD_CONFIG_NOTIFICATION
                },
                methods: {
                    get: [ async function (req, res) {

                        ChildRouter.renderToView(req, res, {
                            TYPE_NOTIFICATION,
                        });
                    }],
                    post: [uploadSingle, async function (req, res) {
                        let { 
                            title,
                            description,
                            content,
                            type,
                            type_expression,
                            expression,
                            trial_program,
                            new_blog,
                            type_promotion,
                            dynamic_promotion
                        } = JSON.parse(req.body.data);
                        console.log({
                            title,
                            description,
                            content,
                            type,
                            type_expression,
                            expression,
                            trial_program,
                            new_blog,
                            type_promotion,
                            dynamic_promotion
                        });
                        let infoAfterInsert = await CONFIG_NOTIFICATION_MODEL.insert({ 
                            title, 
                            description, 
                            content, 
                            type, 
                            expression, 
                            type_expression, 
                            file: req.file, 
                            trial_program,
                            new_blog,
                            type_promotion,
                            dynamic_promotion
                        });

                        res.json(infoAfterInsert);
                    }],
                },
            },

            /**
             * Function: ADD CONFIG NOTIFICATION
             */
             [CF_ROUTINGS_NOTIFICATION.LIST_CONFIG_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    view: 'index.ejs',
					inc: path.resolve(__dirname, '../views/list_config_notification.ejs'),
					code: CF_ROUTINGS_NOTIFICATION.LIST_CONFIG_NOTIFICATION
                },
                methods: {
                    get: [ async function (req, res) {

                        let listConfigNotification = await CONFIG_NOTIFICATION_MODEL.getList({ })

                        ChildRouter.renderToView(req, res, {
                            TYPE_NOTIFICATION,
                            listConfigNotification: listConfigNotification.data
                        });
                    }],
                    
                },
            },
           
            [CF_ROUTINGS_NOTIFICATION.API_UPDATE_NOTIFICATIONS_BY_TYPE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: receive } = req.customer;
                        const { type } = req.body;
                       
                        const listNotificationAfterUpdate = await NOTIFICATION_MODEL.updateNotificationsByType({ 
                            receive, type
                        });
                        res.json(listNotificationAfterUpdate);
                    }]
                },
            },
        }
    }
};