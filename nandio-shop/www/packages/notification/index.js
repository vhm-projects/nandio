const NOTIFICATION_MODEL    = require('./models/notification').MODEL;
const NOTIFICATION_COLL     = require('./databases/notification-coll');
const NOTIFICATION_ROUTES   = require('./apis/notification');

module.exports = {
    NOTIFICATION_ROUTES,
    NOTIFICATION_COLL,
    NOTIFICATION_MODEL,
}