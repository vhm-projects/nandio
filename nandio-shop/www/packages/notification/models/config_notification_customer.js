"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                        = require('moment');

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const CONFIG_NOTIFICATION_CUSTOMER_COLL  		    = require('../databases/config_notification_customer-coll');

class Model extends BaseModel {
    constructor() {
        super(CONFIG_NOTIFICATION_CUSTOMER_COLL);
    }

	insert({ listCustomerReceiveNoti }) {
        return new Promise(async resolve => {
            try {
                
                let listDataAfterInsert = await CONFIG_NOTIFICATION_CUSTOMER_COLL.insertMany(listCustomerReceiveNoti)
                if(!listDataAfterInsert)
                    return resolve({ error: true, message: 'insert_config_notification_customer_failed' });
              
                
                return resolve({ error: false, data: listDataAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ config_notification }) {
        return new Promise(async resolve => {
            try {
                let listData = await CONFIG_NOTIFICATION_CUSTOMER_COLL.find({
                    config_notification
                }).populate('config_notification')
                
                if(!listData)
                    return resolve({ error: true, message: 'get_list_config_notification_customer_failed' });
                
                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
}
exports.MODEL = new Model;
