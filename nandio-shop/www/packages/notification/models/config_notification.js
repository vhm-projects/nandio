"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                        = require('moment');
const XlsxPopulate                  = require('xlsx-populate');
const fs                            = require('fs');
const agenda						= require('../../../config/cf_agenda');

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const CONFIG_NOTIFICATION_COLL  		    = require('../databases/config_notification-coll');
const CUSTOMER_MODEL            		    = require('../../customer/models/customer').MODEL;
const NOTIFICATION_MODEL            		= require('../models/notification').MODEL;
const CONFIG_NOTIFICATION_CUSTOMER_MODEL    = require('../models/config_notification_customer').MODEL;

class Model extends BaseModel {
    constructor() {
        super(CONFIG_NOTIFICATION_COLL);
        
        this.TYPE_NOTI_UPDATE_STATUS_ORDER  = 1;
        this.TYPE_NOTI_TRIAL_PROGRAM        = 2;
        this.TYPE_NOTI_NEW_BLOG             = 3;
        this.TYPE_NOTI_PROMOTION            = 4;
        this.TYPE_NOTI_BALANCE_FLUCTUATUION = 5;
        this.ARR_TYPE_VALID = [1,2,3,4,5];

        this.TYPE_EXPRESSION_NOW       = 1;
        this.TYPE_EXPRESSION_SCHEDULER = 2;
        this.ARR_TYPE_EXPRESSION_VALID = [1,2];

        agenda.define('push_notification', async (job, done) => {
            console.log(`========================================`);
            console.log(`done push_notification!!!!`);
            console.log({ 
                __JOB: job.attrs,
            });

            let { configNotificationID } = job.attrs.data;

            let listConfigNotiCustomer = await CONFIG_NOTIFICATION_CUSTOMER_MODEL.getList({ config_notification: configNotificationID })

            let { data: listConfigNotiCustomerAfterFind } = listConfigNotiCustomer;

            let listCustomerReceiveNoti = listConfigNotiCustomerAfterFind.map(item => {
                let dataInsert = {
                    title:       item.config_notification.title, 
                    description: item.config_notification.description, 
                    content:     item.config_notification.content, 
                    type:        item.config_notification.type, 
                    receive:     item.receive,
                    createAt:    new Date(),
                    modifyAt:    new Date(),
                };

                if(item.config_notification.trial_program){
                    dataInsert.trialProgram = item.config_notification.trial_program;
                }

                if(item.config_notification.blog){
                    dataInsert.blog = item.config_notification.blog;
                }

                if(item.config_notification.promotion && item.config_notification.onModel) {
                    dataInsert.onModel   = item.config_notification.onModel;
                    dataInsert.promotion = item.config_notification.promotion;
                }
                return dataInsert;
            });
            console.log({
                listCustomerReceiveNoti
            });
            if (listCustomerReceiveNoti.length) {
                let listNotiAfterInsert = await NOTIFICATION_MODEL.insertV2({ listNotification: listCustomerReceiveNoti });
            }

            // await agenda.cancel({ _id: job.attrs._id });
            done();
        });
    }

	insert({ 
        title, description, content, type, expression, type_expression, file,
        trial_program,
        new_blog,
        type_promotion,
        dynamic_promotion
    }) {
        return new Promise(async resolve => {
            try {
                if(!title)
                    return resolve({ error: true, message: 'Tiêu đề không hợp lệ' });
                if(!description)
                    return resolve({ error: true, message: 'Mô tả không hợp lệ' });

                if (Number.isNaN(Number(type)) || !this.ARR_TYPE_VALID.includes(Number(type)))
                    return resolve({ error: true, message: 'Loại thông báo không hợp lệ' });
                
                if (Number.isNaN(Number(type_expression)) || !this.ARR_TYPE_EXPRESSION_VALID.includes(Number(type_expression)))
                    return resolve({ error: true, message: 'Loại thời gian không hợp lệ' });

                if(!expression)
                    return resolve({ error: true, message: 'Thời gian không hợp lệ' });

                if(!file)
                    return resolve({ error: true, message: 'FILE EXCEL không hợp lệ' });

                let dataInsert = { title, description, type, type_expression, expression };

                if(content){
                    dataInsert.content = content;
                }

                if(trial_program && type == this.TYPE_NOTI_TRIAL_PROGRAM){
                    dataInsert.trial_program = trial_program;
                }

                if(new_blog && type == this.TYPE_NOTI_NEW_BLOG){
                    dataInsert.blog = new_blog;
                }

                if(type_promotion && dynamic_promotion && type == this.TYPE_NOTI_PROMOTION) {
                    if (type_promotion == 'BIG_SALE') {
                        dataInsert.onModel = 'big_sale';
                    }

                    if (type_promotion == 'FLASH_SALE') {
                        dataInsert.onModel = 'flash_sale';
                    }
                    dataInsert.promotion = dynamic_promotion;
                }
                console.log({
                    dataInsert
                });
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_config_notification_failed' });
                
                let listCustomerAfterInsertNoti = await this.insertNotificationBy__Customers({ 
                    file, title, description, content, type, 
                    configNotificationID: infoAfterInsert._id,
                    type_expression,
                    expression,
                    trial_program,
                    new_blog,
                    type_promotion,
                    dynamic_promotion
                });
                
                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    insertNotificationBy__Customers({ 
        file, title, description, content, type, configNotificationID, expression, type_expression,
        trial_program,
        new_blog,
        type_promotion,
        dynamic_promotion
    }) {
        return new Promise(async resolve => {
            try {
                XlsxPopulate.fromFileAsync(file.path)
                    .then(async workbook => {
                        let listCustomerReceiveNoti = [];
                        let listConfigNotiCustomer  = [];
                        let index = 2;
                        for (;true;) {
                            let phone       = workbook.sheet(0).cell(`A${index}`).value();
                            let fullname    = workbook.sheet(0).cell(`B${index}`).value();
                            
                            if(!phone) break;

                            let checkPhone = `${phone}`;
                            if (checkPhone[0] != '0') {
                                checkPhone = '0' + checkPhone;
                            }
                            phone    = checkPhone.trim();
                            fullname = fullname.trim();
                         
                            let infoCustomer = await CUSTOMER_MODEL.getInfo({ phone });

                            if (!infoCustomer.error) {
                                let conditionObj = {
                                    title, description, content, type, 
                                    receive: infoCustomer.data._id,
                                    createAt: new Date(),
                                    modifyAt: new Date(),
                                };
                                if(trial_program && type == this.TYPE_NOTI_TRIAL_PROGRAM){
                                    conditionObj.trialProgram = trial_program;
                                }
                
                                if(new_blog && type == this.TYPE_NOTI_NEW_BLOG){
                                    conditionObj.blog = new_blog;
                                }
                
                                if(type_promotion && dynamic_promotion && type == this.TYPE_NOTI_PROMOTION) {
                                    if (type_promotion == 'BIG_SALE') {
                                        conditionObj.onModel = 'big_sale';
                                    }
                
                                    if (type_promotion == 'FLASH_SALE') {
                                        conditionObj.onModel = 'flash_sale';
                                    }
                                    conditionObj.promotion = dynamic_promotion;
                                }
                                listCustomerReceiveNoti = [
                                    ...listCustomerReceiveNoti,
                                    conditionObj
                                ];

                                listConfigNotiCustomer = [
                                    ...listConfigNotiCustomer,
                                    {
                                        receive: infoCustomer.data._id,
                                        config_notification: configNotificationID,
                                        createAt: new Date(),
                                        modifyAt: new Date(),
                                    }
                                ];
                            }

                            index++;
                        }
                        console.log({
                            listCustomerReceiveNoti
                        });
                        await fs.unlinkSync(file.path);

                        if (listCustomerReceiveNoti.length) {
                            if (Number(type_expression) == this.TYPE_EXPRESSION_NOW) {
                                console.log("--------------------TYPE_EXPRESSION_NOW--------------------");
                                let listNotiAfterInsert = await NOTIFICATION_MODEL.insertV2({ listNotification: listCustomerReceiveNoti })
                                let listConfigNotiCustomerAfterInsert = await CONFIG_NOTIFICATION_CUSTOMER_MODEL.insert({ listCustomerReceiveNoti: listConfigNotiCustomer });

                                return resolve({ error: false, data: listNotiAfterInsert });
                            } else {
                                console.log("--------------------TYPE_EXPRESSION_SCHEDULER--------------------");
                                let listConfigNotiCustomerAfterInsert = await CONFIG_NOTIFICATION_CUSTOMER_MODEL.insert({ listCustomerReceiveNoti: listConfigNotiCustomer });
                            
                                await agenda.start();
                                await agenda.every(expression, "push_notification", { configNotificationID });
                                return resolve({ error: false, dảtâ: listConfigNotiCustomerAfterInsert });
                            }
                        } else {
                            return resolve({ error: true, message: 'Danh sách khách hàng không hợp lệ' });
                        }
                    });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    getList({ }) {
        return new Promise(async resolve => {
            try {
                let listConfigNoti = await CONFIG_NOTIFICATION_COLL.find({}).sort({ createAt: -1 });
                if (!listConfigNoti) {
                    return resolve({ error: true, message: 'cannont_get_list_config_noti' });
                }

                return resolve({ error: false, data: listConfigNoti });

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
}
exports.MODEL = new Model;
