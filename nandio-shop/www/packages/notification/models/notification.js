"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                        = require('moment');
/**
 * BASE
 */
const BaseModel 					  = require('../../../models/intalize/base_model');
const { checkObjectIDs }              = require('../../../utils/utils');
const { sendMessageMobile__Customer } = require('../../../fcm/utils');
/**
 * COLLECTIONS
 */
const NOTIFICATION_COLL  		    = require('../databases/notification-coll');
const TRIAL_PROGRAM_COLL  		    = require('../../trial_program/databases/trial_program-coll');
const VIEW_TRIAL_PROGRAM_MODEL      = require('../../trial_program/models/view_trial_program').MODEL;

class Model extends BaseModel {
    constructor() {
        super(NOTIFICATION_COLL);

        this.ARR_TYPE_VALID = [1,2,3,4,5];
    }

	insert({ title, description, content, type, trialProgram, sender, userDevice, deviceID }) {
        return new Promise(async resolve => {
            try {
                if(!title || !type ||!userDevice)
                    return resolve({ error: true, message: 'params_invalid' });

                let dataInsert = { title, type, sender, userDevice, deviceID };
                
                if(trialProgram){
                    dataInsert.trialProgram = trialProgram;
                }

                if(description){
                    dataInsert.description = description;
                }

                if(content){
                    dataInsert.content = content;
                }
                
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_notification_failed' });
                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    insertV2({ listNotification }) {
        return new Promise(async resolve => {
            try {
                let listNotificationAfterInsert = await NOTIFICATION_COLL.insertMany(listNotification)
                if(!listNotificationAfterInsert)
                    return resolve({ error: true, message: 'insert_list_notification_failed' });

                let title = '';
                let description = '';
                let body = {
                    // screen_key: 'SCREEN',
                };

                let listCustomerID = listNotification.map(item => {
                    title       = item.title;
                    description = item.description;
                    if(item.trialProgram){
                        let screen_key = '';
                        if (item.screen_key) {
                            screen_key = item.screen_key
                        } else {
                            screen_key = 'DetailTrialProgramScreen';
                        }
                        console.log({
                            screen_key
                        });
                        body = {
                            // screen_key: 'DetailTrialProgramScreen',
                            screen_key: screen_key,
                            trialProgramID: item.trialProgram,
                        }
                    }
    
                    if(item.blog){
                        body = {
                            screen_key: 'DetailPostScreen',
                            blogID: item.blog
                        }
                    }
    
                    if(item.promotion && item.onModel) {
                        if (item.onModel == 'big_sale') {
                            body = {
                                screen_key: 'DetailVoucherScreen',
                            }
                        } 

                        if (item.onModel == 'flash_sale') {
                            body = {
                                screen_key: 'FlashSaleScreen',
                            }
                        }
                        body.promotionID = item.promotion;
                    }

                    if (item.centralize_order) {
                        body = {
                            screen_key: 'HistoryOrderScreenNew',
                            centralizeOrderID: item.centralize_order
                        }
                    }

                    if (item.history_point) {
                        body = {
                            screen_key: item.screen_key, // GỬI TỪ HISTORY_POINT_MODEL qua
                            historyPointID: item.history_point
                        }
                    }
                   
                    body.type = item.type;
                    return item.receive.toString();
                });
                console.log({
                    body, description, title
                });
                
                sendMessageMobile__Customer({
                    title,
                    description,
                    arrReceiverID: listCustomerID,
                    body
                });

                return resolve({ error: false, data: listNotificationAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListNotificationOfDeviceWithPagingnation({ deviceID, page = 1, limit = 20 }) {
        return new Promise(async resolve => {
            try {
                if(!deviceID)
                    return resolve({ error: true, message: 'params_invalid' });
                let listNotification = await NOTIFICATION_COLL.find({ deviceID })
                                    .select("title description createAt type trialProgram status")
                                    .populate({
                                        path: "trialProgram",
                                        match: {
                                            status: 1,
                                        },
                                        select: "title status",
                                    })
                                    .skip((page * limit) - limit)
                                    .limit(limit)
                                    .sort({ createAt: -1 });

                let listNotificationAfterFormat = listNotification.filter(item => {
                    if(item.type == 2){
                        if(item.trialProgram){
                            return item
                        }
                    }else{
                        return item
                    }
                })

                if(!listNotificationAfterFormat)
                    return resolve({ error: true, message: 'get_notification_failed' });

                let totalItem = await NOTIFICATION_COLL.count({ deviceID });
                let pages = Math.ceil(totalItem/limit);

                // let amountNotifiNotseen = await NOTIFICATION_COLL.count({ deviceID, status: 0 });
                let amountNotifiNotseen = await NOTIFICATION_COLL.aggregate([
                    {
                        $match: {
                            deviceID,
                            status: 0
                        }
                    },
                    {
                       $lookup:{
                             from: "trial_programs",
                             localField: "trialProgram",
                             foreignField: "_id",
                             as: "trialProgram"
                        }
                    },
                    {
                        $unwind: "$trialProgram"
                    },
                    {
                        $match: {
                        "trialProgram.status": 1,
                        }
                    },
                    {
                        $count: "amountNoti"
                    }
                ])
                return resolve({ error: false, data: { listNotificationAfterFormat, page: +page, pages: +pages, amountNotifiNotseen: amountNotifiNotseen[0] && amountNotifiNotseen[0].amountNoti || 0 } });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateSeenNotification({ notificationID }) {
        return new Promise(async resolve => {
            try {
                if(!notificationID)
                    return resolve({ error: true, message: 'params_invalid' });
                let infoNotification = await NOTIFICATION_COLL.findByIdAndUpdate(notificationID, { status: 1 }, { new: true });
                
                if(!infoNotification)
                    return resolve({ error: true, message: 'cannot_update_notification' });

                // Nếu thông báo thuộc trialProgram thì cập nhật số lượng lượt xem cho trialprogram
                if(infoNotification.trialProgram){
                    await TRIAL_PROGRAM_COLL.findByIdAndUpdate(infoNotification.trialProgram, { $inc: { views: 1 } });
                    // Cập nhật trạng thái đã xem trial program
                    await VIEW_TRIAL_PROGRAM_MODEL.updateStatus({ trialProgramID: infoNotification.trialProgram, deviceID: infoNotification.deviceID, status: 1 });
                }
                return resolve({ error: false, data: infoNotification });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListNotification({ receive, type, page = 1, limit = 20,  }) {
        return new Promise(async resolve => {
            try {
                limit = +limit;
                page  = +page;
                
                let condition = {};
                if (type) {
                    if (Number.isNaN(Number(type)) || !this.ARR_TYPE_VALID.includes(Number(type)))
                        return resolve({ error: true, message: 'Loại thông báo không hợp lệ' });
                    condition.type = type;
                }

                if (receive) { 
                    if (!checkObjectIDs(receive))
                        return resolve({ error: true, message: 'Khách hàng không hợp lệ' });
                    condition.receive = receive;
                }

                let skip = (page - 1) * limit;
                let listNotification = await NOTIFICATION_COLL.find(condition)
                    .skip(skip)
                    .limit(limit)
                    .sort({ createAt: -1 })
                    .select("title description createAt type trialProgram blog promotion onModel receive status")
                    .populate({
                        path: "trialProgram",
                        select: "title status",
                    })
                    .populate({
                        path: "blog",
                        select: "title status",
                    })
                    .populate({
                        path: "promotion",
                        select: "name status",
                    });

                if(!listNotification)
                    return resolve({ error: true, message: 'get_notifications_failed' });

                let totalItem = await NOTIFICATION_COLL.count(condition);
                console.log({
                    totalItem,
                    limit,
                    page
                });
                let pages = Math.ceil(totalItem/limit);

                return resolve({ 
                    error: false, 
                    data: { 
                        listNotification, 
                        page: +page, 
                        pages: +pages, 
                    } 
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getAmountNotificationUnSeen({ 
        receive, types
    }) {
        return new Promise(async resolve => {
            try {
                if (types && types.length) {
                    if (!checkObjectIDs(receive))
                        return resolve({ error: true, message: 'Khách hàng không hợp lệ' });

                    const STATUS_INACTIVE = 0;
                    let countNotificationUnSeen = async (type, receive) => {
                        let infoNotiAfterCount = await NOTIFICATION_COLL.count({
                            type,
                            receive,
                            status: STATUS_INACTIVE
                        });

                        return {
                            type,
                            countNotiUnSeen: infoNotiAfterCount
                        }
                    }

                    let listNotification = types.map(item => {
                        return countNotificationUnSeen(item, receive);
                    });

                    let result = await Promise.all(listNotification);
                    return resolve({ 
                        error: false, 
                        data: result
                    });
                } else {
                    return resolve({ error: true, message: 'Loại thông báo không hợp lệ' });
                }
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateNotificationsByType({ 
        receive, type
    }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(receive))
                    return resolve({ error: true, message: 'Khách hàng không hợp lệ' });

                if (!type) {
                    return resolve({ error: true, message: 'Loại thông báo không hợp lệ' });
                }

                if (Number.isNaN(Number(type)) || !this.ARR_TYPE_VALID.includes(Number(type)))
                    return resolve({ error: true, message: 'Loại thông báo không hợp lệ' });
                
                const STATUS_ACTIVE = 1;
                let listNotificationAfterUpdate = await NOTIFICATION_COLL.updateMany({
                    receive, type
                }, {
                    status: STATUS_ACTIVE
                });

                if (!listNotificationAfterUpdate) {
                    return resolve({ error: true, message: 'Không thể cập nhật trạng thái thông báo' });
                }

                return resolve({ 
                    error: false, 
                    data: listNotificationAfterUpdate
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}
exports.MODEL = new Model;
