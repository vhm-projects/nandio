const BASE_ROUTE = '/notification';
const BASE_ROUTE_AMIND = '/admin';
const API = '/api';

const CF_ROUTINGS_NOTIFICATION = {
	LIST_NOTIFICATION_OF_DEVICEID: `${BASE_ROUTE}/list-notification-of-deviceid`,
	DEVICEID_SEEN_NOTIFICATION: `${BASE_ROUTE}/deviceid-seen-notification/:notificationID`,
	API_LIST_NOTIFICATION:      `/api${BASE_ROUTE}/list-notification`,
	API_LIST_TYPE_NOTIFICATION: `/api${BASE_ROUTE}/list-type-notification`,
	API_UPDATE_NOTIFICATIONS_BY_TYPE: `/api${BASE_ROUTE}/update-notifications-by-type`,
    
	// CONFIG NOTIFICATION 
	ADD_CONFIG_NOTIFICATION:  `${BASE_ROUTE}/add-config-notification`,
	LIST_CONFIG_NOTIFICATION: `${BASE_ROUTE}/list-config-notification`,
	
	ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_NOTIFICATION = CF_ROUTINGS_NOTIFICATION;
