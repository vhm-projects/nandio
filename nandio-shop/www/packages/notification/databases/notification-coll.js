"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');
/**
 * QUẢN LÝ THÔNG BÁO
 */
module.exports = BASE_COLL('notification', {
	title: String,
	description: String,
	content: String,
	/**
	 * 1: Thông báo bình thường
	 * 2: Thông báo trial program
	 */
	type: {
		type: Number,
		default: 1
	},
	trialProgram: {
        type: Schema.Types.ObjectId,
        ref: 'trial_program'
    },

	blog: {
		type:  Schema.Types.ObjectId,
		ref : 'blog'
	},

	promotion: {
		type:  Schema.Types.ObjectId,
		refPath: 'onModel'
	},
	onModel: {
		type: String,
		enum: ['big_sale', 'flash_sale']
	},

	blog: {
		type:  Schema.Types.ObjectId,
		ref : 'blog'
	},

	centralize_order: {
		type:  Schema.Types.ObjectId,
		ref : 'centralize_order'
	},

	history_point: {
		type:  Schema.Types.ObjectId,
		ref : 'history_point'
	},
	/**
	 * Người tạo thông báo
	 */
	sender: {
		type:  Schema.Types.ObjectId,
		ref : 'user'
	},
	/**
	 * Người nhận thông báo
	 */
	receive: {
		type:  Schema.Types.ObjectId,
		ref : 'customer'
	},
	/**
	 * Thiết bị nhận
	 */
	userDevice: {
		type:  Schema.Types.ObjectId,
		ref : 'user_device'
	},
	// Lưu để dễ truy vấn
	deviceID: String,
	/**
	 * Trạng thái xem thông báo(0 chưa xem/ 1 đã xem)
	 */
	status: {
		type: Number,
		default: 0
	},
});
