"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');
/**
 * QUẢN LÝ THÔNG BÁO
 */
module.exports = BASE_COLL('config_notification_customer', {
	/**
	 * Người nhận thông báo
	 */
	 receive: {
		type:  Schema.Types.ObjectId,
		ref : 'customer'
	},
	/**
	 * Cấu hình thông báo
	 */
	config_notification: {
		type:  Schema.Types.ObjectId,
		ref : 'config_notification'
	},
});
