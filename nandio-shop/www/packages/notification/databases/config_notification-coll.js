"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');
/**
 * QUẢN LÝ THÔNG BÁO
 */
module.exports = BASE_COLL('config_notification', {
	title: String,
	description: String,
	content: String,
	/**
	 * 1: Thông báo bình thường => Trạng thái đơn hàng
	 * 2: Thông báo trial program
	 * 3: Thông báo tin tức mới
	 * 4: Khuyến mãi
	 * 5: Biến động số dư
	 */
	type: {
		type: Number,
		default: 1
	},

	trial_program: {
		type:  Schema.Types.ObjectId,
		ref : 'trial_program'
	},

	blog: {
		type:  Schema.Types.ObjectId,
		ref : 'blog'
	},

	promotion: {
		type:  Schema.Types.ObjectId,
		refPath: 'onModel'
	},
	onModel: {
		type: String,
		enum: ['big_sale', 'flash_sale']
	},

	/**
	 * 1. NGAY BÂY GIỜ
	 * 2. LÊN LỊCH
	 */
	type_expression: {
		type: Number,
		default: 1
	},

	expression: String,
	
	/**
	 * Trạng thái xem thông báo
	 * 	1: không hoạt động
	 *  2: hoạt động)
	 */
	status: {
		type: Number,
		default: 1
	},
});
