"use strict";

const Schema 	= require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');


module.exports = BASE_COLL('referral_code', {
    /**
     * Khách hàng đăng ký tài khoản
     */
    customer: {
		type: Schema.Types.ObjectId,
		ref: 'customer'
	},
     /**
     * Khách hàng được nhận điểm
     */
    customerReceive: {
		type: Schema.Types.ObjectId,
		ref: 'customer'
	},
    /**
     * Số điện thoại người được nhận điểm
     */
    phone: String,
    /**
     * Số điểm nhận
     */
    point: {
        type: Number
    }
});
