const BASE_ROUTE = '/version';

const CF_ROUTINGS_VERSION = {
	ADD_VERSION: `${BASE_ROUTE}/add-version`,
	LIST_VERSION: `${BASE_ROUTE}/list-version`,
	GET_LASTED_RECORD_VERSION: `${BASE_ROUTE}/lasted-version`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_VERSION = CF_ROUTINGS_VERSION;
