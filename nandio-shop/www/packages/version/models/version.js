"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { checkObjectIDs }            = require("../../../utils/utils");
/**
 * MODELS
 */

/**
 * COLLECTIONS
 */
const VERSION_COLL  					= require('../databases/version-coll');


class Model extends BaseModel {
    constructor() {
        super(VERSION_COLL);
    }

	insert({ name, timeCreate, platform }) {
        return new Promise(async resolve => {
            try {
                if(!name)
                    return resolve({ error: true, message: 'Tên phiên bản không hợp lệ' });
                if(!timeCreate)
                    return resolve({ error: true, message: 'Thời gian tạo phiên bản không hợp lệ' });
                if(!platform)
                    return resolve({ error: true, message: 'Platform không hợp lệ' });

                let dataInsert = { name, timeCreate: new Date(timeCreate), platform };

                let infoVersionInsert = await this.insertData(dataInsert);
                if(!infoVersionInsert)
                    return resolve({ error: true, message: 'Thêm danh mục thất bại, hoặc tên danh mục đã tồn tại' });

                return resolve({ error: false, data: infoVersionInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getLastedRecord({ platform }) {
        return new Promise(async resolve => {
            try {
                if (!platform) return resolve({ error: true, message: 'Platform không hợp lệ' });
                if (![1, 2].includes(Number(platform))) return resolve({ error: true, message: 'Loại Platform không hợp lệ' });

                let infoVersion = await VERSION_COLL.findOne({ platform }).sort({ createAt: -1 })
                if(!infoVersion)
                    return resolve({ error: true, message: 'cannot_get_info_version' });

                return resolve({ error: false, data: infoVersion });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({}) {
        return new Promise(async resolve => {
            try {
                let listVersion = await VERSION_COLL.find({ }).sort({ createAt: -1 })
                    
                if(!listVersion)
                    return resolve({ error: true, message: 'cannot_get_List_version' });

                return resolve({ error: false, data: listVersion });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
