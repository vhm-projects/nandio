"use strict";

const Schema    = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION CATEGORY CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('version', {
	// Tên version
	name: {
		type: String,
	},

	// Thời gian tạo version
    timeCreate: {
		type: Date,
	},

	// Platform: 
	// 1: Android
	// 2: IOS
	platform: {
		type: Number,
	}
	
});
