const ORDER_MODEL    = require('./models/order').MODEL;
const ORDER_COLL     = require('./databases/order-coll');
const ORDER_ROUTES   = require('./apis/order');

module.exports = {
    ORDER_ROUTES,
    ORDER_COLL,
    ORDER_MODEL,
}