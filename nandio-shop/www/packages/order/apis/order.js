"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const TOKEN_STORE = process.env.TOKEN_STORE;
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { CF_ROUTINGS_ORDER } 			    = require('../constants/order.uri');
const { PAYMENT_TYPE, ORDER_STATUS_TYPE, STATUS_ORDER, TYPE_CENTRALIZE_ORDER }   = require('../../../../www/config/cf_constants');

/**
 * MODELS
 */
const ORDER_MODEL 		        = require('../models/order').MODEL;
const ORDER_LINE_MODEL	        = require('../models/order_line').MODEL;
const CENTRALIZE_ORDER_MODEL	= require('../models/centralize_order').MODEL;
const MERGE_CENTRALIZE_ORDER_MODEL	= require('../models/merge_centralize_order').MODEL;

/**
 * COLLECTIONS
 */
const { PRODUCT_COLL } 	= require('../../product');
const { CUSTOMER_COLL } = require('../../customer');
const  ADDRESS_MODEL    = require('../../address/models/address').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ ORDER ===============================
             * =============================== ************* ===============================
             */

             /**
             * Function: Tạo đơn hàng (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            // [CF_ROUTINGS_ORDER.ADD_ORDER]: {
            //     config: {
            //         auth: [ roles.role.customer.bin ],
			// 		type: 'json',
            //     },
            //     methods: {
			// 		post: [ async (req, res) => {
			// 			const { 
			// 				orderLine, customerID, note, amount, provisionalAmount,
			// 				deliveryPrice, deliveryDate, voucher, payment, addressID, email
			// 			} = req.body;

			// 			const dataAfterAdd = await ORDER_MODEL.insert({
			// 				orderLine, customerID, note, amount, provisionalAmount,
			// 				deliveryPrice, deliveryDate, voucher, payment, addressID, email
			// 			});
			// 			res.json(dataAfterAdd);
			// 		}]
            //     },
            // },

            [CF_ROUTINGS_ORDER.ADD_ORDER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async (req, res) => {
                        let { _id: customerID } = req.customer;
						const { 
							orderLines, deliveryPrice, promotion, payment, addressID
						} = req.body;

						const dataAfterAdd = await ORDER_MODEL.insert({
                            orderLines, deliveryPrice, promotion, payment, addressID, customerID
						});
						res.json(dataAfterAdd);
					}]
                },
            },

             /**
             * Function: Cập nhật trạng thái đơn hàng (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.UPDATE_STATUS_ORDER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async (req, res) => {
						const { orderID, status } = req.body;

						const infoAfterUpdateStatus = await ORDER_MODEL.updateStatus({
							orderID, status
						});
						res.json(infoAfterUpdateStatus);
					}]
                },
            },

			/**
             * Function: Cập nhật đơn hàng (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.UPDATE_ORDER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						const { 
							orderID, orderLine, customerID, note, amount, provisionalAmount,
							deliveryPrice, deliveryDate, voucher, payment, addressID, status
						} = req.body;

						const infoAfterUpdate = await ORDER_MODEL.update({
                            orderID, orderLine, customerID, note, amount, provisionalAmount,
							deliveryPrice, deliveryDate, voucher, payment, addressID, status
						});
						res.json(infoAfterUpdate);
					}]
                },
            },

			/**
             * Function: Xóa đơn hàng (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.DELETE_ORDER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						const { orderID } = req.query;

						const infoAfterDelete = await ORDER_MODEL.delete({ orderID });
						res.json(infoAfterDelete);
					}]
                },
            },

            /**
             * Function: Thông tin đơn hàng (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.INFO_ORDER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
						const { orderID } = req.query;

						const infoOrder = await CENTRALIZE_ORDER_MODEL.getCentralizeOrder({ 
                            orderID 
                        });
						res.json(infoOrder);
					}]
                },
            },

			/**
             * Function: Danh sách đơn hàng (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.API_LIST_ORDER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
                        const { _id: customerID } = req.customer;
						const { page, limit } = req.query;
						const listOrder = await ORDER_MODEL.getListOrderOfCustomer({ customerID, page: +page, limit: +limit });
						res.json(listOrder);
					}]
                },
            },


			/**
             * Function: Danh sách đơn hàng theo bộ lọc (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.API_LIST_ORDER_FILTER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
						const { 
							keyword, status, productID, customerID, payment, fromDate, toDate, page, limit
						 } = req.query;

						const listOrder = await ORDER_MODEL.getListByFilterWithPaging({
							keyword, status, productID, customerID, payment, fromDate, toDate, page, limit
						});
						res.json(listOrder);
					}]
                },
            },

            /**
             * Function: Chi tiết đơn hàng (VIEW)
             * Date: 23/11/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.DETAIL_ORDER]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'view',
                    view: 'index.ejs',
					title: 'Detail Order - NANDIO',
					code: CF_ROUTINGS_ORDER.DETAIL_ORDER,
					inc: path.resolve(__dirname, '../views/detail_order.ejs')
                },
                methods: {
					get: [ async (req, res) => {
						const { orderID } = req.query;
						const infoOrder = await ORDER_MODEL.getInfo({ orderID });

						ChildRouter.renderToView(req, res, { 
                            infoOrder: infoOrder.data || {},
                            PAYMENT_TYPE,
                            ORDER_STATUS_TYPE
                        })
					}]
                },
            },

			/**
             * Function: Danh sách đơn hàng theo bộ lọc (VIEW)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.LIST_ORDER]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'view',
                    view: 'index.ejs',
					title: 'List Order - NANDIO',
					code: CF_ROUTINGS_ORDER.LIST_ORDER,
					inc: path.resolve(__dirname, '../views/list_order.ejs')
                },
                methods: {
					get: [ async (req, res) => {
						const { 
							keyword, status, productID, customerID, payment, fromDate, toDate
						} = req.query;

						const listOrder = await ORDER_MODEL.getListByFilter({
							keyword, status, productID, customerID, payment, fromDate, toDate
						});

						if(listOrder.error){
							return ChildRouter.renderToView(req, res, {
								listOrder: [],
								currentPage: 0,
								perPage: 0,
								total: 0
							});
						}

						ChildRouter.renderToView(req, res, {
							listOrder: listOrder.data || [],
						});
					}]
                },
            },
			/**
             * =============================== ********************* ===============================
             * =============================== QUẢN LÝ ORDER - LINE ===============================
             * =============================== ********************* ===============================
             */

			/**
             * Function: Create order line (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.ADD_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async (req, res) => {
                        let { _id: customerID } = req.customer;
                        // Tạm đóng code cũ của Minh
						// const { 
						// 	productID, amount, quantities, note,
						// 	typeDiscount, discount, provisionalAmount
						// } = req.body;
 
						// const infoAfterInsert = await ORDER_LINE_MODEL.insert({ 
						// 	productID, amount, quantities, note,
						// 	typeDiscount, discount, provisionalAmount
						// });

                        // Code của Đệ
                        const { productID, quantities, note } = req.body;
 
						const infoAfterInsert = await ORDER_LINE_MODEL.insert({ 
							productID, customerID, quantities, note
						});
						res.json(infoAfterInsert);
					}]
                },
            },

            /**
             * Function: Thông tin giỏ hàng (API)
             * Date: 03/07/2021
             * Dev: Depv
             */
            [CF_ROUTINGS_ORDER.API_INFO_CART]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
                        let { _id: customerID } = req.customer;
                        let { page, limit } = req.query;
                        const infoAddressDefault = await ADDRESS_MODEL.getInfoAddressDefault({ customerID });
                        // Danh sách orderLine
						const listOrderLine = await ORDER_LINE_MODEL.getListOrderLineOfCustomer({ 
							customerID, page, limit
						});  
						res.json({ 
                            listOrderLine, 
                            infoAddressDefault: infoAddressDefault.data,
                            PAYMENT_TYPE
                        });
					}]
                },
            },
  
            /**
             * Function: Tính toán giá trong orderline (API)
             * Date: 20/11/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.API_CALCULATE_PRICE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async (req, res) => {
                        let { _id: customerID } = req.customer;
                        let { orderLinesID, promotion } = req.body;

						const listOrderLineDiscount = await ORDER_LINE_MODEL.calculatePrice({
							customerID, orderLinesID, promotion
						});
						res.json(listOrderLineDiscount);
					}]
                },
            },

            /**
             * Function: Danh sách tất cả orderLine
             * Date: 03/07/2021
             * Dev: Depv
             */
            [CF_ROUTINGS_ORDER.API_ALL_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
                        let { _id: customerID } = req.customer;
						const listOrderLine = await ORDER_LINE_MODEL.getAllOrderLineOfCustomer({ 
							customerID
						});  
						res.json(listOrderLine);
					}]
                },
            },

            /**
             * Function: Danh sách hình thức thanh toán (API)
             * Date: 03/07/2021
             * Dev: Depv
             */
            [CF_ROUTINGS_ORDER.API_LIST_PAYMENT_METHOD]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
						res.json(PAYMENT_TYPE);
					}]
                },
            },

			/**
             * Function: Update order line (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.UPDATE_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async (req, res) => {
						// const { 
						// 	orderLineID, amount, quantities, note, provisionalAmount
						// } = req.body;
                        const { orderLineID, quantities, promotions } = req.body;

						const infoAfterUpdate = await ORDER_LINE_MODEL.update({ 
							orderLineID, quantities
						});
						res.json(infoAfterUpdate);
					}]
                },
            },

			/**
             * Function: Delete order line (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_ORDER.DELETE_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
						const { orderLineID } = req.query;

						const infoAfterDelete = await ORDER_LINE_MODEL.delete({ orderLineID });
						res.json(infoAfterDelete);
					}]
                },
            },

			/**
             * Function: Info order line (API)
             * Date: 03/07/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_ORDER.INFO_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
						const { orderLineID } = req.query;
						const infoOrderLine = await ORDER_LINE_MODEL.getInfo({ orderLineID });
						res.json(infoOrderLine);
					}]
                },
            },

            /**
             * =============================== ************************** ===============================
             * =============================== QUẢN LÝ CENTRALIZE - ORDER ===============================
             * =============================== ************************** ===============================
             */

            [CF_ROUTINGS_ORDER.LIST_CENTRALIZE_ORDER]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'view',
                    view: 'index.ejs',
					title: 'List Order - NANDIO',
					code: CF_ROUTINGS_ORDER.LIST_CENTRALIZE_ORDER,
					inc: path.resolve(__dirname, '../views/list_centralize_order.ejs')
                },
                methods: {
					get: [ async (req, res) => {
                        const { key, value } = req.params;
						const { city, district, ward } = req.query;
                        // console.log({
                        //     city, district, ward
                        // });
						// const listCentralizeOrder = await CENTRALIZE_ORDER_MODEL.getList({ city, district, ward });
                        // if(listCentralizeOrder.error){
						// 	return ChildRouter.renderToView(req, res, {
						// 		listCentralizeOrder: [],
						// 		currentPage: 0,
						// 		perPage: 0,
						// 		total: 0
						// 	});
						// }
                       
						ChildRouter.renderToView(req, res, {
							// listCentralizeOrder: listCentralizeOrder.data || [],
                            key, value,
                            TYPE_CENTRALIZE_ORDER,
                            city, district, ward
						});
					}]
                },
            },

            [CF_ROUTINGS_ORDER.LIST_CENTRALIZE_ORDER_SERVER_SIDE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async (req, res) => {
						const { kind, city, district, ward, fromDate, toDate, start, length, search } = req.body;
                        let page = Number(start)/Number(length) + 1;
                        console.log({
                            start, length, search, page
                        });
						const listCentralizeOrder = await CENTRALIZE_ORDER_MODEL.getList({ 
                            city, district, ward, kind, fromDate, toDate,
                            page: Number(page), 
                            limit: Number(length), 
                            keyword: search.value 
                        });
                       
						res.json(listCentralizeOrder)
					}]
                },
            },

            [CF_ROUTINGS_ORDER.LIST_CENTRALIZE_ORDER_CHOICE]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async (req, res) => {
                        const { listCentralizeID, start, length, search } = req.body;
                        let page = Number(start)/Number(length) + 1;
                       
						const listCentralizeOrder = await CENTRALIZE_ORDER_MODEL.getListByListCentralizeID({ 
                            listCentralizeID, 
                            page:  page ? Number(page) : '1', 
                            limit: length ? Number(length) : '100', 
                            keyword: search ? search.value : ''
                        });
                        console.log({
                            listCentralizeOrder
                        });
                       
                        res.json(listCentralizeOrder)
					}]
                },
            },

            [CF_ROUTINGS_ORDER.GET_DELIVERY_BY_CENTRALIZE_ORDER_ID]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async (req, res) => {
                        let { 
                            weight,
                            length,
                            width,
                            height,
                            centralizeOrderGetAddress,
                            wardStore,
                            districtStore,
                            storeID,
                        } = req.body;

						const listCentralizeOrder = await CENTRALIZE_ORDER_MODEL.calculateFeeGHN({ 
                            weight,
                            length,
                            width,
                            height,
                            centralizeOrderGetAddress,
                            wardStore,
                            districtStore,
                            storeID
                        });
                        
                        res.json(listCentralizeOrder)
					}]
                },
            },

            [CF_ROUTINGS_ORDER.ADD_MERGE_CENTRALIZE_ORDER]: { 
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async (req, res) => {
						let { _id: userCreate } = req.user;
                        let { 
                            id: centralizeOrderID,
							weight,
							length,
							width,
							height,
							agency
                        } = req.body;
                       
						const infoAfterInsert = await MERGE_CENTRALIZE_ORDER_MODEL.insert({ 
                            // centralizeOrders,
                            // deliveryPrice,
                            // weight,
                            // length,
                            // width,
                            // height,
                            // userCreate,
                            // centralizeOrderGetAddress,
                            // wardStore,
                            // districtStore,
                            // storeID,
                            // ward,
                            // district,
                            // city,
                            // cc,
                            // bcc,
                            // to
                            centralizeOrderID,
                            weight,
							length,
							width,
							height,
							agency,
                            userCreate
                        });
						res.json(infoAfterInsert)
					}]
                },
            },

            [CF_ROUTINGS_ORDER.LIST_MERGE_CENTRALIZE_ORDER]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async (req, res) => {
                        let { kind, start, length, search } = req.body;
                        let page = Number(start)/Number(length) + 1;
                        // console.log({
                        //     kind
                        // });
						const listMergeCentralizeOrder = await MERGE_CENTRALIZE_ORDER_MODEL.getList({
                            kind,
                            page: Number(page), 
                            limit: Number(length), 
                            keyword: search.value 
                        });
						res.json(listMergeCentralizeOrder)
					}]
                },
            },

            [CF_ROUTINGS_ORDER.API_LIST_CENTRALIZE_ORDER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
                        let { _id: customerID } = req.customer;
                        let { page, limit, status, kind, group } = req.query;
                       
						const listMergeCentralizeOrder = await CENTRALIZE_ORDER_MODEL.getCentralizeOrders({ 
                            status,
                            customerID,
                            page, 
                            limit, 
                            kind,
                            group
                        });
                        
						res.json({
                            ...listMergeCentralizeOrder,
                            STATUS_ORDER
                        })
					}]
                },
            },

            [CF_ROUTINGS_ORDER.API_LIST_CENTRALIZE_ORDER_NOT_SEEN]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
                        let { _id: customerID } = req.customer;
                        let { page, limit, seen } = req.query;
                       
						const listMergeCentralizeOrder = await CENTRALIZE_ORDER_MODEL.getCentralizeOrders({ 
                            seen,
                            customerID,
                            page, 
                            limit, 
                        });
                        
						res.json({
                            ...listMergeCentralizeOrder,
                            STATUS_ORDER
                        })
					}]
                },
            },

            [CF_ROUTINGS_ORDER.API_UPDATE_SEEN_CENTRALIZE_ORDER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async (req, res) => {
                        // let { _id: customerID } = req.customer;
                        let { centralizeOrderID } = req.params;
                        let { seen } = req.body;
                       
						const listMergeCentralizeOrder = await CENTRALIZE_ORDER_MODEL.update({ 
                            centralizeOrderID,
                            seen, 
                        });
                        
						res.json({
                            ...listMergeCentralizeOrder,
                            STATUS_ORDER
                        })
					}]
                },
            },

            [CF_ROUTINGS_ORDER.INFO_MERGE_CENTRALIZE_ORDER]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'view',
                    view: 'index.ejs',
					title: 'List Order - NANDIO',
					code: CF_ROUTINGS_ORDER.INFO_MERGE_CENTRALIZE_ORDER,
					inc: path.resolve(__dirname, '../views/info_centralize_order.ejs')
                },
                methods: {
					get: [ async (req, res) => {
                        let { mergeCentralizeOrderID } = req.params;
                        
                        const infoMergeCentralizeOrder = await MERGE_CENTRALIZE_ORDER_MODEL.getInfo({ 
                            mergeCentralizeOrderID
                        });

						ChildRouter.renderToView(req, res, {
							infoMergeCentralizeOrder: infoMergeCentralizeOrder.data,
                            STATUS_ORDER,
                            TYPE_CENTRALIZE_ORDER,
						});
					}]
                },
            },

            [CF_ROUTINGS_ORDER.EXPORT_LIST_MERGE_CENTRALIZE_ORDER]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async (req, res) => {
                        
                        const listMergeCentralizeOrder = await MERGE_CENTRALIZE_ORDER_MODEL.export({ });

                        res.json(listMergeCentralizeOrder);
					}]
                },
            },

            /**
             * Function: Cập nhật trạng thái đơn hàng (API)
             */
             [CF_ROUTINGS_ORDER.WEBHOOK_GHN_UPDATE_CENTRALIZE_ORDER]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
					post: [ async (req, res) => {
						const { Description, OrderCode, Status, Time } = req.body;

						const infoAfterUpdateStatus = await MERGE_CENTRALIZE_ORDER_MODEL.updateStatusByWebHookGHN({
							Description, OrderCode, Status, Time
						});
						res.json(infoAfterUpdateStatus);
					}]
                },
            },

        }
    }
};
