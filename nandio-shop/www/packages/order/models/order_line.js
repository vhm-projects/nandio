"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      		= require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel 							= require('../../../models/intalize/base_model');
const { checkObjectIDs }					= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const ORDER_LINE_COLL  						= require('../databases/order_line-coll');
const PRODUCT_COLL  						= require('../../product/databases/product-coll');
const FLASHSALE_COLL  						= require('../../promotion/databases/flash_sale-coll');
const BIGSALE_COLL  						= require('../../promotion/databases/big_sale-coll');


class Model extends BaseModel {
    constructor() {
        super(ORDER_LINE_COLL);
        this.STATUS_NO_CREATE_ORDER = 0;
    }
    // Tạm đóng code cũ của Minh
	// insert({ productID, amount, quantities, note, typeDiscount = 0, discount, provisionalAmount }) {
    //     return new Promise(async resolve => {
    //         try {
	// 			if(!ObjectID.isValid(productID))
    //                 return resolve({ error: true, message: "param_invalid" }); 

	// 			let dataInsert = {
	// 				product: productID,
    //                 amount,
	// 				quantities,
	// 				typeDiscount,
	// 				note
	// 			};

	// 			// If there is a discount, then set discounted and provisional amount
	// 			if([1,2].includes(+typeDiscount)){
	// 				dataInsert.discount 		 = discount;
	// 				dataInsert.provisionalAmount = provisionalAmount;
	// 			}

    //             let infoAfterInsert = await this.insertData(dataInsert);

    //             if(!infoAfterInsert)
    //                 return resolve({ error: true, message: 'add_order_line_failed' });

    //             return resolve({ error: false, data: infoAfterInsert });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }

    /**
     * Thêm giỏ hàng order-line
     */
    insert({ productID, customerID, quantities, note }) {
        return new Promise(async resolve => {
            try {
				if(!ObjectID.isValid(productID) || !ObjectID.isValid(customerID) )
                    return resolve({ error: true, message: "param_invalid" }); 

                let checkProductInOrderline = await ORDER_LINE_COLL.findOne({ 
                    product: productID, 
                    customer: customerID,
                    status: this.STATUS_NO_CREATE_ORDER
                }).lean();

                if(checkProductInOrderline)
                    return resolve({ error: true, message: "product_exist_in_cart"});

                let infoProduct = await PRODUCT_COLL.findById(productID);
                if(!infoProduct)
                    return resolve({ error: true, message: "product_not_exist"})

                let { price } = infoProduct;
				let dataInsert = {
					product: productID,
                    amount: price,
                    price,
                    note,
                    quantities: !isNaN(quantities) ? quantities : 1,
                    customer: customerID
				};

                if(quantities){
                    dataInsert.amount = price * quantities;
                }

				// If there is a discount, then set discounted and provisional amount
				// if([1,2].includes(+typeDiscount)){
				// 	dataInsert.discount 		 = discount;
				// 	dataInsert.provisionalAmount = provisionalAmount;
				// }

                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_order_line_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ orderLineID, quantities }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(orderLineID))
                    return resolve({ error: true, message: "param_invalid" });

                let infoOrderLine = await ORDER_LINE_COLL.findById(orderLineID).populate("product");

                if(isNaN(quantities))
                    return resolve({ error: true, message: "quantities_isNaN" });

                let { price } = infoOrderLine.product;
                let dataUpdate = { amount: price * quantities };
                quantities  && (dataUpdate.quantities = quantities);

                let infoAfterUpdate = await ORDER_LINE_COLL.findByIdAndUpdate(orderLineID, dataUpdate, { new: true });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	delete({ orderLineID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(orderLineID))
                    return resolve({ error: true, message: "param_invalid" });

                let infoAfterDelete = await ORDER_LINE_COLL.findByIdAndDelete(orderLineID);
                if(!infoAfterDelete)
                    return resolve({ error: true, message: "order_line_is_not_exist" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    calculatePrice({ customerID, orderLinesID, promotion }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: "Params customerID invalid" });

                if(!checkObjectIDs(orderLinesID))
                    return resolve({ error: true, message: "Params orderLinesID invalid" });

                if(!promotion)
                    return resolve({ error: true, message: "Params promotion invalid" });

                let listOrderline = await ORDER_LINE_COLL
                    .find({ _id: { $in: orderLinesID }, status: 0 })
                    .select('_id product')
                    .lean();

                if(!listOrderline)
                    return resolve({ error: true, message: "Can't get list orderline" });

                let listOrderlineHaveSale = [];
                for (const orderLine of listOrderline) {
                    let infoBigSale;
                    let infoFlashSale;

                    if(promotion.kind === 'big_sale'){
                        infoBigSale = await BIGSALE_COLL.findOne({
                            _id: promotion._id,
                            products: { $in: [orderLine.product] },
                            status: 1
                        })
                    }

                    if(promotion.kind === 'flash_sale'){
                        infoFlashSale = await FLASHSALE_COLL.findOne({
                            _id: promotion._id,
                            products: { $in: [orderLine.product] },
                            status: 1,
                        })
                    }

                    if(!infoFlashSale){
                        infoFlashSale = await FLASHSALE_COLL.findOne({
                            products: { $in: [orderLine.product] },
                            status: 1,
                        }).lean();
                    }

                    if(infoBigSale || infoFlashSale){
                        listOrderlineHaveSale = [...listOrderlineHaveSale, {
                            orderLineID: orderLine._id,
                            infoBigSale,
                            infoFlashSale
                        }]
                    }
                }

                return resolve({ error: false, data: listOrderlineHaveSale });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ orderLineID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(orderLineID))
                    return resolve({ error: true, message: "param_invalid" });

                let infoOrderLine = await ORDER_LINE_COLL
					.findById(orderLineID)
					.populate('product promotion')
					.lean();

                if(!infoOrderLine)
                    return resolve({ error: true, message: "order_line_is_not_exist" });

                return resolve({ error: false, data: infoOrderLine });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListOrderLineOfCustomer({ customerID, page = 1, limit = 10 }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: "param_invalid" });

                if(isNaN(limit)){
                    limit = 10;
                } else{
                    limit = +limit;
                }

                if(isNaN(page)){
                    page = 1;
                } else{
                    page = +page;
                }

                let listOrderLine = await ORDER_LINE_COLL
					.find({ customer: customerID, status: this.STATUS_NO_CREATE_ORDER })
                    .select("product quantities")
                    .populate({
                        path: "product",
                        select: "name avatar price",
                        populate: {
                            path: "avatar",
                            select: "path"
                        }
                    })
                    .skip((page * limit) - limit)
                    .limit(limit)
                    .sort({ createAt: -1 })
					.lean();

                if(!listOrderLine)
                    return resolve({ error: true, message: "order_line_is_not_exist" });

                let totalItem = await ORDER_LINE_COLL.countDocuments({ 
                    customer: customerID, status: this.STATUS_NO_CREATE_ORDER 
                });
                let pages = Math.ceil(totalItem/limit);

                return resolve({ error: false, data: { listOrderLine, currentPage: page, totalPage: pages } });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getAllOrderLineOfCustomer({ customerID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: "param_invalid" });

                let listOrderLine = await ORDER_LINE_COLL
					.find({ customer: customerID, status: this.STATUS_NO_CREATE_ORDER })
                    .select("product quantities")
					.populate({
                        path: "product",
                        select: "name avatar price",
                        populate: {
                            path: "avatar",
                            select: "path"
                        }
                    })
                    .sort({ createAt: -1 })
					.lean();

                if(!listOrderLine)
                    return resolve({ error: true, message: "order_line_is_not_exist" });
                return resolve({ error: false, data: listOrderLine });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
