"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      		= require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGES
 */
const { checkObjectIDs }					= require('../../../utils/utils');
const { randomStringOnlyNumber } 			= require('../../../utils/string_utils');
const sendMail 								= require('../../../mailer/mailer');
const { PAYMENT_TYPE }					    = require('../../../config/cf_constants');

/**
 * BASE
 */
const BaseModel 							= require('../../../models/intalize/base_model');
const COMMON_MODEL                  		= require('../../common/models/common').MODEL;
const CENTRALIZE_ORDER_MODEL                = require('../models/centralize_order').MODEL;
const NOTIFICATION_MODEL                    = require('../../notification/models/notification').MODEL;

/**
 * COLLECTIONS
 */
const ORDER_COLL  							= require('../databases/order-coll');
const ORDER_LINE_COLL  						= require('../databases/order_line-coll');
const BIGSALE_COLL  						= require('../../promotion/databases/big_sale-coll');
const FLASHSALE_COLL  						= require('../../promotion/databases/flash_sale-coll');
const ADDRESS_COLL  					    = require('../../address/databases/address-coll');


class Model extends BaseModel {
    constructor() {
        super(ORDER_COLL);
    }

	insert({ 
		orderLines, deliveryPrice, promotion, payment, addressID, customerID
	}) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(orderLines, addressID))
                    return resolve({ error: true, message: 'param_invalid' });

				if(![1,2].includes(+payment))
					return resolve({ error: true, message: 'payment_method_not_exist' });

				let dataInsert = {
					orderID: randomStringOnlyNumber(10),
					payment,
					address: addressID,
					customer: customerID,
				}

				let listProduct = [];
                let point       = 0; // POINT TỪ PRODUCT

				if(orderLines && orderLines.length){
					let listOrderLine = await ORDER_LINE_COLL.find({ _id: { $in: orderLines }}).lean();

					let amount = 0;
					// listOrderLine.forEach(orderLine => {
					for (let orderLine of listOrderLine) {
						amount += orderLine.amount;
						let quantities = orderLine.quantities;

						// let infoProduct = await PRODUCT_COLL.findById(orderLine.product).select('idSQL');
						// if (infoProduct) {
						// 	let infoProductPoint = await PRODUCT_POINT_COLL.findOne({ product_id: infoProduct.idSQL }).select('point');
						// 	if (infoProductPoint) {
						// 		console.log({
						// 			infoProductPoint,
						// 			point: Number(infoProductPoint.point)
						// 		});
						// 		point += (infoProductPoint.point && !Number.isNaN(Number(infoProductPoint.point))) ? Number(infoProductPoint.point) : 0;
						// 	}
						// }

						listProduct = [
							...listProduct,
							{
								product: orderLine.product,
								onModel: 'product',
								quantities,
								totalPrice: orderLine.amount
							}
						];
					}
					// });
					// Tổng tiền đơn hàng
					dataInsert.amount = amount;

					// Tổng tiền tạm tính đơn hàng
					dataInsert.provisionalAmount = amount;

					// Tổng tiền giảm đơn hàng
					dataInsert.discountPrice = 0;

					dataInsert.orderLines = orderLines;

					// Trường hợp có khuyến mãi
					if(promotion){
						dataInsert.promotion = promotion._id;
						dataInsert.onModel = promotion.kind;

						let amount = 0;
						let provisionalAmount = 0;
						let discountPrice = 0;
						let infoSale = null;

						if(promotion.kind == "big_sale"){
							infoSale = await BIGSALE_COLL.findOne({
								_id: promotion._id,
								status: 1,
							}).lean();
						}
						if(promotion.kind == "flash_sale"){
							infoSale = await FLASHSALE_COLL.findOne({
								_id: promotion._id,
								status: 1,
							}).lean();
						}

						for (const orderLine of listOrderLine) {
							provisionalAmount += orderLine.amount;

							if(!infoSale){
								infoSale = await FLASHSALE_COLL.findOne({
									products: { $in: [orderLine.product] },
									status: 1,
								}).lean();
							}

							if(infoSale){
								let productsSale = infoSale.products.map(product => product.toString());

								if(productsSale.includes(orderLine.product.toString())){
									const { typeDiscount, value } = infoSale;

									// Giảm giá theo tiền
									if(typeDiscount == 1){
										const priceAfterDiscount = orderLine.amount - (orderLine.quantities * value);

										amount += priceAfterDiscount;
										discountPrice += (orderLine.quantities * value);
	
										// Cập nhật tiền sau khi giảm cho orderLine
										await ORDER_LINE_COLL.findByIdAndUpdate(orderLine._id, {
											amountAfterDiscount: priceAfterDiscount
										});
									}
	
									// Giảm giá theo %
									if(typeDiscount == 2){
										const percentAfterDiscount = (100 - value) / 100;
										const priceAfterDiscount = orderLine.amount * percentAfterDiscount;

										amount += priceAfterDiscount;
										discountPrice += orderLine.amount - priceAfterDiscount;

										// Cập nhật tiền sau khi giảm cho orderLine
										await ORDER_LINE_COLL.findByIdAndUpdate(orderLine._id, { 
											amountAfterDiscount: priceAfterDiscount
										});
									}

								} else{
									amount += orderLine.amount;
								}

							} else{
								amount += orderLine.amount;
							}
						}

						// Tổng tiền đơn hàng
						dataInsert.amount = amount;
						dataInsert.provisionalAmount = provisionalAmount;
						dataInsert.discountPrice = discountPrice;
					}
				}

                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_order_failed' });

				// if(orderLines && orderLines.length){
				// 	await ORDER_LINE_COLL.updateMany({ _id: { $in: orderLines } }, {
				// 		$set: { status: 1 }
				// 	});
				// }

				let namePayment = '';
				PAYMENT_TYPE.map(type => {
					if (type.value == Number(payment)) {
						namePayment = type.text;
					}
				});

				console.log({
					namePayment
				});

				infoAfterInsert = {
					...infoAfterInsert._doc,
					namePayment
				}
				console.log({
					infoAfterInsert
				});
				// LƯU VÀO QUẢN LÝ TẬP TRUNG
				let infoAddress = await ADDRESS_COLL.findById(addressID);
				let { address, ward, district, city } = infoAddress;

				const cityInfo = COMMON_MODEL.getInfoProvince({ provinceCode: city });
				const districtInfo = COMMON_MODEL.getInfoDistrict({ districtCode: district });
				const wardInfo = await COMMON_MODEL.getInfoWard({ district: district, wardCode: ward });

				let cityText     = cityInfo.data && cityInfo.data[1]?.name_with_type;
				let districtText = districtInfo.data && districtInfo.data[1]?.name_with_type;
				let wardText     = wardInfo.data && wardInfo.data[1]?.name_with_type;

				const TYPE_ORDER = 1;
				let conditionObj__CentralizeOrder = {
					type: infoAfterInsert._id, 
					onModel: 'order',
					customer: customerID,
					onModelCustomer: 'customer',
					address, ward, district, city, cityText, wardText, districtText,
					products: listProduct,
					kind: TYPE_ORDER,
					addressID,
					totalPrice: dataInsert.amount, 
					provisionalAmount: dataInsert.provisionalAmount, 
					discountPrice: dataInsert.discountPrice
				}
				
				let infoAfterInsertCentralizeOrder = await CENTRALIZE_ORDER_MODEL.insert(conditionObj__CentralizeOrder);
				if (infoAfterInsertCentralizeOrder.error) {
					return resolve(infoAfterInsertCentralizeOrder);
				}

				// let title = 'Bạn đã tạo đơn hàng thành công';
				// const TYPE_ORDER_NOTIFICATION = 1;
				// let infoNotificationAfterInsert = await NOTIFICATION_MODEL.insertV2({ 
				// 	listNotification: [{
				// 		title:       title, 
				// 		description: `Đơn hàng đã được tạo, Vui lòng chờ xác nhận`, 
				// 		type:        TYPE_ORDER_NOTIFICATION, 
				// 		receive:     customerID,
				// 		centralize_order: infoAfterInsertCentralizeOrder.data._id,
				// 		createAt:    new Date(),
				// 		modifyAt:    new Date(),
				// 	}]
				// });
				// sendMail(email, 'Subject', 'This is content', ({ error, message }) => {
				// 	console.log({ error, message });
				// });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	updateStatus({ orderID, status }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(orderID) || ![0,1,2,3].includes(+status))
                    return resolve({ error: true, message: "param_invalid" });

				let infoAfterUpdate = await ORDER_COLL.findByIdAndUpdate(orderID, {
					$set: { status }
				});
				if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'update_status_order_failed' });

				return resolve({ error: false, data: infoAfterUpdate });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

    update({ 
		orderID, orderLine, customerID, note, amount, provisionalAmount,
		deliveryPrice, deliveryDate, voucher, payment, addressID, status
	}) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(orderID))
                    return resolve({ error: true, message: "param_invalid" });

                if((customerID && !ObjectID.isValid(customerID)) || 
				   (addressID  && !ObjectID.isValid(addressID)))
                    return resolve({ error: true, message: "param_invalid" });

				if((orderLine && orderLine.length) && !checkObjectIDs(orderLine))
                    return resolve({ error: true, message: 'param_invalid' });

                let checkExists = await ORDER_COLL.findById(orderID);
                if(!checkExists)
                    return resolve({ error: true, message: "order_is_not_exists" });

                let dataUpdate = {};
                (orderLine && orderLine.length) && (dataUpdate.orderLine = orderLine);

                customerID  		&& (dataUpdate.customerID      		= customerID);
                note        		&& (dataUpdate.note 				= note);
                voucher  			&& (dataUpdate.voucher   			= voucher);
                deliveryDate  		&& (dataUpdate.deliveryDate   		= deliveryDate);
                provisionalAmount  	&& (dataUpdate.provisionalAmount 	= provisionalAmount);
                addressID  			&& (dataUpdate.addressID   			= addressID);

				if(!isNaN(status) && [0,1,2,3].includes(+status)){
					dataUpdate.status = status;
				}

				if(!isNaN(payment) && [0,1,2].includes(+payment)){
					dataUpdate.payment = payment;
				}

				if(!isNaN(deliveryPrice) && +deliveryPrice >= 0){
					dataUpdate.deliveryPrice = deliveryPrice;
				}

				if(!isNaN(amount) && +amount >= 0){
					dataUpdate.amount = amount;
				}

                await this.updateWhereClause({ _id: orderID }, dataUpdate);

                return resolve({ error: false, data: dataUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	delete({ orderID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(orderID))
                    return resolve({ error: true, message: "param_invalid" });

                let infoAfterDelete = await ORDER_COLL.findByIdAndDelete(orderID);
                if(!infoAfterDelete)
                    return resolve({ error: true, message: "order_is_not_exist" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ orderID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(orderID))
                    return resolve({ error: true, message: "param_invalid" });

                let infoOrder = await ORDER_COLL
					.findById(orderID)
					// .select("orderID createAt address orderLines provisionalAmount discountPrice deliveryPrice amount payment status")
					.populate({
						path: 'address',
						select: "fullname phone address city district ward"
					})
					.populate({
						path: 'customer',
						select: "fullname phone email"
					})
					.populate({
						path: 'orderLines',
						select: "product amount amountAfterDiscount quantities",
						populate: {
							path: "product",
							select: "name avatar",
							populate: {
								path: "avatar",
								select: "path",
							}
						}
					})
					.lean();

                if(!infoOrder)
                    return resolve({ error: true, message: "order_is_not_exist" });

				if(infoOrder.address){
					const { city, district, ward } = infoOrder.address;
					const cityInfo = COMMON_MODEL.getInfoProvince({ provinceCode: city });
					const districtInfo = COMMON_MODEL.getInfoDistrict({ districtCode: district });
					const wardInfo = await COMMON_MODEL.getInfoWard({ district: district, wardCode: ward });
	
					infoOrder.address.cityText = cityInfo.data && cityInfo.data[1]?.name_with_type;
					infoOrder.address.districtText = districtInfo.data && districtInfo.data[1]?.name_with_type;
					infoOrder.address.wardText = wardInfo.data && wardInfo.data[1]?.name_with_type;
				}

                return resolve({ error: false, data: infoOrder });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListOrderOfCustomer({ customerID, page = 1, limit = 30 }){
        return new Promise(async resolve => {
            try {
                const listOrder = await ORDER_COLL
					.find({ customer: customerID})
					.select("orderLines createAt orderID amount")
					// .populate("promotion")
					.populate({
						path: 'orderLines',
						select: 'product amount amountAfterDiscount quantities',
						populate: {
							path: "product", 
							select: 'name avatar',
							populate: {
								path: "avatar", 
								select: 'path',
							},

						},
					})
					.sort({ modifyAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
					.lean();

                if(!listOrder)
                    return resolve({ error: true, message: "cannot_get_list_order" });

				let totalItem = await ORDER_COLL.count({ customer: customerID});
				let pages = Math.ceil(totalItem/limit);

				return resolve({ 
					error: false, 
					data: {
						listOrder,
						currentPage: +page,
						totalPgae: +pages
					}
				});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListWithPaging({ page = 1, limit = 30 }){
        return new Promise(async resolve => {
            try {
				limit = +limit;
				page  = +page;

                const listOrder = await ORDER_COLL
					.find({})
					.populate({
						path: 'orderLine customer address',
						populate: 'product'
					})
					.sort({ modifyAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
					.lean();

                if(!listOrder)
                    return resolve({ error: true, message: "cannot_get_list_order" });

				let totalOrder = await ORDER_COLL.countDocuments({});

				return resolve({ 
					error: false, 
					data: {
						listOrder,
						currentPage: page,
						perPage: limit,
						total: totalOrder
					}
				});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListByFilterWithPaging({ 
		keyword, status, productID, customerID, payment, fromDate, toDate, page = 1, limit = 30 
	}){
		return new Promise(async resolve => {
			try {
				let conditionObj = {};
				limit 	= +limit;
				page  	= +page;
				payment = +payment;
				status 	= +status;

				if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [
						{ orderID: new RegExp(key, 'i') },
						{ 'customer.customerID': new RegExp(key, 'i') },
						{ 'customer.fullname': new RegExp(key, 'i') },
					]
                }

				if(fromDate && toDate){
					conditionObj.createAt = {
                        $gte: new Date(fromDate),
                        $lt: new Date(toDate)
                    }
				}

				if([0,1,2].includes(payment)){
					conditionObj.payment = payment;
				}

				if([0,1,2,3].includes(status)){
					conditionObj.status	= status;
				}

				if(productID){
					conditionObj['orderLine.product'] = ObjectID(productID);
					// conditionObj.orderLine = {
					// 	$elemMatch: {
					// 		product: ObjectID(productID)
					// 	}
					// }
				}

				if(customerID){
					conditionObj['customer._id'] = ObjectID(customerID);
				}

				let pipeline = [
					{
						$lookup: {
						   from: 'customers',
						   localField: 'customer',
						   foreignField: '_id',
						   as: 'customer'
						 }
					},
					{ 
						$unwind: {
							path: '$customer',
							preserveNullAndEmptyArrays: true
						}
					},
					{
						$lookup: {
							from: 'order_lines',
							localField: 'orderLine',
							foreignField: '_id',
							as: 'orderLine'
						}
					},
					{
						$match: conditionObj
					},
					{
						$lookup: {
						   from: 'addresses',
						   localField: 'address',
						   foreignField: '_id',
						   as: 'address'
						 }
					},
					{
						$unwind: {
							path: '$address',
							preserveNullAndEmptyArrays: true
						}
					}
				];

				let listOrder = await ORDER_COLL.aggregate([
					...pipeline,
					{ $limit: limit },
					{ $skip: (page - 1) * limit },
					{ $sort: { modifyAt: -1 } }
				])

				if(!listOrder)
					return resolve({ error: true, message: "cannot_get_list_order" });

				let totalOrder = await ORDER_COLL.aggregate([
					...pipeline,
					{ $count: 'total' },
				]);

				return resolve({ 
					error: false, 
					data: {
						listOrder,
						currentPage: page,
						perPage: limit,
						total: totalOrder.length && totalOrder[0].total
					}
				});
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

	getListByFilter({ keyword, status, productID, customerID, payment, fromDate, toDate }){
		return new Promise(async resolve => {
			try {
				let conditionObj = {};
				payment = +payment;
				status 	= +status;

				if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [
						{ orderID: new RegExp(key, 'i') },
						{ 'customer.customerID': new RegExp(key, 'i') },
						{ 'customer.fullname': new RegExp(key, 'i') },
					]
                }

				if(fromDate && toDate){
					conditionObj.createAt = {
                        $gte: new Date(fromDate),
                        $lt: new Date(toDate)
                    }
				}

				if([0,1,2].includes(payment)){
					conditionObj.payment = payment;
				}

				if([0,1,2,3].includes(status)){
					conditionObj.status	= status;
				}

				if(productID){
					conditionObj['orderLine.product'] = ObjectID(productID);
					// conditionObj.orderLine = {
					// 	$elemMatch: {
					// 		product: ObjectID(productID)
					// 	}
					// }
				}

				if(customerID){
					conditionObj['customer._id'] = ObjectID(customerID);
				}

				let listOrder = await ORDER_COLL.aggregate([
					{
						$lookup: {
						   from: 'customers',
						   localField: 'customer',
						   foreignField: '_id',
						   as: 'customer'
						 }
					},
					{ 
						$unwind: {
							path: '$customer',
							preserveNullAndEmptyArrays: true
						}
					},
					{
						$lookup: {
							from: 'order_lines',
							localField: 'orderLine',
							foreignField: '_id',
							as: 'orderLine'
						}
					},
					{
						$match: conditionObj
					},
					{
						$lookup: {
						   from: 'addresses',
						   localField: 'address',
						   foreignField: '_id',
						   as: 'address'
						 }
					},
					{
						$unwind: {
							path: '$address',
							preserveNullAndEmptyArrays: true
						}
					},
					{ $sort: { modifyAt: -1 } }
				])

				if(!listOrder)
					return resolve({ error: true, message: "cannot_get_list_order" });

				return resolve({ error: false, data: listOrder });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

}

exports.MODEL = new Model;
