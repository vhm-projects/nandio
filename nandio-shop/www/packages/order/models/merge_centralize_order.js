"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      		= require('mongoose').Types.ObjectId;
const formatCurrency 						= require('number-format.js');
const XlsxPopulate                          = require('xlsx-populate');
const moment                                = require('moment');
const path                                  = require('path');
// const NANDIO_STAGING                        = process.env.NANDIO_STAGING || 'http://eebc-2001-ee0-5007-e0a0-e5c9-f15d-6baf-4ad0.ngrok.io';
const NANDIO_STAGING                        = process.env.NANDIO_STAGING || 'http://localhost:5002';
/**
 * INTERNAL PACKAGES
 */
const { checkObjectIDs, isEmptyObject }			  = require('../../../utils/utils');
const { randomStringOnlyNumber } 			      = require('../../../utils/string_utils');
const { sendMailCentralizeOrder }                 = require('../../../mailer/module/mail_user');
const { TYPE_CENTRALIZE_ORDER, STATUS_ORDER, STATUS_SHIPPING }     = require('../../../config/cf_constants')

/**
 * BASE
 */
const BaseModel 							= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const MERGE_CENTRALIZE_ORDER_COLL  							= require('../databases/merge_centralize_order-coll');
const CENTRALIZE_ORDER_COLL  							    = require('../databases/centralize_order-coll');
const AGENCY_MODEL  							            = require('../../store/models/agency').MODEL;
const GIAO_HANG_NHANH_MODEL                                 = require('../../giao_hang_nhanh/models/giao_hang_nhanh').MODEL;
const NOTIFICATION_MODEL                                    = require('../../notification/models/notification').MODEL;

class Model extends BaseModel {
    constructor() {
        super(MERGE_CENTRALIZE_ORDER_COLL);
    }

	insert({ 
		centralizeOrderID,
        weight,
        length,
        width,
        height,
        agency,
        userCreate
	}) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(centralizeOrderID))
                    return resolve({ error: true, message: 'param_invalid' });

                if(!checkObjectIDs(userCreate))
                    return resolve({ error: true, message: 'param_invalid' });
               
                // // LẤY DANH SÁCH SẢN PHẨM CỦA ORDER
                console.log("=======================MERGE_CENTRALIZE_ORDER======================1");
                console.log({
                    centralizeOrderID
                });
                let infoCentralizeOrder = await CENTRALIZE_ORDER_COLL.findById(centralizeOrderID)  
                    .populate('type')
                    .populate({
                        path: 'customer',
                        select: 'phone fullname email',
                    })
                    .populate({
                        path: 'products.product',
                    });   
                console.log({
                    infoCentralizeOrder
                });

                if (!infoCentralizeOrder) { 
                    return resolve({ error: true, message: 'Đơn không tồn tại' });
                }

                let { customer: { phone, fullname }, address, ward, district, wardText, districtText, cityText } = infoCentralizeOrder;
                // let infoAgency = await AGENCY_COLL.findById(agency);
                let infoAgencyData = await AGENCY_MODEL.getInfo({ 
                    wardName: wardText, districtName: districtText, cityName: cityText 
                });
                console.log({
                    infoAgencyData
                });
                let { data: { infoAgency, infoAddressGHN }} = infoAgencyData;
                if (!infoAgency) {
                    return resolve({ error: true, message: 'Chi nhánh không hợp lệ' });
                }

                let codeRandom = randomStringOnlyNumber(10);
                const USER_CONFIRM     = 2;
                const CREATE_ORDER_GHN = 4;
				let dataInsert = {
					mergeCentralizeOrderID: codeRandom,
					centralizeOrder: centralizeOrderID,
                    customer: infoCentralizeOrder.customer._id,
                    onModel:  infoCentralizeOrder.onModelCustomer,
                    status: CREATE_ORDER_GHN,
                    userCreate
				}
                console.log({
                    dataInsert
                });
                if (weight) {
                    if (weight < 0) {
                        return resolve({ error: true, message: 'Trọng lượng không được phép nhỏ hơn 0' });
                    }
                    dataInsert = {
                        ...dataInsert,
                        weight
                    }
                }

                if (length) {
                    if (length < 0) {
                        return resolve({ error: true, message: 'Độ dài không được phép nhỏ hơn 0' });
                    }
                    dataInsert = {
                        ...dataInsert,
                        length
                    }
                }

                if (width) {
                    if (width < 0) {
                        return resolve({ error: true, message: 'Độ rộng không được phép nhỏ hơn 0' });
                    }
                    dataInsert = {
                        ...dataInsert,
                        width
                    }
                }

                if (height) {
                    if (height < 0) {
                        return resolve({ error: true, message: 'Chiều cao không được phép nhỏ hơn 0' });
                    }
                    dataInsert = {
                        ...dataInsert,
                        height
                    }
                }

                let totalPrice    = 0;
                let discountPrice = 0;
                let listProduct = [];
                let amount                         = (infoCentralizeOrder.type && infoCentralizeOrder.type.amount)        ? infoCentralizeOrder.type.amount        : 0;
                let discountPriceOfCentralizeOrder = (infoCentralizeOrder.type && infoCentralizeOrder.type.discountPrice) ? infoCentralizeOrder.type.discountPrice : 0;
                console.log({
                    amount,
                    discountPriceOfCentralizeOrder
                });
                const TYPE_ORDER = 1;
                if (infoCentralizeOrder.kind == TYPE_ORDER) {
                    totalPrice += amount;
                    discountPrice += discountPriceOfCentralizeOrder;
                } else {
                    totalPrice += 0;
                    discountPrice += 0;
                }

                const SHOP_SELLER     = 1;
                const BUYER_CONSIGNEE = 2;
                let payment_type_id = BUYER_CONSIGNEE;
                if (infoCentralizeOrder.pay_shipping_fee == SHOP_SELLER) {
                    payment_type_id = SHOP_SELLER;
                }

                let listProductCentralize = [];
                infoCentralizeOrder.products.map(product => {
                    console.log({
                        product
                    });
                    listProduct = [
                        ...listProduct,
                        {
                            name: (product.product && product.product.name) ? product.product.name: '',
                            code: (product.product &&product.product.SKU)   ? product.product.SKU : '1',
                            quantity: product.quantities
                        }
                    ];
                    listProductCentralize = [
                        ...listProductCentralize,
                        {
                            product:    product.product._id,
                            onModel:    product.onModel,
                            quantities: product.quantities
                        }
                    ]
                });

                dataInsert = {
                    ...dataInsert,
                    products: listProductCentralize
                }
                
                console.log({
                    clientCode: codeRandom,
                    districtStore: infoAgency.district,
                    storeID:       infoAgency.storeCodeGHN,
                    wardStore:     infoAgency.ward,
                    wardCustomer:  infoAddressGHN.data.infoWard.WardCode,
                    districtCustomer: infoAddressGHN.data.infoDistrict.DistrictID,
                    weight,
                    length,
                    width,
                    height,
                    listProduct,
                    totalPrice,
                    fullname,
                    phone,
                    address,
                    payment_type_id,
                });
                let infoAfterInsertOrderGHN = await GIAO_HANG_NHANH_MODEL.createOrderGHN({ 
                    payment_type_id,
                    clientCode: codeRandom,
                    districtStore: infoAgency.district,
                    storeID:       infoAgency.storeCodeGHN,
                    wardStore:     infoAgency.ward,
                    wardCustomer:  infoAddressGHN.data.infoWard.WardCode,
                    districtCustomer: infoAddressGHN.data.infoDistrict.DistrictID,
                    weight,
                    length,
                    width,
                    height,
                    listProduct,
                    totalPrice,
                    fullname,
                    phone,
                    address
                });
                console.log({
                    infoAfterInsertOrderGHN
                });
                if (infoAfterInsertOrderGHN.code != 200) {
                    return resolve({
                        error: true, 
                        ...infoAfterInsertOrderGHN,
                        message: infoAfterInsertOrderGHN.code_message_value
                    });
                }
                // console.log("=============================================4");

                dataInsert = {
                    ...dataInsert,
                    provisionalAmount: totalPrice,
                    discountPrice,
                    // totalPrice:        totalPrice + Number(infoAfterInsertOrderGHN.data.total_fee),
                    // centralizeOrderGetAddress,
                    kind: infoCentralizeOrder.kind,
                    storeID: infoAgency.storeCodeGHN,
                    mergeCentralizeOrderIDGHN: infoAfterInsertOrderGHN.data.order_code,
                    deliveryPrice:             infoAfterInsertOrderGHN.data.total_fee,
                    expected_delivery_time:    new Date(infoAfterInsertOrderGHN.data.expected_delivery_time),
                }
                console.log({
                    pay_shipping_fee: infoCentralizeOrder.pay_shipping_fee
                });
                let totalPriceAfterChange = totalPrice;
                if (infoCentralizeOrder.pay_shipping_fee == SHOP_SELLER) {
                    dataInsert = {
                        ...dataInsert,
                        totalPrice: totalPriceAfterChange,
                    }
                } else {
                    totalPriceAfterChange += Number(infoAfterInsertOrderGHN.data.total_fee);
                    dataInsert = {
                        ...dataInsert,
                        totalPrice: totalPrice  + Number(infoAfterInsertOrderGHN.data.total_fee),
                    }
                }
                console.log({
                    dataInsert
                });
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_centralize_order_failed' });

                const MERGE_CENTRALIZE_ORDER = 2;
                await CENTRALIZE_ORDER_COLL.updateMany({
                    _id: {
                        $in: [centralizeOrderID]
                    }
                }, {
                    $set: {
                        status:            MERGE_CENTRALIZE_ORDER,
                        provisionalAmount: totalPrice,
                        discountPrice,
                        totalPrice:        totalPriceAfterChange,
                        storeID:           infoAgency.storeCodeGHN,
                        mergeCentralizeOrder:      infoAfterInsert._id,
                        mergeCentralizeOrderIDGHN: infoAfterInsertOrderGHN.data.order_code,
                        deliveryPrice:             infoAfterInsertOrderGHN.data.total_fee,
                        expected_delivery_time:    new Date(infoAfterInsertOrderGHN.data.expected_delivery_time),
                        modifyAt : new Date()
                    }
                });
                
                let listMailCC = infoAgency.cc;
                // if (cc) {
                //     listMailCC = infoAgency.cc;
                //     //  cc.split(',');
                // }
               
                // let listMailBCC = [];
                // if (bcc) {
                //     listMailBCC = bcc.split(',');
                // }

                let listEmailSendReceive = [infoAgency.email];
                // if (to) {
                //     listEmailSendReceive = to.split(',');
                // }
                // IN ĐƠN VẬN
                let infoPrintfOrderGHN = await GIAO_HANG_NHANH_MODEL.printfOrderGHN({ 
                    code: infoAfterInsertOrderGHN.data.order_code
                });
                // console.log("=============================================5");

                console.log({
                    // listMailBCC,
                    listMailCC,
                    listEmailSendReceive,
                    infoPrintfOrderGHN
                });
                if (infoPrintfOrderGHN.code != 200) {
                    return resolve({
                        error: true, 
                        ...infoPrintfOrderGHN,
                        message: infoPrintfOrderGHN.code_message_value
                    });
                }

                if (listEmailSendReceive.length) {
                    for (let mail of listEmailSendReceive) {

                        let dateNow = moment(new Date()).format('HH:mm DD/MM/YYYY');
                        console.log({
                            dateNow, mail
                        });
                        // let attachmentFileExcel = await this.export({ 
                        //     mergeCentralizeOrderID: infoAfterInsert._id
                        // });
                        // let { data: urlFileExcel, path: nameFileExcel } = attachmentFileExcel;
                       
                        // let urlDownloadExcel = NANDIO_STAGING + urlFileExcel;
                        let urlDownloadExcel = NANDIO_STAGING;
                        let attachment = [
                            // {
                            //     // file on disk as an attachment
                            //     filename: nameFileExcel,
                            //     path: 'https://880a-123-21-252-176.ngrok.io' + urlFileExcel // stream this file
                            // },
                            // {   // use URL as an attachment
                            //     filename: 'license.txt',
                            //     path: 'https://raw.github.com/nodemailer/nodemailer/master/LICENSE'
                            // },
                        ];
                        console.log({
                            attachment
                        });
                        sendMailCentralizeOrder({
                            email: mail, 
                            code: infoAfterInsertOrderGHN.data.order_code, 
                            date: dateNow, 
                            cc: listMailCC, 
                            // bcc, 
                            url: infoPrintfOrderGHN.url, 
                            // attachment, 
                            // urlDownloadExcel
                        })
                    }
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getList({ 
        kind,
        keyword,
        limit = 20, page = 1,
    }) {
        return new Promise(async resolve => {
            try {
                let dataFind = {};

                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    dataFind.$or = [
                        { mergeCentralizeOrderID: new RegExp(key, 'i') },
                        { mergeCentralizeOrderIDGHN: new RegExp(key, 'i') },
                        // { storeID: new RegExp(key, 'i') },
                    ]
                }

                if (kind) {
                    dataFind = {
                        ...dataFind,
                        kind
                    }
                }
                console.log({
                    dataFind
                });
                let skip = (page - 1) * limit;
                let count = await MERGE_CENTRALIZE_ORDER_COLL.count(dataFind);
                let listMergeCentralizeOrder = await MERGE_CENTRALIZE_ORDER_COLL
                    .find(dataFind) 
                    .limit(limit * 1)
                    .skip(skip)
                    .sort({ createAt: -1 })
					.populate({
						path: 'centralizeOrder',
					})
                    
                if(!listMergeCentralizeOrder)
                    return resolve({ error: true, message: 'add_centralize_order_failed' });

                let listMergeCentralizeOrderServerSide = listMergeCentralizeOrder.map((item, index) => {
                    let value = `
                        <span class="badge badge-pill ${STATUS_ORDER[item.status].badge}" style="
                            font-size: 10px;
                            padding: 8px;
                        ">
                            ${STATUS_ORDER[item.status].text}
                        </span> 
                    `;
                    
                    return {
                        index: skip + index + 1,
                        mergeCentralizeOrderID: `<a href="/merge-centralize-order/info-merge-centralize-order/${item._id}">${item.mergeCentralizeOrderID}</a>`,
                        mergeCentralizeOrderIDGHN: item.mergeCentralizeOrderIDGHN,
                        deliveryPrice: formatCurrency('###,###.', item.deliveryPrice, '' ),
                        address:  (item.centralizeOrder && item.centralizeOrder.address)  ? item.centralizeOrder.address : '',
                        ward:     (item.centralizeOrder && item.centralizeOrder.wardText) ? item.centralizeOrder.wardText : '',
                        district: (item.centralizeOrder && item.centralizeOrder.districtText)  ? item.centralizeOrder.districtText : '',
                        city:     (item.centralizeOrder && item.centralizeOrder.cityText)  ? item.centralizeOrder.cityText : '',
                        status:   value,
                        createAt:   moment(item.createAt).format('HH:mm DD/MM/YYYY'),
                    }
                })

                return resolve({ error: false, data: listMergeCentralizeOrderServerSide, recordsTotal: count, recordsFiltered: count });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getMergeCentralizeOrders({ 
        customerID, limit = 20, page = 1,
    }) {
        return new Promise(async resolve => {
            try {

                if (!checkObjectIDs(customerID)) {
                    return resolve({ error: true, message: 'ID Khách hàng không hợp lệ' });
                }
                let dataFind = {
                    customer: customerID
                };

                // if(keyword){
                //     let key = keyword.split(" ");
                //     key = '.*' + key.join(".*") + '.*';
                //     dataFind.$or = [
                //         { mergeCentralizeOrderID: new RegExp(key, 'i') },
                //         { mergeCentralizeOrderIDGHN: new RegExp(key, 'i') },
                //         // { storeID: new RegExp(key, 'i') },
                //     ]
                // }

                // if (kind) {
                //     dataFind = {
                //         ...dataFind,
                //         kind
                //     }
                // }
                console.log({
                    dataFind
                });
                let skip = (page - 1) * limit;
                let count = await MERGE_CENTRALIZE_ORDER_COLL.count(dataFind);
                let listMergeCentralizeOrder = await MERGE_CENTRALIZE_ORDER_COLL.find(dataFind) 
                    .select('mergeCentralizeOrderID mergeCentralizeOrderIDGHN products kind deliveryPrice totalPrice')
                    .limit(limit * 1)
                    .skip(skip)
                    .sort({ modifyAt: -1 })
					.populate({
                        path: 'products.product',
                        select: 'name'
                    });  
                    
                if(!listMergeCentralizeOrder)
                    return resolve({ error: true, message: 'cannot_get_list_centralize_order_failed' });
                
                let pages = Math.ceil(count/limit);

                return resolve({ 
                    error: false, 
                    data: listMergeCentralizeOrder, 
                    page: page, 
                    pages
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ 
        mergeCentralizeOrderID
    }) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(mergeCentralizeOrderID))
                    return resolve({ error: true, message: 'param_invalid' });

                let infoMergeCentralizeOrder = await MERGE_CENTRALIZE_ORDER_COLL.findById(mergeCentralizeOrderID)
                    .populate({
                        path: 'centralizeOrder',
                        // select: 'customer address wardText districtText cityText',

                        populate: {
                            path: 'customer type products.product',
                            select: 'fullname email phone name provisionalAmount discountPrice amount'
                        },
                    })
                    // .populate({
                    //     path: 'centralizeOrders',
                    //     // select: 'centralizeOrderID products customer address wardText districtText cityText',
                    //     populate: {
                    //         path: 'customer type products.product',
                    //         select: 'fullname email phone name provisionalAmount discountPrice amount'
                    //     }
                    // })
                if(!infoMergeCentralizeOrder)
                    return resolve({ error: true, message: 'cannot_get_info' });

                return resolve({ error: false, data: infoMergeCentralizeOrder });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateStatus({  orderID, status }){
		return new Promise(async resolve => {
			try {
				if(!checkObjectIDs(orderID))
                    return resolve({ error: true, message: "param_invalid" });

				let infoAfterUpdate = await MERGE_CENTRALIZE_ORDER_COLL.updateOne({ _id: orderID }, {
					$set: { status, modifyAt: new Date() }
				});
				if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'update_status_order_failed' });

				// // INSERT LOG ORDER
				// let infoAfterInsertLog = await LOG_ORDER_MODEL.insert({ status: status, order: orderID, userCreate: userID })
				// console.log({
				// 	infoAfterInsertLog: infoAfterInsertLog
				// });

				return resolve({ error: false, data: infoAfterUpdate });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

    updateStatusByWebHookGHN({ Description, OrderCode, Status, Time }){
		return new Promise(async resolve => {
			try {
				if(!OrderCode)
                    return resolve({ error: true, message: "OrderCode không hợp lệ" });
				if(!Status)
                    return resolve({ error: true, message: "Status không hợp lệ" });
				if(!Time)
                    return resolve({ error: true, message: "Time không hợp lệ" });

				let infoCentralizeOrder = await MERGE_CENTRALIZE_ORDER_COLL.findOne({ mergeCentralizeOrderIDGHN: OrderCode });
				if (!infoCentralizeOrder) {
                    return resolve({ error: true, message: "OrderCode không tồn tại" });
				}

                let error = true;
                STATUS_SHIPPING.map(status => {
                    if (status.value == Status) {
                        error = false;
                    }
                });
                if(error)
                    return resolve({ error: true, message: "Status không hợp lệ" });
				// let infoAfterInsert = await LOG_SHIPPING_MODEL.insert({
				// 	Description, OrderCode, Status, Time, order: infoCentralizeOrder._id
				// }) 
				
				// if (infoAfterInsert.error) {
				// 	return resolve(infoAfterInsert);
				// }
				
				if (["ready_to_pick", "storing", "delivering", "delivered", "waiting_to_return", "cancel"].includes(Status)) {
					let status = 0;
					const CREATE_SHIPPING          = 3;
					const STORING_SHIPPING         = 5;
					const DELIVERING_SHIPPING      = 6;
					const DELIVERING_SHIPPING_SUCCESS      = 7;
					const CANCEL                   = 8;
					const RETURN_GOODS             = 9;
					switch (Status) {
						// case "ready_to_pick":
						// 	status = CREATE_SHIPPING;
						// 	break;
						case "storing":
							status = STORING_SHIPPING;
							break;
						case "delivering":
							status = DELIVERING_SHIPPING;
							break;
						case "delivered":
							status = DELIVERING_SHIPPING_SUCCESS;
							break;
						case "cancel":
							status = CANCEL;
							break;
                        case "waiting_to_return":
							status = RETURN_GOODS;
							break;
						default:
							break;
					}
                    if (status) {
                        let infoAfterUpdate = await this.updateStatus({ orderID: infoCentralizeOrder._id, status });
                        console.log({
                            infoAfterUpdate
                        });
                        if (infoAfterUpdate.error) {
                            return resolve(infoAfterUpdate);
                        }

                        let infoCentralizeOrderAfterUpdate = await CENTRALIZE_ORDER_COLL.findByIdAndUpdate(infoCentralizeOrder.centralizeOrder, {
                            status, modifyAt: new Date(), seen: 'NOT_SEEN'
                        }, { new: true });
                        console.log({
                            infoCentralizeOrderAfterUpdate
                        });                      
                        
                        let title = '';
                        STATUS_ORDER.forEach(elem => {
                            if (elem.value == status) {
                                title = elem.text;
                            }
                        });
                        
                        const TYPE_ORDER = 1;
                        let infoNotificationAfterInsert = await NOTIFICATION_MODEL.insertV2({ 
                            listNotification: [{
                                title:       title, 
                                description: `Đơn hàng đã được cập nhật trạng thái thành ${title}`, 
                                type:        TYPE_ORDER, 
                                receive:     infoCentralizeOrderAfterUpdate.customer,
                                centralize_order: infoCentralizeOrderAfterUpdate._id,
                                createAt:    new Date(),
                                modifyAt:    new Date(),
                            }]
                        });
                        // return resolve({ error: false, data: infoAfterUpdate });
                    } 
                    // else {
                    //     return resolve({ error: false, data: infoCentralizeOrder });
                    // }
				} 

                let conditionObj__CentralizeOrder = {};
                if (Description) {
                    conditionObj__CentralizeOrder = {
                        $addToSet: {
                            status_ghn: {
                                name:     Status,
                                description: Description,
                                timeGhn:  new Date(Time),
                                createAt: new Date()
                            }
                        }
                    }
                } else {
                    conditionObj__CentralizeOrder = {
                        $addToSet: {
                            status_ghn: {
                                name:     Status,
                                timeGhn:  new Date(Time),
                                createAt: new Date()
                            }
                        }
                    }
                }
                console.log({
                    conditionObj__CentralizeOrder
                });
                let infoCentralizeOrderAfterUpdateStatus = await CENTRALIZE_ORDER_COLL.findByIdAndUpdate(infoCentralizeOrder.centralizeOrder, {
                    ...conditionObj__CentralizeOrder
                }, { new: true });
                if (!infoCentralizeOrderAfterUpdateStatus) {
                    return resolve({ error: true, message: 'Không thể cập nhật trạng thái đơn hàng' });
                }
                console.log({
                    infoCentralizeOrderAfterUpdateStatus
                });   

                return resolve({ error: false, data: infoCentralizeOrderAfterUpdateStatus });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

    export({ 
        mergeCentralizeOrderID
    }) {
        return new Promise(async resolve => {
            try {
                let condition = {};
                if (mergeCentralizeOrderID) {
                    if(!checkObjectIDs(mergeCentralizeOrderID))
                        return resolve({ error: true, message: 'param_invalid' });
                    condition = {
                        _id: {
                            $in: [mergeCentralizeOrderID]
                        }
                    }
                }
                console.log({
                    condition
                });
                let listMergeCentralizeOrder = await MERGE_CENTRALIZE_ORDER_COLL.find(condition)
                    .populate({
                        path: 'centralizeOrderGetAddress',
                        // select: 'customer address wardText districtText cityText',

                        populate: {
                            path: 'customer',
                            select: 'fullname email phone'
                        },
                    })
                    .populate({
                        path: 'centralizeOrders',
                        // select: 'centralizeOrderID products customer address wardText districtText cityText',
                        populate: {
                            path: 'customer type products.product',
                            select: 'fullname email phone name SKU provisionalAmount discountPrice amount'
                        }
                    })
                if(!listMergeCentralizeOrder)
                    return resolve({ error: true, message: 'cannot_get_info' });

                    // require('../../../../files')Sheet1
                let fileNameRandom = `Danh_sách_đơn_gộp_${moment(new Date()).format('LT')}_${moment(new Date()).format('DD-MM-YYYY')}.xlsx`
                let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', fileNameRandom);
                
                XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report.xlsx')))
                .then(async workbook => {
                    workbook.sheet("Sheet1").row(1).cell(1).value('STT');
                    workbook.sheet("Sheet1").row(1).cell(2).value('Mã đơn hàng');
                    workbook.sheet("Sheet1").row(1).cell(3).value('Tên sản phẩm');
                    workbook.sheet("Sheet1").row(1).cell(4).value('SKU');
                    workbook.sheet("Sheet1").row(1).cell(5).value('Số lượng');
                    workbook.sheet("Sheet1").row(1).cell(6).value('Tên khách hàng');
                    workbook.sheet("Sheet1").row(1).cell(7).value('Số điện thoại');
                    workbook.sheet("Sheet1").row(1).cell(8).value('Email');
                    workbook.sheet("Sheet1").row(1).cell(9).value('Loại');
                    workbook.sheet("Sheet1").row(1).cell(10).value('Số tiền tạm tính');
                    workbook.sheet("Sheet1").row(1).cell(11).value('Số tiền giảm');
                    workbook.sheet("Sheet1").row(1).cell(12).value('Số tiền còn lại');
                    workbook.sheet("Sheet1").row(1).cell(13).value('Phí vận chuyển');
                    workbook.sheet("Sheet1").row(1).cell(14).value('Trạng thái');
                    workbook.sheet("Sheet1").row(1).cell(15).value('Ngày tạo');
                    console.log({
                        listMergeCentralizeOrder
                    });
                    let indexCount = 1;
                    listMergeCentralizeOrder && listMergeCentralizeOrder.length && listMergeCentralizeOrder.map((item, index) => {
                        item.centralizeOrders.map(centralizeOrder => {
                            centralizeOrder.products.map((product, indexProduct) => {
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(1).value(indexCount);
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(2).value(item.mergeCentralizeOrderID);
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(3).value(product.product.name);
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(4).value(product.product.SKU);
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(5).value(product.quantities);
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(6).value(centralizeOrder.customer.fullname);
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(7).value(centralizeOrder.customer.phone);
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(8).value(centralizeOrder.customer.email);
                                TYPE_CENTRALIZE_ORDER.map(type => {
                                    if (type.value == centralizeOrder.kind) {
                                        workbook.sheet("Sheet1").row(indexCount + 1).cell(9).value(type.text);
                                    }
                                });
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(10).value(centralizeOrder.type.provisionalAmount);
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(11).value(item.discountPrice);
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(12).value(centralizeOrder.type.amount);
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(13).value(item.deliveryPrice);
                                STATUS_ORDER.map(status => {
                                    if (status.value == item.status) {
                                        workbook.sheet("Sheet1").row(indexCount + 1).cell(14).value(status.text);
                                    }
                                });
                                workbook.sheet("Sheet1").row(indexCount + 1).cell(15).value(moment(item.createAt).format('HH:mm DD/MM/YYYY'));
                                indexCount ++;
                            });
                        });
                    });

                    workbook.toFileAsync(pathWriteFile)
                    .then(_ => {
                        // Download file from server
                        return resolve({
                            error: false,
                            data: "/files/upload_excel_temp/" + fileNameRandom,
                            path: fileNameRandom,
                        });
                    });
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
