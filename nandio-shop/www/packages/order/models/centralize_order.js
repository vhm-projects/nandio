"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      		= require('mongoose').Types.ObjectId;
const lodash                      	     	= require('lodash');
const moment                                = require('moment');
/**
 * INTERNAL PACKAGES
 */
const { checkObjectIDs }					= require('../../../utils/utils');
const { randomStringOnlyNumber } 			= require('../../../utils/string_utils');
const sendMail 								= require('../../../mailer/mailer');

/**
 * BASE
 */
const BaseModel 							= require('../../../models/intalize/base_model');

/**
 * MODELS
 */
 const COMMON_MODEL  				= require('../../common/models/common').MODEL;
 const GIAO_HANG_NHANH_MODEL  		= require('../../giao_hang_nhanh/models/giao_hang_nhanh').MODEL;
 const AGENCY_MODEL   				= require('../../store/models/agency').MODEL;

/**
 * COLLECTIONS
 */
const CENTRALIZE_ORDER_COLL  							= require('../databases/centralize_order-coll');
const PRODUCT_COLL  						         	= require('../../product/databases/product-coll');
const GIFT_ADDITIONAL_COLL  						    = require('../../reward/databases/gift_additional-coll');

class Model extends BaseModel {
    constructor() {
        super(CENTRALIZE_ORDER_COLL);
    }

	insert({ 
		type, onModel, customer, onModelCustomer, ward, wardText, district, districtText, city, 
        cityText, products, kind, address, pay_shipping_fee, addressID, totalPrice, provisionalAmount, discountPrice
	}) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(type, customer))
                    return resolve({ error: true, message: 'param_invalid' });

				if(!['order', 'user_register_trial_program', 'order_gift_exchange'].includes(onModel))
					return resolve({ error: true, message: 'onModel do not define' });
				if(!['customer', 'user_register_trial_program'].includes(onModelCustomer))
					return resolve({ error: true, message: 'onModelCustomer do not define' });

				if (!ward || !district || !city) 
					return resolve({ error: true, message: 'Phải nhập đầy đủ thông tin địa chỉ' });

				let dataInsert = {
					centralizeOrderID: randomStringOnlyNumber(10),
					type, onModel, customer, onModelCustomer, 
					ward, wardText, district, districtText, city, cityText, kind
				}

                if (products && products.length) {
                    dataInsert = {
                        ...dataInsert,
                        products
                    }
                }

                if (totalPrice) {
                    if (Number.isNaN(Number(totalPrice)) || Number(totalPrice) < 0) {
					    return resolve({ error: true, message: 'Tổng tiền không hợp lệ' });
                    }
                    dataInsert = {
                        ...dataInsert,
                        totalPrice
                    }
                }

                if (discountPrice) {
                    if (Number.isNaN(Number(discountPrice)) || Number(discountPrice) < 0) {
					    return resolve({ error: true, message: 'Tổng tiền không hợp lệ' });
                    }
                    dataInsert = {
                        ...dataInsert,
                        discountPrice
                    }
                }

                if (provisionalAmount) {
                    if (Number.isNaN(Number(provisionalAmount)) || Number(provisionalAmount) < 0) {
					    return resolve({ error: true, message: 'Tổng tiền không hợp lệ' });
                    }
                    dataInsert = {
                        ...dataInsert,
                        provisionalAmount
                    }
                }
              
                pay_shipping_fee && (dataInsert.pay_shipping_fee = pay_shipping_fee);
                address          && (dataInsert.address          = address);
                addressID        && (dataInsert.addressID        = addressID);
                console.log({
                    dataInsert
                });
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_centralize_order_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getList({ 
        city, district, ward, kind, fromDate, toDate,
        page, 
        limit, 
        keyword 
    }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                let conditionObj = {};

                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    conditionObj.$or = [
                        { address: new RegExp(key, 'i') },
                        { wardText: new RegExp(key, 'i') },
                        { districtText: new RegExp(key, 'i') },
                        { cityText: new RegExp(key, 'i') },
                    ]
                }

                if (city) {
                    conditionObj = {
                        city
                    }
                }

                if (district) {
                    conditionObj = {
                        ...conditionObj,
                        district
                    }
                }

                if (ward) {
                    conditionObj = {
                        ...conditionObj,
                        ward
                    }
                }

                if (kind) {
                    conditionObj = {
                        ...conditionObj,
                        kind
                    }
                }

                if (fromDate) {
                    conditionObj.createAt = {
                        $gte: new Date(moment(fromDate).startOf('day')) 
                    }
                }

                if (toDate) {
                    conditionObj.createAt = {
                        ...conditionObj.createAt,
                        $lte: new Date(moment(toDate).endOf('day'))
                    }
                }

                console.log({
                    conditionObj
                });
                let skip = (page - 1) * limit;
                let count = await CENTRALIZE_ORDER_COLL.count({ ...conditionObj, status: STATUS_ACTIVE });
                let listCentralizeOrder = await CENTRALIZE_ORDER_COLL.find({
                    ...conditionObj, status: STATUS_ACTIVE
                })
                    .limit(limit * 1)
                    .skip(skip)
                    .sort({ createAt: -1 })
					.populate({
						path: 'type',
					})
					.populate({
						path: 'customer',
					})
                    .populate({
                        path: 'products.product',
                        select: 'name',
					})
                if(!listCentralizeOrder)
                    return resolve({ error: true, message: 'cannot_get_centralize_order' });

                let listCentralizeAfterUpdate = [];
                if (listCentralizeOrder && listCentralizeOrder.length) {
                    // let listCentralizeOrderAfterGroup  = lodash .chain(listCentralizeOrder)
                    //                                         // .groupBy('ward district city' `"${item.ward}+${item.key2}"`)
                    //                                         .groupBy(listCentralizeOrder, item => `"${item.ward}+${item.district}+${item.city}"`)
                    //                                         .map((value) => ({ listCentralizeOrder: value }))
                    //                                         .value()
                                                            // .map((value, key) => ({ suppilerID: key, listOrderLines: value }))
                    let grouped = lodash.groupBy(listCentralizeOrder, item => `${item.ward}+${item.district}+${item.city}`);
                    // grouped = Object.values(grouped)
                    grouped = Object.entries(grouped);
                    
                    let listCentralizeAfterGroup = [];
                    grouped.map(item => {
                        listCentralizeAfterGroup = [
                            ...listCentralizeAfterGroup,
                            ...item[1]
                        ]
                    });
                  
                    listCentralizeAfterUpdate = listCentralizeAfterGroup.map((item, index) => {
                        let kind = '';``
                        let badge = '';
                        switch (item.kind) {
                            case 1:
                                kind = 'Hàng đặt mua';
                                badge = 'badge-success';
                                break;
                            case 2:
                                kind = 'Hàng dùng thử';
                                badge = 'badge-info';
                                break;
                            case 3:
                                kind = 'Hàng quà tặng';
                                badge = 'badge-dark';
                                break;
                            default:
                                break;
                        }

                        return {
                            cb: `<input style="margin-left: 8px;" class="text-center cb-centralize-order" type="checkbox" _centralizeOrderID = "${item._id}">`,
                            index: skip + index + 1,
                            centralizeOrderID: item.centralizeOrderID,
                            kind: ` <span class="badge badge-pill ${badge} " style="padding: 5px; font-size: 10px;">${kind}</span>`,
                            fullname:     item.customer     ? item.customer.fullname : '',
                            address:      item.address      ? item.address      : '',
                            wardText:     item.wardText     ? item.wardText     : '',
                            districtText: item.districtText ? item.districtText : '',
                            cityText:     item.cityText     ? item.cityText     : '',
                            createAt:     moment(item.createAt).format('HH:mm DD/MM/YYYY')
                        }
                    });
                }

                return resolve({ error: false, data: listCentralizeAfterUpdate, recordsTotal: count, recordsFiltered: count });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByListCentralizeID({ 
        listCentralizeID, page = 1, limit = 25, keyword 
    }) {
        return new Promise(async resolve => {
            try {
                if (!listCentralizeID || !listCentralizeID.length) {
                    return resolve({ error: true, message: 'Phải chọn ít nhất 1 đơn hàng' });
                }
                if (!checkObjectIDs(listCentralizeID))
                    return resolve({ error: true, message: 'param_invalid' });

                let skip = (page - 1) * limit;
                let count = await PRODUCT_COLL.count({ 
                    _id: {
                        $in: listCentralizeID
                    } 
                });
                let listCentralizeOrder = await CENTRALIZE_ORDER_COLL.find({
                    _id: {
                        $in: listCentralizeID
                    }
                })
                    .limit(limit * 1)
                    .skip(skip)
					.populate({
						path: 'type',
					})
					.populate({
						path: 'customer',
					}).lean()
                
            
                if(!listCentralizeOrder)
                    return resolve({ error: true, message: 'cannot_get_centralize_order' });
                console.log({
                    listCentralizeOrder
                });
                let listCentralizeOrderAfterChange = [];
                listCentralizeOrderAfterChange = listCentralizeOrder.map((item, index) => {
                    return this.getListCentralizeChoiceServerSide({ 
                        infoCentralize: item,
                        index
                    })
                });

                let result = await Promise.all(listCentralizeOrderAfterChange);
                // result.map(aaa => {
                //     console.log({
                //         aaa
                //     });
                // })
                // console.log({
                //     result
                // });
               
                return resolve({ error: false, data: result, recordsTotal: count, recordsFiltered: count });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListCentralizeChoiceServerSide({ 
        infoCentralize,
        index
    }) {
        return new Promise(async resolve => {
            try {
                let conditionObjProduct = {
                    weight: 0,
                    length: 0,
                    width:  0,
                    height: 0,
                };

                let mark = false;
                for (let product of infoCentralize.products) {
                    let infoProduct = await PRODUCT_COLL.findById(product.product).select('weight length width height');
                    if (!infoProduct) {
                        let infoGiftAddtional = await GIFT_ADDITIONAL_COLL.findById(product.product).select('weight length width height');
                        if (infoGiftAddtional) {
                            conditionObjProduct.weight += infoGiftAddtional.weight * product.quantities;
                            conditionObjProduct.length += infoGiftAddtional.length * product.quantities;
                            conditionObjProduct.width  += infoGiftAddtional.width  * product.quantities;
                            conditionObjProduct.height += infoGiftAddtional.height * product.quantities;
                        } else {
                            mark = true;
                            return resolve({
                                error: true,
                                message: 'Mời bạn nhập thông tin (Trọng lượng (gram) || Độ dài (cm) || Độ rộng (cm) || Chiều cao (cm)) của sản phẩm'
                            });
                        }
                    } else {
                        conditionObjProduct.weight += infoProduct.weight * product.quantities;
                        conditionObjProduct.length += infoProduct.length * product.quantities;
                        conditionObjProduct.width  += infoProduct.width  * product.quantities;
                        conditionObjProduct.height += infoProduct.height * product.quantities;
                    }
                }

                if (mark) {
                    return resolve({
                        error: true,
                        message: 'Mời bạn nhập thông tin (Trọng lượng (gram) || Độ dài (cm) || Độ rộng (cm) || Chiều cao (cm)) của sản phẩm'
                    });
                }

                if (!conditionObjProduct.weight || !conditionObjProduct.length || !conditionObjProduct.width || !conditionObjProduct.height) {
                    return resolve({
                        error: true,
                        message: 'Mời bạn nhập thông tin (Trọng lượng (gram) || Độ dài (cm) || Độ rộng (cm) || Chiều cao (cm)) của sản phẩm'
                    });
                }

                let { wardText, districtText, cityText } = infoCentralize;
                let infoAgencyData = await AGENCY_MODEL.getInfo({ 
                    wardName: wardText, districtName: districtText, cityName: cityText 
                });
                console.log({
                    infoAgencyData,  wardText, districtText, cityText 
                });
                if (infoAgencyData.error) {
                    return resolve(infoAgencyData);
                }
                
                let { data: { infoAgency, infoAddressGHN }} = infoAgencyData;

                console.log({
                    districtStore: infoAgency.district,
                    wardStore:     infoAgency.ward,
                    storeID:       infoAgency.storeCodeGHN,
                    wardCustomer:     infoAddressGHN.data.infoWard.WardCode,
                    districtCustomer: infoAddressGHN.data.infoDistrict.DistrictID,
                    weight:           conditionObjProduct.weight ? conditionObjProduct.weight : 0,
                    length:           conditionObjProduct.length ? conditionObjProduct.length : 0,
                    width:            conditionObjProduct.width  ? conditionObjProduct.width  : 0,
                    height:           conditionObjProduct.height ? conditionObjProduct.height : 0,
                });
                let feeGHN = await GIAO_HANG_NHANH_MODEL.calculateFee({ 
                    districtStore: infoAgency.district,
                    wardStore:     infoAgency.ward,
                    storeID:       infoAgency.storeCodeGHN,
                    wardCustomer:     infoAddressGHN.data.infoWard.WardCode,
                    districtCustomer: infoAddressGHN.data.infoDistrict.DistrictID,
                    weight:           conditionObjProduct.weight ? conditionObjProduct.weight : 0,
                    length:           conditionObjProduct.length ? conditionObjProduct.length : 0,
                    width:            conditionObjProduct.width  ? conditionObjProduct.width  : 0,
                    height:           conditionObjProduct.height ? conditionObjProduct.height : 0,
                });
                console.log({
                    feeGHN
                });
                if (feeGHN.code != 200) {
                    return resolve({
                        error: true,
                        ...feeGHN
                    });
                }
                // console.log("===============================ádasds");
                let address =  infoCentralize.address      ? infoCentralize.address + ', '      : '';
                address     += infoCentralize.wardText     ? infoCentralize.wardText + ', '     : '';
                address     += infoCentralize.districtText ? infoCentralize.districtText + ', ' : '';
                address     += infoCentralize.cityText     ? infoCentralize.cityText            : '';
                // console.log({
                //     address
                // });
                return resolve({
                    index: index + 1,
                    centralizeOrderID: infoCentralize.centralizeOrderID,
                    products: infoCentralize.products.length,
                    weight:   `<input type="number" _agency="${infoAgency._id}" class="form-control centralize-weight centralize-weight-${infoCentralize._id}" _centralizeOrderID="${infoCentralize._id}" value="${conditionObjProduct.weight}">`,
                    length:   `<input type="number" _agency="${infoAgency._id}" class="form-control centralize-length centralize-length-${infoCentralize._id}" _centralizeOrderID="${infoCentralize._id}" value="${conditionObjProduct.length}">`,
                    width:    `<input type="number" _agency="${infoAgency._id}" class="form-control centralize-width  centralize-width-${infoCentralize._id}"  _centralizeOrderID="${infoCentralize._id}" value="${conditionObjProduct.width}">`,
                    height:   `<input type="number" _agency="${infoAgency._id}" class="form-control centralize-height centralize-height-${infoCentralize._id}" _centralizeOrderID="${infoCentralize._id}" value="${conditionObjProduct.height}">`,
                    deliveryPrice:   feeGHN.data.total,
                    agency:   infoAgency.name,
                    address:  address,
                });
                // listCentralizeOrderAfterChange = [
                //     ...listCentralizeOrderAfterChange,
                //     {
                //         index: index + 1,
                //         centralizeOrderID: infoCentralize.centralizeOrderID,
                //         products: infoCentralize.products.length,
                //         weight:   conditionObjProduct.weight,
                //         length:   conditionObjProduct.length,
                //         width:    conditionObjProduct.width,
                //         height:   conditionObjProduct.height,
                //         deliveryPrice:   feeGHN.data.total,
                //         agency:   infoAgency.name,
                //         address:  address,
                //     }
                // ];
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    calculateFeeGHN({ 
        weight,
        length,
        width,
        height,
        centralizeOrderGetAddress,
        wardStore,
        districtStore,
        storeID
    }) {
        return new Promise(async resolve => {
            try {
                
                if(!checkObjectIDs(centralizeOrderGetAddress))
                    return resolve({ error: true, message: 'param_invalid' });

                if (!weight || weight < 0) {
                    return resolve({ error: true, message: 'Trọng lượng không hợp lệ' });
                }

                if (!length || length < 0) {
                    return resolve({ error: true, message: 'Độ dài không hợp lệ' });
                }

                if (!width || width < 0) {
                    return resolve({ error: true, message: 'Độ rộng không hợp lệ' });
                }

                if (!height || height < 0) {
                    return resolve({ error: true, message: 'Chiều cao không hợp lệ' });
                }

                let infoCentralize = await CENTRALIZE_ORDER_COLL.findById(centralizeOrderGetAddress);
                if(!infoCentralize)
                    return resolve({ error: true, message: 'cannot_get_centralize_order' });

                let { ward, district, city } = infoCentralize;
                let infoAddress = await COMMON_MODEL.getInfoAddressByDistrictWardCity({ city, district, ward })

                if (infoAddress.error) {
                    return resolve(infoAddress);
                }
                
                let { cityInfo, districtInfo, wardInfo } = infoAddress.data;
                let infoAddressGHN = await GIAO_HANG_NHANH_MODEL.getInfoAddressGHN({ 
                    cityName:     cityInfo.data[1].name_with_type,
                    districtName: districtInfo.data[1].name_with_type,
                    wardName:     wardInfo.data[1].name_with_type,
                });
                if (infoAddressGHN.error) {
                    return resolve(infoAddressGHN);
                }
                let feeGHN = await GIAO_HANG_NHANH_MODEL.calculateFee({ 
                    districtStore,
                    wardStore,
                    storeID,
                    wardCustomer:     infoAddressGHN.data.infoWard.WardCode,
                    districtCustomer: infoAddressGHN.data.infoDistrict.DistrictID,
                    weight,
                    length,
                    width,
                    height,
                });
                
                if (feeGHN.code != 200) {
                    return resolve(infoAddressGHN);
                }
                return resolve({ 
                    error: false, 
                    data: {
                        infoCentralize,
                        infoAddressGHN,
                        feeGHN: feeGHN.data
                    }
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getCentralizeOrders({ 
        customerID,
        status,
        kind,
        seen,
        group,
        page = 1, 
        limit = 25, 
    }) {
        return new Promise(async resolve => {
            try {
                
                if(!checkObjectIDs(customerID))
                    return resolve({ error: true, message: 'ID Khách hàng không hợp lệ' });
                let conditionObj = { 
                    customer: customerID
                };
                if (status) {
                    if (Number.isNaN(Number(status)) || ![1,2,3,4,5,6,7,8,9].includes(Number(status))) {
                        return resolve({ error: true, message: 'Trạng thái không hợp lệ' });
                    }

                    if (group) {
                        const GROUP_STATUS_TRUE  = 1;
                        if (Number.isNaN(Number(group)) || ![GROUP_STATUS_TRUE].includes(Number(group))) {
                            return resolve({ error: true, message: 'Nhóm trạng thái không hợp lệ' });
                        }

                        const STATUS_PENDING = 1;
                        if (status == STATUS_PENDING) { // ĐANG XỬ LÝ
                            conditionObj = {
                                ...conditionObj,
                                status,
                            }
                        }

                        const STATUS_CONFIRM = 2;
                        if (status == STATUS_CONFIRM) { // CHỜ LẤY HÀNG
                            const USER_CONFIRM     = 3;
                            const CREATE_ORDER_GHN = 4;
                            conditionObj = {
                                ...conditionObj,
                                status: {
                                    $in: [STATUS_CONFIRM, USER_CONFIRM, CREATE_ORDER_GHN]
                                }
                            }
                        }

                        const STORING = 5;
                        if (status == STORING) { // ĐANG GIAO
                            const DELIVERING       = 6;
                            conditionObj = {
                                ...conditionObj,
                                status: {
                                    $in: [STORING, DELIVERING]
                                }
                            }
                        }

                        const DELIVERING_SHIPPING_SUCCESS = 7;
                        if (status == DELIVERING_SHIPPING_SUCCESS) { // GIAO THÀNH CÔNG
                            conditionObj = {
                                ...conditionObj,
                                status
                            }
                        }

                    } else {
                        conditionObj = {
                            ...conditionObj,
                            status,
                        }
                    }
                }

                if (kind) {
                    if (Number.isNaN(Number(kind)) || ![1,2,3].includes(Number(kind))) {
                        return resolve({ error: true, message: 'Trạng thái không hợp lệ' });
                    }
                    conditionObj = {
                        ...conditionObj,
                        kind
                    }
                }

                if (seen) {
                    if (!['SEEN', 'NOT_SEEN'].includes(seen)) {
                        return resolve({ error: true, message: 'Trạng thái xem không hợp lệ' });
                    }
                    conditionObj = {
                        ...conditionObj,
                        seen
                    }
                }

                console.log({
                    conditionObj
                });
                let skip = (page - 1) * limit;
                let count = await CENTRALIZE_ORDER_COLL.count({ ...conditionObj });
                let listCentralizeOrder = await CENTRALIZE_ORDER_COLL.find({
                    ...conditionObj
                })
                    .select('centralizeOrderID mergeCentralizeOrderIDGHN status_ghn products kind type onModel deliveryPrice discountPrice totalPrice customer onModelCustomer status createAt seen')
                    .limit(limit * 1)
                    .skip(skip)
                    .sort({ modifyAt: -1 })
					// .populate({
                    //     path: 'addressID',
					// })
					.populate({
                        path: 'products.product',
                        select: 'name brand',
                        populate: {
                            path: 'avatar brand',
                            select: 'name path'
                        }
					})
                    
                if(!listCentralizeOrder)
                    return resolve({ error: true, message: 'cannot_get_centralize_order' });
                let pages = Math.ceil(count/limit);

                return resolve({
                    error: false, 
                    data: listCentralizeOrder, 
                    page: page, 
                    pages
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ 
        centralizeOrderID,
        seen,
    }) {
        return new Promise(async resolve => {
            try {
                
                if(!checkObjectIDs(centralizeOrderID))
                    return resolve({ error: true, message: 'ID Đơn hàng không hợp lệ' });
                let conditionObj = {
                    modifyAt: new Date()
                };
                
                if (seen) {
                    if (!['SEEN', 'NOT_SEEN'].includes(seen)) {
                        return resolve({ error: true, message: 'Trạng thái xem không hợp lệ' });
                    }
                    conditionObj = {
                        ...conditionObj,
                        seen
                    }
                }

                console.log({
                    conditionObj
                });
                let infoCentralizeOrder = await CENTRALIZE_ORDER_COLL.findByIdAndUpdate(centralizeOrderID, conditionObj, { new: true });
                if(!infoCentralizeOrder)
                    return resolve({ error: true, message: 'cannot_update_centralize_order' });
                console.log({
                    infoCentralizeOrder
                });
                return resolve({
                    error: false, 
                    data: infoCentralizeOrder, 
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateByType ({ 
        type,
        status,
    }) {
        return new Promise(async resolve => {
            try {
                
                if(!checkObjectIDs(type))
                    return resolve({ error: true, message: 'ID Đơn hàng không hợp lệ' });
                
                let conditionObj = {
                    modifyAt: new Date()
                };
                
                if (status) {
                    conditionObj = {
                        ...conditionObj,
                        status
                    }
                }

                console.log({
                    conditionObj
                });
                let infoCentralizeOrder = await CENTRALIZE_ORDER_COLL.findOneAndUpdate({ type }, conditionObj, { new: true });
                if(!infoCentralizeOrder)
                    return resolve({ error: true, message: 'cannot_update_centralize_order' });
                console.log({
                    infoCentralizeOrder
                });
                return resolve({
                    error: false, 
                    data: infoCentralizeOrder, 
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getCentralizeOrder({ 
        orderID 
    }) {
        return new Promise(async resolve => {
            try {
                
                if(!checkObjectIDs(orderID))
                    return resolve({ error: true, message: 'ID Đơn hàng không hợp lệ' });
                
                let infoCentralizeOrder = await CENTRALIZE_ORDER_COLL.findById(orderID)
					.populate({
                        path: 'type',
					})
                    .populate({
                        path: 'addressID',
					})
					.populate({
                        path: 'products.product',
                        select: 'name brand',
                        populate: {
                            path: 'avatar brand',
                            select: 'name path'
                        }
					})
                    
                if(!infoCentralizeOrder)
                    return resolve({ error: true, message: 'cannot_get_centralize_order' });

                return resolve({
                    error: false, 
                    data: infoCentralizeOrder, 
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getCentralizeOrderByType({ 
        type 
    }) {
        return new Promise(async resolve => {
            try {
                
                if(!checkObjectIDs(type))
                    return resolve({ error: true, message: 'ID Đơn hàng không hợp lệ' });
                
                let infoCentralizeOrder = await CENTRALIZE_ORDER_COLL.findOne({ type })
                if(!infoCentralizeOrder)
                    return resolve({ error: true, message: 'cannot_get_centralize_order' });

                return resolve({
                    error: false, 
                    data: infoCentralizeOrder, 
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
