const BASE_ROUTE = '/order';
const API = '/api';

const CF_ROUTINGS_ORDER = {
	ADD_ORDER: `${BASE_ROUTE}/add-order`,
	ADD_ORDER_LINE: `${BASE_ROUTE}/add-order-line`,
	DETAIL_ORDER: `${BASE_ROUTE}/detail-order`,
	API_INFO_CART: `${API}/info-cart`,
	API_CALCULATE_PRICE: `${API}/calculate-price`,

	API_ALL_ORDER_LINE: `${API}/all-order-line`,
	API_LIST_PAYMENT_METHOD: `${API}/list-payment-method`,
	UPDATE_ORDER: `${BASE_ROUTE}/update-order`,
	UPDATE_ORDER_LINE: `${BASE_ROUTE}/update-order-line`,
	UPDATE_STATUS_ORDER: `${BASE_ROUTE}/update-status-order`,

	INFO_ORDER: `${BASE_ROUTE}/info-order`,
	INFO_ORDER_LINE: `${BASE_ROUTE}/info-order-line`,

	DELETE_ORDER: `${BASE_ROUTE}/delete-order`,
	DELETE_ORDER_LINE: `${BASE_ROUTE}/delete-order-line`,

	LIST_ORDER: `${BASE_ROUTE}/list-order`,
	
	API_LIST_ORDER_FILTER: `/api${BASE_ROUTE}/list-order-filter`,
	API_LIST_ORDER: `${API}/list-order`,
	API_LIST_ORDER_BY_CUSTOMER: `${API}/order/list-order-by-customer/:customerID`,

	// CENTRALIZE ORDER
	LIST_CENTRALIZE_ORDER:               `/centralize-order/list-centralize-order/:key/:value`,
	LIST_CENTRALIZE_ORDER_SERVER_SIDE:   `/centralize-order/list-centralize-order-server-side`,
	LIST_CENTRALIZE_ORDER_CHOICE:        `/centralize-order/list-centralize-order-choice`,
	GET_DELIVERY_BY_CENTRALIZE_ORDER_ID: `/centralize-order/get-delivery-by-centralize-order-id`,
	API_LIST_CENTRALIZE_ORDER:           `/api/centralize-order/list-centralize-order`,
	API_LIST_CENTRALIZE_ORDER_NOT_SEEN:  `/api/centralize-order/list-centralize-order-seen`,
	API_UPDATE_SEEN_CENTRALIZE_ORDER:    `/api/centralize-order/update-seen/:centralizeOrderID`,

	ADD_MERGE_CENTRALIZE_ORDER:           `/merge-centralize-order/add-merge-centralize-order`,
	LIST_MERGE_CENTRALIZE_ORDER:          `/merge-centralize-order/list-merge-centralize-order`,
	INFO_MERGE_CENTRALIZE_ORDER:          `/merge-centralize-order/info-merge-centralize-order/:mergeCentralizeOrderID`,
	EXPORT_LIST_MERGE_CENTRALIZE_ORDER:   `/merge-centralize-order/export-list-merge-centralize-order`,
	WEBHOOK_GHN_UPDATE_CENTRALIZE_ORDER:  `/webhook/ghn/update-order`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_ORDER = CF_ROUTINGS_ORDER;
