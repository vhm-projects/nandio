"use strict";

const { Schema } 	= require('mongoose');
const BASE_COLL 	= require('../../../database/intalize/base-coll');

/**
 * COLLECTION ORDER CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('merge_centralize_order', {

	// Mã vận đơn
	mergeCentralizeOrderID: {
		type: String,
	}, 

	// Mã vận đơn từ GHN
	mergeCentralizeOrderIDGHN: {
		type: String,
	},

	// centralizeOrders: [{
    //     type:  Schema.Types.ObjectId,
    //     ref: 'centralize_order'
    // }],

	centralizeOrder: {
        type:  Schema.Types.ObjectId,
        ref: 'centralize_order'
    },

	/**
	 * Quản lý tập trung
	 * CUSTOMER // Khách hàng đã đăng nhập
	 * CUSTOMER_ANONYMOUS // Khách hàng vô danh
	 */
	customer: {
        type:  Schema.Types.ObjectId,
        refPath: 'onModel'
    },

	onModel: {
		type: String,
		enum: ['customer', 'user_register_trial_program']
	},

	products: [
		{
			product: {
				type:  Schema.Types.ObjectId,
				refPath: 'products.onModel'
			},
			onModel: {
				type: String,
				enum: ['product', 'gift_additional']
			},
			quantities: {
				type: Number,
				default: 1
			}
		}
	],

	// Đơn chọn lấy địa chỉ
	// centralizeOrderGetAddress: {
    //     type:  Schema.Types.ObjectId,
    //     ref: 'centralize_order'
    // },

	/**
	 * Quản lý tập trung
	 * 1: ORDER
	 * 2: TRIAL
	 * 3: GIFT 
	 */
	 kind: {
		type: Number
	},

	/**
	 * Trọng lượng (gram)
	 */
	weight: {
		type: Number,
		default: 0
    },
	/**
	 * Độ dài (cm)
	 */
	length: {
		type: Number,
		default: 0
    },
	/**
	 * Độ rộng (cm)
	 */
	width: {
		type: Number,
		default: 0
    },
	/**
	 * Chiều cao (cm)
	 */
	height: {
		type: Number,
		default: 0
    },

	// Phí vận chuyển
	deliveryPrice: {
		type: Number,
		default: 0
    },

	// Người tạo đơn
	userCreate: {
		type:  Schema.Types.ObjectId,
        ref: 'user'
	},

	/**
	 * Quản lý tập trung
	 * CUSTOMER // Khách hàng đã đăng nhập
	 * CUSTOMER_ANONYMOUS // Khách hàng vô danh
	 */
    // customer: {
    //     type:  Schema.Types.ObjectId,
    //     refPath: 'onModel'
    // },

	// onModel: {
	// 	type: String,
	// 	enum: ['customer', 'user_register_trial_program']
	// },

	// ID CỬA HÀNG GHN
	storeID: String,

	// Địa chỉ của Customer
	// address: String,
     
	// ward: String,
	// district: String,
	// city: String,

	// wardText: String,
	// districtText: String,
	// cityText: String,

	// wardGHN: String,
	// districtGHN: String,
	// cityGHN: String,

	// Số tiền giảm
	discountPrice: {
		type: Number,
        default: 0,
	},

	// TIỀN BAN ĐẦU
	provisionalAmount: {
		type: Number,
        default: 0,
	},

	// TỔNG TIỀN
	totalPrice: {
		type: Number,
        default: 0,
	},

	// Thoi gian du tinh
	expected_delivery_time: Date,
	/**
	 * Trạng thái đơn (cf_constants)
	 */
	 status: {
		type: Number,
		default: 1
	},
});
