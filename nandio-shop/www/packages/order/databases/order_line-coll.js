"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION ORDER LINE CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('order_line', {
	/**
     * SẢN PHẨM
     */
	product: {
        type:  Schema.Types.ObjectId,
        ref: 'product'
    },
	
	/**
     * Giá sản phẩm tại thời điểm hiện tại
     */
	price: {
        type: Number,
	},

	/**
     * Khách hàng
     */
	customer: {
        type:  Schema.Types.ObjectId,
        ref: 'customer'
    },
	// /**
	//  * Loại giảm giá
	//  * 0: Không giảm giá
	//  * 1: Giảm giá theo tiền
	//  * 2: Giảm giá theo %
	//  */
	// typeDiscount: {
	// 	type: Number,
	// 	default: 0
	// },
	// // Giảm giá
	// discount: {
	// 	type: Number,
    //     default: 0,
	// },
	// Số tiền tạm tính
	provisionalAmount: {
		type: Number,
        default: 0,
	},

	// Thành tiền sau khi giảm
    amountAfterDiscount: {
        type: Number,
        default: 0,
    },

	// Thành tiền
    amount: {
        type: Number,
        default: 0,
    },
	// Số lượng
	quantities: {
        type: Number,
        default: 0
    },
	// Ghi chú
	note: {
        type: String,
        default: ''
    },
	
	/**
	 *  Trạng thái 
	 * 	0: Chưa tạo order
	 *  1: Đã tạo order
	 **/  
	status: {
        type: Number,
        default: 0
    },
});

