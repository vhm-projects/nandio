"use strict";

const { Schema } 	= require('mongoose');
const BASE_COLL 	= require('../../../database/intalize/base-coll');

/**
 * COLLECTION ORDER CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('order', {
	orderID: {
		type: String,
		required: true,
	}, 
	orderLines: [{
        type:  Schema.Types.ObjectId,
        ref: 'order_line'
    }],
	customer: {
        type:  Schema.Types.ObjectId,
		ref: 'customer' 
	},
	// Ghi chú
    note: {
        type: String,
        default: ''
    },
	// Số tiền tạm tính
	provisionalAmount: {
		type: Number,
        default: 0,
	},

	// Số tiền giảm
	discountPrice: {
		type: Number,
        default: 0,
	},
	// Thành tiền
    amount: {
        type: Number,
        default: 0,
    },
	// Phí giao hàng
	deliveryPrice: {
		type: Number,
        default: 0,
	},
	// Ngày giao hàng dự tính
	deliveryDate: {    
		type: Date,
	},
	// Mã giảm giá
	// voucher: {
	// 	type: String,
	// 	default: ''
	// },
	/**
	 * Phương thức thanh toán:
	 * 1: Thanh toán qua thẻ ATM nội địa, thẻ thanh toán quốc tế
	 * 2: Thanh toán khi nhận hàng
	 */
	payment: {
		type: Number,
		default: 2
	},

	// Địa chỉ giao hàng
	address: {
		type:  Schema.Types.ObjectId,
		ref: 'address'
	},
	/**
	 * 0: Đang xử lý
     * 1: Đã nhận
     * 2: Đang giao
     * 3: Đã giao thành công
     */
	status: {
        type: Number,
        default: 0
    },

	/**
	 * Khuyên mãi
	 * Bigsale
	 * fashsale 
	 */
	promotion: { 
		type:  Schema.Types.ObjectId,
        refPath: 'onModel'
	},

	// Khuyến mãi trên big_sale || flash_sale
	onModel: {
		type: String,
		enum: ['big_sale', 'flash_sale']
	},

    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref: 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});
