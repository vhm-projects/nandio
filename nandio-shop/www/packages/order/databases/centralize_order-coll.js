"use strict";

const { Schema } 	= require('mongoose');
const BASE_COLL 	= require('../../../database/intalize/base-coll');

/**
 * COLLECTION ORDER CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('centralize_order', {
	centralizeOrderID: {
		type: String,
	}, 
	
	// Mã vận đơn từ GHN
	mergeCentralizeOrderIDGHN: {
		type: String,
	},

	mergeCentralizeOrder: {
        type:  Schema.Types.ObjectId,
        ref: 'merge_centralize_order'
    },

	// products: [{
    //     type:  Schema.Types.ObjectId,
    //     ref: 'product'
    // }],

	products: [
		{
			product: {
				type:  Schema.Types.ObjectId,
				refPath: 'products.onModel'
			},
			onModel: {
				type: String,
				enum: ['product', 'gift_additional']
			},
			quantities: {
				type: Number,
				default: 1
			},
			totalPrice: {
				type: Number,
				default: 0
			},
			// Chỉ dùng với đổi quà
			point: {
				type: Number,
				default: 0
			},
		}
	],

	/**
	 * Quản lý tập trung
	 * 1: ORDER
	 * 2: TRIAL
	 * 3: GIFT 
	 */
	kind: {
		type: Number
	},
	/**
	 * Quản lý tập trung
	 * ORDER
	 * TRIAL
	 * GIFT 
	 */
	type: { 
		type:  Schema.Types.ObjectId,
        refPath: 'onModel'
	},

	onModel: {
		type: String,
		enum: ['order', 'user_register_trial_program', 'order_gift_exchange']
	},

	/**
	 * Quản lý tập trung
	 * CUSTOMER // Khách hàng đã đăng nhập
	 * CUSTOMER_ANONYMOUS // Khách hàng vô danh
	 */
    customer: {
        type:  Schema.Types.ObjectId,
        refPath: 'onModelCustomer'
    },

	onModelCustomer: {
		type: String,
		enum: ['customer', 'user_register_trial_program']
	},

	transaction: {
        type:  Schema.Types.ObjectId,
        ref: 'transaction'
    },

	addressID: {
        type:  Schema.Types.ObjectId,
        ref: 'address'
    },

	address: String,
     
	ward: String,
	district: String,
	city: String,

	wardText: String,
	districtText: String,
	cityText: String,

	/**
	 * Người trả tiền ship
	 * 1: SHOP/SELLER
	 * 2: BUYER/CONSIGNEE
	 */
	pay_shipping_fee: {
		type: Number,
		default: 2
	},
	
	// Phí vận chuyển
	deliveryPrice: {
		type: Number,
		default: 0
    },

	// ID CỬA HÀNG GHN
	storeID: String,

	// Số tiền giảm
	discountPrice: {
		type: Number,
        default: 0,
	},

	// TIỀN BAN ĐẦU
	provisionalAmount: {
		type: Number,
        default: 0,
	},

	// TỔNG TIỀN
	totalPrice: {
		type: Number,
        default: 0,
	},

	// Thoi gian du tinh
	expected_delivery_time: Date,

	seen: {
		type: String,
		enum: ['SEEN', 'NOT_SEEN'],
		default: 'NOT_SEEN'
	},

	status_ghn: [{
		name:        String,
		description: String,
		timeGhn:     Date,
		createAt:    Date,
	}],

	/**
	 * Trạng thái hoạt động.
	 * 1: Chờ xác nhận
	 * 2: Đã xác nhận
	 */
	 status: {
		type: Number,
		default: 1
	},
});
