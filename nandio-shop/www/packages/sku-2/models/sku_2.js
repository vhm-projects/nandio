"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGES
 */

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { checkObjectIDs, isTrue }    = require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const SKU_2_COLL  				= require('../databases/sku_2-coll');


class Model extends BaseModel {
    constructor() {
        super(SKU_2_COLL);
    }

    /**
     * FUNCTION: INSERT 
     * AUTHOR: SONLP
     * DATE: 03/07/2021
     */
	insert({ name, variant, brand, isGoodBuy, isGoodReward }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK TÊN CÓ TỒN TẠI
                 */
                if(!name) {
                    return resolve({ error: true, message: 'Mời bạn nhập tên SKU 2' });
                }
                if(!variant) {
                    return resolve({ error: true, message: 'Mời bạn nhập biến thể' });
                }
                if(!brand) {
                    return resolve({ error: true, message: 'Mời bạn chọn thương hiệu' });
                }

                let checkExist = await SKU_2_COLL.findOne({ name });
                if (checkExist) {
                    return resolve({ error: true, message: 'Tên SKU 2 đã tồn tại' });
                }

                let dataInsert = {
                    name, variant, brand
                }

                if (isTrue(isGoodBuy)) {
                    dataInsert = {
                        ...dataInsert,
                        isGoodBuy: true
                    }
                } else {
                    dataInsert = {
                        ...dataInsert,
                        isGoodBuy: false
                    }
                }

                if (isTrue(isGoodReward)) {
                    dataInsert = {
                        ...dataInsert,
                        isGoodReward: true
                    }
                } else {
                    dataInsert = {
                        ...dataInsert,
                        isGoodReward: false
                    }
                }
               
                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'create_segment_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: LIST SEGEMNT
     * AUTHOR: SONLP
     * DATE: 03/07/2021
     */
	getList({  }) {
        return new Promise(async resolve => {
            try {
                let listData = await SKU_2_COLL.find({ })
                    .populate({
                        path: 'brand',
                        select: 'name'
                    })
                    .sort({ modifyAt: -1 })
                
                if(!listData)
                    return resolve({ error: true, message: 'get_list_failed' });

                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ sku2ID }) {
        return new Promise(async resolve => {
            try {
                let infoData = await SKU_2_COLL.findById(sku2ID)
                
                if(!infoData)
                    return resolve({ error: true, message: 'get_list_failed' });

                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: UPDATE SEGEMNT
     * AUTHOR: SONLP
     * DATE: 03/07/2021
     */
	update({ 
        sku2ID,
        name,
        variant,
        brand,
        isGoodReward,
        isGoodBuy, 
    }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK TÊN CÓ TỒN TẠI
                 */
                if (!checkObjectIDs(sku2ID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }
                if(!name) {
                    return resolve({ error: true, message: 'Mời bạn nhập tên SKU 2' });
                }
                if(!variant) {
                    return resolve({ error: true, message: 'Mời bạn nhập biến thể' });
                }
                if(!brand) {
                    return resolve({ error: true, message: 'Mời bạn chọn thương hiệu' });
                }

                let checkExist = await SKU_2_COLL.findOne({ 
                    name, 
                    _id: {
                        $nin: [sku2ID]
                    } 
                });

                if (checkExist) {
                    return resolve({ error: true, message: 'Tên SKU 2 đã tồn tại' });
                }


                let dataUpdate = {
                    name,
                    variant,
                    brand,
                    modifyAt: new Date()
                }

                if (isGoodBuy) {
                    if (isTrue(isGoodBuy)) {
                        dataUpdate = {
                            ...dataUpdate,
                            isGoodBuy: true
                        }
                    } else {
                        dataUpdate = {
                            ...dataUpdate,
                            isGoodBuy: false
                        }
                    }
                }

                if (isGoodReward) {
                    if (isTrue(isGoodReward)) {
                        dataUpdate = {
                            ...dataUpdate,
                            isGoodReward: true
                        }
                    } else {
                        dataUpdate = {
                            ...dataUpdate,
                            isGoodReward: false
                        }
                    }
                }
               
               
                let infoAfterUpdate = await SKU_2_COLL.findByIdAndUpdate(sku2ID, {...dataUpdate}, { new: true });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_failed' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateDynamic({ 
        skuID,
        isGoodBuy,
        isGoodReward,
    }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK TÊN CÓ TỒN TẠI zxc
                 */
                if (!checkObjectIDs(skuID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }
                let dataUpdate = { modifyAt: new Date() }
                if (isGoodBuy) {
                    if (isTrue(isGoodBuy)) {
                        dataUpdate = {
                            ...dataUpdate,
                            isGoodBuy: true
                        }
                    } else {
                        dataUpdate = {
                            ...dataUpdate,
                            isGoodBuy: false
                        }
                    }
                }

                if (isGoodReward) {
                    if (isTrue(isGoodReward)) {
                        dataUpdate = {
                            ...dataUpdate,
                            isGoodReward: true
                        }
                    } else {
                        dataUpdate = {
                            ...dataUpdate,
                            isGoodReward: false
                        }
                    }
                }
               
                let infoAfterUpdate = await SKU_2_COLL.findByIdAndUpdate(skuID, {
                    ...dataUpdate
                }, { 
                    new: true 
                });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_failed' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    
}

exports.MODEL = new Model;
