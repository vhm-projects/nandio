const SKU_2_MODEL 		= require('./models/sku_2').MODEL;
const SKU_2_COLL  		= require('./databases/sku_2-coll');
const SKU_2_ROUTES       = require('./apis/sku_2');

module.exports = {
    SKU_2_ROUTES,
    SKU_2_COLL,
    SKU_2_MODEL,
}
