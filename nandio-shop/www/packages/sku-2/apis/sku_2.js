"use strict";
const path = require('path');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { CF_ROUTINGS_SKU_2 } 				= require('../constants/sku_2.uri');
const { CF_ROUTINGS_PARCEL } 				= require('../constants/parcel.uri');

/**
 * MODELS
 */
const SKU_2_MODEL          = require('../models/sku_2').MODEL;
const PARCEL_MODEL          = require('../models/parcel').MODEL;
const { BRAND_MODEL }      = require('../../brand');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ==========================       QUẢN LÝ SKU 2      ================================
             * ========================== ************************ ================================
             */

			/**
             * Function: TẠO API
             */
             [CF_ROUTINGS_SKU_2.ADD_SKU_2]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_SKU_2.ADD_SKU_2,
                    inc : path.resolve(__dirname, '../views/add_sku_2.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [async (req, res) => {
                        /**
                         *  1// LẤY THƯƠNG HIỆU
                         */
                        let listBrand    = await BRAND_MODEL.getList();

                        ChildRouter.renderToView(req, res, {
                            listBrand: listBrand.data
                        });
                    }],
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        const {
                            name,
                            variant,
                            brand,
                            isGoodReward,
                            isGoodBuy,
                        } = req.body;

                        
                        const infoAfterInsert = await SKU_2_MODEL.insert({ 
                            name,
                            variant,
                            brand,
                            isGoodReward,
                            isGoodBuy,
                        });

                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: LIST API
             * Date: 03/07/2021
             * Dev: SONLP
             */
             [CF_ROUTINGS_SKU_2.LIST_SKU_2]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_SKU_2.LIST_SKU_2,
                    inc : path.resolve(__dirname, '../views/list_sku_2.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [async (req, res) => {
                        const listData = await SKU_2_MODEL.getList({});
                       
                        ChildRouter.renderToView(req, res, { 
                            listData: listData.data 
                        });
                    }]
                },
            },

            /**
             * Function: UPDATE API
             */
             [CF_ROUTINGS_SKU_2.UPDATE_SKU_2]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_SKU_2.UPDATE_SKU_2,
                    inc : path.resolve(__dirname, '../views/update_sku_2.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [async (req, res) => {
                        let { sku2ID }   = req.params;
                      
                        let infoSKU = await SKU_2_MODEL.getInfo({ sku2ID });
                        /**
                         *  1// LẤY THƯƠNG HIỆU
                         */
                        let listBrand    = await BRAND_MODEL.getList();

                        ChildRouter.renderToView(req, res, { 
                            listBrand: listBrand.data,
                            infoSKU: infoSKU.data
                        });
                    }],
                    post: [ async function (req, res) {
                        let { sku2ID }   = req.params;
                        const {
                            name,
                            variant,
                            brand,
                            isGoodReward,
                            isGoodBuy,
                        } = req.body;
                        
                        const infoAfterInsertSegment = await SKU_2_MODEL.update({ 
                            sku2ID,
                            name,
                            variant,
                            brand,
                            isGoodReward,
                            isGoodBuy,
                        });

                        res.json(infoAfterInsertSegment);
                    }]
                },
            },

            /**
             * Function: UPDATE API DYNAMIC
             * Dev: SONLP
             */
             [CF_ROUTINGS_SKU_2.UPDATE_DYNAMIC_SKU_2]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { 
                            skuID,
                            isGoodBuy,
                            isGoodReward
                        } = req.body;
                        const infoAfterInsertSegment = await SKU_2_MODEL.updateDynamic({ 
                            skuID,
                            isGoodBuy,
                            isGoodReward,
                        });

                        res.json(infoAfterInsertSegment);
                    }]
                },
            },
             /**
             * ========================== ************************ ================================
             * ==========================       QUẢN LÝ SKU 2      ================================
             * ========================== ************************ ================================
             */

            /**
             * ========================== ************************ ================================
             * ==========================      QUẢN LÝ PARCEL      ================================
             * ========================== ************************ ================================
             */

            /**
             * Function: TẠO API
             */
             [CF_ROUTINGS_PARCEL.ADD_PARCEL]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_PARCEL.ADD_PARCEL,
                    inc : path.resolve(__dirname, '../views/add_parcel.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [async (req, res) => {
                        const listSKU2 = await SKU_2_MODEL.getList({});

                        ChildRouter.renderToView(req, res, {
                            listSKU2: listSKU2.data
                        });
                    }],
                    post: [ async function (req, res) {
                        const {
                            quantities_parcel,
                            point,
                            sku_2,
                            manufactureDate,
                            expiryDate,
                        } = req.body;

                        
                        const infoAfterInsert = await PARCEL_MODEL.insert({ 
                            quantities_parcel,
                            point,
                            sku_2,
                            manufactureDate,
                            expiryDate,
                        });

                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: LIST API
             * Date: 03/07/2021
             * Dev: SONLP
             */
            [CF_ROUTINGS_PARCEL.LIST_PARCEL]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_PARCEL.LIST_PARCEL,
                    inc : path.resolve(__dirname, '../views/list_parcel.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [async (req, res) => {
                        let { fromDay, toDay, brand, sku_2, isGood } = req.query;
                       
                        const listSKU2     = await SKU_2_MODEL.getList({});
                        const listBrand    = await BRAND_MODEL.getList();

                        ChildRouter.renderToView(req, res, { 
                            listBrand: listBrand.data,
                            listSKU2:  listSKU2.data,
                            fromDay, toDay, brand, sku_2, isGood
                        });
                    }]
                },
            },

            /**
             * Function: LIST API SERVER SIDE
             * Date: 03/07/2021
             * Dev: SONLP
             */
            [CF_ROUTINGS_PARCEL.LIST_PARCEL_PAGINATION]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async (req, res) => {
                        let { brand, isGood, sku_2, fromDay, toDay,  start, length, search } = req.body;
                        let page = Number(start)/Number(length) + 1;

                        const listData = await PARCEL_MODEL.getList({ 
                            brand, isGood, sku_2, fromDay, toDay,
                            page: Number(page), 
                            limit: Number(length), 
                            keyword: search.value 
                        });
                        
                        res.json(listData)
                    }]
                },
            },

            /**
             * Function: UPDATE API
             */
             [CF_ROUTINGS_PARCEL.UPDATE_PARCEL]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_PARCEL.UPDATE_PARCEL,
                    inc : path.resolve(__dirname, '../views/update_parcel.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [async (req, res) => {
                        let { parcelID }   = req.params;
                      
                        const infoParcel = await PARCEL_MODEL.getInfo({ parcelID });

                        const listSKU2 = await SKU_2_MODEL.getList({});

                        ChildRouter.renderToView(req, res, { 
                            listSKU2: listSKU2.data,
                            infoParcel: infoParcel.data
                        });
                    }],
                    post: [ async function (req, res) {
                        let { parcelID }   = req.params;
                        const {
                            quantities_parcel,
                            point,
                            sku_2,
                            manufactureDate,
                            expiryDate,
                        } = req.body;
                        
                        const infoAfterInsertSegment = await PARCEL_MODEL.update({ 
                            parcelID,
                            quantities_parcel,
                            point,
                            sku_2,
                            manufactureDate,
                            expiryDate,
                        });

                        res.json(infoAfterInsertSegment);
                    }]
                },
            },

            /**
             * Function: UPDATE API DYNAMIC
             * Dev: SONLP
             */
            [CF_ROUTINGS_PARCEL.UPDATE_DYNAMIC_PARCEL]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { 
                            parcelID,
                            point,
                        } = req.body;
                        const infoAfterInsert = await PARCEL_MODEL.updateDynamic({ 
                            parcelID,
                            point,
                        });

                        res.json(infoAfterInsert);
                    }]
                },
            },

            [CF_ROUTINGS_PARCEL.SYNC_PARCEL]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { 
                            data
                        } = req.body;
                        const infoAfterInsert = await PARCEL_MODEL.syncData({ 
                            data
                        });

                        res.json(infoAfterInsert);
                    }]
                },
            },

             /**
             * ========================== ************************ ================================
             * ==========================      QUẢN LÝ PARCEL      ================================
             * ========================== ************************ ================================
             */
        }
    }
};
