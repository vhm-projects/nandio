"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION SLIDE CỦA HỆ THỐNG
 */
module.exports = BASE_COLL("ranking", {
        /**
         * Hình ảnh slide
         */
        name: {
            type: String,
            require
        },
        minPoint: {
            type: Number,
        },
        maxPoint: {
            type: Number,
        },
        status: {
            type: Number,
            default: 1
        },
        logo: {
            type:  Schema.Types.ObjectId,
            ref: "image"
        },
        // Màu process ranking
        color: {
            type:  String,
        },
        // Đặc quyền
        benefits: [
          {
            type: String,
          }
        ],
        // Điều khoản điều kiện
        rules: [
            {
              type: String,
            }
        ],
        //_________Người tạo
        userCreate: {
            type:  Schema.Types.ObjectId,
            ref : 'user'
        },
        //_________Người cập nhật
        userUpdate: {
            type:  Schema.Types.ObjectId,
            ref : 'user'
        },
    });
