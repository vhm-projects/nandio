"use strict";
/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                             = require('../../../routing/child_routing');
const roles                                   = require('../../../config/cf_role');
const RANKING_MODEL                           = require('../models/ranking').MODEL;
const { CF_ROUTINGS_RANKING }                 = require("../constants/ranking.uri");
/**
 * MODELS, COLLECTIONS
 */
module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
          
            /**
             * Function: Thêm ranking (API)
             * Date: 20/08/2021
             * Dev: Depv
             */
            [CF_ROUTINGS_RANKING.ADD_RANKING]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async function (req, res) {
                        const { name, minPoint, maxPoint, benefits, rules, logo, color } = req.body;
                        let infoAfterInsert = await RANKING_MODEL.insert({ name, minPoint, maxPoint, benefits, rules, logo, color });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            
            /**
             * Function: CẬP NHẬT QUẢN LÝ LÃI
             * Date: 17/04/2021
             * Dev: DEPV
             */
            [CF_ROUTINGS_RANKING.UPDATE_RANKING]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async function (req, res) {
                        const { rankingID, name, benefits, rules, logo, color  } = req.body;
                        let infoAfterUpdate = await RANKING_MODEL.update({ rankingID, name, benefits, rules, logo, color });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: danh sách ranking
             * Date: 17/04/2021
             * Dev: DEPV
             */
             [CF_ROUTINGS_RANKING.LIST_RANKING_API]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [async function (req, res) {
                        let listData = await RANKING_MODEL.getListForAdmin();
                        res.json(listData);
                    }]
                },
            },

             /**
             * Function: Lấy thông tin quản lý lãi
             * Date: 17/04/2021
             * Dev: DEPV
             */
            [CF_ROUTINGS_RANKING.INFO_RANKING]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [async function (req, res) {
                        const { rankingID } = req.params;
                        let infoAfterUpdate = await RANKING_MODEL.getInfo({ rankingID });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

             /**
             * Function: Lấy thông ranking qua point
             * Date: 17/04/2021
             * Dev: DEPV
             */
            [CF_ROUTINGS_RANKING.INFO_RANKING_BY_POINT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [async function (req, res) {
                        const { point } = req.params;
                        let infoData = await RANKING_MODEL.getInfoByPoint({ point: +point });
                        res.json(infoData);
                    }]
                },
            },

             /**
             * Function: Xoá Quản lý lãi
             * Date: 17/04/2021
             * Dev: DEPV
             */
              [CF_ROUTINGS_RANKING.REMOVE_RANKING]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [async function (req, res) {
                        const { _id: userID } = req.user;
                        const { rankingID  } = req.params;
                        let infoAfterUpdate = await RANKING_MODEL.removeByID({ rankingID  });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            // LIST RANKING
            [CF_ROUTINGS_RANKING.LIST_RANKING]: {
                config: {
                    auth: [ roles.role.editer.bin ],
					type: 'view',
					title: `List Ranking`,
					code: CF_ROUTINGS_RANKING.LIST_RANKING,
					inc: path.resolve(__dirname, '../views/list_ranking.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let listData = await RANKING_MODEL.getListForAdmin();
                        ChildRouter.renderToView(req, res, {
                            listData: listData.data
						});
                    }]
                },
            },
        }
    }
};
