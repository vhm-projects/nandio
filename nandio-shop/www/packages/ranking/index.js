const RANKING_MODEL 		= require('./models/ranking').MODEL;
const RANKING_COLL  		= require('./databases/ranking-coll');
const RANKING_ROUTES       = require('./apis/ranking');

module.exports = {
    RANKING_ROUTES,
    RANKING_COLL,
    RANKING_MODEL
}