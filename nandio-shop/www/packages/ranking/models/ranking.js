"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;
const request  = require('request');
const lodash  = require('lodash');

/**
 * INTERNAL PAKCAGE
 */
 const jwt                           = require('jsonwebtoken');
 const { hash, hashSync, compare }   = require('bcryptjs');
/**
 * BASE
 */
const BaseModel               = require('../../../models/intalize/base_model');
const { calculateInteres }    = require("../../../utils/utils");
const { subStractDateGetDay}  = require('../../../config/cf_time');

/**
 * MODELS
 */
const  IMAGE_MODEL           = require('../../image/models/image').MODEL;

/**
 * COLLECTIONS
 */
const  RANKING_COLL           = require('../databases/ranking-coll.js');

class Model extends BaseModel {
    constructor() {
        super(RANKING_COLL);
    }

	insert({ name, minPoint, maxPoint, benefits, rules, logo, color }) {
        return new Promise(async resolve => {
            try {
                
                let checkData = await RANKING_COLL.find().sort({ maxPoint: -1 });
                
                // Kiểm tra ngày lớn nhất
                let maxRanking = 0;
                if(checkData.length){
                    maxRanking = checkData[0].maxPoint;
                }

                if( maxRanking != 0 && minPoint != maxRanking + 1){
                    return resolve({ error: true, message: `from_invalid`, from: maxRanking + 1 });
                }

                if( maxRanking == 0 && minPoint != 0 ){
                    return resolve({ error: true, message: `from_invalid`, from: 0 });
                }

                if(Number(minPoint) > Number(maxPoint))
                    return resolve({ error: true, message: 'maxPoint_invalid' });
                let dataInsert = {
                    name, minPoint, maxPoint, benefits, rules, color
                };

                if(logo){
                    let { name, size, path } = logo;
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path
                    });
                    dataInsert.logo = ObjectID(infoImageAfterInsert.data._id);
                }
                
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ rankingID, name, benefits, rules, logo, color }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(rankingID))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataUpdate = {
                    name, benefits, rules, color
                };

                if(logo){
                    let { name, size, path } = logo;
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path
                    });
                    dataUpdate.logo = ObjectID(infoImageAfterInsert.data._id);
                }
                
                let infoAfterUpdate = await RANKING_COLL.findByIdAndUpdate(rankingID, dataUpdate, { new: true });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'update_failed' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    getListForAdmin() {
        return new Promise(async resolve => {
            try {
                let listData = await RANKING_COLL.find().populate({
                    path: "logo",
                    select: "path"
                }).sort({ minPoint: 1 });
                if(!listData)
                    return resolve({ error: true, message: 'get_list_faild' });
                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ rankingID }) {
        return new Promise(async resolve => {
            try {
                let infoData = await RANKING_COLL.findById(rankingID).populate({
                    path: "logo",
                    select: "path"
                });
                if(!infoData)
                    return resolve({ error: true, message: 'get_info_failded' });

                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoByPoint({ point }) {
        return new Promise(async resolve => {
            try {
                let infoData = await RANKING_COLL.findOne({
                    minPoint: {
                        $lte: point
                    },
                    maxPoint: {
                        $gte: point
                    }
                }).populate({
                    path: "logo",
                    select: "path"
                });
                if(!infoData)
                    return resolve({ error: true, message: 'get_info_failded' });

                let { maxPoint } = infoData;
                let infoRankingNext = await RANKING_COLL.findOne({
                    minPoint: {
                        $lte: maxPoint+1
                    },
                    maxPoint: {
                        $gte: maxPoint+1
                    }
                })

                infoData._doc.nameRankingNext = infoRankingNext? infoRankingNext.name : "";
                // console.log({ infoData });
                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    removeByID({ rankingID }) {
        return new Promise(async resolve => {
            try {
                let infoData = await RANKING_COLL.findByIdAndDelete(rankingID);
                if(!infoData)
                    return resolve({ error: true, message: 'cannot_remove' });

                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
