const BASE_ROUTE = '/ranking';
const BASE_ROUTE_ADMIN = '/admin';

const CF_ROUTINGS_RANKING = {
    ADD_RANKING: `${BASE_ROUTE}/add-ranking`,
    LIST_RANKING: `${BASE_ROUTE_ADMIN}/list-ranking`,
    LIST_RANKING_API: `${BASE_ROUTE}/list-ranking`,
    INFO_RANKING: `${BASE_ROUTE}/info-ranking/:rankingID`,
    INFO_RANKING_BY_POINT: `${BASE_ROUTE}/info-ranking-by-point/:point`,
    UPDATE_STATUS_RANKING: `${BASE_ROUTE}/update-status-ranking`,
    UPDATE_RANKING: `${BASE_ROUTE}/update-ranking`,
    REMOVE_RANKING: `${BASE_ROUTE}/remove-ranking/:rankingID`,
    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_RANKING = CF_ROUTINGS_RANKING;
