"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path									= require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { uploadSingle }                      = require('../../../config/cf_helpers_multer');
const { HISTORY_POINT_TYPE }                = require('../../../config/cf_constants');
const { 
    CF_ROUTINGS_HISTORY_POINT, 
    CF_ROUTINGS_DELIVERY_POINT_CONFIG 
} = require('../constants/history_point.uri');

/**
 * MODELS
 */
const HISTORY_POINT_MODEL 					= require('../models/history_point').MODEL;
const DELIVERY_POINT_CONFIG_MODEL 			= require('../models/delivery_point_config').MODEL;
const ORDER_GIFT_EXCHANGE_MODEL 			= require('../models/order_gift_exchange').MODEL;
const CUSTOMER_RECEIVE_POINT_CONFIG_MODEL 	= require('../models/customer_receive_point_config').MODEL;

/**
 * COLLECTIONS
 */
const GROUP_MEMBER_POINT_CONFIG_COLL        = require('../databases/group_member_point_config-coll');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ====================== ****************************** ================================
             * ======================  QUẢN LÝ LỊCH SỬ ĐỔI ĐIỂM  ================================
             * ====================== ****************************** ================================
             */

            /**
             * Function: Thên lịch sử đổi điểm (API)
             * Date: 05/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.ADD_HISTORY_POINT]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
                        let { _id: customerID } = req.customer;
                        const { productID, transactionID, rewards, addressID, type, typeGiftExchange, buyType, amount } = req.body;

                        const infoAfterInsert = await ORDER_GIFT_EXCHANGE_MODEL.insert({ 
                            customer: customerID, rewards, type: typeGiftExchange, addressID
							// productID, customerID, transactionID, rewards, type, typeGiftExchange, amount
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
             * Function: Thông tin lịch sử đổi điểm (API)
             * Date: 05/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.INFO_HISTORY_POINT]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { orderGiftExchangeID } = req.query;
                        const infoOrderGiftExchange = await ORDER_GIFT_EXCHANGE_MODEL.getOrderGiftExchange({ orderGiftExchangeID });
                        res.json(infoOrderGiftExchange);
                    }]
                },
            },


			/**
             * Function: Danh sách lịch sử đổi điểm của customer (API)
             * Date: 05/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.LIST_HISTORY_POINT_BY_CUSTOMER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { customerID, type, fromDate, toDate, page, limit, sort } = req.query;

                        const listHistoryPointOfCustomer = await HISTORY_POINT_MODEL.getListByCustomer({ 
							customerID, type, fromDate, toDate, page, limit, sort
						});
                        res.json(listHistoryPointOfCustomer);
                    }]
                },
            },

            /**
             * Function: Danh sách lịch sử tích đổi điểm của customer theo type (API)
             * Date: 05/07/2021
             * Dev: depv
             */
            [CF_ROUTINGS_HISTORY_POINT.LIST_HISTORY_POINT_OF_CUSTOMER_BY_TYPE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } = req.customer;
                        const { type, fromDate, toDate, page, limit, typePoint } = req.query;

                        const listHistoryPointOfCustomer = await HISTORY_POINT_MODEL.getListByCustomer({ 
							customerID, type, fromDate, toDate, page, limit, typePoint
						});
                        res.json(listHistoryPointOfCustomer);
                    }]
                },
            },

			/**
             * Function: Danh sách lịch sử đổi điểm (API)
             * Date: 05/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.API_LIST_HISTORY_POINT]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
						const { type, page, limit } = req.query;

                        const listHistoryPoint = await HISTORY_POINT_MODEL.getListWithPaging({ type, page, limit });
                        res.json(listHistoryPoint);
                    }]
                },
            },

			/**
             * Function: Danh sách lịch sử đổi điểm (VIEW)
             * Date: 05/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.LIST_HISTORY_POINT]: {
                config: {
					auth: [ roles.role.editer.bin ],
					type: 'view',
                    view: 'index.ejs',
					title: 'List History Point - NANDIO',
					code: CF_ROUTINGS_HISTORY_POINT.LIST_HISTORY_POINT,
					inc: path.resolve(__dirname, '../views/list_history_point.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
						const { typePoint } = req.query;
                        const listHistoryPoint = await HISTORY_POINT_MODEL.getList({ type: +typePoint });
                        
                        ChildRouter.renderToView(req, res, {
							listHistoryPoint: listHistoryPoint.data || [],
							typePoint,
						})
                    }]
                },
            },

			/**
             * Function: Danh sách quà đã đổi (API)
             * Date: 05/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.API_LIST_REWARD_OF_YOU]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } = req.customer;
						const { page, limit } = req.query;
                        const listHistoryPoint = await HISTORY_POINT_MODEL.getListRewardOfYou({ customerID, page: +page, limit: +limit });
                        res.json(listHistoryPoint);
                    }]
                },
            },

            /**
             * Function: Danh sách quà đã đổi theo danh sách customer (API)
             * Date: 03/12/2021
             * Dev: MinhVH
             */
            // [CF_ROUTINGS_HISTORY_POINT.API_LIST_REWARD_OF_LIST_CUSTOMER]: {
            //     config: {
            //         auth: [ roles.role.all.bin ],
            //         type: 'json'
            //     },
            //     methods: {
            //         post: [ async function (req, res) {
            //             let { customersID } = req.body;

            //             const listHistoryPoint = await HISTORY_POINT_COLL
            //                 .find({ customer: { $in: customersID } })
            //                 .lean();

            //             res.json(listHistoryPoint);
            //         }]
            //     },
            // },
            
             /**
             * Function: Thêm thông tin chuyển điểm (API)
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.ADD_INFO_TRANSFER_POINT]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: senderID } = req.customer;
						const { receiverID, content, point, lnt, lat, pinCode } = req.body;

                        const response = await HISTORY_POINT_MODEL.createInfoTransferPoint({
                            senderID, receiverID, content, point, lnt, lat, pinCode
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Danh sách giao dịch chuyển/nhận điểm (API)
             * Dev: MinhVH
             */
            [CF_ROUTINGS_HISTORY_POINT.API_LIST_POINT_TRANSFER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { _id: customerID } = req.customer;
						const { page, limit } = req.query;

                        const response = await HISTORY_POINT_MODEL.getListTransferPoint({
                            customerID, page, limit
                        });
                        res.json(response);
                    }]
                },
            },

            /**
             * Function: Danh sách lịch sử điểm của GLOQ (VIEW)
             */
            [CF_ROUTINGS_HISTORY_POINT.LIST_HISTORY_POINT_GLOQ]: {
                config: {
					auth: [ roles.role.gloq.bin ],
					type: 'view',
                    view: 'index.ejs',
					title: 'List History Point - NANDIO',
					code: CF_ROUTINGS_HISTORY_POINT.LIST_HISTORY_POINT_GLOQ,
					inc: path.resolve(__dirname, '../views/list_history_point_gloq.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        let { customer }    = req.user;
						const { typePoint } = req.query;
                       
                        const listHistoryPoint = await HISTORY_POINT_MODEL.getListHistoryPoint__GLOQ({ 
                            type: +typePoint,
                            customer
                        });

                        ChildRouter.renderToView(req, res, {
							listHistoryPoint: listHistoryPoint.data || [],
							typePoint,
                            HISTORY_POINT_TYPE
						})
                    }]
                },
            },

            /**
             * Function: NẠP ĐIỂM VÀO TK GLOQ (API)
             */
             [CF_ROUTINGS_HISTORY_POINT.ADD_HISTORY_POINT_BY_GLOQ]: {
                config: {
                    auth: [ roles.role.gloq.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
                        let { customer }    = req.user;
                        const { point }     = req.body;

                        const ADD_POINT = 7; // NẠP ĐIỂM
                        const infoAfterInsert = await HISTORY_POINT_MODEL.insert({ 
                            customerID: customer, 
                            type: ADD_POINT, 
                            currentPoint: point
						});
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: CHUYỂN ĐIỂM CHO NANDIO ADMIN (API)
             */
             [CF_ROUTINGS_HISTORY_POINT.ADD_HISTORY_POINT_TRANSFER_BY_GLOQ]: {
                config: {
                    auth: [ roles.role.gloq.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
                        let { customer: sender }            = req.user;
                        const { point, user: receiver }     = req.body;

                        const infoAfterInsert = await HISTORY_POINT_MODEL.createInfoTransferPoint({ 
                            senderID:   sender, 
                            receiverID: receiver,
                            point,
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: DANH SÁCH LỊCH SỬ CHUYỂN/NHẬN ĐIỂM (VIEW)
             */
            [CF_ROUTINGS_HISTORY_POINT.LIST_HISTORY_POINT_TRANSFER]: {
                config: {
					auth: [ roles.role.editer.bin ],
					type: 'view',
                    view: 'index.ejs',
					title: 'List History Point - NANDIO',
					code: CF_ROUTINGS_HISTORY_POINT.LIST_HISTORY_POINT_TRANSFER,
					inc: path.resolve(__dirname, '../views/list_history_point_transfer.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        const listHistoryPoint = await HISTORY_POINT_MODEL.getListTransfer({  });
                        
                        ChildRouter.renderToView(req, res, {
							listHistoryPoint: listHistoryPoint.data || [],
						})
                    }]
                },
            },

              /**
             * Function: TẠO LỊCH SỬ CHUYỂN/NHẬN ĐIỂM (JSON) CALL TỪ STORE
             */
            [CF_ROUTINGS_HISTORY_POINT.ADD_HISTORY_POINT_TRANSFER]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { productID, customerID, typeNandioAdmin, type, currentPoint, flatform } = req.body;
                        
                        const infoAfterInsert = await HISTORY_POINT_MODEL.createTransferPointBy__NandioAdmin({ 
                            productID, customerID, typeNandioAdmin, type, currentPoint, flatform 
                        });
                        
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * ====================== ******************************************* ================================
             * ======================  QUẢN LÝ CẤU HÌNH ĐIỂM MIẾN PHÍ VẬN CHUYỂN  ================================
             * ====================== ******************************************* ================================
             */

            /**
             * Function: Tạo số điểm miễn phí đổi quà (API)
             */
             [CF_ROUTINGS_DELIVERY_POINT_CONFIG.ADD_DELIVERY_POINT_CONFIG]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    view: 'index.ejs',
					title: 'NANDIO',
					code: CF_ROUTINGS_DELIVERY_POINT_CONFIG.ADD_DELIVERY_POINT_CONFIG,
					inc: path.resolve(__dirname, '../views/add_delivery_point_config.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        const infoDeliveryPointConfigAfterInsert = await DELIVERY_POINT_CONFIG_MODEL.getInfo({});

                        ChildRouter.renderToView(req, res, {
                            infoDeliveryPointConfigAfterInsert: infoDeliveryPointConfigAfterInsert.data || null
                        });
                    }],
                    post: [ async function (req, res) {
                        let { _id: userCreate } = req.user;
						const { point, pointReferral } = req.body;
                        const infoDeliveryPointConfigAfterInsert = await DELIVERY_POINT_CONFIG_MODEL.insert({ point, pointReferral, userCreate });
                        res.json(infoDeliveryPointConfigAfterInsert);
                    }]
                },
            },

            /**
             * ====================== ******************************************* ================================
             * ======================           QUẢN LÝ LỊCH SỬ ĐỔI QUÀ           ================================
             * ====================== ******************************************* ================================
             */
             /**
             * Function: Thông tin ORDER GIFT EXCHANGE (API)
             */
            [CF_ROUTINGS_HISTORY_POINT.INFO_ORDER_GIFT_EXCHANGE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: customerID } = req.customer;
                        const { rewards} = req.body;

                        const infoHistoryPoint = await ORDER_GIFT_EXCHANGE_MODEL.getInfoWith__Rewards({ 
                            customer: customerID, rewards 
                        });
                        res.json(infoHistoryPoint);
                    }]
                },
            },

            /**
             * Function: Thông tin ORDER GIFT EXCHANGE (API)
             */
            [CF_ROUTINGS_HISTORY_POINT.CALCULATE_FEE_ORDER_GIFT_EXCHANGE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: customerID } = req.customer;
                        const { rewards, addressID } = req.body;

                        const infoHistoryPoint = await ORDER_GIFT_EXCHANGE_MODEL.calculateFeeBy__Rewards({
                            customer: customerID, rewards, addressID
                        });
                        res.json(infoHistoryPoint);
                    }]
                },
            },

            /**
             * ====================== ******************************************* ================================
             * ======================  QUẢN LÝ CẤU HÌNH NHÓM ĐỐI TƯỢNG NHẬN ĐIỂM  ================================
             * ====================== ******************************************* ================================
             */

            [CF_ROUTINGS_HISTORY_POINT.ADD_CUSTOMER_RECEIVE_POINT_CONFIG]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    view: 'index.ejs',
					title: 'NANDIO',
					code: CF_ROUTINGS_HISTORY_POINT.ADD_CUSTOMER_RECEIVE_POINT_CONFIG,
					inc: path.resolve(__dirname, '../views/add_customer_receive_point_config.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        
                        ChildRouter.renderToView(req, res, {});
                    }],
                    post: [ async function (req, res) {
                        const { name, point } = req.body;

                        const infoCustomerReceivePointConfig = await CUSTOMER_RECEIVE_POINT_CONFIG_MODEL.insert({ name, point });
                        
                        res.json(infoCustomerReceivePointConfig);
                    }]
                },
            },

            [CF_ROUTINGS_HISTORY_POINT.LIST_CUSTOMER_RECEIVE_POINT_CONFIG]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    view: 'index.ejs',
					title: 'NANDIO',
					code: CF_ROUTINGS_HISTORY_POINT.LIST_CUSTOMER_RECEIVE_POINT_CONFIG,
					inc: path.resolve(__dirname, '../views/list_customer_receive_point_config.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        let listCustomerReceivePointConfig = await CUSTOMER_RECEIVE_POINT_CONFIG_MODEL.getCustomerReceivePointConfigs({});

                        ChildRouter.renderToView(req, res, {
                            listCustomerReceivePointConfig: listCustomerReceivePointConfig.data
                        });
                    }],
                },
            },

            [CF_ROUTINGS_HISTORY_POINT.UPDATE_CUSTOMER_RECEIVE_POINT_CONFIG]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    view: 'index.ejs',
					title: 'NANDIO',
					code: CF_ROUTINGS_HISTORY_POINT.UPDATE_CUSTOMER_RECEIVE_POINT_CONFIG,
					inc: path.resolve(__dirname, '../views/update_customer_receive_point_config.ejs')
                },
                methods: {
                    get: [ async function (req, res) {
                        let { customerReceivePointConfigID } = req.params;

                        let infoCustomerReceivePointConfig = await CUSTOMER_RECEIVE_POINT_CONFIG_MODEL.getCustomerReceivePointConfig({  
                            customerReceivePointConfigID
                        });

                        let listMemberOfGroup = await GROUP_MEMBER_POINT_CONFIG_COLL
                            .find({ group: infoCustomerReceivePointConfig.data && infoCustomerReceivePointConfig.data._id })
                            .sort({ _id: -1 })
                            .lean();

                        ChildRouter.renderToView(req, res, {
                            infoCustomerReceivePointConfig: infoCustomerReceivePointConfig.data,
                            listMemberOfGroup
                        });
                    }],
                    post: [ async function (req, res) {
                        let { customerReceivePointConfigID } = req.params;
                        const { name, point, isDefault } = req.body;

                        const infoCustomerReceivePointConfig = await CUSTOMER_RECEIVE_POINT_CONFIG_MODEL.update({ customerReceivePointConfigID, name, point, isDefault });
                        
                        res.json(infoCustomerReceivePointConfig);
                    }]
                },
            },

            [CF_ROUTINGS_HISTORY_POINT.ADD_POINT_FOR_MEMBER_POINT_CONFIG]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function(req, res) {
                        const { _id: customerID } = req.customer;
                        const infoAfterUpdatePoint = await CUSTOMER_RECEIVE_POINT_CONFIG_MODEL.updatePointCustomerRegister({ customerID });
                        res.json(infoAfterUpdatePoint);
                    }]
                },
            },

            [CF_ROUTINGS_HISTORY_POINT.IMPORT_MEMBER_POINT_CONFIG]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ uploadSingle, async function(req, res) {
                        const { group } = req.body;
                        const response = await CUSTOMER_RECEIVE_POINT_CONFIG_MODEL.importMemberPointConfig(req.file, group);
                        res.json(response);
                    }]
                },
            },

            [CF_ROUTINGS_HISTORY_POINT.IMPORT_CUSTOMER_RECEIVE_POINT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ uploadSingle, async function(req, res) {
                        const response = await CUSTOMER_RECEIVE_POINT_CONFIG_MODEL.importCustomerReceivePoint(req.file);
                        res.json(response);
                    }]
                },
            },

        }
    }
};
