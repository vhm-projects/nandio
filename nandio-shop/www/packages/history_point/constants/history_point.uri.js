const BASE_ROUTE = '/history';

const CF_ROUTINGS_HISTORY_POINT = {
	ADD_HISTORY_POINT: `${BASE_ROUTE}/add-history-point`,
	INFO_HISTORY_POINT: `${BASE_ROUTE}/info-history-point`,

	LIST_HISTORY_POINT: `${BASE_ROUTE}/list-history-point`,
	LIST_HISTORY_POINT_BY_CUSTOMER: `${BASE_ROUTE}/list-history-point-by-customer`,
	LIST_HISTORY_POINT_OF_CUSTOMER_BY_TYPE: `${BASE_ROUTE}/list-history-point-of-customer-by-type`,
	API_LIST_HISTORY_POINT: `/api${BASE_ROUTE}/list-history-point`,
	API_LIST_REWARD_OF_YOU: `/api${BASE_ROUTE}/list-reward-of-you`,
	API_LIST_REWARD_OF_LIST_CUSTOMER: `/api${BASE_ROUTE}/list-reward-of-list-customer`,

	// LỊCH SỬ ĐỔI QUÀ
	INFO_ORDER_GIFT_EXCHANGE:          `/order-gift-exchange/info-order-gift-exchange`,
	CALCULATE_FEE_ORDER_GIFT_EXCHANGE: `/order-gift-exchange/calculate-fee`,

	// CẤU HÌNH NHÓM ĐỐI TƯỢNG NHẬN ĐIỂM
	ADD_CUSTOMER_RECEIVE_POINT_CONFIG:     `/customer-receive-point-config/add-customer-receive-point-config`,
	LIST_CUSTOMER_RECEIVE_POINT_CONFIG:    `/customer-receive-point-config/list-customer-receive-point-config`,
	UPDATE_CUSTOMER_RECEIVE_POINT_CONFIG:  `/customer-receive-point-config/update-customer-receive-point-config/:customerReceivePointConfigID`,
	ADD_POINT_FOR_MEMBER_POINT_CONFIG:  `/api/add-point-member-in-group`,
	IMPORT_MEMBER_POINT_CONFIG:  `/api/import-member-point-config`,
	IMPORT_CUSTOMER_RECEIVE_POINT:  `/api/import-customer-receive-point`,

	ADD_INFO_TRANSFER_POINT:  `/api${BASE_ROUTE}/add-info-transfer-point`,
	API_LIST_POINT_TRANSFER:  `/api${BASE_ROUTE}/list-transfer-point`,

	// LÍCH SỬ ĐIỂM CỦA TÀI KHOẢN GLOQ
	LIST_HISTORY_POINT_GLOQ:    `/history-point/list-history-point-gloq`,
	ADD_HISTORY_POINT_BY_GLOQ:  `/history-point/add-history-point-by-gloq`,
	ADD_HISTORY_POINT_TRANSFER_BY_GLOQ:  `/history-point/add-history-point-transfer-by-gloq`,

	// LỊCH SỬ BIẾN ĐỘNG NHẬN/CHUYỂN ĐIỂM
	LIST_HISTORY_POINT_TRANSFER: `${BASE_ROUTE}/list-history-point-transfer`,

	ADD_HISTORY_POINT_TRANSFER: `${BASE_ROUTE}/add-history-point-transfer`,

    ORIGIN_APP: BASE_ROUTE
}

const BASE_ROUTE_DELIVERY_POINT_CONFIG = '/delivery-point-config';

const CF_ROUTINGS_DELIVERY_POINT_CONFIG = {
	// ADD_DELIVERY_POINT_CONFIG: `${BASE_ROUTE_DELIVERY_POINT_CONFIG}/add-delivery-point-config`,
	ADD_DELIVERY_POINT_CONFIG: `${BASE_ROUTE_DELIVERY_POINT_CONFIG}`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_HISTORY_POINT = CF_ROUTINGS_HISTORY_POINT;
exports.CF_ROUTINGS_DELIVERY_POINT_CONFIG = CF_ROUTINGS_DELIVERY_POINT_CONFIG;
