/**
 * Loại lịch sử:
 * 1: Tích điểm
 * 2: Đổi điểm
 * 3: Đăng ký tài khoản Nhận điểm
 * 4: Nhận điểm chuyển tới
 * 5: Chuyển điểm
 * 6: Member get member (mã giới thiệu)
 */
export const TYPE_HISTORY_POINT = [1,2,3,4,5,6];