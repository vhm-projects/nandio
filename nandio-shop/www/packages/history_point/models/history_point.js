/**
 * EXTERNAL PACKAGE
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                      	= require('moment');
const request                      	= require('request');
const ISOdate                      	= require('isodate');

/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs } 			= require('../../../utils/utils');
const { HISTORY_POINT_TYPE }        = require('../../../config/cf_constants')
/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');

const NOTIFICATION_MODEL            = require('../../notification/models/notification').MODEL;

class Model extends BaseModel {
    constructor() {
        super(require('../databases/history_point-coll'));

		/**
		 * Loại lịch sử:
		 * 1: Tích điểm
		 * 2: Đổi điểm
		 * 3: Đăng ký tài khoản Nhận điểm
		 * 4: Nhận điểm chuyển tới
		 * 5: Chuyển điểm
		 * 6: Member get member (mã giới thiệu)
		 * 7: GLOQ nạp điểm
		 */
		this.TYPE_ACCUMULATE_POINT 			= 1;
		this.TYPE_EXCHANGE_POINT   			= 2;
		this.TYPE_REGISTER_RECEIVE_POINT	= 3; 
		this.TYPE_RECEIVE_POINT 			= 4;
		this.TYPE_TRANSFER_POINT 			= 5;
		this.TYPE_MEMBER_GET_MEMBER 		= 6;
		this.TYPE_GLOQ_ADD_POINT     		= 7;

		this.ARR_TYPE_VALID 			 = [1,2,3,4,5,6,7];
		this.ARR_TYPE_RECEIVE_POINT 	 = [1,3,4,6,7];
		this.ARR_TYPE_TRANSFER_POINT 	 = [2,5];
		this.ARR_TYPE_MINUS_POINT_NANDIO = [1,2,3,6];
		this.ARR_TYPE_MINUS_POINT        = [1,2,3,5,6]; //TYPE SẼ TRỪ ĐIỂM SENDER

		// DEFINE CÁC THAM SỐ ĐỒNG BỘ AMS
		this.TYPE_SYNC_AMS_ACCUMULATE_POINT = 1;
		this.TYPE_SYNC_AMS_EXCHANGE_POINT   = 2;
    }
	
	insert({ productID, customerID, type, currentPoint, flatform }) {
        return new Promise(async resolve => {
            try {
				if(!type || !currentPoint)
					return resolve({ error: true, message: 'Cần nhập đầy đủ điểm hoặc loại lịch sử' });

				/**
				 * convert number
				 */
				type = +type;
				currentPoint = +currentPoint;

				let dataInsert = { 
					customer: customerID, type, currentPoint,
				};

				if (flatform) {
					dataInsert.flatform = flatform;
				} else {
					dataInsert.flatform = 'SHOP';
				}

				if (!checkObjectIDs(customerID))
                    return resolve({ error: true, message: 'ID Khách hàng không hợp lệ' });

                if (isNaN(type) || !this.ARR_TYPE_VALID.includes(type)) 
                    return resolve({ error: true, message: 'Loại lịch sử điểm không hợp lệ' });

				if (isNaN(currentPoint) || currentPoint < 0) 
					return resolve({ error: true, message: 'Điểm được cộng/trừ không đúng định dạng' });

				if (productID) {
					if(!checkObjectIDs(productID))
						return resolve({ error: true, message: 'ID Sản phẩm không hợp lệ' });

					dataInsert.product = productID;
				}

				let infoCustomer = await CUSTOMER_COLL.findById(customerID);
				if(!infoCustomer)
					return resolve({ error: true, message: 'Khách hàng không tồn tại' });

				let pointCustomer        = infoCustomer.point        ? infoCustomer.point        : 0;

				let screen_key = ''; // DÚNG GỬI CLOUD MESSAGING
				if (this.ARR_TYPE_TRANSFER_POINT.includes(type)) {
					if (pointCustomer < currentPoint)
						return resolve({ error: true, message: 'Bạn không đủ số điểm để đổi quà' });

					dataInsert = {
						...dataInsert,
						beforPoint:   pointCustomer,                        // SỐ ĐIỂM TRƯỚC KHI ĐỔI
						currentPoint: currentPoint,                         // SỐ ĐIỂM ĐỔI
						point:        pointCustomer - Number(currentPoint), // SỐ ĐIỂM SAU KHI ĐỔI
					}

					screen_key = 'HistorySavePointScreen';
				} 

				if (this.ARR_TYPE_RECEIVE_POINT.includes(type)) {
					dataInsert = {
						...dataInsert,
						beforPoint:   pointCustomer,                        // SỐ ĐIỂM TRƯỚC KHI TÍCH
						currentPoint: currentPoint,                         // SỐ ĐIỂM TÍCH
						point:        pointCustomer + Number(currentPoint), // SỐ ĐIỂM SAU KHI TÍCH
					}
					screen_key = 'HistorySaveAccumulatePointScreen';
				}

				// KIỂM TRA ĐIỂM CỦA NANDIO ADMIN
				// if (this.ARR_TYPE_MINUS_POINT_NANDIO.includes(type)) {
				// 	const KIND_WALLET_NANDIO = 2;
				// 	let infoWalletNandio = await CUSTOMER_COLL.findOne({ kind: KIND_WALLET_NANDIO });
				// 	if (!infoWalletNandio)
                //     	return resolve({ error: true, message: 'Ví của NANDIO không tồn tại, Bạn phải tạo Ví của NANDIO' });
					
				// 	if (infoWalletNandio.point < currentPoint)
                //     	return resolve({ error: true, message: 'Ví của NANDIO không đủ điểm để đổi' });
				// }
				
                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_history_point_failed' });

				let title = '';
				HISTORY_POINT_TYPE.forEach(type_history_point => {
					if (type_history_point.value == type) {
						title = type_history_point.text;
					}
				});
				console.log({
					title, screen_key
				});
				const TYPE_HISTORY_POINT = 5;
				let infoNotificationAFterInsert = await NOTIFICATION_MODEL.insertV2({ 
					listNotification: [{
						title:       title, 
						description: `Bạn ${title} thành công`, 
						type:        TYPE_HISTORY_POINT, 
						receive:     customerID,
						// centralize_order: infoCentralizeOrderAfterUpdate._id,
						history_point: infoAfterInsert._id,
						createAt:    new Date(),
						modifyAt:    new Date(),
						screen_key
					}]
				})
				// ===============CẬP NHẬT ĐIỂM CHO CUSTOMER============== //
				// Tích điểm
				if(this.ARR_TYPE_RECEIVE_POINT.includes(type)){
					let conditionReceivePoint = {};
					if (this.TYPE_ACCUMULATE_POINT == type) { // ADD POINT RANKING CHỈ CÓ TÍCH ĐIỂM
						conditionReceivePoint = {
							$inc: { 
								point:        +currentPoint, 
								pointRanking: +currentPoint 
							}
						};
					} else {
						conditionReceivePoint = {
							$inc: { 
								point:        +currentPoint, 
								// pointRanking: +currentPoint 
							}
						};
					}
					
					await CUSTOMER_COLL.updateOne({ _id: customerID }, conditionReceivePoint);
					// await CUSTOMER_COLL.findOneAndUpdate({ _id: customerID }, {
					// 	$inc: { 
					// 		point:        currentPoint, 
					// 		pointRanking: currentPoint
					// 	}
					// });

    				if (process.env.NODE_ENV == 'production') {
						// =================================================SYNS AMS(tích điểm)========================================//
						let options = {
							'method': 'POST',
							// 'url': `http://dms1.yensaothienviet.vn:8680/thsams/web/index.php?r=api%2Fcapnhatdiemkh&p1=crm&p2=123456&p3=4PBagn81jWU3ouPVt4OeqzDeYrU7iYH9&dtdd=0937362026&diemtichluy=${point}&diemdoiqua=1&tran_type=1&diemhientai=${pointCustomer}&id_order=1`,
							'url': `http://dms1.yensaothienviet.vn:8680/thsams/web/index.php?r=api%2Fcapnhatdiemkh&p1=crm&p2=123456&p3=4PBagn81jWU3ouPVt4OeqzDeYrU7iYH9&dtdd=${infoCustomer.phone}&diemtichluy=${currentPoint}&diemdoiqua=0&tran_type=${this.TYPE_SYNC_AMS_ACCUMULATE_POINT}&diemhientai=${pointCustomer}&id_order=1`,
						};
                        console.log("🚀 ~ file: history_point.js ~ line 128 ~ Model ~ insert ~ options", options)
						request(options, function (error, response, body) {
							if (error) 
								return console.log("🚀 ~ file: history_point.js ~ line 135 ~ Model ~ error", error)
							// console.log("🚀 ~ file: history_point.js ~ line 134 ~ Model ~ body", body)
						});
					}
				}

				// Đổi điểm
				if(this.ARR_TYPE_TRANSFER_POINT.includes(type)){
					await CUSTOMER_COLL.findOneAndUpdate({ _id: customerID }, {
						$inc: { point: -currentPoint }
					});

    				if (process.env.NODE_ENV == 'production') {
						// =================================================SYNS AMS(đổi điểm)========================================//
						let options = {
							'method': 'POST',
							'url': `http://dms1.yensaothienviet.vn:8680/thsams/web/index.php?r=api%2Fcapnhatdiemkh&p1=crm&p2=123456&p3=4PBagn81jWU3ouPVt4OeqzDeYrU7iYH9&dtdd=${infoCustomer.phone}&diemtichluy=0&diemdoiqua=${currentPoint}&tran_type=${this.TYPE_SYNC_AMS_EXCHANGE_POINT}&diemhientai=${pointCustomer}&id_order=1`,
						};
                        console.log("🚀 ~ file: history_point.js ~ line 147 ~ Model ~ insert ~ options", options)
						request(options, function (error, response, body) {
							if (error) 
								return console.log("🚀 ~ file: history_point.js ~ line 135 ~ Model ~ error", error)
							console.log("🚀 ~ file: history_point.js ~ line 134 ~ Model ~ body", body)
						});
					}
				}

				// if (this.ARR_TYPE_MINUS_POINT_NANDIO.includes(type)) {
				// 	const KIND_WALLET_NANDIO = 2;
				// 	let infoWalletNandioAfterUpdate = await CUSTOMER_COLL.updateOne({ kind: KIND_WALLET_NANDIO }, {
				// 		$inc: { point: -currentPoint }
				// 	});
				// 	console.log({
				// 		infoWalletNandioAfterUpdate
				// 	});
				// }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error });
            }
        })
    }

    getInfo({ historyPointID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(historyPointID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoHistoryPoint = await HISTORY_POINT_COLL
					.findById(historyPointID)
					// .select("point reward createAt buyType transaction amount")
					.populate({
						path: "customer",
					})
					.populate({
						path: "point_transfer",
						populate: {
							path: 'receiver sender'
						}
					})
					.populate({
						path: "product",
					})
					.lean();

                if(!infoHistoryPoint)
                    return resolve({ error: true, message: 'cannot_get_history_point' });

                return resolve({ error: false, data: infoHistoryPoint });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListByCustomer({ customerID, fromDate, toDate, type, typePoint, page = 1, limit = 30, sort = {} }){
		return new Promise(async resolve => {
			try {
				page  = +page;
				limit = +limit;
				type && (type = +type);

				let sortBy = { createAt: -1 };
				let conditionObj = { customer: customerID };

				if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });

				if(!page || !limit)
					return resolve({ error: true, message: 'param_invalid' });

				if(typeof sort === 'string'){
					sort = JSON.parse(sort);
					sortBy = { ...sort, ...sortBy };
				}

				if(!type && !typePoint){
					conditionObj.type = { $in: this.ARR_TYPE_VALID };
				}

				if(typePoint === 'receive'){
					conditionObj.type = { $in: this.ARR_TYPE_RECEIVE_POINT };
				}

				if(typePoint === 'transfer'){
					conditionObj.type = { $in: [this.TYPE_TRANSFER_POINT] };
				}

				if(this.ARR_TYPE_VALID.includes(type)){
					conditionObj.type = type;
				}

				if(fromDate && toDate){
					conditionObj.createAt = {
                        $gte: new Date(moment(fromDate).startOf('day').format()),
                        $lte: new Date(moment(toDate).endOf('day').format()),
                    }
				}

				let listHistoryPoint = await HISTORY_POINT_COLL
					.find(conditionObj)
					.populate({
						path: 'product',
						select: 'name SKU'
					})
					.populate({
						path: 'customer',
						select: '_id fullname phone point pointRanking'
					})
					.populate({
						path: 'point_transfer',
						select: 'sender receiver point type',
						populate: {
							path: 'sender receiver',
							select: 'fullname'
						}
					})
					.sort(sortBy)
					.limit(limit)
					.skip((page - 1) * limit)
					.lean();

				if(!listHistoryPoint)
					return resolve({ error: true, message: 'cannot_get_list_history_point' });

				let totalHistoryPoint = await HISTORY_POINT_COLL.count(conditionObj);
				let pages = Math.ceil(totalHistoryPoint / limit);


				if (typePoint === 'transfer') {
					let listOrderGiftExchange = [];
					if (type == this.TYPE_EXCHANGE_POINT || !type) {
						let listOrderGiftExchangeAfterCheck = await ORDER_GIFT_EXCHANGE_MODEL.getOrderGiftExchangesBy__Rewards({ 
							customer: customerID, fromDate, toDate, page, limit
						});
						listOrderGiftExchange = listOrderGiftExchangeAfterCheck.data.listOrderGiftExchange
					}

					return resolve({
						error: false,
						data: {
							listHistoryPoint,
							currentPage: +page,
							perPage: limit,
							totalPage: +pages,
							listOrderGiftExchange: listOrderGiftExchange
						},
					});
				} else {
					return resolve({ 
						error: false,
						data: {
							listHistoryPoint,
							currentPage: +page,
							perPage: limit,
							totalPage: +pages
						}
					});
				}
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

	getListWithPaging({ type, page = 1, limit = 30 }){
		return new Promise(async resolve => {
			try {
				page  = +page;
				limit = +limit;
				type  = +type;

				if(!page || !limit)
					return resolve({ error: true, message: 'param_invalid' });

				let conditionObj = {};
				if(this.ARR_TYPE_VALID.includes(type)){
					conditionObj.type = type;
				} else{
					conditionObj.type = { $in: this.ARR_TYPE_VALID };
				}

				let listHistoryPoint = await HISTORY_POINT_COLL
					.find(conditionObj)
					.populate('product customer transaction buyType amount')
					.sort({ createAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
					.lean();

                if(!listHistoryPoint)
                    return resolve({ error: true, message: 'cannot_get_list_history_point' });

				let totalHistoryPoint = await HISTORY_POINT_COLL.countDocuments(conditionObj);

				return resolve({ 
					error: false, 
					data: {
						listHistoryPoint,
						currentPage: +page,
						totalPage: +totalHistoryPoint
					}
				});
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

	getList({ type }){
		return new Promise(async resolve => {
			try {

				let conditionObj = {};
				if(this.ARR_TYPE_VALID.includes(type)){
					conditionObj.type = type;
				} else{
					conditionObj.type = { $in: this.ARR_TYPE_VALID };
				}

				console.log({
					conditionObj
				});
				let listHistoryPoint = await HISTORY_POINT_COLL
					.find(conditionObj)
					.populate('reward customer order buyType')
					.sort({ createAt: -1 })
					.lean();
                if(!listHistoryPoint)
                    return resolve({ error: true, message: 'cannot_get_list_history_point' });

				return resolve({ error: false, data: listHistoryPoint });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

	getListHistoryPoint__GLOQ({ type, customer }){
		return new Promise(async resolve => {
			try {

				let conditionObj = {};
				if(this.ARR_TYPE_VALID.includes(type)){
					conditionObj.type = type;
				} 

				if (customer) {
					if(!checkObjectIDs(customer))
                    	return resolve({ error: true, message: 'params_invalid' });

					conditionObj.customer = customer;
				}

				console.log({
					conditionObj
				});
				let listHistoryPoint = await HISTORY_POINT_COLL
					.find(conditionObj)
					.populate('customer')
					.sort({ createAt: -1 })
					.lean();
                if(!listHistoryPoint)
                    return resolve({ error: true, message: 'cannot_get_list_history_point' });

				return resolve({ error: false, data: listHistoryPoint });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

	getListRewardOfYou({ customerID, page = 1, limit = 3 }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(customerID))
					return resolve({ error: true, message: 'params_invalid' });

				let listReward = await HISTORY_POINT_COLL
					.find({ customer: customerID, reward: { $exists: true }})  
					.populate({
						path: "reward",
						select: "amount gift",
						populate: {
							path : 'gift.item',
							select: "brand name avatar",
							populate: {
								path: "brand avatar",
								select: "name path",
							}
						}
					})
					.sort({ createAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
					.lean();

                if(!listReward)
                    return resolve({ error: true, message: 'cannot_get_list_history_point' });

				let totalHistoryPoint = await HISTORY_POINT_COLL.countDocuments({ customer: customerID, reward: { $exists: true }});

				return resolve({ 
					error: false, 
					data: {
						listReward,
						currentPage: +page,
						totalPage: +totalHistoryPoint
					}
				});
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

	createInfoTransferPoint({ senderID, receiverID, content, point, lnt, lat, pinCode }){
		return new Promise(async (resolve) => {
			try {
				let dataInsert = {
					sender: senderID,
					receiver: receiverID,
					point,
					content,
					createAt: new Date()
				};

				if(!checkObjectIDs(senderID))
					return resolve({ error: true, message: 'params_senderID_invalid', status: 400 });

				if(!checkObjectIDs(receiverID))
					return resolve({ error: true, message: 'params_receiverID_invalid', status: 400 });

				if(isNaN(point) || point < 0)
					return resolve({ error: true, message: 'params_point_invalid', status: 400 });

				// const verifyPinCode = await CUSTOMER_MODEL.verifyPinCode({ customerID: senderID, pinCode });
				// if(verifyPinCode.error)
				// 	return resolve(verifyPinCode);

				if(lnt && lat){
					dataInsert.location = {
						type: "point",
						coordinates: [lnt, lat]
					}
				}

				let infoCustomer = await CUSTOMER_COLL.findById(senderID);
				if(!infoCustomer)
					return resolve({ error: true, message: 'Khách hàng không tồn tại' });

				let pointCustomer = infoCustomer.point || 0;

				if (pointCustomer < point)
					return resolve({ error: true, message: 'Bạn không đủ số điểm để chuyển' });

				const infoAfterInsert = await POINT_TRANSFER_COLL.create(dataInsert);
				if(!infoAfterInsert)
					return resolve({ error: true, message: 'create_info_transfer_point_failed', status: 422 });

				let listHistoryPointID = [];
				// Tạo record cho người chuyển điểm
				const infoAfterTransfer = await this.insert({
					customerID: senderID,
					currentPoint: point,
					type: this.TYPE_TRANSFER_POINT
				});
				console.log({
					infoAfterTransfer
				});
				// Tạo record cho người nhận điểm
				if(!infoAfterTransfer.error){
					// await this.insert({
					// 	customerID: receiverID,
					// 	currentPoint: point,
					// 	type: this.TYPE_RECEIVE_POINT
					// });
					let infoAfterHistoryPointReceive = await this.insert({
						customerID: receiverID,
						currentPoint: point,
						type: this.TYPE_RECEIVE_POINT
					});

					listHistoryPointID = [
						infoAfterTransfer.data._id,
						infoAfterHistoryPointReceive.data._id
					];
				}

				let listHistoryPointAfterUpdate = await HISTORY_POINT_COLL.updateMany({ 
					_id: {
						$in: listHistoryPointID
					}
				}, {
					point_transfer: infoAfterInsert._id
				});
				console.log({
					listHistoryPointAfterUpdate
				});
				
				return resolve({ error: false, data: infoAfterInsert, status: 200 });
			} catch (error) {
				return resolve({ error: true, message: error.message, status: 500 });
			}
		})
	}

	createTransferPointDynamicType({ senderID, receiverID, typeSender, typeReceiver, content, point, lnt, lat, pinCode, productID, flatform }){
		return new Promise(async (resolve) => {
			try {
				let dataInsert = {
					sender: senderID,
					receiver: receiverID,
					point,
					content,
					createAt: new Date(),
					type: typeReceiver
				};

				if(!checkObjectIDs(senderID))
					return resolve({ error: true, message: 'params_senderID_invalid', status: 400 });

				if(!checkObjectIDs(receiverID))
					return resolve({ error: true, message: 'params_receiverID_invalid', status: 400 });

				if(isNaN(point) || point < 0)
					return resolve({ error: true, message: 'params_point_invalid', status: 400 });

				// const verifyPinCode = await CUSTOMER_MODEL.verifyPinCode({ customerID: senderID, pinCode });
				// if(verifyPinCode.error)
				// 	return resolve(verifyPinCode);

				if(lnt && lat){
					dataInsert.location = {
						type: "point",
						coordinates: [lnt, lat]
					}
				}

				let infoCustomer = await CUSTOMER_COLL.findById(senderID);
				if(!infoCustomer)
					return resolve({ error: true, message: 'Khách hàng không tồn tại' });

				let pointCustomer = infoCustomer.point || 0;

				if (Number.isNaN(Number(typeSender)) || !this.ARR_TYPE_VALID.includes(Number(typeSender))) {
					return resolve({ error: true, message: 'Loại người gửi không hợp lệ' });
				}

				if (Number.isNaN(Number(typeReceiver)) || !this.ARR_TYPE_VALID.includes(Number(typeReceiver))) {
					return resolve({ error: true, message: 'Loại người nhận không hợp lệ' });
				}

				if (this.ARR_TYPE_MINUS_POINT.includes(Number(typeSender))) {
					if (pointCustomer < point)
						return resolve({ error: true, message: 'Bạn không đủ số điểm để chuyển' });
				}

				let listHistoryPointID = [];
				// Tạo record cho người chuyển điểm
				const infoAfterTransfer = await this.insert({
					customerID: senderID,
					currentPoint: point,
					type: typeSender,
					flatform
				});
				
				// Tạo record cho người nhận điểm
				if(!infoAfterTransfer.error){
					let conditionObjInsertHistoryPointBy__Receiver = {
						customerID: receiverID,
						currentPoint: point,
						type: typeReceiver,
					};
					productID && (conditionObjInsertHistoryPointBy__Receiver.productID = productID);
					flatform  && (conditionObjInsertHistoryPointBy__Receiver.flatform  = flatform);
					let infoAfterHistoryPointReceive = await this.insert(conditionObjInsertHistoryPointBy__Receiver);
					listHistoryPointID = [
						infoAfterTransfer.data._id,
						infoAfterHistoryPointReceive.data._id
					];
				} else {
					return resolve(infoAfterTransfer);
				}

				const infoAfterInsert = await POINT_TRANSFER_COLL.create(dataInsert);
				if(!infoAfterInsert)
					return resolve({ error: true, message: 'create_info_transfer_point_failed', status: 422 });

				let listHistoryPointAfterUpdate = await HISTORY_POINT_COLL.updateMany({ 
					_id: {
						$in: listHistoryPointID
					}
				}, {
					point_transfer: infoAfterInsert._id
				});
				console.log({
					listHistoryPointAfterUpdate
				});

				return resolve({ error: false, data: infoAfterInsert, status: 200 });
			} catch (error) {
				return resolve({ error: true, message: error.message, status: 500 });
			}
		})
	}

	createTransferPointBy__NandioAdmin({ productID, customerID, typeNandioAdmin, type, currentPoint, flatform }){
		return new Promise(async (resolve) => {
			try {
				let dataInsert = {
					point: currentPoint,
				};
				productID && (dataInsert.productID = productID);
				flatform  && (dataInsert.flatform = flatform);

				const KIND_WALLET_NANDIO = 2;
				let infoWalletNandio = await CUSTOMER_COLL.findOne({ kind: KIND_WALLET_NANDIO });
				if (!infoWalletNandio)
					return resolve({ error: true, message: 'Ví của NANDIO không tồn tại, Bạn phải tạo Ví của NANDIO' });

				if (typeNandioAdmin == this.TYPE_TRANSFER_POINT) {
					if (infoWalletNandio.point < currentPoint)
						return resolve({ error: true, message: 'Ví của NANDIO không đủ điểm để đổi' });

					dataInsert = {
						...dataInsert,
						senderID:     infoWalletNandio._id,
						receiverID:   customerID,
						typeSender:   typeNandioAdmin,
						typeReceiver: type,
					}
				} else if (typeNandioAdmin == this.TYPE_RECEIVE_POINT) {
					dataInsert = {
						...dataInsert,
						senderID:     customerID,
						receiverID:   infoWalletNandio._id,
						typeSender:   type,
						typeReceiver: typeNandioAdmin,
					}
				} else {
					return resolve({ error: true, message: 'Loại không hợp lệ' });
				}

				// const verifyPinCode = await CUSTOMER_MODEL.verifyPinCode({ customerID: senderID, pinCode });
				// if(verifyPinCode.error)
				// 	return resolve(verifyPinCode);
				console.log({
					dataInsert
				});
				let infoAfterInsert = await this.createTransferPointDynamicType(dataInsert);
				return resolve(infoAfterInsert);
			} catch (error) {
				return resolve({ error: true, message: error.message, status: 500 });
			}
		})
	}

	getListTransferPoint({ customerID, page = 1, limit = 10 }){
		return new Promise(async resolve => {
			try {
				page  = +page;
				limit = +limit;

				if(!page || !limit)
					return resolve({ error: true, message: 'param_invalid' });

				if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_customerID_invalid' });

				let KIND_WALLET_NANDIO = 2;
				let infoWalletNandio = await CUSTOMER_COLL.findOne({ kind: KIND_WALLET_NANDIO });
				if (!infoWalletNandio)
					return resolve({ error: true, message: 'Ví của NANDIO không tồn tại, Bạn phải tạo Ví của NANDIO' });

				let conditionObj = {
					$or: [
						{ sender: customerID },
						{ receiver: customerID },
					],
					sender: {
						$nin: [infoWalletNandio._id]
					},
					receiver: {
						$nin: [infoWalletNandio._id]
					}
				};

				let listTransferPoint = await POINT_TRANSFER_COLL
					.find(conditionObj)
					.populate({
						path: 'sender receiver',
						select: '_id fullname phone avatar',
						populate: {
							path: 'avatar',
							select: 'path'
						}
					})
					.sort({ createAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
					.lean();

                if(!listTransferPoint)
                    return resolve({ error: true, message: 'cannot_get_list_transfer_point' });

				let totalRecord = await POINT_TRANSFER_COLL.countDocuments(conditionObj);

				return resolve({ 
					error: false, 
					data: {
						listTransferPoint,
						currentPage: +page,
						totalRecord: +totalRecord
					}
				});
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

	getListTransfer({  }) {
		return new Promise(async resolve => {
			try {
				let conditionObj = {};
				
				let listHistoryPoint = await POINT_TRANSFER_COLL
					.find(conditionObj)
					.populate('sender receiver')
					.sort({ createAt: -1 })
					.limit(1000)
					.lean();

                if(!listHistoryPoint)
                    return resolve({ error: true, message: 'cannot_get_list_history_point' });

				return resolve({ error: false, data: listHistoryPoint });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

}

module.exports.MODEL = new Model;

/**
 * COLLECTIONS
 */
var HISTORY_POINT_COLL  			= require('../databases/history_point-coll');
var POINT_TRANSFER_COLL 			= require('../databases/point_transfer-coll');
var CUSTOMER_COLL 					= require('../../customer/databases/customer-coll');
 
/**
 * MODELS
 */
var ORDER_GIFT_EXCHANGE_MODEL		= require('./order_gift_exchange').MODEL;