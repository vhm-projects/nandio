/**
 * EXTERNAL PACKAGE
 */
const XlsxPopulate                  = require('xlsx-populate');
const fs                            = require('fs');

/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs } 			= require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');


class Model extends BaseModel {
    constructor() {
        super(require('../databases/customer_receive_point_config-coll'));
    }
	
	insert({ name, point }) {
        return new Promise(async resolve => {
            try {
				
				if (!name) {
					return resolve({ error: true, message: 'Tên nhóm đối tượng nhận điểm không hợp lệ' });
				}

				if (Number.isNaN(Number(point)) || !Number(point) < 0) {
					return resolve({ error: true, message: 'Số điểm nhận không hợp lệ' });
				}

				let dataInsert = { 
					name, point
				};

				console.log({
					dataInsert
				});
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_customer_receive_point_config_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getCustomerReceivePointConfigs({  }) {
        return new Promise(async resolve => {
            try {

				let listCustomerReceivePointConfig = await CUSTOMER_RECEIVE_POINT_CONFIG_COLL.find({});
                if(!listCustomerReceivePointConfig)
                    return resolve({ error: true, message: 'cannot_get_list_customer_receive_point_config_failed' });

                return resolve({ error: false, data: listCustomerReceivePointConfig });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getCustomerReceivePointConfig({ customerReceivePointConfigID }) {
        return new Promise(async resolve => {
            try {
				if (!checkObjectIDs(customerReceivePointConfigID)) {
                    return resolve({ error: true, message: 'Cấu hình nhóm đối tượng ID không hợp lệ' });
				}

				let infoCustomerReceivePointConfig = await CUSTOMER_RECEIVE_POINT_CONFIG_COLL.findById(customerReceivePointConfigID);
                if(!infoCustomerReceivePointConfig)
                    return resolve({ error: true, message: 'cannot_get_info_customer_receive_point_config_failed' });

                return resolve({ error: false, data: infoCustomerReceivePointConfig });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	update({ customerReceivePointConfigID, name, point, isDefault }) {
        return new Promise(async resolve => {
            try {
				if (!checkObjectIDs(customerReceivePointConfigID)) {
                    return resolve({ error: true, message: 'Cấu hình nhóm đối tượng ID không hợp lệ' });
				}

				if (!name) {
					return resolve({ error: true, message: 'Tên nhóm đối tượng nhận điểm không hợp lệ' });
				}

				if (Number.isNaN(Number(point)) || !Number(point) < 0) {
					return resolve({ error: true, message: 'Số điểm nhận không hợp lệ' });
				}

                if(isDefault){
                    const isHaveDefaultGroup = await CUSTOMER_RECEIVE_POINT_CONFIG_COLL.findOne({ isDefault: true }).lean();
                    
                    if(isHaveDefaultGroup){
					    return resolve({ error: true, message: 'Đã tồn tại nhóm mặc định' });
                    }
                }

				let dataUpdate = { 
					name, point, modifyAt: new Date(), isDefault: isDefault === 'true'
				};

				let infoCustomerReceivePointConfigAfterUpdate = await CUSTOMER_RECEIVE_POINT_CONFIG_COLL.findByIdAndUpdate(customerReceivePointConfigID, dataUpdate, { new: true });

                if(!infoCustomerReceivePointConfigAfterUpdate)
                    return resolve({ error: true, message: 'update_customer_receive_point_config_failed' });

                return resolve({ error: false, data: infoCustomerReceivePointConfigAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updatePointCustomerRegister({ customerID }) {
        return new Promise(async resolve => {
            try {
				if (!checkObjectIDs(customerID)) {
					return resolve({ error: true, message: 'ID khách hàng không hợp lệ' });
				}

                const infoCustomer = await CUSTOMER_COLL.findOne({ _id: customerID, isReceivedPointRegister: 1 }).lean();
                if(!infoCustomer){
                    return resolve({ error: true, message: 'can_not_get_info_customer' });
                }

                let infoMemberPointConfig = await GROUP_MEMBER_POINT_CONFIG_COLL.findOne({ phone: infoCustomer.phone }).lean();
                let infoGroup = null;

                if(infoMemberPointConfig){
                    infoGroup = await CUSTOMER_RECEIVE_POINT_CONFIG_COLL.findById(infoMemberPointConfig.group);
                } else{
                    infoGroup = await CUSTOMER_RECEIVE_POINT_CONFIG_COLL.findOne({ isDefault: true });
                }

                if(!infoGroup)
                    return resolve({ error: true, message: 'can_not_get_info_group_have_customer' });

                // const infoCustomer = await CUSTOMER_COLL.findOne({ phone, isReceivedPointRegister: 1 }).lean();

                // if(!infoCustomer){
                //     return resolve({ error: true, message: 'can_not_get_info_customer' });
                // }

                // TRỪ ĐIỂM CỦA NANDIO ADMIN
                const infoAfterUpdate = await HISTORY_POINT_MODEL.createTransferPointBy__NandioAdmin({ 
                    customerID:      infoCustomer._id, 
                    typeNandioAdmin: HISTORY_POINT_MODEL.TYPE_TRANSFER_POINT, 
                    type:            HISTORY_POINT_MODEL.TYPE_REGISTER_RECEIVE_POINT, 
                    currentPoint:    infoGroup.point 
                });
                // .insert({
                //     customerID: infoCustomer._id,
                //     currentPoint: infoGroup.point,
                //     type: 3,
                // })

                if (infoAfterUpdate.error) {
                    return resolve(infoAfterUpdate);
                }

                await CUSTOMER_COLL.findByIdAndUpdate(infoCustomer._id, { isReceivedPointRegister: 2 });

                return resolve(infoAfterUpdate);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    importMemberPointConfig(fileInfo, groupID) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(groupID)) {
                    return resolve({ error: true, message: 'Nhóm đối tượng ID không hợp lệ' });
				}

                XlsxPopulate.fromFileAsync(fileInfo.path)
                    .then(async workbook => {
                        let index = 2;
                        let listMemberPointConfig = [];
                        let listPhoneExists = [];
                        let listExists = [];

                        for (;true;) {
                            let phone      = workbook.sheet(0).cell(`A${index}`).value();
                            let fullname   = workbook.sheet(0).cell(`B${index}`).value();

                            if(!phone || !fullname) break;

                            phone    = phone.toString().trim();
                            fullname = fullname.toString().trim();

                            const infoMemberPointConfig = await GROUP_MEMBER_POINT_CONFIG_COLL
                                .findOne({ phone })
                                .lean();

                            if(!infoMemberPointConfig && !listPhoneExists.includes(phone)){
                                listPhoneExists[listPhoneExists.length] = phone;
                                listMemberPointConfig[listMemberPointConfig.length] = {
                                    phone,
                                    fullname,
                                    group: groupID,
                                    createAt: new Date()
                                }
                            }

                            if(listPhoneExists.includes(phone) || infoMemberPointConfig) {
                                listExists[listExists.length] = {
                                    phone, fullname
                                }
                            }

                            index++;
                        }

                        await fs.unlinkSync(fileInfo.path);

                        if(listMemberPointConfig.length){
                            await GROUP_MEMBER_POINT_CONFIG_COLL.insertMany(listMemberPointConfig);
                        } else{
                            return resolve({ 
                                error: true, 
                                message: 'Import người dùng nhận điểm thất bại', 
                                listExists
                            });
                        }

                        return resolve({ 
                            error: false, 
                            message: 'Import người dùng nhận điểm thành công', 
                            listExists
                        });
                    });

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

    importCustomerReceivePoint(fileInfo) {
        return new Promise(async resolve => {
            try {
                XlsxPopulate.fromFileAsync(fileInfo.path)
                    .then(async workbook => {
                        let index = 2;
                        let bulkUpdateOps = [];

                        for (;true;) {
                            let phone      = workbook.sheet(0).cell(`A${index}`).value();
                            let fullname   = workbook.sheet(0).cell(`B${index}`).value();
                            let point      = workbook.sheet(0).cell(`C${index}`).value();

                            if(!phone || Number.isNaN(point)) break;

                            phone    = phone.toString().trim();
                            fullname = fullname ? fullname.toString().trim() : '';

                            /**
                             * vì sẽ có trường hợp: 0964209752 -> excel: '964209752'
                             */
                            if (phone && phone.indexOf('0', 0)) {
                                phone = `0${phone}`
                            }
                            console.log("🚀 ~ file: customer_receive_point_config.js ~ line 279 ~ Model ~ importCustomerReceivePoint ~ phone", phone)

                            const infoCustomer = await CUSTOMER_COLL
                                .findOne({ phone })
                                .lean();
                            
                            if(infoCustomer){
                                bulkUpdateOps[bulkUpdateOps.length] = {
                                    updateOne: {
                                        filter: { _id: infoCustomer._id },
                                        update: {
                                            $inc: {
                                                point: +point,
                                                pointRanking: +point,
                                            },
                                            $set: {
                                                modifyAt: new Date()
                                            }
                                        }
                                    }
                                }
                            }

                            index++;
                        }

                        // console.log({ bulkUpdateOps, bulkUpdateOps_LENGTH: bulkUpdateOps.length })

                        await fs.unlinkSync(fileInfo.path);

                        if(bulkUpdateOps.length){
                            await CUSTOMER_COLL.bulkWrite(bulkUpdateOps);
                        } else{
                            return resolve({ 
                                error: true, 
                                message: 'Import người dùng nhận điểm thất bại', 
                            });
                        }

                        return resolve({ 
                            error: false, 
                            message: 'Import người dùng nhận điểm thành công', 
                        });
                    });

            } catch (error) {
                console.error(error);
                return resolve({
                    error: true,
                    message: error.message
                });
            }
        })
    }

}

module.exports.MODEL = new Model;

var CUSTOMER_RECEIVE_POINT_CONFIG_COLL      = require('../databases/customer_receive_point_config-coll');
var GROUP_MEMBER_POINT_CONFIG_COLL          = require('../databases/group_member_point_config-coll');
var CUSTOMER_COLL                           = require('../../customer/databases/customer-coll');
var HISTORY_POINT_MODEL                     = require('../models/history_point').MODEL;

/**
 * COLLECTIONS
 */
//  var { CUSTOMER_COLL } 			= require('../../customer');
 // var REWARD_COLL  					= require('../../reward/databases/reward-coll');
 // var TRANSACTION_COLL  			= require('../../transaction/databases/transaction-coll');
 // var ORDER_COLL                	= require('../../order/databases/order-coll');
 // var ORDER_LINE_COLL               = require('../../order/databases/order_line-coll');
 // var ADDRESS_COLL  				= require('../../address/databases/address-coll');
 // var PRODUCT_COLL  				= require('../../product/databases/product-coll');
 // var DELIVERY_POINT_CONFIG_COLL  	= require('../databases/delivery_point_config-coll');
 
 /**
  * MODELS
  */
 // var COMMON_MODEL                  		= require('../../common/models/common').MODEL;
 // var CENTRALIZE_ORDER_MODEL                = require('../../order/models/centralize_order').MODEL;
//  var ORDER_GIFT_EXCHANGE_MODEL           	= require('./order_gift_exchange').MODEL;