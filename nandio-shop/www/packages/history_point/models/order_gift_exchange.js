// "use strict";

/**
 * EXTERNAL PACKAGE
 */
const moment = require('moment');
/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs } 			= require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
 var ORDER_GIFT_EXCHANGE_COLL  	= require('../databases/order_gift_exchange-coll');

class Model extends BaseModel {
    constructor() {
        super(ORDER_GIFT_EXCHANGE_COLL);
        this.TYPE_EXCHANGE_GIFT_STORE_NEAR = 1; // Đổi quà từ cửa hàng gần nhất
		this.TYPE_EXCHANGE_GIFT_FROM_HOME  = 2; // Nhận quà tại nhà
    }
	
	insert({ customer, rewards, type, addressID }) {
        return new Promise(async resolve => {
            try {
               
				if(!checkObjectIDs(customer))
                    return resolve({ error: true, message: 'params_invalid' });
                if(!checkObjectIDs(addressID))
                    return resolve({ error: true, message: 'Mời bạn chọn địa chỉ' });
				// if(!checkObjectIDs(rewards))
                //     return resolve({ error: true, message: 'params_invalid' });

				if (!rewards || !rewards.length) {
                    return resolve({ error: true, message: 'Quà tặng không hợp lệ' });
				}

                let mark       = false;
                let message    = '';
                let listRewardInsertOrderGiftExchange = [];
                let listProductCentralizeOrder        = [];
                let totalPoint = 0;

                for (let reward of rewards) {
                    let infoReward = await REWARD_COLL.findById(reward.rewardID);
                    if (!infoReward) {
                        mark    = true;
                        message = 'Quà tặng không hợp lệ';
                        return resolve({ error: true, message: 'Quà tặng không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(reward.quantities)) || Number(reward.quantities) < 0) {
                            mark    = true;
                            message = 'Số lượng quà tặng không hợp lệ';
                            return resolve({ error: true, message: 'Số lượng quà tặng không hợp lệ' });
                        }
                        totalPoint += infoReward.point * Number(reward.quantities);
                        listRewardInsertOrderGiftExchange = [
                            ...listRewardInsertOrderGiftExchange,
                            {
                                reward:     reward.rewardID,
                                quantities: reward.quantities,
                                point:      infoReward.point
                            }
                        ];

                        listProductCentralizeOrder = [
                            ...listProductCentralizeOrder,
                            {
                                product:    infoReward.gift.item,
                                onModel:    infoReward.gift.kind,
                                quantities: reward.quantities,
                                point:      infoReward.point,
                            }
                        ];
                    }
                }
                console.log({
                    totalPoint,
                });

                if (mark) {
                    return resolve({ error: true, message });
                }

				if(![this.TYPE_EXCHANGE_GIFT_STORE_NEAR, this.TYPE_EXCHANGE_GIFT_FROM_HOME].includes(type)) 
                    return resolve({ error: true, message: 'Loại đổi quà không hợp lệ' });

                const ADDRESS_ACTIVE = 1;
                let infoAddress = await ADDRESS_COLL.findById(addressID);
                // if (!infoAddress) {
                //     return resolve({ error: true, message: "Địa chỉ không tồn tại" });
                // }

                let dataInsert = {
                    customer,
                    rewards: listRewardInsertOrderGiftExchange,
                    type,
                    totalPoint,
                    flatform: 'SHOP'
                };

                const SHOP_SELLER     = 1;
                const BUYER_CONSIGNEE = 2;
                let pointDeliveryPointConfig = 0;
                let pay_shipping_fee  = BUYER_CONSIGNEE;
                if (type == this.TYPE_EXCHANGE_GIFT_FROM_HOME) { 
                    if (!infoAddress) {
                        return resolve({ error: true, message: 'Khách hàng chưa cập nhật địa chỉ' });
                    }

                    let infoPointGiftExchange = await DELIVERY_POINT_CONFIG_COLL.findOne({ // ĐIỂM CÓ ĐƯỢC MIỄN PHÍ VẬN CHUYỂN
                        point: {
                            $lte: totalPoint
                        }
                    });
    
                    if (infoPointGiftExchange) {
                        pointDeliveryPointConfig = infoPointGiftExchange.point;

                        pay_shipping_fee  = SHOP_SELLER;

                        dataInsert = {
                            ...dataInsert,
                            deliveryPointConfig: infoPointGiftExchange._id
                        }
                    }
                    // totalPoint += pointDeliveryPointConfig; // TRỪ ĐIỂM MIỄN PHÍ VẬN CHUYỂN
                    dataInsert = {
                        ...dataInsert,
                        pointDeliveryPointConfig,
                        totalPoint,
                    }
                }

                // INSERT HISTORY POINT 
                const RECEIVE_POINT = 4;
                let infoHistoryPointAfterInsert = await HISTORY_POINT_MODEL
                // .insert({ 
                //     customerID: customer, 
                //     type: HISTORY_POINT_MODEL.TYPE_EXCHANGE_POINT, 
                //     currentPoint: totalPoint 
                // });
                .createTransferPointBy__NandioAdmin({ 
                    customerID:      customer, 
                    typeNandioAdmin: RECEIVE_POINT, 
                    type:            HISTORY_POINT_MODEL.TYPE_EXCHANGE_POINT,
                    currentPoint:    totalPoint
                });
                console.log({
                    infoHistoryPointAfterInsert
                });
                if(infoHistoryPointAfterInsert.error)
                    return resolve(infoHistoryPointAfterInsert);
               
                let infoAfterInsert = await this.insertData(dataInsert);
                console.log({
                    infoAfterInsert
                });
                if (!infoAfterInsert)
                    return resolve({ error: true, message: 'add_order_gift_exchange_failed' });

                // LẤY QUÀ TẠI NHÀ THÌ LƯU CENTRALIZE ORDER
                if (type == this.TYPE_EXCHANGE_GIFT_FROM_HOME) {
                    console.log("=================TYPE_EXCHANGE_GIFT_FROM_HOME===================");
                    // // LƯU VÀO QUẢN LÝ TẬP TRUNG
                    let { address, ward, district, city } = infoAddress;

                    const cityInfo     = COMMON_MODEL.getInfoProvince({ provinceCode: city });
                    const districtInfo = COMMON_MODEL.getInfoDistrict({ districtCode: district });
                    const wardInfo     = await COMMON_MODEL.getInfoWard({ district: district, wardCode: ward });

                    let cityText     = cityInfo.data && cityInfo.data[1]?.name_with_type;
                    let districtText = districtInfo.data && districtInfo.data[1]?.name_with_type;
                    let wardText     = wardInfo.data && wardInfo.data[1]?.name_with_type;

                    const TYPE_GIFT = 3;
                    let conditionObj__CentralizeOrder = {
                        type: infoAfterInsert._id, 
                        onModel: 'order_gift_exchange',
                        customer: customer,
                        onModelCustomer: 'customer',
                        address, ward, district, city, cityText, wardText, districtText,
                        products: listProductCentralizeOrder,
                        kind: TYPE_GIFT,
                        pay_shipping_fee,
                        addressID
                    }
                   
                    let infoAfterInsertCentralizeOrder = await CENTRALIZE_ORDER_MODEL.insert(conditionObj__CentralizeOrder);
                    if (infoAfterInsertCentralizeOrder.error) {
                        return resolve(infoAfterInsertCentralizeOrder);
                    }
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoWith__Rewards({ customer, rewards }) {
        return new Promise(async resolve => {
            try {
				if(!checkObjectIDs(customer))
                    return resolve({ error: true, message: 'params_invalid' });

				// if(!checkObjectIDs(rewards))
                //     return resolve({ error: true, message: 'params_invalid' });

				if (!rewards || !rewards.length) {
                    return resolve({ error: true, message: 'Quà tặng không hợp lệ' });
				}

                let mark       = false;
                let message    = '';
                let listReward = [];
                let totalPoint = 0;

                for (let reward of rewards) {
                    let infoReward = await REWARD_COLL.findById(reward.rewardID)
                        .select('gift point')
                        .populate({
                            path: 'gift.item',
                            select: 'name'
                        });
                    
                    if (!infoReward) {
                        mark    = true;
                        message = 'Quà tặng không hợp lệ';
                        return resolve({ error: true, message: 'Quà tặng không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(reward.quantities)) || Number(reward.quantities) < 0) {
                            mark    = true;
                            message = 'Số lượng quà tặng không hợp lệ';
                            return resolve({ error: true, message: 'Số lượng quà tặng không hợp lệ' });
                        }
                        totalPoint += infoReward.point * Number(reward.quantities);
                        listReward = [
                            ...listReward,
                            {
                                name:       infoReward.gift.item.name,
                                reward:     reward.rewardID,
                                quantities: reward.quantities,
                                point:      infoReward.point
                            }
                        ];
                    }
                }

                if (mark) {
                    return resolve({ error: true, message });
                }
                
                return resolve({ error: false, data: {
                    listReward,
                    totalPoint
                }});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    calculateFeeBy__Rewards({ customer, rewards, addressID }) {
        return new Promise(async resolve => {
            try {
				if(!checkObjectIDs(customer))
                    return resolve({ error: true, message: 'params_invalid' });

				// if(!checkObjectIDs(rewards))
                //     return resolve({ error: true, message: 'params_invalid' });

				if (!rewards || !rewards.length) {
                    return resolve({ error: true, message: 'Sản phẩm không hợp lệ' });
				}

                let mark       = false;
                let message    = '';
                let listReward = [];
                let totalPoint = 0;
                let conditionObjProduct = {
                    weight: 0,
                    length: 0,
                    width:  0,
                    height: 0,
                };

                for (let reward of rewards) {
                    let infoReward = await REWARD_COLL.findById(reward.rewardID)
                        .select('gift point')
                        .populate({
                            path: 'gift.item',
                            select: 'name weight length width height'
                        });
                    
                    if (!infoReward) {
                        mark    = true;
                        message = 'Quà tặng không hợp lệ';
                        return resolve({ error: true, message: 'Quà tặng không hợp lệ' });
                    } else {
                        if (Number.isNaN(Number(reward.quantities)) || Number(reward.quantities) < 0) {
                            mark    = true;
                            message = 'Số lượng quà tặng không hợp lệ';
                            return resolve({ error: true, message: 'Số lượng quà tặng không hợp lệ' });
                        }
                        totalPoint += infoReward.point * Number(reward.quantities);
                        listReward = [
                            ...listReward,
                            {
                                name:       infoReward.gift.item.name,
                                reward:     reward.rewardID,
                                quantities: reward.quantities,
                                point:      infoReward.point
                            }
                        ];

                        conditionObjProduct.weight += infoReward.gift.item.weight * reward.quantities;
                        conditionObjProduct.length += infoReward.gift.item.length * reward.quantities;
                        conditionObjProduct.width  += infoReward.gift.item.width  * reward.quantities;
                        conditionObjProduct.height += infoReward.gift.item.height * reward.quantities;
                    }
                }
                
                if (!conditionObjProduct.weight || !conditionObjProduct.length || !conditionObjProduct.width || !conditionObjProduct.height) {
                    return resolve({
                        error: true,
                        message: 'Thông tin (Trọng lượng (gram) || Độ dài (cm) || Độ rộng (cm) || Chiều cao (cm)) của quà tặng không hợp lệ'
                    });
                }

                if (mark) {
                    return resolve({ error: true, message });
                }

                let pointRemain = 0;
                let infoPointGiftExchange = await DELIVERY_POINT_CONFIG_COLL.findOne({
                    // point: {
                    //     $lte: totalPoint
                    // }
                });

                let FREE_SHIPPING = false
                if (infoPointGiftExchange && infoPointGiftExchange.point <= totalPoint) {
                    FREE_SHIPPING = true;
                } else  if (infoPointGiftExchange) {
                    pointRemain = infoPointGiftExchange.point - totalPoint;
                }
                console.log({
                    totalPoint,
                    FREE_SHIPPING,
                    pointRemain
                });
                const ADDRESS_ACTIVE = 1;
                if (!addressID) {
                    return resolve({ error: true, message: 'Khách hàng chưa chọn địa chỉ' });
                } 
                let infoAddressCustomer = await ADDRESS_COLL.findById(addressID);
                if (!infoAddressCustomer) {
                    return resolve({ error: true, message: 'Khách hàng chưa chọn địa chỉ' });
                } 

                let { city, district, ward } = infoAddressCustomer;
                let infoAddress = await COMMON_MODEL.getInfoAddressByDistrictWardCity({ city, district, ward })
                if (infoAddress.error) {
                    return resolve(infoAddress);
                }

                let { cityInfo, districtInfo, wardInfo } = infoAddress.data;
                let cityName     = cityInfo.data && cityInfo.data[1]?.name_with_type;
                let districtName = districtInfo.data && districtInfo.data[1]?.name_with_type;
                let wardName     = wardInfo.data && wardInfo.data[1]?.name_with_type;

                // let infoAddressGHN = await GIAO_HANG_NHANH_MODEL.getInfoAddressGHN({ 
                //     cityName,
                //     districtName,
                //     wardName,
                // });

                // if (infoAddressGHN.error) {
                //     return resolve(infoAddressGHN);
                // }
                let infoAgencyData = await AGENCY_MODEL.getInfo({ 
                    wardName: wardName, districtName: districtName, cityName: cityName 
                });
                if (infoAgencyData.error) {
                    return resolve(infoAgencyData);
                }
                let { data: { infoAgency, infoAddressGHN }} = infoAgencyData;
                // console.log({
                //     // infoAddressGHN,
                //     cityInfo, districtInfo, wardInfo,
                //     infoAgency, infoAddressGHN
                // });

                let calculateFeeGHN = await GIAO_HANG_NHANH_MODEL.calculateFee({ 
                    districtStore:    infoAgency.district,
                    wardStore:        infoAgency.ward,
                    storeID:          infoAgency.storeCodeGHN,
                    wardCustomer:     infoAddressGHN.data.infoWard.WardCode,
                    districtCustomer: infoAddressGHN.data.infoDistrict.DistrictID,
                    weight:           conditionObjProduct.weight ? conditionObjProduct.weight : 0,
                    length:           conditionObjProduct.length ? conditionObjProduct.length : 0,
                    width:            conditionObjProduct.width  ? conditionObjProduct.width  : 0,
                    height:           conditionObjProduct.height ? conditionObjProduct.height : 0,
                });
                console.log({
                    calculateFeeGHN
                });
                if (calculateFeeGHN.code != 200) {
                    return resolve({
                        error: true,
                        ...calculateFeeGHN,
                    });
                }
                
                return resolve({ error: false, ...calculateFeeGHN, FREE_SHIPPING, pointRemain });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getOrderGiftExchangesBy__Rewards({ customer, fromDate, toDate, page = 1, limit = 30 }) {
        return new Promise(async resolve => {
            try {
				page  = +page;
				limit = +limit;

				if(!checkObjectIDs(customer))
                    return resolve({ error: true, message: 'params_invalid' });

				if(!page || !limit)
					return resolve({ error: true, message: 'param_invalid' });

				let conditionObj = { customer: customer };

				if(fromDate && toDate){
					conditionObj.createAt = {
                        $gte: new Date(moment(fromDate).startOf('day').format()),
                        $lte: new Date(moment(toDate).endOf('day').format()),
                    }
				}
				console.log({
					conditionObj,
				});

                let listOrderGiftExchange = await ORDER_GIFT_EXCHANGE_COLL.find(conditionObj)
                    .select("rewards totalPoint createAt")
                    .populate({
                        path: 'rewards.reward',
                        select: 'gift',
                        populate: {
                            path: 'gift.item',
                            select: 'name'
                        }
                    })
                    .sort({ createAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
                    .lean();

                if(!listOrderGiftExchange)
                    return resolve({ error: true, message: 'cannot_get_list_history_point' });


                let totalOrderGiftExchange = await ORDER_GIFT_EXCHANGE_COLL.count(conditionObj);
                let pages = Math.ceil(totalOrderGiftExchange/limit);
                
                return resolve({ 
                    error: false, 
                    data: {
                        listOrderGiftExchange,
                        currentPage: +page,
                        // perPage: limit,
                        totalPage: +pages
                    }
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getOrderGiftExchange({ orderGiftExchangeID }) {
        return new Promise(async resolve => {
            try {
				if(!checkObjectIDs(orderGiftExchangeID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoOrderGiftExchange = await ORDER_GIFT_EXCHANGE_COLL.findById(orderGiftExchangeID)
                    .select('rewards totalPoint type createAt')
                    .populate({
                        path: 'rewards.reward',
                        select: 'gift',
                        populate: {
                            path: 'gift.item',
                            select: 'name'
                        }
                    });

                if (!infoOrderGiftExchange) {
                    let infoHistoryPoint = await HISTORY_POINT_MODEL.getInfo({ historyPointID: orderGiftExchangeID });
                    if (infoHistoryPoint.error) {
                        return resolve({ error: true, message: 'Quà không tồn tại' });
                    } else {
                        infoOrderGiftExchange = infoHistoryPoint.data
                    }
                } 
				
                return resolve({ error: false, data: infoOrderGiftExchange });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

module.exports.MODEL = new Model;
// var HISTORY_POINT_MODEL         	= require('../../history_point/models/history_point').MODEL;
var { MODEL: HISTORY_POINT_MODEL }			= require('../../history_point/models/history_point');
var REWARD_COLL               	= require('../../reward/databases/reward-coll');
var DELIVERY_POINT_CONFIG_COLL 	= require('../databases/delivery_point_config-coll');
var ADDRESS_COLL  				= require('../../address/databases/address-coll');

/**
 * MODELS
 */
var CENTRALIZE_ORDER_MODEL        = require('../../order/models/centralize_order').MODEL;
var COMMON_MODEL                  = require('../../common/models/common').MODEL;
var GIAO_HANG_NHANH_MODEL         = require('../../giao_hang_nhanh/models/giao_hang_nhanh').MODEL;
var AGENCY_MODEL   				= require('../../store/models/agency').MODEL;
