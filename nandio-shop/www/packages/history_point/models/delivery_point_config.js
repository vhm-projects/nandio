"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
// const request                      	= require('request');

/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs } 			= require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const DELIVERY_POINT_CONFIG_COLL  	= require('../databases/delivery_point_config-coll');

/**
 * MODELS
 */

class Model extends BaseModel {
    constructor() {
        super(DELIVERY_POINT_CONFIG_COLL);
    }
	
	insert({ point, pointReferral, userCreate }) {
        return new Promise(async resolve => {
            try {
				if (!checkObjectIDs(userCreate))
					return resolve({ error: true, message: 'Param Invalid' });

				let dataInsert = { userCreate };

				if (!point) {
                    return resolve({ error: true, message: 'Số điểm không hợp lệ' });
				} else {
					if (Number.isNaN(Number(point)) || Number(point) < 0) {
						return resolve({ error: true, message: 'Số điểm không đúng định dạng' });
					}

					dataInsert = {
						point
					}
				}

				if(pointReferral){
					if(pointReferral < 0){
						return resolve({ error: true, message: 'Số điểm giới thiệu không hợp lệ' });
					}

					dataInsert.pointReferral = pointReferral;
				}

				let infoDeliveryPointConfig = await DELIVERY_POINT_CONFIG_COLL.findOne({});

				if (infoDeliveryPointConfig) {
					let infoDeliveryPointConfigAfterUpdate = await DELIVERY_POINT_CONFIG_COLL.findByIdAndUpdate(infoDeliveryPointConfig._id, {
						point, pointReferral, userUpdate: userCreate
					}, {
						new: true
					});
					if(!infoDeliveryPointConfigAfterUpdate)
						return resolve({ error: true, message: 'cannot_update_delivery_point_config' });
					
					return resolve({ error: false, data: infoDeliveryPointConfigAfterUpdate });
				} else {
					let infoAfterInsert = await this.insertData(dataInsert);
					if(!infoAfterInsert)
						return resolve({ error: true, message: 'add_delivery_point_config_failed' });
					
					return resolve({ error: false, data: infoAfterInsert });
				}
               
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfo({}) {
        return new Promise(async resolve => {
            try {
				let infoDeliveryPointConfig = await DELIVERY_POINT_CONFIG_COLL.findOne({});
                if(!infoDeliveryPointConfig)
                    return resolve({ error: true, message: 'cannot_get_info_delivery_point_config' });
				
                return resolve({ error: false, data: infoDeliveryPointConfig });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
