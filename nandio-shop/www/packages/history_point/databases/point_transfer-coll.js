"use strict";

const { Schema } = require('mongoose');
const BASE_COLL  = require('../../../database/intalize/base-coll');

const GeoSchema = new Schema({
    type: {
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        index: "2dsphere"
    }
});

/**
 * COLLECTION MEMBER NHẬN ĐIỂM CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('point_transfer', {
	/**
     * Người chuyển
     */
	sender: {
		type:  Schema.Types.ObjectId,
        ref: 'customer'
	},
    /**
     * Người nhận
     */
	receiver: {
		type:  Schema.Types.ObjectId,
        ref: 'customer'
	},
    /**
     * Hình thức dựa vào HISTORY_POINT
     */
    type: {
        type: Number,
    },
    /**
     * Só điểm nhận
     */
    point: {
        type: Number,
        require: true
    },
    /**
     * Nội dung chuyển
     */
    content: {
        type: String
    },
	// Vị trí
	location: GeoSchema,
});
