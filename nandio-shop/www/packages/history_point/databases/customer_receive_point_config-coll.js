"use strict";

const BASE_COLL  = require('../../../database/intalize/base-coll');

/**
 * COLLECTION CẤU HÌNH NHÓM ĐỐI TƯỢNG NHẬN ĐIỂM
 */
module.exports = BASE_COLL('customer_receive_point_config', {
	name: String,
	//________Điểm
	point : {
		type: Number,
		default: 0
	},
	isDefault: {
		type: Boolean,
		default: false
	}
});
