"use strict";

const { Schema } = require('mongoose');
const BASE_COLL  = require('../../../database/intalize/base-coll');

/**
 * COLLECTION MEMBER NHẬN ĐIỂM CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('group_member_point_config', {
	/**
     * Số điện thoại
     */
	phone: {
		type: String,
		trim: true,
		require: true,
        unique: true
	},
    /**
     * Họ và tên
     */
	fullname: {
		type: String,
		trim: true,
		default: '',
	},
	/**
	 * Nhóm nhận quà
	 */
	group: {
		type:  Schema.Types.ObjectId,
        ref: 'customer_receive_point_config'
	},
});
