"use strict";

const { Schema } = require('mongoose');
const BASE_COLL  = require('../../../database/intalize/base-coll');

/**
 * COLLECTION LỊCH SỬ ĐỔI ĐIỂM CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('delivery_point_config', {

	// Điểm miễn phí vận chuyển
	point: {
		type: Number,
		required: true
	},

	// Điểm tặng mã giới thiệu
	pointReferral: {
		type: Number,
	},

	userCreate: {
		type:  Schema.Types.ObjectId,
		ref : 'user'
	},

	userUpdate: {
		type:  Schema.Types.ObjectId,
		ref : 'user'
	}
});
