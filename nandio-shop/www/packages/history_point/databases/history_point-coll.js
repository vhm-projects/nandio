"use strict";

const { Schema } = require('mongoose');
const BASE_COLL  = require('../../../database/intalize/base-coll');

/**
 * COLLECTION LỊCH SỬ ĐỔI ĐIỂM CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('history_point', {
	/**
     * KHÁCH HÀNG
     */
	customer: {
        type:  Schema.Types.ObjectId,
        ref: 'customer'
    },

	point_transfer: {
        type:  Schema.Types.ObjectId,
        ref: 'point_transfer'
    },
	/**
     * GIAO DỊCH
     */
	// transaction: {
    //     type:  Schema.Types.ObjectId,
    //     ref: 'transaction'
    // },
	/**
	 * CASE: do khách hàng mua hàng về cần tích điểm trên từng sản phẩm
	 * 	-> chỉ lưu product vào history_point, không cần lưu order 
	 */
	product: {
		type:  Schema.Types.ObjectId,
        ref: 'product'
	},
	// order: {
	// 	type:  Schema.Types.ObjectId,
    //     ref: 'order'
	// },
	//________ID đổi quà
	// rewards: [{
	// 	type:  Schema.Types.ObjectId,
    //     ref: 'reward'
	// }],
	//________Điểm trước khi thay đổi
	beforPoint: {
		type: Number,
	},

	//________Điểm được cộng/trừ
	currentPoint: {
		type: Number,
	},

	//________Điểm sau khi thay đổi
	point: {
		type: Number,
		required: true
	},
	/**
	 * Loại lịch sử:
	 * 1: Tích điểm
	 * 2: Đổi điểm
	 * 3: Đăng ký tài khoản Nhận điểm
	 * 4: Nhận điểm chuyển tới
	 * 5: Chuyển điểm
	 * 6: Member get member (mã giới thiệu)
	 * 7. Nạp điểm (GLOQ)
	 */
	type: {
		type: Number,
		default: 1
	},
	
	/**
	 * Số lượng đổi quà
	 */
	// amount: {
	// 	type: Number,
	// 	default: 1
	// },

	/**
	 * Lưu ý: Chỉ áp dụng cho tích điêm
	 * Loại mua hàng:
	 * 1: Qua quét QR
	 * 2: Mua online
	 * 3: Mua offline
	 */
	// buyType: {
	// 	type: Number,
	// 	default: 2
	// },
	/**
	 * Loại lịch sử:
	 * 1: STORE
	 * 2: SHOP
	 */
	flatform: {
		type: String,
		enum: ['STORE', 'SHOP'],
	},
});
