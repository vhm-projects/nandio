const SEGMENT_MODEL 		= require('./models/segment').MODEL;
const SEGMENT_COLL  		= require('./databases/segment-coll');
const SEGMENT_ROUTES       = require('./apis/segment');

module.exports = {
    SEGMENT_ROUTES,
    SEGMENT_COLL,
    SEGMENT_MODEL,
}
