const BASE_ROUTE = '/segment';

const CF_ROUTINGS_SEGMENT = {
    // USER PERMISSION
	ADD_SEGMENT:             `${BASE_ROUTE}/add-segment`,
	LIST_SEGMENT:            `${BASE_ROUTE}/list-segment`,
	INFO_SEGMENT:            `${BASE_ROUTE}/info-segment/:segmentID`,
    UPDATE_SEGMENT:          `${BASE_ROUTE}/update-segment/:segmentID`,
    UPDATE_SEGMENT_STT:          `${BASE_ROUTE}/update-segment-stt`,
    UPDATE_SEGMENT_STATUS:          `${BASE_ROUTE}/update-segment-status`,
    DELETE_SEGMENT:          `${BASE_ROUTE}/delete-segment`,
	ADD_CUSTOMER_SEGMENT:    `${BASE_ROUTE}/add-customer-segment`,


    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_SEGMENT = CF_ROUTINGS_SEGMENT;
