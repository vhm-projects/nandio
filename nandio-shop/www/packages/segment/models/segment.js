"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGES
 */

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const SEGMENT_COLL  				= require('../databases/segment-coll');
const USER_SEGMENT_MODEL  			= require('../../product/models/user_segment').MODEL;
const USER_SEGMENT_COLL  			= require('../../product/databases/user_segment-coll');
const { checkObjectIDs }            = require('../../../utils/utils');


class Model extends BaseModel {
    constructor() {
        super(SEGMENT_COLL);
    }

    /**
     * FUNCTION: INSERT SEGEMNT
     * AUTHOR: SONLP
     * DATE: 03/07/2021
     */
	insert({ name, description }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK TÊN CÓ TỒN TẠI
                 */
                if(!name) {
                    return resolve({ error: true, message: 'name_invalid' });
                }
                let dataInsert = {
                    name
                }
                if(description) {
                    dataInsert.description = description;
                }
               
                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'create_segment_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: LIST SEGEMNT
     * AUTHOR: SONLP
     * DATE: 03/07/2021
     */
	getList({  }) {
        return new Promise(async resolve => {
            try {
                let listSegment = await SEGMENT_COLL.find({}).sort({ order: 1, modifyAt: -1 })

                if(!listSegment)
                    return resolve({ error: true, message: 'get_list_failed' });

                return resolve({ error: false, data: listSegment });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListActiveV2({  }) {
        return new Promise(async resolve => {
            try {
                let listSegment = await SEGMENT_COLL.find({ status: 1 }).sort({ order: 1, modifyAt: -1 })

                if(!listSegment)
                    return resolve({ error: true, message: 'get_list_failed' });

                return resolve({ error: false, data: listSegment });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListSegmentActive({ listSegmentID }) {
        return new Promise(async resolve => {
            try {
                let listSegment = await SEGMENT_COLL.find({ status: 1, _id: { $in: listSegmentID } }).sort({ order: 1, modifyAt: -1 })

                if(!listSegment)
                    return resolve({ error: true, message: 'get_list_failed' });

                return resolve({ error: false, data: listSegment });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * DANH SÁCH SẢN PHẨM ĐANG HOẠT ĐỘNG
     */
    getListActive({ segmentID }) {
        return new Promise(async resolve => {
            try {
                let condition = { status: 1 };
                if (segmentID) {
                    if (!checkObjectIDs(segmentID))
                        return resolve({ error: true, message: 'id_invalid' });

                    condition = {
                        ...condition,
                        _id : {
                            $nin: [segmentID]
                        }
                    }
                }
                
                let listSegment = await SEGMENT_COLL.find({ ...condition }, { name: 1 }).limit(10).sort({ order: 1, modifyAt: -1 });

                if(!listSegment)
                    return resolve({ error: true, message: 'get_list_failed' });

                return resolve({ error: false, data: listSegment });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: GET INFO SEGEMNT
     * AUTHOR: SONLP
     * DATE: 03/07/2021
     */
     getInfo({ segmentID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(segmentID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }
                let infoSegment       = await SEGMENT_COLL.findById(segmentID);
                if(!infoSegment)
                    return resolve({ error: true, message: 'get_list_failed' });

                let listUserOfSegment = await USER_SEGMENT_COLL.find({ segment: segmentID })
                    .populate("user");

                return resolve({ error: false, data: infoSegment, listUserOfSegment });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateSTT({ segmentID, order }) {
        return new Promise(async resolve => {
            try {
                
                let dataUpdate = {}
                if(!checkObjectIDs(segmentID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }

                if (order || order != 0) {
                    dataUpdate.order      = order;
                }
                
                let infoAfterUpdate = await SEGMENT_COLL.findByIdAndUpdate(segmentID, {
                    ...dataUpdate
                }, {
                    new: true
                });
                if (!infoAfterUpdate) {
                    return resolve({ error: true, message: "cannot_update" });
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoV2({ segmentID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(segmentID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }
                let infoSegment       = await SEGMENT_COLL.findById(segmentID, { name: 1 });
                if(!infoSegment)
                    return resolve({ error: true, message: 'get_list_failed' });

                let listUserOfSegment = await USER_SEGMENT_COLL.find({ segment: segmentID })
                    .populate("user");

                return resolve({ error: false, data: infoSegment, listUserOfSegment });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoSegment({ segmentID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(segmentID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }
                let infoSegment       = await SEGMENT_COLL.findById(segmentID);
                if(!infoSegment)
                    return resolve({ error: true, message: 'get_list_failed' });

                return resolve({ error: false, data: infoSegment });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: UPDATE SEGEMNT
     * AUTHOR: SONLP
     * DATE: 03/07/2021
     */
	update({ segmentID, name, description }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK TÊN CÓ TỒN TẠI
                 */
                if (!checkObjectIDs(segmentID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }
                if(!name) {
                    return resolve({ error: true, message: 'name_invalid' });
                }
                let dataUpdate = {
                    name,
                    modifyAt: new Date()
                }

                if (description) {
                    dataUpdate.description = description;
                } else {
                    dataUpdate.description = ""
                }
               
                let infoAfterUpdate = await SEGMENT_COLL.findByIdAndUpdate(segmentID, {...dataUpdate}, { new: true });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'create_segment_failed' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateStatus({ segmentID, status }) {
        return new Promise(async resolve => {
            try {
                /**
                 * CHECK TÊN CÓ TỒN TẠI zxc
                 */
                if (!checkObjectIDs(segmentID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }
                let dataUpdate = { modifyAt: new Date() }
                if (status || status == 0) {
                    dataUpdate.status      = status
                }
               
                let infoAfterUpdate = await SEGMENT_COLL.findByIdAndUpdate(segmentID, {
                    ...dataUpdate
                }, { 
                    new: true 
                });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'create_segment_failed' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: LIST SEGEMNT
     * AUTHOR: SONLP
     */
	insertCustomerSegment({ segmentID, listCustomer }) {
        return new Promise(async resolve => {
            try {
                const ADMIN_CREATE = 0;
                if (!checkObjectIDs(segmentID)) {
                    return resolve({ error: true, message: 'id_invalid' });
                }
                let infoSegment = await SEGMENT_COLL.findById(segmentID);

                if(!infoSegment)
                    return resolve({ error: true, message: 'segment_not_existed' });

                let listUserSegment = listCustomer.map( customer => {
                    if (!checkObjectIDs(customer)) {
                        return resolve({ error: true, message: 'id_invalid' });
                    }
                    return USER_SEGMENT_MODEL.insert({ segment: [segmentID], user: customer, type: ADMIN_CREATE })
                });
                let resultAfterInsert = await Promise.all(listUserSegment);
                if (!resultAfterInsert) {
                    return resolve({ error: true, message: 'cannot_insert_user_segment' });
                }

                return resolve({ error: false, data: resultAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
