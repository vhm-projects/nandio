"use strict";
const path = require('path');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { CF_ROUTINGS_SEGMENT } 				= require('../constants/segment.uri');

/**
 * MODELS
 */
const SEGMENT_MODEL        = require('../models/segment').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ SEGMENT  ================================
             * ========================== ************************ ================================
             */

			/**
             * Function: TẠO SEGMENT API
             * Date: 03/07/2021
             * Dev: SONLP
             */
             [CF_ROUTINGS_SEGMENT.ADD_SEGMENT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_SEGMENT.ADD_SEGMENT,
                    inc : path.resolve(__dirname, '../views/add_segment.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [async (req, res) => {
                        ChildRouter.renderToView(req, res, { });
                    }],
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        const { name, description } = req.body;
                        
                        const infoAfterInsertSegment = await SEGMENT_MODEL.insert({ 
                            name, description
                        });

                        res.json(infoAfterInsertSegment);
                    }]
                },
            },

            /**
             * Function: TẠO SEGMENT API
             * Date: 03/07/2021
             * Dev: SONLP
             */
             [CF_ROUTINGS_SEGMENT.LIST_SEGMENT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_SEGMENT.LIST_SEGMENT,
                    inc : path.resolve(__dirname, '../views/list_segment.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [async (req, res) => {
                        let { type } = req.query;
                        const listSegment = await SEGMENT_MODEL.getList({});
                        if(type){
                            res.json(listSegment)
                        }else{
                            ChildRouter.renderToView(req, res, { listSegment: listSegment.data });
                        }
                    }]
                },
            },

            /**
             * Function: UPDATE SEGMENT API
             * Date: 03/07/2021
             * Dev: SONLP
             */
             [CF_ROUTINGS_SEGMENT.UPDATE_SEGMENT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_SEGMENT.UPDATE_SEGMENT,
                    inc : path.resolve(__dirname, '../views/update_segment.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [async (req, res) => {
                        let { segmentID } = req.params;

                        let infoSegment = await SEGMENT_MODEL.getInfo({ segmentID });

                        if (infoSegment.error) {
                            return res.json(infoSegment);
                        }

                        ChildRouter.renderToView(req, res, { infoSegment: infoSegment.data });
                    }],
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { segmentID }   = req.params;
                        const { name, description } = req.body;
                        
                        const infoAfterInsertSegment = await SEGMENT_MODEL.update({ 
                            name, description, segmentID
                        });

                        res.json(infoAfterInsertSegment);
                    }]
                },
            },

            [CF_ROUTINGS_SEGMENT.UPDATE_SEGMENT_STT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        const { stt, segmentID } = req.body;

                        const infoDetailBrand = await SEGMENT_MODEL.updateSTT({ segmentID, order: stt });

                        res.json(infoDetailBrand);
                    }]
                },
            },

            [CF_ROUTINGS_SEGMENT.UPDATE_SEGMENT_STATUS]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        const { status, segmentID } = req.body;

                        const infoDetailBrand = await SEGMENT_MODEL.updateStatus({ segmentID, status });

                        res.json(infoDetailBrand);
                    }]
                },
            },

            /**
             * Function: INFO SEGMENT API
             * Date: 03/07/2021
             * Dev: SONLP
             */
             [CF_ROUTINGS_SEGMENT.INFO_SEGMENT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'view',
                    code: CF_ROUTINGS_SEGMENT.INFO_SEGMENT,
                    inc : path.resolve(__dirname, '../views/info_segment.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [async (req, res) => {
                        let { segmentID } = req.params;

                        let infoSegment = await SEGMENT_MODEL.getInfo({ segmentID });

                        if (infoSegment.error) {
                            return res.json(infoSegment);
                        }

                        ChildRouter.renderToView(req, res, { infoSegment: infoSegment.data, listUserOfSegment: infoSegment.listUserOfSegment });
                    }]
                },
            },

            /**
             * Function: THÊM KHÁCH HÀNG VÀO SEGMENT
             * Dev: SONLP
             */
             [CF_ROUTINGS_SEGMENT.ADD_CUSTOMER_SEGMENT]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        const { listCustomer, segmentID } = req.body;
                        const infoAfterInsertSegment = await SEGMENT_MODEL.insertCustomerSegment({ segmentID, listCustomer });

                        res.json(infoAfterInsertSegment);
                    }]
                },
            },
        }
    }
};
