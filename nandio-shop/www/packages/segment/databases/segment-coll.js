"use strict";
const Schema        = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION PRODUCT CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('segment', {
	name: {
		type: String,
	}, 
	description: {
		type: String,
	},
	// STT HIỂN THỊ
	order: {
		type: Number,
	},
	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},
});
