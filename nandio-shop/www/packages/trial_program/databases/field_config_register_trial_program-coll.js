"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * TRAIAL PROGRAM - SẢN PHẨM DÙNG THỬ
 */
module.exports = BASE_COLL('field_config_register_trial_program', {

	trialProgram: {
        type: Schema.Types.ObjectId,
        ref: 'trial_program'
    },
	
	title: String,
	/**
	 * 1: text
	 * 2: dropdown
	 */
	type: {
		type: Number,
		default: 1
	},
	/**
	 *  Gía trị cho dropdown
	 */ 
	values: String,
	/**
	 * 1: Hoạt động
	 * 2: Đã xóa
	 */
	status: {
		type: Number,
		default: 1
	}
});
