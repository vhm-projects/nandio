"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');
/**
 * collection số lượng khách hàng nhận thông báo
 */
module.exports = BASE_COLL('view_trial_program', {

	trialProgram: {
        type: Schema.Types.ObjectId,
        ref: 'trial_program'
    },

	deviceID: String,
	/**
	 * Trạng thái
	 * -1: Chưa đóng popup hoặc chưa nhấn vào xem chi tiết
	 *  0: Chưa xem
	 *  1: Đã xem
	 */
	status: {
		type: Number,
		default: -1
	}
});
