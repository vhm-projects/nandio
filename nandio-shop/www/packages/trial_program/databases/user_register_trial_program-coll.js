"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');
/**
 * TRAIAL PROGRAM - SẢN PHẨM DÙNG THỬ
 */
module.exports = BASE_COLL('user_register_trial_program', {

	trialProgram: {
        type: Schema.Types.ObjectId,
        ref: 'trial_program'
    },
	
	product: {
        type: Schema.Types.ObjectId,
        ref: 'product'
    },
	/**
	 * REGISTER_TRIAL_PROGRAM_STATUS cf_constant
	 *  { value: 0, text: 'Đã Nhận' },
		{ value: 1, text: 'Đã Xem' },
		{ value: 2, text: 'Đang Chuyển Hàng' },
		{ value: 3, text: 'Khách không nhận' },
		{ value: 4, text: 'Khách đã nhận' },
	 */
	status: {
		type: Number,
		default: 0
	},
	deviceID: String,
	fullname: String,
	email: String,
	phone: String,
	address: String,

	// Địa chỉ
	cityNumber: String,
	cityText: String,
	districtNumber: String,
	districtText: String,
	wardNumber: String,
	wardText: String,
	fullAddress: String,
	note: String,
	// Các trường thông tin khác
	fields: [
		{
			type: Schema.Types.ObjectId,
			ref: 'user_fields_register_trial_program'
		}
	]
});
