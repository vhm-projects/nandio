"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');
/**
 * Bảng lưu những giá trị mà khách hàng nhập hoặc chọn những trường cấu hình thêm khi đăng ký dùng thử trialprogram
 */
module.exports = BASE_COLL('user_fields_register_trial_program', {
	field: {
		type: Schema.Types.ObjectId,
        ref: 'field_config_register_trial_program'
	},
	value: String,
});
