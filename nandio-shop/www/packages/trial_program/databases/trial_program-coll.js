"use strict";

const { Schema } = require('mongoose');
const BASE_COLL = require('../../../database/intalize/base-coll');
/**
 * TRAIAL PROGRAM - SẢN PHẨM DÙNG THỬ
 */
module.exports = BASE_COLL('trial_program', {
	title: String,
	description: String,
	content: String,
	startTime: Date,
	endTime: Date,
	// ảnh đại diện
	image: {
        type: Schema.Types.ObjectId,
        ref: 'image'
    },
	// Ảnh banner
	banner: {
		type: Schema.Types.ObjectId,
		ref: 'image'
	},
	/**
	 * 0: Không hoạt động,
	 * 1: Hoạt động,
	 * 2: Đã xóa
	 */
	status: { 
		type: Number,
		default: 1
	},
	// Lượt xem
	views: { 
		type: Number,
		default: 0
	},
	// Lượt đăng ký dùng thử
	registers: { 
		type: Number,
		default: 0
	},
	segments: [
		{
			type: Schema.Types.ObjectId,
			ref: 'segment'
		}
	],
	products: [
		{
			type: Schema.Types.ObjectId,
			ref: 'product'
		}
	],
	// ảnh đại diện
	userCreate: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
});
