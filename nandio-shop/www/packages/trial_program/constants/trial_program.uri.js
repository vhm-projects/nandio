const BASE_ROUTE = '/trial-program';
const BASE_ROUTE_AMIND = '/admin';
const API = '/api';

const CF_ROUTINGS_TRIAL_PROGRAM = {
	ADD_TRIAL_PROGRAM: `${BASE_ROUTE}/add-trial-program`,
	REMOVE_TRIAL_PROGRAM: `${BASE_ROUTE}/remove-trial-program/:trialProgramID`,
	UPDATE_TRIAL_PROGRAM: `${BASE_ROUTE}/update-trial-program`,
	UPDATE_TRIAL_PROGRAM_STATUS: `${BASE_ROUTE}/update-trial-program-status`,
	LIST_TRIAL_PROGRAM_ADMIN: `${BASE_ROUTE_AMIND}/list-trial-program`,
	LIST_REGISTER_TRIAL_PROGRAM_ADMIN: `${BASE_ROUTE_AMIND}/trial-program/:trialProgramID`,
	USER_REGISTER_TRIAL_PROGRAM_ADMIN: `${BASE_ROUTE_AMIND}/user-register-trial-program-admin/:userRegistertrialProgramID`,
	INFO_TRIAL_PROGRAM_FOR_ADMIN: `${BASE_ROUTE}/info-trial-program-for-admin/:trialProgramID`,
	INFO_TRIAL_PROGRAM_FOR_ENDUSER: `${BASE_ROUTE}/info-trial-program-for-enduser/:trialProgramID`,
	INFO_TRIAL_PROGRAM_CURRENT: `${BASE_ROUTE}/info-trial-program-current/:deviceID`,
	INFO_TRIAL_PROGRAM_CURRENT_FOR_ADMIN: `${BASE_ROUTE}/info-trial-program-current-for-admin`,
	INFO_TRIAL_PROGRAM_CURRENT_FOR_ENDUSER: `${BASE_ROUTE}/info-trial-program-current-for-enduser`,
	ADD_VIEW_TRIAL_PROGRAM: `${BASE_ROUTE}/add-view-trial-program`,
	UPDATE_STATUS_VIEW_TRIAL_PROGRAM: `${BASE_ROUTE}/update-status-view-trial-program`,
	USER_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/user-register-trial-program`,
	INFO_USER_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/info-user-register-trial-program/:registerTrialProgramID`,
	UPDATE_USER_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/update-user-register-trial-program`,
	ADD_FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/add-fields-config-register-trial-program`,

	UPDATE_FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/update-fields-config-register-trial-program`,
	REMOVE_FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/remove-fields-config-register-trial-program/:fieldID`,
	INFO_FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM: `${BASE_ROUTE}/info-field-config-register-trial-program/:fieldID`,
	LIST_FIELD_REGISTER_TRIAL_PROGRAM_FOR_ENDUSER: `${BASE_ROUTE}/list-field-register-trial-program-for-enduser/:trialProgramID`,
	
	API_LIST_TRIAL_PROGRAM_ADMIN: `/api${BASE_ROUTE_AMIND}/list-trial-program`,

    ORIGIN_APP: BASE_ROUTE
}
exports.CF_ROUTINGS_TRIAL_PROGRAM = CF_ROUTINGS_TRIAL_PROGRAM;
