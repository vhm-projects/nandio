"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { CF_ROUTINGS_TRIAL_PROGRAM } 		= require('../constants/trial_program.uri');

const PRODUCT_COLL                          = require('../../product/databases/product-coll');
const SEGMENT_COLL                          = require('../../segment/databases/segment-coll');
const VIEW_TRIAL_PROGRAM_COLL               = require('../databases/view_trial_program-coll');

/**
 * MODELS
 */
const TRIAL_PROGRAM_MODEL                   = require('../models/trial_program').MODEL;
const VIEW_TRIAL_PROGRAM_MODEL              = require('../models/view_trial_program').MODEL;
const USER_REGISTER_TRIAL_PROGRAM_MODEL     = require('../models/user_register_trial_program').MODEL;
const FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL = require('../models/fields_config_register_trial_program').MODEL;
const COMMON_MODEL                          = require("../../common/models/common").MODEL;

const { REGISTER_TRIAL_PROGRAM_STATUS  }    = require("../../../config/cf_constants");
module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ====================== ************************ ================================
             * ======================    QUẢN LÝ CỬA HÀNG    ================================
             * ====================== ************************ ================================
             */
            /**
             * View: Danh sách cửa hàng admin
             * Date: 20/06/2021
             * Dev: depv
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.LIST_TRIAL_PROGRAM_ADMIN]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'List Trial Program - Nandio',
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_TRIAL_PROGRAM.LIST_TRIAL_PROGRAM_ADMIN,
                    inc: path.resolve(__dirname, '../views/list_trial_program.ejs'),
                },
                methods: {
                    get: [ async function(req, res) {
                        let listTrialProgram = await TRIAL_PROGRAM_MODEL.getListByAdmin();
                        let listProductTrialProgram = await PRODUCT_COLL.find({ isTrialProgram: 1 }).select("name");
                        let listSegment = await SEGMENT_COLL.find({ status: 1 });
                        ChildRouter.renderToView(req, res, {
                            listTrialProgram: listTrialProgram.data,
                            listProductTrialProgram,
                            listSegment
                        });
					}],
                },
            },

             /**
             * View: Danh sách người đăng ký sản phẩm thử nghiệm
             * Date: 20/06/2021
             * Dev: depv
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.LIST_REGISTER_TRIAL_PROGRAM_ADMIN]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'List Trial Program - Nandio',
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_TRIAL_PROGRAM.LIST_REGISTER_TRIAL_PROGRAM_ADMIN,
                    inc: path.resolve(__dirname, '../views/list_register_trial_program.ejs'),
                },
                methods: {
                    get: [ async function(req, res) {
                        let { trialProgramID } = req.params;
                        let listRegisterTrialProgram = await USER_REGISTER_TRIAL_PROGRAM_MODEL.getListForAdmin({ trialProgramID });
                        let infoTrialProgram = await TRIAL_PROGRAM_MODEL.getInfoForAdmin({ trialProgramID });
                        let amountCustomerReceiver = await VIEW_TRIAL_PROGRAM_COLL.count({ trialProgram: trialProgramID });
                        let amountCustomerSeen = await VIEW_TRIAL_PROGRAM_COLL.count({ trialProgram: trialProgramID, status: 1 });
                        let amountCustomerNotSeen = await VIEW_TRIAL_PROGRAM_COLL.count({ trialProgram: trialProgramID, status: { $ne: 1 } });
                        let listProvince   = COMMON_MODEL.listProvinceAll({});
                        const listFields = await FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getList({ trialProgramID });
                        ChildRouter.renderToView(req, res, {
                            infoTrialProgram: infoTrialProgram.data,
                            listRegisterTrialProgram: listRegisterTrialProgram.data,
                            amountCustomerReceiver,
                            amountCustomerSeen,
                            amountCustomerNotSeen,
                            REGISTER_TRIAL_PROGRAM_STATUS,
                            listProvince: listProvince.data,
                            listFields: listFields.data
                        });
					}],
                },
            },

              /**
             * View: Thông tin người dùng đăng ký thử nghiệm trial program
             * Date: 20/06/2021
             * Dev: depv
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.USER_REGISTER_TRIAL_PROGRAM_ADMIN]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Info User Register Trial Program - Nandio',
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_TRIAL_PROGRAM.USER_REGISTER_TRIAL_PROGRAM_ADMIN,
                    inc: path.resolve(__dirname, '../views/info_user_register_trial_program.ejs'),
                },
                methods: {
                    get: [ async function(req, res) {
                        const { userRegistertrialProgramID } = req.params;
                        const infoUserRegistertrialProgram = await USER_REGISTER_TRIAL_PROGRAM_MODEL.getInfo({ registerTrialProgramID: userRegistertrialProgramID  });
                        ChildRouter.renderToView(req, res, {
                            infoUserRegistertrialProgram: infoUserRegistertrialProgram.data
                        });
					}],
                },
            },

            /**
             * Function: Tạo Store (VIEW, API)
             * Date: 20/06/2021
             * Dev: Depv
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.ADD_TRIAL_PROGRAM]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Add Trial Program - Nandio',
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_TRIAL_PROGRAM.ADD_TRIAL_PROGRAM,
                    inc: path.resolve(__dirname, '../views/add_trial_program.ejs'),
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { title, description, content, startTime, endTime, segments, products, image, banner } = req.body;
                        let infoAfterInsert = await TRIAL_PROGRAM_MODEL.insert({ title, description, content, startTime, endTime, segments, products, image, banner, userID });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Cập nhật Store (VIEW, API)
             * Date: 20/06/2021
             * Dev: Depv
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.UPDATE_TRIAL_PROGRAM]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    title: 'Update Trial Program - Nandio',
                    type: 'view',
                    view: 'index.ejs',
                    code: CF_ROUTINGS_TRIAL_PROGRAM.UPDATE_TRIAL_PROGRAM,
                    inc: path.resolve(__dirname, '../views/update_trial_program.ejs')
                },
                methods: {
                    post: [ async function (req, res) {
                        let { trialProgramID, title, description, content, startTime, endTime, segments, products, image, banner } = req.body
                        let infoAfterUpdate = await TRIAL_PROGRAM_MODEL.update({ trialProgramID, title, description, content, startTime, endTime, segments, products, image, banner });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
             * Function: Xóa Store (API)
             * Date: 20/06/2021
             * Dev: Depv 
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.REMOVE_TRIAL_PROGRAM]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { trialProgramID } = req.params;
                        const infoAfterDelete = await TRIAL_PROGRAM_MODEL.delete({ trialProgramID });
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Xóa Store (API)
             * Date: 20/06/2021
             * Dev: Depv
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.UPDATE_TRIAL_PROGRAM_STATUS]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        const { trialProgramID, status } = req.body;
                        const infoAfterUpdate = await TRIAL_PROGRAM_MODEL.updateStatus({ trialProgramID, status });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
             * Function: Xem chi tiết trial program (API)
             * Date: 02/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.INFO_TRIAL_PROGRAM_FOR_ADMIN]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { trialProgramID } = req.params;
                        const infoTrialProgram = await TRIAL_PROGRAM_MODEL.getInfoForAdmin({ trialProgramID });
                        res.json(infoTrialProgram);
                    }]
                },
            },

            /**
             * Function: Xem chi tiết Store (API)
             * Date: 02/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.INFO_TRIAL_PROGRAM_FOR_ENDUSER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { trialProgramID } = req.params;
                        const infoTrialProgram = await TRIAL_PROGRAM_MODEL.getInfoForEndUser({ trialProgramID });
                        res.json(infoTrialProgram);
                    }]
                },
            },
            /**
             * Function: Lấy thông tin trial program hiện tại
             * Dev: DePV
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.INFO_TRIAL_PROGRAM_CURRENT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { deviceID } = req.params;
                        const infoTrialProgram = await TRIAL_PROGRAM_MODEL.getInfoTrialProgramCurrent({ deviceID });
                        res.json(infoTrialProgram);
                    }]
                },
            },

             /**
             * Function: Lấy thông tin trial program hiện tại
             * Dev: DePV
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.INFO_TRIAL_PROGRAM_CURRENT_FOR_ADMIN]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const infoTrialProgram = await TRIAL_PROGRAM_MODEL.getInfoTrialProgramCurrentForAdmin();
                        res.json(infoTrialProgram);
                    }]
                },
            },

             /**
             * Function: Lấy thông tin trial program hiện tại cho enduser
             * Dev: DePV
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.INFO_TRIAL_PROGRAM_CURRENT_FOR_ENDUSER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const infoTrialProgram = await TRIAL_PROGRAM_MODEL.getInfoTrialProgramCurrentForAdmin();
                        res.json(infoTrialProgram);
                    }]
                },
            },

            

            /**
             * Function:Update status trial program
             * Dev: DePV
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.UPDATE_STATUS_VIEW_TRIAL_PROGRAM]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const {  trialProgramID, deviceID, status } = req.body;
                        const infoAfterHandel = await VIEW_TRIAL_PROGRAM_MODEL.updateStatus({ trialProgramID, deviceID, status });
                        res.json(infoAfterHandel);
                    }]
                },
            },
            /**
             * Function: user đăng ký trial program
             * Dev: DePV
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.USER_REGISTER_TRIAL_PROGRAM]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: customerID } = req.customer;
                        const { trialProgramID, deviceID, productID, fullname, email, phone, address, fields, cityNumber, districtNumber, wardNumber, cityText, districtText, wardText, fullAddress } = req.body;
                        const infoAfterHandel = await USER_REGISTER_TRIAL_PROGRAM_MODEL.insert({ customerID, trialProgramID, deviceID, productID, fullname, email, phone, address, fields, cityNumber, districtNumber, wardNumber, cityText, districtText, wardText, fullAddress });
                        res.json(infoAfterHandel);
                    }]
                },
            },
            
            /**
             * Function: Lấy thông tin user đăng ký trial program
             * Dev: DePV
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.INFO_USER_REGISTER_TRIAL_PROGRAM]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { registerTrialProgramID } = req.params;
                        const infoAfterHandel = await USER_REGISTER_TRIAL_PROGRAM_MODEL.getInfo({ registerTrialProgramID  });
                        res.json(infoAfterHandel);
                    }]
                },
            },

            /**
             * Function: Cập nhật thông tin user đăng ký trial program
             * Dev: DePV
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.UPDATE_USER_REGISTER_TRIAL_PROGRAM]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const {  registerTrialProgramID, fullname, email, phone, address, status, cityNumber, districtNumber, wardNumber, cityText, districtText, wardText, fullAddress, note } = req.body;
                        const infoAfterHandel = await USER_REGISTER_TRIAL_PROGRAM_MODEL.update({  registerTrialProgramID, fullname, email, phone, address, status, cityNumber, districtNumber, wardNumber, cityText, districtText, wardText, fullAddress, note  });
                        res.json(infoAfterHandel);
                    }]
                },
            },

            /**
             * Function: Thêm fields config cho đăng ký trialprogram
             * Dev: DePV
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.ADD_FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { trialProgramID, title, type, values  } = req.body;
                        const infoAfterHandel = await FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.insert({ trialProgramID, title, type, values });
                        res.json(infoAfterHandel);
                    }]
                },
            },

            /**
             * Function: Thông tin fields config cho đăng ký trialprogram
             * Dev: DePV
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.INFO_FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { fieldID  } = req.params;
                        const infoAfterHandel = await FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getInfo({ fieldID });
                        res.json(infoAfterHandel);
                    }]
                },
            },

            /**
             * Function: Cập nhập fields config cho đăng ký trialprogram
             * Dev: DePV
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.UPDATE_FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { fieldID, title, type, values  } = req.body;
                        const infoAfterHandel = await FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.update({ fieldID, title, type, values });
                        res.json(infoAfterHandel);
                    }]
                },
            },

            /**
             * Function: Xóa fields config cho đăng ký trialprogram
             * Dev: DePV
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.REMOVE_FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM]: {
                config: {
                    auth: [ roles.role.editer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { fieldID } = req.params;
                        const infoAfterHandel = await FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.remove({ fieldID });
                        res.json(infoAfterHandel);
                    }]
                },
            },

            /**
             * Function: list fields config cho đăng ký trialprogram
             * Dev: DePV
             */
            [CF_ROUTINGS_TRIAL_PROGRAM.LIST_FIELD_REGISTER_TRIAL_PROGRAM_FOR_ENDUSER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { trialProgramID } = req.params;
                        const listFields = await FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_MODEL.getList({ trialProgramID });
                        res.json(listFields);
                    }]
                },
            },

             /**
             * Function: list fields config cho đăng ký trialprogram
             * Dev: DePV
             */
              [CF_ROUTINGS_TRIAL_PROGRAM.API_LIST_TRIAL_PROGRAM_ADMIN]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let listTrialProgram = await TRIAL_PROGRAM_MODEL.getListByAdmin();
                        res.json(listTrialProgram);
                    }]
                },
            },

        }
    }
};