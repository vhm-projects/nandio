"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                        = require('moment');

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { loadPathImage } 			= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const TRIAL_PROGRAM_COLL  		    = require('../databases/trial_program-coll');
const IMAGE_MODEL                   = require('../../image/models/image').MODEL;
const VIEW_TRIAL_PROGRAM_COLL  		= require('../databases/view_trial_program-coll');
const USER_SEGMENT_COLL  		    = require('../../product/databases/user_segment-coll');
const USER_DEVICE_COLL  		    = require('../../customer/databases/user_device-coll');
const NOTIFICATION_MODEL  		    = require('../../notification/models/notification').MODEL;
const VIEW_TRIAL_PROGRAM_MODEL  	= require('../models/view_trial_program').MODEL;


class Model extends BaseModel {
    constructor() {
        super(TRIAL_PROGRAM_COLL);
    }

	insert({ title, description, content, startTime, endTime, segments, products, image, banner, userID }) {
        return new Promise(async resolve => {
            try {
                if(!title || !startTime || !endTime ||!products)
                    return resolve({ error: true, message: 'params_invalid' });

                let dataInsert = { title, startTime, endTime, products, userCreate: userID };

                if(description){
                    dataInsert.description = description;
                }

                if(content){
                    dataInsert.content = content;
                }

                if(segments){
                    dataInsert.segments = segments;
                }

                if(image){
                    let { name, size, path } = image;
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path
                    });
                    dataInsert.image = infoImageAfterInsert.data._id;
                }

                if(banner){
                    let { name, size, path } = banner;
                    let infoBannerAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path
                    });
                    dataInsert.banner = infoBannerAfterInsert.data._id;
                }

                // Check trial program có đang hoạt động không
                let isExistTrialProgram = await TRIAL_PROGRAM_COLL.findOne({
                    status: 1,
                    startTime: {
                        $lte: moment()
                    },
                    endTime: {
                        $gte: moment().endOf('day').format()
                    }
                })
                
                if(isExistTrialProgram)
                    return resolve({ error: true, message: 'Hiện tại đang có 1 trial program đang hoạt động' });
                
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_trial_program_failed' });

                // Gửi notification tới reviceID
                // Khánh nói đóng lại, sau này sẽ có 1 luồng thông báo riêng
                let listUserDevice = await USER_DEVICE_COLL.find();
                listUserDevice = listUserDevice.filter(device => device.customer);
                const TYPE_TRIAL_PROGRAM = 2;
                if (listUserDevice && listUserDevice.length) {
                    listUserDevice = listUserDevice.map(device => {
                        return {
                            title:       title, 
                            description: description, 
                            type:        TYPE_TRIAL_PROGRAM, 
                            receive:     device.customer,
                            // centralize_order: infoCentralizeOrderAfterUpdate._id,
                            trialProgram: infoAfterInsert._id,
                            createAt:    new Date(),
                            modifyAt:    new Date(),
                        }
                    });
                    console.log({
                        listUserDevice
                    });
                    let infoNotificationAFterInsert = await NOTIFICATION_MODEL.insertV2({ 
                        listNotification: listUserDevice
                    });
                }

                // let listCustomersReciverNotification = [];
                // if(segments){
                //     for (const segment of segments) {
                //         let listCustomers = await USER_SEGMENT_COLL.find({ segment }).select("user");
                //         let listCustomersID = listCustomers.map(item => item.user.toString());
                //         listCustomersReciverNotification = [...listCustomersReciverNotification, listCustomersID ]
                //     }
                // }

                // let listCustomersReciverNotificationUnique = Array.from(new Set(listCustomersReciverNotification));
                // let typeNotificationTrialProgram = 2;
                // let trialProgramID = infoAfterInsert._id;
                // if(listCustomersReciverNotificationUnique.length){
                //     for (const customersID of listCustomersReciverNotificationUnique) {
                //         let infoDevice = await USER_DEVICE_COLL.findOne({ customer: customersID, deviceID: { $exists: true } });
                //         if(infoDevice){
                //             let userDevice = infoDevice._id;
                //             let deviceID = infoDevice.deviceID;

                //             // Tạo thông báo có trial program cho user
                //             await NOTIFICATION_MODEL.insert({ 
                //                 title, description, content, type: typeNotificationTrialProgram, trialProgram: trialProgramID, sender: userID, userDevice, deviceID 
                //             });

                //             // User xem trial program để hiển thị popup
                //             await VIEW_TRIAL_PROGRAM_MODEL.insert({ trialProgramID, deviceID });
                //         }
                //     }
                // }else{
                //     let listUserDevice = await USER_DEVICE_COLL.find();
                //     for (const infoDevice of listUserDevice) {
                //         if(infoDevice){
                //             let userDevice = infoDevice._id;
                //             let deviceID = infoDevice.deviceID;

                //             // Tạo thông báo có trial program cho user
                //             await NOTIFICATION_MODEL.insert({ 
                //                 title, description, content, type: typeNotificationTrialProgram, trialProgram: trialProgramID, sender: userID, userDevice, deviceID 
                //             });

                //             // User xem trial program để hiển thị popup
                //             await VIEW_TRIAL_PROGRAM_MODEL.insert({ trialProgramID, deviceID });
                //         }
                //     }
                // }
                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ trialProgramID, title, description, content, startTime, endTime, segments, products, image, banner }) {
        return new Promise(async resolve => {
            try {
                if(!trialProgramID || !title || !startTime || !endTime ||!products)
                    return resolve({ error: true, message: 'params_invalid' });
                let dataUpdate = { title, startTime, endTime, products };

                if(description){
                    dataUpdate.description = description;
                }

                if(content){
                    dataUpdate.content = content;
                }
                
                if(segments){
                    dataUpdate.segments = segments;
                }

                if(image){
                    let { name, size, path } = image;
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path
                    });
                    dataUpdate.image = infoImageAfterInsert.data._id;
                }

                if(banner){
                    let { name, size, path } = banner;
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({
                        name, size, path
                    });
                    dataUpdate.banner = infoImageAfterInsert.data._id;
                }
                
                let infoAfterUpdate = await TRIAL_PROGRAM_COLL.findByIdAndUpdate(trialProgramID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'update_trial_program_failed' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	delete({ trialProgramID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(trialProgramID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterDelete = await TRIAL_PROGRAM_COLL.findByIdAndUpdate(trialProgramID, { status: 2 }, { new: true });
                if(!infoAfterDelete)
                    return resolve({ error: false, message: 'cannot_remove_store' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateStatus({ trialProgramID, status }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(trialProgramID))
                    return resolve({ error: true, message: 'params_invalid' });
                if(status == 1){
                    // Check trial program có đang hoạt động không
                    let isExistTrialProgram = await TRIAL_PROGRAM_COLL.findOne({
                        _id: { $ne: trialProgramID}, 
                        status: 1,
                        startTime: {
                            $lte: moment()
                        },
                        endTime: {
                            $gte: moment().endOf('day').format()
                        }
                    })
                    if(isExistTrialProgram)
                    return resolve({ error: true, message: 'Hiện tại đang có 1 trial program đang hoạt động' });
                }

                let infoAfterDelete = await TRIAL_PROGRAM_COLL.findByIdAndUpdate(trialProgramID, { status }, { new: true });
                if(!infoAfterDelete)
                    return resolve({ error: false, message: 'cannot_remove_store' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoForEndUser({ trialProgramID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(trialProgramID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoTrialProgram = await TRIAL_PROGRAM_COLL.findOne({ _id: trialProgramID, status: 1})
                                    .select("title description image startTime endTime createAt products content")
                                    .populate({
                                        path: "image",
                                        select: "path"
                                    })
                                    .populate({
                                        path: "products",
                                        select: "avatar name price",
                                        populate: {
                                            path: "avatar",
                                            select: "path"
                                        }
                                    })
                if(!infoTrialProgram)
                    return resolve({ error: false, message: 'cannot_get_trial_program' });
              
                return resolve({ error: false, data: infoTrialProgram });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoForAdmin({ trialProgramID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(trialProgramID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoTrialProgram = await TRIAL_PROGRAM_COLL.findById(trialProgramID).populate({
                    path: "products",
                    select: "name"
                })
                .populate({
                    path: "segments",
                    select: "name"
                });
                if(!infoTrialProgram)
                    return resolve({ error: false, message: 'cannot_get_trial_program' });
              
                return resolve({ error: false, data: infoTrialProgram });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListByAdmin(){
		return new Promise(async resolve => {
			try {
                let listTrialProgram = await TRIAL_PROGRAM_COLL.find({ status: { $ne: 2 }})
                    .populate({
                        path: "image banner",
                        select: "path"
                    })
					.sort({ modifyAt: -1 });

                return resolve({ 
                    error: false, 
                    data: listTrialProgram
                });

				// let listTrialProgramActive = await TRIAL_PROGRAM_COLL.find({
                //         status: 1,
                //         startTime: {
                //             $lte: moment()
                //         },
                //         endTime: {
                //             $gte: moment().endOf('day').format()
                //         }
                //     })
                //     .populate({
                //         path: "image",
                //         select: "path"
                //     })
				// 	.sort({ modifyAt: -1 });

                // let arrTrialProgramIDActive = listTrialProgramActive.map(trialProgram => trialProgram._id);

                // let listTrialProgramInActive = await TRIAL_PROGRAM_COLL.find({ 
                //     status: { $ne: 2 },
                //     _id: { $nin: arrTrialProgramIDActive }
                // }).populate({
                //     path: "image",
                //     select: "path"
                // })
                // .sort({ modifyAt: -1 });

                // if(!listTrialProgramActive)
                //     return resolve({ error: true, message: 'cannot_get_list_store' });
				// return resolve({ 
				// 	error: false, 
				// 	data: { listTrialProgramActive, listTrialProgramInActive }
				// });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

    getInfoTrialProgramCurrent({ deviceID }) {
        return new Promise(async resolve => {
            try {

                let infoTrialProgramCurrent = await TRIAL_PROGRAM_COLL.findOne({
                    status: 1,
                    startTime: {
                        $lte: moment()
                    },
                    endTime: {
                        $gte: moment().endOf('day').format()
                    }
                })
                .select("title description image startTime endTime createAt products content")
                .populate({
                    path: "image",
                    select: "path"
                })
                .populate({
                    path: "products",
                    select: "avatar name price",
                    populate: {
                        path: "avatar",
                        select: "path"
                    }
                }).sort({
                    createAt: -1
                })

                if(!infoTrialProgramCurrent)
                    return resolve({ error: true, message: "cannot_get_trial_program" });

                let checkPermisionAcceptTrialProgram = await VIEW_TRIAL_PROGRAM_COLL.findOne({ trialProgram: infoTrialProgramCurrent._id, deviceID });
                if(!checkPermisionAcceptTrialProgram)
                    return resolve({ error: true, message: "deviceID_no_premision_access_trialprogram" });

                let checkDeviceSeenTrialprogram = await VIEW_TRIAL_PROGRAM_COLL.findOne({ trialProgram: infoTrialProgramCurrent._id, deviceID, status: { $ne: -1 } });
                if(checkDeviceSeenTrialprogram)
                    return resolve({ error: true, message: "deviceID_seen_trialprogram" });

                return resolve({ error: false, data: infoTrialProgramCurrent });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoTrialProgramCurrentForAdmin() {
        return new Promise(async resolve => {
            try {

                let infoTrialProgramCurrent = await TRIAL_PROGRAM_COLL.findOne({
                    status: 1,
                    startTime: {
                        $lte: moment()
                    },
                    endTime: {
                        $gte: moment().endOf('day').format()
                    }
                })
                .select("title description image banner startTime endTime createAt products content")
                .populate({
                    path: "image banner",
                    select: "path"
                })
                .populate({
                    path: "products",
                    select: "avatar name price",
                    populate: {
                        path: "avatar",
                        select: "path"
                    }
                }).sort({
                    createAt: -1
                })

                if(!infoTrialProgramCurrent)
                    return resolve({ error: true, message: "cannot_get_trial_program" });

                return resolve({ error: false, data: infoTrialProgramCurrent });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoTrialProgramCurrentV2() {
        return new Promise(async resolve => {
            try {

                let infoTrialProgramCurrent = await TRIAL_PROGRAM_COLL.findOne({
                    status: 1,
                    startTime: {
                        $lte: moment()
                    },
                    endTime: {
                        $gte: moment().endOf('day').format()
                    }
                })
                .select("title description image startTime endTime createAt products content")
                .populate({
                    path: "image",
                    select: "path"
                })
                .populate({
                    path: "products",
                    select: "avatar name price",
                    populate: {
                        path: "avatar",
                        select: "path"
                    }
                })
                // if(!infoTrialProgramCurrent)
                //     return resolve({ error: true, message: "cannot_get_trial_program" });

                // let checkPermisionAcceptTrialProgram = await VIEW_TRIAL_PROGRAM_COLL.findOne({ trialProgram: infoTrialProgramCurrent._id, deviceID });
                // if(!checkPermisionAcceptTrialProgram)
                //     return resolve({ error: true, message: "deviceID_no_premision_access_trialprogram" });

                // let checkDeviceSeenTrialprogram = await VIEW_TRIAL_PROGRAM_COLL.findOne({ trialProgram: infoTrialProgramCurrent._id, deviceID, status: { $ne: -1 } });
                // if(checkDeviceSeenTrialprogram)
                //     return resolve({ error: true, message: "deviceID_seen_trialprogram" });

                return resolve({ error: false, data: infoTrialProgramCurrent });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}
exports.MODEL = new Model;
