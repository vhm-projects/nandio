"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                        = require('moment');

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { loadPathImage } 			= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const VIEW_TRIAL_PROGRAM_COLL  		    = require('../databases/view_trial_program-coll');
const TRIAL_PROGRAM_COLL  		        = require('../databases/trial_program-coll');

class Model extends BaseModel {
    constructor() {
        super(VIEW_TRIAL_PROGRAM_COLL);
    }

	insert({ trialProgramID, deviceID }) {
        return new Promise(async resolve => {
            try {
                if(!trialProgramID || !deviceID)
                    return resolve({ error: true, message: 'params_invalid' });

                let dataInsert = { trialProgram: trialProgramID , deviceID };
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_view_trial_program_failed' });
                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    updateStatus({ trialProgramID, deviceID, status }) {
        return new Promise(async resolve => {
            try {
                if(!trialProgramID || !deviceID)
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await VIEW_TRIAL_PROGRAM_COLL.findOneAndUpdate({ trialProgram: trialProgramID, deviceID }, { status }, { upsert: true, new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'update_view_trial_program_failed' });
                // Cập nhật số lường người đã xem trial program
                if(status == 1){
                    await TRIAL_PROGRAM_COLL.findByIdAndUpdate(trialProgramID, { $inc: { views: 1 } });
                }
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}
exports.MODEL = new Model;
