"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                        = require('moment');

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { loadPathImage } 			= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const USER_FIELDS_REGISTER_TRIAL_PROGRAM_COLL = require('../databases/user_fields_register_trial_program-coll');
const TRIAL_PROGRAM_COLL               = require('../databases/trial_program-coll');

class Model extends BaseModel {
    constructor() {
        super(USER_FIELDS_REGISTER_TRIAL_PROGRAM_COLL);
    }
    
	insert({ fieldID, value }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(fieldID) || !value )
                    return resolve({ error: true, message: 'params_invalid' });
                
                let dataInsert = { field: fieldID , value };
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_view_trial_program_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
}
exports.MODEL = new Model;
