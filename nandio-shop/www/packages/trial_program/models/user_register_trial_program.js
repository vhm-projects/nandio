"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                        = require('moment');

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { loadPathImage } 			= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const USER_REGISTER_TRIAL_PROGRAM_COLL = require('../databases/user_register_trial_program-coll');
const TRIAL_PROGRAM_COLL               = require('../databases/trial_program-coll');

const USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL = require('../models/user_fields_register_trial_program').MODEL;
const CENTRALIZE_ORDER_MODEL                   = require('../../order/models/centralize_order').MODEL;
const CUSTOMER_MODEL                           = require('../../customer/models/customer').MODEL;
const NOTIFICATION_MODEL                       = require('../../notification/models/notification').MODEL;

class Model extends BaseModel {
    constructor() {
        super(USER_REGISTER_TRIAL_PROGRAM_COLL);
    }

	insert({ customerID, trialProgramID, deviceID, productID, fullname, email, phone, address, fields, cityNumber, districtNumber, wardNumber, cityText, districtText, wardText, fullAddress }) {
        return new Promise(async resolve => {
            try {
                if(!trialProgramID || !deviceID || !productID || !fullname || !email || !phone || !address)
                    return resolve({ error: true, message: 'params_invalid' });
                
                let checkExist = await USER_REGISTER_TRIAL_PROGRAM_COLL.findOne({ trialProgram:trialProgramID, deviceID, product: productID });
                if(checkExist)
                    return resolve({ error: true, message: 'Bạn đã đăng ký dùng thử sản phẩm này' });
                let arrFieldsID = [];

                let customer = '';
                console.log({
                    customerID
                });
                let OLD_CUSTOMER = false;
                if (customerID) {
                    OLD_CUSTOMER = true;
                    customer = customerID;
                } else {
                    let infoCustomerAfterInsert = await CUSTOMER_MODEL.insert__TrialProgram({ fullname, email, phone, address: fullAddress });
                    console.log({
                        infoCustomerAfterInsert
                    });
                    if (infoCustomerAfterInsert.error) {
                        return resolve(infoCustomerAfterInsert);
                    }
                    customer = infoCustomerAfterInsert.data._id;
                }

                if(fields && fields.length){
                    for (const field of fields) {
                        let { fieldID, value }  = field;
                        let infoUserField = await USER_FIELDS_REGISTER_TRIAL_PROGRAM_MODEL.insert({ fieldID, value });
                        arrFieldsID.push(infoUserField.data._id);
                    }
                }

                let dataInsert = { trialProgram: trialProgramID , deviceID, product: productID, fullname, email, phone, address, fields: arrFieldsID, cityNumber, districtNumber, wardNumber, cityText, districtText, wardText, fullAddress };
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_view_trial_program_failed' });
                // Update số lượng người dùng đăng ký trial program
                await TRIAL_PROGRAM_COLL.findByIdAndUpdate(trialProgramID, { $inc: { registers: 1 }})

                // LƯU VÀO QUẢN LÝ TẬP TRUNG
				const TYPE_TRIAL = 2;
				let conditionObj__CentralizeOrder = {
					type: infoAfterInsert._id, 
					onModel: 'user_register_trial_program',
					customer: customer,
					// onModelCustomer: 'user_register_trial_program',
					onModelCustomer: 'customer',
					address, ward: wardNumber, district: districtNumber, city: cityNumber, cityText, wardText, districtText,
					products: [{
                        product: productID,
                        quantities: 1,
                        onModel: 'product'
                    }],
					kind: TYPE_TRIAL
				}
               
				let infoAfterInsertCentralizeOrder = await CENTRALIZE_ORDER_MODEL.insert(conditionObj__CentralizeOrder);
				if (infoAfterInsertCentralizeOrder.error) {
					return resolve(infoAfterInsertCentralizeOrder);
				}

                if (customerID) {
                    const TYPE_TRIAL_PROGRAM = 2;
                    let infoNotificationAFterInsert = await NOTIFICATION_MODEL.insertV2({ 
                        listNotification: [{
                            title:       'Đăng ký dùng thử thành công', 
                            description: 'Bạn đã đăng ký dùng thử sản phẩm thành công', 
                            type:        TYPE_TRIAL_PROGRAM, 
                            receive:     customerID,
                            screen_key:  'HistoryOrderScreenNew',
                            // centralize_order: infoCentralizeOrderAfterUpdate._id,
                            trialProgram: trialProgramID,
                            createAt:    new Date(),
                            modifyAt:    new Date(),
                        }]
                    });
                }
                

                return resolve({ error: false, data: infoAfterInsert, OLD_CUSTOMER });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    update({ registerTrialProgramID, fullname, email, phone, address, status, note, cityNumber, districtNumber, wardNumber, cityText, districtText, wardText, fullAddress }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(registerTrialProgramID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await USER_REGISTER_TRIAL_PROGRAM_COLL.findByIdAndUpdate(registerTrialProgramID, {
                    fullname, email, phone, address, status, note, cityNumber, districtNumber, wardNumber, cityText, districtText, wardText, fullAddress
                }, { new: true });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ registerTrialProgramID }) {   
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(registerTrialProgramID))
                    return resolve({ error: true, message: 'params_invalid' });
                
                let infoRegisterTrialProgram = await USER_REGISTER_TRIAL_PROGRAM_COLL.findById(registerTrialProgramID)
                                                .populate({
                                                    path: "product",
                                                    select: "name",
                                                })
                                                .populate({
                                                    path: "trialProgram",
                                                    select: "title",
                                                })
                                                .populate({
                                                    path: "fields",
                                                    populate: {
                                                        path: "field",
                                                    }
                                                });
                if(!infoRegisterTrialProgram)
                    return resolve({ error: true, message: 'Cannot_get_info_register_trial_program' });

                return resolve({ error: false, data: infoRegisterTrialProgram });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListForAdmin({ trialProgramID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(trialProgramID))
                    return resolve({ error: true, message: 'params_invalid' });
                let listData = await USER_REGISTER_TRIAL_PROGRAM_COLL.find({ trialProgram: trialProgramID }).populate({
                    path: "product",
                    select: "name",
                })
                .populate({
                    path: "fields",
                    populate: {
                        path: "field",
                    }
                }).sort({
                    createAt: -1
                });
                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}
exports.MODEL = new Model;
