"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const moment                        = require('moment');

/**
 * BASE
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { loadPathImage } 			= require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_COLL = require('../databases/field_config_register_trial_program-coll');
const TRIAL_PROGRAM_COLL               = require('../databases/trial_program-coll');

class Model extends BaseModel {
    constructor() {
        super(FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_COLL);
    }

	insert({ trialProgramID, title, type, values }) {
        return new Promise(async resolve => {
            try {
                
                if(!trialProgramID || !title || !type)
                    return resolve({ error: true, message: 'params_invalid' });

                let dataInsert = { trialProgram: trialProgramID , title, type, values };
                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_fields_config_trial_program_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ fieldID, title, type, values }) {
        return new Promise(async resolve => {
            try {
                
                if(!fieldID || !title || !type)
                    return resolve({ error: true, message: 'params_invalid' });

                let dataUpdate = { title, type, values };
                let infoAfterUpdate = await FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_COLL.findByIdAndUpdate(fieldID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'insert_fields_config_trial_program_failed' });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    getInfo({ fieldID }) {
        return new Promise(async resolve => {
            try {
                if(!fieldID)
                    return resolve({ error: true, message: 'params_invalid' });

                let infoData = await FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_COLL.findById(fieldID);
                if(!infoData)
                    return resolve({ error: true, message: 'connot_get_data' });

                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ trialProgramID }) {
        return new Promise(async resolve => {
            try {
                if(!trialProgramID)
                    return resolve({ error: true, message: 'params_invalid' });
                let listData = await FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_COLL.find({ trialProgram: trialProgramID, status: 1 });
                if(!listData)
                    return resolve({ error: true, message: 'connot_get_data' });

                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    remove({ fieldID }) {
        return new Promise(async resolve => {
            try {
                if(!fieldID)
                    return resolve({ error: true, message: 'params_invalid' });

                let infoDataAfterUpdate = await FIELDS_CONFIG_REGISTER_TRIAL_PROGRAM_COLL.findByIdAndUpdate(fieldID, { status: 2 }, { new: true });
                if(!infoDataAfterUpdate)
                    return resolve({ error: true, message: 'connot_get_data' });

                return resolve({ error: false, data: infoDataAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    
}
exports.MODEL = new Model;
