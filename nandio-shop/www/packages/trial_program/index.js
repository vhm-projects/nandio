const TRIAL_PROGRAM_MODEL    = require('./models/trial_program').MODEL;
const TRIAL_PROGRAM_COLL     = require('./databases/trial_program-coll');
const TRIAL_PROGRAM_ROUTES   = require('./apis/trial_program');

module.exports = {
    TRIAL_PROGRAM_ROUTES,
    TRIAL_PROGRAM_COLL,
    TRIAL_PROGRAM_MODEL,
}