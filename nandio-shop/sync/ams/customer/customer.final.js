let CUSTOMER_TEMP_COLL               = require('../../../www/packages/customer/databases/customer_temp-coll');
let CUSTOMER_COLL                    = require('../../../www/packages/customer/databases/customer-coll');
const mongodb                   = require("mongodb").MongoClient;
const path                      = require("path");
const fs                        = require("fs");
const ObjectID                        = require("mongoose").Types.ObjectId;

const { provinces }                                 = require('../../../www/packages/common/constants/provinces');
const { districts }                                 = require('../../../www/packages/common/constants/districts');
// const { provinces }                                 = require('../../../www/packages/common/constants/wards');
let url = "mongodb://localhost:27017";

async function run() {
    let listCustomer            = await CUSTOMER_TEMP_COLL.find({}).sort({ _id: -1 }).lean();
    let infoAferUpdateAll = [];
    let index = 0;
    for (const customer of listCustomer) {
        console.log(`customer index with -_id: ${index}, _id: ${customer && customer._id}`)
        index++;

        const {
            name, phone, point, isDelete
        } = customer;
    

        if (phone && phone.length ) {
            let itemForInsert = {
                fullname: name,
                phone,
                status: Number(!isDelete),

                // hiện tại lấy điểm apply cho cả 2
                pointRanking: point,
                point: point
            }
            infoAferUpdateAll = [
                ...infoAferUpdateAll, itemForInsert
            ]
        }
    }

    return infoAferUpdateAll;
}

run()
    .then(listCustomerConverted => {
        console.log({ listCustomerConverted: listCustomerConverted.length })
        mongodb.connect(
            url,
            { useNewUrlParser: true, useUnifiedTopology: true },
            (err, client) => {
              console.log({ err, client });
              if (err) throw err;
              client
                .db("nandio")
                .collection("customer_final")
                .insertMany(listCustomerConverted, (err, res) => {
                  if (err) throw err;
        
                  console.log(`Inserted: ${res.insertedCount} rows`);
                  client.close();
                });
            }
        );
    })
    .catch(error => console.log({ error }))