// import khu phố
const fs                            = require("fs");
const mongodb                       = require("mongodb").MongoClient;
const fastcsv                       = require("fast-csv");
const ObjectID                      = require('mongoose').Types.ObjectId;

// let url = "mongodb://username:password@localhost:27017/";
let url = "mongodb://localhost:27017";
let stream = fs.createReadStream('./data.nandio.csv');
let csvData = [];

let numberRow = 0;
let csvStream = fastcsv
  .parse()
  .on("data", function(data) {
    console.log(`numberRow: ${numberRow}`);
    numberRow++;
    
    let infoCustomer = {};

    let name        = data[3];
    let phone       = data[4];
    let point       = data[5];
    let isDelete    = data[6];

    if (name && phone) {
        csvData = [
            ...csvData,
            {
                name, 
                phone, 
                point: Number(point), 
                isDelete: parseInt(isDelete)
            }
        ]
    }
  })
  .on("end", async function() {
    // remove the first line: header
    // csvData.shift();
    console.log(`-----------------------------`)
    console.log({ csvData })
    console.log({ LENGTH: csvData.length })
    console.log(`-----------------------------`)

    mongodb.connect(
      url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err, client) => {
        console.log({ err, client });
        if (err) throw err;
        client
          .db("nandio")
          .collection("customer_import")
          .insertMany(csvData, (err, res) => {
            if (err) throw err;

            console.log(`Inserted: ${res.insertedCount} rows`);
            client.close();
          });
      }
    );
  });

stream.pipe(csvStream);