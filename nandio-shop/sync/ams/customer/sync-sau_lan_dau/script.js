const CUSTOMER_COLL             = require('../../../../www/packages/customer/databases/customer-coll');
const queryString               = require('query-string');
const axios                     = require('axios');

async function run() {
    let listCustomerNotSync            = await CUSTOMER_COLL.find({
        password: {
            $exists: true
        }
    }).sort({ _id: -1 }).lean();
    let index = 0;
    for (const customerNotSyncToAMS of listCustomerNotSync) {
        console.log(`customerNotSyncToAMS index with -_id: ${index}, _id: ${customerNotSyncToAMS && customerNotSyncToAMS._id}`)
        index++;

        const {
            fullname,
            email,
            phone,
            birthday,
            gender,
        } = customerNotSyncToAMS;
        console.log( {
            fullname,
            email,
            phone,
            birthday,
            gender,
        })
        let objStringQuery = {
            r: 'api/taomoikhachhang',
            DTDD: phone,
            store_id: '1006',
            loai_ca: '1',
            full_name: fullname,
            email: email,
            p1: 'crm',
            p2: '123456',
            p3: '4PBagn81jWU3ouPVt4OeqzDeYrU7iYH9',
            createbyuser: '276'
        }
        if (gender) {
            objStringQuery.gender = gender;
        } else {
            objStringQuery.gender = 1;
        };
       
        objStringQuery.DiaChi = 'SHOPER';
        
        const stringified = queryString.stringify(objStringQuery);
        
        // ĐỒNG BỘ SANG DMS
        var options = {
          'method': 'post',
            url: `http://dms1.yensaothienviet.vn:8680/thsams/web/index.php?${stringified}`
            // `http://dms1.yensaothienviet.vn:8680/thsams/web/index.php?r=api%2Ftaomoikhachhang&DTDD=0928581121&store_id=1006&loai_ca=1&full_name=KHANH&email=lucdeit121358@gmail.com&p1=crm&p2=123456&p3=4PBagn81jWU3ouPVt4OeqzDeYrU7iYH9&createbyuser=276&DiaChi=120`
            
        };
        // console.log({
        //     ['objStringQuery']: objStringQuery.search
        // })
        let response = await axios(options);
        console.log({ index, fullname, message: JSON.stringify(response.data) })
    }
}

run()
    .then(_ => {
        console.log({ message: 'ok' })
    })
    .catch(error => console.log({ error }))

