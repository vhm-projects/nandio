let PARCEL_TEMP_COLL              = require('../../../www/packages/sku-2/databases/parcel_import-coll.js');
let PARCEL_COLL                   = require('../../../www/packages/sku-2/databases/parcel-coll');
let SKU_2_COLL                    = require('../../../www/packages/sku-2/databases/sku_2-coll');
let PRODUCT_QR_COLL  			  = require('../../../www/packages/product_qr/databases/product_qr-coll');

const mongodb                   = require("mongodb").MongoClient;
const path                      = require("path");
const fs                        = require("fs");
const ObjectID                  = require("mongoose").Types.ObjectId;
const moment                    = require('moment');
// const { provinces }                                 = require('../../../www/packages/common/constants/wards');
let url = "mongodb://localhost:27017";

function formatDate(date) {
    let arrayDate       = date.split(' ');
    let dateNotHaveTime = new Date(arrayDate[0]);
    let arrayHour = arrayDate[1].split(':');
    let dateHaveTime = dateNotHaveTime.setHours(Number(arrayHour[0]), Number(arrayHour[1]));
    return new Date(dateHaveTime);
}

async function run() {
    let listParcel            = await PARCEL_TEMP_COLL.find({})
    let infoAferUpdateAll = [];
    let index = 0;
    for (const parcel of listParcel) {
        console.log(`parcel index with -_id: ${index}, _id: ${parcel && parcel._id}`)
        index++;

        const {
            SKU, UNITID, ADDDATE, COMMAND, LOTTABLE04, LOTTABLE05
        } = parcel;
        
        // let convertLOTTABLE04 = formatDa2te(LOTTABLE04);
        // console.log({
        //     SKU, UNITID, ADDDATE, COMMAND, LOTTABLE04, LOTTABLE05, 
        // });
        let checkExist = await PARCEL_COLL.findOne({ quantities_parcel: COMMAND });
        if (!checkExist) {
            let infoSku2 = await SKU_2_COLL.findOne({ name: SKU });
            if (infoSku2) {
                let infoAfterInsert = await PARCEL_COLL.create({
                    quantities_parcel: COMMAND, // số lô
                    sku_2:           infoSku2._id, // SKU 2
                    manufactureDate: new Date(LOTTABLE04), // Ngày sản xuất bbbb
                    expiryDate:      new Date(LOTTABLE05), // Hạn sử dụng
                    createAt:        new Date(ADDDATE),
                    modifyAt:        new Date(ADDDATE),
                });
                
                if (infoAfterInsert) {
                    let checkQrExist = await PRODUCT_QR_COLL.findOne({ code: UNITID });
                    if (!checkQrExist) {
                        infoAferUpdateAll = [
                            ...infoAferUpdateAll,
                            {
                                parcel: infoAfterInsert._id,
                                code: UNITID,
                                sku_2: infoSku2._id,
                            }
                        ];
                    }
                }
            }
        } else {
            let infoSku2 = await SKU_2_COLL.findOne({ name: SKU });
            if (infoSku2) {
                let checkQrExist = await PRODUCT_QR_COLL.findOne({ code: UNITID });
                if (!checkQrExist) {
                    infoAferUpdateAll = [
                        ...infoAferUpdateAll,
                        {
                            parcel: checkExist._id,
                            code: UNITID,
                            sku_2: infoSku2._id,
                        }
                    ];
                }
               
            }
        }
    }

    return infoAferUpdateAll;
}

run()
    .then(listParcelConverted => {
        console.log({ listParcelConverted: listParcelConverted.length })
        mongodb.connect(
            url,
            { useNewUrlParser: true, useUnifiedTopology: true },
            (err, client) => {
              console.log({ err, client });
              if (err) throw err;
              client
                .db("nandio_staging")
                .collection("product_qrs")
                .insertMany(listParcelConverted, (err, res) => {
                  if (err) throw err;
        
                  console.log(`Inserted: ${res.insertedCount} rows`);
                  client.close();
                });
            }
        );
    })
    .catch(error => console.log({ error }))