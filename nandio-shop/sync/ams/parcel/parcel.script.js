let PARCEL_COLL                   = require('../../../www/packages/sku-2/databases/parcel-coll');
let SKU_2_COLL                    = require('../../../www/packages/sku-2/databases/sku_2-coll');
const fs                            = require("fs");
const mongodb                       = require("mongodb").MongoClient;
const fastcsv                       = require("fast-csv");
const ObjectID                      = require('mongoose').Types.ObjectId;


// let url = "mongodb://username:password@localhost:27017/";
let url = "mongodb://localhost:27017";
let stream = fs.createReadStream('./unitid-thienviet-vo-2021-11-01-to-2021-11-05.xlsx - unitid-thienviet-vo-2021-11-01-.csv');
let csvData = [];
// console.log({
//   PARCEL_COLL
// });
// process.exit(0);

let numberRow = 0;
let LOTTABLE04__Before;
let LOTTABLE05__Before;
let csvStream = fastcsv
  .parse()
  .on("data", async function(data) {
    console.log(`numberRow: ${numberRow}`);
    numberRow++;
    
    // let infoCustomer = {};

    let SKU        = data[0]; // MÃ ĐẦU 2
    let UNITID     = data[1]; // MÃ QR
    let ADDDATE    = data[2]; // NGÀY TẠO
    // let ADDWHO     = data[3]; 
    let COMMAND    = data[4]; // SỐ LÔ
    let LOTTABLE04 = data[5]; // NSX
    let LOTTABLE05 = data[6]; // HSD

    if (!LOTTABLE04) { 
      LOTTABLE04 = LOTTABLE04__Before; // GÁN NSX BẰNG NGÀY CỦA CÁC RECORD TRƯỚC ĐÓ
    } else {
      LOTTABLE04__Before = LOTTABLE04;
    }

    if (!LOTTABLE05) {
      LOTTABLE05 = LOTTABLE05__Before; // GÁN HSD BẰNG NGÀY CỦA CÁC RECORD TRƯỚC ĐÓ
    } else {
      LOTTABLE05__Before = LOTTABLE05;
    }
    
    // if (SKU && UNITID && ADDDATE && COMMAND && LOTTABLE04 && LOTTABLE05) {
        csvData = [
            ...csvData,
            {
                SKU, 
                UNITID, 
                ADDDATE, 
                // ADDWHO, 
                COMMAND, 
                LOTTABLE04,
                LOTTABLE05
            }
        ];
    // }
  })
  .on("end", async function() {
    // remove the first line: header
    // csvData.shift();
    console.log(`-----------------------------`)
    console.log({ csvData })
    console.log({ LENGTH: csvData.length })
    console.log(`-----------------------------`)

    mongodb.connect(
      url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err, client) => {
        console.log({ err, client });
        if (err) throw err;
        client
          .db("nandio_staging")
          .collection("parcel_imports")
          .insertMany(csvData, (err, res) => {
            if (err) throw err;

            console.log(`Inserted: ${res.insertedCount} rows`);
            client.close();
          });
      }
    );
  });

stream.pipe(csvStream);