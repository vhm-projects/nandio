var request = require('request');
var mongodb = require('mongodb');
let url = "mongodb://localhost:27017";

var options = {
	'method': 'POST',
	'url': 'http://dms1.yensaothienviet.vn:8580/webresources/ios',
	'headers': {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Cookie': 'PHPSESSID=79183vm5mj4bb5igphlj0183g1'
	},
	body: '{\n    "MESSAGE":"DMS.PKG_SYNC_ASM.store_list_brand_crm",\n    "PARAMS": [\n        {"p1":"01/01/2000"}\n    ]\n}'
};

request(options, function (error, response) {
	if (error) throw new Error(error);
	let dataResp = JSON.parse(response.body);
	let RESULT_DATA     = dataResp.DATA[0].RESULT_DATA;

	let RESULT_DATAAfterConvert = RESULT_DATA.map(store => {
		return {
			code: store[0], //SHORT_CODE
			name: store[1], //CUSTOMER_NAME
			address: store[2], //ADDRESS
			province: store[3], //PROVINCE
			district: store[4], //DISTRICT
			precinct: store[5], //PRECINCT
			lat: store[6], //LAT
			lng: store[7], //LNG
			phone: store[8], //PHONE
			brandCode: store[9], //BRAND CODE
			brandName: store[10], //BRAND NAME
		}
	})

	mongodb.connect(
		url,
		{ useNewUrlParser: true, useUnifiedTopology: true },
		(err, client) => {
			console.log({ err, client });
			if (err) throw err;

			client
				.db("nandio_convert")
				.collection("store_imports")
				.insertMany(RESULT_DATAAfterConvert, (err, res) => {
					if (err) throw err;

					console.log(`Inserted: ${res.insertedCount} rows`);
					client.close();
				});
		}
	);
});
