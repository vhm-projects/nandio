const ObjectID              = require("mongoose").Types.ObjectId;
const MongoClient           = require("mongodb").MongoClient;
const path                  = require("path");
const fs                    = require("fs");
const request               = require('request');
const fastCSV               = require('fast-csv');

const { provinces }         = require('../../../www/packages/common/constants/provinces');
const { districts }         = require('../../../www/packages/common/constants/districts');
const timeUtils             = require('../../../www/utils/time_utils');

const databaseConfig        = require('../../../www/config/cf_database');
const cfMode                = require('../../../www/config/cf_mode');

let mongodUrl = "";
if (cfMode.database_product) {
    mongodUrl = `${databaseConfig.product._mongod_user === '' ? 'mongodb://' + databaseConfig.product._mongodb_host + ':' + databaseConfig.product._mongodb_port + '/' + databaseConfig.product._mongod_name :
        'mongodb://' + databaseConfig.product._mongod_user + ':' + databaseConfig.product._mongodb_pass + '@' + databaseConfig.product._mongodb_host + ':' + databaseConfig.product._mongodb_port + '/' + databaseConfig.product._mongod_name}`;
 }
else {
    mongodUrl = `${databaseConfig.development._mongod_user === '' ? 'mongodb://' + databaseConfig.development._mongodb_host + ':' + databaseConfig.development._mongodb_port + '/' + databaseConfig.development._mongod_name :
            'mongodb://' + databaseConfig.development._mongod_user + ':' + databaseConfig.development._mongodb_pass + '@' + databaseConfig.development._mongodb_host + ':' + databaseConfig.development._mongodb_port + '/' + databaseConfig.development._mongod_name}`;
}

const mongoClient = new MongoClient(mongodUrl);


const filterObject = (obj, filter, filterValue, filterCuster) => {
    return !filterCuster ?
        Object.keys(obj).reduce((acc, val) =>
            (obj[val][filter].toLowerCase() === filterValue.toLowerCase() ? {
                ...acc,
                [val]: obj[val]
            } : acc
        ), {})
            :
        Object.keys(obj).reduce((acc, val) =>
            {
                // console.log({ filter: obj[val][filter], filterCuster: obj[val][filterCuster], filterValue, check: (obj[val][filter] || obj[val][filterCuster]) === filterValue })
                let filterVal = obj[val][filter].toLowerCase();
                let filterValCuster = obj[val][filterCuster].toLowerCase();

                return ((filterVal === filterValue.toLowerCase()) || (filterValCuster === filterValue.toLowerCase()) ? {
                        ...acc,
                        [val]: obj[val]
                    } : acc
                )
            }, {})
}

const getObjItemByWardName = ({ district, wardName }) => {
    return new Promise(async resolve => {
        try {
            if (!district || !wardName)
                return resolve({ error: true, message: "invalid_param" });

            let listWards = [];
            let  filePath = path.resolve(__dirname, `../../../www/packages/common/constants/wards/${district}.json`);
            await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                if (district == '088') {
                    console.log({ err, data })
                }
                if (!err) {
                    if (!data)
                        return resolve({ error: true, message: "no_data_district" });

                    listWards = JSON.parse(data);

                    if (listWards && wardName) {
                        let itemWard = filterObject(listWards, 'name', wardName, 'name_with_type');
                        // console.log({ itemWard, district, wardName })
                        if (itemWard) {
                            return resolve({
                                error: false,
                                data: Object.values(itemWard)[0]
                            })
                        } else {
                            return resolve({ error: true, message: "cannot_get_itemWard" });
                        }
                    } else {
                        return resolve({ error: true, message: "cannot_get_listWards_or_wardName" });
                    }
                } else {
                    return resolve({ error: true, message: "district_not_exist" });
                }
            });
        } catch (error) {
            return { error: true, message: error.message };
        }
    })
}

const getObjItemByProvinceName = ({ provinceName }) => {
    let filterObject = (obj, filter, filterValue) =>
        Object.keys(obj).reduce((acc, val) =>
        (obj[val][filter] === filterValue ? {
            ...acc,
            [val]: obj[val]
        } : acc
    ), {});
    if (provinceName && provinceName.length) {
        if (provinceName.includes('TP')) {
            provinceName = provinceName.replace('TP', '')
            provinceName = provinceName.trim();
        }
        let infoProvince = filterObject(provinces, 'name', provinceName.toString())
        return Object.values(infoProvince)[0];
    }
}

const getObjItemByDistrictName = ({ province, districtName }) => {
    if (province && !Number.isNaN(Number(province))) {
        let listDistricts = filterObject(districts, 'parent_code', province.toString());

        let itemDistrict = filterObject(listDistricts, 'name', districtName.trim(), 'name_with_type')
        if (!itemDistrict) {
            console.log(`-----------------------itemDistrict--------------------------`)
        }

        return Object.values(itemDistrict)[0];
    } else {
        console.log(`--district: province null`)
    }
}


// ======================== START SCRIPT IMPORT ======================== ===========


function run1rd(dateP1 = '01/01/2000') {
    return new Promise(async (resolve, reject) => {
        const options = {
            'method': 'POST',
            'url': 'http://dms1.yensaothienviet.vn:8580/webresources/ios',
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Cookie': 'PHPSESSID=79183vm5mj4bb5igphlj0183g1'
            },
            body: `{\n    "MESSAGE":"DMS.PKG_SYNC_ASM.store_list_brand_crm",\n    "PARAMS": [\n        {"p1":"${dateP1}"}\n    ]\n}`
        };

        request(options, async function (error, response) {
            if (error) throw new Error(error);

            try {
                let dataResp = JSON.parse(response.body);
                console.log({ dataResp })
                let listStore = dataResp.DATA[0].RESULT_DATA;

                // fs.writeFile("./sync/dms/store/response.store.v2.json", response.body, 'utf8', function (err) {
                //     if (err) {
                //         console.log("An error occured while writing JSON Object to File.");
                //         return console.log(err);
                //     }

                //     console.log("JSON file has been saved.");
                // });

                // let rawdata = fs.readFileSync('./sync/dms/store/response.store.v2.json');
                // let store = JSON.parse(rawdata);
                // let listStore = store.DATA[0].RESULT_DATA;

                let dataAfterConvert = listStore.map(store => {
                    let [province_code,,district_code,,shop_code,shop_name,short_code,customer_name,address,province_name,district_name,precinct,lat,lng,phone,brand_code,brand_name, status] = store;

                    return { 
                        province: province_code, //PROVINCE_CODE
                        district: district_code, //DISTRICT_CODE
                        code: shop_code, //SHOP_CODE
                        name: shop_name, //SHOP_NAME
                        customer_code: short_code, //SHORT_CODE
                        customer_name: customer_name, //CUSTOMER_NAME
                        address: address, //ADDRESS
                        province_name: province_name, // PROVINCE_NAME
                        district_name: district_name, // DISTRICT_NAME
                        precinct: precinct, //PRECINCT
                        lat: lat, //LAT
                        lng: lng, //LNG
                        phone: phone, //PHONE
                        brandCode: brand_code, //BRAND CODE
                        brandName: brand_name, //BRAND NAME
                        status: +status === 0 ? 2 : 1
                    }
                })

                await mongoClient.connect();
                let db = mongoClient.db('nandio_store');
                let collection = db.collection('store_import');

                await collection.deleteMany({});
                let infoAfterInsert = await collection.insertMany(dataAfterConvert);
                console.log(`Inserted: ${infoAfterInsert.insertedCount} rows`);

                resolve(infoAfterInsert);
            } catch (error) {
                reject(error);
            }
        });
    })
}

function run2rd() {
    return new Promise(async (resolve, reject) => {
        try {
            await mongoClient.connect();
            const db = mongoClient.db('nandio_store');
            const storeImportColl = db.collection('store_import');
            const storeColl = db.collection('stores');
            const regionColl = db.collection('regions');
            const areaColl = db.collection('areas');
            const brandColl = db.collection('brands');
            const distributorColl = db.collection('distributors');

            let listStore = await storeImportColl.find({}).sort({ _id: -1 }).toArray();
            let dataInsertAll = [];
            let index = 0;

            for (const store of listStore) {
                console.log(`store index with -_id: ${index}, _id: ${store && store._id}`);
                index++;

                let {
                    code, province, district, customer_code, customer_name, address, precinct, 
                    lat, lng, phone, brandCode, province_name, district_name, status
                } = store;

                let isExistStore = await storeColl.findOne({ code: customer_code });

                if (isExistStore) {
                    let infoAfterPushBrandIntoStoreExists = await storeColl.findOneAndUpdate({ code: customer_code }, {
                        $addToSet: {
                            brands: brandCode
                        }
                    })

                    if(!infoAfterPushBrandIntoStoreExists)
                        console.log({ infoAfterPushBrandIntoStoreExists });

                    return;
                }    

                if (!isExistStore && code && code.length) {
                    let itemForInsert = {
                        code: customer_code,
                        name: customer_name,
                        phone,
                        address,
                        status,
                        state: 1,
                        createAt: timeUtils.getCurrentTime(),
                        modifyAt: timeUtils.getCurrentTime()
                    }

                    let infoDistributor = await distributorColl.findOne({ code });
                    if (infoDistributor) {
                        itemForInsert.distributor = infoDistributor._id;
                    }

                    if (brandCode) {
                        let infoBrand = await brandColl.findOne({ brandCode });
                        if (infoBrand) {
                            itemForInsert.brands = [ObjectID(infoBrand._id)];
                        }
                    }

                    if (province) {
                        let infoRegion = await regionColl.findOne({ code: province });
                        if (infoRegion) {
                            itemForInsert.region = infoRegion._id;
                        }
                    }

                    if (district) {
                        let infoArea = await areaColl.findOne({ code: district });
                        if (infoArea) {
                            itemForInsert.area = infoArea._id;
                        }
                    }

                    if (lat && lng) {
                        itemForInsert.location = {
                            type: "point",
                            coordinates: [Number(lng), Number(lat)]
                        }
                    }

                    if (province_name) {
                        /**
                         * có 2 trường hợp đặt biệt
                         *     bên trái là nội dung trong file import - bên phải là nội dung dữ liệu tỉnh thành
                         *    Hòa Bình -> Hoà Bình
                             Thừa Thiên - Huế ->Thừa Thiên Huế
                        */
                        if (province_name == 'Hòa Bình') {
                            province_name = 'Hoà Bình';
                        }
                        if (province_name == 'Thừa Thiên - Huế') {
                            province_name = 'Thừa Thiên Huế';
                        }
                        if (province_name == 'TIỀN GIANG') {
                            province_name = 'Tiền Giang';
                        }

                        let itemProvince = getObjItemByProvinceName({ provinceName: province_name });
                        if (itemProvince) {
                            let { code: provinceCode } = itemProvince;
                            itemForInsert.city = provinceCode;
                            itemForInsert.cityname = province_name;

                            console.log({ provinceCode, province_name })

                            if (district_name) {
                                if (district_name.includes('Huyện') || district_name.includes('huyen')) {
                                    district_name = district_name.replace('Huyện', '')
                                    district_name = district_name.trim();
                                }
                                if (district_name.includes('TP')) {
                                    district_name = district_name.replace('TP.', '')
                                    district_name = district_name.trim();
                                }

                                let itemDistrict = getObjItemByDistrictName({
                                    province: provinceCode,
                                    districtName: district_name
                                })
                                if (itemDistrict) {
                                    let { code: districtCode } = itemDistrict;
                                    itemForInsert.district = districtCode;
                                    itemForInsert.districtName = district_name;

                                    console.log({ districtCode, district_name })

                                    if (precinct) {
                                        let infoWard = await getObjItemByWardName({
                                            district: districtCode,
                                            wardName: precinct
                                        })
                                        console.log({ infoWard, districtCode, precinct })
                                        if (!infoWard.error) {
                                            let dataOfWard = infoWard.data;
                                            if (dataOfWard) {
                                                let { code: wardCode } = dataOfWard
                                                if (wardCode) {
                                                    itemForInsert.ward = wardCode;
                                                    itemForInsert.wardName = precinct;
                                                    console.log({ wardCode, precinct })
                                                }
                                            }
                                        } else {
                                            console.log(`code: ${code}, district: ${district_name}, precinct: ${precinct}`)
                                        }
                                    }
                                }
                                else {
                                    console.log(`----------KHÔNG TÌM THẤY DISTRICT--------`)
                                    console.log({ itemDistrict, provinceCode, district_name })
                                }
                            }

                        }
                    }

                    console.log('================================')

                    dataInsertAll = [
                        ...dataInsertAll, itemForInsert
                    ]
                }
            }

            const infoAfterInsert = await storeColl.insertMany(dataInsertAll);
            console.log(`Inserted: ${infoAfterInsert.insertedCount} rows`);
            console.log({ STORE_LENGTH: listStore.length })

            resolve(infoAfterInsert);
        } catch (error) {
            reject(error);
        }
    })
}

function run3rd() {
    return new Promise(async resolve => {
        await mongoClient.connect();
        const db = mongoClient.db('nandio_store');
        const storeColl = db.collection('stores');
        let listStore = await storeColl.find({}).sort({ _id: -1 }).toArray();
        let index = 0;

        for (const store of listStore) {
            console.log(`store index with -_id: ${index}, _id: ${store && store._id}`)
            index++;
            let { code } = store;
            let listStoreExists = await storeColl.find({ code }).toArray();
            if (listStoreExists && listStoreExists.length === 1) {
                console.log(`---ko có trùng----`)
                //ko có trùng
                continue;
            } else {// có trùng
                console.log(`---có trùng----`)
                let [fistItemStore, ...listStoreDuplicateForRemove] = listStoreExists;
                if (!fistItemStore) return;

                // tìm danh sách brands của những thằng trùng
                let listBrandAfterMerge = [];
                listStoreExists.forEach(item => {
                    if(item.brands && item.brands.length){
                        listBrandAfterMerge = [...listBrandAfterMerge, ...item.brands]
                    }
                });

                // xoá từ thằng thứ 2 -> n-1
                if (listStoreDuplicateForRemove && listStoreDuplicateForRemove.length) {
                    let listStoreDuplicateForRemoveIDs = [...listStoreDuplicateForRemove.map(item => ObjectID(item._id))];
                    let infoAfterRemove = await storeColl.deleteMany({
                        _id: {
                            $in: listStoreDuplicateForRemoveIDs
                        }
                    });
                    // console.log({ infoAfterRemove })
                } else {
                    // console.log(`--error---01---`)
                }

                await storeColl.findOneAndUpdate({ _id: fistItemStore._id }, {
                    $set: {
                        brands: listBrandAfterMerge
                    }
                });
            }
        }

        resolve();
    })
}

function run4rd() {
    return new Promise(async (resolve, reject) => {
        try {
            let stream = fs.createReadStream('./sync/dms/store/csv/vw_userstore.csv');
            let csvData = [];
            let numberRow = 0;

            let csvStream = fastCSV
            .parse()
            .on("data", async function(data) {
                console.log(`numberRow: ${numberRow}`);
                numberRow++;

                const [,region_code,,area_code,,distributor_code,,,,userID,,,,,,store_code] = data;

                if(region_code && area_code && distributor_code && userID && store_code){
                    csvData[csvData.length] = {
                        region_code,
                        area_code,
                        distributor_code,
                        userID,
                        store_code: store_code.split('_')[1],
                    }
                }

                console.log({
                    region_code,
                    area_code,
                    distributor_code,
                    userID,
                    store_code: store_code.split('_')[1],
                })
            })
            .on("end", async function() {
                // remove the first line: header
                csvData.shift();
                console.log(`-----------------------------`)
                console.log({ csvData })
                console.log({ LENGTH: csvData.length })
                console.log(`-----------------------------`)

                await mongoClient.connect();
                const db = mongoClient.db('nandio_store');
                const employeeImportColl = db.collection('employee_store_import');

                await employeeImportColl.deleteMany({});
                const infoAfterInsert = await employeeImportColl.insertMany(csvData);
                console.log(`Inserted: ${infoAfterInsert.insertedCount} rows`);

                resolve(infoAfterInsert);
            });

            stream.pipe(csvStream);
        } catch (error) {
            reject(error);   
        }
    })
}

function run5rd() {
    return new Promise(async (resolve, reject) => {
        try {
            let stream = fs.createReadStream('./sync/dms/store/csv/user.csv');
            let csvData = [];
            let numberRow = 0;

            await mongoClient.connect();
            const db = mongoClient.db('nandio_store');

            const employeeStoreImportColl   = db.collection('employee_store_import');
            const regionColl                = db.collection('regions');
            const areaColl                  = db.collection('areas');
            const distributorColl           = db.collection('distributors');
            const employeeColl              = db.collection('employees');
            const detailTypeEmployeeColl    = db.collection('detail_type_employees');

            let csvStream = fastCSV
            .parse()
            .on("data", async function(data) {
                console.log(`numberRow: ${numberRow}`);
                numberRow++;

                const [idSql, username, fullname,,,,email,,,,,,shopID,,,,,phone,type] = data;

                if(type && ['A','B','C'].includes(type.trim())){
                    console.log({
                        idSql,
                        username,
                        fullname,
                        email,
                        shopID,
                        phone,
                        type
                    })

                    const infoEmployeeStore = await employeeStoreImportColl.findOne({ userID: idSql });

                    if(infoEmployeeStore){
                        const { region_code, area_code, distributor_code, store_code } = infoEmployeeStore;
                        const dataInsertEmployee = {
                            fullname,
                            username,
                            email,
                            code: username,
                            roles: ["61799ef6ccd7cf418c13caa3"],
                            status: 1,
                            state: 1,
                            modifyAt: timeUtils.getCurrentTime(),
                            createAt: timeUtils.getCurrentTime(),
                        };

                        const infoRegion = await regionColl.findOne({ code: region_code });
                        if(infoRegion){
                            dataInsertEmployee.region = infoRegion._id;
                        }

                        const infoArea = await areaColl.findOne({ code: area_code });
                        if(infoArea){
                            dataInsertEmployee.area = infoArea._id;
                        }

                        const infoDistributor = await distributorColl.findOne({ code: distributor_code });
                        if(infoDistributor){
                            dataInsertEmployee.distributor = infoDistributor._id;
                        }

                        let conditionTypeEmployee = {};
                        switch (type.trim()) {
                            case 'A':
                                conditionTypeEmployee.name = 'Loại A';
                                break;
                            case 'B':
                                conditionTypeEmployee.name = 'Loại B';
                                break;
                            case 'C':
                                conditionTypeEmployee.name = 'Loại C';
                                break;
                            default:
                                break;
                        }

                        const infoTypeEmployee = await detailTypeEmployeeColl.findOne(conditionTypeEmployee);
                        if(infoTypeEmployee){
                            dataInsertEmployee.detail_type_employee = infoTypeEmployee._id;
                            dataInsertEmployee.type_employee = infoTypeEmployee.parent;
                        }

                        const infoAfterInsert = await employeeColl.insertOne(dataInsertEmployee);
                        console.log({ infoAfterInsert });
                    }
                }
            })
            .on("end", async function() {
                console.log(`-----------------------------`)
                console.log({ csvData })
                console.log({ LENGTH: csvData.length })
                console.log(`-----------------------------`)

                // const employeeColl = db.collection('employees');
                // const infoAfterInsert = await employeeColl.insertMany(csvData);
                // console.log(`Inserted: ${infoAfterInsert.insertedCount} rows`);

                resolve(csvData);
            });

            stream.pipe(csvStream);
        } catch (error) {
            reject(error);   
        }
    })
}

// (async () => {
//     try {
//         await run1rd();
//         await run2rd();
//         await run3rd();
//         // await run4rd();
//         // await run5rd();
//         console.log('Sync done!!');
//     } catch (error) {
//         console.error(error);
//     }
// })()

module.exports = async dateP1 => {
    try {
        await run1rd(dateP1);
        await run2rd();
        await run3rd();
        console.info('Sync done!!', dateP1);
    } catch (error) {
        console.error(error);
    }
}
