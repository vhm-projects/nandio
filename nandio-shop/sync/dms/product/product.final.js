let PRODUCT_TEMP_COLL               = require('../../../www/packages/product/databases/product_temp-coll');
let PRODUCT_COLL                    = require('../../../www/packages/product/databases/product_temp-coll');
let BRAND_COLL                      = require('../../../www/packages/brand/databases/brand-coll');
const mongodb                   = require("mongodb").MongoClient;
const path                      = require("path");
const fs                        = require("fs");
const ObjectID                        = require("mongoose").Types.ObjectId;

const { provinces }                                 = require('../../../www/packages/common/constants/provinces');
const { districts }                                 = require('../../../www/packages/common/constants/districts');
// const { provinces }                                 = require('../../../www/packages/common/constants/wards');
let url = "mongodb://localhost:27017";

const checkDateValid = (d) => {
    if (Object.prototype.toString.call(d) === "[object Date]") {
        // it is a date
        if (isNaN(d.getTime())) {  // d.valueOf() could also work
          // date is not valid
          return false;
        } else {
          // date is valid
          return true;
        }
      } else {
        // not a date
        return false;
      }
}

async function run() {
    let listProducts            = await PRODUCT_TEMP_COLL.find({}).sort({ _id: -1 }).lean();
    let infoAferUpdateAll = [];
    let index = 0;
    for (const product of listProducts) {
        console.log(`product index with -_id: ${index}, _id: ${product && product._id}`)
        index++;

        const {
            id,
            code,
            name,
            brandCode,
            brandName,
            variant,
            convfact,
            uom,
            description,
            price,
            priceP1,
            priceP2,
            updateDate,
        } = product;
    

        if (id && id.length && code && code.length) {
            let itemForInsert = {
                idSQL: id,
                productCode: code,
                name,
                description,
                status: 0, //mặc định: tắt hoạt động
            }
            if (brandCode) {
                let infoBrand = await BRAND_COLL.findOne({ brandCode: brandCode }).lean();
                if (infoBrand) {
                    itemForInsert = {
                        ...itemForInsert,
                        brand: ObjectID(infoBrand._id)
                    }
                }
            }

            if (price && !Number.isNaN(parseInt(price))) {
                itemForInsert = {
                    ...itemForInsert,
                    price
                }
            }

            if (priceP1 && !Number.isNaN(parseInt(priceP1))) {
                itemForInsert = {
                    ...itemForInsert,
                    priceP1
                }
            }

            if (priceP2 && !Number.isNaN(parseInt(priceP2))) {
                itemForInsert = {
                    ...itemForInsert,
                    priceP2
                }
            }
            let dateInsert ;
            if (checkDateValid(new Date(updateDate))) {
                dateInsert = new Date(updateDate)
            } else {
                dateInsert = new Date(Date.now())
            }

            itemForInsert = {
                ...itemForInsert, 
                createAt: dateInsert,
                modifyAt: dateInsert
            }

            infoAferUpdateAll = [
                ...infoAferUpdateAll, itemForInsert
            ]
        }
    }

    return infoAferUpdateAll;
}

run()
    .then(listProductConverted => {
        mongodb.connect(
            url,
            { useNewUrlParser: true, useUnifiedTopology: true },
            (err, client) => {
              console.log({ err, client });
              if (err) throw err;
              client
                .db("nandio")
                .collection("product_final")
                .insertMany(listProductConverted, (err, res) => {
                  if (err) throw err;
        
                  console.log(`Inserted: ${res.insertedCount} rows`);
                  client.close();
                });
            }
        );
    })
    .catch(error => console.log({ error }))