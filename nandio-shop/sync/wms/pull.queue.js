var AWS             = require('aws-sdk');
const { config }    = require('../../www/packages/upload-s3/constants');
const PARCEL_MODEL  = require('../../www/packages/sku-2/models/parcel').MODEL;

// const { Consumer } = require('sqs-consumer');

// AWS.config.update({
//     accessKeyId: config.aws_access_key_id,
//     secretAccessKey: config.aws_secret_access_key,
//     region: config.aws_region,
// });

// // Create an SQS service object
// var sqs = new AWS.SQS({apiVersion: '2012-11-05'});
// var queueURL = "https://sqs.ap-southeast-1.amazonaws.com/311139337643/WMS-v2";

// // Create our consumer
// const app = Consumer.create({
//     queueUrl: queueURL,
//     handleMessage: async (message) => {
//         let { MessageId, ReceiptHandle, MD5OfBody, Body } = message;
//         let dataAfterParse = JSON.parse(Body);
//         console.log({
//             dataAfterParse
//         })
//     },
//     sqs: new AWS.SQS()
// });

// app.on('error', (err) => {
//     console.error(err.message);
// });

// app.on('processing_error', (err) => {
//     console.error(err.message);
// });

// console.log('WMS pull queue PROCESS');
// app.start();

// // sqs.receiveMessage(params, function(err, data) {
// //     console.log({
// //         ['err, data']: err, data
// //     })
// //     // if (err) {
// //     //     console.log("Receive Error", err);
// //     // } else if (data.Messages) {
// //     //     var deleteParams = {
// //     //         QueueUrl: queueURL,
// //     //         ReceiptHandle: data.Messages[0].ReceiptHandle
// //     //         };
// //     //         sqs.deleteMessage(deleteParams, function(err, data) {
// //     //             if (err) {
// //     //                 console.log("Delete Error", err);
// //     //             } else {
// //     //                 console.log("Message Deleted", data);
// //     //             }
// //     //         });
// //     // }
// // });


// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({
    accessKeyId: config.aws_access_key_id,
    secretAccessKey: config.aws_secret_access_key,
    region: config.aws_region,
});
// Create the SQS service object
var sqs = new AWS.SQS({apiVersion: '2012-11-05'});
var queueURL = process.env.QUEUE_WMS_QR || process.env.QUEUE_WMS_QR_DEVELOPMENT;
// var queueURL = "https://sqs.ap-southeast-1.amazonaws.com/311139337643/WMS-v2-Development";

var params = {
    AttributeNames: [
        "SentTimestamp"
    ],
    MaxNumberOfMessages: 1,
    MessageAttributeNames: [
        "All"
    ],
    QueueUrl: queueURL,
    WaitTimeSeconds: 20
};

const runComsumer = () => {
    sqs.receiveMessage(params, async function(err, data) {
        if (err) {
            console.log("Receive Error", err);
        } else if (data.Messages) {
            // console.log({
            //     'data.Messages': data.Messages
            // })
            let { MessageId, ReceiptHandle, MD5OfBody, Body } = data.Messages[0];
            let dataAfterParse = JSON.parse(Body);
            if (!Array.isArray(dataAfterParse)) return;

            // ĐẦU 1 => Kích hoạt từ ngày 03/12
            let dataAfterParseSku1 = dataAfterParse.filter(item => item.sku && item.sku.indexOf('1') == 0);
           
            if (dataAfterParseSku1 && dataAfterParseSku1.length) { // SKU 1
                const infoAfterInsertSku1 = await PARCEL_MODEL.syncDataSku1({ 
                    data: dataAfterParseSku1
                });
                
            }

            // ĐẦU 2 => Kích hoạt từ ngày 10/11
            dataAfterParse = dataAfterParse.filter(item => item.sku && item.sku.indexOf('2') == 0);
            
            const infoAfterInsert = await PARCEL_MODEL.syncData({ 
                data: dataAfterParse
            });
            if (infoAfterInsert.error) {
                // do wms đẩy mã đầu 2, nên cần xóa
                var deleteParams = {
                    QueueUrl: queueURL,
                    ReceiptHandle: ReceiptHandle
                };
                sqs.deleteMessage(deleteParams, function(err, data) {
                    if (err) {
                        console.log("Delete Error", err);
                    } else {
                        console.log("Message Deleted", data);
                    }
                });
            }
            
            if (!infoAfterInsert.error) {
                var deleteParams = {
                    QueueUrl: queueURL,
                    ReceiptHandle: ReceiptHandle
                };
                sqs.deleteMessage(deleteParams, function(err, data) {
                    if (err) {
                        console.log("Delete Error", err);
                    } else {
                        console.log("Message Deleted", data);
                    }
                });
            }

           
        }
    });
}

setInterval(runComsumer, 1 * 60 * 60);