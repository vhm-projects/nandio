module.exports = {
    apps: [
        {
            name: "NANDIO_SHOP",
            script: "app.js",
            max_memory_restart: "400M", // cho mỗi process
            log_date_format : "YYYY-MM-DD HH:mm Z",
            ignore_watch: ["node_modules"],
            watch_options: {
                followSymlinks: false,
            },
            env_production: {
                    "NODE_ENV": "production",
            },
        }
    ],
};
