const { execFile } = require('child_process');
const moment = require('moment');
const cron = require('cron');

function initCron(time, cb = null) {
    return new cron.CronJob({
        cronTime: time,
        start: true, 
        timeZone: 'Asia/Ho_Chi_Minh',
        onTick: function() {
          console.log('Cron job runing...', __dirname);

          if (cb instanceof Function) {
            return cb();
          }

          execFile(__dirname + '/backup/backup_db_to_s3.sh', (error, stdout, stderr) => {
              if (error) {
                console.error(`error: ${error.message}`);
                return;
              }

              if (stderr) {
                console.error(`stderr:\n${stderr}`);
                return;
              }

              console.log(`stdout:\n${stdout}`);
          });
        }
    })
}

function runSyncProduct() {
  const syncProduct = require('./sync/dms/product/product.import');
  const datePrevious = moment().subtract(1, 'months').format('DD/MM/YYYY');
  syncProduct(datePrevious);
}

// initCron('00 59 23 1 * 0', runSyncProduct).start(); // 23:59 ngày đầu trong tháng
initCron('00 15 12 * * 0-6').start(); // Chạy Jobs vào 12h15 trưa
initCron('00 15 00 * * 0-6').start(); // Chạy Jobs vào 00h15 hằng đêm
